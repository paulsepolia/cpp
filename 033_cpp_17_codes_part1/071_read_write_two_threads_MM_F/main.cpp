#include <iostream>
#include <cmath>
#include <chrono>
#include <iomanip>
#include <vector>
#include <memory>
#include <thread>
#include <mutex>
#include <array>
#include <numeric>
#include <atomic>

const auto DIM_DATA = 2 * static_cast<uint64_t>(std::pow(10.0, 8.0));
std::mutex mtx{};
std::vector<double> data_vec{};
std::atomic<bool> flg_write = true;
double sum_loc = 0;
uint64_t runs = 0;

void read_delete_data() {

    while (true) {

        const auto dura = std::chrono::milliseconds(500);
        std::this_thread::sleep_for(dura);
        flg_write = false;

        std::unique_lock<std::mutex> lck(mtx, std::defer_lock);
        lck.lock();

        if (data_vec.size() >= DIM_DATA) {

            sum_loc = std::accumulate(data_vec.begin(), data_vec.end(), 0.0);
            runs++;

            std::cout << " --------------------->> run = " << runs << std::endl;
            std::cout << " -->> extra = " << static_cast<uint64_t>(sum_loc - DIM_DATA) << std::endl;

            data_vec.clear();
            data_vec.shrink_to_fit();
        }

        flg_write = true;
        lck.unlock();
    }
}

void write_data() {

    while (true) {
        if (flg_write) {
            std::unique_lock<std::mutex> lck(mtx, std::defer_lock);
            lck.lock();
            data_vec.push_back(1.0);
            lck.unlock();
        }
    }
}

int main() {

    std::thread th1(write_data);
    std::thread th2(read_delete_data);

    th1.join();
    th2.join();

    int sentinel = 0;
    std::cout << "Enter integer to exit local scope: ";
    std::cin >> sentinel;
    std::cout << "pgg --> after the exit" << std::endl;
}
