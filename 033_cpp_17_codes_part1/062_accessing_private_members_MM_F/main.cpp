#include <iostream>
#include <memory>

class A {

public:

    A() : x1(1), x2(2), x3(3), x4(4) {}

    virtual ~A() = default;

private:

    int32_t x1;
    int32_t x2;
    int32_t x3;
    int32_t x4;
};

int main() {

    A a;
    auto *pi = reinterpret_cast<int32_t *>(&a);

    for (int32_t i = 0; i < 10; i++) {
        std::cout << *(pi++) << std::endl;
    }

    return 0;
}