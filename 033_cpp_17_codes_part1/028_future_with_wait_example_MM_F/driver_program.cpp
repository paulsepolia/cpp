#include <iostream>
#include <future>
#include <thread>

uint64_t fib(uint64_t n) {
    if (n < 3) return 1;
    else return fib(n - 1) + fib(n - 2);
}

int main() {

    std::future<uint64_t> f1 = std::async(std::launch::async, []() {
        return fib(50);
    });

    std::cout << "waiting..." << std::endl;

    std::future_status status;

    do {

        status = f1.wait_for(std::chrono::milliseconds(10));

        if (status == std::future_status::deferred) {
            std::cout << "deferred" << std::endl;
        } else if (status == std::future_status::timeout) {

            std::future<uint64_t> f2 = std::async(std::launch::async, []() {
                return fib(47);
            });

            std::cout << "timeout --> f2.get() = " << f2.get() << std::endl;

        } else if (status == std::future_status::ready) {
            std::cout << "ready!" << std::endl;
        }
    } while (status != std::future_status::ready);


    std::cout << " --> ready to get the results " << std::endl;

    std::this_thread::sleep_for(std::chrono::milliseconds(1000));

    std::cout << "f1: " << f1.get() << std::endl;

}
