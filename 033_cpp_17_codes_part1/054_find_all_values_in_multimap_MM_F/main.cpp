#include <map>
#include <iterator>
#include <algorithm>
#include <iostream>

int main() {

    {
        // MultiMap of char and int

        std::multimap<char, int> mmapOfPos = {
                {'a', 1},
                {'b', 2},
                {'c', 3},
                {'a', 4},
                {'c', 5},
                {'c', 6},
                {'d', 7},
        };

        typedef std::multimap<char, int>::iterator MMAPIterator;

        // it returns a pair representing the range of elements with key equal to 'c'

        std::pair<MMAPIterator, MMAPIterator> result = mmapOfPos.equal_range('c');

        std::cout << "All values for key 'c' are," << std::endl;

        // iterate over the range

        for (auto it = result.first; it != result.second; it++) {
            std::cout << it->second << std::endl;
        }

        // total elements in the range

        const auto count = std::distance(result.first, result.second);

        std::cout << "Total values for key 'c' are : " << count << std::endl;
    }

    {
        // MultiMap of char and int

        std::multimap<size_t, size_t> mm;

        for(size_t i = 0; i < 1000; i++) {
            for (size_t j = 0; j < 1000; j++) {
                mm.insert(std::pair<size_t, size_t>(i, j));
            }
        }

        // # 1
        const auto result1 = mm.equal_range(1);
        for (auto it = result1.first; it != result1.second; it++) {
            std::cout << it->second << std::endl;
        }

        // total elements in the range
        const auto count1 = std::distance(result1.first, result1.second);
        std::cout << "Total values for key '1' are : " << count1 << std::endl;

        // # 2
        const auto result2 = mm.equal_range(1);
        for (auto it = result2.first; it != result2.second; it++) {
            std::cout << it->second << std::endl;
        }

        // total elements in the range
        const auto count2 = std::distance(result2.first, result2.second);
        std::cout << "Total values for key '2' are : " << count2 << std::endl;
    }

    return 0;
}