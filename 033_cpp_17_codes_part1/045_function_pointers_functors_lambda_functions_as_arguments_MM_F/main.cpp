#include <iostream>
#include <memory>
#include <functional>

std::string fun1(const std::string &val) {
    const std::string res = "1-" + val + "-2";
    return res;
}

std::string fun2(const std::string &val) {
    const std::string res = "2-" + val + "-3";
    return res;
}

std::string fun3(const std::string &val) {
    const std::string res = "3-" + val + "-4";
    return res;
}

std::string f1(const std::string &val,
               std::string(*f_call_back)(const std::string &input)) {

    const std::string res = f_call_back(val);
    return res;
}

class functor1 {
public:
    std::string operator()(const std::string &val) {
        const std::string res = "1-" + val + "-2";
        return res;
    }
};

class functor2 {
public:
    std::string operator()(const std::string &val) {
        const std::string res = "2-" + val + "-3";
        return res;
    }
};

class functor3 {
public:
    std::string operator()(const std::string &val) {
        const std::string res = "3-" + val + "-4";
        return res;
    }
};

std::string f2(const std::string &val,
               const std::function<std::string(std::string)> &Arg1) {

    const std::string res = Arg1(val);
    return res;
}

int main() {

    {
        // use of function pointers here

        std::cout << "--> use of functions" << std::endl;

        std::cout << f1("A", fun1) << std::endl;
        std::cout << f1("A", fun2) << std::endl;
        std::cout << f1("A", fun3) << std::endl;
    }

    {
        // use of functors here

        std::cout << "--> use of functors" << std::endl;

        std::cout << f2("A", functor1()) << std::endl;
        std::cout << f2("A", functor2()) << std::endl;
        std::cout << f2("A", functor3()) << std::endl;
    }

    {
        // use of functors here (another way)

        std::cout << "--> use of functors (another way)" << std::endl;

        const auto functorObj1 = functor1();
        const auto functorObj2 = functor2();
        const auto functorObj3 = functor3();

        std::cout << f2("A", functorObj1) << std::endl;
        std::cout << f2("A", functorObj2) << std::endl;
        std::cout << f2("A", functorObj3) << std::endl;
    }

    {
        // use of lamda functions here

        const auto l1 = [](std::string val) -> std::string {
            return "1-" + val + "-2";
        };

        const auto l2 = [](std::string val) -> std::string {
            return "2-" + val + "-3";
        };

        const auto l3 = [](std::string val) -> std::string {
            return "3-" + val + "-4";
        };

        std::cout << "--> use of lamda functions" << std::endl;

        std::cout << f2("A", l1) << std::endl;
        std::cout << f2("A", l2) << std::endl;
        std::cout << f2("A", l3) << std::endl;
    }


    return 0;
}