#include <iostream>
#include <thread>
#include <mutex>
#include <condition_variable>

std::mutex mtx;
std::condition_variable cv;
bool ready = false;

void print_id(int id) {

    std::unique_lock<std::mutex> lck(mtx);

    while (!ready) {
        cv.wait(lck);
    }

    std::cout << "thread " << id << std::endl;
}

void go() {

    std::unique_lock<std::mutex> lck(mtx);
    ready = true;
    cv.notify_all();
}

int main() {

    const int nt = 100;

    std::thread threads[nt];

    std::cout << " --> create 10 threads and sleep..." << std::endl;

    for (int i = 0; i < nt; ++i) {
        threads[i] = std::thread(print_id, i);
    }

    std::this_thread::sleep_for(std::chrono::seconds(5));

    std::cout << " --> 10 threads ready to race..." << std::endl;

    go();

    std::cout << " --> 10 threads are done and join them now..." << std::endl;

    for (auto &th : threads) {
        th.join();
    }

    return 0;
}