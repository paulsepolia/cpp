#include <iostream>
#include <future>
#include <thread>

uint64_t fib(uint64_t n) {

    if (n < 3) {
        return 1;
    } else {
        return fib(n - 1) + fib(n - 2);
    }
}

int main() {

    // future from a promise

    std::promise<uint64_t> p;

    std::future<uint64_t> fut = p.get_future();

    std::thread([&p] {
        std::cout << "fib(50) --> 1" << std::endl;
        uint64_t res = fib(50);
        std::cout << "fib(50) --> 2" << std::endl;
        p.set_value_at_thread_exit(res);
    }).detach();

    std::cout << "Waiting..." << std::endl << std::flush;
    fut.wait();
    std::cout << "Results are: " << fut.get() << std::endl;

}