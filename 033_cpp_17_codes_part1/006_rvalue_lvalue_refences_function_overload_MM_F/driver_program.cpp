#include <iostream>


// The compiler treats a named rvalue reference
// as an lvalue and an unnamed rvalue reference as an rvalue.

class A {
    // TODO: Add resources for the class here.
};

void f1(const A &) {
    std::cout << "In f1(const A&). This version cannot modify the parameter." << std::endl;
}

void f1(A &&) {
    std::cout << "In f1(A&&). This version can modify the parameter." << std::endl;
}

void f2(const A &) {
    std::cout << "In f2(const A&). This version cannot modify the parameter." << std::endl;
}

int main() {

    {
        A block;
        f1(block); // uses the: f1(const A &)
        f1(A()); // uses the: f1(A &&)
    }

    {
        A block;
        f2(block); // uses the f2(const A &)
        f2(A()); // same as above
    }
}
