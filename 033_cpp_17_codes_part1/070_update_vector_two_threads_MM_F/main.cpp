#include <iostream>
#include <cmath>
#include <chrono>
#include <iomanip>
#include <vector>
#include <memory>
#include <thread>
#include <mutex>

const auto DIM_DATA = 10 * static_cast<uint64_t>(std::pow(10.0, 7.0));
const auto DO_MAX = static_cast<uint64_t>(std::pow(10.0, 8.0));
const uint32_t PRECISION_LOC = 8;

std::mutex mtx;

void mult(std::vector<double> data_vec, double coeff, std::vector<double>& data_res, double& time_mult) {

	std::unique_lock<std::mutex> lck(mtx, std::defer_lock);
	lck.lock();
	std::cout << "-->> mult is executing with thread id = " << std::this_thread::get_id()
		<< " and coeff = " << coeff << std::endl;
	lck.unlock();

	const auto t1 = std::chrono::steady_clock::now();

	for (auto& el : data_vec) {
		el *= coeff;
	}

	data_res = std::move(data_vec);
	const auto t2 = std::chrono::steady_clock::now();
	time_mult = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count();
}

int main() {

	std::cout << std::fixed;
	std::cout << std::setprecision(PRECISION_LOC);

	std::vector<double> data_vec(DIM_DATA, 1.0);
	double time_glr = 0;

	for (uint64_t i = 1; i <= DO_MAX; i++) {

		std::cout << "--------------------------------------->> run = " << i << std::endl;

		std::vector<double> vec1(DIM_DATA, 0.0);
		std::vector<double> vec2(DIM_DATA, 0.0);
		double time1;
		double time2;

		std::thread th1(mult, data_vec, 2, std::ref(vec1), std::ref(time1));
		std::thread th2(mult, data_vec, 3, std::ref(vec2), std::ref(time2));

		th1.join();
		th2.join();

		time_glr += time1 + time2;

		std::cout << " --> time1 = " << time1 << std::endl;
		std::cout << " --> time2 = " << time2 << std::endl;
		std::cout << " --> t/r   = " << time_glr / i << std::endl;
		std::cout << " --> vec1[DIM_DATA-1] = " << vec1[DIM_DATA - 1] << std::endl;
		std::cout << " --> vec2[DIM_DATA-1] = " << vec2[DIM_DATA - 1] << std::endl;
	}

	int sentinel = 0;
	std::cout << "Enter integer to exit local scope: ";
	std::cin >> sentinel;
	std::cout << "pgg --> after the exit" << std::endl;
}
