// future example

#include <iostream>
#include <future>
#include <chrono>

// a non-optimized way of checking for prime numbers

const uint64_t PRIME_NUM = 444444443;
uint64_t index_loc = 0;
const uint64_t TIME_WAIT = 100;

bool is_prime(uint64_t x) {
    bool flg = true;

    for (int i2 = 0; i2 < 10; i2++) {
        index_loc = 0;
        std::cout << std::endl;
        std::cout << "---------------------->> " << i2 << std::endl << std::flush;
        flg = true;
        for (uint64_t i = 2; i < x; ++i) {
            if (x % i == 0) flg = false;
        }
    }

    return flg;
}

int main() {

    // call function asynchronously
    std::future<bool> fut = std::async(is_prime, PRIME_NUM);

    // do something while waiting for function to set future:
    std::cout << "checking, please wait";
    std::chrono::milliseconds span(TIME_WAIT);

    while (true) {

        const auto val = fut.wait_for(span); // waits here and then gets a value

        if (val == std::future_status::timeout) {
            index_loc++;
            std::cout << index_loc << " " << std::flush;
        } else {
            std::cout << " ----> break here" << std::endl << std::flush;
            break;
        }
    }

    bool x = fut.get();     // retrieve return value

    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << std::endl << PRIME_NUM
              << (x ? " is prime" : " is not prime") << std::endl;

    return 0;
}
