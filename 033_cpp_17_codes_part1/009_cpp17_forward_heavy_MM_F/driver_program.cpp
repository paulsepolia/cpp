// forward example

#include <utility>
#include <iostream>
#include <vector>
#include <cmath>
#include <chrono>

// pass by reference is faster than pass by rvalue reference (move)
// and of course much faster than pass by value

// function with lvalue and rvalue reference overloads

void overloaded(const std::vector<double> &x) {
    std::cout << "[lvalue] --> const std::vector<double> &x --> x[0] = " << x[0] << std::endl;
}

void overloaded(std::vector<double> &&x) {
    std::cout << "[rvalue] --> std::vector<double> &&x --> x[0] = " << x[0] << std::endl;
}

void overloaded2(std::vector<double> x) {
    std::cout << "[lvalue] --> std::vector<double> x --> x[0] = " << x[0] << std::endl;
}

// function template taking rvalue reference to deduced type

template<class T>
void fn(T &&x) {
    overloaded(x);                   // always an lvalue
    overloaded(std::forward<T>(x));  // rvalue if argument is rvalue
}

template<class T>
void fn2(T x) {
    overloaded2(x);                   // always an lvalue
}

int main() {

    const auto DIM = static_cast<uint64_t>(std::pow(10.0, 9.0));
    std::vector<double> v;
    v.resize(DIM);

    {
        auto start = std::chrono::system_clock::now();
        std::cout << "calling fn with lvalue: " << std::endl;
        fn(v);
        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_seconds = end - start;
        std::cout << "elapsed time build: " << elapsed_seconds.count() << "s" << std::endl;
    }

    {
        auto start = std::chrono::system_clock::now();
        std::cout << "calling fn with rvalue: " << std::endl;
        fn(std::vector<double>(std::move(v)));
        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_seconds = end - start;
        std::cout << "elapsed time build: " << elapsed_seconds.count() << "s" << std::endl;
    }

    {
        // pass by value same size vector
        std::vector<double> v1;
        v1.resize(DIM);
        auto start = std::chrono::system_clock::now();
        std::cout << "calling fn2 with lvalue: " << std::endl;
        fn2(v1);
        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_seconds = end - start;
        std::cout << "elapsed time build: " << elapsed_seconds.count() << "s" << std::endl;
    }
}
