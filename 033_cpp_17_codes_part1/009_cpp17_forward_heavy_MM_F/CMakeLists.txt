cmake_minimum_required(VERSION 3.10)
project(cpp17_2)

set(CMAKE_CXX_STANDARD 17)

add_executable(cpp17_2 driver_program.cpp)