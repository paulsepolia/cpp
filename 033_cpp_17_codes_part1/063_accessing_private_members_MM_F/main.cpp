#include <iostream>
#include <memory>

class A {

public:

    A() : x1{1, 2, 3, 4, 5, 6, 7, 8, 9, 10} {}

    virtual ~A() = default;

private:

    int32_t x1[10];
};

int main() {

    A a;
    auto *pi = reinterpret_cast<int32_t *>(&a);

    for (int32_t i = 1; i <= 20; i++) {
        std::cout << " line --> " << i << " --> " << *(pi++) << std::endl;
    }

    return 0;
}
