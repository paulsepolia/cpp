// future example

#include <iostream>
#include <future>
#include <chrono>
#include <cmath>
#include <mutex>

static uint64_t index_gen = 0;
static std::mutex mtx;

double heavy_load(const double &large_value) {

    const auto t1 = std::chrono::high_resolution_clock::now();

    mtx.lock();
    index_gen++;
    uint64_t index_loc = index_gen;
    std::cout << "--> heavy load id = " << index_loc << std::endl;
    mtx.unlock();

    double sum = 0;
    while (true) {
        sum++;
        if (sum >= large_value) break;
    }

    const auto t2 = std::chrono::high_resolution_clock::now();
    const auto time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);

    mtx.lock();
    std::cout << "--> exit heavy load id = " << index_loc << std::endl;
    std::cout << "--> took (secs) = " << time_span.count() << std::endl;
    mtx.unlock();

    return sum;
}


int main() {

    // call function asynchronously

    const double LARGE_NUM1 = std::pow(10.0, 10.0);
    const double LARGE_NUM2 = std::pow(10.0, 10.0);
    const double LARGE_NUM3 = std::pow(10.0, 10.0);
    const double LARGE_NUM4 = std::pow(10.0, 9.0);
    const double LARGE_NUM5 = std::pow(10.0, 11.0);
    const double LARGE_NUM6 = std::pow(10.0, 8.0);
    const double LARGE_NUM7 = std::pow(10.0, 8.0);
    const double LARGE_NUM8 = std::pow(10.0, 8.0);

    auto fut1 = std::async(heavy_load, LARGE_NUM1);
    auto fut2 = std::async(heavy_load, LARGE_NUM2);
    auto fut3 = std::async(heavy_load, LARGE_NUM3);
    auto fut4 = std::async(heavy_load, LARGE_NUM4);
    auto fut5 = std::async(heavy_load, LARGE_NUM5);
    auto fut6 = std::async(heavy_load, LARGE_NUM6);
    auto fut7 = std::async(heavy_load, LARGE_NUM7);
    auto fut8 = std::async(heavy_load, LARGE_NUM8);

    auto res1 = fut1.get();
    std::cout << "--> res1 = " << res1 << std::endl;
    auto res2 = fut2.get();
    std::cout << "--> res2 = " << res2 << std::endl;
    auto res3 = fut3.get();
    std::cout << "--> res3 = " << res3 << std::endl;
    auto res4 = fut4.get();
    std::cout << "--> res4 = " << res4 << std::endl;
    auto res5 = fut5.get();
    std::cout << "--> res5 = " << res5 << std::endl;
    auto res6 = fut6.get();
    std::cout << "--> res6 = " << res6 << std::endl;
    auto res7 = fut7.get();
    std::cout << "--> res7 = " << res7 << std::endl;
    auto res8 = fut8.get();
    std::cout << "--> res8 = " << res8 << std::endl;

    return 0;
}
