#include <iostream>

// 0, 1, 2, 3, 4, 5, 6,  7,  8,  9, 10, 11,  12
// 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144

double fib_re(double index) {

    if (index == 0) {
        return 0;
    }

    if (index == 1) {
        return 1;
    }

    double sum = fib_re(index - 1) + fib_re(index - 2);

    return sum;
}

double fib_it(double index) {

    if (index == 0) {
        return 0;
    }

    if (index == 1) {
        return 1;
    }

    double index_m1 = 1;
    double index_m2 = 0;
    double sum_index = 0;

    for (uint64_t i = 1; i < index; i++) {

        sum_index = index_m1 + index_m2;
        index_m2 = index_m1;
        index_m1 = sum_index;
    }

    return sum_index;
}

int main() {

    // 0, 1, 2, 3, 4, 5, 6,  7,  8,  9, 10, 11,  12
    // 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144

    std::cout << " --> 1" << std::endl;

    double res1 = fib_re(40);
    std::cout << res1 << std::endl;

    std::cout << " --> 2" << std::endl;

    double res2 = fib_it(40);
    std::cout << res2 << std::endl;

    std::cout << " --> 3" << std::endl;

    std::cout << std::boolalpha;
    std::cout << (res1 == res2) << std::endl;
    return 0;
}


