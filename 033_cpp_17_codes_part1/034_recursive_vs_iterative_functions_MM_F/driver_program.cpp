#include <iostream>
#include <cmath>
#include <chrono>

const double MAX_DO = std::pow(10.0, 9.0);

double f_re(double sum) {

    if (sum == MAX_DO) {
        return sum;
    }
    sum++;
    sum = f_re(sum);

    return sum;
}

double f_it1(double sum) {

    while (true) {
        sum++;
        if (sum == MAX_DO) break;
    }

    return sum;
}

double f_it2(double sum) {

    for (uint64_t i = 0; i < MAX_DO; i++) {
        sum++;
    }

    return sum;
}

int main() {

    {
        std::cout << " --> 1" << std::endl;

        const auto t1 = std::chrono::steady_clock::now();

        const double sum = f_re(0);

        const auto t2 = std::chrono::steady_clock::now();
        const auto time_span = std::chrono::duration_cast < std::chrono::duration < double >> (t2 - t1);

        std::cout << " --> time used = " << time_span.count() << std::endl;
        std::cout << " --> sum = " << sum << std::endl;
    }

    {
        std::cout << " --> 2" << std::endl;

        const auto t1 = std::chrono::steady_clock::now();

        const double sum = f_it1(0);

        const auto t2 = std::chrono::steady_clock::now();
        const auto time_span = std::chrono::duration_cast < std::chrono::duration < double >> (t2 - t1);

        std::cout << " --> time used = " << time_span.count() << std::endl;
        std::cout << " --> sum = " << sum << std::endl;
    }

    {
        std::cout << " --> 3" << std::endl;

        const auto t1 = std::chrono::steady_clock::now();

        const double sum = f_it2(0);

        const auto t2 = std::chrono::steady_clock::now();
        const auto time_span = std::chrono::duration_cast < std::chrono::duration < double >> (t2 - t1);

        std::cout << " --> time used = " << time_span.count() << std::endl;
        std::cout << " --> sum = " << sum << std::endl;
    }

    return 0;
}