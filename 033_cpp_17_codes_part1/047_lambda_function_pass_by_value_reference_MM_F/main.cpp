#include <iostream>
#include <string>

int main() {

    {
        std::cout << "---------------->> pass local variables by value" << std::endl;

        std::string msg = "Hello";
        int counter = 10;

        // defining Lambda function and capturing local variables by value

        auto func = [msg, counter]() mutable {

            std::cout << "inside lambda :: msg = " << msg << std::endl;
            std::cout << "inside lambda :: counter = " << counter << std::endl;

            // change the counter & msg
            // change will not affect the outer variable because counter variable is
            // captured by value in Lambda function

            msg = "Temp";
            counter = 20;

            std::cout << "inside lambda :: after changing :: msg = " << msg << std::endl;
            std::cout << "inside lambda :: after changing :: counter = " << counter << std::endl;
        };

        // call the lambda function

        func();

        // values of local variables are not changed

        std::cout << "msg = " << msg << std::endl;
        std::cout << "counter = " << counter << std::endl;
    }

    {
        std::cout << "---------------->> pass local variables by reference" << std::endl;

        std::string msg = "Hello";
        int counter = 10;

        // defining Lambda function and capturing local variables by reference

        auto func = [&msg, &counter]() {

            std::cout << "inside lambda :: msg = " << msg << std::endl;
            std::cout << "inside lambda :: counter = " << counter << std::endl;

            // change the counter & msg
            // change will not affect the outer variable because counter variable is
            // captured by value in Lambda function

            msg = "Temp";
            counter = 20;

            std::cout << "inside lambda :: after changing :: msg = " << msg << std::endl;
            std::cout << "inside lambda :: after changing :: counter = " << counter << std::endl;
        };

        // call the lambda function

        func();

        // values of local variables are not changed

        std::cout << "msg = " << msg << std::endl;
        std::cout << "counter = " << counter << std::endl;
    }

    {
        std::cout << "---------------->> pass all local by value" << std::endl;

        std::string msg = "Hello";
        int counter = 10;

        // defining Lambda function and capturing all local variables by value

        auto func = [=]() mutable {

            std::cout << "inside lambda :: msg = " << msg << std::endl;
            std::cout << "inside lambda :: counter = " << counter << std::endl;

            // change the counter & msg
            // change will not affect the outer variable because counter variable is
            // captured by value in Lambda function

            msg = "Temp";
            counter = 20;

            std::cout << "inside lambda :: after changing :: msg = " << msg << std::endl;
            std::cout << "inside lambda :: after changing :: counter = " << counter << std::endl;
        };

        // call the lambda function

        func();

        // values of local variables are not changed

        std::cout << "msg = " << msg << std::endl;
        std::cout << "counter = " << counter << std::endl;
    }

    {
        std::cout << "---------------->> pass all local by reference" << std::endl;

        std::string msg = "Hello";
        int counter = 10;

        // defining Lambda function and capturing all local variables by value

        auto func = [&]() mutable {

            std::cout << "inside lambda :: msg = " << msg << std::endl;
            std::cout << "inside lambda :: counter = " << counter << std::endl;

            // change the counter & msg
            // change will not affect the outer variable because counter variable is
            // captured by value in Lambda function

            msg = "Temp";
            counter = 20;

            std::cout << "inside lambda :: after changing :: msg = " << msg << std::endl;
            std::cout << "inside lambda :: after changing :: counter = " << counter << std::endl;
        };

        // call the lambda function

        func();

        // values of local variables are not changed

        std::cout << "msg = " << msg << std::endl;
        std::cout << "counter = " << counter << std::endl;
    }

    {
        std::cout << "---------------->> pass by value and reference" << std::endl;

        std::string msg = "Hello";
        int counter = 10;

        // defining Lambda function and capturing all local variables by value

        auto func = [=, &msg]() mutable {

            std::cout << "inside lambda :: msg = " << msg << std::endl;
            std::cout << "inside lambda :: counter = " << counter << std::endl;

            // change the counter & msg
            // change will not affect the outer variable because counter variable is
            // captured by value in Lambda function

            msg = "Temp";
            counter = 20;

            std::cout << "inside lambda :: after changing :: msg = " << msg << std::endl;
            std::cout << "inside lambda :: after changing :: counter = " << counter << std::endl;
        };

        // call the lambda function

        func();

        // values of local variables are not changed

        std::cout << "msg = " << msg << std::endl;
        std::cout << "counter = " << counter << std::endl;
    }

    return 0;
}