// forward example

#include <utility>
#include <iostream>

// function with lvalue and rvalue reference overloads

void overloaded(const int &x) {
    std::cout << "[lvalue] --> x = " << x << std::endl;
}

void overloaded(int &&x) {
    std::cout << "[rvalue] --> x = " << x << std::endl;
}

// function template taking rvalue reference to deduced type

template<class T>
void fn(T &&x) {
    overloaded(x);                   // always an lvalue
    overloaded(std::forward<T>(x));  // rvalue if argument is rvalue
}

int main() {
    int a = 1;

    std::cout << "calling fn with lvalue: " << std::endl;
    fn(a);

    std::cout << "calling fn with rvalue: " << std::endl;
    fn(2);
}