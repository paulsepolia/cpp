#include <iostream>
#include <future>
#include <chrono>
#include <cmath>
#include <mutex>
#include <vector>
#include <random>
#include <algorithm>

std::mutex mtx;

double stuff(std::atomic_bool &complete, std::size_t id) {

    mtx.lock();
    std::cout << "--> starting: " << id << std::endl;
    mtx.unlock();

    // do stuff
    std::mt19937 rng;
    rng.seed(std::random_device()());
    std::uniform_int_distribution<std::mt19937::result_type> dist_loc(1000, 6000);

    mtx.lock();
    const auto time_to_sleep = dist_loc(rng);
    std::cout << "--> sleeping for (secs) " << time_to_sleep << std::endl;
    mtx.unlock();

    std::this_thread::sleep_for(std::chrono::milliseconds(time_to_sleep));

    // generate value
    const auto value = static_cast<double>(dist_loc(rng));

    // signal end
    complete = true;
    mtx.lock();
    std::cout << "--> ended: " << id << " -> " << value << std::endl;
    mtx.unlock();

    return value;
}

struct TaskLoc {

    std::future<double> fut;
    std::atomic_bool complete;

    TaskLoc() = default;

    TaskLoc(TaskLoc &&t) : fut(std::move(t.fut)), complete(t.complete.load()) {}
};

int main() {

    // list of tasks
    std::vector<TaskLoc> tasks;

    // reserve enough spaces so that nothing gets reallocated
    // as that would invalidate the references to the atomic_bools
    // needed to signal the end of a thread
    tasks.reserve(3);

    // create a new task
    tasks.emplace_back();

    // start it running
    tasks.back().fut = std::async(stuff, std::ref(tasks.back().complete), tasks.size());

    tasks.emplace_back();
    tasks.back().fut = std::async(stuff, std::ref(tasks.back().complete), tasks.size());

    tasks.emplace_back();
    tasks.back().fut = std::async(stuff, std::ref(tasks.back().complete), tasks.size());

    // Keep going as long as any of the tasks is incomplete

    mtx.lock();
    std::cout << "waiting for the results ..." << std::endl;
    mtx.unlock();

    while (std::any_of(std::begin(tasks), std::end(tasks),
                       [](auto &t) { return !t.complete.load(); })) {

        // do some parallel stuff
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
        std::cout << "." << std::flush;
    }

    // process the results

    double sum = 0;
    for (auto &t: tasks) {
        sum += t.fut.get();
    }

    std::cout << " --> sum: " << sum << '\n';
}