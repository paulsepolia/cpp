#include <iostream>
#include <vector>
#include <cmath>
#include <chrono>

// simple function pointer

double call(double (*fun)(double), double arg) {
    return fun(arg);
}

std::vector<double> call(std::vector<double> (*fun)(std::vector<double>), const std::vector<double> &arg) {
    return fun(arg);
}

double fun1(double val) {
    return val + 10;
}

std::vector<double> fun2(const std::vector<double> &val) {
    return val;
}

void fun3(const std::vector<double> &val) {
    std::cout << val[1] << std::endl;
}


int main() {

    const auto DIM = static_cast<uint64_t>(std::pow(10.0, 8.0));

    {
        double val1 = 20;
        double res1 = call(fun1, val1);
        std::cout << res1 << std::endl;
    }

    {
        double val2 = 30;
        double res2 = call(fun1, val2);
        std::cout << res2 << std::endl;
    }

    {
        // build vector

        auto start = std::chrono::system_clock::now();

        std::vector<double> v1;
        std::vector<double> v2;
        v1.resize(DIM);
        v2.resize(DIM);

        double sum = 0;

        for (auto &el1 : v1) {
            sum++;
            el1 = sum;
        }

        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_seconds = end - start;
        std::cout << " --> time build: " << elapsed_seconds.count() << "s" << std::endl;

        // pass function and argument

        start = std::chrono::system_clock::now();
        v2 = fun2(v1);
        end = std::chrono::system_clock::now();
        elapsed_seconds = end - start;
        std::cout << " --> time pass argument and call function and copy: " << elapsed_seconds.count() << "s"
                  << std::endl;

        // comparison

        start = std::chrono::system_clock::now();
        std::cout << std::boolalpha << (v1 == v2) << std::endl;
        end = std::chrono::system_clock::now();
        elapsed_seconds = end - start;
        std::cout << " --> time for comparison: " << elapsed_seconds.count() << "s" << std::endl;

        // pass function and argument

        start = std::chrono::system_clock::now();
        fun3(v1);
        end = std::chrono::system_clock::now();
        elapsed_seconds = end - start;
        std::cout << " --> time pass argument and call function and no copy: " << elapsed_seconds.count() << "s" << std::endl;

    }
}