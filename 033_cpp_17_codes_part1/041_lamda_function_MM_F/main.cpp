#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>
#include <chrono>

class fun2 {
public:

    explicit fun2(double el) : el(el) {}

    double operator()(double coeff) {
        return el * coeff;
    }

private:
    double el;
};

int main() {

    const auto dim = static_cast<size_t>(std::pow(10.0, 2.0));
    const auto T1 = static_cast<size_t>(std::pow(10.0, 2.0));

    {
        std::vector<double> vec(dim);
        double i = 0;
        for (auto &el: vec) {
            el = i++;
        }

        const double coeff = 2;
        const auto start = std::chrono::steady_clock::now();

        for (size_t k1 = 0; k1 < T1; k1++) {

            std::transform(vec.begin(), vec.end(), vec.begin(),
                           [&coeff](double &el) -> double {
                               return coeff * el;
                           });
        }

        const auto end = std::chrono::steady_clock::now();
        const std::chrono::duration<double> elapsed_seconds = end - start;

        std::cout << " --> time --> 1 --> " << elapsed_seconds.count() << std::endl;
        std::cout << " --> vec[0] = " << vec[0] << std::endl;
        std::cout << " --> vec[1] = " << vec[1] << std::endl;
    }

    {
        std::vector<double> vec(dim);
        double i = 0;
        for (auto &el: vec) {
            el = i++;
        }

        const double coeff = 2;
        const auto start = std::chrono::steady_clock::now();

        for (size_t k1 = 0; k1 < T1; k1++) {

            std::transform(vec.begin(), vec.end(), vec.begin(),
                           [&coeff](double el) -> double {
                               return coeff * el;
                           });
        }

        const auto end = std::chrono::steady_clock::now();

        const std::chrono::duration<double> elapsed_seconds = end - start;

        std::cout << " --> time --> 2 -->  " << elapsed_seconds.count() << std::endl;
        std::cout << " --> vec[0] = " << vec[0] << std::endl;
        std::cout << " --> vec[1] = " << vec[1] << std::endl;
    }

    {
        std::vector<double> vec(dim);
        double i = 0;
        for (auto &el: vec) {
            el = i++;
        }

        const double coeff = 2;
        const auto start = std::chrono::steady_clock::now();

        for (size_t k1 = 0; k1 < T1; k1++) {

            std::transform(vec.begin(), vec.end(), vec.begin(),
                           [coeff](double el) -> double {
                               return coeff * el;
                           });
        }

        const auto end = std::chrono::steady_clock::now();
        const std::chrono::duration<double> elapsed_seconds = end - start;

        std::cout << " --> time --> 3 -->  " << elapsed_seconds.count() << std::endl;
        std::cout << " --> vec[0] = " << vec[0] << std::endl;
        std::cout << " --> vec[1] = " << vec[1] << std::endl;
    }

    {
        std::vector<double> vec(dim);
        double i = 0;
        for (auto &el: vec) {
            el = i++;
        }

        const double coeff = 2;
        const auto start = std::chrono::steady_clock::now();

        for (size_t k1 = 0; k1 < T1; k1++) {

            std::transform(vec.begin(), vec.end(), vec.begin(), fun2(coeff));
        }

        const auto end = std::chrono::steady_clock::now();
        const std::chrono::duration<double> elapsed_seconds = end - start;

        std::cout << " --> time --> 4 -->  " << elapsed_seconds.count() << std::endl;
        std::cout << " --> vec[0] = " << vec[0] << std::endl;
        std::cout << " --> vec[1] = " << vec[1] << std::endl;
    }

    {
        auto algorithm = [&](double x, double m, double b) -> double {
            return m * x + b;
        };

        const auto a1 = algorithm(1, 2, 3);
        const auto a2 = algorithm(4, 5, 6);

        std::cout << " --> a1 = " << a1 << std::endl;
        std::cout << " --> a2 = " << a2 << std::endl;
    }

    {
        auto algorithm = [&](double x, double m, double b) -> double {
            return m * x + b;
        };

        const auto a1 = algorithm(1, 2, 3);
        const auto a2 = algorithm(4, 5, 6);

        std::cout << " --> a1 = " << a1 << std::endl;
        std::cout << " --> a2 = " << a2 << std::endl;
    }

    {
        std::cout << "------------------------>> k1" << std::endl;

        std::vector<double> v1(dim);
        std::vector<double> v2(dim);

        for (size_t i = 0; i < dim; i++) {
            v1[i] = static_cast<double>(i);
            v2[i] = static_cast<double>(i);
        }

        auto add_vectors = [&](const std::vector<double> &v1,
                               const std::vector<double> &v2) -> std::vector<double> {

            std::vector<double> v3(dim);

            for (size_t i = 0; i < dim; i++) {
                v3[i] = v1[i] + v2[i];
            }

            return v3;
        };

        const auto res1 = add_vectors(v1, v2);
        const auto res2 = add_vectors(res1, res1);

        std::cout << " --> res1[0] = " << res1[0] << std::endl;
        std::cout << " --> res1[1] = " << res1[1] << std::endl;
        std::cout << " --> res2[0] = " << res2[0] << std::endl;
        std::cout << " --> res2[1] = " << res2[1] << std::endl;
    }

    {
        std::cout << "------------------------>> k2" << std::endl;

        std::vector<double> v1(dim);
        std::vector<double> v2(dim);

        for (size_t i = 0; i < dim; i++) {
            v1[i] = static_cast<double>(i);
            v2[i] = static_cast<double>(i);
        }

        auto add_vectors = [=](const std::vector<double> &v1,
                               const std::vector<double> &v2) -> std::vector<double> {

            std::vector<double> v3(dim);

            for (size_t i = 0; i < dim; i++) {
                v3[i] = v1[i] + v2[i];
            }

            return v3;
        };

        const auto res1 = add_vectors(v1, v2);
        const auto res2 = add_vectors(res1, res1);

        std::cout << " --> res1[0] = " << res1[0] << std::endl;
        std::cout << " --> res1[1] = " << res1[1] << std::endl;
        std::cout << " --> res2[0] = " << res2[0] << std::endl;
        std::cout << " --> res2[1] = " << res2[1] << std::endl;
    }

    return 0;
}