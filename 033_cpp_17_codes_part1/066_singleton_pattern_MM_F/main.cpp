#include <iostream>

class Singleton {

public:

    static Singleton *getInstance();

    Singleton(const Singleton &) = delete;

    void operator=(const Singleton &) = delete;

    Singleton(Singleton &&) = delete;

    void operator=(Singleton &&) = delete;

private:

    Singleton() = default;

    static Singleton *instance;
};

Singleton *Singleton::instance = nullptr;

Singleton *Singleton::getInstance() {

    if (instance == nullptr) {
        instance = new Singleton();
    }
    return instance;
}

int main() {

    {
        Singleton *s = Singleton::getInstance();
        Singleton *r = Singleton::getInstance();

        std::cout << s << std::endl;
        std::cout << r << std::endl;
    }

    {
        Singleton *s = Singleton::getInstance();
        Singleton *r = Singleton::getInstance();

        std::cout << s << std::endl;
        std::cout << r << std::endl;
    }
}