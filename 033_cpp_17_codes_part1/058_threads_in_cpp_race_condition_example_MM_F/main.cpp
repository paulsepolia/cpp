#include <iostream>
#include <thread>
#include <vector>
#include <mutex>
#include <cmath>

class Wallet {

public:
    Wallet() : _m_money(0) {}

    size_t getMoney() const { return _m_money; }

    void addMoney(const size_t &money) {
        _mtx.lock();
        for (size_t i = 0; i < money; i++) {
            _m_money++;
        }
        _mtx.unlock();
    }

private:
    size_t _m_money;
    std::mutex _mtx;
};

size_t testMultithreadedWallet(const size_t &nt,
                               const size_t &money_to_add) {

    Wallet walletObject;
    std::vector<std::thread> threads;
    threads.reserve(nt);

    for (size_t i = 0; i < nt; ++i) {
        threads.emplace_back(std::thread(&Wallet::addMoney, &walletObject, money_to_add));
    }

    for (auto &el: threads) {
        el.join();
    }

    return walletObject.getMoney();
}

int main() {

    const auto K_MAX = static_cast<size_t>(std::pow(10.0, 5));
    const size_t MONEY_TO_ADD = 1000;
    const size_t NT = 20;

    size_t val = 0;

    for (size_t k = 0; k < K_MAX; k++) {
        if ((val = testMultithreadedWallet(NT, MONEY_TO_ADD)) != NT * MONEY_TO_ADD) {
            std::cout << "Error at count = " << k << " Money in Wallet = " << val << std::endl;
        }
    }

    return 0;
}