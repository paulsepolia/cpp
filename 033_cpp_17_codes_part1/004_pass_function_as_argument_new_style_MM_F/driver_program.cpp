#include <iostream>
#include <vector>
#include <cmath>
#include <chrono>
#include <functional>

double func1(double arg, std::function<double(double)> fun) {

    return fun(arg);
}

double func2(double arg, const std::function<double(double)> & fun) {

    return fun(arg);
}

std::vector<double> func3(const std::vector<double> & arg,
                          const std::function<std::vector<double>(std::vector<double>)> & fun) {

    return fun(arg);
}

double fun_real1(double val) {
    return val;
}

std::vector<double> fun_real2(std::vector<double> val) {
    return val;
}


int main() {

    std::cout << func1(20, fun_real1) << std::endl;
    std::cout << func2(30, fun_real1) << std::endl;

    std::vector<double> v1{1,2,3,4};

    const auto vec_ret = func3(v1, fun_real2);

    for(const auto & el: vec_ret) {
        std::cout << "--> " << el << std::endl;
    }

}