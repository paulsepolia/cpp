#include <iostream>
#include <vector>
#include <cmath>
#include <chrono>

int main() {

    const auto DIM = static_cast<uint64_t>(std::pow(10.0, 7.0));
    std::vector<double> v1;
    std::vector<std::vector<double>> v2;

    {
        v2.clear();

        auto start = std::chrono::system_clock::now();
        std::cout << "--------------------------->> copy" << std::endl;

        for (uint64_t i = 0; i != DIM; i++) {
            auto a1 = static_cast<double>(i);
            v1.push_back(a1);
        }

        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_seconds = end - start;
        std::cout << "elapsed time: " << elapsed_seconds.count() << "s" << std::endl;
        std::cout << "v1[0]: " << v1[0] << std::endl;
        std::cout << "v1[DIM-1]: " << v1[DIM - 1] << std::endl;
    }

    {
        v2.clear();

        auto start = std::chrono::system_clock::now();
        std::cout << "--------------------------->> move" << std::endl;

        for (uint64_t i = 0; i != DIM; i++) {
            auto a1 = static_cast<double>(i);
            v1.push_back(std::move(a1)); // no effect on trivial data type
        }

        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_seconds = end - start;
        std::cout << "elapsed time: " << elapsed_seconds.count() << "s" << std::endl;
        std::cout << "v1[0]: " << v1[0] << std::endl;
        std::cout << "v1[DIM-1]: " << v1[DIM - 1] << std::endl;
    }

    {
        v2.clear();

        auto start = std::chrono::system_clock::now();
        std::cout << "--------------------------->> copy small vector" << std::endl;
        for (uint64_t i = 0; i != DIM; i++) {
            auto a1 = std::vector<double>({1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
            v2.push_back(a1);
        }

        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_seconds = end - start;
        std::cout << "elapsed time: " << elapsed_seconds.count() << "s" << std::endl;
        std::cout << "v1[0]: " << v1[0] << std::endl;
        std::cout << "v1[DIM-1]: " << v1[DIM - 1] << std::endl;
    }

    {
        v2.clear();

        auto start = std::chrono::system_clock::now();
        std::cout << "--------------------------->> move small vector" << std::endl;

        for (uint64_t i = 0; i != DIM; i++) {
            auto a1 = std::vector<double>({1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
            v2.push_back(std::move(a1));
        }

        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_seconds = end - start;
        std::cout << "elapsed time: " << elapsed_seconds.count() << "s" << std::endl;
        std::cout << "v1[0]: " << v1[0] << std::endl;
        std::cout << "v1[DIM-1]: " << v1[DIM - 1] << std::endl;
    }


    {
        v2.clear();

        auto start = std::chrono::system_clock::now();

        std::cout << "--------------------------->> copy large vector" << std::endl;
        for (uint64_t i = 0; i != DIM/100; i++) {
            std::vector<double> a1;
            a1.resize(1000);
            v2.push_back(a1);
        }

        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_seconds = end - start;
        std::cout << "elapsed time: " << elapsed_seconds.count() << "s" << std::endl;
        std::cout << "v1[0]: " << v1[0] << std::endl;
        std::cout << "v1[DIM-1]: " << v1[DIM - 1] << std::endl;
    }

    {
        v2.clear();

        auto start = std::chrono::system_clock::now();
        std::cout << "--------------------------->> move large vector" << std::endl;

        for (uint64_t i = 0; i != DIM/100; i++) {
            std::vector<double> a1;
            a1.resize(1000);
            v2.push_back(std::move(a1));
        }

        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_seconds = end - start;
        std::cout << "elapsed time: " << elapsed_seconds.count() << "s" << std::endl;
        std::cout << "v2[0]: " << v2[0][0] << std::endl;
        std::cout << "v2[DIM-1]: " << v2[DIM/100 - 1][0] << std::endl;
    }
}