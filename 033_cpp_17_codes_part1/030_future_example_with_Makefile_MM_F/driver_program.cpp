// future example

#include <iostream>
#include <future>
#include <chrono>
#include <cmath>
#include <mutex>
#include <vector>
#include <algorithm>

// class declaration

class GetAsyncRes {

public:

    GetAsyncRes() = default;

    ~GetAsyncRes() = default;

    double heavy_load(const double &);

    std::atomic<bool> get_status();

private:

    static std::atomic<bool> _is_done;
    uint32_t _index_local;
    static uint32_t _index_global;
public:
    static std::mutex _mtx;
};

// static members

std::atomic<bool> GetAsyncRes::_is_done = false;
uint32_t GetAsyncRes::_index_global = 0;
std::mutex GetAsyncRes::_mtx{};

// class definition

double GetAsyncRes::heavy_load(const double &large_value) {

    const auto t1 = std::chrono::high_resolution_clock::now();

    _mtx.lock();
    _index_global++;
    _index_local = _index_global;
    std::cout << "--> heavy load id = " << _index_local << std::endl;
    _mtx.unlock();

    double sum = 0;
    while (true) {
        sum++;
        if (sum >= large_value) break;
    }

    const auto t2 = std::chrono::high_resolution_clock::now();
    const auto time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);

    _mtx.lock();
    std::cout << "--> exit heavy load id = " << _index_local << std::endl;
    std::cout << "--> took (secs) = " << time_span.count() << std::endl;
    _is_done = true;
    _mtx.unlock();

    return sum;
}

std::atomic<bool> GetAsyncRes::get_status() {
    return _is_done.load();
}

// the driver program

int main() {

    const uint32_t NUM_JOBS(50);

    // create vector with loads

    std::vector<double> vec_loads;

    for (uint32_t i = 0; i != NUM_JOBS; i++) {
        vec_loads.push_back(i * std::pow(10.0, 9.0));
    }

    // create vector with objects

    std::vector<GetAsyncRes> vec_objs;
    vec_objs.reserve(NUM_JOBS);

    // create vector with futures

    std::vector<std::future<double>> vec_futs;

    // launch asynchronous operation

    for (uint32_t i = 0; i != NUM_JOBS; i++) {

        vec_objs.emplace_back(GetAsyncRes());
        auto fut = std::async(&GetAsyncRes::heavy_load, &vec_objs.back(), vec_loads[i]);
        vec_futs.push_back(std::move(fut));
    }

    std::vector<uint64_t> k_index{};

    while (true) {

        for (uint64_t i = 0; i != vec_objs.size(); i++) {

            auto it = std::find(k_index.begin(), k_index.end(), i);

            if (it == k_index.end()) {
                
                if (vec_objs[i].get_status()) {

                    k_index.push_back(i);
                    const double val_tmp = vec_futs[i].get();
                    GetAsyncRes::_mtx.lock();
                    std::cout << " value is --> " << val_tmp << std::endl << std::flush;
                    GetAsyncRes::_mtx.unlock();
                }
            }
        }

        if (k_index.size() == NUM_JOBS) break;
    }

    std::cout << "-------------------->> end " << std::endl;

    return 0;
}
