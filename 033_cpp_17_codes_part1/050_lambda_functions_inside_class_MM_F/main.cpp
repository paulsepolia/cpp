#include <string>
#include <vector>
#include <algorithm>
#include <iostream>

class OddCounter {

public:

    size_t get_count() const {
        return m_counter;
    }

    void update(const std::vector<int> &vec) {

        // Traverse the vector and increment m_counter if element is odd
        // this is captured by value inside lambda

        std::for_each(vec.begin(), vec.end(),
                      [this](int element) {
                          if (element % 2 == 1) {
                              m_counter++;
                          } // Accessing member variable from outer scope
                      });
    }

private:
    // tracks the count of odd numbers encountered

    size_t m_counter = 0;
};


int main() {

    std::vector<int> vec = {12, 3, 2, 1, 8, 9, 0, 2, 3, 9, 7};

    OddCounter counter_obj;

    // Passing the vector to OddCounter object

    counter_obj.update(vec);

    const auto count = counter_obj.get_count();

    std::cout << "Counter = " << count << std::endl;

    return 0;
}