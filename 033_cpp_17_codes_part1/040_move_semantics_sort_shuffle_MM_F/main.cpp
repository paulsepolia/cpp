#include <iostream>
#include <vector>
#include <random>
#include <algorithm>
#include <chrono>

class A {

public:

    A() : _vec(nullptr), _dim(0) {}

    explicit A(uint64_t dim) : _vec(nullptr), _dim(0) {

        _dim = dim;
        _vec = new double[_dim];

        for (size_t i = 0; i < _dim; i++) {
            _vec[i] = static_cast<double>(i);
        }

        std::random_device rd;
        std::mt19937 g(rd());
        std::shuffle(_vec, _vec + _dim, g);
    }

    A(const A &obj) : _vec(nullptr), _dim(0) {

        //std::cout << "COPY CONSTRUCTOR" << std::endl;
        _dim = obj._dim;
        _vec = new double[_dim];
        std::copy(obj._vec, obj._vec + _dim, _vec);
    }

    A &operator=(const A &obj) {

        //std::cout << "COPY ASSIGNMENT" << std::endl;

        if (this != &obj) {
            delete[] _vec;
            _dim = obj._dim;
            _vec = new double[_dim];
            std::copy(obj._vec, obj._vec + _dim, _vec);
        }

        return *this;
    }

    A &operator=(A &&obj) noexcept {

        //std::cout << "MOVE ASSIGNMENT" << std::endl;

        if (this != &obj) {
            delete[] _vec;
            _vec = obj._vec;
            _dim = obj._dim;
            obj._vec = nullptr;
            obj._dim = 0;
        }

        return *this;
    }

    A(A &&obj) noexcept : _vec(nullptr), _dim(0) {

        //std::cout << "MOVE CONSTRUCTOR" << std::endl;
        *this = std::move(obj);
    }

    double elem(uint64_t idx) const {
        return _vec[idx];
    }

    void delete_obj() {
        delete[] _vec;
        _vec = nullptr;
        _dim = 0;
    }

    virtual ~A() {
        delete[] _vec;
        _vec = nullptr;
    }

    bool operator==(const A &obj) const {

        if (this != &obj) {

            if (_dim != obj._dim) {
                return false;
            } else {

                for (uint64_t i = 0; i < _dim; i++) {
                    if (_vec[i] != obj._vec[i]) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

private:

    double *_vec;
    uint64_t _dim;
};


int main() {

    const auto dim_elem = 2 * static_cast<uint64_t>(std::pow(10.0, 4.0));
    const auto dim_tot = static_cast<uint64_t>(std::pow(10.0, 4.0));
    const auto DO_MAX = 10;

    for (uint64_t k = 0; k < DO_MAX; k++) {

        std::cout << " --> run ---------------> " << k << std::endl;

        {
            std::cout << " --> 1 --> example" << std::endl;

            std::vector<A> a1;

            auto start = std::chrono::steady_clock::now();

            for (size_t i = 0; i < dim_tot; i++) {
                A el(dim_elem);
                a1.push_back(el);
            }

            auto end = std::chrono::steady_clock::now();
            std::chrono::duration<double> elapsed_seconds = end - start;
            std::cout << " --> time 1 --> build      --> "
                      << elapsed_seconds.count() << std::endl;

            start = std::chrono::steady_clock::now();

            std::sort(a1.begin(), a1.end(),
                      [](const A &a, const A &b) -> bool {
                          return a.elem(0) > b.elem(0);
                      });

            end = std::chrono::steady_clock::now();
            elapsed_seconds = end - start;
            std::cout << " --> time 2 --> sort       --> "
                      << elapsed_seconds.count() << std::endl;
        }

        {
            std::cout << " --> 2 --> example" << std::endl;

            std::vector<A> a1;

            auto start = std::chrono::steady_clock::now();

            for (size_t i = 0; i < dim_tot; i++) {
                A el(dim_elem);
                a1.push_back(el);
            }

            auto end = std::chrono::steady_clock::now();
            std::chrono::duration<double> elapsed_seconds = end - start;
            std::cout << " --> time 3 --> build      --> "
                      << elapsed_seconds.count() << std::endl;

            start = std::chrono::steady_clock::now();
            std::random_device rd;
            std::mt19937 g(rd());

            std::shuffle(a1.begin(), a1.end(), g);

            end = std::chrono::steady_clock::now();
            elapsed_seconds = end - start;
            std::cout << " --> time 4 --> shuffle    --> "
                      << elapsed_seconds.count() << std::endl;
        }

        {
            std::cout << " --> 3 --> example" << std::endl;
            A a1(dim_elem);
            A a2(dim_elem);

            a1 = a2;
            a1 = std::move(a2);
        }

        {
            std::cout << " --> 4 --> example" << std::endl;
            A a1(dim_elem);
            A a2(a1);
        }

        {
            std::cout << " --> 5 --> example" << std::endl;
            A a1(dim_elem);
            A a2(std::move(a1));
        }
    }

    return 0;
}
