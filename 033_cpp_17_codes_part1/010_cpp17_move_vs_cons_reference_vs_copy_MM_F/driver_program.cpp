#include <iostream>
#include <vector>
#include <cmath>
#include <chrono>

int main() {

    const auto DIM = static_cast<uint64_t>(std::pow(10.0, 9.0));
    std::vector<double> v1;
    v1.resize(DIM);
    v1[0] = 123.456;
    v1[DIM - 1] = 789.123;

    {
        auto start = std::chrono::system_clock::now();
        std::cout << "--------------------------->> copy" << std::endl;
        std::cout << "v2 = v1" << std::endl;
        std::vector<double> v2;
        v2 = v1;
        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_seconds = end - start;
        std::cout << "elapsed time: " << elapsed_seconds.count() << "s" << std::endl;
        std::cout << "v2[0]: " << v2[0] << std::endl;
        std::cout << "v2[DIM-1]: " << v2[DIM - 1] << std::endl;
    }

    {
        auto start = std::chrono::system_clock::now();
        std::cout << "--------------------------->> const reference" << std::endl;
        std::cout << "& v2 = v1" << std::endl;
        const std::vector<double> & v2 = v1;
        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_seconds = end - start;
        std::cout << "elapsed time: " << elapsed_seconds.count() << "s" << std::endl;
        std::cout << "v2[0]: " << v2[0] << std::endl;
        std::cout << "v2[DIM-1]: " << v2[DIM - 1] << std::endl;
    }

    {
        auto start = std::chrono::system_clock::now();
        std::cout << "--------------------------->> move" << std::endl;
        std::cout << "v2 = std::move(v1)" << std::endl;
        std::vector<double> v2;
        v2 = std::move(v1);
        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_seconds = end - start;
        std::cout << "elapsed time: " << elapsed_seconds.count() << "s" << std::endl;
        std::cout << "v2[0]: " << v2[0] << std::endl;
        std::cout << "v2[DIM-1]: " << v2[DIM - 1] << std::endl;
    }
}