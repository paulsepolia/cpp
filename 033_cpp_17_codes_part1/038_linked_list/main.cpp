#include <iostream>

struct Node {
    double data;
    Node *next;
};

class List {

private:

    Node *head;
    Node *tail;

public:

    List() {
        head = nullptr;
        tail = nullptr;
    }

    void create_node(double value);

    void display();

    void insert_start(double value);

    void insert_position(uint64_t pos, double value);

    void delete_first();

    void delete_last();

    void delete_position(uint64_t pos);
};

void List::create_node(double value) {

    Node *temp = new Node;
    temp->data = value;
    temp->next = nullptr;

    if (head == nullptr) {
        head = temp;
        tail = temp;
        temp = nullptr;
    } else {
        tail->next = temp;
        tail = temp;
    }
}

void List::display() {

    Node *temp;
    temp = head;
    while (temp != nullptr) {
        std::cout << temp->data << "\t";
        temp = temp->next;
    }
}

void List::insert_start(double value) {

    Node *temp = new Node;
    temp->data = value;
    temp->next = head;
    head = temp;
}

void List::insert_position(uint64_t pos, double value) {

    Node *pre = new Node;
    Node *cur = new Node;
    Node *temp = new Node;
    cur = head;

    for (uint64_t i = 1; i < pos; i++) {
        pre = cur;
        cur = cur->next;
    }

    temp->data = value;
    pre->next = temp;
    temp->next = cur;
}

void List::delete_first() {

    Node *temp = new Node;
    temp = head;
    head = head->next;
    delete temp;
}

void List::delete_last() {
    Node *current = new Node;
    Node *previous = new Node;
    current = head;
    while (current->next != nullptr) {
        previous = current;
        current = current->next;
    }
    tail = previous;
    previous->next = nullptr;
    delete current;
}

void List::delete_position(uint64_t pos) {

    Node *current = new Node;
    Node *previous = new Node;

    current = head;

    for (int i = 1; i < pos; i++) {
        previous = current;
        current = current->next;
    }

    previous->next = current->next;
}


int main() {


}

