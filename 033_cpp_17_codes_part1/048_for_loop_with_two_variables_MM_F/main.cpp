#include <iostream>

int main() {

    // for loop with two variable i & j
    // i will start with 0 and keep on incrementing till 10
    // j will start with 10 and keep on decrementing till 0

    for (int i1 = 0, i2 = 10;
         i1 < 100 && i2 > 0;
         i1++, i2--) {

        std::cout << "i1 = " << i1 << " :: i2 = " << i2 << std::endl;
    }

    for (int i1 = 0, i2 = 10, i3 = 0;
         i1 < 100 && i2 > 0 && i3 < 10;
         i1++, i2--, i3++) {
        
        std::cout << "i1 = " << i1 << " :: i2 = " << i2 << " i3 = " << i3 << std::endl;
    }

    return 0;
}