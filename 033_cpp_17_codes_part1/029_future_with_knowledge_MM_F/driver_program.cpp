#include <iostream>
#include <future>
#include <thread>

uint64_t fib(uint64_t n) {
    if (n < 3) return 1;
    else return fib(n - 1) + fib(n - 2);
}

int main() {

    std::cout << "starting async tasks..." << std::endl;

    std::future<uint64_t> f1 = std::async(std::launch::async, []() {
        return fib(50);
    });

    std::future<uint64_t> f2 = std::async(std::launch::async, []() {
        return fib(48);
    });

    std::cout << "waiting for any of the async tasks to be ready ..." << std::endl;

    std::future_status stat1;
    std::future_status stat2;

    do {

        stat1 = f1.wait_for(std::chrono::milliseconds(500));
        stat2 = f2.wait_for(std::chrono::milliseconds(0));

        if (stat1 == std::future_status::deferred ||
            stat2 == std::future_status::deferred) {

            std::cout << "deferred." << std::endl;

        } else if (stat1 == std::future_status::timeout ||
                   stat2 == std::future_status::timeout) {

            std::cout << "timeout..." << std::endl;

        } else if (stat1 == std::future_status::ready && stat2 == std::future_status::ready) {

            std::cout << "ready!" << std::endl;
        }
    } while (stat1 != std::future_status::ready || stat2 != std::future_status::ready);


    std::cout << " --> ready to get the results " << std::endl;

    std::this_thread::sleep_for(std::chrono::milliseconds(1000));

    std::cout << "f1: " << f1.get() << std::endl;

}
