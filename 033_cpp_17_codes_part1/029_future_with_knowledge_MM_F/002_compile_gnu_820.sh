#!/bin/bash

      /opt/gcc/8.2.0/bin/g++-8.2.0  -O3  \
      -Wall              \
	  -pthread           \
      -fopenmp           \
      -std=gnu++17       \
      driver_program.cpp \
      -o x_gnu
