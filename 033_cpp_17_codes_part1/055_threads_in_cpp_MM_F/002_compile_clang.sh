#!/bin/bash

  clang++   -O3         \
            -Wall       \
            -std=c++1z  \
            -pthread    \
            main.cpp    \
            -o x_clang
