#!/bin/bash

  g++-9.2.0   -O3          \
              -Wall        \
              -std=c++17   \
              -pthread     \
              -pedantic    \
              main.cpp     \
              -o x_gnu

