// future example

// https://stackoverflow.com/questions/45852218/signaling-main-thread-when-stdfuture-is-ready-to-be-retreived

#include <iostream>
#include <future>
#include <chrono>
#include <cmath>
#include <mutex>
#include <vector>

const uint64_t NUM_ASYNC_JOBS = 4;
static std::mutex mtx;

double heavy_load(const double &large_value) {

    static uint64_t index_gen = 0;

    const auto t1 = std::chrono::high_resolution_clock::now();

    mtx.lock();
    index_gen++;
    uint64_t index_loc = index_gen;
    std::cout << "--> heavy load id = " << index_loc << std::endl;
    mtx.unlock();

    double sum = 0;
    while (true) {
        sum++;
        if (sum >= large_value) break;
    }

    const auto t2 = std::chrono::high_resolution_clock::now();
    const auto time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);

    mtx.lock();
    std::cout << "--> exit heavy load id = " << index_loc << std::endl;
    std::cout << "--> took (secs) = " << time_span.count() << std::endl;
    mtx.unlock();

    return sum;
}

void get_res_async(std::future<double> el) {

    auto res = el.get();
    std::cout << "--> res = " << res << std::endl;
}

int main() {

    try {

        // create vector with large doubles

        std::vector<double> loads_vec;
        for (uint64_t i = 0; i != NUM_ASYNC_JOBS; i++) {
            loads_vec.push_back(std::pow(10.0, 11.0 - i));
        }

        // create vector to hold the futures and launch the futures

        std::vector<std::future<double>> fut_vec;

        for (const auto &el: loads_vec) {
            auto fut = std::async(heavy_load, el);
            fut_vec.push_back(std::move(fut));
        }

        // get the values of the futures as soon as they are ready
        // by spawning threads

        for (auto &el: fut_vec) {
            // SEGMENTATION FAULT HERE
            // BECAUSE OF THE move ...
            // How can i get the value as soon as it is ready???
            std::thread th(get_res_async, std::move(el));
        }
    }
    catch (const std::exception &e) {
        std::cout << "ERROR: " << e.what() << std::endl;
    }

    return 0;
}
