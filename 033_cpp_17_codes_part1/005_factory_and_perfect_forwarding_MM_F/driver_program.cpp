#include <iostream>

// some structs as an example

struct W {
    W(uint32_t &a, uint32_t &b) {
        std::cout << " --> W --> a = " << a << std::endl;
        std::cout << " --> W --> b = " << b << std::endl;
        c = a;
    }

    uint32_t c;
};

struct X {
    X(const uint32_t &a, uint32_t &b) {
        std::cout << " --> X --> a = " << a << std::endl;
        std::cout << " --> X --> b = " << b << std::endl;
        c = a;
    }

    uint32_t c;
};

struct Y {
    Y(uint32_t &a, const uint32_t &b) {
        std::cout << " --> Y --> a = " << a << std::endl;
        std::cout << " --> Y --> b = " << b << std::endl;
        c = a;
    }

    uint32_t c;
};

struct Z {
    Z(const uint32_t &a, const uint32_t &b) {
        std::cout << " --> Z --> a = " << a << std::endl;
        std::cout << " --> Z --> b = " << b << std::endl;
        c = a;
    }

    uint32_t c;
};

// generic factory to create objects

template<typename T, typename A1, typename A2>
T *factory(A1 &a1, A2 &a2) {
    return new T(a1, a2);
}


template<typename T, typename A1, typename A2>
T *factory(A1 &&a1, A2 &&a2) {
    return new T(std::forward<A1>(a1), std::forward<A2>(a2));
}


int main() {

    std::cout << " ----------------------------------->> 1 " << std::endl;

    {
        uint32_t a = 4;
        uint32_t b = 5;
        W *pw = factory<W>(a, b);
        std::cout << " pw->c = " << pw->c << std::endl;
    }

    std::cout << " ----------------------------------->> 2 " << std::endl;

    {
        Z *pz = factory<Z>(6, 7);
        std::cout << " pz->c = " << pz->c << std::endl;
    }

    std::cout << " ----------------------------------->> 3 " << std::endl;

    {
        uint32_t b = 9;
        X *px = factory<X>(8, b);
        std::cout << " px->c = " << px->c << std::endl;
    }

    std::cout << " ----------------------------------->> 4 " << std::endl;

    {
        uint32_t a = 10;
        Y *py = factory<Y>(a, 11);
        std::cout << " py->c = " << py->c << std::endl;
    }

}