#!/bin/bash

  clang++   -O3         \
            -Wall       \
            -std=c++1z  \
            main.cpp    \
            -o x_clang
