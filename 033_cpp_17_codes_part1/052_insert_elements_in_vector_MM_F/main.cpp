#include <iterator>
#include <iostream>
#include <vector>
#include <algorithm>
#include <string>

template<typename T>
void print(T &v, std::string delimeter = " , ") {
    for (auto &el : v) {
        std::cout << el << delimeter;
    }
    std::cout << std::endl;
}

int main() {

    {
        std::vector<int> v{1, 4, 5, 22, 33, 2, 11, 89, 49};

        // inserting an element at specific position in vector

        // create Iterator pointing to 4th Position

        const auto it_pos = v.begin() + 4;

        // insert element with value 9 at 4th Position in vector

        v.insert(it_pos, 9);

        print(v);
    }

    {
        // inserting multiple elements / range at specific position in vector

        std::vector<std::string> vec1{"at", "hello", "hi", "there", "where", "now", "is", "that"};
        std::vector<std::string> vec2{"one", "two", "two"};

        // insert all the elements in vec2 at 3rd position in vec1

        vec1.insert(vec1.begin() + 3, vec2.begin(), vec2.end());

        print(vec1);
    }

    {
        // inserting all elements in initialization_list in another vector
        // at specific position.

        std::vector<int> v{1, 4, 5, 22, 33, 2, 11, 89, 49};

        // insert all elements from initialization_list to vector at 3rd position

        v.insert(v.begin() + 3, {34, 55, 66, 77});

        print(v);
    }
    return 0;
}