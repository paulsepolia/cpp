#include <iostream>
#include <vector>
#include <cmath>
#include <chrono>

const uint64_t DIM = static_cast<uint64_t>(std::pow(10.0, 9.0));

std::vector<double> ret_vector() {

    auto start = std::chrono::system_clock::now();

    std::vector<double> v1;
    v1.resize(DIM);
    double a1 = 0;

    for (auto &el: v1) {
        a1++;
        el = a1;
    }

    auto end = std::chrono::system_clock::now();

    std::chrono::duration<double> elapsed_seconds = end - start;

    std::cout << "elapsed time build: " << elapsed_seconds.count() << "s" << std::endl;

    return v1;
}


int main() {

    {
        std::cout << "----------------------------------------------->> 1" << std::endl;

        auto start = std::chrono::system_clock::now();

        std::vector<double> vec_loc = ret_vector();

        auto end = std::chrono::system_clock::now();

        std::chrono::duration<double> elapsed_seconds = end - start;

        std::cout << "elapsed time total: " << elapsed_seconds.count() << "s" << std::endl;

        std::cout << vec_loc[0] << std::endl;
        std::cout << vec_loc[1] << std::endl;
        std::cout << vec_loc[2] << std::endl;
    }

    {
        std::cout << "----------------------------------------------->> 2" << std::endl;
        std::cout << "std::vector<double> && vec_loc1 = std::move(vec_loc);" << std::endl;

        std::vector<double> vec_loc = ret_vector();

        auto start = std::chrono::system_clock::now();

        std::vector<double> &&vec_loc1 = std::move(vec_loc);

        auto end = std::chrono::system_clock::now();

        std::chrono::duration<double> elapsed_seconds = end - start;

        std::cout << "elapsed time return: " << elapsed_seconds.count() << "s" << std::endl;

        std::cout << vec_loc1[0] << std::endl;
        std::cout << vec_loc1[1] << std::endl;
        std::cout << vec_loc1[2] << std::endl;
    }

    {
        std::cout << "----------------------------------------------->> 3" << std::endl;
        std::cout << "std::vector<double> vec_loc1 = vec_loc;" << std::endl;

        std::vector<double> vec_loc = ret_vector();

        auto start = std::chrono::system_clock::now();

        std::vector<double> vec_loc1 = vec_loc;

        auto end = std::chrono::system_clock::now();

        std::chrono::duration<double> elapsed_seconds = end - start;

        std::cout << "elapsed time return: " << elapsed_seconds.count() << "s" << std::endl;

        std::cout << vec_loc1[0] << std::endl;
        std::cout << vec_loc1[1] << std::endl;
        std::cout << vec_loc1[2] << std::endl;
    }

    {
        std::cout << "----------------------------------------------->> 4" << std::endl;
        std::cout << "const std::vector<double> & vec_loc1 = vec_loc;" << std::endl;

        std::vector<double> vec_loc = ret_vector();

        auto start = std::chrono::system_clock::now();

        const std::vector<double> &vec_loc1 = vec_loc;

        auto end = std::chrono::system_clock::now();

        std::chrono::duration<double> elapsed_seconds = end - start;

        std::cout << "elapsed time return: " << elapsed_seconds.count() << "s" << std::endl;

        std::cout << vec_loc1[0] << std::endl;
        std::cout << vec_loc1[1] << std::endl;
        std::cout << vec_loc1[2] << std::endl;
    }

    {
        std::cout << "----------------------------------------------->> 5" << std::endl;
        std::cout << "std::vector<double> & vec_loc1 = vec_loc;" << std::endl;

        std::vector<double> vec_loc = ret_vector();

        auto start = std::chrono::system_clock::now();

        std::vector<double> &vec_loc1 = vec_loc;

        auto end = std::chrono::system_clock::now();

        std::chrono::duration<double> elapsed_seconds = end - start;

        std::cout << "elapsed time return: " << elapsed_seconds.count() << "s" << std::endl;

        std::cout << vec_loc1[0] << std::endl;
        std::cout << vec_loc1[1] << std::endl;
        std::cout << vec_loc1[2] << std::endl;
    }

    {
        std::cout << "----------------------------------------------->> 6" << std::endl;
        std::cout << "std::vector<double> vec_loc1(std::move(vec_loc));" << std::endl;

        std::vector<double> vec_loc = ret_vector();

        auto start = std::chrono::system_clock::now();

        std::vector<double> vec_loc1(std::move(vec_loc));

        auto end = std::chrono::system_clock::now();

        std::chrono::duration<double> elapsed_seconds = end - start;

        std::cout << "elapsed time return: " << elapsed_seconds.count() << "s" << std::endl;

        std::cout << vec_loc1[0] << std::endl;
        std::cout << vec_loc1[1] << std::endl;
        std::cout << vec_loc1[2] << std::endl;
    }

    {
        std::cout << "----------------------------------------------->> 7" << std::endl;
        std::cout << "std::vector<double> vec_loc1(vec_loc);" << std::endl;

        std::vector<double> vec_loc = ret_vector();

        auto start = std::chrono::system_clock::now();

        std::vector<double> vec_loc1(vec_loc);

        auto end = std::chrono::system_clock::now();

        std::chrono::duration<double> elapsed_seconds = end - start;

        std::cout << "elapsed time return: " << elapsed_seconds.count() << "s" << std::endl;

        std::cout << vec_loc1[0] << std::endl;
        std::cout << vec_loc1[1] << std::endl;
        std::cout << vec_loc1[2] << std::endl;
    }

}

