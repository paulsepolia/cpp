#include <iostream>
#include <memory>
#include <vector>
#include <cmath>
#include <algorithm>
#include <chrono>
#include <random>

class Double {
public:

    Double() : val(0.0) {}

    explicit Double(const double &val) : val(val) {}

    Double(const Double &obj) = default;

    Double &operator=(const Double &obj) {

        if (this != &obj) {
            val = obj.val;
        }
        return *this;
    }

    Double(Double &&obj) noexcept(true) {
        val = obj.val;
        obj.val = 0;
    }

    Double &operator=(Double &&obj) noexcept(true) {

        if (this != &obj) {
            val = obj.val;
        }
        return *this;
    }

    virtual ~Double() = default;

    double get() const {
        return val;
    }

    void set(const double &arg) {
        val = arg;
    }

    friend bool operator<(const Double &arg1, const Double &arg2) {
        return arg1.val < arg2.val;
    }

private:

    double val;
};

int main() {

    const auto dim = static_cast<size_t>(std::pow(10.0, 7.0));
    const auto TEST_DIM = static_cast<size_t>(std::pow(10.0, 1));

    for (size_t k = 0; k < 10; k++) {

        std::cout << " ------------------------------>> k = " << k << std::endl;

        {
            const auto seed = std::chrono::system_clock::now().time_since_epoch().count();

            std::vector<double> vec;

            for (size_t i = 0; i < dim; i++) {
                vec.push_back(static_cast<double>(i));
            }

            std::shuffle(vec.begin(), vec.end(), std::default_random_engine(seed));

            std::cout << " --> 1 --> sorting vector of doubles..." << std::endl;

            const auto start = std::chrono::steady_clock::now();

            std::sort(vec.begin(), vec.end());

            const auto finish = std::chrono::steady_clock::now();

            const double elapsed_seconds = std::chrono::duration_cast<
                    std::chrono::duration<double> >(finish - start).count();

            std::cout << " time used = " << elapsed_seconds << std::endl;

            for (size_t i = 0; i < TEST_DIM; i++) {
                std::cout << vec[i] << " ";
            }

            std::cout << std::endl;
        }

        {
            const auto seed = std::chrono::system_clock::now().time_since_epoch().count();

            std::vector<Double> vec;

            for (size_t i = 0; i < dim; i++) {
                vec.emplace_back(Double(static_cast<double>(i)));
            }

            std::shuffle(vec.begin(), vec.end(), std::default_random_engine(seed));

            std::cout << " --> 2 --> sorting vector of Double objects..." << std::endl;

            const auto start = std::chrono::steady_clock::now();

            std::sort(vec.begin(), vec.end());

            const auto finish = std::chrono::steady_clock::now();

            const double elapsed_seconds = std::chrono::duration_cast<
                    std::chrono::duration<double> >(finish - start).count();

            std::cout << " time used = " << elapsed_seconds << std::endl;

            for (size_t i = 0; i < TEST_DIM; i++) {
                std::cout << vec[i].get() << " ";
            }

            std::cout << std::endl;
        }

        {
            const auto seed = std::chrono::system_clock::now().time_since_epoch().count();

            std::vector<Double> vec;

            for (size_t i = 0; i < dim; i++) {
                vec.emplace_back(Double(static_cast<double>(i)));
            }

            std::shuffle(vec.begin(), vec.end(), std::default_random_engine(seed));

            std::cout << " --> 3 --> sorting vector of Double objects (lambda function)..." << std::endl;

            const auto start = std::chrono::steady_clock::now();

            std::sort(vec.begin(), vec.end(),
                      [](const Double &v1, const Double &v2) -> bool {
                          return v1.get() < v2.get();
                      });

            const auto finish = std::chrono::steady_clock::now();

            const double elapsed_seconds = std::chrono::duration_cast<
                    std::chrono::duration<double> >(finish - start).count();

            std::cout << " time used = " << elapsed_seconds << std::endl;

            for (size_t i = 0; i < TEST_DIM; i++) {
                std::cout << vec[i].get() << " ";
            }

            std::cout << std::endl;
        }
    }

    return 0;
}