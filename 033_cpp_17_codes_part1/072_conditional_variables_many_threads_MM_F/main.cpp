#include <iostream>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <vector>

const uint64_t NUM_THREADS = 1000;
const uint64_t MOD_NUM = 10;
std::mutex mtx{};
std::condition_variable cv{};

void print_id(uint64_t id) {
	std::unique_lock<std::mutex> lck(mtx);
	cv.wait(lck);
	if (id % MOD_NUM == 0)
	{
		std::cout << "thread " << id << std::endl;
	}
}

void go() {
	std::unique_lock<std::mutex> lck(mtx);
	cv.notify_all();
}

int main() {

	std::vector<std::thread> threads(NUM_THREADS);

	for (uint64_t i = 0; i < NUM_THREADS; ++i) {
		threads[i] = std::thread(print_id, i);
	}

	std::cout << " all threads ready to race..." << std::endl;

	go();

	for (auto& th : threads) {
		th.join();
	}

	std::cout << "Enter integer to exit: ";
	int sentinel = 0;
	std::cin >> sentinel;

	return 0;
}
