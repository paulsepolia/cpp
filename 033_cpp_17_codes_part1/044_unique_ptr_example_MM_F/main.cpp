#include <iostream>
#include <memory>

int main() {

    {
        // here is an empty unique_ptr object

        std::unique_ptr<uint64_t> ptr1;

        // check if unique pointer object is empty
        if (ptr1 == nullptr) {
            std::cout << " unique ptr object is empty. It points to nowhere" << std::endl;
        }

        ptr1.reset(new uint64_t(10));

        if (ptr1 != nullptr) {
            std::cout << " unique ptr object is not empty. It points to " << *ptr1 << std::endl;
        }

        ptr1.reset();

        // check if unique pointer object is empty
        if (ptr1 == nullptr) {
            std::cout << " unique ptr object is empty. It points to nowhere" << std::endl;
        }
    }

    {
        std::unique_ptr<uint64_t> ptr1(new uint64_t(11));
        std::unique_ptr<uint64_t> ptr2 = std::move(ptr1);

        if (ptr1 == nullptr) {
            std::cout << " unique ptr1 object is empty. It points to nowhere" << std::endl;
        }

        if (ptr2) {
            std::cout << " unique ptr2 object is not empty." << std::endl;
            std::cout << " It points to " << *ptr2 << std::endl;
        }
    }

    {
        std::unique_ptr<uint64_t> ptr1(new uint64_t(12));

        if (ptr1) {
            std::cout << " unique ptr2 object is not empty." << std::endl;
            std::cout << " It points to " << *ptr1 << std::endl;
        }

        auto *raw_prt = ptr1.release();

        if (ptr1 == nullptr) {
            std::cout << " unique ptr1 object is empty. It points to nowhere" << std::endl;
        }

        if (raw_prt) {
            std::cout << " raw ptr object points to " << *raw_prt << std::endl;
        }

        delete raw_prt;
        raw_prt = nullptr;
    }

    {
        std::shared_ptr<uint64_t> ptr1(new uint64_t(12));

        if (ptr1) {
            std::cout << " unique ptr2 object is not empty." << std::endl;
            std::cout << " It points to " << *ptr1 << std::endl;
        }

        std::cout << ptr1.unique() << std::endl;

        std::shared_ptr<uint64_t> ptr2 = ptr1;

        std::cout << ptr1.unique() << std::endl;

        std::shared_ptr<uint64_t> ptr3(ptr1);

        std::cout << ptr2.unique() << std::endl;

        std::shared_ptr<uint64_t> ptr4(std::move(ptr1));

        std::cout << ptr4.unique() << std::endl;
    }

    return 0;
}