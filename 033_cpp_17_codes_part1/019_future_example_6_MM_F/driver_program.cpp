// future example

#include <iostream>
#include <future>
#include <chrono>
#include <cmath>
#include <mutex>

// class declaration

class GetAsyncRes {

public:

    GetAsyncRes();

    ~GetAsyncRes();

    double heavy_load(const double &);

    bool get_status() const;

private:

    bool _is_done;
    uint32_t _index_local;
    static uint32_t _index_global;
    static std::mutex _mtx;
};

// static members

uint32_t GetAsyncRes::_index_global = 0;
std::mutex GetAsyncRes::_mtx{};

// class definition

GetAsyncRes::GetAsyncRes() : _is_done(false), _index_local(0) {}

GetAsyncRes::~GetAsyncRes() {};

double GetAsyncRes::heavy_load(const double &large_value) {

    const auto t1 = std::chrono::high_resolution_clock::now();

    _mtx.lock();
    _index_global++;
    _index_local = _index_global;
    std::cout << "--> heavy load id = " << _index_local << std::endl;
    _mtx.unlock();

    double sum = 0;
    while (true) {
        sum++;
        if (sum >= large_value) break;
    }

    const auto t2 = std::chrono::high_resolution_clock::now();
    const auto time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);

    _mtx.lock();
    std::cout << "--> exit heavy load id = " << _index_local << std::endl;
    std::cout << "--> took (secs) = " << time_span.count() << std::endl;
    _is_done = true;
    _mtx.unlock();

    return sum;
}

bool GetAsyncRes::get_status() const {
    return _is_done;
}

// the dirver program

int main() {

    // call function asynchronously

    const double LARGE_NUM1 = std::pow(10.0, 10.0);
    const double LARGE_NUM2 = std::pow(10.0, 9.0);
    const double LARGE_NUM3 = std::pow(10.0, 8.0);
    const double LARGE_NUM4 = std::pow(10.0, 7.0);

    // async objects

    GetAsyncRes a1;
    GetAsyncRes a2;
    GetAsyncRes a3;
    GetAsyncRes a4;

    // launch asynchronous operation

    auto fut1 = std::async(&GetAsyncRes::heavy_load, a1, LARGE_NUM1);
    auto fut2 = std::async(&GetAsyncRes::heavy_load, a2, LARGE_NUM2);
    auto fut3 = std::async(&GetAsyncRes::heavy_load, a3, LARGE_NUM3);
    auto fut4 = std::async(&GetAsyncRes::heavy_load, a4, LARGE_NUM4);

    // get results

    auto res1 = fut1.get();
    std::cout << "--> res1 = " << res1 << std::endl;
    auto res2 = fut2.get();
    std::cout << "--> res2 = " << res2 << std::endl;
    auto res3 = fut3.get();
    std::cout << "--> res3 = " << res3 << std::endl;
    auto res4 = fut4.get();
    std::cout << "--> res4 = " << res4 << std::endl;

    return 0;
}
