
//==================================//
// Constructs: while, eof           //
// Manipulators: ios, out, app      //
// Functions: open, get, close, eof //
//==================================//

//===============//
// code --> 0015 //
//===============//

// keywords: while, eof, ios, out, app, get, open, close, ifstream, ofstream

#include <iostream>
#include <fstream>
#include <cmath>

using std::cin;
using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::ios;
using std::pow;

// the main function

int main()
{
    // variables and parameters declaration

    cout << " --> 1" << endl;

    const long long int I_MAX = static_cast<long long>(pow(10.0, 2.0));
    ifstream inFile;
    ofstream outFile;
    char ch;
    long long int i;

    cout << " --> 2" << endl;

    for (i = 0; i < I_MAX; i++) {
        // open the uint

        inFile.open("file_input.txt", ios::in);
        inFile.get(ch);
        outFile.open("file_output.txt", ios::out | ios::app);

        // while game

        while(!inFile.eof()) {
            outFile << ch;
            inFile.get(ch);
        }

        // close the units

        inFile.close();
        outFile.close();
    }

    cout << " --> 3 --> end" << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    cout << " --> 4 --> exit" << endl;

    return 0;
}

//======//
// FINI //
//======//

