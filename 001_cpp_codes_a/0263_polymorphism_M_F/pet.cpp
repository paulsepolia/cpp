
//======================//
// pet class definition //
//======================//

#include <iostream>
#include "pet.h"

using std::cout;
using std::endl;

namespace pgg {
// constructor

Pet::Pet() : name("No name yet") {}

// member function

void Pet::print() const
{
    cout << "name: " << name << endl;
}

// destructor

Pet::~Pet() {}

} // pgg

//======//
// FINI //
//======//

