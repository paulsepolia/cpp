
//=============================//
// manipulators: setw, setfill //
//=============================//

//===============//
// code --> 0005 //
//===============//

// keywords: setw, setfill, iomanip, manipulators

#include <iostream>
#include <iomanip>

using std::cout;
using std::cin;
using std::endl;
using std::setw;
using std::setfill;

// the main function

int main()
{
    int x = 15;
    int y = 7634;

    cout << " --> 1" << endl;

    cout << "12345678901234567890" << endl;
    cout << setw(5) << x << setw(7) << y << setw(8) << "Warm" << endl;

    cout << " --> 2" << endl;

    cout << setfill('*');

    cout << setw(5) << x << setw(7) << y << setw(8) << "Warm" << endl;

    cout << " --> 3" << endl;

    cout << setfill('#');

    cout << setw(5) << x << setw(7) << y << setw(8) << "Warm" << endl;

    cout << " --> 4" << endl;

    cout << setfill(' ');

    cout << setw(5) << x << setw(7) << y << setw(8) << "Warm" << endl;

    cout << " --> 5" << endl;

    cout << setfill('-');

    cout << setw(5) << x << setw(7) << y << setw(8) << "Warm" << endl;

    cout << " --> 6 --> end" << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

