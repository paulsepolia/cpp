//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/10/23              //
// Functions: namespace          //
//===============================//

#include <iostream>
#include <iomanip>

using namespace std;

//namespace
namespace A {
int i = 10;
}
//namespace
namespace B {
int i = 20;
}
//function
void fA()
{
    using namespace A;
    cout << "In fA: " << A::i << " " << B::i << " " << i << endl;
}
//function
void fB()
{
    using namespace B;
    cout << "In fB: " << B::i << " " << i << endl;
}
// main function
int main()
{
    fA();
    fB();
    cout << "In main: " << A::i << " " << B::i  << endl;
    using A::i;
    cout << i << endl;
    return 0;
}
// end
