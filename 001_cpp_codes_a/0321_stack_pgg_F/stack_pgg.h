
//====================================//
// declaration of the stack_pgg class //
//====================================//

#ifndef STACK_PGG_H
#define STACK_PGG_H

#include <vector>
#include <cstdlib>

using std::vector;
using std::size_t;

template <typename T, typename cont = vector<T> >
class stack_pgg {
public:

    stack_pgg<T, cont>(); // default constructor
    ~stack_pgg<T, cont>(); // destructor
    stack_pgg<T, cont>(const stack_pgg<T, cont> &); // copy constructor

    // member functions

    bool empty() const;
    size_t size() const;
    T & top();
    void push(const T &);
    void pop();
    void swap(stack_pgg<T, cont> &);
    void emplace(const T &);

    // operators overload

    stack_pgg<T, cont> & operator = (const stack_pgg<T, cont> &); // assignment operator

private:

    cont elems;
};

#endif // STACK_PGG_H

//======//
// FINI //
//======//

