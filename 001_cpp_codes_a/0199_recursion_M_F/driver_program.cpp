
//==========================//
// Largest element in array //
//==========================//

#include <iostream>
#include <cmath>
#include <iomanip>
#include <vector>
#include <thread>
#include <pthread.h>
#include <climits>
#include <assert.h>
#include <sys/resource.h>

using std::endl;
using std::cin;
using std::cout;
using std::cos;
using std::setprecision;
using std::fixed;
using std::showpos;
using std::showpoint;
using std::pow;
using std::vector;
using std::thread;

// functions declaration

// 1

double largest (const double [], long, long);

// 2

void benchFun()
{
    const long DIM_ARRAY = static_cast<long>(pow(10.0, 5.0));
    double *arrayA = new double [DIM_ARRAY]; // for heap usage
//  double arrayA[DIM_ARRAY]; // for stack usage

    cout << fixed;
    cout << showpos;
    cout << showpoint;
    cout << setprecision(10);

    for (long i = 0; i < DIM_ARRAY; i++) {
        arrayA[i] = cos(static_cast<double>(i));
    }

    double elem;

    elem = largest(arrayA, 0, DIM_ARRAY);

    cout << " --> elem = " << elem << endl;

    delete [] arrayA; // for heap usage
}

// the main function

int main()
{
    const long I_MAX = static_cast<long>(pow(10.0, 4.0));
    const int NUM_THREADS = 2;

    // adjust the stack size

    const rlim_t kStackSize = 1*1024*1024*1024;

    struct rlimit rl;
    rl.rlim_cur = kStackSize;
    setrlimit(RLIMIT_STACK, &rl);

    // end - adjust the stack size

    vector<thread> vecTh;

    for (long i = 0; i < I_MAX; i++) {
        for (int j = 0; j < NUM_THREADS; j++) {
            vecTh.push_back(thread(benchFun));
        }

        for (auto & t : vecTh) {
            t.join();
        }

        vecTh.clear();
    }

    return 0;
}

// function definition

double largest(const double list[], long lowerIndex, long upperIndex)
{
    double max;

    if (lowerIndex == upperIndex) { // size of list is 1
        return list[lowerIndex];
    } else {
        max = largest(list, lowerIndex+1, upperIndex);

        if (list[lowerIndex] >= max) {
            return list[lowerIndex];
        } else {
            return max;
        }
    }
}

//======//
// FINI //
//======//
