#!/bin/bash

  # 1. compile

  icpc -O3           \
       -Wall         \
       -std=c++11    \
       -static       \
       oxam_test.cpp \
       -o x_intel
