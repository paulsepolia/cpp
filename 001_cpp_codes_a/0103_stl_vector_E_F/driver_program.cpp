
#include <iostream>
#include <vector>
#include <cmath>

using std::vector;
using std::endl;
using std::cout;
using std::pow;
using std::cin;

int main()
{
    vector<int> vecA;
    long int i;
    const long int I_MAX = static_cast<long int>(pow(10.0, 7.0));
    const long int I_MAX_ERASE = static_cast<long int>(pow(10.0, 1.0));
    int sentinel;
    int j;
    const int J_MAX = static_cast<int>(pow(10.0, 3.0));

    // 1. build the vecA

    for (j = 0; j < J_MAX; j++) {
        cout << "  0 ----------------------------------------------->>>> " << j << endl;

        cout << "  1 --> Building the vecA " << endl;

        for (i = 1; i <= I_MAX; i++) {
            vecA.push_back(i);
        }

        // 2. add extra elements to the vecA

        cout << "  2 --> Adding extra elements to the vecA " << endl;

        for (i = 1; i <= I_MAX; i++) {
            vecA.push_back(i);
        }

        // 3. get the size of the vector

        cout << "  3 --> Get the size of the vecA " << endl;
        cout << "  vecA.size() = " << vecA.size() << endl;

        // 4. erase the 4th element

        cout << "  4 --> Erase the 4th element of vecA " << endl;
        cout << " vecA.erase(vecA.begin()+4) " << endl;

        vecA.erase(vecA.begin()+4);

        // 5. get the size again

        cout << "  5 --> And get the size of the vector again " << endl;

        cout << "  6 --> Get the size of the vecA " << endl;
        cout << "  vecA.size() = " << vecA.size() << endl;

        // 6. erase the second element many times consequtive

        cout << "  7 --> Erase the second element many times " << endl;

        for (i = 1; i <= I_MAX_ERASE; i++) {
            vecA.erase(vecA.begin()+2);
        }

        // 7. get the size again

        cout << "  8 --> And get the size of the vector again " << endl;
        cout << "  9 --> Get the size of the vecA " << endl;
        cout << "  vecA.size() = " << vecA.size() << endl;

        cout << " 10 --> to erase an element is a very slow process " << endl;

        cout << " 11 --> I clear all the elements of the vector " << endl;

        vecA.clear();

        cout << " 12 --> And get the size of the vector again " << endl;
        cout << " 13 --> Get the size of the vecA " << endl;
        cout << " vecA.size() = " << vecA.size() << endl;

        // 14. build again

        for (i = 1; i <= I_MAX; i++) {
            vecA.push_back(i);
        }

        for (i = 1; i <= I_MAX; i++) {
            vecA.push_back(i);
        }

        vecA.clear();

    }

    // x. sentinel

    cout << "  x --> Enter an integer to exit: ";
    cin >> sentinel;

    return 0;
}

// fini
