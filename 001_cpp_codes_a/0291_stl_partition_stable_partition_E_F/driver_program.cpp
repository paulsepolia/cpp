
//=====================================//
// STL, partition and stable_partition //
//=====================================//

#include <iostream>
#include <algorithm>
#include <iterator>

using std::cout;
using std::cin;
using std::endl;
using std::partition;
using std::stable_partition;
using std::copy;
using std::ostream_iterator;

// a predicate function via class

class above40C {
public:
    bool operator()(int n)   // the function oblject will be above40C()
    {
        return (n > 40);
    }
};

// a predicate function

bool above40(int n)
{
    return (n > 40);
}

// the main function

int main()
{
    cout << "Illustrating the generic partition and stable_partition algorithms." << endl;

    const int N = 7;
    ostream_iterator<int> out(cout, " ");

    int a0[N] = {50, 30, 10, 70, 60, 40, 20};
    int a1[N];

    copy(&a0[0], &a0[N], &a1[0]);

    cout << "Original sequence: ";

    copy(&a1[0], &a1[N], out);

    cout << endl;

    // partition a1, putting numbers greater than 40
    // first, followed by those less than or equal to 40

    int * split = partition(&a1[0], &a1[N], above40C());

    cout << "Result of unstable partitioning: ";

    copy(&a1[0], split, out);
    cout << " | ";
    copy(split, &a1[N], out);
    cout << endl;

    // restore a1 to a0

    copy(&a0[0], &a0[N], &a1[0]);

    // stable partition

    split = stable_partition(&a1[0], &a1[N], above40);

    cout << "Result of stable partitioning: ";

    copy(&a1[0], split, out);
    cout << " | ";
    copy(split, &a1[N], out);
    cout << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

