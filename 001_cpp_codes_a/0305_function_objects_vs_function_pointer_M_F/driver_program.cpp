
//=====================================//
// function object vs function pointer //
//=====================================//

#include <iostream>
#include <iomanip>
#include <vector>
#include <cmath>
#include <ctime>
#include <numeric>

using std::cout;
using std::cin;
using std::endl;
using std::fixed;
using std::setprecision;
using std::showpos;
using std::showpoint;
using std::vector;
using std::pow;
using std::clock;
using std::clock_t;
using std::accumulate;

// accumulate_pgg_a
// takes a pointer to a function

template <class inputIter, class T>
T accumulate_pgg_a(inputIter first, inputIter last, T init, T (*bin_fun)(const T& x, const T& y))
{
    while (first != last) {
        init = bin_fun(init, *first);
        ++first;
    }

    return init;
}

// accumulate_pgg_b
// takes a function object

template <class inputIter, class T, class binaryFunction>
T accumulate_pgg_b(inputIter first, inputIter last, T init, binaryFunction bin_fun)
{
    while (first != last) {
        init = bin_fun(init, *first);
        ++first;
    }

    return init;
}

// function object

template <class T>
class add_pgg_CL {
public:
    T operator() (const T& x, const T& y) const
    {
        return x+y;
    }
};

// function

template <class T>
inline T add_pgg_fun(const T& x, const T& y) // that interface here must match the
{
    // interface of accumulate_pgg_a
    return x+y;			     // That is why we prefer function objects
}

// the main function

int main()
{
    // local variables and parameters

    const long long DIM_MAX = static_cast<long long>(pow(10.0, 5.0));
    const long long K_MAX = static_cast<long long>(pow(10.0, 5.0));
    const long long CALL_MAX = static_cast<long long>(pow(10.0, 5.0));
    clock_t t1;
    clock_t t2;
    double res;
    double init;
    add_pgg_CL<double> add_pgg_CL_obj;

    // output format

    cout << fixed;
    cout << showpos;
    cout << showpoint;
    cout << setprecision(20);

    // for loop

    for (long long k = 0; k < K_MAX; k++) {
        // the counter

        cout << "-------------------------------------------------------->> " << k << endl;

        // local variables

        vector<double> *VA = new vector<double>;
        double * AA = new double [DIM_MAX];

        //
        // build the vector
        //

        cout << endl;
        cout << " --> build the vector" << endl;
        cout << endl;

        for (long long i = 0; i != DIM_MAX; i++) {
            VA->push_back(cos(static_cast<double>(i)));
        }

        // apply the handmade accumulate_pgg_b

        cout << " --> apply the handmade accumulate_pgg_b for function objects"
             << " --> add_pgg_CL<double>()" << endl;

        t1 = clock();

        for (long long i = 0; i != CALL_MAX; i++) {
            res = 0;
            init = 0.0;
            res = accumulate_pgg_b(VA->begin(), VA->end(), init, add_pgg_CL<double>());
        }

        t2 = clock();

        cout << " --> result = " << res << endl;
        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        cout << endl;

        // apply the handmade accumulate_pgg_b

        cout << " --> apply the handmade accumulate_pgg_b for function objects"
             << " --> add_pgg_CL_obj" << endl;

        t1 = clock();

        for (long long i = 0; i != CALL_MAX; i++) {
            res = 0;
            init = 0.0;
            res = accumulate_pgg_b(VA->begin(), VA->end(), init, add_pgg_CL_obj);
        }

        t2 = clock();

        cout << " --> result = " << res << endl;
        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        cout << endl;

        // apply the STL accumulate

        cout << " --> apply the STL accumulate for function objects"
             << " --> add_pgg_CL_obj" << endl;

        t1 = clock();

        for (long long i = 0; i != CALL_MAX; i++) {
            res = 0;
            init = 0.0;
            res = accumulate(VA->begin(), VA->end(), init, add_pgg_CL_obj);
        }

        t2 = clock();

        cout << " --> result = " << res << endl;
        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        cout << endl;

        // apply the STL accumulate

        cout << " --> apply the STL accumulate for function objects"
             << " --> add_pgg_CL<double>()" << endl;

        t1 = clock();

        for (long long i = 0; i != CALL_MAX; i++) {
            res = 0;
            init = 0.0;
            res = accumulate(VA->begin(), VA->end(), init, add_pgg_CL<double>());
        }

        t2 = clock();

        cout << " --> result = " << res << endl;
        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        cout << endl;

        // apply the handmade accumulate_pgg_a

        cout << " --> apply the handmade accumulate_pgg_a for function pointers"
             << " --> add_pgg_fun" << endl;

        t1 = clock();

        for (long long i = 0; i != CALL_MAX; i++) {
            res = 0;
            init = 0.0;
            res = accumulate_pgg_a(VA->begin(), VA->end(), init, &add_pgg_fun);
        }

        t2 = clock();

        cout << " --> result = " << res << endl;
        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // delete the vector

        delete VA;

        //
        // build the array
        //

        cout << endl;
        cout << " --> build the array" << endl;
        cout << endl;

        for (long long i = 0; i != DIM_MAX; i++) {
            AA[i]=cos(static_cast<double>(i));
        }

        // apply the handmade accumulate_pgg_b

        cout << " --> apply the handmade accumulate_pgg_b for function objects"
             " --> add_pgg_CL<double>()" << endl;

        t1 = clock();

        for (long long i = 0; i != CALL_MAX; i++) {
            res = 0;
            init = 0.0;
            res = accumulate_pgg_b(&AA[0], &AA[DIM_MAX], init, add_pgg_CL<double>());
        }

        t2 = clock();

        cout << " --> result = " << res << endl;
        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        cout << endl;

        // apply the handmade accumulate_pgg_b

        cout << " --> apply the handmade accumulate_pgg_b for function objects"
             << " --> add_pgg_CL_obj" << endl;

        t1 = clock();

        for (long long i = 0; i != CALL_MAX; i++) {
            res = 0;
            init = 0.0;
            res = accumulate_pgg_b(&AA[0], &AA[DIM_MAX], init, add_pgg_CL_obj);
        }

        t2 = clock();

        cout << " --> result = " << res << endl;
        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        cout << endl;

        // apply the STL accumulate

        cout << " --> apply the STL accumulate for function objects"
             << " --> add_pgg_CL_obj" << endl;

        t1 = clock();

        for (long long i = 0; i != CALL_MAX; i++) {
            res = 0;
            init = 0.0;
            res = accumulate(&AA[0], &AA[DIM_MAX], init, add_pgg_CL_obj);
        }

        t2 = clock();

        cout << " --> result = " << res << endl;
        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        cout << endl;

        // apply the STL accumulate

        cout << " --> apply the STL accumulate for function objects"
             << " --> add_pgg_CL<double>()" << endl;

        t1 = clock();

        for (long long i = 0; i != CALL_MAX; i++) {
            res = 0;
            init = 0.0;
            res = accumulate(&AA[0], &AA[DIM_MAX], init, add_pgg_CL<double>());
        }

        t2 = clock();

        cout << " --> result = " << res << endl;
        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        cout << endl;

        // apply the handmade accumulate_pgg_a

        cout << " --> apply the handmade accumulate_pgg_a for function pointers"
             << " --> add_pgg_fun" << endl;

        t1 = clock();

        for (long long i = 0; i != CALL_MAX; i++) {
            res = 0;
            init = 0.0;
            res = accumulate_pgg_a(&AA[0], &AA[DIM_MAX], init, &add_pgg_fun);
        }

        t2 = clock();

        cout << " --> result = " << res << endl;
        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        cout << endl;

        // delete the array

        delete [] AA;
    }


    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

