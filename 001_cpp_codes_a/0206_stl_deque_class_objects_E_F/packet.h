
#ifndef PACKET_H
#define PACKET_H

//==========//
// packet.h //
//==========//

#include <string>

using std::string;

class packet {
public:

    packet(); // default constructor
    packet(int, string); // constructor
    int getpnum() const; // member function
    string getinfo() const; // member function

private:

    int pnum; // packet number
    string info; // information
};

#endif // PACKET_H

//======//
// FINI //
//======//

