
//======================//
// sort and functionals //
//======================//

#include <iostream>
#include <algorithm>
#include <cassert>
#include <functional>
#include <iomanip>
#include <cmath>

using std::cout;
using std::cin;
using std::endl;
using std::sort;
using std::random_shuffle;
using std::greater;
using std::less;
using std::fixed;
using std::setprecision;
using std::showpos;
using std::showpoint;
using std::pow;

// function object

template <class T>
class lesspgg {
public:
    bool operator() (const T & x, const T & y) const
    {
        return (x < y);
    }
};

template <class T>
class greaterpgg {
public:
    bool operator() (const T & x, const T & y) const
    {
        return (x > y);
    }
};

// the main program

int main()
{
    // local parameters and variables

    const long long DIM_MAX = static_cast<long long>(pow(10.0, 8.0));
    const long long K_MAX = static_cast<long long>(pow(10.0, 7.0));

    for (long long k = 0; k < K_MAX; k++) {
        cout << "----------------------------------------------------------->> " << k << endl;

        // the container

        double * a = new double [DIM_MAX];

        // build the container

        cout << "  1 --> build the container" << endl;

        for (long long i = 0; i < DIM_MAX; i++) {
            a[i] = static_cast<double>(i);
        }

        // random suffle the container

        cout << "  2 --> random shuffle the container" << endl;

        random_shuffle(&a[0], &a[DIM_MAX]);

        // sort into asceding order

        cout << "  3 --> sort the container using the binary predicate less<double>" << endl;

        sort(&a[0], &a[DIM_MAX], less<double>());

        // test the ordering

        cout << "  4 --> test for success" << endl;

        for (long long int i = 0; i < DIM_MAX; i++) {
            assert(a[i] == i);
        }

        // output for success

        cout << " --> All are okay --> 1" << endl;

        // sort into descending order

        cout << "  5 --> sort the container using the binary predicate greater<double>" << endl;

        sort(&a[0], &a[DIM_MAX], greater<double>());

        // test the ordering

        cout << "  6 --> test for success" << endl;

        for (long long int i = 0; i < DIM_MAX; i++) {
            assert(a[i] == DIM_MAX-1-i);
        }

        // output for success

        cout << " --> All are okay --> 2" << endl;

        // sort into asceding order

        cout << "  7 --> sort the container using the binary predicate lesspgg<double>" << endl;

        sort(&a[0], &a[DIM_MAX], lesspgg<double>());

        // test the ordering

        cout << "  8 --> test for success" << endl;

        for (long long int i = 0; i < DIM_MAX; i++) {
            assert(a[i] == i);
        }

        // output for success

        cout << " --> All are okay --> 3" << endl;

        // sort into descending order

        cout << "  9 --> sort the container using the binary predicate greaterpgg<double>" << endl;

        sort(&a[0], &a[DIM_MAX], greaterpgg<double>());

        // test the ordering

        cout << " 10 --> test for success" << endl;

        for (long long int i = 0; i < DIM_MAX; i++) {
            assert(a[i] == DIM_MAX-1-i);
        }

        // output for success

        cout << " --> All are okay --> 4" << endl;

        // delete the container

        cout << " 11 --> delete the container" << endl;

        delete [] a;

    }

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

