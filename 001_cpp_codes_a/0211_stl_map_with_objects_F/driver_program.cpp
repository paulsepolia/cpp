
//================================//
// storing class objects in a map //
//================================//

// use a map to create a phone directory

#include <iostream>
#include <map>
#include <string>

using std::cout;
using std::cin;
using std::endl;
using std::map;
using std::string;
using std::pair;

// this is the key class

class name {
public:
    name() : str ("") {}
    explicit name(string s) : str(s) {}
    string get() const
    {
        return str;
    }

private:
    string str;
};

// must define less than relative to name objects

bool operator < (name a, name b)
{
    return a.get() < b.get();
}

// this is the value class

class phoneNum {
public:

    phoneNum() : str ("") {}
    explicit phoneNum(string s) : str (s) {}
    string get() const
    {
        return str;
    }

private:
    string str;
};

// the main function

int main()
{
    // local variables and parameters

    map<name, phoneNum> directory;
    map<name, phoneNum>::iterator p;

    // put names and numbers into map

    directory.insert(pair<name, phoneNum>(name("Tom"), phoneNum("555-4533")));
    directory.insert(pair<name, phoneNum>(name("Chris"), phoneNum("555-9768")));
    directory.insert(pair<name, phoneNum>(name("John"), phoneNum("555-8195")));
    directory.insert(pair<name, phoneNum>(name("Rachel"), phoneNum("555-0809")));

    // output the contents of the directory

    for (p = directory.begin(); p != directory.end(); p++) {
        cout << " name : " << p->first.get() << ", phone : " <<  p->second.get() << endl;
    }

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//