//====================//
// Common Functions //
//====================//

#include "IncludeLibs.h"

// external function

template <typename T>
void benchFun(VectorPaul<T> & obj, long dim)
{
    long i;

    // 1.

    cout << " --> obj.setDim(dim);" << endl;
    obj.setDim(dim);

    // 2.

    cout << " --> obj.allocateVector();" << endl;
    obj.allocateVector();

    // 3.

    cout << " --> obj.setVector(j, val);" << endl;

    for (i = 0; i < dim; i++) {
        double val = static_cast<double>(i);
        obj.setVector(i, val);
    }

    // 4.

    cout << " --> obj.getVector(0L) = " << obj.getVector(0L)  << endl;

    // 5.

    cout << " --> obj.reverseVector();" << endl;
    obj.reverseVector();

    // 6.

    cout << " --> obj.getVector(0L) = " << obj.getVector(0L)  << endl;

    // 7.

    cout << " --> obj.setVector(j, val);" << endl;

    for (i = 0; i < dim; i++) {
        double val = cos(static_cast<double>(i));
        obj.setVector(i, val);
    }

    // 8.

    cout << " --> obj.getVector(0L) = " << obj.getVector(0L)  << endl;

    // 10.

    cout << " --> obj.sortVector();" << endl;
    obj.sortVector();

    // 11.

    cout << " --> obj.isSortedVector() = " << obj.isSortedVector() << endl;

    // 12.

    for (i = 2 ; i < 4; i++)
        cout << " --> obj.getVector(" << i << ") = " << obj.getVector(i) << endl;

    // 13.

    cout << " --> obj.randomShuffleVector();" << endl;
    obj.randomShuffleVector();

    // 14.

    for (i = 12 ; i < 14; i++)
        cout << " --> obj.getVector(" << i << ") = " << obj.getVector(i) << endl;

    // 15.

    cout << " --> obj.maxElementVector() = " << obj.maxElementVector() << endl;

    // 16.

    cout << " --> obj.minElementVector() = " << obj.minElementVector() << endl;

    // 17.

    cout << " --> obj.getDim() = " << obj.getDim() << endl;

    // 18.

    cout << " --> obj.fillVector(10);" << endl;
    double val = 10.0;
    obj.fillVector(val);

    // 19.

    cout << " --> obj.maxElementVector() = " << obj.maxElementVector() << endl;

    // 20.

    cout << " --> obj.minElementVector() = " << obj.minElementVector() << endl;

    // 21.

    cout << " --> obj.generateVectorRandom();" << endl;
    obj.generateVectorRandom();

    // 22.

    cout << " --> obj.maxElementVector() = " << obj.maxElementVector() << endl;

    // 23.

    cout << " --> obj.minElementVector() = " << obj.minElementVector() << endl;

    // 24.

    cout << " --> obj.sortVectorQSort();" << endl;
    obj.sortVectorQSort();

    // 25.

    cout << " --> obj.isSortedVector() = " << obj.isSortedVector() << endl;

    // 26.

    cout << " --> obj.generateVectorUnique();" << endl;
    obj.generateVectorUnique();

    // 27.

    cout << " --> obj.maxElementVector() = " << obj.maxElementVector() << endl;

    // 28.

    cout << " --> obj.minElementVector() = " << obj.minElementVector() << endl;

    // 29.

    VectorPaul<T> objLocA;
    VectorPaul<T> objLocB;
    VectorPaul<T> objLocC;
    VectorPaul<T> objLocD;
    VectorPaul<T> objLocE;
    VectorPaul<T> objLocF;

    objLocA.setEqualTo(obj);
    objLocB.setEqualTo(obj);
    objLocB = objLocA;
    objLocC = objLocB + objLocA;
    objLocD = objLocB - objLocA; // THIS IS WRONG
    objLocE.addVector(objLocA , objLocB);
    objLocF.subtractVector(objLocA , objLocB);

    // 30.

    cout << " --> objLocA.maxElementVector() = " << objLocA.maxElementVector() << endl;

    // 31.

    cout << " --> objLocA.minElementVector() = " << objLocA.minElementVector() << endl;

    // 32.

    cout << " --> objLocB.maxElementVector() = " << objLocB.maxElementVector() << endl;

    // 33.

    cout << " --> objLocB.minElementVector() = " << objLocB.minElementVector() << endl;

    // 34.

    cout << " --> objLocC.maxElementVector() = " << objLocC.maxElementVector() << endl;

    // 35.

    cout << " --> objLocC.minElementVector() = " << objLocC.minElementVector() << endl;

    // 36.

    cout << " --> objLocD.maxElementVector() = " << objLocD.maxElementVector() << endl;

    // 37.

    cout << " --> objLocD.minElementVector() = " << objLocD.minElementVector() << endl;

    // 38.

    cout << " --> objLocE.maxElementVector() = " << objLocE.maxElementVector() << endl;

    // 39.

    cout << " --> objLocE.minElementVector() = " << objLocE.minElementVector() << endl;

    // 40.

    cout << " --> objLocF.maxFlementVector() = " << objLocF.maxElementVector() << endl;

    // 41.

    cout << " --> objLocF.minElementVector() = " << objLocF.minElementVector() << endl;

    // 42.

    cout << " --> obj.deleteVector();" << endl;

    obj.deleteVector();

    // 43.

    cout << " --> objLocA.deleteVector();" << endl;

    objLocA.deleteVector();

    // 44.

    cout << " --> objLocB.deleteVector();" << endl;

    objLocB.deleteVector();

    // 45.

    cout << " --> objLocC.deleteVector();" << endl;

    objLocC.deleteVector();

    // 46.

    cout << " --> objLocD.deleteVector();" << endl;

    objLocD.deleteVector();

    // 47.

    cout << " --> objLocE.deleteVector();" << endl;

    objLocE.deleteVector();

    // 48.

    cout << " --> objLocF.deleteVector();" << endl;

    objLocF.deleteVector();

}

//======//
// FINI //
//======//
