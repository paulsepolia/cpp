
//==========================//
// STL, transform algorithm //
//==========================//

#include <iostream>
#include <algorithm>
#include <vector>
#include <functional>
#include <cmath>

using std::cout;
using std::cin;
using std::endl;
using std::transform;
using std::vector;
using std::plus;
using std::multiplies;
using std::pow;

// function

int op_A (int i)
{
    return ++i;
}

// function

int op_AA (const int& i, const int& j)
{
    return i+j;
}

// function object

class op_B {
public:
    int operator()(int i)
    {
        return ++i;
    }
};

// function object

class op_BB {
public:
    int operator()(const int& i, const int& j)
    {
        return (i+j);
    }
};

// the main function

int main ()
{
    vector<int> foo;
    vector<int> bar;
    vector<int>::iterator itv;

    // set some values:

    for (int i = 1; i < 6; i++) {
        foo.push_back (i*10);
    }

    bar.resize(foo.size());  // allocate space

    // bar: 11 21 31 41 51

    transform(foo.begin(), foo.end(), bar.begin(), op_A);

    for (itv = bar.begin(); itv != bar.end(); ++itv) {
        cout << " " << *itv;
    }

    cout << endl;

    // bar: 12 22 32 42 52

    transform(bar.begin(), bar.end(), bar.begin(), op_B());

    for (itv = bar.begin(); itv != bar.end(); ++itv) {
        cout << " " << *itv;
    }

    cout << endl;

    // plus adds together its two arguments:

    // foo: 22 42 62 82 102

    transform(foo.begin(), foo.end(), bar.begin(), foo.begin(), plus<int>());

    cout << "foo contains:";

    for (itv = foo.begin(); itv != foo.end(); ++itv) {
        cout << ' ' << *itv;
    }

    //

    cout << endl;

    transform(foo.begin(), foo.end(), bar.begin(), foo.begin(), op_AA);

    cout << "foo contains:";

    for (itv = foo.begin(); itv != foo.end(); ++itv) {
        cout << ' ' << *itv;
    }

    //

    cout << endl;

    //

    transform(foo.begin(), foo.end(), bar.begin(), foo.begin(), op_BB());

    cout << "foo contains:";

    for (itv = foo.begin(); itv != foo.end(); ++itv) {
        cout << ' ' << *itv;
    }

    //

    cout << endl;

    transform (foo.begin(), foo.end(), bar.begin(), foo.begin(), multiplies<int>());

    cout << "foo contains:";

    for (itv = foo.begin(); itv != foo.end(); ++itv) {
        cout << ' ' << *itv;
    }

    cout << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

