//=====================//
// define preprocessor //
//=====================//

//===============//
// code --> 0343 //
//===============//

// standard macro names

#include <iostream>
using namespace std;

// the main function

int main()
{
    cout << "This is the line number " << __LINE__ << endl;
    cout << "of file " << __FILE__ << endl;
    cout << "Its compilation began " << __DATE__ << endl;
    cout << "at " << __TIME__ << endl;
    cout << "The compiler gives a __cplusplus value of " << __cplusplus << endl;

    return 0;
}

// FINI
