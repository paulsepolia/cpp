//=========================//
// PFArrayBakDeclaration.h //
//=========================//

// objects of this class are partially filled arrays of doubles

#ifndef PFARRAYDBAK_H
#define PFARRAYDBAK_H

#include "PFArrayDeclaration.h"

namespace pgg {
template <typename T, typename P>
class PFArrayDBak : public PFArrayD<T,P> { // public inheritance
public:

    PFArrayDBak(); // default constructor
    explicit PFArrayDBak(T); // a non-default constructor
    PFArrayDBak(const PFArrayDBak&); // copy constructor

    void backup(); // makes a backup copy of the partially filled array

    void restore(); // restores the partially filled array to the last saved version.
    // if backup has never been invoked, this empties the partially filled array.

    PFArrayDBak& operator = (const PFArrayDBak&);

    ~PFArrayDBak();

private:

    P *b; // for a backup of the main array
    T usedB;
};
} // pgg

#endif // PFARRAYDBAK_H

//======//
// FINI //
//======//
