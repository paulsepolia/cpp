//================//
// driver program //
//================//

#include <iostream>
#include <cmath>
#include <iomanip>
#include "base.h"
#include "derived1.h"
#include "derived2.h"

using std::endl;
using std::cout;
using std::cin;
using std::pow;
using std::boolalpha;

using pgg::Base;
using pgg::Derived1;
using pgg::Derived2;

// the main function

int main()
{
    // local variables and parameters

    const long DIM_A = static_cast<long>(pow(10.0, 7.2));
    const int K_MAX = static_cast<int>(pow(10.0, 5.0));
    Base A1;
    Base A2;
    Derived1 B1;
    Derived1 B2;
    Derived2 C1;
    Derived2 C2;

    // set the output format

    cout << boolalpha;

    // the for test loop

    for (int k = 0; k < K_MAX; k++) {

        cout << "------------------------------------------------------------>> " << k << endl;

        //=================//
        // base class here //
        //=================//

        cout << "===============" << endl;
        cout << "base class here" << endl;
        cout << "===============" << endl;

        cout << " --> A1.setDim(DIM_A);" << endl;

        A1.setDim(DIM_A);

        //

        cout << " --> A1.reserve();" << endl;

        A1.reserve();

        //

        cout << " --> A1.build(10.0);" << endl;

        A1.build(10.0);

        //

        cout << " --> A2.setEqual(A1);" << endl;

        A2.setEqual(A1);

        //

        cout << " --> A2 = A1;" << endl;

        A2 = A1;

        //

        cout << " --> A1.destroy();" << endl;

        A1.destroy();

        //

        cout << " --> A2.destroy();" << endl;

        A2.destroy();

        //

        cout << " --> A1.setDim(DIM_A);" << endl;

        A1.setDim(DIM_A);

        //

        cout << " --> A1.reserve();" << endl;

        A1.reserve();

        //

        cout << " --> A1.buildRandom(10);" << endl;

        A1.buildRandom(10);

        //

        cout << " --> A2.setEqual(A1);" << endl;

        A2.setEqual(A1);

        //

        cout << " --> A1.destroy();" << endl;

        A1.destroy();

        //

        cout << " --> A2.destroy();" << endl;

        A2.destroy();

        //

        cout << " --> A1.setDim(DIM_A);" << endl;

        A1.setDim(DIM_A);

        //

        cout << " --> A1.reserve();" << endl;

        A1.reserve();

        //

        cout << " --> A1.setElement(cos(static_cast<double>(i)), i);" << endl;

        for (long i = 0; i < DIM_A; i++) {
            A1.setElement(cos(static_cast<double>(i)), i);
        }

        //

        cout << " --> A2.setDim(DIM_A);" << endl;

        A2.setDim(DIM_A);

        //

        cout << " --> A2.reserve();" << endl;

        A2.reserve();

        //

        cout << " --> A2.setElement(cos(static_cast<double>(i)), i);" << endl;

        for (long i = 0; i < DIM_A; i++) {
            A2.setElement(cos(static_cast<double>(i)), i);
        }

        //

        cout << " --> A2.isEqual(A1) = " << A2.isEqual(A1) << endl;

        A2 = A1;

        cout << " --> A2.isEqual(A1) = " << A2.isEqual(A1) << endl;

        Base A3(A1);

        cout << " --> A3.isEqual(A1) = " << A3.isEqual(A1) << endl;

        //

        cout << " --> A1 == A2 " << (A1 == A2) << endl;
        cout << " --> A3 == A2 " << (A3 == A2) << endl;
        cout << " --> A3 == A1 " << (A3 == A1) << endl;

        //

        cout << " --> A3 = A1 + A2;" << endl;

        A3 = A1 + A2;

        cout << " --> test if + overloaded operator works fine..." << endl;

        for (long i = 0; i < DIM_A; i++) {

            if (  A3.getElementAlpha(i) !=
                    A1.getElementAlpha(i) +
                    A2.getElementAlpha(i) ) {
                cout << "Error" << endl;
            }

        }

        cout << " --> A3 = A1 - A2;" << endl;

        A3 = A1 - A2;

        cout << " --> A3 = A1 * A2;" << endl;

        A3 = A1 * A2;

        cout << " --> A3 = A1 / A2;" << endl;

        A3 = A1 / A2;

        cout << " --> A3 += A1;" << endl;

        A3 += A1;

        cout << " --> A3 -= A1;" << endl;

        A3 -= A1;

        cout << " --> A3 *= A2;" << endl;

        A3 *= A2;

        cout << " --> A3 /= A2;" << endl;

        A3 /= A2;

        cout << " --> ++A3;" << endl;

        ++A3;

        cout << " --> A3++;" << endl;

        A3++;

        cout << " --> A3--;" << endl;

        A3--;

        cout << " --> --A3;" << endl;

        --A3;

        cout << " --> A3 = -A3;" << endl;

        A3 = -A3;

        //

        A1.destroy();
        A2.destroy();
        A3.destroy();

        //=====================//
        // derived1 class here //
        //=====================//

        cout << "===================" << endl;
        cout << "derived1 class here" << endl;
        cout << "===================" << endl;

        cout << " --> B1.setDim(DIM_A);" << endl;

        B1.setDim(DIM_A);

        //

        cout << " --> B1.reserve();" << endl;

        B1.reserve();

        //

        cout << " --> B1.build(10.0);" << endl;

        B1.build(10.0);

        //

        cout << " --> B2.setEqual(B1);" << endl;

        B2.setEqual(B1);

        B2 = B1;

        //

        cout << " --> B1.destroy();" << endl;

        B1.destroy();

        //

        cout << " --> B2.destroy();" << endl;

        B2.destroy();

        //

        cout << " --> B1.setDim(DIM_A);" << endl;

        B1.setDim(DIM_A);

        //

        cout << " --> B1.reserve();" << endl;

        B1.reserve();

        //

        cout << " --> B1.buildRandom(10);" << endl;

        B1.buildRandom(10);

        //

        cout << " --> B2.setEqual(B1);" << endl;

        B2.setEqual(B1);

        //

        cout << " --> B1.destroy();" << endl;

        B1.destroy();

        //

        cout << " --> B2.destroy();" << endl;

        B2.destroy();

        //

        cout << " --> B1.setDim(DIM_A);" << endl;

        B1.setDim(DIM_A);

        //

        cout << " --> B1.reserve();" << endl;

        B1.reserve();

        //

        cout << " --> B1.setElement(cos(static_cast<double>(i)), i);" << endl;

        for (long i = 0; i < DIM_A; i++) {
            B1.setElement(cos(static_cast<double>(i)), i);
        }

        //

        cout << " --> B2.setDim(DIM_A);" << endl;

        B2.setDim(DIM_A);

        //

        cout << " --> B2.reserve();" << endl;

        B2.reserve();

        //

        cout << " --> B2.setElement(cos(static_cast<double>(i)), i);" << endl;

        for (long i = 0; i < DIM_A; i++) {
            B2.setElement(cos(static_cast<double>(i)), i);
        }

        //

        cout << " --> B2.isEqual(B1) = " << B2.isEqual(B1) << endl;

        B2 = B1;

        cout << " --> B2.isEqual(B1) = " << B2.isEqual(B1) << endl;

        Derived1 B3(B1);

        cout << " --> B3.isEqual(B1) = " << B3.isEqual(B1) << endl;

        //

        cout << " --> B1 == B2 " << (B1 == B2) << endl;
        cout << " --> B3 == B2 " << (B3 == B2) << endl;
        cout << " --> B3 == B1 " << (B3 == B1) << endl;

        //

        cout << " --> B3 = B1 + B2;" << endl;

        B3 = B1 + B2;

        cout << " --> test if + overloaded operator works fine..." << endl;

        for (long i = 0; i < DIM_A; i++) {

            if (  B3.getElementAlpha1(i) !=
                    B1.getElementAlpha1(i) +
                    B2.getElementAlpha1(i) ) {
                cout << "Error" << endl;
            }

        }

        cout << " --> B3 = B1 - B2;" << endl;

        B3 = B1 - B2;

        cout << " --> B3 = B1 * B2;" << endl;

        B3 = B1 * B2;

        cout << " --> B3 = B1 / B2;" << endl;

        B3 = B1 / B2;

        cout << " --> B3 += B1;" << endl;

        B3 += B1;

        cout << " --> B3 -= B1;" << endl;

        B3 -= B1;

        cout << " --> B3 *= B2;" << endl;

        B3 *= B2;

        cout << " --> B3 /= B2;" << endl;

        B3 /= B2;

        cout << " --> ++B3;" << endl;

        ++B3;

        cout << " --> B3++;" << endl;

        B3++;

        cout << " --> B3--;" << endl;

        B3--;

        cout << " --> --B3;" << endl;

        --B3;

        cout << " --> B3 = -B3;" << endl;

        //

        B1.destroy();
        B2.destroy();
        B3.destroy();

        //=====================//
        // derived2 class here //
        //=====================//

        cout << "===================" << endl;
        cout << "derived2 class here" << endl;
        cout << "===================" << endl;

        cout << " --> C1.setDim(DIM_A);" << endl;

        C1.setDim(DIM_A);

        //

        cout << " --> C1.reserve();" << endl;

        C1.reserve();

        //

        cout << " --> C1.build(10.0);" << endl;

        C1.build(10.0);

        //

        cout << " --> C2.setEqual(C1);" << endl;

        C2.setEqual(C1);

        C2 = C1;

        //

        cout << " --> C1.destroy();" << endl;

        C1.destroy();

        //

        cout << " --> C2.destroy();" << endl;

        C2.destroy();

        //

        cout << " --> C1.setDim(DIM_A);" << endl;

        C1.setDim(DIM_A);

        //

        cout << " --> C1.reserve();" << endl;

        C1.reserve();

        //

        cout << " --> C1.buildRandom(10);" << endl;

        C1.buildRandom(10);

        //

        cout << " --> C2.setEqual(C1);" << endl;

        C2.setEqual(C1);

        //

        cout << " --> C1.destroy();" << endl;

        C1.destroy();

        //

        cout << " --> C2.destroy();" << endl;

        C2.destroy();

        //

        cout << " --> C1.setDim(DIM_A);" << endl;

        C1.setDim(DIM_A);

        //

        cout << " --> C1.reserve();" << endl;

        C1.reserve();

        //

        cout << " --> C1.setElement(cos(static_cast<double>(i)), i);" << endl;

        for (long i = 0; i < DIM_A; i++) {
            C1.setElement(cos(static_cast<double>(i)), i);
        }

        //

        cout << " --> C2.setDim(DIM_A);" << endl;

        C2.setDim(DIM_A);

        //

        cout << " --> C2.reserve();" << endl;

        C2.reserve();

        //

        cout << " --> C2.setElement(cos(static_cast<double>(i)), i);" << endl;

        for (long i = 0; i < DIM_A; i++) {
            C2.setElement(cos(static_cast<double>(i)), i);
        }

        //

        cout << " --> C2.isEqual(C1) = " << C2.isEqual(C1) << endl;

        C2 = C1;

        cout << " --> C2.isEqual(C1) = " << C2.isEqual(C1) << endl;

        Derived2 C3(C1);

        cout << " --> C3.isEqual(C1) = " << C3.isEqual(C1) << endl;

        //

        cout << " --> C1 == C2 " << (C1 == C2) << endl;
        cout << " --> C3 == C2 " << (C3 == C2) << endl;
        cout << " --> C3 == C1 " << (C3 == C1) << endl;

        //

        cout << " --> C3 = C1 + C2;" << endl;

        C3 = C1 + C2;

        cout << " --> test if + overloaded operator works fine..." << endl;

        for (long i = 0; i < DIM_A; i++) {

            if (  C3.getElementAlpha1(i) !=
                    C1.getElementAlpha1(i) +
                    C2.getElementAlpha1(i) ) {
                cout << "Error" << endl;
            }

        }

        cout << " --> C3 = C1 - C2;" << endl;

        C3 = C1 - C2;

        cout << " --> C3 = C1 * C2;" << endl;

        C3 = C1 * C2;

        cout << " --> C3 = C1 / C2;" << endl;

        C3 = C1 / C2;

        cout << " --> C3 += C1;" << endl;

        C3 += C1;

        cout << " --> C3 -= C1;" << endl;

        C3 -= C1;

        cout << " --> C3 *= C2;" << endl;

        C3 *= C2;

        cout << " --> C3 /= C2;" << endl;

        C3 /= C2;

        cout << " --> ++C3;" << endl;

        ++C3;

        cout << " --> C3++;" << endl;

        C3++;

        cout << " --> C3--;" << endl;

        C3--;

        cout << " --> --C3;" << endl;

        --C3;

        cout << " --> C3 = -C3;" << endl;

        //

        C1.destroy();
        C2.destroy();
        C3.destroy();
    }

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
