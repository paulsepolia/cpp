
//===========================//
// derived2 class definition //
//===========================//

#include <cstdlib>
#include <cmath>
#include "base.h"
#include "derived1.h"
#include "derived2.h"

using std::rand;
using std::srand;

namespace pgg {
// default constructor

Derived2::Derived2() : Derived1(), dim2(0), a2(NULL), b2(NULL) {}

// virtual destructor

Derived2::~Derived2()
{
    delete [] a2;
    delete [] b2;

    a2 = NULL;
    b2 = NULL;
}

// member function

void Derived2::setDim(long theDim)
{
    Derived1::setDim(theDim);
    dim2 = theDim;
}

// member function

long Derived2::getDim() const
{
    return dim2;
}

// member function

void Derived2::reserve()
{
    Derived1::reserve();
    a2 = new double [dim2];
    b2 = new double [dim2];
}

// member function

void Derived2::build(double theElem)
{
    Derived1::build(theElem);

    for (long i = 0; i < dim2; i++) {
        a2[i] = theElem;
        b2[i] = theElem;
    }
}

// member function

void Derived2::buildRandom(int theSeed)
{
    Derived1::buildRandom(theSeed);

    srand(theSeed);

    double theElem = static_cast<double>(rand());

    for (long i = 0; i < dim2; i++) {
        a2[i] = theElem;
        b2[i] = theElem;
    }
}

// member function

void Derived2::destroy()
{
    Derived1::destroy();

    delete [] a2;
    delete [] b2;

    a2 = NULL;
    b2 = NULL;

    dim2 = 0;
}

// member function

void Derived2::setEqual(const Derived2& objA)
{
    Derived1::setEqual(objA);

    this->dim2 = objA.dim2;
    this->a2 = new double [objA.dim2];
    this->b2 = new double [objA.dim2];

    for (long i = 0; i < objA.dim2; i++) {
        this->a2[i] = objA.a2[i];
        this->b2[i] = objA.b2[i];
    }
}

// member function

double Derived2::getElementAlpha(long index) const
{
    return Base::getElementAlpha(index);
}

// member function

double Derived2::getElementBeta(long index) const
{
    return Base::getElementBeta(index);
}

// member function

double Derived2::getElementAlpha1(long index) const
{
    return Derived1::getElementAlpha1(index);
}

// member function

double Derived2::getElementBeta1(long index) const
{
    return Derived1::getElementBeta1(index);
}

// member function

double Derived2::getElementAlpha2(long index) const
{
    return a2[index];
}

// member function

double Derived2::getElementBeta2(long index) const
{
    return b2[index];
}

// member function

void Derived2::setElement(double theElem, long index)
{
    Derived1::setElement(theElem, index);

    a2[index] = theElem;
    b2[index] = -theElem;
}

// member function

bool Derived2::isEqual(const Derived2& objA) const
{
    if (!Derived1::isEqual(objA)) {
        return false;
    }

    if (objA.dim2 != this->dim2) {
        return false;
    }

    for (long i = 0; i != objA.dim2; i++) {
        if (objA.a2[i] != this->a2[i]) {
            return false;
        }

        if (objA.b2[i] != this->b2[i]) {
            return false;
        }
    }

    return true;
}

// = operator overload

Derived2& Derived2::operator = (const Derived2& objA)
{
    this->setEqual(objA);

    return *this;
}


// copy constructor

Derived2::Derived2(const Derived2& objA) : Derived1(objA), dim2(objA.dim2), a2(NULL), b2(NULL)
{
    a2 = new double [dim2];
    b2 = new double [dim2];

    for (long i = 0; i < dim2; i++) {
        a2[i] = objA.a2[i];
        b2[i] = objA.b2[i];
    }
}

// friend function

bool operator == (const Derived2& objA1, const Derived2& objA2)
{
    return (objA1.isEqual(objA2));
}

// friend function
// operator overload +

const Derived2 operator + (const Derived2& objA1, const Derived2& objA2)
{
    Derived2 objA3;

    objA3.setDim(objA1.dim2);

    objA3.reserve();

    for (long i = 0; i < objA1.dim2; i++) {
        objA3.setElement(objA1.a2[i] + objA2.a2[i], i);
    }

    return Derived2(objA3);
}

// friend function
// operator overload -

const Derived2 operator - (const Derived2& objA1, const Derived2& objA2)
{
    Derived2 objA3;

    objA3.setDim(objA1.dim2);

    objA3.reserve();

    for (long i = 0; i < objA1.dim2; i++) {
        objA3.setElement(objA1.a2[i] - objA2.a2[i], i);
    }

    return Derived2(objA3);
}

// friend function
// operator overload *

const Derived2 operator * (const Derived2& objA1, const Derived2& objA2)
{
    Derived2 objA3;

    objA3.setDim(objA1.dim2);

    objA3.reserve();

    for (long i = 0; i < objA1.dim2; i++) {
        objA3.setElement(objA1.a2[i] * objA2.a2[i], i);
    }

    return Derived2(objA3);
}

// friend function
// operator overload /

const Derived2 operator / (const Derived2& objA1, const Derived2& objA2)
{
    Derived2 objA3;

    objA3.setDim(objA1.dim2);

    objA3.reserve();

    for (long i = 0; i < objA1.dim2; i++) {
        objA3.setElement(objA1.a2[i] / objA2.a2[i], i);
    }

    return Derived2(objA3);
}

// friend function
// operator overload +=

const Derived2& operator += (Derived2& objA1, const Derived2& objA2)
{
    objA1 = objA1 + objA2;

    return objA1;
}

// friend function
// operator overload -=

const Derived2& operator -= (Derived2& objA1, const Derived2& objA2)
{
    objA1 = objA1 - objA2;

    return objA1;
}

// friend function
// operator overload *=

const Derived2& operator *= (Derived2& objA1, const Derived2& objA2)
{
    objA1 = objA1 * objA2;

    return objA1;
}

// friend function
// operator overload /=

const Derived2& operator /= (Derived2& objA1, const Derived2& objA2)
{
    objA1 = objA1 / objA2;

    return objA1;
}

// friend function
// unary operator overload -

const Derived2 operator - (const Derived2& objA1)
{
    Derived2 objA3;

    objA3.setDim(objA1.dim2);

    objA3.reserve();

    for (long i = 0; i < objA1.dim2; i++) {
        objA3.setElement(-objA1.a2[i], i);
    }

    return Derived2(objA3);
}

// friend function
// operator overload ++ (prefix)

const Derived2 operator ++ (Derived2& objA1)
{
    Derived2 objA3;

    objA3.setDim(objA1.dim2);

    objA3.reserve();

    for (long i = 0; i < objA1.dim2; i++) {
        objA3.setElement(++objA1.a2[i], i);
    }

    return Derived2(objA3);
}

// friend function
// operator overload ++ (postfix)

const Derived2 operator ++ (Derived2& objA1, int)
{
    Derived2 objA3;

    objA3.setDim(objA1.dim2);

    objA3.reserve();

    for (long i = 0; i < objA1.dim2; i++) {
        objA3.setElement(++objA1.a2[i], i);
    }

    return Derived2(objA3);
}

// friend function
// operator overload -- (prefix)

const Derived2 operator -- (Derived2& objA1)
{
    Derived2 objA3;

    objA3.setDim(objA1.dim2);

    objA3.reserve();

    for (long i = 0; i < objA1.dim2; i++) {
        objA3.setElement(--objA1.a2[i], i);
    }

    return Derived2(objA3);
}

// friend function
// operator overload -- (postfix)

const Derived2 operator -- (Derived2& objA1, int)
{
    Derived2 objA3;

    objA3.setDim(objA1.dim2);

    objA3.reserve();

    for (long i = 0; i < objA1.dim2; i++) {
        objA3.setElement(--objA1.a2[i], i);
    }

    return Derived2(objA3);
}

} // pgg

//======//
// FINI //
//======//

