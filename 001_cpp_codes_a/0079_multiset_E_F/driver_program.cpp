//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/10/30              //
// Functions: set                //
// Algorithms:                   //
//===============================//

#include <iostream>
#include <set>

using namespace std;

// main

int main ()
{
    // variables declaration

    long int I_MAX = 2 * static_cast<long int>(10000000);
    multiset<double, less<double> > S1;
    multiset<double, less<double> > S2;
    long int i;
    long int j;
    const long int J_MAX = 100;

    for (j = 0; j < J_MAX; j++) {
        cout << "----------------------------------------------------------> " << j + 1 << endl;

        // build the 1st set

        cout << " 1 --> Build the S1 set" << endl;

        for (i = 0; i < I_MAX; i++) {
            S1.insert(static_cast<double>(i));
        }

        // build the 2nd set

        cout << " 2 --> Build the S2 set" << endl;

        for (i = 0; i < I_MAX; i++) {
            S2.insert(static_cast<double>(I_MAX-1-i));
        }

        // compare the sets

        cout << " 3 --> Compare the sets, should be equal: ";

        if (S1 == S2) {
            cout << " Equal sets" << endl;
        }

        // free RAM

        S1.clear();
        S2.clear();
    }

    return 0;
}

// end
