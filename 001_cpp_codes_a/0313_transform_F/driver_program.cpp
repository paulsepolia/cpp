
//=================//
// ptr_fun example //
//=================//

#include <iostream>
#include <functional>
#include <algorithm>
#include <cstdlib>
#include <numeric>
#include <iomanip>
#include <cmath>
#include <string>
#include <vector>
#include <sstream>

using std::cout;
using std::cin;
using std::endl;
using std::ptr_fun;
using std::transform;
using std::accumulate;
using std::fixed;
using std::showpoint;
using std::showpos;
using std::setprecision;
using std::pow;
using std::string;
using std::vector;
using std::to_string;
using std::stringstream;

// function object

class s_to_d_CL {
public:

    double operator() (const string & s)
    {
        double d;
        stringstream ss;

        ss << s;
        ss >> d;

        return d;
    }
};

// function object

class d_to_d_CL {
public:

    double operator() (const double& s)
    {
        return s;
    }
};


// the main function

int main()
{
    // local variables and parameters

    const long long DIM_MAX = static_cast<long long>(pow(10.0, 7.0));
    const long long K_MAX = static_cast<long long>(pow(10.0, 5.0));
    double sum1 = 0;
    double sum2 = 0;


    // set the output format

    cout << fixed;
    cout << setprecision(20);
    cout << showpos;
    cout << showpoint;

    // the for testing loop

    for (long long k = 0; k != K_MAX; k++) {
        // the counter

        cout << "------------------------------------------------>> " << k << endl;

        // the containers

        vector<string> * vs1 = new vector<string>;
        double * d1_array = new double [DIM_MAX];

        // build the string

        cout << " --> build the string" << endl;

        for (long long i = 0; i < DIM_MAX; i++) {
            vs1->push_back(to_string(cos(static_cast<double>(i))));
        }

        // apply the function object

        cout << " --> transform from string to doubles" << endl;

        transform(vs1->begin(), vs1->end(), &d1_array[0], s_to_d_CL());

        // verify that everthing is okay

        cout << " --> sum here" << endl;

        sum1 = accumulate(&d1_array[0], &d1_array[DIM_MAX], 0.0);

        cout << " --> sum1 = " << sum1 << endl;

        // delete the containers

        cout << " --> delete the array of doubles" << endl;

        delete [] d1_array;

        cout << " --> delete the vector of strings" << endl;

        delete vs1;

        //
        // with array only and doubles
        //

        // the containers

        double * d2_array = new double [DIM_MAX];
        double * d3_array = new double [DIM_MAX];

        // build the arrays

        cout << " --> build the array" << endl;

        for (long long i = 0; i < DIM_MAX; i++) {
            d2_array[i] = cos(static_cast<double>(i));
        }

        // apply the function object

        cout << " --> transform from doubles to doubles" << endl;

        transform(&d2_array[0], &d2_array[DIM_MAX], &d3_array[0], d_to_d_CL());

        // verify that everything is okay

        cout << " --> sum here" << endl;

        sum2 = accumulate(&d3_array[0], &d3_array[DIM_MAX], 0.0);

        cout << " --> sum2 = " << sum2 << endl;

        cout << " --> abs(sum1-sum2) = " << abs(sum1-sum2) << endl;

        // delete the containers

        cout << " --> delete the array of doubles" << endl;

        delete [] d2_array;

        cout << " --> delete the array of doubles" << endl;

        delete [] d3_array;
    }

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
