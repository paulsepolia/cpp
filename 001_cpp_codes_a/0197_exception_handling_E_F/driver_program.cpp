
//==============================//
// User defined exception class //
//==============================//

#include <iostream>
#include <string>

using std::string;
using std::cout;
using std::cin;
using std::endl;

// a class

class myException {
public:
    myException() : message ("Something is wrong!") {}
    explicit myException(string str) : message (str) {}
    string what()
    {
        return message;
    }

private:
    string message;
};

// function declarations

void functionA() throw (myException);
void functionB() throw (myException);
void functionC() throw (myException);

// the main function

int main()
{
    try {
        functionA();
    } catch ( myException me ) {
        cout << me.what() << " Caught in main." << endl;
    }

    int sentinel;
    cin >> sentinel;

    return 0;
}

// functions definition

// 1

void functionA() throw (myException)
{
    functionB();
}

// 2

void functionB() throw (myException)
{
    functionC();
}

// 3

void functionC() throw (myException)
{
    throw myException("Exception generated in function C.");
}

//======//
// FINI //
//======//
