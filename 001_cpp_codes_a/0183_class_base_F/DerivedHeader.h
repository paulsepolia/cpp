
//=================//
// DerivedHeader.h //
//=================//

#ifndef DERIVEDHEADER_H
#define DERIVEDHEADER_H

#include <string>
#include "BaseHeader.h"
#include "BaseDefinition.h"

using pgg::base;
using std::string;

namespace pgg {
class derived : public base {
public:

    derived();
    derived(int, double, string);
    string getTicketSeason() const;
    void setTicketSeason(string);
    void resetTicket();

private:

    string ticketSeason;
};
} // pgg

#endif // DERIVEDHEADER_H

//======//
// FINI //
//======//
