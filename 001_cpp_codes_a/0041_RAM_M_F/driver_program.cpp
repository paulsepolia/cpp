//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/10/23              //
// Functions: RAM                //
//===============================//

#include <iostream>

using namespace std;

// main function
int main()
{
    int *p;
    for(;;) {
        p = new int;
        delete p;
    }

    return 0;
}
// end
