//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/10/17              //
// Functions: Class              //
//===============================//

#include <iostream>
#include "illustrateHeader.h"

using std::cout;
using std::endl;

// class definition

// 1.

int illustrate::count = 0;
int illustrate::y = 0;

// 2.

void illustrate::print() const
{
    cout << " x = " << x << ", y = " << y
         << ", count = " << count << endl;
}

// 2.

void illustrate::setX(int a)
{
    x = a;
}

// 3.

void illustrate::incrementY()
{
    y++;
}

// 4.

illustrate::illustrate(int a)
{
    x = a;
}

//======//
// FINI //
//======//
