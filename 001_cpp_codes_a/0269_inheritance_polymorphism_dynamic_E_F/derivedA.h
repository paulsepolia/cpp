
//============================//
// DerivedA class declaration //
//============================//

#ifndef DERIVED_A_H
#define DERIVED_A_H

#include "base.h"

namespace pgg {
class DerivedA: public Base {
public:

    using Base::operator=;

    // constructors

    DerivedA();
    DerivedA(double, long);
    DerivedA(const DerivedA&);

    // destructor

    virtual ~DerivedA();

    // member functions

    void createBackup();
    void getBackup();
    void destroyBackup();
    void destroy();

private:

    long dimA;
    double * vecA = nullptr;
};

} // pgg

#endif // DERIVED_A_H

//======//
// FINI //
//======//
