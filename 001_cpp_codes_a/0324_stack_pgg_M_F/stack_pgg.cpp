
//=======================================//
// implementation of the stack_pgg class //
//=======================================//

#include "stack_pgg.h"
#include <algorithm>

using std::reverse;

// default constructor

template <typename T, typename CONT>
stack_pgg<T, CONT>::stack_pgg() {}

// destructor

template <typename T, typename CONT>
stack_pgg<T, CONT>::~stack_pgg() {}

// assignment operator

template <typename T, typename CONT>
template <typename T2, typename CONT2>
stack_pgg<T, CONT> & stack_pgg<T, CONT>::operator = (const stack_pgg<T2, CONT2> & s1)
{
    if ((void*) this == (void*) & s1) { // avoid to assignment to itself
        return *this;
    }

    stack_pgg<T2, CONT2> tmp(s1);

    elems.clear();

    while (!tmp.empty()) {
        elems.push_back(tmp.top());
        tmp.pop();
    }

    reverse(elems.begin(), elems.end());

    return *this;
}

// copy constructor

template <typename T, typename CONT>
template <typename T2, typename CONT2>
stack_pgg<T, CONT>::stack_pgg(const stack_pgg<T2, CONT2> & s1)
{
    *this = s1; // the assgnment operator is already defined
}

// empty --> public member function

template <typename T, typename CONT>
bool stack_pgg<T, CONT>::empty() const
{
    return elems.empty();
}

// size --> public member function

template <typename T, typename CONT>
size_t stack_pgg<T, CONT>::size() const
{
    return elems.size();
}

// top --> public member function

template <typename T, typename CONT>
T & stack_pgg<T, CONT>::top()
{
    return elems.back();
}

// push --> public member function

template <typename T, typename CONT>
void stack_pgg<T, CONT>::push(const T & arg)
{
    elems.push_back(arg);
}

// pop --> public member function

template <typename T, typename CONT>
void stack_pgg<T, CONT>::pop()
{
    elems.pop_back();
}

// swap --> public member function

template <typename T, typename CONT>
template <typename T2, typename CONT2>
void stack_pgg<T, CONT>::swap(stack_pgg<T2, CONT2> & s1)
{
    stack_pgg<T2, CONT2> stmp1;

    stmp1 = s1;
    s1 = *this;
    *this = stmp1;
}

// emplace --> public member function

template <typename T, typename CONT>
void stack_pgg<T, CONT>::emplace(const T & arg)
{
    elems.emplace_back(arg);
}


//======//
// FINI //
//======//

