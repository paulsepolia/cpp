//=======================//
// classes and           //
// member initialization //
//=======================//

#include <iostream>

// a class

class Circle {
public:

    Circle(double r) : radius (r) // a constructor
    {}                            // the private variable 'radius' takes the value of r

    double area()   // a public member function
    {
        return (radius*radius*3.14159265);
    }

private:

    double radius; // a private variable
};

// a class

class Cylinder {
public:

    Cylinder(double r, double h) : base (r), height (h) // a constructor
    {}                                                  // 'base' and 'height' private variables
    // take the values r and h
    // in the case of base(r) the
    // constructor of the 'Circle' class is being called
    double volume()
    {
        return (base.area() * height);
    }

private:
    Circle base;
    double height;
};

// the main function

int main()
{
    Cylinder foo (10, 20);
    std::cout << "foo's volume: " << foo.volume() << std::endl;

    int sentinel;
    std::cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
