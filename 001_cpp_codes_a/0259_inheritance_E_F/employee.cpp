
//======================================//
// implementation of the class Employee //
//======================================//

#include <string>
#include <cstdlib>
#include <iostream>
#include "employee.h"

using std::string;
using std::cout;
using std::cin;
using std::endl;
using std::exit;

namespace pgg {
// default constructor

Employee::Employee() : name ("No name yet"), ssn ("No number yet"), netPay(0) {}

// constructor with two arguments

Employee::Employee(string theName, string theNumber) : name (theName), ssn (theNumber), netPay(0) {}

// member function

string Employee::getName() const
{
    return name;
}

// member function

string Employee::getSsn() const
{
    return ssn;
}

// member function

double Employee::getNetPay() const
{
    return netPay;
}

// member function

void Employee::setName(string newName)
{
    name = newName;
}

// member function

void Employee::setSsn(string newSsn)
{
    ssn = newSsn;
}

// member function

void Employee::setNetPay(double newNetPay)
{
    netPay = newNetPay;
}

// member function

void Employee::printCheck() const
{
    cout << "ERROR: printCheck FUNCTION CALLED FOR AN" << endl;
    cout << "UNDIFFERENTIATED EMPLOYEE." << endl;
    cout << "Enter an interger to abort normaly the program:";
    int sentinel;
    cin >> sentinel;
    exit(1);
}

// virtual destructor

Employee::~Employee() {};

} // pgg

//======//
// FINI //
//======//
