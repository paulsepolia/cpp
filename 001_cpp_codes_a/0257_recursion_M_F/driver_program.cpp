
#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <cmath>

using std::cout;
using std::cin;
using std::endl;
using std::exit;
using std::setprecision;
using std::fixed;
using std::showpos;
using std::showpoint;
using std::ios;
using std::pow;
using std::ios_base;

double power(double, int);

// the main function

int main()
{
    // get the original settings

    int precisionSetting = cout.precision();
    ios_base::fmtflags flagSettings = cout.flags();

    // adjust the output format

    cout.setf(ios::fixed);
    cout.precision(10);
    cout.setf(ios::showpos);
    cout.setf(ios::showpoint);

    // program

    const double TR = 3.000000;

    for (int n = 0; n < 100; n++) {
        cout << "3 to the power " << n
             << " is " << power(TR, n) << endl;
        cout << "3 to the power " << n
             << " is " << pow(TR, n) << endl;
        cout << " diff --> " << power(TR, n) - pow(TR, n) << endl;
    }

    // restore the settings

    cout.precision(precisionSetting);
    cout.flags(flagSettings);

    // sentineling

    int sentinel;
    cin >> sentinel;
}

// the function

double power(double x, int n)
{
    if (n < 0) {
        cout << "Illegal agrument to power. Enter an integer to exit:";
        int sentinel;
        cin >> sentinel;
        exit(1);
    }

    if (n > 0) {
        return (power(x, n-1) * x);
    } else { // n == 0
        return (1.0);
    }
}

//======//
// FINI //
//======//
