
//=======================================================================//
// operators overload as member functions                                //
// and some other as external functions                                  //
// Also "conversion operators"                                           //
//=======================================================================//

//===================//
// --> code --> 0140 //
//===================//

// status --> FINISHED

//==============================================================================
//
// --> the main points here are:
//
// --> 1 --> overloading the assignment operator = can be done by two ways.
//           Either to return by value or to return by const reference.
//	     Returning by values calls the copy constructor and then the destructor.
//	     Returning by const reference does not call any constructor.
//	     So the later is much faster.
//
// --> 2 --> overloading the binary operator +, -, *, /,
//	     can be done via public member functions.
//	     It is needed the declaration of a temporary object
//	     and so the related to it call to the constructor and then destructor
//
// --> 3 --> overloading the binary operators as member functions
//	     cause ASSYMETRIC definitions since e.g. only b = a + 5 can work
//           but NOT b = 5 + a;
//	     SO IT IS PREFERED TO OVERLOAD BINARY OPERATORS AS FRIEND FUNCTIONS
//
// --> 4 --> some operators like '<<' and '>>' can be overloaded ONLY
//	     as FRIEND FUNCTIONS or AS EXTERNAL FUNCTIONS (what I did here)
//
//==============================================================================

#include <iostream>
#include <iomanip>

using std::cout;
using std::cin;
using std::endl;
using std::boolalpha;
using std::ostream;
using std::istream;

// class --> CVector

class CVector {
private:

    // variables

    int x;
    int y;

public:

    // constructor with zero arguments

    CVector();

    // constructor with two arguments

    CVector (int, int);

    // destructor

    ~CVector();

    // get_x

    int get_x() const;

    // get_y

    int get_y() const;

    // copy constructor

    CVector(const CVector&);

    // set_x

    void set_x(int);

    // set_y

    void set_y(int);

    // operator =

    CVector operator = (const CVector &);

    // operator +

    CVector operator + (const CVector &) const;

    // operator -

    CVector operator - (const CVector &) const;

    // operator *

    CVector operator * (const CVector &) const;

    // operator /

    CVector operator / (const CVector &) const;

    // operator ==

    bool operator == (const CVector &) const;

    // operator !=

    bool operator != (const CVector &) const;

    // operator +=

    CVector operator +=(const CVector &);

    // operator -=

    CVector operator -=(const CVector &);

    // operator *=

    CVector operator *=(const CVector &);

    // operator /=

    CVector operator /=(const CVector &);

    // operator ++ (prefix)

    CVector operator ++();

    // operator ++ (postfix)

    CVector operator ++(int);

    // operator -- (prefix)

    CVector operator --();

    // operator -- (postfix)

    CVector operator --(int);

    // operator = for doubles and ints

    template <class T>
    CVector operator = (T);

    // operator + for doubles and ints

    template <class T>
    CVector operator + (T) const;

    // operator - for doubles and ints

    template <class T>
    CVector operator - (T) const;

    // operator * for doubles and ints

    template <class T>
    CVector operator * (T) const;

    // operator / for doubles and ints

    template <class T>
    CVector operator / (T) const;

    // consversion operator to T

    template <class T>
    operator T () const;

};

//
//
// --> member functions definitions
//
//

// constructor with zero arguments

CVector::CVector () : x(0), y(0)
{
    cout << " --> constructor --> zero" << endl;
}

// constructor with two arguments

CVector::CVector (int a, int b) : x(a), y(b)
{
    cout << " --> constructor --> two arguments" << endl;
}

// destructor

CVector::~CVector()
{
    cout << " --> destructor --> ~CVector" << endl;
}

// get_x

int CVector::get_x() const
{
    return this->x;
}

// get_y

int CVector::get_y() const
{
    return this->y;
}

// set_x

void CVector::set_x(int x)
{
    this->x = x;
}

// set_y

void CVector::set_y(int y)
{
    this->y = y;
}

// copy constructor

CVector::CVector(const CVector & a)
{
    cout << " --> copy constructor --> CVector" << endl;

    this->x = a.get_x();
    this->y = a.get_y();
}

// operator =

CVector CVector::operator = (const CVector & a)
{
    cout << " --> CVector operator = (const CVector &)" << endl;

    this->x = a.get_x();
    this->y = a.get_y();

    return *this;
}

// operator +

CVector CVector::operator + (const CVector & param) const
{
    CVector temp;

    int xLoc = this->get_x() + param.get_x();
    int yLoc = this->get_y() + param.get_y();

    temp.set_x(xLoc);
    temp.set_y(yLoc);

    return temp;
}

// operator -

CVector CVector::operator - (const CVector & param) const
{
    CVector temp;

    int xLoc = this->get_x() - param.get_x();
    int yLoc = this->get_y() - param.get_y();

    temp.set_x(xLoc);
    temp.set_y(yLoc);

    return temp;
}

// operator *

CVector CVector::operator * (const CVector & param) const
{
    CVector temp;

    int xLoc = this->get_x() * param.get_x();
    int yLoc = this->get_y() * param.get_y();

    temp.set_x(xLoc);
    temp.set_y(yLoc);

    return temp;
}

// operator /

CVector CVector::operator / (const CVector & param) const
{
    CVector temp;

    int xLoc = this->get_x() / param.get_x();
    int yLoc = this->get_y() / param.get_y();

    temp.set_x(xLoc);
    temp.set_y(yLoc);

    return temp;
}

// operator ==

bool CVector::operator == (const CVector & param) const
{
    bool xb = (this->get_x() == param.get_x());
    bool yb = (this->get_y() == param.get_y());

    return (xb && yb);
}

// operator !=

bool CVector::operator != (const CVector & param) const
{
    return !(*this == param);
}

// operator +=

CVector CVector::operator +=(const CVector & param)
{
    int xL = this->get_x() + param.get_x();
    int yL = this->get_y() + param.get_y();

    this->set_x(xL);
    this->set_y(yL);

    return *this;
}

// operator -=

CVector CVector::operator -=(const CVector & param)
{
    int xL = this->get_x() - param.get_x();
    int yL = this->get_y() - param.get_y();

    this->set_x(xL);
    this->set_y(yL);

    return *this;
}

// operator *=

CVector CVector::operator *=(const CVector & param)
{
    int xL = this->get_x() * param.get_x();
    int yL = this->get_y() * param.get_y();

    this->set_x(xL);
    this->set_y(yL);

    return *this;
}

// operator /=

CVector CVector::operator /=(const CVector & param)
{
    int xL = this->get_x() / param.get_x();
    int yL = this->get_y() / param.get_y();

    this->set_x(xL);
    this->set_y(yL);

    return *this;
}

// operator ++ (prefix)

CVector CVector::operator ++ ()
{
    int xL = this->get_x();
    int yL = this->get_y();

    this->set_x(++xL);
    this->set_y(++yL);

    return *this;
}

// operator ++ (postfix)

CVector CVector::operator ++ (int)
{
    return ++(*this);
}

// operator -- (prefix)

CVector CVector::operator --()
{
    int xL = this->get_x();
    int yL = this->get_y();

    this->set_x(--xL);
    this->set_y(--yL);

    return *this;
}

// operator -- (postfix)

CVector CVector::operator --(int)
{
    return --(*this);
}

// operator = for ints and doubles

template <class T>
CVector CVector::operator = (T a)
{
    int xL = a;
    int yL = a;

    this->set_x(xL);
    this->set_y(yL);

    return *this;
}

// operator + for doubles and int

template <class T>
CVector CVector::operator + (T a) const
{
    int xL = this->get_x() + a;
    int yL = this->get_x() + a;

    CVector temp;

    temp.set_x(xL);
    temp.set_y(yL);

    return temp;
}

// operator - for doubles and int

template <class T>
CVector CVector::operator - (T a) const
{
    int xL = this->get_x() - a;
    int yL = this->get_x() - a;

    CVector temp;

    temp.set_x(xL);
    temp.set_y(yL);

    return temp;
}

// operator * for doubles and int

template <class T>
CVector CVector::operator * (T a) const
{
    int xL = this->get_x() * a;
    int yL = this->get_x() * a;

    CVector temp;

    temp.set_x(xL);
    temp.set_y(yL);

    return temp;
}

// operator / for doubles and int

template <class T>
CVector CVector::operator / (T a) const
{
    int xL = this->get_x() / a;
    int yL = this->get_x() / a;

    CVector temp;

    temp.set_x(xL);
    temp.set_y(yL);

    return temp;
}

// operator <<

// IS NOT MEMBER FUNCTION
// IS NOT FRIEND FUNCTION

ostream & operator << (ostream & outStream, const CVector & a)
{
    outStream << "{ "  << a.get_x()
              << " , " << a.get_y() << " }";

    return outStream;
}


// operator >>

// IS NOT MEMBER FUNCTION
// IS NOT FRIEND FUNCTION

istream & operator >> (istream & inStream, CVector & a)
{
    int tmp;

    inStream >> tmp;

    a = tmp;

    return inStream;
}

// operator '+' --> as external function
//		    to make the operator symmetrix

template <class T>
CVector operator + (T a, const CVector & b)
{
    return (b + a);
}

// operator '-' --> as external function
//		    to make the operator symmetric

template <class T>
CVector operator - (T a, const CVector & b)
{
    return (b - a);
}

// operator '*' --> as external function
// 	            to make the operator symmetric

template <class T>
CVector operator * (T a, const CVector & b)
{
    return (b * a);
}

// conversion operator definition

template <class T>
CVector::operator T () const
{
    return static_cast<T>(this->get_x());
}

//
//
// the main function
//
//

int main()
{

    // adjust the output

    cout << boolalpha;

    cout << " --> 1" << endl;

    CVector foo (3, 1); // using the non-default constructor

    cout << " --> 2" << endl;

    CVector bar (1, 2); // using the non default constructor

    cout << " --> 3" << endl;

    CVector result;     // using the default constructor

    // '+'

    cout << " --> 4" << endl;

    result = foo + bar; // HOW IT WORKD HERE:
    // 1 --> calls the operator +
    // 2 --> calls the constructor for 'temp' object
    // 3 --> executes the rest of the body of the operator + function
    // 4 --> calls the assignment operator
    // 5 --> calls the destructor for the 'temp' object

    cout << " --> 5" << endl;

    cout << " --> '+' " << endl;

    cout << foo.get_x()    << ',' << foo.get_y()    << endl;
    cout << bar.get_x()    << ',' << bar.get_y()    << endl;
    cout << result.get_x() << ',' << result.get_y() << endl;

    // '-'

    cout << " --> 6" << endl;

    result = foo - bar; // HOW IT WORKD HERE:
    // 1 --> calls the operator -
    // 2 --> calls the constructor for 'temp' object
    // 3 --> executes the rest of the body of the operator - function
    // 4 --> calls the assignment operator
    // 5 --> calls the destructor for the 'temp' object

    cout << " --> 7" << endl;

    cout << " --> '-' " << endl;

    cout << foo.get_x()    << ',' << foo.get_y()    << endl;
    cout << bar.get_x()    << ',' << bar.get_y()    << endl;
    cout << result.get_x() << ',' << result.get_y() << endl;

    // '*'

    cout << " --> 8" << endl;

    result = foo * bar; // HOW IT WORKD HERE:
    // 1 --> calls the operator *
    // 2 --> calls the constructor for 'temp' object
    // 3 --> executes the rest of the body of the operator * function
    // 4 --> calls the assignment operator
    // 5 --> calls the destructor for the 'temp' object

    cout << " --> 9" << endl;

    cout << " --> '*' " << endl;

    cout << foo.get_x()    << ',' << foo.get_y()    << endl;
    cout << bar.get_x()    << ',' << bar.get_y()    << endl;
    cout << result.get_x() << ',' << result.get_y() << endl;

    // '/'

    cout << " --> 10" << endl;

    result = foo / bar; // HOW IT WORKD HERE:
    // 1 --> calls the operator /
    // 2 --> calls the constructor for 'temp' object
    // 3 --> executes the rest of the body of the operator / function
    // 4 --> calls the assignment operator
    // 5 --> calls the destructor for the 'temp' object

    cout << " --> 11" << endl;

    cout << " --> '/' " << endl;

    cout << foo.get_x()    << ',' << foo.get_y()    << endl;
    cout << bar.get_x()    << ',' << bar.get_y()    << endl;
    cout << result.get_x() << ',' << result.get_y() << endl;

    // '+'

    cout << " --> 12" << endl;

    result = foo.operator + (bar); // HOW IT WORKD HERE:
    // 1 --> calls the operator +
    // 2 --> calls the constructor for 'temp' object
    // 3 --> executes the rest of the body of the operator + function
    // 4 --> calls the assignment operator
    // 5 --> calls the destructor for the 'temp' object

    cout << " --> 13" << endl;

    cout << " --> '+' " << endl;

    cout << foo.get_x()    << ',' << foo.get_y()    << endl;
    cout << bar.get_x()    << ',' << bar.get_y()    << endl;
    cout << result.get_x() << ',' << result.get_y() << endl;

    // '-'

    cout << " --> 14" << endl;

    result = foo.operator - (bar); // HOW IT WORKD HERE:
    // 1 --> calls the operator -
    // 2 --> calls the constructor for 'temp' object
    // 3 --> executes the rest of the body of the operator - function
    // 4 --> calls the assignment operator
    // 5 --> calls the destructor for the 'temp' object

    cout << " --> 15" << endl;

    cout << " --> '-' " << endl;

    cout << foo.get_x()    << ',' << foo.get_y()    << endl;
    cout << bar.get_x()    << ',' << bar.get_y()    << endl;
    cout << result.get_x() << ',' << result.get_y() << endl;

    // '*'

    cout << " --> 16" << endl;

    result = foo.operator * (bar); // HOW IT WORKD HERE:
    // 1 --> calls the operator *
    // 2 --> calls the constructor for 'temp' object
    // 3 --> executes the rest of the body of the operator * function
    // 4 --> calls the assignment operator
    // 5 --> calls the destructor for the 'temp' object

    cout << " --> 17" << endl;

    cout << " --> '*' " << endl;

    cout << foo.get_x()    << ',' << foo.get_y()    << endl;
    cout << bar.get_x()    << ',' << bar.get_y()    << endl;
    cout << result.get_x() << ',' << result.get_y() << endl;

    // '/'

    cout << " --> 18" << endl;

    result = foo.operator / (bar); // HOW IT WORKD HERE:
    // 1 --> calls the operator /
    // 2 --> calls the constructor for 'temp' object
    // 3 --> executes the rest of the body of the operator / function
    // 4 --> calls the assignment operator
    // 5 --> calls the destructor for the 'temp' object

    cout << " --> 19" << endl;

    cout << " --> '/' " << endl;

    cout << foo.get_x()    << ',' << foo.get_y()    << endl;
    cout << bar.get_x()    << ',' << bar.get_y()    << endl;
    cout << result.get_x() << ',' << result.get_y() << endl;

    cout << " --> 20" << endl;

    cout << " --> (foo == bar) = " << (foo == bar) << endl;

    cout << " --> 21" << endl;

    cout << " --> (foo != bar) = " << (foo != bar) << endl;

    cout << " --> 22" << endl;

    foo += bar;

    cout << foo.get_x() << endl;
    cout << foo.get_y() << endl;

    cout << " --> 23" << endl;

    foo -= bar;

    cout << foo.get_x() << endl;
    cout << foo.get_y() << endl;

    cout << " --> 24" << endl;

    foo *= bar;

    cout << foo.get_x() << endl;
    cout << foo.get_y() << endl;

    cout << " --> 25" << endl;

    foo /= bar;

    cout << foo.get_x() << endl;
    cout << foo.get_y() << endl;

    cout << " --> 26" << endl;

    ++bar;

    cout << bar.get_x() << endl;
    cout << bar.get_y() << endl;

    cout << " --> 27" << endl;

    bar++;

    cout << bar.get_x() << endl;
    cout << bar.get_y() << endl;

    cout << " --> 28" << endl;

    --bar;

    cout << bar.get_x() << endl;
    cout << bar.get_y() << endl;

    cout << " --> 29" << endl;

    bar--;

    cout << bar.get_x() << endl;
    cout << bar.get_y() << endl;

    cout << " --> 30" << endl;

    bar = 22;
    foo = 23;

    cout << bar.get_x() << endl;
    cout << bar.get_y() << endl;
    cout << foo.get_x() << endl;
    cout << foo.get_y() << endl;

    cout << " --> 31" << endl;

    bar = 22;
    foo = 23;

    bar = bar + 10;
    foo = bar + 20;

    cout << bar.get_x() << endl;
    cout << bar.get_y() << endl;
    cout << foo.get_x() << endl;
    cout << foo.get_y() << endl;

    cout << " --> 32" << endl;

    bar = 22;
    foo = 23;

    bar = bar - 10;
    foo = bar - 20;

    cout << bar.get_x() << endl;
    cout << bar.get_y() << endl;
    cout << foo.get_x() << endl;
    cout << foo.get_y() << endl;

    cout << " --> 33" << endl;

    bar = 22;
    foo = 23;

    bar = bar * 10;
    foo = bar * 20;

    cout << bar.get_x() << endl;
    cout << bar.get_y() << endl;
    cout << foo.get_x() << endl;
    cout << foo.get_y() << endl;

    cout << " --> 34" << endl;

    bar = 100;
    foo = 0;

    bar = bar / 10;
    foo = bar / 2;

    cout << bar.get_x() << endl;
    cout << bar.get_y() << endl;
    cout << foo.get_x() << endl;
    cout << foo.get_y() << endl;

    cout << " --> 35" << endl;

    cout << " bar = " << bar << endl;

    cout << " --> 36" << endl;

    cout << " foo = " << foo << endl;

    cout << " --> 37" << endl;

    cin >> foo;

    cout << " foo now is " << foo << endl;

    cout << " --> 38" << endl;

    foo = 5 + bar;

    cout << " foo = " << foo << endl;

    cout << " --> 39" << endl;

    foo = 5 - bar;

    cout << " foo = " << foo << endl;

    cout << " --> 40" << endl;

    foo = 5 * bar;

    cout << " foo = " << foo << endl;

    cout << " --> LAST" << endl;

    cout << " --> 41" << endl;

    foo = 100;

    int x = foo;

    cout << " foo = 100; int x = foo; x = " << x << endl;

    cout << " --> 42" << endl;

    foo = 200.0;

    double d = foo;

    cout << " foo = 200.0; double d = foo; d = " << d << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    cout << " --> LAST+1" << endl;

    return 0;
}

//======//
// FINI //
//======//

