//================================//
// Author: Pavlos G. Galiatsatos  //
// Date: 2013/10/23               //
// Functions: vector, deque       //
// Algorithms: sort               //
//================================//

#include <iostream>
#include <vector>
#include <deque>
#include <iomanip>
#include <ctime>
#include <algorithm>

using namespace std;

int main ()
{
    // variables declaration

    vector<double> aVector;
    deque<double> aDeque;
    long int i;
    long int j;
    const long int J_MAX_DO = 10 * static_cast<long int>(2);
    const long int MAX_ELEM = 4 * static_cast<long int>(100000000);
    clock_t t1;
    clock_t t2;

    // set up the output style

    cout << fixed;
    cout << setprecision(10);
    cout << showpos;
    cout << showpoint;

    // allocate an array with space
    // for MAX_LEM elements using vector's allocator

    cout << " vector --> build, sort " << endl;

    t1 = clock();

    for (j = 0; j < J_MAX_DO; j++) {
        // build values for the vector

        cout << "--------------------------------------------------------> " << j+1 << endl;

        for (i = 0; i < MAX_ELEM; i++) {
            aVector.push_back(static_cast<double>(MAX_ELEM-i));
        }

        // some output

        cout << "The allocated vector contains (0,3):" << endl;

        for (i = 0; i < 3; i++) {
            cout << " --> " << i << " --> " << aVector[i] << endl;
        }

        // sort the vector

        sort(aVector.begin(), aVector.end());

        // some output after sort

        cout << "The sorted allocated vector contains (0,3):" << endl;

        for (i = 0; i < 3; i++) {
            cout << " --> " << i << " --> " << aVector[i] << endl;
        }

        aVector.clear();

    }

    t2 = clock();

    cout << " time for vector --> " << (t2-t1)/CLOCKS_PER_SEC << endl;

    cout << " deque --> allocate, construct, sort, destroy, deallocate " << endl;

    t1 = clock();

    for (j = 0; j < J_MAX_DO; j++) {

        cout << "--------------------------------------------------------> " << j+1 << endl;

        // build the deque

        for (i = 0; i < MAX_ELEM; i++) {
            aDeque.push_back(static_cast<double>(MAX_ELEM-i));
        }

        // some output

        cout << "The allocated deque contains (0,3):" << endl;

        for (i = 0; i < 3; i++) {
            cout << " --> " << i << " --> " << aDeque[i] << endl;
        }

        // sort the deque

        sort(aDeque.begin(), aDeque.end());

        // some output after sort

        cout << "The sorted allocated deque contains (0,3):" << endl;

        for (i = 0; i < 3; i++) {
            cout << " --> " << i << " --> " << aDeque[i] << endl;
        }

        aDeque.clear();

    }

    t2 = clock();

    cout << " time for deque --> " << (t2-t1)/CLOCKS_PER_SEC << endl;

    return 0;
}

// end
