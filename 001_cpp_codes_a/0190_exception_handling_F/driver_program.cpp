
//=====================//
// handling exceptions //
//=====================//

#include <iostream>
#include <stdexcept>
#include <vector>

using std::endl;
using std::cout;
using std::cin;
using std::vector;
using std::out_of_range;

int main()
{
    vector<int> myvector(10);

    // try #1

    try {
        cout << " --> try --> 1" << endl;
        myvector.at(5) = 100; // vector::at does not throw an out-of range
    } catch (const out_of_range & oor) {
        cout << "Out of range error: " << oor.what() << endl;
    }

    // try #2

    try {
        cout << " --> try --> 2" << endl;
        myvector.at(10) = 100; // vector::at throws an out-of range
    } catch (const out_of_range & oor) {
        cout << "Out of range error: " << oor.what() << endl;
    }

    // try #3

    try {
        cout << " --> try --> 3" << endl;
        myvector.at(20) = 100; // vector::at throws an out-of range
    } catch (const out_of_range & oor) {
        cout << "Out of range error: " << oor.what() << endl;
    }

    return 0;
}

//======//
// FINI //
//======//
