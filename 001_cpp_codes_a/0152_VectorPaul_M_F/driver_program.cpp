//================//
// driver program //
//================//

#include "IncludeLibs.h"
#include "VectorPaul.h"
#include "VectorPaulFunctions.h"

// the main function

int main()
{
    // local parameters and variables

    const long MAX_DO = static_cast<long>(pow(10.0, 7.0));
    const long DIM = static_cast<long>(pow(10.0, 8.0));
    VectorPaul<double> objA;
    long i;
    long j;
    long k;

    // set the output format

    cout << fixed;
    cout << setprecision(15);
    cout << showpoint;
    cout << showpos;

    // main do-loop

    for (i = 0; i < MAX_DO; i++) {
        // 1.

        cout << "------------------------------------------------------------>> " << i << " / " << MAX_DO << endl;

        // 2.

        cout << " --> objA.setDim(DIM);" << endl;
        objA.setDim(DIM);

        // 3.

        cout << " --> objA.allocateVector();" << endl;
        objA.allocateVector();

        // 4.

        cout << " --> objA.setVector(j, val);" << endl;

        for (j = 0; j < DIM; j++) {
            double val = static_cast<double>(j);
            objA.setVector(j, val);
        }

        // 5.

        cout << " --> objA.getVector(0L)  = " << objA.getVector(0L)  << endl;
        cout << " --> objA.getVector(1L)  = " << objA.getVector(1L)  << endl;
        cout << " --> objA.getVector(2L)  = " << objA.getVector(2L)  << endl;

        // 6.

        cout << " --> objA.setVector(j, val);" << endl;

        for (j = 0; j < DIM; j++) {
            double val = cos(static_cast<double>(j));
            objA.setVector(j, val);
        }

        // 7.

        cout << " --> objA.getVector(0L)  = " << objA.getVector(0L) << endl;
        cout << " --> objA.getVector(1L)  = " << objA.getVector(1L) << endl;
        cout << " --> objA.getVector(2L)  = " << objA.getVector(2L) << endl;

        // 8.

        cout << " --> objA.sortVector();" << endl;
        objA.sortVector();

        // 9.

        for (k = 0 ; k < 10L; k++) {
            cout << " --> objA.getVector(" << k << ")= " << objA.getVector(k) << endl;
        }

        // 10.

        cout << " --> objA.randomShuffleVector();" << endl;
        objA.randomShuffleVector();

        // 11.

        for (k = 0 ; k < 10L; k++) {
            cout << " --> objA.getVector(" << k << ")= " << objA.getVector(k) << endl;
        }

        // 12.

        cout << " --> objA.maxElementVector() = " << objA.maxElementVector() << endl;

        // 13.

        cout << " --> objA.minElementVector() = " << objA.minElementVector() << endl;

        // 12.

        cout << " --> objA.deleteVector();" << endl;
        objA.deleteVector();
    }

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
