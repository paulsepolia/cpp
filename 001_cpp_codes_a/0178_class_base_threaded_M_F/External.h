
#ifndef EXTERNAL_H
#define EXTERNAL_H

//====================//
// benchmark function //
//====================//

#include <iostream>
#include <cmath>
#include "PFArrayDeclaration.h"
#include "PFArrayDefinition.h"

using std::cin;
using std::cout;
using std::endl;
using pgg::PFArrayD;

template<typename T, typename P>
void benchFun(const T dim)
{
    PFArrayD<T,P> obA(dim);

    for (T j = 0; j < dim; j++) {
        obA.addElement(cos(static_cast<P>(j)));
    }

    // make use of copy constructor

    PFArrayD<T, P> obB(obA);
    PFArrayD<T, P> obC(obB);
}

#endif // EXTERNAL_H

//======//
// FINI //
//======//
