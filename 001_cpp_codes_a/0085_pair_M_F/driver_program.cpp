//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/11/04              //
// Functions: pair               //
// Algorithms:                   //
//===============================//

#include <iostream>
#include <utility>
#include <iomanip>

using namespace std;

int main ()
{
    // variables declaration

    const long int DIM_AR = 10 * static_cast<long int>(10000000);
    const long int J_MAX = 100;
    long int i;
    long int j;
    pair<int, int> P1;
    pair<int, int> P2;
    pair<double, double> P3;
    pair<double, double> P4;

    // set the output style

    cout << fixed;
    cout << showpos;
    cout << showpoint;
    cout << setprecision(4);

    // some output

    cout << " P1.first = " << P1.first << " <--> P1.second = " << P1.second << endl;
    cout << " P2.first = " << P2.first << " <--> P2.second = " << P2.second << endl;
    cout << " P3.first = " << P3.first << " <--> P3.second = " << P3.second << endl;
    cout << " P4.first = " << P4.first << " <--> P4.second = " << P4.second << endl;

    // making some pairs

    P1 = make_pair(1,2);
    P2 = make_pair(3,4);
    P3 = make_pair(5.1, 6.1);
    P4 = make_pair(7.1, 8.1);

    // some output

    cout << " P1.first = " << P1.first << " <--> P1.second = " << P1.second << endl;
    cout << " P2.first = " << P2.first << " <--> P2.second = " << P2.second << endl;
    cout << " P3.first = " << P3.first << " <--> P3.second = " << P3.second << endl;
    cout << " P4.first = " << P4.first << " <--> P4.second = " << P4.second << endl;

    for (j = 0; j < J_MAX; j++) {
        cout << "--------------------------------------------------------------> " << j+1 << endl;

        // declare the arrays of pairs

        cout << " 1 --> Declare the arrays of pairs" << endl;

        pair<double, double> *arrayPairA = new pair<double, double> [DIM_AR];
        pair<double, double> *arrayPairB = new pair<double, double> [DIM_AR];

        // build the arrays of pairs

        cout << " 2 --> Build the arrays of pairs" << endl;

        for (i = 0; i < DIM_AR; i++) {
            arrayPairA[i] = make_pair(i, i+1);
            arrayPairB[i] = make_pair(i+2, i+3);
        }

        // some output

        cout << " 3 --> Some output" << endl;

        cout << " arrayPairA[0].first = " << arrayPairA[0].first << endl;
        cout << " arrayPairB[1].first = " << arrayPairB[2].first << endl;

        cout << " arrayPairA[0].second = " << arrayPairA[0].second << endl;
        cout << " arrayPairA[1].second = " << arrayPairA[1].second << endl;

        // free up RAM

        cout << " 4 --> Free up the RAM" << endl;

        delete [] arrayPairA;
        delete [] arrayPairB;
    }

    return 0;
}

// end
