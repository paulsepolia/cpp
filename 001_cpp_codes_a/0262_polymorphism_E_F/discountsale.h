
//===================================//
// declaration of class DiscountSale //
//===================================//

#ifndef DISCOUNTSALE_H
#define DISCOUNTSALE_H

#include "sale.h"

namespace pgg {
class DiscountSale : public Sale {
public:

    DiscountSale();
    DiscountSale(double, double);
    double getDiscount() const;
    void setDiscount(double);
    double bill() const;

private:

    double discount;
};

} // pgg


#endif // DISCOUNTSALE_H

//======//
// FINI //
//======//
