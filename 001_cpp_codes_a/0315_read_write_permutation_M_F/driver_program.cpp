
//=================================//
// create a dictionary             //
// read it                         //
// try to find all words based on  //
// an input word as anagramed      //
//=================================//

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <numeric>
#include <iterator>
#include <ctime>
#include <iomanip>
#include <cmath>

using std::endl;
using std::cout;
using std::cin;
using std::ifstream;
using std::ofstream;
using std::string;
using std::vector;
using std::binary_search;
using std::copy;
using std::ios;
using std::exit;
using std::remove;
using std::next_permutation;
using std::sort;
using std::accumulate;
using std::iterator;
using std::ostream_iterator;
using std::istream_iterator;
using std::clock;
using std::clock_t;
using std::fixed;
using std::showpoint;
using std::showpos;
using std::setprecision;
using std::flush;
using std::pow;

// function object

class add_string {
public:

    string operator() (const string& x, const string& y)
    {
        return (x+y);
    }
};

// the main function

int main()
{
    // local variables and parameters

    const long K_MAX = static_cast<long>(pow(10.0, 4.0));
    vector<string> vs0 {"1", "2", "3", "4", "5", "6", "a", "b", "c", "d", "e"}; // base
    vector<string> vs3 {"1", "a", "2", "b", "3", "c", "4", "d", "5", "e", "6"}; // look for
    //vector<string> vs0 {"2", "3", "4", "5", "6", "a", "b", "c", "d", "e"}; // base
    //vector<string> vs3 {"a", "2", "b", "3", "c", "4", "d", "5", "e", "6"}; // look for
    string fileName = "dictionary.txt";
    typedef istream_iterator<string> string_input;
    ofstream ofs;
    ifstream ifs;
    clock_t t1;
    clock_t t2;
    int sentinel;
    bool bool_flag;
    long long word_counter;
    long long tmpLL;
    int tmpInt;

    // set the output format

    cout << fixed;
    cout << setprecision(10);
    cout << showpos;
    cout << showpoint;

    // main for loop

    for (long k = 0; k != K_MAX; k++) {
        // the counter

        cout << endl;
        cout << "-------------------------------------------->> " << k << endl;
        cout << endl;

        //
        // create a vector of strings
        //
        // and write the words/strings to the file
        // that is my dictionary

        // the vector to put all the above permutations

        vector<string> * vs1 = new vector<string>;

        // main loop to create the words

        cout << " --> creating the vector<string> which contains all the words" << endl;

        //============================//
        // THIS IS IMPORTANT

        sort(vs0.begin(), vs0.end());

        //============================//

        t1 = clock();

        do {
            string a_word = "";
            string init = "";
            a_word = accumulate(vs0.begin(), vs0.end(), init, add_string());
            vs1->push_back(a_word);

        } while(next_permutation(vs0.begin(), vs0.end()));

        t2 = clock();

        cout << " --> time used is --> "
             << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        //
        // open a file for ascii writing
        //

        cout << " --> opening the file to write to it the dictionary" << endl;

        ofs.open(fileName, ios::out);

        // test if opened

        if (!ofs.is_open()) {
            cout << " The output stream is not opened. Enter an integer to exit: ";
            cin >> sentinel;
            exit(-1);
        }

        cout << " --> file opened with success" << endl;

        // write the words

        ostream_iterator<string> out(ofs, "\n");

        cout << " --> writing the words to the file (hard disk)" << endl;

        t1 = clock();

        copy(vs1->begin(), vs1->end(), out);

        ofs << flush;

        t2 = clock();

        cout << " --> time used is --> "
             << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // close the stream

        ofs.close();

        cout << " --> file closed with success" << endl;

        // free the RAM

        cout << " --> delete the dictionary from RAM (it is on the hard disk)" << endl;

        delete vs1;

        //
        // open the dictionary for reading
        //

        cout << " --> opening the dictionary file to read it and store it in RAM" << endl;

        ifs.open(fileName, ios::in);

        // test if opened

        if (!ifs.is_open()) {
            cout << " The input stream is not opened. Enter an integer to exit: ";
            cin >> sentinel;
            exit(-1);
        }

        cout << " --> file opened with success" << endl;

        // read the dictionary

        vector<string> * vs2 = new vector<string>;

        cout << " --> reading the dictionary and storing it in RAM" << endl;

        t1 = clock();

        copy(string_input(ifs), string_input(), back_inserter(*vs2));

        t2 = clock();

        cout << " --> time used is --> "
             << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // close the input stream

        ifs.close();

        cout << " --> file closed with success" << endl;

        // remove the file from the hard disk

        cout << " --> remove the file from the hard disk" << endl;

        tmpInt = remove(fileName.c_str());

        if (tmpInt != 0) {
            cout << "Error removing the file! Enter an interger to exit: ";
            cin >> sentinel;
            exit(-1);
        }

        cout << " --> file deleted from the hard disk" << endl;

        // get the size of the dictionary

        tmpLL = vs2->size();

        cout << " --> the size of the dictionary in RAM is --> " << tmpLL << endl;

        // sort the dictionary

        cout << " --> sorting the dictionary" << endl;

        t1 = clock();

        sort(vs2->begin(), vs2->end());

        t2 = clock();

        cout << " --> time used is --> "
             << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // test if the vector is sorted

        cout << " --> testing if dictionary is sorted or not" << endl;

        t1 = clock();

        bool_flag = is_sorted(vs2->begin(), vs2->end());

        if(!bool_flag) {
            cout << "Error sorting the dictionary! Enter an interger to exit: ";
            cin >> sentinel;
            exit(-1);
        }

        t2 = clock();

        cout << " --> dictionary is sorted" << endl;

        cout << " --> time used is --> "
             << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // create word and search if exists
        // count how many words you have found

        cout << " --> binary searching the dictionary" << endl;

        t1 = clock();

        word_counter = 0LL; // reset counter

        //=========================//
        // THIS IS IMPORTANT

        sort(vs3.begin(), vs3.end());

        //=========================//

        bool_flag = false;

        do {
            string a_word = "";
            string init = "";
            a_word = accumulate(vs3.begin(), vs3.end(), init, add_string());

            bool_flag = binary_search(vs2->begin(), vs2->end(), a_word);

            if (bool_flag) {
                ++word_counter;
            }
        } while(next_permutation(vs3.begin(), vs3.end()));

        t2 = clock();

        cout << " --> number of words found --> " << word_counter << endl;

        cout << " --> time used is --> "
             << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // free the RAM

        cout << " --> delete the dictionary from RAM" << endl;

        delete vs2;
    }

    // sentineling
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

