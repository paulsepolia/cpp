//=====================//
// define preprocessor //
//=====================//

//===============//
// code --> 0345 //
//===============//

#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::string;

// define
// function macro

#define getmax(a,b) ((a)>(b)?(a):(b))

// the main function

int main()
{
    int x = 5;
    int y;

    y = getmax(x, getmax(1, 11));

    cout << y << endl;
    cout << getmax(7,x) << endl;

    double x1 = 12.0;
    double y1 = 11.0;

    y1 = getmax(x1, y1);

    cout << y1 << endl;

    return 0;
}

// FINI
