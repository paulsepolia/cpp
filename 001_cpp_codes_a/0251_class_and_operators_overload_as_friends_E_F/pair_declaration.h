
#ifndef PAIR_H
#define PAIR_H

//========================//
// Pair class declaration //
//========================//

#include <iostream>

using std::ostream;
using std::istream;

template <class T>
class Pair {
public:

    // constructors

    Pair();
    Pair(T);
    Pair(T,T);

    // member functions

    void set(T, T);
    void set(T);
    const T getFirst() const;
    const T getSecond() const;
    void print() const;

    // [] operator overload as member function
    // that way can only be overloaded

    const T & operator [] (int) const;

    // operator overload
    // via friend functions

    // +, -, *, /

    template <class T1>
    friend const Pair<T1> operator + (const Pair<T1> &, const Pair<T1> &);

    template <class T1>
    friend const Pair<T1> operator - (const Pair<T1> &, const Pair<T1> &);

    template <class T1>
    friend const Pair<T1> operator * (const Pair<T1> &, const Pair<T1> &);

    template <class T1>
    friend const Pair<T1> operator / (const Pair<T1> &, const Pair<T1> &);

    // +=, -=, *=, /=

    template <class T2>
    friend const Pair<T2> & operator += (Pair<T2> &, const Pair<T2> &);

    template <class T2>
    friend const Pair<T2> & operator -= (Pair<T2> &, const Pair<T2> &);

    template <class T2>
    friend const Pair<T2> & operator *= (Pair<T2> &, const Pair<T2> &);

    template <class T2>
    friend const Pair<T2> & operator /= (Pair<T2> &, const Pair<T2> &);

    // -, ++, --

    template <class T2>
    friend const Pair<T2> operator -  (const Pair<T2> &);

    template <class T2>
    friend const Pair<T2> operator ++ (Pair<T2> &);

    template <class T2>
    friend const Pair<T2> operator ++ (Pair<T2> &, int);

    template <class T2>
    friend const Pair<T2> operator -- (Pair<T2> &);

    template <class T2>
    friend const Pair<T2> operator -- (Pair<T2> &, int);

    // <<, >>

    template <class T2>
    friend ostream & operator << (ostream &, const Pair<T2> &);

    template <class T2>
    friend istream & operator >> (istream &, Pair<T2> &);

    // ==, <, >, <=, >=

    template <class T2>
    friend bool operator == (const Pair<T2> &, const Pair<T2> &);

    template <class T2>
    friend bool operator <  (const Pair<T2> &, const Pair<T2> &);

    template <class T2>
    friend bool operator >  (const Pair<T2> &, const Pair<T2> &);

    template <class T2>
    friend bool operator >= (const Pair<T2> &, const Pair<T2> &);

    template <class T2>
    friend bool operator <= (const Pair<T2> &, const Pair<T2> &);

private:

    T first;
    T second;
};

//======//
// FINI //
//======//

#endif // PAIR_H

