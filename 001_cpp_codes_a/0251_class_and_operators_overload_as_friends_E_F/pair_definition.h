
#ifndef PAIR_DEF_H
#define PAIR_DEF_H

//=======================//
// Pair class definition //
//=======================//

#include <iostream>
#include <cstdlib>
#include "pair_declaration.h"

using std::cout;
using std::endl;
using std::exit;

// defualt constructor

template <class T>
Pair<T>::Pair() : first (0.0), second (0.0) {}

// one argument constructor

template <class T>
Pair<T>::Pair(T val) : first (val), second (val) {}

// two argument constructor

template <class T>
Pair<T>::Pair(T val1, T val2 ) : first (val1), second (val2) {}

// member function

template <class T>
void Pair<T>::set(T val1, T val2)
{
    first = val1;
    second = val2;
}

// member function

template <class T>
void Pair<T>::set(T val)
{
    first = val;
    second = val;
}

// member function

template <class T>
const T Pair<T>::getFirst() const
{
    return first;
}

// member function

template <class T>
const T Pair<T>::getSecond() const
{
    return second;
}

// member function

template <class T>
void Pair<T>::print() const
{
    cout << " first = " << first << ", second = " << second << endl;
}

// friend function
// operator overload +

template <class T>
const Pair<T> operator + (const Pair<T> & p1, const Pair<T> & p2)
{
    return Pair<T>(p1.first + p2.first, p1.second + p2.second);
}

// friend function
// operator overload -

template <class T>
const Pair<T> operator - (const Pair<T> & p1, const Pair<T> & p2)
{
    return Pair<T>(p1.first - p2.first, p1.second - p2.second);
}

// friend function
// operator overload *

template <class T>
const Pair<T> operator * (const Pair<T> & p1, const Pair<T> & p2)
{
    return Pair<T>(p1.first * p2.first, p1.second * p2.second);
}

// friend function
// operator overload /

template <class T>
const Pair<T> operator / (const Pair<T> & p1, const Pair<T> & p2)
{
    return Pair<T>(p1.first / p2.first, p1.second / p2.second);
}

// friend function
// operator overload +=

template <class T>
const Pair<T>  & operator += (Pair<T> & p1, const Pair<T> & p2)
{
    p1.first = p1.first + p2.first;
    p1.second = p1.second + p2.second;

    return p1;
}

// friend function
// operator overload -=

template <class T>
const Pair<T> & operator -= (Pair<T> & p1, const Pair<T> & p2)
{
    p1.first = p1.first - p2.first;
    p1.second = p1.second - p2.second;

    return p1;
}

// friend function
// operator overload *=

template <class T>
const Pair<T> & operator *= (Pair<T> & p1, const Pair<T> & p2)
{
    p1.first = p1.first * p2.first;
    p1.second = p1.second * p2.second;

    return p1;
}

// friend function
// operator overload /=

template <class T>
const Pair<T> & operator /= (Pair<T> & p1, const Pair<T> & p2)
{
    p1.first = p1.first / p2.first;
    p1.second = p1.second / p2.second;

    return p1;
}

// friend function
// unary operator overload -

template <class T>
const Pair<T> operator - (const Pair<T> & p)
{
    return Pair<T>(-p.first, -p.second);
}

// friend function
// operator overload ++ (prefix)

template <class T>
const Pair<T> operator ++ (Pair<T> & p)
{
    return Pair<T>(++p.first, ++p.second);
}

// friend function
// operator overload ++ (postfix)

template <class T>
const Pair<T> operator ++ (Pair<T> & p, int)
{
    return Pair<T>(++p.first, ++p.second);
}

// friend function
// operator overload -- (prefix)

template <class T>
const Pair<T> operator -- (Pair<T> & p)
{
    return Pair<T>(--p.first, --p.second);
}

// friend function
// operator overload -- (postfix)

template <class T>
const Pair<T> operator -- (Pair<T> & p, int)
{
    return Pair<T>(--p.first, --p.second);
}

// friend function
// operator overload <<

template <class T>
ostream & operator << (ostream & outStream, const Pair<T> & p)
{
    outStream << p.first << ", " << p.second;

    return outStream;
}

// friend function
// operator overload >>

template <class T>
istream & operator >> (istream & inStream, Pair<T> & p)
{
    inStream >> p.first;
    inStream >> p.second;

    return inStream;
}

// friend function
// operator overload ==

template <class T>
bool operator == (const Pair<T> & p1, const Pair<T> & p2)
{
    return ((p1.first == p2.first) && (p1.second == p2.second));
}

// friend function
// operator overload <

template <class T>
bool operator < (const Pair<T> & p1, const Pair<T> & p2)
{
    T val1 = p1.first * p1.first + p1.second * p1.second;
    T val2 = p2.first * p2.first + p2.second * p2.second;

    return (val1 < val2);
}

// friend function
// operator overload >

template <class T>
bool operator > (const Pair<T> & p1, const Pair<T> & p2)
{
    T val1 = p1.first * p1.first + p1.second * p1.second;
    T val2 = p2.first * p2.first + p2.second * p2.second;

    return (val1 > val2);
}

// friend function
// operator overload >=

template <class T>
bool operator >= (const Pair<T> & p1, const Pair<T> & p2)
{
    T val1 = p1.first * p1.first + p1.second * p1.second;
    T val2 = p2.first * p2.first + p2.second * p2.second;

    return (val1 >= val2);
}

// friend function
// operator overload <=

template <class T>
bool operator <= (const Pair<T> & p1, const Pair<T> & p2)
{
    T val1 = p1.first * p1.first + p1.second * p1.second;
    T val2 = p2.first * p2.first + p2.second * p2.second;

    return (val1 <= val2);
}

// member function
// [] operator overload
// this is the only possible way to overload [] operator
// using a member function

template <class T>
const T & Pair<T>::operator [] (int index) const
{
    if (index == 1) {
        return first;
    } else if (index == 2) {
        return second;
    } else {
        cout << "Illegal index value." << endl;
        exit(1);
    }
}

//======//
// FINI //
//======//

#endif // PAIR_DEF_H

