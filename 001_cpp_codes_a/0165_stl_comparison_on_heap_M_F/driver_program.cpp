//======================//
// STL clear and delete //
//======================//

#include "includes.h"
#include "bench_fun.h"

// the main program

int main()
{
    // variables and parameters

    const long int K_MAX = static_cast<long int>(pow(10.0, 6.0));
    long int k;

    // main bench loop

    for (k = 0; k < K_MAX; k++) {
        cout << endl;
        cout << "------------------------------------------------------------>>> " << k << endl;
        cout << endl;

        bench_fun();
    }

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
