//===========//
// SaleDef.h //
//===========//

#ifndef SALE_DEF
#define SALE_DEF

#include "SaleHeader.h"
#include <iostream>
using std::cout;
using std::endl;

namespace pgg {
//default constructor

Sale::Sale() : price(0) {}

// non-default constructor

Sale::Sale(double thePrice) : price (thePrice)
{
    if (thePrice >= 0) {
        price = thePrice;
    } else {
        cout << "Error!" << endl;
        exit(1);
    }
}

// member function

double Sale::bill() const
{
    return price;
}

// member function

double Sale::getPrice() const
{
    return price;
}

// member function

void Sale::setPrice(double newPrice)
{
    if (newPrice >= 0) {
        price = newPrice;
    } else {
        cout << "Error!" << endl;
        exit(1);
    }
}

// member function

double Sale::savings(const Sale& other) const
{
    return (bill() - other.bill());
}

// operator <

bool operator < (const Sale& first, const Sale& second)
{
    return (first.bill() < second.bill());
}
} // pgg

#endif // SALE_DEF

//======//
// FINI //
//======//
