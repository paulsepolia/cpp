
//=======================================================//
// Classes: string, ifstream, ofstream                   //
// Functions: open, close                                //
// Manipulators: showpoint, showpos, setprecision, fixed //
//=======================================================//

//===============//
// code --> 0008 //
//===============//

// keywords: ifstream, ofstream, fixed, showpos, showpoint, string
//	     setprecision, pow, ascii input, ascii output
//	     open file stream, close file stream

#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <cmath>

using std::cout;
using std::cin;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::string;
using std::fixed;
using std::showpoint;
using std::showpos;
using std::setprecision;
using std::pow;

// the main function

int main()
{
    // 1 --> declare variables

    cout << " --> 1 --> declare local variables and parameters" << endl;

    const int I_MAX = static_cast<int>(pow(10, 6.0));
    ifstream inFile;
    ofstream outFile;
    double tmp1;
    double tmp2;
    double tmp3;
    double tmp4;
    double tmp5;
    double tmp6;
    double tmp7;
    double tmp8;
    double tmp9;
    double tmp10;

    // 2 --> open units

    cout << " --> 2 --> open the output unit" << endl;

    outFile.open("text.output");

    cout << " --> 3 --> set the output format" << endl;

    outFile << fixed;
    outFile << showpoint;
    outFile << showpos;
    outFile << setprecision(20);

    // 3 --> execution body

    cout << " --> 4 --> read and write from/to the hard disk..." << endl;

    for (int i = 0; i < I_MAX; i++) {
        inFile.open("text.input");

        inFile >> tmp1;
        inFile >> tmp2;
        inFile >> tmp3;
        inFile >> tmp4;
        inFile >> tmp5;
        inFile >> tmp6;
        inFile >> tmp7;
        inFile >> tmp8;
        inFile >> tmp9;
        inFile >> tmp10;

        inFile.close();

        outFile << tmp1 << endl;
        outFile << tmp2 << endl;
        outFile << tmp3 << endl;
        outFile << tmp4 << endl;
        outFile << tmp5 << endl;
        outFile << tmp6 << endl;
        outFile << tmp7 << endl;
        outFile << tmp8 << endl;
        outFile << tmp9 << endl;
        outFile << tmp10 << endl;
    }

    // 4 --> close units

    cout << " --> 5 --> close output unit" << endl;

    outFile.flush();
    outFile.close();

    cout << " --> 6 --> end" << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    cout << " --> 7 --> exit" << endl;

    return 0;
}

//======//
// FINI //
//======//

