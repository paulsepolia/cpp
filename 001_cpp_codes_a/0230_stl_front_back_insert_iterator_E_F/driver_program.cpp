
//========================//
// front_inserter example //
//========================//

#include <iostream>
#include <iterator>
#include <deque>
#include <algorithm>

using std::endl;
using std::cout;
using std::cin;
using std::deque;
using std::copy;
using std::front_inserter;
using std::back_inserter;

// the main function

int main()
{
    // local variables and parameters

    deque<int> foo;
    deque<int> bar;
    deque<int>::iterator it;

    // build the vectors

    for (int i = 1; i <= 5; i++) {
        foo.push_back(i);
        bar.push_back(i*10);
    }

    // use the back_inserter iterator

    copy(bar.begin(), bar.end(), back_inserter(foo));

    for (it = foo.begin(); it != foo.end(); it++) {
        cout << *it << " ";
    }

    cout << endl;

    // use the front_inserter iterator

    copy(bar.begin(), bar.end(), front_inserter(foo));

    for (it = foo.begin(); it != foo.end(); it++) {
        cout << *it << " ";
    }

    cout << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

