//============================//
// Base and derived class     //
// Public, Protected, Private //
//============================//

#include <iostream>

using std::cout;
using std::cin;
using std::endl;

// class A

class A {
private:

    int _privInt = 0;
    int privFunc()
    {
        return 0;
    }
    virtual int privVirtFunc()
    {
        return 0;
    }

protected:

    int _protInt = 0;
    int protFunc()
    {
        return 0;
    }

public:

    int _publInt = 0;
    int publFunc()
    {
        cout << privVirtFunc() << endl;
        return privVirtFunc();
    }

    // my default constructor
    A() : _privInt (10), _protInt (11), _publInt (12)  {};

};

// class B

class B : public A {
private:

    virtual int privVirtFunc()
    {
        return 1;
    }

public:

    void func()
    {
//    _privInt = 1; // wont work
        _protInt = 1; // will work
        _publInt = 1; // will work
//    privFunc(); // wont work
//    privVirtFunc(); // wont work
        protFunc(); // will work
        publFunc(); // will return 1 since it's overridden in this class

        cout << " --> publFunc() = " << publFunc() << endl;
    }
};

// the main function

int main()
{
    A objA;

    cout << " --> objA._publInt = " << objA._publInt << endl;

    B objB;

    objB.func();

    cout << " --> the following is possible because of the public inheritance" << endl;
    cout << " --> objB._publInt = " << objB._publInt << endl;

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
