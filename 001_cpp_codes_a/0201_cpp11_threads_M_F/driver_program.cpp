
//=========================//
// pthreads and stack size //
//=========================//

#include <iostream>
#include <string>
#include <cstdlib>
#include <cassert>
#include <unistd.h>
#include <climits>
#include <cmath>
#include <thread>

using std::cin;
using std::cout;
using std::endl;
using std::thread;

// 1. size of the arrays

const long MAX_DIM = static_cast<long>(pow(10.0, 8.4));

// 2. function

void threadFunc1()
{
    double * arrayA = new double [MAX_DIM];

    for (long i = 0; i < MAX_DIM; i++) {
        arrayA[i] = cos(static_cast<double>(i));
    }

    cout << " --> Hello from threadFunc1" << endl;
    cout << " --> threadFunc1 --> arrayA[1] = " << arrayA[1] << endl;

    delete [] arrayA;
}

// 3. function

void threadFunc2()
{
    double * arrayB = new double [MAX_DIM];

    for (long i = 0; i < MAX_DIM; i++) {
        arrayB[i] = cos(static_cast<double>(i));
    }

    cout << " --> Hello from threadFunc2" << endl;
    cout << " --> threadFunc2 --> arrayB[1] = " << arrayB[1] << endl;

    delete [] arrayB;
}

// 4. the main function

int main()
{
    // launch thread 1

    thread t1(threadFunc1);

    // launch thread 2

    thread t2(threadFunc2);

    // join threads

    t1.join();
    t2.join();

    cout << " --> Main done()" << endl;

    return 0;
}

//======//
// FINI //
//======//
