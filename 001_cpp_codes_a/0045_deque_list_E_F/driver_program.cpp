//================================//
// Author: Pavlos G. Galiatsatos  //
// Date: 2013/10/23               //
// Functions:  deque, list        //
//================================//

#include <iostream>
#include <deque>
#include <list>

using namespace std;

// main function
int main()
{
    deque<double> aDeque;
    list<double> aList;
    long int i;
    long int j;
    const long int I_MAX = 3 * 40000000;
    const long int J_MAX = 1000;
    double tmpA;

    for (j = 0; j < J_MAX ; j++) {
        cout << "----------------------------------------------> " << j << endl;

        cout << " 1 --> Insert at the beginning" << endl;
        cout << " 2 --> For deque" << endl;

        // deque: add at the beginning

        tmpA = 0.0;

        for (i = 0; i < I_MAX; i++) {
            tmpA = tmpA + 1.0;
            aDeque.push_front(tmpA);
        }

        cout << " 3 --> Delete at the beginning" << endl;
        cout << " 4 --> For deque" << endl;

        // deque: delete at the beginning

        for (i = 0; i < I_MAX; i++) {
            aDeque.pop_front();
        }

        // list: add at the beginning

        cout << " 5 --> Insert at the beginning" << endl;
        cout << " 6 --> For list" << endl;

        tmpA = 0.0;

        for (i = 0; i < I_MAX; i++) {
            tmpA = tmpA + 1.0;
            aList.push_front(tmpA);
        }

        cout << " 7 --> Delete at the beginning" << endl;
        cout << " 8 --> For list" << endl;

        // list: delete at the beginning

        for (i = 0; i < I_MAX; i++) {
            aList.pop_front();
        }
    }

    return 0;
}
// end
