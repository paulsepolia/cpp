//=================//
// bind1st example //
//=================//

#include <iostream>
#include <functional>
#include <algorithm>

using std::endl;
using std::cout;
using std::cin;
using std::count_if;
using std::equal_to;

// the main function

int main ()
{
    // local variables and parameters

    int numbers[] = { 10, 20, 30, 40, 50, 10 };
    int cx;

    // apply the bind1st binder

    cx = count_if(numbers, numbers+6, bind1st(equal_to<int>(), 10) );

    // output the results

    cout << "There are --> " << cx << " --> elements that are equal to 10" << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
