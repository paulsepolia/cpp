//===============================//
// non-member operator overloads //
//===============================//

#include <iostream>

// a class

class CVector {
public:

    int x,y;
    CVector() : x(0), y(0) {} // default constructor
    CVector (int a, int b) : x(a), y(b) {} // non-default constructor

    CVector & operator = (const CVector & param); // a member function

};

// a non-member function to overload '='

CVector & CVector::operator = (const CVector & param)
{
    x = param.x;
    y = param.y;

    return *this;
}

// a non-member function to overload '+'

CVector operator + (const CVector & lhs, const CVector & rhs)
{
    CVector temp;
    temp.x = lhs.x + rhs.x;
    temp.y = lhs.y + rhs.y;

    return temp;
}

// a non-member function to overload '-'

CVector operator - (const CVector & lhs, const CVector & rhs)
{
    CVector temp;
    temp.x = lhs.x - rhs.x;
    temp.y = lhs.y - rhs.y;

    return temp;
}

// a non-member function to overload '*'

CVector operator * (const CVector & lhs, const CVector & rhs)
{
    CVector temp;
    temp.x = lhs.x * rhs.x;
    temp.y = lhs.y * rhs.y;

    return temp;
}

// a non-member function to overload '/'

CVector operator / (const CVector & lhs, const CVector & rhs)
{
    CVector temp;
    temp.x = lhs.x / rhs.x;
    temp.y = lhs.y / rhs.y;

    return temp;
}
// the main function

int main ()
{
    CVector foo (3,1);
    CVector bar (1,2);
    CVector result;

    // '+'

    std::cout << "+" << std::endl;

    result = foo + bar;
    std::cout << result.x << ',' << result.y << std::endl;

    result = operator + (foo, bar);
    std::cout << result.x << ',' << result.y << std::endl;

    // '-'

    std::cout << "-" << std::endl;

    result = foo - bar;
    std::cout << result.x << ',' << result.y << std::endl;

    result = operator - (foo, bar);
    std::cout << result.x << ',' << result.y << std::endl;

    // '*'

    std::cout << "*" << std::endl;

    result = foo * bar;
    std::cout << result.x << ',' << result.y << std::endl;

    result = operator * (foo, bar);
    std::cout << result.x << ',' << result.y << std::endl;

    // '/'

    std::cout << "/" << std::endl;

    result = foo / bar;
    std::cout << result.x << ',' << result.y << std::endl;

    result = operator / (foo, bar);
    std::cout << result.x << ',' << result.y << std::endl;

    // '='

    std::cout << "=" << std::endl;

    result = foo;
    std::cout << result.x << ',' << result.y << std::endl;

    // '='

    std::cout << "=" << std::endl;

    result = bar;
    std::cout << result.x << ',' << result.y << std::endl;

    // sentineling

    int sentinel;
    std::cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

