
//===================//
// Constructs: while //
//===================//

//===============//
// code --> 0014 //
//===============//

// keywords: while, rand, srand, time

#include <iostream>
#include <cstdlib>
#include <ctime>

using std::cin;
using std::cout;
using std::endl;

// the main function

int main()
{
    // --> declare the variables

    int num;
    int guess;
    bool isGuessed;

    // --> initialize some variables

    srand(time(0));
    num = rand() % 100;
    isGuessed = false;

    // --> while game

    while (!isGuessed) {
        cout << " --> Enter an integer greater than or equal to 0 and less than 100: ";
        cin >> guess;
        cout << endl;

        if (guess == num) {
            cout << " --> You guessed the correct number" << endl;
            isGuessed = true;
        } else if (guess < num) {
            cout << " --> Your guess is lower than the number" << endl;
            cout << " --> Guess again!" << endl;
        } else {
            cout << " --> Your guess is higher than the number" << endl;
            cout << " --> Guess again!" << endl;
        }
    }

    // --> end while game

    cout << " --> end" << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    cout << " --> exit" << endl;

    return 0;
}

//======//
// FINI //
//======//

