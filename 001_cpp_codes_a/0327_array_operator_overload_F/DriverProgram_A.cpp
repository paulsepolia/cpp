//========================================//
// Driver program to DoublePairCL  class. //
//========================================//

#include <iostream>

#include "DoublePairCL.h"
#include "DoublePairCLMemberFunctions.h"

using namespace std;

int main()
{
    DoublePairCL a;

    a[1] = 10.0;
    a[2] = 11.1;

    cout << "a[1] and a[2] are: " << endl;
    cout << a[1] << " " << a[2] << endl;

    cout << "Enter two doubles." << endl;
    cin >> a[1];
    cin >> a[2];

    cout << "a[1] and a[2] are: " << endl;
    cout << a[1] << " " << a[2] << endl;

    return 0;
}

//==============//
// End of code. //
//==============//
