
//==================//
// BaseDefinition.h //
//==================//

#ifndef BASEDEFINITION_H
#define BASEDEFINITION_H

#include "BaseHeader.h"

using pgg::base;

namespace pgg {
// default constructor

base::base() : ticketPrice (0.0), ticketNum (0) {}

// non-default constructor

base::base(int num, double price) : ticketPrice (price), ticketNum (num) {}

// member function

int base::getTicketNumber() const
{
    return ticketNum;
}

// member function

double base::getTicketPrice() const
{
    return ticketPrice;
}

// member function

void base::setTicketNumber(int num)
{
    ticketNum = num;
}

// member function

void base::setTicketPrice(double price)
{
    ticketPrice = price;
}

// member function

void base::resetTicket()
{
    ticketNum = 0;
    ticketPrice = 0.0;
}

} // pgg

#endif // BASEDEFINITION_H

//======//
// FINI //
//======//
