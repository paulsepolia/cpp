
//================//
// driver program //
//================//

#include <iostream>
#include <cmath>
#include <iomanip>
#include <cstdlib>
#include "base.h"

using std::cout;
using std::endl;
using std::cin;
using std::pow;
using std::setprecision;
using std::fixed;
using std::showpos;
using std::showpoint;
using std::boolalpha;
using std::exit;

using pgg::Base;

// main function

int main()
{
    // set the output format

    cout << fixed;
    cout << setprecision(10);
    cout << showpos;
    cout << showpoint;
    cout << boolalpha;

    const long K_MAX = static_cast<long>(pow(10.0, 7.0));

    for (long k = 0; k < K_MAX; k++) {

        cout << "---------------------------------------------------------------->> " << k << endl;

        // A1 object

        Base A1;

        const long DIM_MAX = static_cast<long>(pow(10.0, 7.5));

        // allocate A1

        cout << "  1 --> allocate the vector: A1.allocate(DIM_MAX);" << endl << endl;

        A1.allocate(DIM_MAX);

        // get the dimension of A1

        cout << "  2 --> get the dimension of the vector: A1.getDim() = " << A1.getDim() << endl << endl;

        // build the A1

        cout << "  3 --> build the vector element by element" << endl << endl;

        for (long i = 0; i < DIM_MAX; i++) {
            A1.setElement(cos(static_cast<double>(i)), i);
        }

        // test the build process

        cout << "  4 --> get few elements of the vector" << endl << endl;

        cout << "  A1.getElement(0) = " << A1.getElement(0) << endl << endl;
        cout << "  cos(0)           = " << cos(static_cast<double>(0)) << endl << endl;
        cout << "  A1.getElement(0) = " << A1.getElement(1) << endl << endl;
        cout << "  cos(1)           = " << cos(static_cast<double>(1)) << endl << endl;
        cout << "  A1.getElement(0) = " << A1.getElement(2) << endl << endl;
        cout << "  cos(2)           = " << cos(static_cast<double>(2)) << endl << endl;

        // set A2 equal to A2 using the copy constructor

        cout << "  5 --> use the copy constructor: Base A2(A1);" << endl << endl;

        Base A2(A1);

        // test for equality via isEqual member function

        cout << "  6A --> test for equality via isEqual member function. Should give --> true" << endl << endl;

        cout << "  A2.isEqual(A1) = " << A2.isEqual(A1) << endl << endl;

        // test for equality via == member function

        cout << "  6B --> test for equality via == member function. Should give --> true" << endl << endl;

        cout << "  (A2 == A1) = " << (A2 == A1) << endl << endl;

        // test for less-than via < member function

        cout << "  6C --> test for equality via < member function. Should give --> false" << endl << endl;

        cout << "  (A2 < A1) = " << (A2 < A1) << endl << endl;

        // test for less-than-or-equal via <= member function

        cout << "  6D --> test for equality via <= member function. Should give --> true" << endl << endl;

        cout << "  (A2 <= A1) = " << (A2 <= A1) << endl << endl;

        // test for greater-than via > member function

        cout << "  6E --> test for equality via > member function. Should give --> false" << endl << endl;

        cout << "  (A2 > A1) = " << (A2 > A1) << endl << endl;

        // test for greater-than-or-equal via >= member function

        cout << "  6F --> test for equality via >= member function. Should give --> true" << endl << endl;

        cout << "  (A2 >= A1) = " << (A2 >= A1) << endl << endl;

        // test for inequality via != member function

        cout << "  6G --> test for equality via == member function. Should give --> false" << endl << endl;

        cout << "  (A2 != A1) = " << (A2 != A1) << endl << endl;

        // A3 object

        Base A3;

        // add A1 and A2 via add member function

        cout << "  7 --> add A1 and A2 via add member function: A3.add(A1, A2);" << endl << endl;

        A3.add(A1, A2);

        // test the addition

        for (long i = 0; i != DIM_MAX; i++) {
            if (A3.getElement(i) != (A1.getElement(i) + A2.getElement(i))) {
                cout << "Error! Enter an integer to exit: ";
                int sentinel;
                cin >> sentinel;
                exit(-1);
            }
        }

        cout << "    --> add member functions works fine!" << endl << endl;

        // multiply A1 and A2 via add member function

        cout << "  8 --> multiply A1 and A2 via multiply member function: A3.multiply(A1, A2);" << endl << endl;

        A3.multiply(A1, A2);

        // test the multiplication

        for (long i = 0; i != DIM_MAX; i++) {
            if (A3.getElement(i) != (A1.getElement(i) * A2.getElement(i))) {
                cout << "Error! Enter an integer to exit: ";
                int sentinel;
                cin >> sentinel;
                exit(-1);
            }
        }

        cout << "    --> multiply member functions works fine!" << endl << endl;

        // subtract A1 and A2 via subtract member function

        cout << "  9 --> subtract A2 from A1 via subtract member function: A3.subtract(A1, A2);" << endl << endl;

        A3.subtract(A1, A2);

        // test the subtraction

        for (long i = 0; i != DIM_MAX; i++) {
            if (A3.getElement(i) != (A1.getElement(i) - A2.getElement(i))) {
                cout << "Error! Enter an integer to exit: ";
                int sentinel;
                cin >> sentinel;
                exit(-1);
            }
        }

        cout << "    --> subtract member functions works fine!" << endl << endl;

        // divide A1 and A2 via divide member function

        cout << " 10 --> divide A1 with A2 via divide member function: A3.divide(A1, A2);" << endl << endl;

        A3.divide(A1, A2);

        // test the divide member function

        for (long i = 0; i != DIM_MAX; i++) {
            if (A3.getElement(i) != (A1.getElement(i) / A2.getElement(i))) {
                cout << "Error! Enter an integer to exit: ";
                int sentinel;
                cin >> sentinel;
                exit(-1);
            }
        }

        cout << "    --> divide member functions works fine!" << endl << endl;

        Base A4;

        A3.destroy();
        A3.allocate(DIM_MAX);

        A3 = (A1 + A2);
        A4.add(A1, A2);

        cout << " add == + ==> (A3 == A4) = " << (A3 == A4) << endl << endl;

        A3 = A1 - A2;
        A4.subtract(A1, A2);

        cout << " subtract == - ==> (A3 == A4) = " << (A3 == A4) << endl << endl;

        A3 = A1 * A2;
        A4.multiply(A1, A2);

        cout << " multiply == * ==> (A3 == A4) = " << (A3 == A4) << endl << endl;

        cout << "  --> A1[1] = " << A1[1] << endl;
        cout << "  --> A1[2] = " << A1[2] << endl;
        cout << "  --> A2[1] = " << A2[1] << endl;
        cout << "  --> A2[2] = " << A2[2] << endl;
        cout << "  --> A3[1] = " << A3[1] << endl;
        cout << "  --> A3[2] = " << A3[2] << endl;
        cout << "  --> A4[1] = " << A4[1] << endl;
        cout << "  --> A4[2] = " << A4[2] << endl;

        A3 = A1 / A2;
        A4.divide(A1, A2);

        cout << " divide == / ==> (A3 == A4) = " << (A3 == A4) << endl << endl;


        cout << "  ==> (++A3 == ++A4)     = " << (++A3 == ++A4) << endl << endl;
        cout << "  ==> (--A3 == --A4)     = " << (--A3 == --A4) << endl << endl;
        cout << "  ==> ((A3++) == (A4++)) = " << ((A3++) == (A4++)) << endl << endl;
        cout << "  ==> ((A3--) == (A4--)) = " << ((A3--) == (A4--)) << endl << endl;

        cout << "  --> A1[1] = " << A1[1] << endl;
        cout << "  --> A1[2] = " << A1[2] << endl;
        cout << "  --> A2[1] = " << A2[1] << endl;
        cout << "  --> A2[2] = " << A2[2] << endl;
        cout << "  --> A3[1] = " << A3[1] << endl;
        cout << "  --> A3[2] = " << A3[2] << endl;
        cout << "  --> A4[1] = " << A4[1] << endl;
        cout << "  --> A4[2] = " << A4[2] << endl;

        cout << " 11 --> A1.destroy(); " << endl << endl;

        A1.destroy();

        cout << " 12 --> A2.destroy(); " << endl << endl;

        A2.destroy();

        cout << " 13 --> A3.destroy(); " << endl << endl;

        A3.destroy();

        cout << " 14 --> A3.destroy(); " << endl << endl;

        A4.destroy();

    }

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
