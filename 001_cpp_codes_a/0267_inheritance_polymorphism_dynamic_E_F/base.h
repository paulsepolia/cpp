
//========================//
// base class declaration //
//========================//

#ifndef BASE_H
#define BASE_H

namespace pgg {
class Base {
public:

    // constructors

    Base();
    Base(double, long);
    Base(const Base&);

    // destructor

    virtual ~Base();

    // member functions

    void allocate(long);
    long getDim() const;
    double getElement(long) const;
    void setDim(long);
    void setElement(double, long);
    void setEqual(const Base&);
    bool isEqual(const Base&) const;
    void add(const Base&, const Base&);
    void subtract(const Base&, const Base&);
    void multiply(const Base&, const Base&);
    void divide(const Base&, const Base&);
    double magnitude() const;
    void destroy();

    // operators overload as member functions

    // ==, <, <=, >, >=, !=

    bool operator == (const Base&) const;
    bool operator != (const Base&) const;
    bool operator <  (const Base&) const;
    bool operator <= (const Base&) const;
    bool operator >  (const Base&) const;
    bool operator >= (const Base&) const;

    // +, -, *, /

    Base operator + (const Base&) const;
    Base operator - (const Base&) const;
    Base operator * (const Base&) const;
    Base operator / (const Base&) const;

    // ++, --, -

    const Base operator ++ ();
    const Base operator ++ (int);
    const Base operator -- ();
    const Base operator -- (int);
    const Base operator -  ();

    // +=, -=, *=, /=

    const Base operator += (const Base&);
    const Base operator -= (const Base&);
    const Base operator *= (const Base&);
    const Base operator /= (const Base&);

    // =

    const Base operator = (const Base&);

    // []

    double operator [] (long);

private:

    long dim;
    double *vec;
};

} // pgg

#endif // BASE_H

//======//
// FINI //
//======//
