
//=======================//
// bind function adaptor //
//=======================//

#include <iostream>
#include <functional>

using std::cout;
using std::cin;
using std::endl;
using std::bind;

// a function

double my_divide(const double& x, const double& y)
{
    return x/y;
}

// a class

class mypair_CL {
public:

    double a;
    double b;

    double multiply()
    {
        return a*b;
    }
};

// the main function

int main ()
{
    using namespace std::placeholders;    // adds visibility of _1, _2, _3,...

    // binding functions

    auto fn_five = bind(my_divide, 10, 2);               // returns 10/2

    cout << fn_five() << endl;                           // 5

    auto fn_half = bind(my_divide, _1, 2);               // returns x/2

    cout << fn_half(100) << endl;                         // 100/2

    auto fn_invert = bind(my_divide, _2, _1);            // returns y/x

    std::cout << fn_invert(200 , 2) << endl;             // 2/200

    auto fn_rounding = bind<int>(my_divide, _1, _2);     // returns int(x/y)

    cout << fn_rounding(10, 3) << endl;                  // 3

    mypair_CL ten_two {10, 2};

    // binding members

    auto bound_member_fn = bind(&mypair_CL::multiply, _1); // returns x.multiply()

    cout << bound_member_fn(ten_two) << endl;              // 20

    auto bound_member_data1 = bind(&mypair_CL::a, ten_two); // returns ten_two.a

    cout << bound_member_data1() << endl;                   // 10

    auto bound_member_data2 = bind(&mypair_CL::b, ten_two); // returns ten_two.a

    cout << bound_member_data2() << endl;                   // 2

    // int sentinel

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

