
//===============================//
// using an array as a container //
//===============================//

#include <iostream>
#include <list>
#include <algorithm>
#include <memory>
#include <functional>

using std::endl;
using std::cout;
using std::cin;
using std::list;
using std::copy;
using std::remove_copy_if;
using std::bind2nd;
using std::bind;
using std::less;

// the main function

int main()
{
    // local parameters and variables

    list<int> listA(10);
    list<int>::iterator it;
    int * ip;
    int * ip_end;
    int nums[10] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

    // display nums array

    cout << " --> initial contents of nums: " << endl;

    for (int i = 0; i < 10; i++) {
        cout << nums[i] << " ";
    }

    cout << endl;

    // copy nums array to list

    copy(&nums[0], &nums[9], listA.begin());

    // contents of listA after copy

    for (it = listA.begin(); it != listA.end(); it++) {
        cout << *it << " ";
    }

    cout << endl;

    // remove elements that are less than 5

    ip_end = remove_copy_if(listA.begin(), listA.end(), nums, bind2nd(less<int>(), 5));

    // contents of nums after remove_copy_if

    for (ip = &nums[0]; ip != ip_end; ip++) {
        cout << *ip << " ";
    }

    cout << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

