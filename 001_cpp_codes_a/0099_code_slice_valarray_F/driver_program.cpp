//========================//
// slice example //
//========================//

#include <iostream>
#include <cstddef>
#include <valarray>

using namespace std;

// the main code

int main()
{
    // 1. local variables and parameters

    const int DIMEN = 12;
    valarray<int> foo(DIMEN);
    int i;

    for (i = 0; i < DIMEN; i++) {
        foo[i] = i;
    }

    for (i = 0; i < DIMEN; i++) {
        cout << foo[i] << " ";
    }

    cout << endl;

    slice mySlice = slice(2,3,4);

    valarray<int> bar = foo[mySlice];

    cout << "slice(2,3,4):";

    for (size_t n = 0; n < bar.size(); n++) {
        cout << " " << bar[n];
    }

    cout << endl;

    return 0;
}

//======//
// FINI //
//======//
