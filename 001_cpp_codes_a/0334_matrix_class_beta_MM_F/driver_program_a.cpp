//=======================================//
// Driver program to the MatrixCL class. //
//=======================================//

#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cmath>

#include "MatrixCL.h"
#include "MatrixCLMemberFunctions.h"

#include "FunctionsFA.h"

#include "TypeDefinitions.h"

using namespace std;

int main()
{
    // local constants

    const TB  NUMROWS = 1E3;
    const TB  NUMCOLS = 1E3;
    const TB  TRIALS  = 10;
    const TB  DIGITS  = 20;
    const TA  DLOW    = 0.0;
    const TA  DHIGH   = 1.0;

    // local variables

    TA atmp;
    TB i, j, k;

    // some objects

    MatrixCL<TA,TB> matrixA;
    MatrixCL<TA,TB> matrixB;
    MatrixCL<TA,TB> matrixC;

    // seeding the random generator

    srand(static_cast<unsigned>(20));

    // setting the precision of the outputs

    cout << setprecision(DIGITS) << fixed << endl;

    // main test loop

    for (i = 0; i < TRIALS; i++) {
        cout << "------------------------------------->>> " << i << endl;

        // create the matrix

        matrixA.CreateF(NUMROWS, NUMCOLS);
        matrixB.CreateF(NUMROWS, NUMCOLS);
        matrixC.CreateF(NUMROWS, NUMCOLS);

        // reset the matrices

        matrixA.SetEqualZeroF();
        matrixB.SetEqualZeroF();
        matrixC.SetEqualZeroF();

        // set values to elements

        for (k = 0; k < NUMROWS; k++) {
            for (j = 0; j < NUMCOLS; j++) {
                atmp = sin(double(k)+double(j));
                // atmp = random_number(DLOW, DHIGH);
                // atmp = 5.0;
                matrixA.SetElementF(k, j, atmp);
                atmp = cos(double(k)+double(j));
                // atmp = random_number(DLOW, DHIGH);
                // atmp = 6.0;
                matrixB.SetElementF(k, j, atmp);
            }
        }

        // output some values

        cout << " some values " << endl;

        atmp = matrixA.GetElementF(i,i);
        cout << i << " --1--> " << i << " --> " << atmp << endl;

        atmp = matrixB.GetElementF(i,i);
        cout << i << " --1--> " << i << " --> " << atmp << endl;

        // set matrixB equal to matrixA

        cout << " SetEqualF " << endl;

        matrixB.SetEqualF(matrixA);

        atmp = matrixA.GetElementF(i,i);
        cout << i << " --2--> " << i << " --> " << atmp << endl;

        atmp = matrixB.GetElementF(i,i);
        cout << i << " --2--> " << i << " --> " << atmp << endl;

        // add matrixA to matrixB and get back matrixC

        cout << " AddF " << endl;

        matrixC.AddF(matrixB, matrixA);

        atmp = matrixA.GetElementF(i,i);
        cout << i << " --3--> " << i << " --> " << atmp << endl;

        atmp = matrixB.GetElementF(i,i);
        cout << i << " --3--> " << i << " --> " << atmp << endl;

        atmp = matrixC.GetElementF(i,i);
        cout << i << " --3--> " << i << " --> " << atmp << endl;

        // subtract matrixA from matrixB and get back matrixC

        cout << " SubtractF " << endl;

        matrixC.SubtractF(matrixB, matrixA);

        atmp = matrixA.GetElementF(i,i);
        cout << i << " --4--> " << i << " --> " << atmp << endl;

        atmp = matrixB.GetElementF(i,i);
        cout << i << " --4--> " << i << " --> " << atmp << endl;

        atmp = matrixC.GetElementF(i,i);
        cout << i << " --4--> " << i << " --> " << atmp << endl;

        // times matrixA by matrixB and get back matrixC

        matrixC.TimesF(matrixA, matrixB);

        cout << " TimesF " << endl;

        atmp = matrixA.GetElementF(i,i);
        cout << i << " --5--> " << i << " --> " << atmp << endl;

        atmp = matrixB.GetElementF(i,i);
        cout << i << " --5--> " << i << " --> " << atmp << endl;

        atmp = matrixC.GetElementF(i,i);
        cout << i << " --5--> " << i << " --> " << atmp << endl;

        // set matrixB and matrixA equal to a number

        cout << " SetEqualNumberF " << endl;

        atmp = 3.0;
        matrixA.SetEqualNumberF(atmp);

        atmp = 4.0;
        matrixB.SetEqualNumberF(atmp);

        atmp = matrixA.GetElementF(i,i);
        cout << i << " --6--> " << i << " --> " << atmp << endl;

        atmp = matrixB.GetElementF(i,i);
        cout << i << " --6--> " << i << " --> " << atmp << endl;

        // dot matrixA and matrixB

        cout << " DotF " << endl;

        matrixC.DotF(matrixA, matrixB);

        atmp = matrixA.GetElementF(9,10);
        cout << i << " --7--> " << i << " --> " << atmp << endl;

        atmp = matrixB.GetElementF(9,10);
        cout << i << " --7--> " << i << " --> " << atmp << endl;

        atmp = matrixC.GetElementF(9,10);
        cout << i << " --7--> " << i << " --> " << atmp << endl;

        // set equal to zero

        cout << " SetEqualZeroF " << endl;

        matrixA.SetEqualZeroF();

        atmp = matrixA.GetElementF(i,i);

        cout << i << " --8--> " << i << " --> " << atmp << endl;

        matrixB.SetEqualZeroF();

        atmp = matrixB.GetElementF(i,i);

        cout << i << " --8--> " << i << " --> " << atmp << endl;

        // give RAM back to system

        matrixA.DeleteF();
        matrixB.DeleteF();
    }

    return 0;

}

//==============//
// End of code. //
//==============//
