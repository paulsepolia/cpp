
//=============================//
// class DYNARR driver program //
//=============================//

#include <iostream>
#include <iomanip>
#include "DYNARR_declaration.h"
#include "DYNARR_definition.h"
#include "functions.h"

using std::cout;
using std::cin;
using std::endl;
using std::fixed;
using std::setprecision;
using std::showpos;
using std::showpoint;
using std::boolalpha;
using std::noboolalpha;

// test function

void testDYNARR();

// the main function

int main()
{
    // set the output format

    cout << fixed;
    cout << setprecision(5);
    cout << showpos;
    cout << showpoint;
    cout << boolalpha;

    // test the class

    cout << "This program tests the class DYNARR." << endl;

    char ans;

    do {
        testClass();
        cout << "Test again? (y/n) ";
        cin >> ans;

    } while ((ans == 'y') || (ans == 'Y'));

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

