
#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <iostream>
#include "DYNARR_declaration.h"
#include "DYNARR_definition.h"

using std::cout;
using std::endl;
using std::cin;

void testClass()
{
    int cap;
    cout << "Enter capacity of this array: ";
    cin >> cap;

    DYNARR temp(cap);

    cout << "Enter up to " << cap << " nonnegative numbers." << endl;
    cout << "Place a negative number at the end." << endl;

    double next;
    cin >> next;

    while ((next >= 0) && (!temp.full())) {
        temp.addElement(next);
        cin >> next;
    }

    cout << "You entered the following "
         << temp.getNumberUsed() << " numbers." << endl;

    int index;
    int count = temp.getNumberUsed();

    for (index = 0; index < count; index++) {
        cout << temp[index] << " ";
    }

    cout << endl;
    cout << "(plus a sentinel value.)" << endl;
}

//======//
// FINI //
//======//

#endif // FUNCTIONS_H

