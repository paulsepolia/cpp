#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main()
{
    // 1. variables and parameters

    const long long int DIMEN = 64ULL*1024ULL*1024ULL;
    const long long int I_DO_MAX = 32;
    const string fileName = "my_file.bin";
    fstream fileIO;
    double* myArray = new double [DIMEN];
    long long int i;

    // 2. open the file stream

    cout << " 1 --> Open the file stream" << endl;

    fileIO.open(fileName.c_str(), ios::out | ios::binary);

    // 3. test if the file stream is opened

    cout << " 2 --> Test if the file stream is opened with success" << endl;

    if (!fileIO.is_open()) {
        cout << "Error! The file stream is not opened. Exit." << endl;
        return -1;
    }

    // 4. build the array with some data

    cout << " 3 --> Build the array with some data" << endl;

    for (i = 0; i < DIMEN; i++) {
        myArray[i] = static_cast<double>(DIMEN-i);
    }

    // 5. write the contents of myArray[] to a file

    cout << " 4 --> Write to the file" << endl;

    for (i = 0; i < I_DO_MAX; i++) {
        fileIO.write(reinterpret_cast<char*>(&myArray[0]), DIMEN * sizeof(double));
    }

    // 6. close the file stream

    fileIO.close();

    // 7. free up the RAM

    delete [] myArray;

    return 0;
}

//======//
// FINI //
//======//
