//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/10/30              //
// Functions: hand made          //
// Algorithms: none              //
//===============================//

#include <iostream>
#include <algorithm>
#include <fstream>
#include <iterator>
#include <vector>

using namespace std;

// class definition

template <class T>
class compare {
public:
    T operator()(T x, T y) const
    {
        return x < y;
    }
};

// main

int main ()
{
    // variables declaration

    compare<int> v;
    const int I_MAX = 1000;
    int *aArray = new int [I_MAX];
    int i;
    vector<int> aVector;

    // build the array

    for (i = 0;  i < I_MAX; i++) {
        aArray[i] = static_cast<int>(I_MAX - i);
    }

    // some test for the handmade operator

    cout << v(2, 15) << endl;
    cout << compare<int>()(5,3) << endl;

    // before sorting

    copy(aArray, aArray+2, ostream_iterator<int>(cout, "\n"));
    cout << endl;

    // sort

    sort(aArray, aArray+I_MAX, compare<int>());

    // after sorting

    copy(aArray, aArray+2, ostream_iterator<int>(cout, "\n"));

    cout << endl;

    // copy the array to a vector

    aVector.resize(I_MAX);

    copy(aArray, aArray+I_MAX, aVector.begin());

    sort(aVector.begin(), aVector.end(), compare<int>());

    cout << "vector" << endl;

    copy(&aVector[0], &aVector[2], ostream_iterator<int>(cout, "\n"));

    cout << endl;


    return 0;
}

// end
