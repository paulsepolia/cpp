
//===========================//
// DerivedA class definition //
//===========================//

#include <iostream>
#include <cstdlib>
#include "base.h"
#include "derivedA.h"

using std::cout;
using std::endl;

namespace pgg {
// default constructor

DerivedA::DerivedA() : Base(), dimA(0), vecA(nullptr) {}

// constructor with two arguments

DerivedA::DerivedA(double val, long theDim) : Base(val, theDim), dimA(0), vecA(nullptr) {}

// copy constructor

DerivedA::DerivedA(const DerivedA& obj) : Base(obj), dimA(obj.dimA)
{
    delete [] DerivedA::vec;

    DerivedA::vec = new double [DerivedA::dim];

    for (long i = 0; i < DerivedA::dim; i++) {
        DerivedA::vec[i] = obj.vec[i];
    }

    delete [] vecA;

    vecA = new double [dimA];

    for (long i = 0; i < this->dimA; i++) {
        vecA[i] = obj.vecA[i];
    }

}

// destructor

DerivedA::~DerivedA()
{
    delete [] vec;
    this->vec = nullptr;
    delete [] vecA;
    vecA = nullptr;
}

// member functions

void DerivedA::createBackup()
{
    this->dimA = this->dim;

    this->vecA = new double [this->dimA];

    for (long i = 0; i < this->dimA; i++) {
        this->vecA[i] = this->vec[i];
    }
}

// member functions

void DerivedA::getBackup()
{
    delete [] this->vec;

    this->dim = this->dimA;

    this->vec = new double [this->dim];

    for (long i = 0; i < this->dim; i++) {
        this->vec[i] = this->vecA[i];
    }
}

// member function

void DerivedA::destroyBackup()
{
    delete [] vecA;
    vecA = nullptr;
    this->dimA = 0;
}

// member function

void DerivedA::destroy()
{
    delete [] this->vec;
    delete [] this->vecA;

    this->vec = nullptr;
    this->vecA = nullptr;
}

} // pgg

//======//
// FINI //
//======//
