
//=============//
// inheritance //
//=============//

#include <iostream>
#include "hourlyemployee.h"
#include "salariedemployee.h"

using std::cout;
using std::cin;
using std::endl;
using pgg::Employee;
using pgg::HourlyEmployee;
using pgg::SalariedEmployee;

// the main function

int main()

{
    // local variable and parameters

    // 1

    HourlyEmployee joe;

    joe.setName("Pavlos Joe");
    joe.setSsn("123-45-6789");
    joe.setRate(20.50);
    joe.setHours(40);

    cout << "Check for " << joe.getName()
         << " for " << joe.getHours() << " hours." << endl;

    joe.printCheck();

    cout << endl;

    // 2

    HourlyEmployee paul;

    cout << "Check for " << paul.getName()
         << " for " << paul.getHours() << " hours." << endl;

    paul.printCheck();

    cout << endl;

    // 3

    SalariedEmployee boss("Mr Big", "897-65-4321", 10500.50);

    cout << "Check for " << boss.getName() << endl;

    cout << boss.getName() << endl;
    cout << boss.getNetPay() << endl;
    cout << boss.getSalary() << endl;
    cout << boss.getSsn() << endl;

    boss.printCheck();

    // 4

    Employee andy;

    cout << "Check for " << andy.getName()
         << " for " << andy.getNetPay() << " Dollars." << endl;

    andy.printCheck();

    cout << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
