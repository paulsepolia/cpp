//==================================//
// Functions: put, getline, fstream //
//==================================//

//===============//
// code --> 0016 //
//===============//

// keywords: ifstream, ofstream, open, close, get, put

#include <iostream>
#include <fstream>
#include <string>

using std::cout;
using std::cin;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::string;
using std::ios;

// the main function

int main()
{
    // local variables and parameters

    ifstream inFile;
    ofstream outFile;
    char ch;
    int ID;
    string name;

    // open the uint

    inFile.open("input.txt", ios::in);
    outFile.open("output.txt", ios::out);

    // i/o game

    inFile >> ID;          // read the integer
    outFile << ID;         // write the integer
    inFile.get(ch);        // read the newline character
    outFile.put(ch);       // write the newline character
    getline(inFile, name); // read the name
    outFile << name;       // write the name
    inFile.get(ch);        // read the newline character
    outFile.put(ch);       // write the newline character

    // close the units

    inFile.close();
    outFile.close();

    cout << " --> end" << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    cout << " --> exit" << endl;

    return 0;
}

//======//
// FINI //
//======//

