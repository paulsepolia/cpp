
#include <iostream>
#include <string>

using std::string;
using std::cout;
using std::cin;
using std::endl;

// the class

class divisionByZero {
public:
    divisionByZero() : message ("Division by zero") {}
    explicit divisionByZero(string str) : message (str) {}
    string what() const
    {
        return message;
    }
private:
    string message;
};

// function

void doDivision() throw (divisionByZero)
{
    int dividend;
    int divisor;
    int quotient;

    try {
        cout << "Line 4: Enter the dividend: ";
        cin >> dividend;
        cout << endl;

        cout << "Line 7: Enter the divisor: ";
        cin >> divisor;
        cout << endl;

        if (divisor == 0) {
            throw divisionByZero("Found division by 0.");
        }

        quotient = dividend / divisor;

        cout << "Line 13: Quotient = " << quotient << endl;
    } catch (divisionByZero) {
        throw;
    }
}

// the main function

int main()
{
    try {
        doDivision();
    } catch (divisionByZero divByZeroObj) {
        cout << "Line 4: In main: " << divByZeroObj.what() << endl;
    }

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
