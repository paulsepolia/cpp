//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/10/12              //
// Functions: static, automatic  //
//===============================//

#include <iostream>

using namespace std;

// 1. functions declaration

void test();

// 2. main function

int main()
{
    int count;

    for (count = 1; count <= 5; count++) {
        test();
    }

    return 0;
}

// 3. functions definition

void test()
{
    static int x = 0;
    int y = 10;

    x = x + 2;
    y = y + 1;

    cout << " inside test, x = " << x << endl;
    cout << " inside test, y = " << y << endl;
}

//======//
// FINI //
//======//
