//=======================================//
// Author: Pavlos G. Galiatsatos         //
// Date: 2013/10/30                      //
// Functions: array, vector, deque, list //
// Algorithms: accumulate                //
//=======================================//

#include <iostream>
#include <algorithm>
#include <iterator>
#include <vector>
#include <list>
#include <deque>
#include <cmath>
#include <numeric>
#include <iomanip>

using namespace std;

// class definition

template <class T>
class fun {
public:
    fun();
    T operator()(T x, T y)
    {
        T u = x + i * y;
        i *= 2;
        return u;
    }

private:
    T i;
};


// global consts

const long int I_MAX = 5 * static_cast<long int>(10);

// main

int main ()
{
    // variables declaration

    int i;
    vector<double> aVector;
    list<double> aList;
    deque<double> aDeque;
    int j;
    int k;
    const int J_MAX = 2;
    double sumA = 1.0;
    double sumV = 1.0;
    double sumL = 1.0;
    double sumD = 1.0;
    const int K_MAX = 2;

    cout << fixed;
    cout << showpos;
    cout << showpoint;
    cout << setprecision(20);

    // build the array

    for (j = 0; j < J_MAX; j++) {
        cout << "------------------------------------------------>> " << j+1 << endl;

        cout << " 1 --> build the array" << endl;

        double *aArray = new double [I_MAX];

        for (i = 0;  i < I_MAX; i++) {
            aArray[i] = 1.0;
        }

        // copy the array to a vector

        cout << " 2 --> build the vector" << endl;

        aVector.resize(I_MAX);

        copy(aArray, aArray+I_MAX, aVector.begin());

        // copy the array to the list

        cout << " 3 --> build the list" << endl;

        aList.resize(I_MAX);

        copy(aArray, aArray+I_MAX, aList.begin());

        // copy the array to the deque

        cout << " 4 --> build the deque" << endl;

        aDeque.resize(I_MAX);

        copy(aList.begin(), aList.end(), aDeque.begin());

        // accumulate

        for (k = 0; k < K_MAX; k++) {
            sumA = accumulate(aArray, aArray+I_MAX, sumA, fun<double>());
            sumV = accumulate(aVector.begin(), aVector.end(), sumV, fun<double>());
            sumL = accumulate(aList.begin(), aList.end(), sumL, fun<double>());
            sumD = accumulate(aDeque.begin(), aDeque.end(), sumD, fun<double>());
        }

        cout << " sumA = " << sumA << endl;
        cout << " sumV = " << sumV << endl;
        cout << " sumL = " << sumL << endl;
        cout << " sumD = " << sumD << endl;

        // free up RAM

        cout << " Free up RAM" << endl;

        aVector.clear();
        aList.clear();
        aDeque.clear();
        delete [] aArray;
    }

    return 0;
}

// default constructor definition

template <class T>
fun<T>::fun() : i(0) { }

// end
