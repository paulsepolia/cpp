
// You may think that anyway you are seldom going to declare const objects
// and thus marking all members that don't modify the object as const is not worth the effort
// but const objects are actually very common.
// Most functions taking classes as parameters actually take them by const reference
// and thus, these functions can only access their const members

//===============//
// const objects //
//===============//

#include <iostream>

// a class

class MyClass {
public:

    MyClass (int val) : x (val) { }

    const int & get() const
    {
        return x;
    }

private:

    int x;
};

// a function

void print (const MyClass & arg)
{
    std::cout << arg.get() << std::endl;
}

// the main function

int main()
{
    MyClass foo (10);
    print (foo);

    int sentinel;
    std::cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
