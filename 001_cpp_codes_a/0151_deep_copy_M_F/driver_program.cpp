// copy constructor: deep copy

#include <iostream>
#include <string>

using namespace std;

class Example5 {
private:

    string * ptr;
    double * a1 = new double [100000];
    double * a2 = new double [100000];

public:

    Example5 (const string& str) : ptr(new string (str)) {} // constructor

    ~Example5 ()   // destructor
    {
        delete ptr;
        delete [] a1;
        delete [] a2;
    }

    Example5 () { } // constructor

    // copy constructor

    Example5 (const Example5& x) : ptr(new string (x.content())) {}

    // access content

    const string& content() const
    {
        return *ptr;
    }

    // assingment definition

    Example5 & operator = (const Example5& x)
    {
        ptr = new string ( x.content() );  // allocate space for new string, and copy
        return *this;
    }
};

int main ()
{
    Example5 foo ("Example");
    Example5 bar = foo; // use of copy constructor
    Example5 bar2;

    bar2 = foo; // assingment using the overloaded operator (assignment operator)
    // if I do not use the overloaded operator
    // then the class use its own default copy shallow constructor
    // so bar2 and foo point to the same address (their local string)
    // so when I exit I get back an error
    // since the destructor deleting 2 times the same memory location

    cout << "foo's content: " << foo.content() << " --> at address: "
         << &foo.content() << endl;

    cout << "bar's content: " << bar.content() << " --> at address: "
         << &bar.content() << endl;

    cout << "bar2's content: " << bar2.content() << " --> at address: "
         << &bar2.content() << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
