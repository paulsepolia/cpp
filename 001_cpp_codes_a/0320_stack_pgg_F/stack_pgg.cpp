
//=======================================//
// implementation of the stack_pgg class //
//=======================================//

#include "stack_pgg.h"

// default constructor

template <typename T, typename cont>
stack_pgg<T, cont>::stack_pgg() {}

// destructor

template <typename T, typename cont>
stack_pgg<T, cont>::~stack_pgg() {}

// assignment operator

template <typename T, typename cont>
stack_pgg<T, cont> & stack_pgg<T, cont>::operator = (const stack_pgg<T, cont> & s1)
{
    this->elems = s1.elems;

    return *this;
}

// copy constructor

template <typename T, typename cont>
stack_pgg<T, cont>::stack_pgg(const stack_pgg<T, cont> & s1)
{
    *this = s1;
}

// empty --> public member function

template <typename T, typename cont>
bool stack_pgg<T, cont>::empty() const
{
    return elems.empty();
}

// size --> public member function

template <typename T, typename cont>
size_t stack_pgg<T, cont>::size() const
{
    return elems.size();
}

// top --> public member function

template <typename T, typename cont>
T & stack_pgg<T, cont>::top()
{
    return elems.back();
}

// push --> public member function

template <typename T, typename cont>
void stack_pgg<T, cont>::push(const T & arg)
{
    elems.push_back(arg);
}

// pop --> public member function

template <typename T, typename cont>
void stack_pgg<T, cont>::pop()
{
    elems.pop_back();
}

// swap --> public member function

template <typename T, typename cont>
void stack_pgg<T, cont>::swap(stack_pgg<T, cont> & s1)
{
    stack_pgg<T, cont> stmp;

    stmp = s1;
    s1 = *this;
    *this = stmp;
}

// emplace --> public member function

template <typename T, typename cont>
void stack_pgg<T, cont>::emplace(const T & arg)
{
    elems.emplace_back(arg);
}

//======//
// FINI //
//======//

