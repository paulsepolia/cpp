#include <iostream>
#include <fstream>
#include <string>
#include <pthread.h>

using namespace std;

// function declaration

void * writeBinDouble(void *);

// main function

int main()
{
    // call the function

    pthread_t f2_thread;
    pthread_t f1_thread;

    string fileOut1;
    string fileOut2;

    fileOut1 = "fileDouble_A.bin";
    fileOut2 = "fileDouble_B.bin";

    // Create independent threads each of which
    // will execute the function and call the function

    pthread_create(&f1_thread, NULL, writeBinDouble, (void*) (&fileOut1));
    pthread_create(&f2_thread, NULL, writeBinDouble, (void*) (&fileOut2));

    // join

    pthread_join(f1_thread, NULL);
    pthread_join(f2_thread, NULL);

    return 0;
}

// function definition

void * writeBinDouble(void * argument)
{
    // variables declaration

    const long int I_MAX = 1000000000;
    string outFileName;
    ofstream outFile;
    double tmpA;
    long int i;

    // name of the output file

    outFileName = *((string *) argument);

    // open the stream for output

    outFile.open(outFileName.c_str(), ios::out | ios::binary);

    if (!outFile.is_open()) {
        cout << " The output stream is not opened. Exit" << endl;
    }

    // write data to the output file

    for (i = 0; i < I_MAX; i++) {
        tmpA = static_cast<double>(i);
        outFile.write((char*)&tmpA, sizeof(double));
    }

    // close the output stream

    outFile.close();

    return NULL;
}

// end

