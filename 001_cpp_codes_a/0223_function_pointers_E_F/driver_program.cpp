
//==========================//
// function pointer example //
//==========================//

#include <iostream>

using std::endl;
using std::cin;
using std::cout;

// function --> 1

void my_int_funcA(int x)
{
    cout << " --> void my_int_func(int x), has been called --> ";
    cout << x << endl;
}

// function --> 2

void my_int_funcB(int & x)
{
    cout << " --> void my_int_func(int & x), has been called --> ";
    cout << x << endl;
}

// function --> 3

double my_double_to_double_funcC (double x)
{
    cout << " --> double my_double_to_double_funcC (double x), has been called --> ";
    return 2*x;
}

// function --> 4

double my_double_to_double_funcD (double & x)
{
    cout << " --> double my_double_to_double_funcD (double & x), has been called --> ";
    return 2*x;
}

// the main function

int main()
{
    // local variables and parameters

    double x;
    int i;

    // function pointer declaration

    // *foo is a function
    // so foo is a pointer to a function
    // *foo is a function which takes int as argument and returns void

    void (*fooA)(int);
    void (*fooB)(int &);
    double (*fooC)(double);
    double (*fooD)(double &);

    // assign a function to a functin pointer
    // the ampersand is actually optional

    fooA = &my_int_funcA;
    fooB = &my_int_funcB;
    fooC = &my_double_to_double_funcC;
    fooD = &my_double_to_double_funcD;

    // get back some values

    // example --> 1

    (*fooA) (1);

    // example --> 2

    fooA (2);

    // example --> 3

    i = 3;

    fooB (i);

    // example --> 4

    i = 4;

    (*fooB) (i);

    // example --> 5

    x = 5.0;

    cout << (*fooC) (x) << endl;

    // example --> 6

    x = 6.0;

    cout << fooD (x) << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
