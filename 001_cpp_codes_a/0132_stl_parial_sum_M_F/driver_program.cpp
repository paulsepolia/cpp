//====================//
// factorials         //
// multiplies example //
//====================//

#include <iostream>
#include <functional>
#include <numeric>
#include <iomanip>

// using

using std::partial_sum;
using std::multiplies;
using std::endl;
using std::cin;
using std::cout;

// the main function

int main()
{
    const int DIM_MAX = 10;
    int numbers[DIM_MAX];
    int factorials[DIM_MAX];
    int i;

    // build numbers[]

    for (i = 0; i < DIM_MAX; i++) {
        numbers[i]=i+1;
    }

    // partial_sum
    // does partial multiplications and set the elements of factorials[]

    partial_sum(numbers, numbers+DIM_MAX, factorials, multiplies<int>());

    // report

    for (i = 0; i < DIM_MAX; i++) {
        std::cout << numbers[i] << "! is " << factorials[i] << endl;
    }

    // the sentinel

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

