
//=================//
// DerivedHeader2.h //
//=================//

#ifndef DERIVEDHEADER2_H
#define DERIVEDHEADER2_H

#include <string>
#include "BaseHeader.h"
#include "BaseDefinition.h"
#include "DerivedHeader.h"
#include "DerivedDefinition.h"

using pgg::base;
using pgg::derived;
using std::string;

namespace pgg {
class derived2 : public derived {
public:

    derived2();
    derived2(int, double, string, string);
    string getTicketType() const;
    void setTicketType(string);
    void resetTicket();

private:

    string ticketType;
};
} // pgg

#endif // DERIVEDHEADER2_H

//======//
// FINI //
//======//
