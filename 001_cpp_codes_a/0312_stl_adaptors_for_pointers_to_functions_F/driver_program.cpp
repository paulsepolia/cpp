
//=================//
// ptr_fun example //
//=================//

#include <iostream>
#include <functional>
#include <algorithm>
#include <cstdlib>
#include <numeric>
#include <iomanip>

using std::cout;
using std::cin;
using std::endl;
using std::ptr_fun;
using std::transform;
using std::accumulate;
using std::fixed;
using std::showpoint;
using std::showpos;
using std::setprecision;

// the main function

int main()
{
    // local variables and parameters

    char* foo[] = {"10","20","30","40","50"};
    double bar[5];
    double sum;

    // set the output format

    cout << fixed;
    cout << setprecision(20);
    cout << showpos;
    cout << showpoint;

    // apply atof to foo and write the result to bar
    // but atof is a function pointer so i use
    // prt_fun to get back a function object

    transform(foo, foo+5, bar, ptr_fun(&atof));

    // verify that everthing is okay

    sum = accumulate(bar, bar+5, 0.0);

    cout << "sum = " << sum << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
