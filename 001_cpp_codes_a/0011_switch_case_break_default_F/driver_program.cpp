//================================//
// Functions: switch, case, break //
//================================//

//===============//
// code --> 0011 //
//===============//

// keywords: switch, case, break, default

#include <iostream>
#include <iomanip>

using std::cout;
using std::cin;
using std::endl;
using std::setprecision;
using std::fixed;
using std::showpoint;

// named constants - residential customers

const double RES_BILL_PROC_FEES = 4.50;
const double RES_BASIC_SERV_COST = 20.50;
const double RES_COST_PREM_CHANNEL = 7.50;

// named constants - business customers

const double BUS_BILL_PROC_FEES = 15.00;
const double BUS_BASIC_SERV_COST = 75.00;
const double BUS_BASIC_CONN_COST = 5.00;
const double BUS_COST_PREM_CHANNEL = 50.00;

// the main function

int main()
{
    // 1 --> variable declaration

    int accountNumber;
    char customerType;
    int numOfPremChannels;
    double amountDue;

    // 2 --> set the output format

    cout << fixed;
    cout << showpoint;
    cout << setprecision(2);

    // 3 --> interface start

    cout << " --> This program computes a cable bill." << endl;
    cout << " --> Enter account number (an integer): ";

    cin >> accountNumber;

    cout << endl;

    cout << " --> Enter custumer type: R or r (Residential), B or b (Business): ";

    cin >> customerType;

    cout << endl;

    // 4 --> switch

    switch (customerType) {
    case 'r':
    case 'R':

        cout << "Enter the number of premium channels: ";

        cin >> numOfPremChannels;

        cout << endl;

        amountDue = RES_BILL_PROC_FEES  +
                    RES_BASIC_SERV_COST +
                    numOfPremChannels * RES_COST_PREM_CHANNEL;

        cout << "Account number: " << accountNumber << endl;

        cout << "Amount due: $" << amountDue << endl;

        break;

    case 'b':
    case 'B':

        cout << "1b" << endl;
        cout << "2b" << endl;

        break;

    default:

        cout << "Invalid customer type" << endl;

    }

    cout << " --> Exit" << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

