//======//
// List //
//======//

#include <iostream>
#include <list>
#include <cmath>
#include <ctime>
#include <iomanip>

int main()
{
    // 1. variable declarations

    const long int I_MAX = static_cast<long int>(pow(10.0, 7.2));
    std::list<double> listA;
    std::list<double> listB;
    std::list<double> listC;
    std::list<double> listD;
    long int i;
    int j;
    int J_MAX = 10000000;

    for (j = 1; j <= J_MAX; j++) {
        std::cout << "------------------------------------------------------------------>> " << j << std::endl;
        std::cout << std::endl;

        // 2. build the lists

        std::cout << "  1 --> build the lists" << std::endl;

        #pragma omp parallel sections \
        shared(listA, listB, listC, listD)\
        private(i)\
        shared(std::cout)
        {
            #pragma omp section
            {
                for (i = 0; i < I_MAX; i++)
                {
                    listA.push_back(cos(static_cast<double>(i+0)));
                }
            }
            #pragma omp section
            {
                for (i = 0; i < I_MAX; i++)
                {
                    listB.push_back(sin(static_cast<double>(i+1)));
                }
            }
            #pragma omp section
            {
                for (i = 0; i < I_MAX; i++)
                {
                    listC.push_back(sin(static_cast<double>(i+2)));
                }
            }
            #pragma omp section
            {
                for (i = 0; i < I_MAX; i++)
                {
                    listD.push_back(sin(static_cast<double>(i+3)));
                }
            }
        }

        // 3. get the size of the lists

        std::cout << std::endl;
        std::cout << "  2 --> The size of the lists is" << std::endl;
        std::cout << std::endl;
        std::cout << "  listA.size() = " << listA.size() << std::endl;
        std::cout << "  listB.size() = " << listB.size() << std::endl;
        std::cout << "  listC.size() = " << listC.size() << std::endl;
        std::cout << "  listD.size() = " << listD.size() << std::endl;
        std::cout << std::endl;

        // 4. sort the lists

        std::cout << "  3 --> Sort the lists" << std::endl;
        std::cout << std::endl;

        #pragma omp parallel sections \
        shared(listA, listB, listC, listD)\
        shared(std::cout)
        {
            #pragma omp section
            {
                std::cout << "  Sorting the listA ..." << std::endl;
                listA.sort();
            }
            #pragma omp section
            {
                std::cout << "  Sorting the listB ..." << std::endl;
                listB.sort();
            }
            #pragma omp section
            {
                std::cout << "  Sorting the listC ..." << std::endl;
                listC.sort();
            }
            #pragma omp section
            {
                std::cout << "  Sorting the listD ..." << std::endl;
                listD.sort();
            }
        }

        std::cout << std::endl;

        // 5. swap the two lists

        std::cout << "  4 --> Swap the 4 lists" << std::endl;
        std::cout << std::endl;

        #pragma omp parallel sections \
        shared(listA, listB, listC, listD)\
        shared(std::cout)
        {
            #pragma omp section
            {
                std::cout << "  swap listA with listB " << std::endl;
                listA.swap(listB);
            }
            #pragma omp section
            {
                std::cout << "  swap listC with listD " << std::endl;
                listC.swap(listD);
            }
        }

        std::cout << std::endl;

        // 6. pop-front elements from listA

        std::cout << "  5 --> Pop-front elements from the lists" << std::endl;
        std::cout << std::endl;

        #pragma omp parallel sections \
        shared(listA, listB, listC, listD)\
        private(i)\
        shared(std::cout)
        {
            #pragma omp section
            {
                std::cout << "  pop-back elements from listA " << std::endl;
                for (i = 0; i < I_MAX; i++)
                {
                    listA.pop_back();
                }
            }
            #pragma omp section
            {
                std::cout << "  pop-back elements from listB " << std::endl;
                for (i = 0; i < I_MAX; i++)
                {
                    listB.pop_back();
                }
            }
            #pragma omp section
            {
                std::cout << "  pop-back elements from listC " << std::endl;
                for (i = 0; i < I_MAX; i++)
                {
                    listC.pop_back();
                }
            }
            #pragma omp section
            {
                std::cout << "  pop-back elements from listD " << std::endl;
                for (i = 0; i < I_MAX; i++)
                {
                    listD.pop_back();
                }
            }
        }

        std::cout << std::endl;

        // 7. get the size of the lists

        std::cout << "  6 --> The size of the lists is" << std::endl;
        std::cout << std::endl;

        std::cout << "  listA.size() = " << listA.size() << std::endl;
        std::cout << "  listB.size() = " << listB.size() << std::endl;
        std::cout << "  listC.size() = " << listB.size() << std::endl;
        std::cout << "  listD.size() = " << listB.size() << std::endl;
        std::cout << std::endl;

        // 8. clear the lists

        std::cout << "  7 --> Clear the two lists " << std::endl;

        listA.clear();
        listB.clear();
        listC.clear();
        listD.clear();

        std::cout << std::endl;
    }

    return 0;
}

// FINI
