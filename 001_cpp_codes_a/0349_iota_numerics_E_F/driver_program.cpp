
//==============//
// iota example //
//==============//

#include <iostream>
#include <numeric>

using std::cout;
using std::cin;
using std::endl;
using std::iota;

// the main function

int main ()
{
    int numbers[10];

    iota (numbers, numbers+10, 100);

    cout << "numbers:";

    for (int& i:numbers) {
        cout << ' ' << i;
    }

    cout << endl;

    return 0;
}

// end
