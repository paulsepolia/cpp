
//=========================================//
// STL, adaptors for pointers to functions //
//=========================================//

#include <iostream>
#include <string>
#include <set>
#include <functional>
#include <iterator>

using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::set;
using std::pointer_to_binary_function;
using std::iterator;

// a function

bool less1(const string& x, const string& y)
{
    return (x < y);
}

// a function

bool greater1(const string& x, const string& y)
{
    return (x > y);
}

// new type definition

typedef set<string, pointer_to_binary_function<const string &, const string&, bool> >
set_type1;

// the main function

int main()
{
    // purpose

    cout << "Illustrating the use of an adaptor for "
         << "pointers to functions" << endl;

    // variables

    set_type1 set1(ptr_fun(&less1)); // ptr_fun converts function pointer
    // to function object

    set_type1::iterator its1;

    set_type1 set2(ptr_fun(&greater1)); // ptr_fun converts function pointer
    // to function object

    // set1

    set1.insert("the");
    set1.insert("quick");
    set1.insert("brown");
    set1.insert("fox");

    cout << endl;
    cout << "set1" << endl;
    cout << endl;

    for (its1 = set1.begin(); its1 != set1.end(); ++its1) {
        cout << "*its1 = " << *its1 << endl;
    }

    // set2

    set2.insert("the");
    set2.insert("quick");
    set2.insert("brown");
    set2.insert("fox");

    cout << endl;
    cout << "set2" << endl;
    cout << endl;

    for (its1 = set2.begin(); its1 != set2.end(); ++its1) {
        cout << "*its1 = " << *its1 << endl;
    }

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

