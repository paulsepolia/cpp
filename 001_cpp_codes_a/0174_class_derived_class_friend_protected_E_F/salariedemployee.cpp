//======================//
// salariedemployee.cpp //
//======================//

//===========================================================//
// This is the file salariedemployee.cpp                     //
// This is the implementation for the class SalariedEmployee //
//===========================================================//

#include <iostream>
#include <string>
#include "salariedemployee.h"

using std::string;
using std::cout;
using std::endl;

namespace pgg {
// default constructor

SalariedEmployee::SalariedEmployee() : Employee(), salary(0) {}

// non-default constructor

SalariedEmployee::SalariedEmployee(string theName, string theNumber, double theWeeklyPay)
    : Employee(theName, theNumber), salary(theWeeklyPay) {}

double SalariedEmployee::getSalary() const
{
    return salary;
}

void SalariedEmployee::setSalary(double newSalary)
{
    salary = newSalary;
}

void SalariedEmployee::printCheck()
{
    setNetPay(salary);

    cout << "\n---------------------------------------------------\n";
    cout << "Pay to the order of " << getName() << endl;
    cout << "The sum of " << getNetPay() << " Dollars\n";
    cout << "-----------------------------------------------------\n";
    cout << "Check stub: NOT NEGOTIABLE\n";
    cout << "Employee Number: " << getSsn() << endl;
    cout << "Salaried Employee. Regular Pay: " << salary << endl;
    cout << "-----------------------------------------------------\n";
}
}

//======//
// FINI //
//======//