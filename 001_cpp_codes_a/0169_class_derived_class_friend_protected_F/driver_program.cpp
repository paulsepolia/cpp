//============================//
// Base and derived class     //
// Public, Protected, Private //
//============================//

#include <iostream>

using std::cout;
using std::cin;
using std::endl;

// the base class

class Base {
private:

    int MyPrivateInt;

protected:

    int MyProtectedInt;

public:

    int MyPublicInt;
    Base() : MyPrivateInt (1),
        MyProtectedInt (2),
        MyPublicInt(3) {}; // my default constructor

};

// the derived class

class Derived : private Base {
public:

// int foo1() { return MyPrivateInt; }
// it is not okay, a derived class does not have access to private vars

    int foo2()
    {
        return MyProtectedInt;     // okay
    }
    int foo3()
    {
        return MyPublicInt;    // okay
    }
    Derived() {}; // the default constructor
};

// an unrelated class to the rest classes

class Unrelated {
private:

    Base B;

public:

    // it is not okay. Unrelated class has not access to private variables of Base and B
    //int foo1()  { return B.MyPrivateInt; }

    // it is not okay. Unrelated class has not access to protected variables of Base and B
    //int foo2()  { return B.MyProtectedInt; }

    int foo3()
    {
        return B.MyPublicInt;     // OK
    }

    Unrelated() : B() {};
};

int main()
{
    // Base class object

    Base BaseObj;

    cout << " --> BaseObj.MyPublicInt = " << BaseObj.MyPublicInt << endl;

    // Derived class object

    Derived DerivedObj;

    cout << " --> DerivedObj.foo2() = " << DerivedObj.foo2() << endl;

    cout << " --> DerivedObj.foo3() = " << DerivedObj.foo3() << endl;

    // Unrealted class object

    Unrelated UnrelatedObj;

    cout << " --> UnrelatedObj.foo3() = " << UnrelatedObj.foo3() << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;
}

//======//
// FINI //
//======//
