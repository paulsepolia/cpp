
//===================================//
// declaration of the class PFArrayD //
//===================================//

#ifndef PFARRAYD_H
#define PFARRAYD_H

namespace pgg {
class PFArrayD {
public:

    // default constructor

    PFArrayD(); // initializes with a capacity of 50

    // constructor with one argument

    explicit PFArrayD(int);

    // copy constructor

    PFArrayD(const PFArrayD&);

    // member functions

    void addElement(double);
    //precondition: the array is not full.
    //postcondition: the element has been added

    bool full() const;
    //returns true if the array is full, false otherwise.

    int getCapacity() const;

    int getNumberUsed() const;

    void emptyArray();
    // resets the number used to zero, effectively empyting the array.

    // operator [] overload

    double& operator[](int);
    // read and change access to elements 0 through numberUsed-1.

    // operator = overload

    PFArrayD& operator =(const PFArrayD&);

    // default destructor

    virtual ~PFArrayD();

protected:

    double * a; // for an array fo doubles
    int capacity; // for the size of the array
    int used; // for the number of array positions currently in use
};

} // pgg

#endif // PFARRAYD_H

//======//
// FINI //
//======//

