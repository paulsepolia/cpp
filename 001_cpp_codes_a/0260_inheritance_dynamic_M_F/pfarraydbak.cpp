
//=====================================//
// definition of the class PFArrayDBak //
//=====================================//

#include "pfarraydbak.h"
#include <iostream>

using std::cout;

namespace pgg {
// default constructor

PFArrayDBak::PFArrayDBak() : PFArrayD(), usedB(0) //, b(NULL)
{
    b = new double [capacity];
}

// constructor with one argument

PFArrayDBak::PFArrayDBak(int capacityValue) : PFArrayD(capacityValue), usedB(0) //, b(NULL)
{
    b = new double [capacity];
}

// copy constructor

PFArrayDBak::PFArrayDBak(const PFArrayDBak& oldObject) : PFArrayD(oldObject), usedB(0) //, b(NULL)
{
    b = new double [capacity];
    usedB = oldObject.usedB;

    for (int i = 0; i < usedB; i++) {
        b[i] = oldObject.b[i];
    }
}

// member function

void PFArrayDBak::backup()
{
    usedB = used;

    for (int i = 0; i < usedB; i++) {
        b[i] = a[i];
    }
}

// member function

void PFArrayDBak::restore()
{
    used = usedB;

    for (int i = 0; i < used; i++) {
        a[i] = b[i];
    }
}

// = operator overload

PFArrayDBak& PFArrayDBak::operator=(const PFArrayDBak& rightSide)
{
    int oldCapacity = capacity;
    PFArrayD::operator=(rightSide);

    if (oldCapacity != rightSide.capacity) {
        delete [] b;
        b = new double [rightSide.capacity];
    }

    usedB = rightSide.usedB;

    for (int i = 0; i < usedB; i++) {
        b[i] = rightSide.b[i];
    }

    return *this;
}

// destructor

PFArrayDBak::~PFArrayDBak()
{
    delete [] b;
}

} // pgg

//======//
// FINI //
//======//

