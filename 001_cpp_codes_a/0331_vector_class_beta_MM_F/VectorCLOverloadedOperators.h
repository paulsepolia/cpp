//=======================================================//
// Header file.                                          //
// VectorCL class member functions overloaded operators. //
//=======================================================//

//  1. '=' operator.

template <typename T, typename P>
VectorCL<T,P> VectorCL<T,P>::operator=(const VectorCL<T,P> vec)
{
    P i;

    if (this != &vec) {                     // 'this' points to the object
        // which calls the function operator=().
        for (i = 0; i < vec.m_Dim; i++) {
            *(m_Vec+i) = vec.GetElementF(i);
        }

        m_Dim = vec.m_Dim;
    }

    return *this;
}

//  2. '+' operator.

template <typename T, typename P>
VectorCL<T,P> VectorCL<T,P>::operator+(VectorCL<T,P> vec)
{
    P i;
    T atmp;

    for (i = 0; i < vec.m_Dim; i++) {
        atmp = *(m_Vec+i) + vec.GetElementF(i);
        vec.SetElementF(i, atmp);
    }

    return vec;
}

//  3. '+=' operator.

template <typename T, typename P>
VectorCL<T,P>& VectorCL<T,P>::operator+=(VectorCL<T,P>& vec)
{
    P i;
    T atmp;

    for (i = 0; i < vec.m_Dim; i++) {
        *(m_Vec+i) = *(m_Vec+i) + vec.GetElementF(i);
    }

    return vec;
}

//==============//
// End of code. //
//==============//
