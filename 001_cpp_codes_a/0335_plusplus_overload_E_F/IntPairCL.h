//==============//
// Header file. //
// IntPairCL.h  //
//==============//

//==========================================//
// Overloading the ++ operator. An example. //
//==========================================//

//============================//
// IntPair class declaration. //
//============================//

class IntPairCL {
public:
    IntPairCL(int firstValue, int secondValue);  // constructor
    IntPairCL operator++();                      // prefix version
    IntPairCL operator++(int);                   // postfix version
    void setFirst(int newValue);                 // mutator
    void setSecond(int newValue);                // mutator
    int getFirst() const;                        // accessor
    int getSecond() const;                       // accessor

private:
    int first;
    int second;
};

//==============//
// End of code. //
//==============//
