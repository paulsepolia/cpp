//=======//
// array //
//=======//

#include <iostream>
#include <array>
#include <cmath>

using namespace std;

int main ()
{

    long int k;
    const long int K_MAX = static_cast<long int>(pow(10.0, 1.0));
    constexpr  long int ARR_DIM = 400000000L;
    long int j;
    const long int J_MAX = 100000L;

    for (j = 1; j <= J_MAX; j++) {
        cout << "---------------------------------------------------------------->>> " << j << endl;

        cout << endl;
        cout << "  1 --> Declare the array of size " << ARR_DIM << endl;

        array<double, ARR_DIM> * arrayA = new array<double, ARR_DIM>;

        cout << "  2 --> Fill the array " << K_MAX << " times with various elements " << endl;

        for (k = 1; k <= K_MAX; k++) {
            arrayA->fill(static_cast<double>(k+j));
        }

        cout << "  3 --> Some array elements " << endl;
        cout << endl;
        cout << "  --->> arrayA[0] = " << (*arrayA)[0] << endl;
        cout << "  --->> arrayA[1] = " << (*arrayA)[1] << endl;
        cout << "  --->> arrayA[2] = " << (*arrayA)[2] << endl;
        cout << endl;

        cout << "  4 --> delete the pointer array" << endl;

        delete arrayA;
    }

    return 0;
}

//======//
// FINI //
//======//

