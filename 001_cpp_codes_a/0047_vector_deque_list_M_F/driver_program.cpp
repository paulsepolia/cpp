//================================//
// Author: Pavlos G. Galiatsatos  //
// Date: 2013/10/23               //
// Functions: vector, deque, list //
//================================//

#include <iostream>
#include <vector>
#include <deque>
#include <list>
#include <iomanip>
#include <ctime>

using namespace std;

int main ()
{
    // variables declaration

    vector<double> aVector;
    deque<double> aDeque;
    list<double> aList;
    double *p;
    long int i;
    long int j;
    const long int J_MAX_DO = 0.5 * static_cast<long int>(100);
    const long int MAX_ELEM = 3 * static_cast<long int>(100000000);
    clock_t t1;
    clock_t t2;

    // set up the output style

    cout << fixed;
    cout << setprecision(10);
    cout << showpos;
    cout << showpoint;

    // allocate an array with space
    // for MAX_LEM elements using vector's allocator

    cout << " vector --> allocate, construct, destroy, deallocate " << endl;

    t1 = clock();

    for (j = 0; j < J_MAX_DO; j++) {
        p = aVector.get_allocator().allocate(MAX_ELEM);

        // construct values in-place on the array

        for (i = 0; i < MAX_ELEM; i++) {
            aVector.get_allocator().construct(&p[i], static_cast<double>(i));
        }

        // some output

//    cout << "The allocated vector contains (0,3):" << endl;
//
//    for (i = 0; i < 3; i++)
//    { cout << " --> " << i << " --> " << p[i] << endl; }

        // destroy element by element

        for (i = 0; i < MAX_ELEM; i++) {
            aVector.get_allocator().destroy(&p[i]);
        }

        // deallocate the vector RAM

        aVector.get_allocator().deallocate(p, MAX_ELEM);
    }

    t2 = clock();

    cout << " time for vector --> " << (t2-t1)/CLOCKS_PER_SEC << endl;

    cout << " deque --> allocate, construct, destroy, deallocate " << endl;

    t1 = clock();

    for (j = 0; j < J_MAX_DO; j++) {
        p = aDeque.get_allocator().allocate(MAX_ELEM);

        // construct values in-place on the array

        for (i = 0; i < MAX_ELEM; i++) {
            aDeque.get_allocator().construct(&p[i], static_cast<double>(i));
        }

        // some output

//    cout << "The allocated deque contains (0,3):" << endl;
//
//    for (i = 0; i < 3; i++)
//    { cout << " --> " << i << " --> " << p[i] << endl; }

        // destroy element by element

        for (i = 0; i < MAX_ELEM; i++) {
            aDeque.get_allocator().destroy(&p[i]);
        }

        // deallocate the vector RAM

        aDeque.get_allocator().deallocate(p, MAX_ELEM);
    }

    t2 = clock();

    cout << " time for deque --> " << (t2-t1)/CLOCKS_PER_SEC << endl;

    cout << " list --> allocate, construct, destroy, deallocate " << endl;

    t1 = clock();

    for (j = 0; j < J_MAX_DO; j++) {
        p = aList.get_allocator().allocate(MAX_ELEM);

        // construct values in-place on the array

        for (i = 0; i < MAX_ELEM; i++) {
            aList.get_allocator().construct(&p[i], static_cast<double>(i));
        }

        // some output

//    cout << "The allocated list contains (0,3):" << endl;
//
//    for (i = 0; i < 3; i++)
//    { cout << " --> " << i << " --> " << p[i] << endl; }

        // destroy element by element

        for (i = 0; i < MAX_ELEM; i++) {
            aList.get_allocator().destroy(&p[i]);
        }

        // deallocate the vector RAM

        aList.get_allocator().deallocate(p, MAX_ELEM);
    }

    t2 = clock();

    cout << " time for list --> " << (t2-t1)/CLOCKS_PER_SEC << endl;

    return 0;
}

// end
