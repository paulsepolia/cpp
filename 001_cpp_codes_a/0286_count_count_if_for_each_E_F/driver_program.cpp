
//================================//
// stl, count, count_if, for_each //
//================================//

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <cassert>
#include <cmath>
#include <iterator>
#include <cstdlib>
#include <functional>

using std::cout;
using std::cin;
using std::endl;

using std::setprecision;
using std::fixed;
using std::showpoint;
using std::showpos;

using std::count;
using std::count_if;
using std::for_each;

using std::pow;
using std::iterator;
using std::exit;

using std::bind2nd;
using std::not_equal_to;
using std::greater;
using std::greater_equal;
using std::less;
using std::less_equal;

// function

template <class T>
void print_p(const T& x)
{
    static int c = 0;

    c++;

    cout << " --> " << c << " --> " << x << endl;


    if (c == 100) {
        cout << " Enter an integer to exit: ";
        int sentinel;
        cin >> sentinel;
        exit(-1);
    }
}

// function

template <class T>
double double_p(T& x)
{
    x = 2 * x;
    return x;
}


// the main function

int main()
{
    // local variables and parameters

    const long long DIM_MAX = static_cast<long long>(pow(10.0, 8.0));
    const long long K_MAX = static_cast<long long>(pow(10.0, 5.0));
    int sentinel;
    long long counter = 0;

    // adjust the output format

    cout << fixed;
    cout << setprecision(20);
    cout << showpoint;
    cout << showpos;

    // main for loop

    for (long long k = 0; k < K_MAX; k++) {
        cout << "-------------------------------------------------->> " << k << endl;

        // allocate the RAM

        cout << "  1 --> allocate RAM" << endl;

        double * a = new double [DIM_MAX];

        // build the array

        cout << "  2 --> build the array" << endl;

        for (long long i = 0; i < DIM_MAX; i++) {
            a[i] = cos(static_cast<double>(i));
        }

        // count

        cout << "  3 --> count how many elements are equal to cos(0.0)" << endl;

        counter = count(&a[0], &a[DIM_MAX], cos(0.0));

        cout << "    --> counter = " << counter << endl;

        // count_if

        cout << "  4 --> count how many elements are not equal to -2.0" << endl;

        counter = count_if(&a[0], &a[DIM_MAX], bind2nd(not_equal_to<double>(), -2.0));

        cout << "    --> counter = " << counter << endl;

        // count_if

        cout << "  5 --> count how many elements are greater_equal 0.0" << endl;

        counter = count_if(&a[0], &a[DIM_MAX], bind2nd(greater_equal<double>(), 0.0));

        cout << "    --> counter = " << counter << endl;

        // count_if

        cout << "  6 --> count how many elements are greater 0.0" << endl;

        counter = count_if(&a[0], &a[DIM_MAX], bind2nd(greater<double>(), 0.0));

        cout << "    --> counter = " << counter << endl;

        // count_if

        cout << "  7 --> count how many elements are less_equal 0.0" << endl;

        counter = count_if(&a[0], &a[DIM_MAX], bind2nd(less_equal<double>(), 0.0));

        cout << "    --> counter = " << counter << endl;

        // count_if

        cout << "  8 --> count how many elements are less 0.0" << endl;

        counter = count_if(&a[0], &a[DIM_MAX], bind2nd(less<double>(), 0.0));

        cout << "    --> counter = " << counter << endl;

        // for_each

        cout << "  9 --> takes the double of each element" << endl;

        for_each(&a[0], &a[DIM_MAX], double_p<double>);

        cout << "    --> counter = " << counter << endl;


        // for_each

        cout << " 10 --> prints out each element" << endl;

        for_each(&a[0], &a[DIM_MAX], print_p<double>);

        cout << "    --> counter = " << counter << endl;

        // delete

        delete [] a;

    }

    // sentineling

    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
