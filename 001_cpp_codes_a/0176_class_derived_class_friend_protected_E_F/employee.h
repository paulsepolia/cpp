//============//
// employee.h //
//============//

//==========================================================//
// This is the header file employee.h                       //
// This is the interface for the class Employee             //
// This is primarily intended to be used as a base class to //
// derive classes for different kinds of employees.         //
//==========================================================//

#ifndef EMPLOYEE_H
#define EMPLOYEE_H

#include <string>
using std::string;

namespace pgg {
class Employee {
public:
    Employee(); // default constructor
    Employee(string, string); // a non-default constructor
    string getName() const;
    string getSsn() const;
    double getNetPay() const;
    void setName(string);
    void setSsn(string);
    void setNetPay(double);
    void printCheck() const;
private:
    string name;
    string ssn;
    double netPay;
};
} // pgg

#endif // EMPLOYEE_H

//======//
// FINI //
//======//