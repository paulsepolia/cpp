//========================//
// base and derived class //
//========================//

#include <iostream>
#include "employee.h"
#include "hourlyemployee.h"
#include "salariedemployee.h"

// namespaces

using std::cout;
using std::endl;
using std::cin;
using pgg::Employee;
using pgg::HourlyEmployee;
using pgg::SalariedEmployee;

// the main function

int main()
{
    HourlyEmployee joe;

    joe.setName("Pavlos G. Galiatsatos");

    joe.setSsn("123-45-6789");

    joe.setRate(20.50);

    joe.setHours(40);

    cout << " Check for " << joe.getName() << " for " << joe.getHours() << " hours.\n";

    joe.printCheck();

    cout << endl;

    SalariedEmployee boss("Mr Big Shot", "987-654-3210", 10500.50);

    cout << " Check for " << boss.getName() << endl;

    boss.printCheck();



    int sentinel;
    cin >> sentinel;
    return 0;
}

//======//
// FINI //
//======//
