
//=======================================//
// implementation of the class queue_pgg //
//=======================================//

#include "queue_pgg.h"
#include <algorithm>

using std::reverse;

// default constructor

template <typename T, typename CONT>
queue_pgg<T, CONT>::queue_pgg() {}

// destructor

template <typename T, typename CONT>
queue_pgg<T, CONT>::~queue_pgg() {}

// assignment operator

template <typename T, typename CONT>
template <typename T2, typename CONT2>
queue_pgg<T, CONT> & queue_pgg<T, CONT>::operator = (const queue_pgg<T2, CONT2> & s1)
{
    if ((void*) this == (void*) & s1) { // avoid to assignment to itself
        return *this;
    }

    queue_pgg<T2, CONT2> tmp(s1);

    elems.clear();

    while (!tmp.empty()) {
        elems.push_front(tmp.front());
        tmp.pop();
    }

    return *this;
}

// copy constructor

template <typename T, typename CONT>
template <typename T2, typename CONT2>
queue_pgg<T, CONT>::queue_pgg(const queue_pgg<T2, CONT2> & s1)
{
    *this = s1; // assignment operator is already defined
}

// empty --> public member function

template <typename T, typename CONT>
bool queue_pgg<T, CONT>::empty() const
{
    return elems.empty();
}

// size --> public member function

template <typename T, typename CONT>
size_t queue_pgg<T, CONT>::size() const
{
    return elems.size();
}

// front --> public member function

template <typename T, typename CONT>
T & queue_pgg<T, CONT>::front()
{
    return elems.front();
}

// back --> public member function

template <typename T, typename CONT>
T & queue_pgg<T, CONT>::back()
{
    return elems.back();
}

// push --> public member function

template <typename T, typename CONT>
void queue_pgg<T, CONT>::push(const T & arg)
{
    elems.push_back(arg);
}

// pop --> public member function

template <typename T, typename CONT>
void queue_pgg<T, CONT>::pop()
{
    elems.pop_front();
}

// swap --> public member function

template <typename T, typename CONT>
template <typename T2, typename CONT2>
void queue_pgg<T, CONT>::swap(queue_pgg<T2, CONT2> & s1)
{
    queue_pgg<T, CONT> stmp;

    stmp = s1;
    s1 = *this;
    *this = stmp;
}

// emplace --> public member function

template <typename T, typename CONT>
void queue_pgg<T, CONT>::emplace(const T & arg)
{
    elems.emplace_back(arg);
}

//======//
// FINI //
//======//

