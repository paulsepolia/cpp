//==========//
// STL Sort //
//==========//

#include <iostream>
#include <algorithm>
#include <array>
#include <vector>
#include <list>
#include <deque>
#include <cmath>
#include <iomanip>
#include <ctime>

// A. sorting function

bool mySortFun (double i, double j)
{
    return (i<j);
}

// B. sorting class

class mySortClass {
public:
    bool operator() (double i, double j)
    {
        return (i<j);
    }
};

mySortClass mySortObj;

// C. the main program

int main()
{
    // 1. variables and parameters

    const long int I_MAX = static_cast<long int>(pow(10.0, 7.5));
    const long int K_MAX = static_cast<long int>(pow(10.0, 6.0));
    long int i;
    long int k;
    std::list<double> listA;
    std::vector<double> vecA;
    std::deque<double> deqA;
    clock_t t1;
    clock_t t2;

    std::cout << std::fixed;
    std::cout << std::setprecision(5);
    std::cout << std::showpoint;

    // 2. main bench loop

    for (k = 0; k < K_MAX; k++) {
        std::cout << std::endl;
        std::cout << "------------------------------------------------>>> " << k << std::endl;
        std::cout << std::endl;

        double* arrayA = new double [I_MAX];

        //=======//
        // BUILD //
        //=======//

        #pragma omp parallel sections \
        shared(arrayA, vecA, listA, deqA, std::cout)\
        private(t1, t2, i)
        {
            #pragma omp section
            {
                // 3. building an array

                t1 = clock();
                std::cout << "  1 --> building the dynamic array arrayA" << std::endl;
                for (i = 0; i < I_MAX; i++)
                {
                    arrayA[i] = static_cast<double>(cos(i));
                }
                t2 = clock();
                std::cout << "    --> done with dynamic array --> arrayA --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 4. building the vector

                t1 = clock();
                std::cout << "  2 --> building the vector vecA" << std::endl;
                for (i = 0; i < I_MAX; i++)
                {
                    vecA.push_back(static_cast<double>(cos(i)));
                }
                t2 = clock();
                std::cout << "    --> done with vector --> vecA --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 5. building the list

                t1 = clock();
                std::cout << "  3 --> building the list listA" << std::endl;
                for (i = 0; i < I_MAX; i++)
                {
                    listA.push_back(static_cast<double>(cos(i)));
                }
                t2 = clock();
                std::cout << "    --> done with list --> listA --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 6. building the deque

                t1 = clock();
                std::cout << "  4 --> building the deque dequeA" << std::endl;
                for (i = 0; i < I_MAX; i++)
                {
                    deqA.push_back(static_cast<double>(cos(i)));
                }
                t2 = clock();
                std::cout << "    --> done with deque --> dequeA --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
        }

        //======//
        // SORT //
        //======//

        #pragma omp parallel sections \
        shared(arrayA, vecA, listA, deqA, std::cout)\
        private(t1, t2)
        {
            #pragma omp section
            {
                // 7. sorting the array

                t1 = clock();
                std::cout << "  5 --> sorting the dynamic array arrayA" << std::endl;
                std::sort(arrayA, arrayA+I_MAX);
                t2 = clock();
                std::cout << "    --> done with dynamic array --> arrayA --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 8. sorting the vector vecA

                t1 = clock();
                std::cout << "  6 --> sorting the vector vecA" << std::endl;
                std::sort(vecA.begin(), vecA.end());
                t2 = clock();
                std::cout << "    --> done with vector --> vecA --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 9. sorting the list listA

                t1 = clock();
                std::cout << "  7 --> sorting the list listA" << std::endl;
                listA.sort();
                t2 = clock();
                std::cout << "    --> done with list --> listA --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 10. sorting the deque deqA

                t1 = clock();
                std::cout << "  8 --> sorting the deque deqA" << std::endl;
                std::sort(deqA.begin(), deqA.end());
                t2 = clock();
                std::cout << "    --> done with deque --> deqA --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
        }

        //===================//
        // Clearing/Deleting //
        //===================//

        #pragma omp parallel sections \
        shared(arrayA, vecA, listA, deqA, std::cout)\
        private(t1, t2)
        {
            #pragma omp section
            {
                // 11. clearing/deleting the array arrayA

                t1 = clock();
                std::cout << "  9 --> clearing (deleting) the dynamic array arrayA" << std::endl;
                delete [] arrayA;
                t2 = clock();
                std::cout << "    --> done with dynamic array --> arrayA --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 12. clearing the vector vecA

                t1 = clock();
                std::cout << " 10 --> clearing the vector vecA" << std::endl;
                vecA.clear();
                t2 = clock();
                std::cout << "    --> done with vector --> vecA --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 13. clearing the list listA

                t1 = clock();
                std::cout << " 11 --> clearing the list listA" << std::endl;
                listA.clear();
                t2 = clock();
                std::cout << "    --> done with list --> listA --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 14. clearing the deque deqA

                t1 = clock();
                std::cout << " 12 --> clearing the deque deqA" << std::endl;
                deqA.clear();
                t2 = clock();
                std::cout << "    --> done with deq --> deqA --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
        }
    }

    int sentinel;
    std::cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
