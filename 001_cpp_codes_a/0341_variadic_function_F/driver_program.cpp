
//===================//
// Variadic function //
//===================//

//===============//
// code --> 0341 //
//===============//

#include <iostream>
#include <cmath>
#include <iomanip>
#include <functional>
#include <cstdarg>

using std::cout;
using std::cin;
using std::endl;
using std::pow;
using std::fixed;
using std::showpoint;
using std::showpos;
using std::setprecision;
using std::boolalpha;

// the variadic function

void simple_printf(const char* fmt...)
{
    va_list args;
    va_start(args, fmt);

    while (*fmt != '\0') {
        if (*fmt == 'd') {
            int i = va_arg(args, int);
            std::cout << i << '\n';
        } else if (*fmt == 'c') {
            // note automatic conversion to integral type
            int c = va_arg(args, int);
            std::cout << static_cast<char>(c) << '\n';
        } else if (*fmt == 'f') {
            double d = va_arg(args, double);
            std::cout << d << '\n';
        }

        ++fmt;
    }

    va_end(args);
}

// the main function

int main()
{
    simple_printf("dcff", 3, 'a', 1.999, 42.5);

    return 0;
}

//======//
// FINI //
//======//
