
//===================//
// ClassA definition //
//===================//

#include <iostream>
#include "classb.h"

using std::cout;
using std::endl;
using pgg::ClassB;

// constructor

ClassB::ClassB() : a(0.0), b(0.0)
{
    c = new double;
    *c = 0.0;
}

// constructor with two arguments

ClassB::ClassB(double aLoc, double bLoc) : a(aLoc), b(bLoc)
{
    c = new double;
    *c = 0.0;
}

// constructor with three arguments

ClassB::ClassB(double aLoc, double bLoc, double cLoc) : a(aLoc), b(bLoc)
{
    c = new double;
    *c = cLoc;
}

// copy constructor

ClassB::ClassB(const ClassB& obj)
{
    a = obj.a;
    b = obj.b;
    c = new double;
    *c = *(obj.c);
}

// destructor

ClassB::~ClassB()
{
    delete c;
    c = NULL;
}

// member function

void ClassB::print() const
{
    cout << "I am the virtual void print() in ClassB : a  = " <<  a << endl;
    cout << "I am the virtual void print() in ClassB : b  = " <<  b << endl;
    cout << "I am the virtual void print() in ClassB : *c = " << *c << endl;
}

// member function

double ClassB::add() const
{
    cout << "I am the virtual double add() in ClassB : a+b+(*c) = ";

    return (a+b+(*c));
}

// member function

double ClassB::multiply() const
{
    cout << "I am the virtual double multiply() in ClassB : a*b*(*c) = ";

    return (a*b*(*c));
}

// member function

void ClassB::locB() const
{
    cout << "I am the locB function." << endl;

}

// member function

double ClassB::value() const
{
    return a;
}

// < operator overload
// member function function

bool ClassB::operator < (const InterfaceA& obj) const
{
    return (this->value() < obj.value());
}

// <= operator overload
// member function function

bool ClassB::operator <= (const InterfaceA& obj) const
{
    return (this->value() <= obj.value());
}

// > operator overload
// member function function

bool ClassB::operator > (const InterfaceA& obj) const
{
    return (this->value() > obj.value());
}

// >= operator overload
// member function function

bool ClassB::operator >= (const InterfaceA& obj) const
{
    return (this->value() >= obj.value());
}

// == operator overload
// member function function

bool ClassB::operator == (const InterfaceA& obj) const
{
    bool tmp;

    tmp = (this->value() == obj.value());

    return tmp;
}

// == operator overload
// member function function

bool ClassB::operator == (const ClassB& obj) const
{
    bool tmp;

    tmp = (this->value() == obj.value()) && ((this->b) == obj.b) && (*(this->c) == *(obj.c));

    return tmp;
}

// + operator overload
// member function function

ClassB ClassB::operator + (const ClassB& obj) const
{
    return ClassB(this->value() + obj.value(), b);
}

// - operator overload
// member function function

ClassB ClassB::operator - (const ClassB& obj) const
{
    return ClassB(this->value() - obj.value(), b);
}

// * operator overload
// member function function

ClassB ClassB::operator * (const ClassB& obj) const
{
    return ClassB(this->value() * obj.value(), b);
}

// / operator overload
// member function function

ClassB ClassB::operator / (const ClassB& obj) const
{
    return ClassB(this->value() / obj.value(), b);
}

// ++ operator overload - prefix
// member function

const ClassB ClassB::operator++()
{
    a++;
    return ClassB(a, b);
}

// ++ operator overload - postfix
// member function

const ClassB ClassB::operator++(int)
{
    a++;
    return ClassB(a, b);
}

// -- operator overload - prefix
// member function

const ClassB ClassB::operator--()
{
    a--;
    return ClassB(a, b);
}

// -- operator overload - postfix
// member function

const ClassB ClassB::operator--(int)
{
    a--;
    return ClassB(a, b);
}

// - operator overload
// member function

const ClassB ClassB::operator-() const
{
    return ClassB(-a, -b);
}

// += operator overload
// member function

const ClassB ClassB::operator += (const ClassB& obj)
{
    return ClassB(a + obj.value(), b);
}

// -= operator overload
// member function

const ClassB ClassB::operator -= (const ClassB& obj)
{
    return ClassB(a - obj.value(), b);
}

// *= operator overload
// member function

const ClassB ClassB::operator *= (const ClassB& obj)
{
    return ClassB(a * obj.value(), b);
}

// /= operator overload
// member function

const ClassB ClassB::operator /= (const ClassB& obj)
{
    return ClassB(a / obj.value(), b);
}

// = operator overload
// member function

const ClassB ClassB::operator = (const ClassB& obj)
{
    this->a = obj.a;
    this->b = obj.b;

    this->c = new double;

    *(this->c) = *(obj.c);

    return *this;
}

//======//
// FINI //
//======//
