//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/10/17              //
// Functions: Class              //
//===============================//

#include <iostream>
#include "dieHeader.h"

using namespace std;

int main()
{
    // 1. variable declaration

    die die1;
    die die2;

    cout << " die1: " << die1.getNum() << endl;
    cout << " die2: " << die2.getNum() << endl;

    // 2. after rolling the die

    cout << " after rolling die1: " << endl;
    cout << die1.roll() << endl;

    cout << " after rolling die2: " << endl;
    cout << die2.roll() << endl;

    cout << " The sum of them is: " << endl;
    cout << die1.getNum() + die2.getNum() << endl;

    cout << " The sum after rolling again is: " << endl;
    cout << die1.roll() + die2.roll() << endl;

    return 0;
}

//======//
// FINI //
//======//
