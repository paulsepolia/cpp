//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/10/17              //
// Functions: Class              //
//===============================//

// class declaration

class die {
public:
    die();
    // default constructor
    // sets the default number rolled by a die to 1

    int roll();
    // function to roll a die
    // this function uses a random number generator to randomly
    // generate a number between 1 and 6, and stores the number
    // in the instance variable num and returns the number

    int getNum() const;
    // Function to return the number on the top face of die
    // Returns the value of the instance variable num

private:
    int num;
};

//======//
// FINI //
//======//
