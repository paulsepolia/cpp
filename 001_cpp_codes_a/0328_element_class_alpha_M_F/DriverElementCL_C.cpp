//=======================================//
// Driver program to the ElementCL class //
//=======================================//

#include <iostream>
#include <iomanip>
#include <cmath>
#include "ElementCL.h"
#include "ElementCLMemberFunctions.h"
#include "TypeDefinitions.h"

using namespace std;

int main()
{
    //  1a. local variables

    TA v1;
    TA v2;
    TA v3;
    TA iTMP;
    TA iA;
    TA iB;
    TA iC;
    int i;
    int j;

    //  1b. Local constants

    const int I_MAX = 200000;
    const int J_MAX = 200000;

    //  2. local objects

    ElementCL<TA> oA;
    ElementCL<TA> oB;
    ElementCL<TA> oC;
    ElementCL<TA> oTMP;

    //  3. set the output format

    cout << setprecision(15) << fixed << endl;

    //  4. set values to objects oA, oB, oC, oTMP

    v1 = static_cast<TA>(11.1111111100000000000);
    oA.SetElementF(v1);

    v2 = static_cast<TA>(22.2222222200000000000);
    oB.SetElementF(v2);

    oC.SetEqualZeroF();
    oTMP.SetEqualZeroF();

    // test --> 1
    // Serial for loop.
    // I ask the compiler to parallelize it, if possible

    cout << " test --> 1 --> starts " << endl;

    // THE FOLLOWING OPENMP PARALLELIZATION IS WRONG
    // oC MUST BE reduction(+:oC)
    // oc MUST NOT BE SHARED
    // BUT THE OPENMP SYESTEM DOES NOT SUPPORT OVERLOADED OPERATORS
    // SO I HAVE FORCED AN OPENMP CODE AND EXECUTION
    // TO HAVE AN IDEA ABOUT SPEED AND OBJECTS

    #pragma omp parallel default (none)       \
    shared(oA,oB,oC)     \
    private(j,i,oTMP)
    {
        #pragma omp for

        for (j=1; j < J_MAX; j++) {
            for (i=1; i < I_MAX; i++) {
                oTMP = oA + oB;
                oC = oC + oTMP;
            }
        }
    }


    v3 = oC.GetElementF();
    cout << " output from test --> 1 --> v3 = " << v3 << endl;
    cout << " test --> 1 --> ends " << endl;
    cout << endl;

    // resetting output values

    v1 = static_cast<TA>(11.1111111100000000000);
    oA.SetElementF(v1);
    v2 = static_cast<TA>(22.2222222200000000000);
    oB.SetElementF(v2);
    oC.SetEqualZeroF();
    oTMP.SetEqualZeroF();
    iC = 0;

    // test --> 2
    // Serial for loop.
    // I have done the things much easier to be parallelized automatically
    // by the compiler. Indeed, the icpc succeeds to parallelize it.

    cout << " test --> 2 --> starts " << endl;

    iA = oA.GetElementF();
    iB = oB.GetElementF();
    iTMP = iA + iB;

    for (j=1; j < J_MAX; j++) {
        for (i=1; i < I_MAX; i++) {
            iC = iC + iTMP;
        }
    }

    oC.SetElementF(iC);
    v3 = oC.GetElementF();
    cout << " output from test --> 2 --> v3 = " << v3 << endl;
    cout << " test --> 2 --> ends " << endl;
    cout << endl;

    // resetting output values

    v1 = static_cast<TA>(11.1111111100000000000);
    oA.SetElementF(v1);
    v2 = static_cast<TA>(22.2222222200000000000);
    oB.SetElementF(v2);
    oC.SetEqualZeroF();
    oTMP.SetEqualZeroF();
    iC = 0;

    // test --> 3
    // OpenMP parallel for loop.

    cout << " test --> 3 --> starts " << endl;

    iA = oA.GetElementF();
    iB = oB.GetElementF();
    iTMP = iA + iB;

    #pragma omp parallel default (none)  \
    shared(iTMP)    \
    private(j,i)    \
    reduction(+:iC)
    {
        #pragma omp for

        for (j=1; j < J_MAX; j++) {
            for (i=1; i < I_MAX; i++) {
                iC = iC + iTMP;
            }
        }
    }

    oC.SetElementF(iC);
    v3 = oC.GetElementF();
    cout << " output from test --> 3 --> v3 = " << v3 << endl;
    cout << " test --> 3 --> ends " << endl;
    cout << endl;

    // XX. Exiting

    int sentinel;
    cin >> sentinel;

    cout << endl;

    return 0;
}

//======//
// FINI //
//======//

