//=============================================//
// Header file                                 //
// ElementCL class member functions definition //
//=============================================//

#include <cmath>

using std::abs;

//  1. Constructor definition

template <typename T>
ElementCL<T>::ElementCL(const T& elem):
    m_Elem(elem)
{}

//  2. Destructor definition

template <typename T>
ElementCL<T>::~ElementCL() {}

//  3. SetElement

template <typename T>
void ElementCL<T>::SetElementF(const T& elem)
{
    m_Elem = elem;
}

//  4. GetElement

template <typename T>
T ElementCL<T>::GetElementF() const
{
    return m_Elem;
}

//  5. SetEqual

template <typename T>
void ElementCL<T>::SetEqualF(const ElementCL<T>& elem)
{
    T tmp;

    tmp = elem.GetElementF();
    m_Elem = tmp;
}

//  6. SetEqualZero

template <typename T>
void ElementCL<T>::SetEqualZeroF()
{
    const T ZERO = static_cast<T>(0.0);

    m_Elem = ZERO;
}

//  7. AddF

template <typename T>
void ElementCL<T>::AddF(const ElementCL<T>& elema, const ElementCL<T>& elemb)
{
    T tmpa;
    T tmpb;

    tmpa = elema.GetElementF();
    tmpb = elemb.GetElementF();
    m_Elem = tmpa + tmpb;
}

//  8. SubtractF

template <typename T>
void ElementCL<T>::SubtractF(const ElementCL<T>& elema, const ElementCL<T>& elemb)
{
    T tmpa;
    T tmpb;

    tmpa = elema.GetElementF();
    tmpb = elemb.GetElementF();
    m_Elem = tmpa - tmpb;
}

//  9. TimesF

template <typename T>
void ElementCL<T>::TimesF(const ElementCL<T>& elema, const ElementCL<T>& elemb)
{
    T tmpa;
    T tmpb;

    tmpa = elema.GetElementF();
    tmpb = elemb.GetElementF();
    m_Elem = tmpa * tmpb;
}

//  10. DivideF

template <typename T>
void ElementCL<T>::DivideF(const ElementCL<T>& elema, const ElementCL<T>& elemb)
{
    T tmpa;
    T tmpb;

    tmpa = elema.GetElementF();
    tmpb = elemb.GetElementF();
    m_Elem = tmpa / tmpb;
}

//  11. AbsF

template <typename T>
void ElementCL<T>::AbsF(const ElementCL<T>& elem)
{
    T tmp;

    tmp = elem.GetElementF();
    tmp = abs(tmp);
    m_Elem = tmp;
}

//======//
// FINI //
//======//
