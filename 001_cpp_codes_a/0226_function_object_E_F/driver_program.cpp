
//===================================//
// function object, lamda expression //
// sort function                     //
//===================================//

#include <iostream>
#include <algorithm>
#include <functional>
#include <cmath>
#include <iomanip>

using std::endl;
using std::cout;
using std::cin;
using std::sort;
using std::greater;
using std::pow;
using std::cos;
using std::fixed;
using std::showpos;
using std::showpoint;
using std::setprecision;

// sort using a custom function object

template <class T>
class customLess {
public:
    bool operator() (T a, T b)
    {
        return a < b;
    }
};

// the main function

int main()
{
    // local variables and parameters

    const int DIMEN_MAX = static_cast<int>(pow(10.0, 5.0));
    double * myArray = new double [DIMEN_MAX];

    // output format

    cout << fixed;
    cout << setprecision(20);
    cout << showpos;
    cout << showpoint;

    // build the array

    for (int i = 0; i < DIMEN_MAX; i++) {
        myArray[i] = cos(static_cast<double>(i));
    }

    // sort the array using the default operator <

    cout << endl;
    cout << " --> sorting using the default operator <" << endl;
    cout << endl;

    sort(myArray, myArray+DIMEN_MAX);

    // display some sorted elements

    for (int i = 0; i < 1000; i++) {
        cout << " --> i = " << i << " --> element = " << myArray[i] << endl;
    }

    // sort the array using a standard library compare function object

    cout << endl;
    cout << " --> sorting using the STL compare function object greater<double>()" << endl;
    cout << endl;

    sort(myArray, myArray+DIMEN_MAX, greater<double>());

    for (int i = 0; i < 1000; i++) {
        cout << " --> i = " << i << " --> element = " << myArray[i] << endl;
    }

    // sort using a custom function object

    cout << endl;
    cout << " --> sorting using a custom function object" << endl;
    cout << endl;

    sort(myArray, myArray+DIMEN_MAX, customLess<double>());

    for (int i = 0; i < 1000; i++) {
        cout << " --> i = " << i << " --> element = " << myArray[i] << endl;
    }

    // sort using a lambda expression

    cout << endl;
    cout << " --> sorting using a lambda expression" << endl;
    cout << endl;

    sort(myArray, myArray+DIMEN_MAX, [](double a, double b) {
        return b < a;
    });

    for (int i = 0; i < 1000; i++) {
        cout << " --> i = " << i << " --> element = " << myArray[i] << endl;
    }

    // delete the array

    cout << " --> delete the array" << endl;

    delete [] myArray;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

