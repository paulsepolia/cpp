//====================//
// Common Functions //
//====================//

#include "IncludeLibs.h"

// external function

// 1.

template <typename T>
void benchFun(VectorPaul<T> obj, long dim)
{
    long i;

    cout << "  1 --> obj.setDim(dim);" << endl;

    obj.setDim(dim);

    cout << "  2 --> obj.allocateVector();" << endl;

    obj.allocateVector();

    cout << "  3 --> obj.setVector(j, val);" << endl;

    for (i = 0; i < dim; i++) {
        double val = static_cast<double>(i);
        obj.setVector(i, val);
    }

    cout << "  4 --> obj.getVector(0L) = " << obj.getVector(0L)  << endl;

    cout << "  5 --> obj.reverseVector();" << endl;

    obj.reverseVector();

    cout << "  6 --> obj.getVector(0L) = " << obj.getVector(0L)  << endl;

    cout << "  7 --> obj.setVector(j, val);" << endl;

    for (i = 0; i < dim; i++) {
        double val = cos(static_cast<double>(i));
        obj.setVector(i, val);
    }

    cout << "  8 --> obj.getVector(0L) = " << obj.getVector(0L)  << endl;

    cout << "  9 --> obj.sortVector();" << endl;

    obj.sortVector();

    cout << " 10 --> obj.isSortedVector() = " << obj.isSortedVector() << endl;

    for (i = 2 ; i < 4; i++) {
        cout << " 11 --> obj.getVector(" << i << ") = " << obj.getVector(i) << endl;
    }

    cout << " 12 --> obj.randomShuffleVector();" << endl;

    obj.randomShuffleVector();

    for (i = 12 ; i < 14; i++) {
        cout << " 13 --> obj.getVector(" << i << ") = " << obj.getVector(i) << endl;
    }

    cout << " 14 --> obj.maxElementVector() = " << obj.maxElementVector() << endl;

    cout << " 15 --> obj.minElementVector() = " << obj.minElementVector() << endl;

    cout << " 16 --> obj.getDim() = " << obj.getDim() << endl;

    cout << " 17 --> obj.fillVector(10);" << endl;

    double val = 10.0;

    obj.fillVector(val);

    cout << " 18 --> obj.maxElementVector() = " << obj.maxElementVector() << endl;

    cout << " 19 --> obj.minElementVector() = " << obj.minElementVector() << endl;

    cout << " 20 --> obj.generateVectorRandom();" << endl;

    obj.generateVectorRandom();

    cout << " 21 --> obj.maxElementVector() = " << obj.maxElementVector() << endl;

    cout << " 22 --> obj.minElementVector() = " << obj.minElementVector() << endl;

    cout << " 23 --> obj.sortVectorQSort();" << endl;

    obj.sortVectorQSort();

    cout << " 24 --> obj.isSortedVector() = " << obj.isSortedVector() << endl;

    cout << " 25 --> obj.generateVectorUnique();" << endl;

    obj.generateVectorUnique();

    cout << " 26 --> obj.maxElementVector() = " << obj.maxElementVector() << endl;
    cout << " 27 --> obj.minElementVector() = " << obj.minElementVector() << endl;

    VectorPaul<T> objLocA;

    objLocA.setDim(obj.getDim());
    objLocA.allocateVector();
    objLocA.initVector();

    cout << " 28 --> objLocA.maxElementVector() = " << objLocA.maxElementVector() << endl;
    cout << " 29 --> objLocA.minElementVector() = " << objLocA.minElementVector() << endl;

    VectorPaul<T> objLocB;

    objLocB.setDim(obj.getDim());
    objLocB.allocateVector();
    objLocB.initVector();

    cout << " 30 --> objLocB.maxElementVector() = " << objLocB.maxElementVector() << endl;
    cout << " 31 --> objLocB.minElementVector() = " << objLocB.minElementVector() << endl;

    VectorPaul<T> objLocC;

    objLocC.setDim(obj.getDim());
    objLocC.allocateVector();
    objLocC.initVector();

    cout << " 32 --> objLocC.maxElementVector() = " << objLocC.maxElementVector() << endl;
    cout << " 33 --> objLocC.minElementVector() = " << objLocC.minElementVector() << endl;

    VectorPaul<T> objLocD;

    objLocD.setDim(obj.getDim());
    objLocD.allocateVector();
    objLocD.initVector();

    cout << " 34 --> objLocD.maxElementVector() = " << objLocD.maxElementVector() << endl;
    cout << " 35 --> objLocD.minElementVector() = " << objLocD.minElementVector() << endl;

    VectorPaul<T> objLocE(obj.getDim());

    objLocE.allocateVector();
    objLocE.initVector();

    cout << " 36 --> objLocE.maxElementVector() = " << objLocE.maxElementVector() << endl;
    cout << " 37 --> objLocE.minElementVector() = " << objLocE.minElementVector() << endl;

    objLocE.generateVectorUnique();

    cout << " 38 --> objLocE.maxElementVector() = " << objLocE.maxElementVector() << endl;
    cout << " 39 --> objLocE.minElementVector() = " << objLocE.minElementVector() << endl;

    VectorPaul<T> objLocF(obj.getDim());

    objLocF.allocateVector();
    objLocF.initVector();

    cout << " 40 --> objLocF.maxElementVector() = " << objLocF.maxElementVector() << endl;
    cout << " 41 --> objLocF.minElementVector() = " << objLocF.minElementVector() << endl;

    objLocF.generateVectorUnique();

    cout << " 42 --> objLocF.maxElementVector() = " << objLocF.maxElementVector() << endl;
    cout << " 43 --> objLocF.minElementVector() = " << objLocF.minElementVector() << endl;

    objLocA.setEqualTo(obj);
    objLocB.setEqualTo(obj);
    objLocB = objLocA;
    objLocC = objLocB + objLocA;
    objLocD = objLocB - objLocA; // THIS IS WRONGLY DEFINED

    cout << " 44 --> objLocA.maxElementVector() = " << objLocA.maxElementVector() << endl;
    cout << " 45 --> objLocA.minElementVector() = " << objLocA.minElementVector() << endl;
    cout << " 46 --> objLocB.maxElementVector() = " << objLocB.maxElementVector() << endl;
    cout << " 47 --> objLocB.minElementVector() = " << objLocB.minElementVector() << endl;
    cout << " 48 --> objLocC.maxElementVector() = " << objLocC.maxElementVector() << endl;
    cout << " 49 --> objLocC.minElementVector() = " << objLocC.minElementVector() << endl;
    cout << " 50 --> objLocD.maxElementVector() = " << objLocD.maxElementVector() << endl;
    cout << " 51 --> objLocD.minElementVector() = " << objLocD.minElementVector() << endl;

    objLocA.setEqualTo(obj);
    objLocB.setEqualTo(obj);
    objLocC.addVector(objLocB, objLocA);
    objLocD.subtractVector(objLocB, objLocA);
    objLocE.addVector(objLocB, objLocA);
    objLocF.subtractVector(objLocB, objLocA);

    cout << " 52 --> objLocA.maxElementVector() = " << objLocA.maxElementVector() << endl;
    cout << " 53 --> objLocA.minElementVector() = " << objLocA.minElementVector() << endl;
    cout << " 54 --> objLocB.maxElementVector() = " << objLocB.maxElementVector() << endl;
    cout << " 55 --> objLocB.minElementVector() = " << objLocB.minElementVector() << endl;
    cout << " 56 --> objLocC.maxElementVector() = " << objLocC.maxElementVector() << endl;
    cout << " 57 --> objLocC.minElementVector() = " << objLocC.minElementVector() << endl;
    cout << " 58 --> objLocD.maxElementVector() = " << objLocD.maxElementVector() << endl;
    cout << " 59 --> objLocD.minElementVector() = " << objLocD.minElementVector() << endl;
    cout << " 60 --> objLocE.maxElementVector() = " << objLocE.maxElementVector() << endl;
    cout << " 61 --> objLocE.minElementVector() = " << objLocE.minElementVector() << endl;
    cout << " 62 --> objLocF.maxFlementVector() = " << objLocF.maxElementVector() << endl;
    cout << " 63 --> objLocF.minElementVector() = " << objLocF.minElementVector() << endl;

    cout << " 64 --> obj.deleteVector();" << endl;

    obj.deleteVector();

    cout << " 65 --> objLocA.deleteVector();" << endl;

    objLocA.deleteVector();

    cout << " 66 --> objLocB.deleteVector();" << endl;

    objLocB.deleteVector();

    cout << " 67 --> objLocC.deleteVector();" << endl;

    objLocC.deleteVector();

    cout << " 68 --> objLocD.deleteVector();" << endl;

    objLocD.deleteVector();

    cout << " 69 --> objLocE.deleteVector();" << endl;

    objLocE.deleteVector();

    cout << " 70 --> objLocF.deleteVector();" << endl;

    objLocF.deleteVector();
}

//======//
// FINI //
//======//
