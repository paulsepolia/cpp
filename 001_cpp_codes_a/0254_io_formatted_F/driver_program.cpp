
//======================//
// a test formatted i/o //
//======================//

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include <cmath>
#include <cstdio>

using std::endl;
using std::cin;
using std::cout;
using std::ofstream;
using std::istream;
using std::exit;
using std::string;
using std::ios;
using std::cos;
using std::pow;
using std::remove;
using std::ios_base;

// the main function

int main()
{
    // local variables and parameters

    ofstream outStream;
    string fileName = "my_file.txt";
    const long I_MAX = static_cast<long>(pow(10.0, 7.0));
    int sentinel;
    double tmpA;

    // open the file and attach it to the stream

    outStream.open(fileName.c_str());

    // test if the opening was with success

    if (outStream.fail()) {
        cout << "Failed! Enter an integet to exit.";
        cin >> sentinel;
        exit(1);
    }

    // get the default output format settings
    // so you can revert any time

    int precisionSetting = outStream.precision();
    ios_base::fmtflags flagSettings = outStream.flags();

    // set the output format

    outStream.setf(ios::fixed);
    outStream.setf(ios::showpoint);
    outStream.setf(ios::showpos);
    outStream.precision(20);

    // write to the file

    cout << " --> writing formatted data to a file that is going to be deleted automatically\n";

    for (long i = 0; i < I_MAX; i++) {
        tmpA = cos(static_cast<double>(i));
        outStream.width(10);
        outStream << i << " --> ";
        outStream.width(20);
        outStream << tmpA << endl;
    }

    // flush  the stream

    outStream.flush();

    // restore the settings

    outStream.precision(precisionSetting);
    outStream.flags(flagSettings);

    // close the stream

    outStream.close();

    // remove file

    remove(fileName.c_str());

    // sentineling

    cout << " Enter an interger to exit: ";
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

