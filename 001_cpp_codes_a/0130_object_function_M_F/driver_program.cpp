//=================//
// object function //
//=================//

#include <iostream>
#include <set>
#include <iterator>
#include <algorithm>

using std::cout;
using std::cin;
using std::endl;
using std::less;
using std::set;
using std::copy;
using std::ostream_iterator;

// a function

void out(const char *s, const set<int, less<int> > &S)
{
    cout << s;
    copy(S.begin(), S.end(), ostream_iterator<int>(cout, "+"));
    cout << endl;
}

// the main function

int main()
{
// 1.

    int a[3] = {1, 2, 3};
    set<int, less<int> > S1;
    set<int, less<int> > S2(a, a+3);
    set<int, less<int> > S3(S2);

    // 2.

    copy(S1.begin(), S1.end(), ostream_iterator<int>(cout, "-"));
    cout << endl;
    copy(S2.begin(), S2.end(), ostream_iterator<int>(cout, "-"));
    cout << endl;
    copy(S3.begin(), S3.end(), ostream_iterator<int>(cout, "-"));
    cout << endl;

    // 3.

    out("S1: ", S1);
    out("S2: ", S2);
    out("S3: ", S3);

    int sentinel;
    cin >> sentinel;
}

//======//
// FINI //
//======//
