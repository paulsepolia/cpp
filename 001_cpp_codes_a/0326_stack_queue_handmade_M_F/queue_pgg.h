
//====================================//
// declaration of the class queue_pgg //
//====================================//

#ifndef QUEUE_PGG_H
#define QUEUE_PGG_H

#include <cstdlib>
#include <deque>

using std::size_t;
using std::deque;

template <typename T, typename CONT = deque<T> >
class queue_pgg {
public:

    queue_pgg<T, CONT>(); // default constructor
    ~queue_pgg<T, CONT>(); // destructor

    template <typename T2, typename CONT2>
    queue_pgg<T, CONT>(const queue_pgg<T2, CONT2> &); // copy constructor

    // member functions

    bool empty() const;
    size_t size() const;
    T & front();
    T & back();
    void push(const T &);
    void pop();

    template <typename T2, typename CONT2>
    void swap(queue_pgg<T2, CONT2> &);

    void emplace(const T &);

    // operators overload

    template <typename T2, typename CONT2>
    queue_pgg<T, CONT> & operator = (const queue_pgg<T2, CONT2> &); // assignment operator

private:

    CONT elems;
};

#endif // deque_PGG_H

//======//
// FINI //
//======//
