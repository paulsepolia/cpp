
//======================//
// Dog class definition //
//======================//

#include <iostream>
#include "pet.h"
#include "dog.h"

using std::cout;
using std::endl;

namespace pgg {
// default constructor

template <class T>
Dog<T>::Dog() : Pet<T>(), breed("No breed yet"), extra_price(0) {}

// member function

template <class T>
void Dog<T>::print() const
{
    cout << "name: " << Pet<T>::name << endl;
    cout << "breed: " << breed << endl;
    cout << "price: " << Pet<T>::price << endl;
    cout << "extra_price: " << extra_price << endl;
}

// destructor

template <class T>
Dog<T>::~Dog() {}

} // pgg

//======//
// FINI //
//======//

