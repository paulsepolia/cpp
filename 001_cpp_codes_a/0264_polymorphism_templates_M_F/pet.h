
//=======================//
// Pet class declaration //
//=======================//

#ifndef PET_H
#define PET_H

#include <string>

using std::string;

namespace pgg {
template <class T>
class Pet {
public:
    Pet();
    string name;
    T price;
    virtual void print() const;
    virtual ~Pet();
};

} // pgg

#endif // PET_H

//======//
// FINI //
//======//

