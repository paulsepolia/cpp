
#include <iostream>
#include "pet.h"
#include "pet.cpp"
#include "dog.h"
#include "dog.cpp"

using std::cout;
using std::cin;
using std::endl;
using pgg::Pet;
using pgg::Dog;

int main()
{
    Dog<double> vdog;
    Pet<double> vpet;

    vdog.name = "Tiny";
    vdog.price = 100.0;
    vdog.breed = "Great Dane";
    vdog.extra_price = 10.0;

    vpet = vdog; // here is the slicing problem

    cout << "The slicing problem:" << endl;

    //vpet.breed; // is illegal since class Pet has no member named breed

    vpet.print();

    cout << "Note that it was print from Pet that was invoked." << endl;

    // defeat the slicing problem

    cout << "The slicing problem defeated:" << endl;

    Pet<double> * ppet;
    ppet = new Pet<double>;
    Dog<double> * pdog;
    pdog = new Dog<double>;

    pdog->name = "Tine";
    pdog->breed = "Great Dane";
    pdog->price = 200.0;
    pdog->extra_price = 20.0;

    ppet = pdog;

    ppet -> print();
    pdog -> print();

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
