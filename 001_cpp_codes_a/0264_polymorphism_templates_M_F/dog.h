
//=======================//
// Dog class declaration //
//=======================//

#ifndef DOG_H
#define DOG_H

#include <string>
#include "pet.h"

using std::string;

namespace pgg {
template <class T>
class Dog : public Pet<T> {
public:
    Dog();
    string breed;
    T extra_price;
    virtual void print() const;
    virtual ~Dog();
};

} // pgg

#endif // DOG_H

//======//
// FINI //
//======//

