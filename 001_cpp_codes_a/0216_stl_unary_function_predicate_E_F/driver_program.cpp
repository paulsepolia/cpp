
//==========================//
// unary function predicate //
//==========================//

#include <iostream>
#include <vector>
#include <algorithm>

using std::cout;
using std::cin;
using std::endl;
using std::vector;
using std::count_if;

// this is a unary predicate
// that determines if a number is even

bool isEven(const int & i)
{
    return !(i%2);
}

// the main function

int main()
{
    // local variables and parameters

    vector<int> vA;
    int counter;

    // build the vector

    for (int i = 0; i < 20; i++) {
        vA.push_back(i);
    }

    // display the vector

    for (unsigned int i = 0; i < vA.size(); i++) {
        cout << vA[i] << " ";
    }

    cout << endl;

    // count the even numbers

    counter = count_if(vA.begin(), vA.end(), isEven);

    cout << " --> " << counter << " numbers are evenly divisible by 2" << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

