//=======================================//
// Author: Pavlos G. Galiatsatos         //
// Date: 2013/10/23                      //
// Functions: vector, deque, array, list //
// Algorithms: initialization            //
//=======================================//

#include <iostream>
#include <vector>
#include <cmath>
#include <iomanip>
#include <fstream>
#include <iterator>
#include <algorithm>

using namespace std;

// hand made sorting function

bool wayToSort(double i, double j)
{
    return i < j;
}

// the main function

int main ()
{
    // variables

    int i;
    const int I_MAX = 10000;

    // open a unit to write random data

    fstream myFileA; // output file stream
    fstream myFileB; // output filr stream
    vector<double> aVector;

    myFileA.open("test.txt", fstream::out);

    // set the output format

    myFileA << fixed;
    myFileA << setprecision(20);
    myFileA << showpoint;
    myFileA << showpos;

    cout << " 1 --> write to the file" << endl;

    for (i = 0; i < I_MAX; i++) { // write data to the file
        myFileA << sin(static_cast<double>(i)) << endl;
    }

    myFileA.close(); // close the stream

    // open a unit to read the data file

    myFileA.open("test.txt", fstream::in);
    myFileB.open("test_sorted.txt", fstream::out);

    // test if opened with success

    if (myFileA.fail()) {
        cout << "Can not open the file to read" << endl;
        return -1;
    }

    if (myFileB.fail()) {
        cout << "Can not open the file to write" << endl;
        return -1;
    }

    // read and put the data to aVector

    cout << " 2 --> read from the file" << endl;

    copy(istream_iterator<double>(myFileA),
         istream_iterator<double>(),
         inserter(aVector, aVector.begin()));

    // sort the vector

    cout << " 3 --> sort the data in RAM" << endl;

    sort(aVector.begin(), aVector.end(), wayToSort);

    // set the output format

    myFileB << fixed;
    myFileB << setprecision(20);
    myFileB << showpoint;
    myFileB << showpos;

    // write to the file

    cout << " 4 --> write to the new file the sorted data" << endl;

    copy(aVector.begin(),
         aVector.end(),
         ostream_iterator<double>(myFileB, "\n"));

    // finale

    return 0;
}

// end
