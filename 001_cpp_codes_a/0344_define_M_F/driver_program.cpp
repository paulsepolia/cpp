//=====================//
// define preprocessor //
//=====================//

//===============//
// code --> 0344 //
//===============//

#include <iostream>

using std::cout;
using std::endl;

// define
// function macro

#define getmax(a,b) ((a)>(b)?(a):(b))

// the main function

int main()
{
    int x = 5;
    int y;

    y = getmax(x,2);

    cout << y << endl;
    cout << getmax(7,x) << endl;

    return 0;
}

// FINI
