//==================================//
// Read and write binary data using //
// fopen, fread, fwrite, fclose     //
//==================================//

#include <iostream>
#include <cstdio>
#include <cmath>

using namespace std;

// the main code

int main()
{

    // 1. variables and parameters

    const long int I_DO_MAX = static_cast<long int>(pow(10.0, 8.0));
    const long int J_DO_MAX = static_cast<long int>(pow(10.0, 2.0));
    long int counter;
    long int j;
    FILE *ptr_myfile;
    long int x;

    // 2. main do loop

    for (j = 1; j <= J_DO_MAX; j++) {
        // 3. open the file

        cout << " 0 -------------------------------------------------------->> " << j << endl;
        cout << " 1 --> Open the file for writting" << endl;

        ptr_myfile = fopen("test.bin","wb");

        // 4. test if the file is opened

        cout << " 2 --> Test if the file is opened" << endl;

        if (!ptr_myfile) {
            cout << "Unable to open file!" << endl;
            return -1;
        }

        cout << " 3 --> The file is opened with success" << endl;

        // 5. writting data to the file

        cout << " 4 --> Write to the file" << endl;

        for (counter=1; counter <= I_DO_MAX; counter++) {
            x = counter;
            fwrite(&x, sizeof(long int), 1, ptr_myfile);
        }

        // 6. close the file

        cout << " 5 --> Close the file" << endl;

        fclose(ptr_myfile);

        // 7. open the file again

        cout << " 6 --> Open the file again" << endl;

        ptr_myfile = fopen("test.bin","rb");

        // 8. test if the file is opened

        cout << " 7 --> Test if the file is opened" << endl;

        if (!ptr_myfile) {
            cout << "Unable to open file!" << endl;
            return -1;
        }

        cout << " 8 --> The file is opened with succeess" << endl;

        // 9. read back data

        cout << " 9 --> Read the file" << endl;

        for (counter=1; counter <= I_DO_MAX; counter++) {
            fread(&x, sizeof(long int), 1, ptr_myfile);

            if (counter == I_DO_MAX) {
                cout << " The last record is  = " << x << endl << endl;;
            }

            if (counter == 1) {
                cout << endl << " The first record is = " << x << endl;
            }
        }

        // 10. close the file

        cout << " 10 --> Close the file" << endl;

        fclose(ptr_myfile);
    }

    return 0;
}

//======//
// FINI //
//======//
