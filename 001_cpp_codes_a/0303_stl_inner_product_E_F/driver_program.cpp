
//====================//
// STL, inner product //
//====================//

#include <iostream>
#include <iomanip>
#include <functional>
#include <numeric>
#include <cmath>

using std::cout;
using std::cin;
using std::endl;

using std::fixed;
using std::showpoint;
using std::showpos;
using std::setprecision;

using std::multiplies;
using std::plus;

using std::inner_product;

using std::pow;

// the main function

int main()
{
    // local variables and parametes

    const long long DIM_MAX = static_cast<long long>(pow(10.0, 8.0));
    const long long K_MAX = static_cast<long long>(pow(10.0, 5.0));
    double result = 0.0;

    // adjust the output

    cout << fixed;
    cout << setprecision(20);
    cout << showpos;
    cout << showpoint;

    // the for loop

    for (long long k = 0; k < K_MAX; k++) {
        cout << "-------------------------------------------------------------->> " << k << endl;

        // the containers

        double * a1 = new double [DIM_MAX];
        double * a2 = new double [DIM_MAX];

        // build the array

        cout << " --> build the arrays" << endl;

        for (long long i = 0; i < DIM_MAX; i++) {
            a1[i] = cos(static_cast<double>(i));
            a2[i] = cos(static_cast<double>(i));
        }

        // inner_product

        cout << " --> execute the inner product" << endl;

        result = inner_product(&a1[0], &a1[DIM_MAX], &a2[0], 0.0);

        cout << " result --> normal --> " << result << endl;

        // inner_product --> new

        cout << " --> execute the inner product --> modified" << endl;

        result = inner_product(&a1[0], &a1[DIM_MAX], &a2[0], 1.0, multiplies<double>(), plus<double>());

        cout << " result --> modified --> " << result << endl;

        // delete containers

        delete [] a1;
        delete [] a2;
    }

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
