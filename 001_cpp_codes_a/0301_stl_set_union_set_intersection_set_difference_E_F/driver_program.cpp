
//===================//
// STL, includes,    //
// set_union,        //
// set_difference    //
// set_intersection  //
//===================//

#include <iostream>
#include <cassert>
#include <algorithm>
#include <vector>
#include <iterator>

using std::cout;
using std::cin;
using std::endl;
using std::includes;
using std::set_union;
using std::set_intersection;
using std::vector;
using std::back_inserter;

// the main function

int main()
{

    cout << "Illustrating the generic set operations." << endl;
    vector<int> v1;
    vector<int> v2;
    vector<int> v3;
    const int DIM_MAX = 100;

    // build v1 and v2

    for (int i = 0; i < DIM_MAX; i++) {
        v1.push_back(i);
        v2.push_back(i+DIM_MAX-10);
    }

    // set_union v3 = union(v1,v2);

    set_union(v1.begin(), v1.end(), v2.begin(), v2.end(), back_inserter(v3));

    cout << " --> v3.size() = " << v3.size() << endl;

    // set_intersection v3 = set_intersection(v1,v2);

    v3.clear();
    v3.shrink_to_fit();

    set_intersection(v1.begin(), v1.end(), v2.begin(), v2.end(), back_inserter(v3));

    cout << " --> v3.size() = " << v3.size() << endl;

    // set_difference v3 = set_difference(v1,v2);

    v3.clear();
    v3.shrink_to_fit();

    set_difference(v1.begin(), v1.end(), v2.begin(), v2.end(), back_inserter(v3));

    cout << " --> v3.size() = " << v3.size() << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

