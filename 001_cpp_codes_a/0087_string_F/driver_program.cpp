//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/11/05              //
// Functions: string             //
// Algorithms:                   //
//===============================//

#include <iostream>
#include <string>

using namespace std;

int main ()
{
    string s(5, 'A');
    string t("BC");
    string u;
    const long int I_MAX = static_cast<long int>(1000000000);
    string s2(I_MAX, 'C');

    u = s + t;

    cout << " u = s + t = " << u << endl;

    cout << " &s2 = " << &s2 << endl;

    cout << " s2[0] = " << s2[0] << endl;
    cout << " s2[1] = " << s2[1] << endl;
    cout << " s2[2] = " << s2[2] << endl;

    return 0;
}

// end

