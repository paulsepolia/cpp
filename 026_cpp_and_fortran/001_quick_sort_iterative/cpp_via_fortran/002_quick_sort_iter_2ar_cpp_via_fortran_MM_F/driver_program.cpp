//===================================//
// Author: Pavlos G. Galiatsatos     //
// Date: 2014/10/22                  //
// Program: The Quick Sort Algorithm //
//===================================//

#include <iostream>
#include <iomanip>
#include <cmath>
#include <ctime>

using std::cout;
using std::cin;
using std::endl;
using std::fixed;
using std::setprecision;
using std::clock;

// the C interface to quick-sort fortran function

extern "C" void quick_sort_iter_2ar_(double *,  double *, const long &);

// the main function

int main()
{
	// local parameters and variables

	const long DIM_ARR = 1 * static_cast<long>(pow(10.0, 8.0));
	const long K_MAX = static_cast<long>(pow(10.0, 4.0));
	clock_t t1;
	clock_t t2;

	// adjust the output format

	cout << fixed;
	cout << setprecision(10);

	for (long k = 0; k != K_MAX; ++k)
	{
		// counter

		cout << " --------------------------------------------------------->> " << k << endl; 
	
		// allocate RAM

		cout << " --> 1 --> allocate the arrays" << endl;

		double * array_a = new double [DIM_ARR];
		double * array_b = new double [DIM_ARR];

		// build the arrays

		cout << " --> 2 --> build the array" << endl;

		for (long i = 0; i != DIM_ARR; ++i)
		{
			array_a[i] = static_cast<double>(DIM_ARR-i-1);
	 		array_b[i] = static_cast<double>(DIM_ARR-i);
		}

		// sort the array array_a and array_b follows 
		// the modifications to array_a

		cout << " --> 3 --> sort the array" << endl;

		t1 = clock();

		quick_sort_iter_2ar_(array_a, array_b, DIM_ARR);

		t2 = clock();

		cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl; 

		// some output to verify the functionality

		cout << " --> 4 --> some output from array_a" << endl;

		cout << " --> array_a[0] = " << array_a[0] << endl;
		cout << " --> array_a[1] = " << array_a[1] << endl;
		cout << " --> array_a[2] = " << array_a[2] << endl;
		cout << " --> array_a[3] = " << array_a[3] << endl;
        	cout << " --> array_a[4] = " << array_a[4] << endl;
        	cout << " --> array_a[DIM_ARR-3] = " << array_a[DIM_ARR-3] << endl;
        	cout << " --> array_a[DIM_ARR-2] = " << array_a[DIM_ARR-2] << endl;
        	cout << " --> array_a[DIM_ARR-1] = " << array_a[DIM_ARR-1] << endl;

		cout << " --> 5 --> some output from array_b" << endl;

		cout << " --> array_b[0] = " << array_b[0] << endl;
		cout << " --> array_b[1] = " << array_b[1] << endl;
		cout << " --> array_b[2] = " << array_b[2] << endl;
		cout << " --> array_b[3] = " << array_b[3] << endl;
        	cout << " --> array_b[4] = " << array_b[4] << endl;
        	cout << " --> array_b[DIM_ARR-3] = " << array_b[DIM_ARR-3] << endl;
        	cout << " --> array_b[DIM_ARR-2] = " << array_b[DIM_ARR-2] << endl;
        	cout << " --> array_b[DIM_ARR-1] = " << array_b[DIM_ARR-1] << endl;

		// delete array_a and array_b

		cout << " --> 6 --> free up the RAM" << endl; 

		delete [] array_a;
		delete [] array_b;
	}

	// sentineling
	
	cout << " --> end" << endl;

	int sentinel;
	cin >> sentinel;
}
