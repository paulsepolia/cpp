!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2014/10/21              !
!===============================!

  subroutine fortran_vec_add(vec_a,   &
                             vec_b,   &
                             vec_c,   &
                             DIMEN)

  use iso_c_binding, only: C_DOUBLE, C_LONG

  implicit none

  integer(kind=C_LONG), intent(in)                         :: DIMEN
  real(kind=C_DOUBLE), dimension(0:dimen-1), intent(in)    :: vec_a
  real(kind=C_DOUBLE), dimension(0:dimen-1), intent(in)    :: vec_b
  real(kind=C_DOUBLE), dimension(0:dimen-1), intent(inout) :: vec_c

  integer(kind=8) i;

  !$omp parallel      &
  !$omp default(none) &
  !$omp shared(vec_a) &
  !$omp shared(vec_b) &
  !$omp shared(vec_c) &
  !$omp shared(DIMEN) &
  !$omp private(i)

  !$omp do

    do i = 0, DIMEN
      vec_c(i) = vec_a(i) + vec_b(i);
    end do

  !$omp end do

  !$omp end parallel

  end subroutine fortran_vec_add

