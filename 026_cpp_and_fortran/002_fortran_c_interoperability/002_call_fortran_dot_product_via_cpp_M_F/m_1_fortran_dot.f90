!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2014/10/21              !
!===============================!

  subroutine fortran_dot(vec_a,   &
                         vec_b,   &
                         res_dot, &
                         DIMEN)

  use iso_c_binding, only: C_DOUBLE, C_LONG

  implicit none

  integer(kind=C_LONG), intent(in)                      :: DIMEN
  real(kind=C_DOUBLE), dimension(0:dimen-1), intent(in) :: vec_a
  real(kind=C_DOUBLE), dimension(0:dimen-1), intent(in) :: vec_b
  real(kind=C_DOUBLE), intent(out)                      :: res_dot

  res_dot = dot_product(vec_a, vec_b)

  end subroutine fortran_dot

!======!
! FINI !
!======!
