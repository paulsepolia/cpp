
#include <iostream>
#include <ctime>

using std::cout;
using std::cin;
using std::endl;
using std::clock;

// C-like interface to fortran function

extern "C" 
{ 
	void fortran_omp_sub_(double &, const int &, const int&, const int&);
}

// the main function

int main()
{
	// local parameters

	const int N1 = 6; 
   	const int N2 = 5;
   	const int NT = 4;

	// local variables

   	double resta;
   	clock_t t1;
	clock_t t2;

	// fortran function execution
	
	t1 = clock();

   	fortran_omp_sub_(resta, N1, N2, NT);

	t2 = clock();

   	cout << " --> N1        = " << N1    << endl ;
   	cout << " --> N2        = " << N2    << endl ;
   	cout << " --> NT        = " << NT    << endl ;
   	cout << " --> resta     = " << resta << endl ;
   	cout << " --> time used = " << (t2-t1)/CLOCKS_PER_SEC/NT << endl ;
	
	// sentineling

	cout << " --> end" << endl;

	int sentinel;
	cin >> sentinel;
	
   	return 0;
}

//======//
// FINI //
//======//

