#!/bin/bash

# fortran part --> 1/1

  gfortran-8.2.0  -c           \
                  -O3          \
  	              -fopenmp     \
  	              fortran_omp_sub.f90
 
# cpp part --> 1/2

  clang++  -c              \
           -O3             \
           -fopenmp        \
           sum_omp_cpp_via_fortran.cpp

# cpp part --> 2/2

  clang++  -O3                       \
           -fopenmp                  \
           fortran_omp_sub.o         \
           sum_omp_cpp_via_fortran.o \
           -lgfortran                \
           -o x_clang

# cleaning the produced objects

  rm *.o
