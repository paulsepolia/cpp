#include <iostream>

extern "C" void add_mat_fortran_(const int &, 
                                 const int &, 
                                 double *, 
                                 double *, 
                                 double *);

// the main function

int main()
{

	// parameters
 
	const long DIM_M = 10000; // rows
   	const long DIM_N = 10000; // columns
   	const long DO_MAX_1 = 20; 
   	const long DO_MAX_2 = 3; 
   	const long DO_MAX_3 = 3;

	// rest local parameters and variables

   	const long TOT_ELEM = DIM_M*DIM_N;
	long i, j;

   	// declarations of the matrices

	std::cout << " --> 1 --> allocate RAM" << std::endl;
 
   	double * A = new double [TOT_ELEM];
   	double * B = new double [TOT_ELEM];
	double * C = new double [TOT_ELEM];
   
   	// initialization of A, B, C matrices

	std::cout << " --> 2 --> build matrices" << std::endl;

	#pragma omp parallel default(none) \
			     shared(A,B,C) \
			     private(i,j)
	{
		#pragma omp for

   		for (i = 0; i < DIM_M; ++i)
   		{ 
     		for (j = 0; j < DIM_N; ++j)
	 		{
				A[i*DIM_M + j] = static_cast<double>(i);
	    		B[i*DIM_M + j] = static_cast<double>(j);
				C[i*DIM_M + j] = 0.0;
	 		}
   		}
	}	
    
   	// adding the matrices A + B = C

	std::cout << " --> 3 --> add matrices via fortran function" << std::endl;

   	for (i = 0; i != DO_MAX_1; ++i)
   	{ 
		add_mat_fortran_(DIM_M, DIM_N, A, B, C); 
	}

	std::cout << " --> 4 --> test the results" << std::endl;

   	for (i = 0; i != DO_MAX_2; ++i)
   	{ 
		for (j = 0; j != DO_MAX_3; ++j)
     	{ 
       		std::cout << " test " << i << " + " << j << "  = " << C[i*DIM_M + j] << std::endl;
     	}
   	}

	std::cout << " --> 5 --> free up RAM" << std::endl;

	delete [] A;
	delete [] B;
	delete [] C;

	// sentineling

	std::cout << " --> end" << std::endl;

	int sentinel;
	std::cin >> sentinel;

   	return 0 ;
}
