#!/bin/bash

  # fortran part --> 1/1

  gfortran-8.2.0  -c               \
                  -O3              \
  	              -fopenmp         \
  	              fortran_add_mat.f90
 
  # cpp part --> 1/2

  clang++  -c                 \
           -O3                \
           -fopenmp           \
           driver_program.cpp

  # cpp part --> 2/2

  clang++  -O3               \
           -fopenmp          \
           fortran_add_mat.o \
           driver_program.o  \
           -o x_clang

  # cleaning the produced objects

  rm *.o
