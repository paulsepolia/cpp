
! input  :: dimen1, dimen2, mat_a, mat_b
! output :: mat_c
 
subroutine add_mat_fortran( dimen1, &
                            dimen2, &
                            mat_a,  &
                            mat_b,  &
                            mat_c )
 
implicit None
 
integer*4, intent(in) :: dimen1
integer*4, intent(in) :: dimen2
real*8, intent(in),  dimension(1:dimen1,1:dimen2) :: mat_a
real*8, intent(in),  dimension(1:dimen1,1:dimen2) :: mat_b
real*8, intent(out), dimension(1:dimen1,1:dimen2) :: mat_c
 
! 1. local variables.
 
integer*8 :: i, j

! 2. the matrix-addition do-loop.

do j = 1, dimen2
  do i = 1, dimen1
     
    mat_c(i,j) = mat_a(i,j) + mat_b(i,j)
       
  end do
end do

end subroutine add_mat_fortran

! FINI.