
 # include <iostream>
 using namespace std ;

 extern "C" 
 void ADD_MAT_FORTRAN(int& m, int& n, double* A, double* B, double* C) ;

 int main()
 {
   int m = 10000 ; // rows
   int n = 10000 ; // columns
   
   long int dimTotal = m * n ;

   // Declarations of the matrices.
   
   double* A = new double [dimTotal] ;
   
   double* B = new double [dimTotal] ;
   
   double* C = new double [dimTotal] ;
   
   // Initialization of A,B matrices

   for ( long int i = 0 ; i < m ; i++ )
   { 
     for ( long int j = 0 ; j < n ; j++ )
	 {  
		A[ i * m + j ] = i ; 
	    B[ i * m + j ] = j ; 
	 }
   }
    
   // Adding the matrices A + B = C

   int test_bound_1 = 1000 ;

   for ( int i = 0 ; i < test_bound_1 ; i++ )
   { ADD_MAT_FORTRAN( m, n, A, B, C ) ; }
    
   int test_bound_2 = 10 ; 
   int test_bound_3 = 10 ;

   for ( long int i = 0 ; i < test_bound_1 ; i++ )
   { for ( long int j = 0 ; j < test_bound_2 ; j++ )
     { 
       cout << " test "
		    << i 
		    << " + " 
			<< j  
		    << "  = " 
			<< C[ i * m + j ] 
	        << endl ;
     }
   }

   return 0 ;
}