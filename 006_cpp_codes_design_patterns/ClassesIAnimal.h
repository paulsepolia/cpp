#ifndef CLASSES_IANIMAL_H
#define CLASSES_IANIMAL_H

#include "IAnimal.h"

// class --> Cat

class Cat : public IAnimal {
    int GetNumberOfLegs() const;
    void Speak();
    void Free();
    static IAnimal * __stdcall Create();
};

// class --> Dog

class Dog : public IAnimal {
    int GetNumberOfLegs() const;
    void Speak();
    void Free();
    static IAnimal * __stdcall Create();
}

// class --> Spider

class Spider : public IAnimal {
    int GetNumberOfLegs() const;
    void Speak();
    void Free();
    static IAnimal * __stdcall Create();
}

// class --> Horse

class Horse : public IAnimal {
    int GetNumberOfLegs() const;
    void Speak();
    void Free();
    static IAnimal * __stdcall Create();
}

#endif // CLASSES_IANIMAL_H
