//======================//
// parallel unique_copy //
//======================//

#include <iostream>
#include <vector>
#include <iterator>
#include <numeric>
#include <cmath>
#include <ctime>
#include <iomanip>

#include <parallel/algorithm>
#include <parallel/settings.h>

using std::cout;
using std::endl;
using std::ostream_iterator;
using std::vector;
using std::pow;
using std::clock;
using std::fixed;
using std::setprecision;

// type definition

typedef unsigned long int uli;

// the main function

int main()
{
    cout << fixed;
    cout << setprecision(5);

    //===================//
    // parallel settings //
    //===================//

    __gnu_parallel::_Settings s;
    s.algorithm_strategy = __gnu_parallel::force_parallel;
    __gnu_parallel::_Settings::set(s);

    //==========================//
    // end of parallel settings //
    //==========================//

    const uli DIM1 = 2 * uli(pow(10.0, 8.0));
    const uli TRIALS1 = 4 * uli(pow(10.0, 2.0));
    const uli DIV1 = 10;

    vector<double> v1(DIM1);
    vector<double> v2(DIM1);
    time_t t1;
    time_t t2;

    cout << " --> build v1" << endl;

    iota(v1.begin(), v1.end(), 0.0);

    cout << " --> unique_copy of v1 in a parallel to v2" << endl;

    t1 = clock();

    for (uli i = 0; i != TRIALS1; i++) {
        if (i%DIV1 == 0) {
            cout << " -------------------------------------------------------->> " << i << endl;
        }
        std::__parallel::unique_copy(v1.begin(), v1.end(), v2.begin());
    }

    t2 = clock();
    cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

    cout << " --> some results :" << endl;

    cout << " --> v1[0] = " << v1[0] << endl;
    cout << " --> v1[1] = " << v1[1] << endl;
    cout << " --> v1[2] = " << v1[2] << endl;
    cout << " --> v1[3] = " << v1[3] << endl;

    cout << " --> v2[0] = " << v2[0] << endl;
    cout << " --> v2[1] = " << v2[1] << endl;
    cout << " --> v2[2] = " << v2[2] << endl;
    cout << " --> v2[3] = " << v2[3] << endl;

    cout << " --> v1.size() = " << v1.size() << endl;
    cout << " --> v2.size() = " << v2.size() << endl;

    return 0;
}

// end
