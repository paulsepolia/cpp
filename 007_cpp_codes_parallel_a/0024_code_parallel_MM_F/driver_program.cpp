//====================//
// parallel remove_if //
//====================//

#include <iostream>
#include <vector>
#include <iterator>
#include <cmath>
#include <ctime>
#include <iomanip>

#include <parallel/algorithm>
#include <parallel/numeric>
#include <parallel/settings.h>

using std::cout;
using std::endl;
using std::ostream_iterator;
using std::vector;
using std::pow;
using std::fixed;
using std::setprecision;
using std::clock;

// type definition

typedef unsigned long int uli;

// functor # 1

struct IsOdd {
    bool operator()(const double & i)
    {
        return ((int(i)%2) == 1);
    }
};

// functor # 2

struct IsEven {
    bool operator()(const double & i)
    {
        return ((int(i)%2) == 0);
    }
};

// the main function

int main()
{
    cout << fixed;
    cout << setprecision(5);

    //===================//
    // parallel settings //
    //===================//

    __gnu_parallel::_Settings s;
    s.algorithm_strategy = __gnu_parallel::force_parallel;
    __gnu_parallel::_Settings::set(s);

    //==========================//
    // end of parallel settings //
    //==========================//

    const uli DIM1 = 2 * uli(pow(10.0, 8.0));
    const uli TRIALS1 = 1 * uli(pow(10.0, 2.0));
    const uli DIV1 = 10;
    const double ELEM1 = 1.0;
    const double ELEM2 = 2.0;

    time_t t1;
    time_t t2;

    cout << " --> build vector" << endl;

    vector<double> v1(DIM1, ELEM1);
    vector<double> v2(DIM1, ELEM2);

    cout << " --> replace_if v1 and v2 in parallel" << endl;

    t1 = clock();

    for (uli i = 0; i != TRIALS1; i++) {
        if (i%DIV1 == 0) {
            cout << " -------------------------------------------------------->> " << i << endl;
        }
        std::__parallel::replace_if(v1.begin(), v1.end(), IsOdd(), 0);
        std::__parallel::replace_if(v2.begin(), v2.end(), IsEven(), 0);
    }

    t2 = clock();

    cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

    cout << " --> some results :" << endl;

    cout << " --> v1[0] = " << v1[0] << endl;
    cout << " --> v1[1] = " << v1[1] << endl;
    cout << " --> v1[2] = " << v1[2] << endl;
    cout << " --> v1[3] = " << v1[3] << endl;

    cout << " --> v2[0] = " << v2[0] << endl;
    cout << " --> v2[1] = " << v2[1] << endl;
    cout << " --> v2[2] = " << v2[2] << endl;
    cout << " --> v2[3] = " << v2[3] << endl;

    return 0;
}

// end
