//======================//
// parallel nth_element //
//======================//

#include <iostream>
#include <vector>
#include <iterator>
#include <cmath>
#include <ctime>
#include <iomanip>

#include <parallel/algorithm>
#include <parallel/numeric>
#include <parallel/settings.h>

using std::cout;
using std::endl;
using std::ostream_iterator;
using std::vector;
using std::pow;
using std::fixed;
using std::setprecision;
using std::clock;

// type definition

typedef unsigned long int uli;

// the main function

int main()
{
    cout << fixed;
    cout << setprecision(5);

    //===================//
    // parallel settings //
    //===================//

    __gnu_parallel::_Settings s;
    s.algorithm_strategy = __gnu_parallel::force_parallel;
    __gnu_parallel::_Settings::set(s);

    //==========================//
    // end of parallel settings //
    //==========================//

    const uli DIM1 = 1 * uli(pow(10.0, 8.0));
    const uli TRIALS1 = 1 * uli(pow(10.0, 2.0));
    const double ELEM1 = 1.0;

    time_t t1;
    time_t t2;

    cout << " --> build vector" << endl;

    vector<double> v1(DIM1, ELEM1);

    cout << " --> iota" << endl;

    iota(v1.begin(), v1.end(), 0.0);

    cout << " --> random_shuffle" << endl;

    random_shuffle(v1.begin(), v1.end());

    cout << " --> parallel nth_element vector v1 " << endl;

    t1 = clock();

    uli i = 0;

    while(i < TRIALS1) {
        std::__parallel::nth_element(v1.begin(), v1.begin()+(i%DIM1), v1.end());
        i++;
        //cout << " --> " << i%DIM1 << endl;
    }

    t2 = clock();

    cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

    cout << " --> some results :" << endl;

    cout << " --> v1[0] = " << v1[0] << endl;
    cout << " --> v1[1] = " << v1[1] << endl;
    cout << " --> v1[2] = " << v1[2] << endl;
    cout << " --> v1[3] = " << v1[3] << endl;

    return 0;
}

// end
