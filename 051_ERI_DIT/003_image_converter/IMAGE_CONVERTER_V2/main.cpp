#include <stdio.h>
#include <iostream>

const int MAX_LEN = 100;
const int MAX_LENGTH_COMMENT = 10000000;
const int PARSING_EXIT = -1;

// Aux function to putchar an integer to digit by digit (char by char)
void putchar_digit_by_digit(int num_loc) {
	int arr[MAX_LEN];
	int i = 0;

	if (num_loc == 0) {
		putchar('0');
		return;
	}

	while (num_loc != 0) {
		int r = num_loc % 10;
		arr[i] = r;
		i++;
		num_loc = num_loc / 10;
	}

	for (int j = i - 1; j >= 0; j--) {
		putchar(arr[j] + '0');
	}
}

// The p3 to p2 body function
void p3_to_p2_body(void) {
	int counter_local = 0;
	int num_local = 0;
	int num_final = 0;
	int ready_to_print = 0;

	while (1) {
		int c = getchar();
		if (c == EOF) break;

		if ((c != ' ') && (c != '\n') && (c != '\t')) {
			if ((c >= '0') && (c <= '9')) {
				num_local = num_local * 10 + (c - '0');
				ready_to_print = 1;
			}
		} else {
			if (ready_to_print) {

				counter_local++;

				if (counter_local == 1) {
					num_final = num_local * 299;
				} else if (counter_local == 2) {
					num_final += num_local * 587;
				} else if (counter_local == 3) {
					num_final += num_local * 114;
					num_final /= 1000;
				}

				if (counter_local == 3) {
					putchar_digit_by_digit(num_final);
					counter_local = 0;
				}
			}

			putchar(c);
			num_local = 0;
			ready_to_print = 0;
		}
	}
}

// The p2 to p1 body function
void p2_to_p1_body(int max_value) {
	int num_local = 0;
	int ready_to_print = 0;

	while (1) {
		int c = getchar();
		if (c == EOF) break;

		if ((c != ' ') && (c != '\n') && (c != '\t')) {
			if ((c >= '0') && (c <= '9')) {
				num_local = num_local * 10 + (c - '0');
				ready_to_print = 1;
			}
		} else {
			if (ready_to_print) {
				if (num_local <= ((max_value + 1) / 2)) {
					putchar('1');
				} else {
					putchar('0');
				}
			}
			putchar(c);
			num_local = 0;
			ready_to_print = 0;
		}
	}
}

// Check if the 1st letter is a 'P', else it is an error.
int check_magic_number_part_a(const int *c) {
	if (*c != 80) // 'P'
	{
		printf("INPUT ERROR");
		putchar(*c);
		return PARSING_EXIT;
	}
	putchar(*c);
	return 1;
}

// Check if the 2nd letter is '1', or '2' ...or '6', else it is an error.
int check_magic_number_part_b(int *c) {
	*c = getchar();

	if ((*c != 49) && (*c != 50) && (*c != 51) &&
		(*c != 52) && (*c != 53) && (*c != 54)) {
		putchar(*c);
		return PARSING_EXIT;
	}

	// Check if we have black-white images. We do nothing then.
	if ((*c == 49) || (*c == 52)) // '1', '4'
	{
		// We do nothing.
	} else if (*c == 50) // '2' to '1'
	{
		// Convert from gray-scale to black-white image.
		*c = 49;
	} else if (*c == 51) // '3' to '2'
	{
		// Convert from RGB image to gray-scale image.
		*c = 50;
	} else if (*c == 53) // '5' to '4'
	{
		// Convert from gray-scale to black-white image.
		*c = 52;
	} else // (c == '6') // '6' to '5'
	{
		// Convert from RGB image to gray-scale image.
		*c = 53;
	}
	putchar(*c);
	return 1;
}

// Check if the 3rd letter is 'space' or a 'newline', else it is an error.
int check_new_line_or_space(int *c) {
	*c = getchar();

	// "10" is a newline character and "32" is a space character.
	if ((*c != 10) && (*c != 32)) {
		printf("INPUT ERROR");
		putchar(*c);
		return PARSING_EXIT;
	}
	putchar(*c);
	return 1;
}

int check_for_the_new_line_and_then_number(int *c) {
	int counter_local = 0;

	while (1) {
		int c_now = *c;
		int c_next = getchar();

		if ((c_now == 10) && (c_next >= 49) && (c_next <= 57)) {
			*c = c_next;
			putchar(*c);
			break;
		} else {
			*c = c_next;
			putchar(c_next);
			counter_local++;
			if (counter_local > MAX_LENGTH_COMMENT) {
				printf("INPUT ERROR");
				return PARSING_EXIT;
			}
		}
	}
	return 1;
}

int parse_rows_number(int *c, int magic_number_part_b) {
	int num_of_rows;
	num_of_rows = (*c - '0');

	while (1) {
		*c = getchar();

		if ((magic_number_part_b != 5) && (magic_number_part_b != 2)) {
			putchar(*c);
		}

		if ((*c == 10) || (*c == 32)) {
			break;
		}

		num_of_rows = 10 * num_of_rows + (*c - '0');
	}

	return num_of_rows;
}

int parse_columns_number(int *c, int magic_number_part_b) {
	return parse_rows_number(c, magic_number_part_b);
}

int parse_max_value(int *c, int magic_number_part_b) {
	return parse_rows_number(c, magic_number_part_b);
}

void check_and_parse_white_space(int *c, int magic_number_part_b) {
	while (1) {
		*c = getchar();

		if ((*c == 10) || (*c == 32)) {
			putchar(*c);
			continue;
		} else {
			if ((magic_number_part_b != 5) && (magic_number_part_b != 2)) {
				putchar(*c);
			}
			break;
		}
	}
}

int main() {
	// data to get and then use
	int rows_number;
	int columns_number;
	int magic_number_part_b;
	int max_value;
	int c;

	//=================//
	// READ THE HEADER //
	//=================//
	{
		c = getchar();

		// CHECK # 1
		{
			int res = check_magic_number_part_a(&c);
			if (res == PARSING_EXIT) {
				return -1;
			}
		}

		// CHECK # 2
		{
			int res = check_magic_number_part_b(&c);
			if (res == PARSING_EXIT) {
				return -1;
			}

			magic_number_part_b = (c + 1) - '0';
		}

		// CHECK # 3
		{
			int res = check_new_line_or_space(&c);
			if (res == PARSING_EXIT) {
				return -1;
			}
		}

		// CHECK # 4
		{
			const int res = check_for_the_new_line_and_then_number(&c);
			if (res == PARSING_EXIT) {
				return -1;
			}
		}

		// CHECK # 5
		{
			columns_number = parse_columns_number(&c, 0);
		}

		// CHECK # 6
		{
			check_and_parse_white_space(&c, 0);
		}

		// CHECK # 7
		{
			rows_number = parse_rows_number(&c, 0);
		}

		// CHECK # 8
		if ((magic_number_part_b == 2) || (magic_number_part_b == 3) ||
			(magic_number_part_b == 5) || (magic_number_part_b == 6)) {
			// CHECK # 8-1
			{
				check_and_parse_white_space(&c, magic_number_part_b);
			}

			// CHECK # 8-2
			{
				max_value = parse_max_value(&c, magic_number_part_b);
			}
		}
	}

	//===============//
	// READ THE BODY //
	//===============//

	//==========//
	// P6 to P5 //
	//==========//
	{
		if (magic_number_part_b == 6) {
			int counter_local = 0;
			const int counter_max = rows_number * columns_number * 3;

			while (1) {
				int c1 = getchar();
				int c2 = getchar();
				int c3 = getchar();

				const int grey_color = (int)(((c1 * 299) + (c2 * 587) + (c3 * 114)) / 1000.0);

				putchar(grey_color);

				counter_local += 3;

				if (counter_local == counter_max) break;
			}
		}
	}

	//==========//
	// P5 to P4 //
	//==========//
	{
		if (magic_number_part_b == 5) {
			int char_to_build = 0;
			int c_gray;

			for (int i = 1; i <= rows_number; i++) {
				for (int j = 1; j <= columns_number; j++) {
					c_gray = getchar();

					if (c_gray <= ((max_value + 1) / 2)) {
						char_to_build = char_to_build << 1;
						char_to_build = char_to_build | 1;
					} else {
						char_to_build = char_to_build << 1;
					}

					if ((columns_number % 8 != 0) && (j > (columns_number - (columns_number % 8)))) {
						char_to_build = char_to_build << 1;
						char_to_build = char_to_build | 1;
					}

					if (((j % 8) == 0) || (j == columns_number)) {
						putchar(char_to_build);
						char_to_build = 0;
					}

				}
			}
		}
	}

	//==========//
	// P3 to P2 //
	//==========//
	{
		if (magic_number_part_b == 3) {
			p3_to_p2_body();
		}
	}

	//==========//
	// P2 to P1 //
	//==========//
	{
		if (magic_number_part_b == 2) {
			p2_to_p1_body(max_value);
		}
	}
}
