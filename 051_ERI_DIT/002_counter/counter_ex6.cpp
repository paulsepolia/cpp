#include <cstdlib>
#include <iostream>
#include <iomanip>

//==========================================================//
// IT IS NOT READY YET BUT GENERATES ALL THE POSSIBLE STOPS //
//==========================================================//

int get_nfl(const int* dests, int nrid)
{
	int nfl = 0;

	for (int i = 0; i < nrid; i++)
	{
		if (dests[i] > nfl)
		{
			nfl = dests[i];
		}
	}
	return nfl;
}

int powu(int base, int exp)
{
	int result = 1;
	while (exp > 0)
	{
		if (exp & 1)
		{
			result *= base;
		}
		base = base * base;
		exp >>= 1;
	}
	return result;
}

int solve(int nrid, int nst, const int* dests)
{
	int min_cost = 0;

	const int nfl = get_nfl(dests, nrid);
	const int nfl2 = nfl + 1;
	int* list = (int*)malloc(sizeof(int) * (nfl2 + 1));
	for (int i = 0; i <= nfl2; i++)
	{
		list[i] = i;
	}
	int* gp = (int*)malloc(nst * sizeof(int));
	int total_n = powu(nfl2, nst);

	for (int i = 0; i < total_n; ++i)
	{
		int n = i;
		for (int j = 0; j < nst; ++j)
		{
			gp[nst - j - 1] = list[n % nfl2];
			n /= nfl2;
		}

		//==============================//
		// Do the job for the LIFT here //
		//==============================//

		// Print the combinations for debugging purposes
		for (int kk = 0; kk < nst; kk++)
		{
			std::cout << std::setw(6) << gp[kk] << " ";
		}
		std::cout << std::endl;
	}

	free(gp);

	return min_cost;
}

int main()
{
	// INPUT
	const int nst = 2;
	const int nrid = 4;
	const int dests[nrid] = { 1, 2, 3, 4 };

	// OUTPUT
	const int res = solve(nrid, nst, dests);

	std::cout << "Min cost = " << res << std::endl;
}
