#include <iostream>
#include <vector>
#include <algorithm>

int main()
{
	const int nfl = 7;
	const int nrid = 4;
	const int nst = 3;
	const int dests[nrid] = { 1, 2, 3, 4 };

	//=================================================================//
	// Allocate the 3D matrix to hold all possible stops and each cost //
	int*** min_cost_for_each_stop_combination;
	min_cost_for_each_stop_combination = (int***)malloc(sizeof(int**) * nfl);
	for (int i1 = 0; i1 < nfl; i1++)
	{
		min_cost_for_each_stop_combination[i1] = (int**)malloc(sizeof(int*) * nfl);
	}
	for (int i1 = 0; i1 < nfl; i1++)
	{
		for (int i2 = 0; i2 < nfl; i2++)
		{
			min_cost_for_each_stop_combination[i1][i2] = (int*)malloc(sizeof(int) * nfl);
		}
	}
	// Done with 3D matrix allocation //
	//================================//

	//=================================================================================//
	// Build the 3D matrix which holds all the possible stops and the related min cost //
	for (int i1 = 0; i1 < nfl; i1++)
	{
		for (int i2 = 0; i2 < nfl; i2++)
		{
			for (int i3 = 0; i3 < nfl; i3++)
			{
				for (const auto& el: dests)
				{
					std::vector<int> costs{ std::abs(el - i1),
											std::abs(el - i2),
											std::abs(el - i3) };

					std::sort(costs.begin(), costs.end());

					min_cost_for_each_stop_combination[i1][i2][i3] += costs.at(0);
				}
			}
		}
	}

	// Find the minimum cost and the related stops

	int total_min_cost = min_cost_for_each_stop_combination[0][0][0];
	int stop1 = 0;
	int stop2 = 0;
	int stop3 = 0;

	for (int i1 = 0; i1 < nfl; i1++)
	{
		for (int i2 = 0; i2 < nfl; i2++)
		{
			for (int i3 = 0; i3 < nfl; i3++)
			{
				if (total_min_cost > min_cost_for_each_stop_combination[i1][i2][i3])
				{
					total_min_cost = min_cost_for_each_stop_combination[i1][i2][i3];
					stop1 = i1;
					stop2 = i2;
					stop3 = i3;
				}
			}
		}
	}

	std::cout << "Total min cost is " << total_min_cost << " at stops "
			  << stop1 << ", " << stop2 << ", " << stop3 << std::endl;

	//=======================================//
	// Free up the Dynamically allocated RAM //
	//=======================================//
	for (int i1 = 0; i1 < nfl; i1++)
	{
		for (int i2 = 0; i2 < nfl; i2++)
		{
			free(min_cost_for_each_stop_combination[i1][i2]);
		}
	}

	for (int i1 = 0; i1 < nfl; i1++)
	{
		free(min_cost_for_each_stop_combination[i1]);
	}

	free(min_cost_for_each_stop_combination);
}
