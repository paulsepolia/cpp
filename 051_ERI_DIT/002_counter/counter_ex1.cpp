#include <iostream>
#include <iomanip>

// FROM
// 0, 0, 0
// 0, 0, 1
// 0, 0, 2
//...

// TO
//...
// 7, 7, 4
// 7, 7, 5
// 7, 7, 6
// 7, 7, 7


int main()
{
	const int DISPLAY_DIST = 6;
	const int nfl = 7;
	int counter1 = 0;
	int counter2 = 0;
	int counter3;

	for (int k = 0; k <= (nfl + 1) * (nfl + 1) * nfl + nfl; k++)
	{
		counter3 = k % (nfl + 1);

		std::cout << std::setw(DISPLAY_DIST) << counter1
				  << std::setw(DISPLAY_DIST) << counter2
				  << std::setw(DISPLAY_DIST) << counter3 << std::endl;

		if (counter3 == nfl)
		{
			counter2++;
			if (counter2 > nfl)
			{
				counter1++;
				counter2 = 1;
			}
		}
	}
}
