#include <iostream>
#include <iomanip>
#include <vector>
#include <algorithm>

// FROM
// 0, 0, 0
// 0, 0, 1
// 0, 0, 2
//...

// TO
//...
// 6, 6, 4
// 6, 6, 5
// 6, 6, 6

int main()
{
	const int DISPLAY_DIST = 6;
	const int nfl = 8;
	const int nrid = 4;
	const int dests[nrid] = { 1, 2, 3, 4 };
	int c1 = 0;
	int c2 = 0;
	int c3;

	//=================================================================//
	// Allocate the 3D matrix to hold all possible stops and each cost //
	//=================================================================//
	int*** min_cost_for_each_stop_combination;
	min_cost_for_each_stop_combination = (int***)malloc(sizeof(int**) * nfl);
	for (int i1 = 0; i1 < nfl; i1++)
	{
		min_cost_for_each_stop_combination[i1] = (int**)malloc(sizeof(int*) * nfl);
	}
	for (int i1 = 0; i1 < nfl; i1++)
	{
		for (int i2 = 0; i2 < nfl; i2++)
		{
			min_cost_for_each_stop_combination[i1][i2] = (int*)malloc(sizeof(int) * nfl);
		}
	}
	//================================//
	// Done with 3D matrix allocation //
	//================================//

	for (int k = 0; k < nfl * nfl * nfl; k++)
	{
		c3 = k % nfl;

		// Print the stops combination
		std::cout << std::setw(DISPLAY_DIST) << c1
				  << std::setw(DISPLAY_DIST) << c2
				  << std::setw(DISPLAY_DIST) << c3 << std::endl;

		// Build the matrix which holds the minimum cost the stops combination
		for (const auto& el: dests)
		{
			std::vector<int> costs{ std::abs(el - c1),
									std::abs(el - c2),
									std::abs(el - c3) };

			std::sort(costs.begin(), costs.end());

			min_cost_for_each_stop_combination[c1][c2][c3] += costs.at(0);
		}

		// Do the maths for the counter here
		if (c3 == nfl - 1)
		{
			c2++;
			if (c2 == nfl)
			{
				c1++;
				c2 = 0;
			}
		}
	}

	// Display the results here
	for (int i1 = 0; i1 < nfl; i1++)
	{
		for (int i2 = 0; i2 < nfl; i2++)
		{
			for (int i3 = 0; i3 < nfl; i3++)
			{
				std::cout << std::setw(DISPLAY_DIST) << i1
						  << std::setw(DISPLAY_DIST) << i2
						  << std::setw(DISPLAY_DIST) << i3
						  << std::setw(DISPLAY_DIST) << min_cost_for_each_stop_combination[i1][i2][i3]
						  << std::endl;
			}
		}
	}

	// Find the minimum cost and the related stops

	int total_min_cost = min_cost_for_each_stop_combination[0][0][0];
	int stop1 = 0;
	int stop2 = 0;
	int stop3 = 0;

	for (int i1 = 0; i1 < nfl; i1++)
	{
		for (int i2 = 0; i2 < nfl; i2++)
		{
			for (int i3 = 0; i3 < nfl; i3++)
			{
				if (total_min_cost > min_cost_for_each_stop_combination[i1][i2][i3])
				{
					total_min_cost = min_cost_for_each_stop_combination[i1][i2][i3];
					stop1 = i1;
					stop2 = i2;
					stop3 = i3;
				}
			}
		}
	}

	std::cout << "Total min cost is " << total_min_cost << " at stops "
			  << stop1 << ", " << stop2 << ", " << stop3 << std::endl;

	//=======================================//
	// Free up the Dynamically allocated RAM //
	//=======================================//
	for (int i1 = 0; i1 < nfl; i1++)
	{
		for (int i2 = 0; i2 < nfl; i2++)
		{
			free(min_cost_for_each_stop_combination[i1][i2]);
		}
	}

	for (int i1 = 0; i1 < nfl; i1++)
	{
		free(min_cost_for_each_stop_combination[i1]);
	}

	free(min_cost_for_each_stop_combination);
}
