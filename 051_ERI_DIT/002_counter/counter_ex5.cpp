#include <cstdio>
#include <cstring>
#include <cstdlib>

int powu(int base, int exp)
{
	int result = 1;
	while (exp > 0)
	{
		if (exp & 1)
		{
			result *= base;
		}
		base = base * base;
		exp >>= 1;
	}
	return result;
}

int main()
{
	char list[] = "01234567";
	int gp_len = 6;
	int list_len = (int)strlen(list);
	char* gp = (char*)malloc((gp_len + 1) * sizeof(char));
	int total_n = powu(list_len, gp_len);

	for (int i = 0; i < total_n; ++i)
	{
		int n = i;
		for (int j = 0; j < gp_len; ++j)
		{
			gp[gp_len - j - 1] = list[n % list_len];
			n /= list_len;
		}
		printf("%s\n", gp);
	}

	free(gp);
}

