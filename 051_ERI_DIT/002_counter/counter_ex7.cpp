#include <cstdlib>
#include <cstdio>
#include <iostream>

void print_combination(const int* stops, int k)
{
	for (int i = 0; i < k; i++)
	{
		printf("%d ", stops[i]);
	}
	printf("\n");
}

void generate_combinations(int m, int k, int idx, int* stops)
{
	if (idx == k)
	{
		// We have generated a combination of k stops
		print_combination(stops, k);
		return;
	}

	// Generate all combinations starting from the current floor
	for (int i = 1; i <= m; i++)
	{
		stops[idx] = i;
		generate_combinations(m, k, idx + 1, stops);
	}
}

int main()
{
	{
		// Example #1
		std::cout << "------------------------------------------------>> 1" << std::endl;
		int m = 10;
		int k = 2;
		int* stops = (int*)malloc(k * sizeof(int));
		generate_combinations(m, k, 0, stops);
		free(stops);
	}

	{
		// Example #2
		std::cout << "------------------------------------------------>> 2" << std::endl;
		int m = 10;
		int k = 3;
		int* stops = (int*)malloc(k * sizeof(int));
		generate_combinations(m, k, 0, stops);
		free(stops);
	}
}
