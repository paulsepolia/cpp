#include <stdio.h>
#include <stdlib.h>
#include "lift.h"

const int LARGE_FLAG_VALUE = 0x7FFFFFFF;

int aux_print_function(int nrid, int nst, const int* dests)
{
	printf("%d ", nrid);
	printf("%d\n", nst);

	for (int i = 0; i < nrid; i++)
	{
		printf("%d ", dests[i]);
	}

	printf("\n");
	const int total_min_cost = solve(nrid, nst, dests);
	printf("Cost is: %d\n", total_min_cost);
	return total_min_cost;
}

int get_nfl(const int* dests, int nrid)
{
	int nfl = 0;

	for (int i = 0; i < nrid; i++)
	{
		if (dests[i] > nfl)
		{
			nfl = dests[i];
		}
	}

	return nfl;
}

int fw_a_b(const int* dests, int nrid, int a, int b)
{
	int total_cost_a_b = 0;

	for (int i = 0; i < nrid; i++)
	{
		if ((dests[i] > a) && (dests[i] <= b))
		{
			const int cost_a_to_dest_i = dests[i] - a;
			const int cost_b_to_dest_i = b - dests[i];

			if (cost_a_to_dest_i <= cost_b_to_dest_i)
			{
				total_cost_a_b += cost_a_to_dest_i;
			}
			else
			{
				total_cost_a_b += cost_b_to_dest_i;
			}
		}
	}

	return total_cost_a_b;
}

int fw_a_infinity(const int* dests, int nrid, int a)
{
	int total_cost_a_infinity = 0;

	for (int i = 0; i < nrid; i++)
	{
		if (dests[i] > a)
		{
			total_cost_a_infinity += (dests[i] - a);
		}
	}

	return total_cost_a_infinity;
}

int fw_0_infinity(const int* dests, int nrid)
{
	int total_cost_0_infinity = 0;

	for (int i = 0; i < nrid; i++)
	{
		total_cost_0_infinity += dests[i];

	}

	return total_cost_0_infinity;
}


int M_0_j(const int* dests, int nrid)
{
	return fw_0_infinity(dests, nrid);
}

int solve(int nrid, int nst, const int* dests)
{
	const int nfl = get_nfl(dests, nrid);
	int total_min_cost = LARGE_FLAG_VALUE;
	int last_stop_at_floor = 0;

	// Build the matrix to hold the M_i_j values already evaluated
	int** matrix_nst_nfl;
	matrix_nst_nfl = (int**)(malloc(sizeof(int*) * (nst + 1)));
	for (int i = 0; i <= nst; i++)
	{
		matrix_nst_nfl[i] = (int*)(malloc(sizeof(int) * (nfl + 1)));
	}

	// Initialize the matrix with a very large flag value
	for (int i = 0; i <= nst; i++)
	{
		for (int j = 0; j <= nfl; j++)
		{
			matrix_nst_nfl[i][j] = LARGE_FLAG_VALUE;
		}
	}

	//=====================================================//
	// Do the evaluation of the matrix M_i_j elements here //
	//=====================================================//

	// For i = 0 then M_0_j = M_0_j(dests, nrid)
	for (int j = 0; j <= nfl; j++)
	{
		matrix_nst_nfl[0][j] = M_0_j(dests, nrid);
	}

	// For i > 0, then do the following
	for (int i = 1; i <= nst; i++)
	{
		for (int j = 0; j <= nfl; j++)
		{
			int min_value = LARGE_FLAG_VALUE;

			for (int k = 0; k <= j; k++)
			{
				int min_value_local = matrix_nst_nfl[i - 1][k] -
									  fw_a_infinity(dests, nrid, k) +
									  fw_a_b(dests, nrid, k, j) +
									  fw_a_infinity(dests, nrid, j);

				if (min_value > min_value_local)
				{
					min_value = min_value_local;
				}
			}

			matrix_nst_nfl[i][j] = min_value;
		}
	}

	for (int j = 0; j <= nfl; j++)
	{
		const int total_min_cost_tmp = matrix_nst_nfl[nst][j];

		if (total_min_cost > total_min_cost_tmp)
		{
			last_stop_at_floor = j;
			total_min_cost = total_min_cost_tmp;
		}
	}

	if (last_stop_at_floor != 0)
	{
		printf("Last stop at floor: %d\n", last_stop_at_floor);
	}
	else
	{
		printf("No lift stops\n");
	}

	// Free up the dynamically allocated RAM
	for (int i = 0; i <= nst; i++)
	{
		free(matrix_nst_nfl[i]);
	}
	free(matrix_nst_nfl);

	return total_min_cost;
}
