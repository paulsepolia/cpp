#include <stdio.h>
#include <stdlib.h>
#include "lift.h"

const int LARGE_FLAG_VALUE = 0x7FFFFFFF;

int generate_combinations_and_find_min(int nfl, int nst, int* stops, int nrid, const int* dests)
{
	// Initialize the first stop to be 1
	for (int i = 0; i < nst; i++)
	{
		stops[i] = 1;
	}

	// Generate all the combinations of stops
	int min_cost = LARGE_FLAG_VALUE;
	while (1)
	{
		//===================================//
		// Do the job for the LIFT from here //
		int total_cost_for_certain_stops = 0;

		for (int kk1 = 0; kk1 < nrid; kk1++)
		{
			int min_cost_for_id = LARGE_FLAG_VALUE;

			for (int kk2 = 0; kk2 < nst; kk2++)
			{
				const int min_cost_for_id_local = abs(stops[kk2] - dests[kk1]);

				if (min_cost_for_id > min_cost_for_id_local)
				{
					min_cost_for_id = min_cost_for_id_local;
				}
			}

			// We should consider the 0 floor as a stop here.
			const int min_cost_for_id_local_zero = abs(0 - dests[kk1]);

			if (min_cost_for_id_local_zero < min_cost_for_id)
			{
				min_cost_for_id = min_cost_for_id_local_zero;
			}

			total_cost_for_certain_stops += min_cost_for_id;
		}

		if (min_cost > total_cost_for_certain_stops)
		{
			min_cost = total_cost_for_certain_stops;
		}
		// Up to here //
		//============//

		// Check if we have reached the maximum number of floors
		int counter = 0;
		for (int i = 0; i < nst; i++)
		{
			if (stops[i] == nfl)
			{
				counter++;
			}
		}

		if (counter == nst) break;

		// Increment the last stop
		stops[nst - 1]++;

		// Carry over to the next stop if necessary
		for (int i = nst - 1; i > 0; i--)
		{
			if (stops[i] == nfl + 1)
			{
				stops[i] = 1;
				stops[i - 1]++;
			}
		}
	}
	return min_cost;
}

int aux_print_function(int nrid, int nst, const int* dests)
{
	printf("%d ", nrid);
	printf("%d\n", nst);

	for (int i = 0; i < nrid; i++)
	{
		printf("%d ", dests[i]);
	}

	printf("\n");
	const int total_min_cost = solve(nrid, nst, dests);
	printf("Cost is: %d\n", total_min_cost);
	return total_min_cost;
}

int get_nfl(const int* dests, int nrid)
{
	int nfl = 0;

	for (int i = 0; i < nrid; i++)
	{
		if (dests[i] > nfl)
		{
			nfl = dests[i];
		}
	}

	return nfl;
}

int solve(int nrid, int nst, const int* dests)
{
	if (nst == 0)
	{
		int res = 0;
		for (int i = 0; i < nrid; i++)
		{
			res += dests[i];
		}
		return res;
	}

	const int nfl = get_nfl(dests, nrid);
	int* stops = (int*)malloc(nst * sizeof(int));

	int min_cost = generate_combinations_and_find_min(nfl, nst, stops, nrid, dests);

	free(stops);

	return min_cost;
}
