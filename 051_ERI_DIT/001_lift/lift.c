#include <stdio.h>
#include "lift.h"

int main()
{
	{
		printf("----------------->> Example # 1\n");
		// Input
		const int nrid = 5;
		const int nst = 1;
		const int dests[] = { 10, 10, 10, 10, 10 };
		// Output
		const int res = aux_print_function(nrid, nst, dests);
		const int res_correct = 0;
		if (res != res_correct)
		{
			printf("ERROR");
			return -1;
		}
	}

	{
		printf("----------------->> Example # 2\n");
		// Input
		const int nrid = 5;
		const int nst = 0;
		const int dests[] = { 1, 1, 1, 1, 10 };
		// Output
		const int res = aux_print_function(nrid, nst, dests);
		const int res_correct = 14;
		if (res != res_correct)
		{
			printf("ERROR");
			return -1;
		}
	}

	{
		printf("----------------->> Example # 3\n");
		// Input
		const int nrid = 5;
		const int nst = 1;
		const int dests[] = { 1, 1, 1, 1, 10 };
		// Output
		const int res = aux_print_function(nrid, nst, dests);
		const int res_correct = 4;
		if (res != res_correct)
		{
			printf("ERROR");
			return -1;
		}
	}

	{
		printf("----------------->> Example # 4\n");
		// Input
		const int nrid = 5;
		const int nst = 2;
		const int dests[] = { 1, 1, 1, 1, 10 };
		// Output
		const int res = aux_print_function(nrid, nst, dests);
		const int res_correct = 0;
		if (res != res_correct)
		{
			printf("ERROR");
			return -1;
		}
	}

	{
		printf("----------------->> Example # 5\n");
		// Input
		const int nrid = 5;
		const int nst = 2;
		const int dests[] = { 11, 2, 7, 13, 7 };
		// Output
		const int res = aux_print_function(nrid, nst, dests);
		const int res_correct = 4;
		if (res != res_correct)
		{
			printf("ERROR");
			return -1;
		}
	}

	{
		printf("----------------->> Example # 6\n");
		// Input
		const int nrid = 7;
		const int nst = 3;
		const int dests[] = { 8, 10, 3, 8, 12, 6, 5 };
		// Output
		const int res = aux_print_function(nrid, nst, dests);
		const int res_correct = 5;
		if (res != res_correct)
		{
			printf("ERROR");
			return -1;
		}
	}

	{
		printf("----------------->> Example # 7\n");
		// Input
		const int nrid = 6;
		const int nst = 4;
		const int dests[] = { 5, 3, 3, 5, 5, 3 };
		// Output
		const int res = aux_print_function(nrid, nst, dests);
		const int res_correct = 0;
		if (res != res_correct)
		{
			printf("ERROR");
			return -1;
		}
	}

	{
		printf("----------------->> Example # 8\n");
		// Input
		const int nrid = 5;
		const int nst = 0;
		const int dests[] = { 1, 2, 3, 4, 5 };
		// Output
		const int res = aux_print_function(nrid, nst, dests);
		const int res_correct = 15;
		if (res != res_correct)
		{
			printf("ERROR");
			return -1;
		}
	}

	{
		printf("----------------->> Example # 9\n");
		// Input
		const int nrid = 20;
		const int nst = 4;
		const int dests[] = { 8, 32, 14, 14, 6, 7, 25, 43, 12, 9, 1, 28, 27, 25, 33, 38, 42, 27, 14, 44 };
		// Output
		const int res = aux_print_function(nrid, nst, dests);
		const int res_correct = 30;
		if (res != res_correct)
		{
			printf("ERROR");
			return -1;
		}
	}

	{
		printf("----------------->> Example # 10\n");
		// Input
		const int nrid = 20;
		const int nst = 5;
		const int dests[] = { 8, 32, 14, 14, 6, 7, 25, 43, 12, 9, 1, 28, 27, 25, 33, 38, 42, 27, 14, 44 };
		// Output
		const int res = aux_print_function(nrid, nst, dests);
		const int res_correct = 20;
		if (res != res_correct)
		{
			printf("ERROR");
			return -1;
		}
	}

	{
		printf("----------------->> Example # 11\n");
		// Input
		const int nrid = 20;
		const int nst = 6;
		const int dests[] = { 8, 32, 14, 14, 6, 7, 25, 43, 12, 9, 1, 28, 27, 25, 33, 38, 42, 27, 14, 44 };
		// Output
		const int res = aux_print_function(nrid, nst, dests);
		const int res_correct = 15;
		if (res != res_correct)
		{
			printf("ERROR");
			return -1;
		}
	}

	{
		printf("----------------->> Example # 12\n");
		// Input
		const int nrid = 25;
		const int nst = 7;
		const int dests[] = { 1, 2, 2, 3, 5, 6, 6, 8, 10, 13, 15, 15, 16, 17, 18, 18, 18, 20, 22, 25, 30, 38, 49, 55,
							  62 };
		// Output
		const int res = aux_print_function(nrid, nst, dests);
		const int res_correct = 35;
		if (res != res_correct)
		{
			printf("ERROR");
			return -1;
		}
	}
}
