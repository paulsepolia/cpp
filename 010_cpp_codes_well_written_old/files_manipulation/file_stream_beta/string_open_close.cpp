// string, open, close

#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>


using namespace std;

int main()
{
    // declare variables; step 1

    ifstream inFile;
    ofstream outFile;

    double test1, test2, test3, test4, test5;
    double average;

    string firstName;
    string lastName;

    inFile.open("test.txt");                              // step 2
    outFile.open("testavg.out");                          // step 3

    outFile << fixed << showpoint;                        // step 4.a
    outFile << setprecision(2);                           // step 4.b

    cout << "Processing data" << endl;

    inFile >> firstName >> lastName;                      // step 5
    outFile << "Student name: " << firstName
            << " " << lastName << endl;                   //step 6

    inFile >> test1 >> test2 >> test3 >> test4 >> test5;  // step 7

    outFile << "Test scores: " << setw(6) << test1         // step 8
            << setw(6) << test2 << setw(6) << test3
            << setw(6) << test4 << setw(6) << test5
            << endl;

    average = (test1 + test2 + test3 + test4 + test5) / 5.0;  // step 9

    outFile << "Average test score: " << setw(6) << average << endl;

    inFile.close();
    outFile.close();

    return 0;
}