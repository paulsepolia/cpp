// fail state
// once an input stream enters a fail state,
// all further I/O statements
// using that stream are ignored.

#include <iostream>

using namespace std;

int main()
{
    int a = 10;
    int b = 20;
    int c = 30;
    int d = 40;

    cout << " Line 5: Enter four integers: ";
    cin >> a >> b >> c >> d;
    cout << endl;
    cout << " Line 8: The numbers you entered are:"
         << endl;
    cout << " Line 9: a = " << a << ", b = " << b
         << ", c = " << c << ", d = " << d << endl;

    int anew = 23;
    int bnew = 34;

    cout << " Line 3: Enter a number followed by a character: ";
    cin >> anew >> bnew;
    cout << endl << "Line 5: a = " << anew << ", b = " << bnew << endl;

    cin.clear(); // restore input stream

    cout << " i am after cin.clear() " << endl;

    cin.ignore(20,'/n'); // clear the buffer

    cout << "Line 8: Enter two numbers: ";
    cin >> anew >> bnew;
    cout << endl << "Line 10: a = " << anew << ", b = " << bnew << endl;

    return 0;
}