//==============================//
// Load the file into RAM.      //
// Write the file to hard disk. //
//==============================//

#include <iostream>
#include <fstream>

using std::ifstream;
using std::ofstream;
using std::endl;
using std::cout;
using std::ios;

int main()
{
    // 1. Opening the input stream.

    ifstream is;
    is.open("fileIn.txt");

    // 2. Get the length of the file.

    long length; // type long to be large enough

    is.seekg(0, ios::end);
    length = is.tellg();
    is.seekg(0, ios::beg);

    // 3. A test.

    cout << " length " << length << endl;

    // 4. Container for the file in RAM.
    //    Allocate memory.

    char* buffer;
    buffer = new char [length];

    // 5. Read data as a block to buffer object.

    is.read(buffer, length);

    // 6. Close input stream.

    is.close();

    // 7. Opening the output stream.

    ofstream os;
    os.open("fileOut.txt");

    // 8. Write to the output stream.

    os.write(buffer, length);

    // 9. Free up RAM.
    //    Deletion of the buffer object.

    delete[] buffer;

    return 0;
}

//==============//
// End of code. //
//==============//
