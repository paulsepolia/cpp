// Using the copy algorithm for I/O.

#include <fstream>
#include <iostream>
#include <iterator>
#include <vector>

using namespace std;

typedef istream_iterator<int> istream_iter;

int main()
{
    vector<int> a;
    ifstream file("example.txt");
    if (file.fail()) {
        cout << "Cannot open file example.txt.\n";
        return 1;
    }

    copy(istream_iter(file), istream_iter(), inserter(a, a.begin()));

    copy(a.begin(), a.end(), ostream_iterator<int>(cout, " "));

    cout << endl;

    return 0;
}

// end of code
