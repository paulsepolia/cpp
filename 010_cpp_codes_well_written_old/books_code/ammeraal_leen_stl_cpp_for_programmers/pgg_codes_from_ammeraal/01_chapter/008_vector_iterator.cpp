// reading and writing a variable number of
// nonzero integers (followed in the input by 0).

#include <iostream>
#include <cstdlib>
#include <vector>
using namespace std;

int main()
{
    vector<double> v;
    vector<double>::iterator i;
    double x;
    double sum_local;
    int sentinel;
    const long int dimen = 10E5;
    long int ic;

    cout << " Enter an integer to start the process:";
    cin >> sentinel;

    for (ic = 0; ic < dimen; ic++) {
        v.push_back( static_cast<double>(ic) );
    }

    cout << " v.begin() = " << *v.begin() << endl;
    cout << " v.end()   = " << *v.end()   << endl;

    for (ic = 0; ic < dimen; ic++) {
        v[ic] = rand() / 10E9;
    }

    sum_local = 0.0;

    for (ic = 0; ic < dimen; ic++) {
        sum_local = sum_local + v[ic];
    }

    cout << " sum_local = " << sum_local << endl;

    cout << endl;

    return 0;
}

// end of code
