// a template class

#include <iostream>

using namespace std;

class PairDouble {
public:
    PairDouble(double x1, double y1): x(x1), y(y1) {}
    void showQ();
private:
    int x, y;
};

void PairDouble::showQ()
{
    cout << x/y << endl;
}

class PairInt {
public:
    PairInt(int x1, int y1): x(x1), y(y1) {}
    void showQ();
private:
    double x, y;
};

void PairInt::showQ()
{
    cout << x/y << endl;
}

int main()
{
    PairDouble a(37.0, 5.0);
    PairInt u(37, 5);
    a.showQ();
    u.showQ();

    return 0;
}

