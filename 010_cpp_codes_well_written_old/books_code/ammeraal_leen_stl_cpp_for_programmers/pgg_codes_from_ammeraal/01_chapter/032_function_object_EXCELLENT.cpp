// sorting in descending order,
// using a function object of our own.

#include <iostream>
#include <iterator>
#include <algorithm>

using namespace std;

// function object
class compare {
public:
    bool operator()(int x, int y) const
    {
        return x < y;
    }
};

// main function
int main()
{
    const int N = 8;
    int a[N] = {1234, 5432, 1234, 3124, 1243, 445245, 534543, 534524};

    cout << "Before sorting:\n";
    copy(a, a+N, ostream_iterator<int>(cout, " "));
    cout << endl;

    sort(a, a+N, compare());
    cout << "After sorting:\n";
    copy(a, a+N, ostream_iterator<int>(cout, " "));
    cout << endl;

    return 0;
}

// end of code
