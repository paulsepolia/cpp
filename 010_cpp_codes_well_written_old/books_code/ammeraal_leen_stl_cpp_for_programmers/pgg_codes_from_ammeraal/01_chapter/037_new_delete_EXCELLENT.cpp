// new and delete

#include <iostream>

using namespace std;

int main()
{
    const long RUN = 10E7;
    const long SIZE_DIM = 10E5;
    long i;
    long *p;
    long *q;


    for (i = 0; i < RUN; i++) {
        p = new long;
        q = new long[SIZE_DIM];

        delete p;
        delete [] q;
    }

    return 0;
}

// end of code

