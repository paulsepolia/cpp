// the remove and remove_if algorithm

#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>

using namespace std;

// function definition

void out(const char *s, const vector<double> &v)
{
    cout << s;
    copy(v.begin(), v.begin()+5, ostream_iterator<int>(cout, " "));
    cout << endl;
}

bool cond( double x)
{
    return x <= 10E5;
}

// main function

int main()
{
    vector<double> v;
    vector<double>::iterator new_end;
    const long SIZE_VEC = 10E5;
    long i;

    for (i = 0; i < SIZE_VEC; i++) {
        v.push_back(rand()/10E1);
    }

    out("Initial sequence v:\n", v);

    new_end = remove(v.begin(), v.end(), 1);

    out("After remove: ", v);

    v.erase(new_end, v.end());

    out("After erase: ", v);

    new_end = remove_if(v.begin(), v.end(), cond);

    v.erase(new_end, v.end());

    out("After erase: ", v);

    return 0;
}

// end of code
