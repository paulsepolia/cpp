// forming sums

#include <iostream>
#include <numeric>
#include <cstdlib>
#include <iomanip>

using namespace std;

int main()
{
    const long SIZE_ARR = 10E7;
    const long TRIALS = 100;
    long i;
    long k;
    double *array;
    array = new double[SIZE_ARR];
    double sum;

    cout << setprecision(10) << fixed;

    for (k = 0; k < TRIALS; k++) {
        cout << endl;
        cout << "  1. Building the array. " << endl;
        for (i = 0; i < SIZE_ARR; i++) {
            array[i] = rand()/10E8;
        }

        cout << endl;
        cout << "  2. Adding the elements. " << endl;
        sum = 0.0;
        sum = accumulate(array, array+SIZE_ARR, sum);

        cout << endl;
        cout << "  3. The sum of all elements: " << sum << endl;

        cout << endl;
        cout << "  4. The average value of all elements: " << sum / SIZE_ARR << endl;

        cout << endl;
        cout << "  5. -------------> " << k << endl;
    }

    cout << endl;
    return 0;
}

// end of code
