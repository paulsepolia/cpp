// counting

#include <iostream>
#include <string>
#include <algorithm>
#include <cstring>

using namespace std;

int main()
{
    char *p = "This demonstrates the STL";
    int n = count(p, p + strlen(p), 'e');
    cout << n << " occurrences of 'e' found.\n";

    const long SIZE_ARR = 10E4;
    long i;
    long *a = new long[SIZE_ARR];

    cout << endl;
    cout << "  1. Building the array. " << endl;
    for (i = 0; i < SIZE_ARR; i++) {
        a[i] = i%100;
    }

    cout << endl;
    cout << "  2. Counting. " << endl;
    long int m = count(a, a + SIZE_ARR, 10);
    cout << m << " occurrences of 10 found.\n";

    cout << endl;
    return 0;
}

// end of code

