// Two multisets

#include <iostream>
#include <set>
#include <iterator>

using namespace std;

int main()
{
    multiset<int, less<int> > S;

    S.insert(10);
    S.insert(20);
    S.insert(30);
    S.insert(10);

    multiset<int, less<int> > T;

    T.insert(20);
    T.insert(30);
    T.insert(10);

    if (S == T ) cout << " Equal multisets:\n";
    else
        cout << " Unequal multisets:\n";

    cout << " S: ";
    copy(S.begin(), S.end(), ostream_iterator<int>(cout, "  "));
    cout << endl;

    cout << " T: ";
    copy(T.begin(), T.end(), ostream_iterator<int>(cout, "  "));
    cout << endl;

    cout << endl;
    return 0;
}

// end of code
