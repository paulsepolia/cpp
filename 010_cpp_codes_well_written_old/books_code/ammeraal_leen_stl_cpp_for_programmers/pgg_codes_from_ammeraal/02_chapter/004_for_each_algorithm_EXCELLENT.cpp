// The for_each algorithm.

#include <iostream>
#include <algorithm>

using namespace std;

// function definition

void display(int x)
{
    static int i = 0;
    cout << " a[" << i++ << "] = " << x << endl;
}

// main function

int main()
{
    const int N = 4;
    int a[N] = {2, 3, 1, 10};

    for_each(a, a+N, display);

    return 0;
}

// end of code
