// A multimap containing equal keys.

#include <iostream>
#include <string>
#include <map>
#include <cstring>

using namespace std;

// the class

class compare {
public:
    bool operator()(const char *s, const char *t) const
    {
        return strcmp(s, t) < 0;
    }
};

// type definition

typedef multimap<char*, long, compare> mmtype;

// main function

int main()
{
    mmtype D;

    D.insert(mmtype::value_type("Johnson, J.", 12345));
    D.insert(mmtype::value_type("Smith, P.", 54321));
    D.insert(mmtype::value_type("Johnson, J.", 12345));
    D.insert(mmtype::value_type("Johnson, J.", 11111));

    cout << " There are " << D.size() << " elements.\n";

    return 0;
}

// end of code
