// string demonstration program.

#include <iostream>
#include <string>

using namespace std;

int main()
{
    string s(10, 'A');
    string t("BC");
    string u;

    u = s + t;

    cout << " u = s + t = " << u << endl;
    cout << " u.size() = " << u.size() << endl;
    cout << " u[6] = " << u[6] << endl;

    cout << " Enter two new strings s and t:\n";
    cin >> s >> t;

    cout << " s.size() = " << s.size() << endl;
    cout << " t.size() = " << t.size() << endl;
    cout << (s < t ? "s < t " : "s >= t");
    cout << endl;

    return 0;
}

// end of code
