// an application of a map: a telephone directory

#include <iostream>
#include <fstream>
#include <iomanip>
#include <cstring>
#include <map>

using namespace std;

// definitions

const int maxlen = 200;

//class

class compare {
public:
    bool operator()(const char *s, const char *t) const
    {
        return strcmp(s, t) < 0;
    }
};

// type definition

typedef map<char*, long, compare> directype;

// function definition

void ReadInput(directype &D)
{
    ifstream ifstr("phone.txt");
    long nr;
    char buf[maxlen], *p;
    if (ifstr) {
        cout << " Entries read from file phone.txt:\n";
        for (;;) {
            ifstr >> nr;
            ifstr.get();  // skip space
            ifstr.getline(buf, maxlen);
            if (!ifstr) break;
            cout << setw(9) << nr << " " << buf << endl;
            p = new char[strlen(buf) + 1];
            strcpy(p, buf);
            D[p] = nr;
        }
    }
    ifstr.close();
}

// function definition

void ShowCommands()
{
    cout <<
         "Commands: ?name       : find phone number, \n"
         "          /name       : delete\n"
         "          !number name: insert (or update)\n"
         "          *           : list whole phonebook\n"
         "          =           : save in file\n"
         "          #           : exit" << endl;
}

// function definition

void ProcessCommands(directype &D)
{
    ofstream ofstr;
    long nr;
    char ch;
    char buf[maxlen];
    char *p;
    directype::iterator i;

    for(;;) {
        cin >> ch; // skip any white-space and read ch.
        switch (ch) {
        case '?':
        case '/':  // find or delete

            cin.getline(buf, maxlen);

            i = D.find(buf);

            if (i == D.end()) cout << "Not found.\n";
            else              // Key found
                if (ch == '?')    // 'Find' command
                    cout << "Number: " << (*i).second << endl;
                else {
                    delete[] (*i).first;
                    D.erase(i);
                }
            break;

        case '!':             // insert (or update)
            cin >> nr;

            if (cin.fail()) {
                cout << "Usage: !number name\n";
                cin.clear();
                cin.getline(buf, maxlen);
                break;
            }

            cin.get(); // skip space

            cin.getline(buf, maxlen);

            i = D.find(buf);

            if (i == D.end()) {
                p = new char[strlen(buf) + 1];
                strcpy(p, buf);
                D[p] = nr;
            }  else (*i).second = nr;
            break;

        case '*':
            for (i = D.begin(); i != D.end(); i++)
                cout << setw(9) << (*i).second << " "
                     << (*i).first << endl;
            break;

        case '=':
            ofstr.open("phone.txt");
            if (ofstr) {
                for (i = D.begin(); i != D.end(); i++)
                    ofstr << setw(9) << (*i).second << " "
                          << (*i).first << endl;
                ofstr.close();
            }  else cout << "Cannot open output file.\n";
            break;

        case '#':
            break;

        default:
            cout << " Use: * (list), ? (find), = (save), "
                 " / (delete), ! (insert), or # (exit).\n";
            cin.getline(buf, maxlen);
            break;
        }
        if (ch == '#') break;
    }
}

// main function

int main()
{
    directype D;
    ReadInput(D);
    ShowCommands();
    ProcessCommands(D);

    directype::iterator i;
    for (i = D.begin(); i != D.end(); ++i) {
        delete [] (*i).first;
    }

    return 0;
}

// end of code
