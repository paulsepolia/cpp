// counting improved

#include <iostream>
#include <string>
#include <algorithm>
#include <cstring>

using namespace std;

// function definition

bool found(char ch)
{
    return ch == 'a' ||
           ch == 'e' ||
           ch == 'i' ||
           ch == 'o' ||
           ch == 'u';
}

// main function

int main()
{
    char *p = "This demonstrates the Standard Template Library";
    int n = count_if(p, p + strlen(p), found);
    cout << n << " vowels (a, e, i, o, u) found.\n";

    return 0;
}

// end of code
