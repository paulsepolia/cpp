// forming sums

#include <iostream>
#include <numeric>
#include <cstdlib>
#include <iomanip>
#include <cmath>

using namespace std;

int main()
{
    const long SIZE_ARR = 10E5;
    const long TRIALS = 100;
    long i;
    long k;
    double *array;
    array = new double[SIZE_ARR];
    double prod;

    cout << setprecision(20) << fixed;

    for (k = 0; k < TRIALS; k++) {
        cout << endl;
        cout << "  1. Building the array. " << endl;
        for (i = 0; i < SIZE_ARR; i++) {
            array[i] = 1.0L - 1.0L/pow(double(i),4.0L);
        }

        cout << endl;
        cout << "  2. The Product of the elements. " << endl;
        prod = 1.0;
        prod = accumulate(array+2, array+SIZE_ARR, prod, multiplies<double>());

        cout << endl;
        cout << "  3. The product of all elements: " << prod << endl;

        cout << endl;
        cout << "  4. The average value of all elements: " << pow(prod,1.0/SIZE_ARR) << endl;

        cout << endl;
        cout << "  5. -------------> " << k << endl;
    }

    cout << endl;
    return 0;
}

// end of code
