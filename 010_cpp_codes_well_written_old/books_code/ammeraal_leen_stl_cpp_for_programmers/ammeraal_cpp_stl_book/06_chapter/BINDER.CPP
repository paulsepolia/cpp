// binder.cpp: The bind2nd adaptor used to count
//    how many array elements are less than 100.
// From: Ammeraal, L. (1997) STL for C++ Programmers,
//       Chichester: John Wiley.
#include <iostream>
#include <algorithm>
#include <functional>
using namespace std;
 
int main()
{  int a[10] = {800, 3, 4, 600, 5, 6, 800, 71, 100, 2},
       n = 0;
#if (defined(__BORLANDC__) &&  __BORLANDC__ <= 0x530)
   count_if(a, a + 10, bind2nd(less<int>(), 100), n);
#else
   n = count_if(a, a + 10, bind2nd(less<int>(), 100));
#endif
   cout << n << endl; // Output: 6
   return 0;
}
