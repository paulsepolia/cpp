// funobj3.cpp: The operator() function is a
//              binary predicate.
// From: Ammeraal, L. (1997) STL for C++ Programmers,
//       Chichester: John Wiley.
#include <iostream.h>

template <class T>
struct LessThan {
   bool operator()(const T &x, const T &y)const
   {  return x < y;
   }
};

struct CompareLastDigits {
   bool operator()(int x, int y)const
   {  return x % 10 < y % 10;
   }
};

template <class T, class Compare>
class PairSelect {
public:
   PairSelect(const T &x, const T &y): a(x), b(y){}
   void PrintSmaller()const
   {  cout << (Compare()(a, b) ? a : b) << endl;
   }
private:
   T a, b;
};

int main()
{  PairSelect<double, LessThan<double> > P(123.4, 98.7);
   P.PrintSmaller(); // Output: 98.7
   
   PairSelect<int, CompareLastDigits> Q(123, 98);
   Q.PrintSmaller(); // Output: 123
   return 0;
}
