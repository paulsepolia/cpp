// compute1.cpp: Replacing a[i] with
//               1.0/(a[i] * a[i] + 1).
// From: Ammeraal, L. (1997) STL for C++ Programmers,
//       Chichester: John Wiley.
#include <iostream>
#include <algorithm>
#include <functional>
using namespace std;

struct compute1: unary_function<int, double> {
    double operator()(int x)const
    {
        return 1.0/(x * x + 1);
    }
};

int main()
{
    // The book contains an obvious error.
    // The text says that the result is placed back in array a,
    // but the program actually places the result in an array b,
    // as pointed out by Richard J. Littlefield (25 May 1999).
    // The following call to transform does as the book
    // says, but it was necessary to declare a as a double
    // array instead of as an int array to produce the
    // output shown in the comment below:
    double a[5] = {2, 0, 1, 3, 7};
    transform(a, a + 5, a, compute1());
    for (int i=0; i<5; i++) cout << a[i] << " ";
    // Output: 0.2 1 0.5 0.1 0.02
    cout << endl;
    return 0;
}
