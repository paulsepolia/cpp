// funobj2.cpp: Function classes used as
//              template arguments.
// From: Ammeraal, L. (1997) STL for C++ Programmers,
//       Chichester: John Wiley.
#include <iostream.h>

struct square {
   int operator()(int x)const {return x * x;}
};

struct cube {
   int operator()(int x)const {return x * x * x;}
};

template <class T>
class cont {
public:
   cont(int i): j(i){}
   void print()const {cout << T()(j) << endl;}
private:
   int j;
};

int main()
{  cont<square> numsq(10);
   numsq.print();  // Output: 100
   cont<cube> numcub(10);
   numcub.print(); // Output: 1000
   return 0;
}
