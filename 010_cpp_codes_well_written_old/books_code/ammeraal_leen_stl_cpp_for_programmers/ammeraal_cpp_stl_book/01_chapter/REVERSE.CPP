// reverse.cpp: Replacing a string with its reverse.
// From: Ammeraal, L. (1997) STL for C++ Programmers,
//       Chichester: John Wiley.
#include <iostream>
#include <string>
#include <algorithm>
using namespace std;

int main()
{  char str[] = "abcklmxyz";
   reverse(str, str+strlen(str));
   cout << str << endl;  // Output: zyxmlkcba
   return 0;
}
