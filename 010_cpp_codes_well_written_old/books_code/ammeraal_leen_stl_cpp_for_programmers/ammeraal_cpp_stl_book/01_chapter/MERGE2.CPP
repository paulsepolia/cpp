// merge2.cpp: Merging records, with names as keys.
// From: Ammeraal, L. (1997) STL for C++ Programmers,
//       Chichester: John Wiley.

#include <iostream>
#include <string>
#include <algorithm>
using namespace std;

struct entry {
   long nr;
   char name[30];
   bool operator<(const entry &b)const 
   {  return strcmp(name, b.name) < 0;
   }
};

int main()
{  entry a[3] = {{10, "Betty"}, 
                 {11, "James"}, 
                 {80, "Jim"}},
         b[2] = {{16, "Fred"}, 
                 {20, "William"}}, 
         c[5], *p;
   merge(a, a+3, b, b+2, c);
   for (p=c; p != c+5; p++)
      cout << p->nr << " " << p->name << endl;
   cout << endl;
   return 0;
}
