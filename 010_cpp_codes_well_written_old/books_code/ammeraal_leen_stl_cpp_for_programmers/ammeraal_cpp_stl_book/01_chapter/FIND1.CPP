// find1.cpp: Finding a given value in a vector.
// From: Ammeraal, L. (1997) STL for C++ Programmers,
//       Chichester: John Wiley.
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int main()
{  vector<int> v;
   int x;
   cout << "Enter positive integers, followed by 0:\n";
   while (cin >> x, x != 0)
      v.push_back(x);
   cout << "Value to be searched for: ";
   cin >> x;
   vector<int>::iterator i = 
      find(v.begin(), v.end(), x);
   if (i == v.end()) 
      cout << "Not found\n";
   else
   {  cout << "Found"; 
      if (i == v.begin()) 
         cout << " as the first element";
      else cout << " after " << *--i;
   }
   cout << endl;
   return 0;
}
