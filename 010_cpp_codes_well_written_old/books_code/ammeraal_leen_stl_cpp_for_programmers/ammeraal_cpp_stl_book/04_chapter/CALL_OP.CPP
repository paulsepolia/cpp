// call_op.cpp: Two call-operators.
// From: Ammeraal, L. (1997) STL for C++ Programmers,
//       Chichester: John Wiley.

#include <iostream.h>

class U {
public:
   char operator()()
   {  return 'Q';
   }
   int operator()(int a, int b, int c)
   {  return a + b + c;
   }
};

int main()
{  U u;
   cout << "u() = " << u() << endl;
   cout << "u(1, 2, 3) = " << u(1, 2, 3) << endl;
   return 0;
}
