// lexcomp.cpp: Lexicographical comparison.
// From: Ammeraal, L. (1997) STL for C++ Programmers,
//       Chichester: John Wiley.

#include <iostream>
#include <algorithm>
#include <functional>
using namespace std;

int main()
{  int a[4] = {1, 3, 8, 2},
       b[4] = {1, 3, 9, 1};
   
   cout << "a: "; 
   copy(a, a+4, 
#if defined(__BORLANDC__) && __BORLANDC__ == 0x530   
      ostream_iterator<int, char, char_traits<char> >(cout, " "));
#else
      ostream_iterator<int>(cout, " "));
#endif
   cout << "\nb: "; 
   copy(b, b+4, 
#if defined(__BORLANDC__) && __BORLANDC__ == 0x530   
      ostream_iterator<int, char, char_traits<char> >(cout, " "));
#else
      ostream_iterator<int>(cout, " "));
#endif
   cout << endl;
   
   if (lexicographical_compare(a, a+4, b, b+4)) 
      cout << "Lexicographically, a precedes b.\n";
   
   if (lexicographical_compare(b, b+4, a, a+4,
      greater<int>()))
      cout << 
      "Using the greater-than relation, we find:\n"
      "b lexicographically precedes a.\n";
   return 0;
}
