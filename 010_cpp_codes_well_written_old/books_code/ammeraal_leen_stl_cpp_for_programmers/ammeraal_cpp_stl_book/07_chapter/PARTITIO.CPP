// partitio.cpp (= partition.cpp): The partition algorithm.
// From: Ammeraal, L. (1997) STL for C++ Programmers,
//       Chichester: John Wiley.
#include <iostream>
#include <algorithm>
using namespace std;

bool below50(int x){return x < 50;}
   
int main()
{  int a[8] = {70, 40, 80, 20, 50, 60, 50, 10};
   int *p = partition(a, a+8, below50);
   copy(a, p, 
#if defined(__BORLANDC__) && __BORLANDC__ == 0x530   
      ostream_iterator<int, char, char_traits<char> >(cout, " "));
#else
      ostream_iterator<int>(cout, " "));
#endif
   cout << "  ";
   copy(p, a+8, 
#if defined(__BORLANDC__) && __BORLANDC__ == 0x530   
      ostream_iterator<int, char, char_traits<char> >(cout, " "));
#else
      ostream_iterator<int>(cout, " "));
#endif
   cout << endl;
   return 0;
}
