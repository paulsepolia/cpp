// copy1.cpp: Shifting array elements to the left.
// From: Ammeraal, L. (1997) STL for C++ Programmers,
//       Chichester: John Wiley.
#include <iostream>
#include <algorithm>
using namespace std;

int main()
{  int a[4] = {10, 20, 30, 40}, i;
   cout << "Before shifting left: ";
   for (i=0; i<4; i++) cout << a[i] << " " ;
   copy(a+1, a+4, a);
   cout << "\nAfter shifting left:  ";
   for (i=0; i<4; i++) cout << a[i] << " " ;
   cout << endl;
   return 0;
}
