// partsum.cpp: Partial sum.
// From: Ammeraal, L. (1997) STL for C++ Programmers,
//       Chichester: John Wiley.
#include <iostream>
#include <numeric>
#include <algorithm>
using namespace std;

int main()
{  int a[4] = {2, 3, 4, 8}, b[4], *iEnd;
   iEnd = partial_sum(a, a+4, b);
   copy(b, iEnd, 
#if defined(__BORLANDC__) && __BORLANDC__ == 0x530   
      ostream_iterator<int, char, char_traits<char> >(cout, " "));
#else
      ostream_iterator<int>(cout, " "));
#endif
   cout << endl; // Output 2 5 9 17
   return 0;     
}
