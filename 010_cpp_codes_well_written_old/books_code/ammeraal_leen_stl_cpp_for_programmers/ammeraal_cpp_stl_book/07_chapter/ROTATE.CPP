// rotate.cpp: Rotations.
// From: Ammeraal, L. (1997) STL for C++ Programmers,
//       Chichester: John Wiley.
#include <iostream>
#include <algorithm>
using namespace std;

int main()
{  int a[4] = {10, 20, 30, 40};
   cout << "Initial contents of array a: ";

#if defined(__BORLANDC__) && __BORLANDC__ == 0x530   
      copy(a, a+4, ostream_iterator<int, char, 
         char_traits<char> >(cout, " "));
#else
      copy(a, a+4, ostream_iterator<int>(cout, " "));
#endif

   rotate(a, a+1, a+4);
   cout << "\nAfter rotate(a, a+1, a+4):   ";

#if defined(__BORLANDC__) && __BORLANDC__ == 0x530   
      copy(a, a+4, ostream_iterator<int, char, 
         char_traits<char> >(cout, " "));
#else
      copy(a, a+4, ostream_iterator<int>(cout, " "));
#endif

   cout << endl;
   return 0;
}
