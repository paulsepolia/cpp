// minmax.cpp: The min and max algorithms.
// From: Ammeraal, L. (1997) STL for C++ Programmers,
//       Chichester: John Wiley.

#include <iostream>
#include <algorithm>
#include <functional>
using namespace std;

#if (defined(_MSC_VER) && !defined(_SGI_MSVC))
// VC++5.0 does not support min and max in the standard way,
// so we define them ourselves:
template <class T>
inline const T& min(const T& a, const T& b)
{  return a < b ? a : b;
}

template <class T>
inline const T& max(const T& a, const T& b)
{  return a < b ? b : a;
}

template <class T, class Compare>
inline const T& min(const T& a, const T& b, Compare comp)
{  return comp(b, a) ? b : a;
}

template <class T, class Compare>
inline const T& max(const T& a, const T& b, Compare comp)
{  return comp(a, b) ? b : a;
}
#endif

bool CompareLastDigit(int x, int y)
{  return x % 10 < y % 10;
}

int main()
{  int x = 123, y = 75, minimum, MaxLastDigit;
   minimum = min(x, y);
   MaxLastDigit = max(x, y, CompareLastDigit);
   cout << minimum << " " << MaxLastDigit << endl;
   return 0;   // Output: 75 75
}
