// gen_n.cpp: The generate_n algorithm.
// From: Ammeraal, L. (1997) STL for C++ Programmers,
//       Chichester: John Wiley.
#include <iostream>
#include <algorithm>
#include <list>
using namespace std;

int fun()
{  static int i=8;
   return i += 2;
}

int main()
{  list<int> a(5);
   generate_n(a.begin(), 5, fun);
   copy(a.begin(), a.end(), 
#if defined(__BORLANDC__) && __BORLANDC__ == 0x530   
      ostream_iterator<int, char, char_traits<char> >(cout, " "));
#else
      ostream_iterator<int>(cout, " "));
#endif
   cout << endl;  // Output: 10 12 14 16 18
   return 0;
}
