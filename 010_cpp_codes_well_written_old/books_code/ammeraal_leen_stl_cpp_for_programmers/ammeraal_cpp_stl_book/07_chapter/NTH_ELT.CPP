// nth_elt.cpp: The nth_element algorithm.
// From: Ammeraal, L. (1997) STL for C++ Programmers,
//       Chichester: John Wiley.
#include <iostream>
#include <algorithm>
using namespace std;

int main()
{  int a[10] = {4, 12, 9, 5, 6, 6, 6, 4, 8, 10};
   nth_element(a, a+2, a+10);
   copy(a, a+10, 
#if defined(__BORLANDC__) && __BORLANDC__ == 0x530   
      ostream_iterator<int, char, char_traits<char> >(cout, " "));
#else
      ostream_iterator<int>(cout, " "));
#endif
   cout << endl;
   return 0;
}
