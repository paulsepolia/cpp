// for_each.cpp: The for_each algorithm.
// From: Ammeraal, L. (1997) STL for C++ Programmers,
//       Chichester: John Wiley.

#include <iostream>
#include <algorithm>

using namespace std;

void display(int x)
{  static int i=0;
   cout << "a[" << i++ << "] = " << x << endl;
}

int main()
{  const int N = 4;
   int a[N] = 
      {1234, 5432, 8943, 3346};
   for_each(a, a+N, display);
   return 0;
}
