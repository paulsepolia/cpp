// count_e.cpp: Counting how often 'e' occurs.
// From: Ammeraal, L. (1997) STL for C++ Programmers,
//       Chichester: John Wiley.
#include <iostream>
#include <string>
#include <algorithm>
using namespace std;

int main()
{  char *p = 
      "This demonstrates the Standard Template Library";
// 'count' now has three parameters. It still has four with BC5.3:
#if (defined(__BORLANDC__) &&  __BORLANDC__ <= 0x530)
   int n = 0;
   count(p, p + strlen(p), 'e', n);
#else
   int n = count(p, p + strlen(p), 'e');
#endif
   cout << n << " occurrences of 'e' found.\n";
   return 0;
} 
