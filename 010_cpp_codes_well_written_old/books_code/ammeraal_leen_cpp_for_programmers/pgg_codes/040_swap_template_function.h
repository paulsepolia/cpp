//==================================//
// Header file.                     //
// 'swapObjects' template function. //
//==================================//

template <class T>
void swapObjects(T &x, T &y)
{
    T w = x;

    x = y;
    y = w;
}

//==============//
// End of code. //
//==============//
