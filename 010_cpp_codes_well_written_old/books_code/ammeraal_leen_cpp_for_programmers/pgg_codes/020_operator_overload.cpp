//====================//
// Operator overload. //
//====================//

#include <iostream>

using std::endl;
using std::cout;
using std::ostream;

// 1.

class vec {
public:

    // a.

    vec(double xx = 0, double yy = 0)
        : x(xx), y(yy)
    {}

    // b.

    void printvec(ostream &os) const
    {
        os << "(" << x << ", " << y << ")";
    }

    // c.

    vec operator+(const vec &b) const
    {
        return vec(x + b.x, y + b.y);
    }

private:
    double x;
    double y;
};

// 2.

ostream &operator<<(ostream &os, const vec &v)
{
    v.printvec(os);

    return os;
}

// 3. The main function
int main()
{
    vec u(3, 1);
    vec v(1, 2);
    vec s;

    s = u + v; // this is the overloaded + operator

    cout << " The sum of (3, 1) and (1, 2) is "
         << s  // this is the overloaded << operator
         << endl;

    return 0;
}

//==============//
// End of code. //
//==============//
