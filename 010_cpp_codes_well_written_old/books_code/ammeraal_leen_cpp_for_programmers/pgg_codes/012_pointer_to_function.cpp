//============================================//
// Pointer to a function                      //
// OR EQUIVALENT                              //
// Function with a function as its arguments. //
//============================================//

#include <iostream>

using std::cout;
using std::endl;

// 1. Function declaration and definition.
//    Name: funsum

double funsum(long n, double (*f)(long k))
{
    double s;
    long i;

    s = 0;

    for (i = 1; i <= n; i++) {
        s += (*f)(i);
    }

    return s;
}

// 2. Function declaration and definition.
//    Name: recipriocal

double reciprocal(long k)
{
    const double ONE = 1.0L;
    return (ONE / k);
}

// 3. Function declaration and definition.
//    Name: square

double square(long k)
{
    return (static_cast<double>(k)*k);
}

// 4. Main function.
//
int main()
{
    cout << " 1. The address of the funsum function     is " << &funsum << endl;
    cout << " 2. The address of the reciprocal function is " << &reciprocal << endl;
    cout << " 3. The address of the square function     is " << &square << endl;

    cout << "Sum of five reciprocals: "
         << funsum(5, &reciprocal) << endl;

    cout << "Sum of three squares: "
         << funsum(3, &square) << endl;

    cout << " 4. The address of the funsum function     is " << &funsum << endl;
    cout << " 5. The address of the reciprocal function is " << &reciprocal << endl;
    cout << " 6. The address of the square function     is " << &square << endl;

    return 0;
}

//==============//
// End of code. //
//==============//
