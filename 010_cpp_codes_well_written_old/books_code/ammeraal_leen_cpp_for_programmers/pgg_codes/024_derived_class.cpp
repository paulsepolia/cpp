//========================//
// Derived class example. //
//========================//

#include <iostream>

using std::endl;
using std::cout;

// 1. Shape class.

class Shape {
public:

    // a. Constructor definition.

    Shape(double x = 0, double y = 0)
        : xC(x), yC(y)
    {}

    // b. Member function definition.

    void printcenter() const
    {
        cout << xC << " " << yC << endl;
    }

protected:
    double xC;
    double yC;
};

// 2.

const double pi = 3.1415926535897932;

// 3. Circle derived class.

class Circle: public Shape {
public:

    // a. Constructor definition.

    Circle(double xC, double yC, double r)
        : Shape(xC, yC)
    {
        radius = r;
    }

    // b. Member function definition.

    double area() const
    {
        return (pi * radius * radius);
    }

private:
    double radius;
};

// 4. Square derived class.

class Square: public Shape {
public:

    // a.

    Square(double xC, double yC, double xP, double yP)
        : Shape(xC, yC)
    {
        this -> xP = xP;
        this -> xP = xP;
    }

    // b.

    double area() const
    {
        double a = xP - xC;
        double b = yP - yC;

        return (2 * (a * a + b * b));
    }

private:
    double xP;
    double yP;
};

// 4. The main function.

int main()
{
    double xCircle = 2;
    double yCircle = 2.5;
    double radius  = 2;

    Circle circle(xCircle, yCircle, radius);

    cout << " Center of circle is ";

    circle.printcenter();

    cout << endl;

    cout << " Radius is " << radius << endl;

    cout << " Area of circle is " << circle.area() << endl << endl;

    double xCsquare = 3;
    double yCsquare = 3.5;
    double xP = 4.37;
    double yP = 3.85;

    Square square(xCsquare, yCsquare, xP, yP);

    cout << " Center of square is ";

    square.printcenter();

    cout << " xP = "  << xP << " yP = " << yP << endl;

    cout << " Area of square is " << square.area() << endl;

    return 0;
}

//==============//
// End of code. //
//==============//
