//==============================================//
// A (not completely useless) temporary object. //
//==============================================//

#include <iostream>

using std::endl;
using std::cout;

// 1. 'Example' class.

class Example {
public:
    Example(int i = 0)
    {
        cout << " Object created with i = " << i << endl;
    }

    void print()
    {
        cout << " Hello! " << endl;
    }
};

// 2. The main function.

int main()
{
    Example(1).print(); // Output: Hello

    Example(2);

    Example();

    return 0;
}

//==============//
// End of code. //
//==============//
