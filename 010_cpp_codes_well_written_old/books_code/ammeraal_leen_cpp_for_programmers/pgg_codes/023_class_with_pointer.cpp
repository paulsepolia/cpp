//======================//
// Class with pointers. //
//======================//

#include <iostream>
#include <string>

using std::cout;
using std::string;
using std::endl;

// 1. Class declaration

class row {
public:
    row(int n = 3);                          // Default constructor
    row(const row &r);                       // Copy constructor
    ~row();                                  // Destructor
    row &operator=(const row &r);            // Copy assignment operator
    void printrow(const string &str) const;

private:
    int *ptr;
    int len;
};

// 2. Defualt constructor

row::row(int n)
{
    len = n;
    ptr = new int[n];

    for (int i = 0; i < n; i++) {
        ptr[i] = 10 * i;
    }
}

// 3. Copy constructor

row::row(const row &r)
{
    len = r.len;
    ptr = new int[len];

    for (int i = 0; i < len; i++) {
        ptr[i] = r.ptr[i];
    }
}

// 4. Destructor

row::~row()
{
    delete[] ptr;
}

// 5. Copy assignment operator

row &row::operator=(const row &r)
{
    if (&r != this) {
        delete [] ptr;
        len = r.len;
        ptr = new int[len];

        for (int i = 0; i < len; i++) {
            ptr[i]  = r.ptr[i];
        }
    }

    return *this;
}

// 6. printrow member function

void row::printrow(const string &str) const
{
    cout << str;

    for (int i = 0; i < len; i++) {
        cout << ptr[i] << ' ';
    }

    cout << endl;
}

// 7. The main function

int main()
{
    row r;
    row s(5);
    row t(s); // Default copy constructor

    r = s; // Using the assignment operator

    r.printrow("r: ");
    s.printrow("s: ");
    t.printrow("t: ");

    return 0;
}

//==============//
// End of code. //
//==============//
