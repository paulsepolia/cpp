//====================//
// Namespace example. //
//====================//

#include <iostream>
using std::cout;
using std::endl;

namespace A {
int i = 10;
};

int i = 20;

int main()
{

    cout << " i in global namespace is " << i << endl;

    cout << " i in using namespace A is " << A::i << endl;

    return 0;
}

//==============//
// End of code. //
//==============//
