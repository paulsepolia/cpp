//================//
// Using a queue. //
//================//

#include <iostream>
#include <list>
#include <queue>

using std::cout;
using std::endl;
using std::queue;
using std::list;

int main()
{
    queue <long, list<long> > Q;

    Q.push(10);
    Q.push(20);
    Q.push(30);

    cout << "After pushing 10, 20, and 30" << endl;

    cout << "Q.front() = " << Q.front() << endl;
    cout << "Q.back()  = " << Q.back()  << endl;

    Q.pop();

    cout << "After Q.pop():" << endl;

    cout << "Q.front() = " << Q.front() << endl;

    return 0;
}

//==============//
// End of code. //
//==============//

