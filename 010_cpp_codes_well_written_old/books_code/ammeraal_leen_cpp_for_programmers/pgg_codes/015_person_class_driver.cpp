//=================================//
// Driver program to class Person. //
//=================================//

#include <string>
#include <iostream>
#include "015_person_class_header.h"

using std::cout;
using std::endl;

int main()
{
    Person a("Mary", 1980, false);
    Person b;

    string s;

    cout << " 1 --> " << endl;

    s = a.getName();

    cout << s << endl;

    cout << " 2 --> " << endl;

    b.print();

    cout << " 3 --> " << endl;

    a.print();

    b.setName("John");
    b.setYear(1975);
    b.setMF(true);

    cout << " 4 --> " << endl;

    b.print();

    return 0;
}

//==============//
// End of code. //
//==============//
