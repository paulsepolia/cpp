//=========================================//
// Using a stack to read a number sequence //
// of arbitrary length and to display this //
// sequence in the reverse order.          //
//=========================================//

#include <iostream>
#include <vector>
#include <stack>

using std::cout;
using std::cin;
using std::endl;
using std::vector;
using std::stack;

int main()
{
    stack<long, vector<long> > S;

    long x;

    cout << "Enter some integers, followed by a letter:" << endl;

    while(cin >> x) S.push(x);

    while(!S.empty()) {
        x = S.top();

        cout << "Size: " << S.size()
             << " Element at the top: " << x << endl;

        S.pop();
    }

    return 0;
}

//==============//
// End of code. //
//==============//
