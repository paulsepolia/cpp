//===============================================//
// Demonstration of the fact that                //
// same classes the one having virtual functions //
// results in larger RAM.                        //
//===============================================//

#include <iostream>

using std::cout;
using std::endl;

// 1.

class late {
public:
    int i;
    virtual void f()
    {
        i = 1;
        cout << i << endl;
    }
};

// 2.

class early {
public:
    int i;
    void f()
    {
        i = 1;
        cout << i << endl;
    }
};

// 3. Main function.

int main()
{
    cout << " 1 --> The size of 'late'  class is " << sizeof(late)  << " bytes." << endl;
    cout << " 2 --> The size of 'early' class is " << sizeof(early) << " bytes." << endl;

    late laOb1;
    late laOb2;

    early eaOb1;
    early eaOb2;

    cout << " 3 --> The size a 'late'  object is " << sizeof(laOb1) << " bytes." << endl;
    cout << " 4 --> The size a 'early' object is " << sizeof(eaOb1) << " bytes." << endl;

    laOb1.f();
    eaOb1.f();

    cout << " 5 --> laOb1.f() = " << laOb1.i << endl;
    cout << " 6 --> eaOb1.f() = " << eaOb1.i << endl;

    return 0;
}

//==============//
// End of code. //
//==============//
