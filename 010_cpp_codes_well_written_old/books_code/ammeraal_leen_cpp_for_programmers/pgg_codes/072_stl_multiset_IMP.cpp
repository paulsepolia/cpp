//============//
// multiset   //
// insert, == //
//============//

#include <iostream>
#include <set>

using std::endl;
using std::cout;
using std::multiset;
using std::less;

// 1. type definitions

typedef multiset<long, less<long> >      musL;
typedef multiset<double, less<double> >  musD;

// 2. the main function

int main()
{
    // 1. Local constants.

    const long NUM_DO_A = 1E4;
    const long NUM_DO_B = 1E1;
    const long DIM_MUS  = 1E7;

    // 2. Local variables.

    long k;
    long i;

    // 3. The main code.

    for (k = 1; k <= NUM_DO_A; k++) {
        cout << "----------------------------------------------->>> " << k << endl;

        musL *ps1 = new musL;
        musL *ps2 = new musL;

        cout << "  1 --> Building the *ps1." << endl;

        for (i = 0; i < DIM_MUS; i++) {
            ps1 -> insert(DIM_MUS-i);
            ps1 -> insert(DIM_MUS-i);
        }

        cout << "  2 --> Building the *ps2." << endl;

        for (i = 0; i < DIM_MUS; i++) {
            ps2 -> insert(DIM_MUS-i);
            ps2 -> insert(DIM_MUS-i);
        }

        cout << "  3 --> The size of *ps1 is:" << endl;

        cout << (*ps1).size() << endl;

        cout << "  4 --> The size of *ps2 is:" << endl;

        cout << (*ps2).size() << endl;

        cout << "  5 --> Test for equality of *ps1 and *ps2." << endl;

        for(i = 1; i <= NUM_DO_B; i++) {
            if (*ps1 == *ps2) {
                cout << " Equal --> " << i << endl;
            } else {
                cout << " Not equal --> " << i << endl;
            }
        }

        cout << "  6 --> Erasing *ps1 element by element. " << endl;

        for (i = 0; i < DIM_MUS; i++) {
            ps1 -> erase(DIM_MUS-i);
        }

        cout << "  7 --> The size of *ps1 is:" << endl;

        cout << (*ps1).size() << endl;

        cout << "  8 --> Erasing *ps2 element by element. " << endl;

        for (i = 0; i < DIM_MUS; i++) {
            ps2 -> erase(DIM_MUS-i);
        }

        cout << "  9 --> The size of *ps2 is:" << endl;

        cout << (*ps2).size() << endl;

        cout << " 10 --> Building the *ps1 again." << endl;

        for (i = 0; i < DIM_MUS; i++) {
            ps1 -> insert(DIM_MUS-i);
            ps1 -> insert(DIM_MUS-i);
        }

        cout << " 11 --> Building the *ps2 again." << endl;

        for (i = 0; i < DIM_MUS; i++) {
            ps2 -> insert(DIM_MUS-i);
            ps2 -> insert(DIM_MUS-i);
        }

        cout << " 12 --> Free up the RAM. " << endl;

        delete ps1;
        delete ps2;

        // about doubles now.

        musD *ps3 = new musD;
        musD *ps4 = new musD;

        cout << " 13 --> Building the *ps3." << endl;

        for (i = 0; i < DIM_MUS; i++) {
            ps3 -> insert(DIM_MUS-double(i));
            ps3 -> insert(DIM_MUS-double(i));
        }

        cout << " 14 --> Building the *ps4." << endl;

        for (i = 0; i < DIM_MUS; i++) {
            ps4 -> insert(DIM_MUS-double(i));
            ps4 -> insert(DIM_MUS-double(i));
        }

        cout << " 15 --> The size of *ps3 is:" << endl;

        cout << (*ps3).size() << endl;

        cout << " 16 --> The size of *ps4 is:" << endl;

        cout << (*ps4).size() << endl;

        cout << " 17 --> Test for equality of *ps3 and *ps4." << endl;

        for(i = 1; i <= NUM_DO_B; i++) {
            if (*ps3 == *ps4) {
                cout << " Equal --> " << i << endl;
            } else {
                cout << " Not equal --> " << i << endl;
            }
        }

        cout << " 18 --> Erasing *ps3 element by element. " << endl;

        for (i = 0; i < DIM_MUS; i++) {
            ps3 -> erase(DIM_MUS-double(i));
        }

        cout << " 19 --> The size of *ps3 is:" << endl;

        cout << (*ps3).size() << endl;

        cout << " 20 --> Erasing *ps4 element by element. " << endl;

        for (i = 0; i < DIM_MUS; i++) {
            ps4 -> erase(DIM_MUS-double(i));
        }

        cout << " 21 --> The size of *ps4 is:" << endl;

        cout << (*ps4).size() << endl;

        cout << " 22 --> Building the *ps3 again." << endl;

        for (i = 0; i < DIM_MUS; i++) {
            ps3 -> insert(DIM_MUS-double(i));
            ps3 -> insert(DIM_MUS-double(i));
        }

        cout << " 23 --> Building the *ps4 again." << endl;

        for (i = 0; i < DIM_MUS; i++) {
            ps4 -> insert(DIM_MUS-double(i));
            ps4 -> insert(DIM_MUS-double(i));
        }

        cout << " 24 --> Free up the RAM. " << endl;

        delete ps3;
        delete ps4;
    }

    return 0;
}