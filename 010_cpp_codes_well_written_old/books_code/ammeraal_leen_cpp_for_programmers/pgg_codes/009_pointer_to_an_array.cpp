//======================//
// Pointer to an array. //
//======================//

#include <iostream>

using std::cout;
using std::endl;

int main()
{
    long a[5];
    long (*p1)[5];
    long *p2;
    int  *p3;

    long i;
    const long NUM_DO = 2 * 1E3;

    p1 = &a;
    p2 = a;

    (*p1)[3] = 123;

    cout << (*p1)[3] << endl;

    for (long i = 0; i <= NUM_DO; i++) {
        cout << i << " -- " << *(p2+i) << endl;
        cout << i << " -- " << *(p3+i) << endl;
    }

    return 0;
}

//==============//
// End of code. //
//==============//
