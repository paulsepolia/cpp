//===================================================//
// Finding the smallest element of an integer array. //
//===================================================//

#include <iostream>

using std::cout;
using std::endl;
using std::cin;

// 1. function declaration and definition.

int minimum(const int *a, int n)
{
    int small = *a;

    for (int i = 1; i < n; ++i) {
        if (*(a+i) < small) small = *(a+i);
    }

    return small;
}

// 2. The main function.

int main()
{
    int table[10];

    cout << " Enter 10 integers: " << endl;

    for (int i = 0; i < 10; i++) {
        cin >> table[i];
    }

    cout << "\nThe minimum of these values is "
         << minimum(&table[0], 10) << endl;
    // or minimum( table[0], 10) << endl; // it is the same

    return 0;
}

//==============//
// End of code. //
//==============//


