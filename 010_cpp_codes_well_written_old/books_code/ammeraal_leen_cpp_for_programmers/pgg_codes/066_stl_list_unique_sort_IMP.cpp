//===========================//
// The list member functions //
// 'unique' and 'sort'.      //
//===========================//

#include <iostream>
#include <list>
#include <iterator>

using std::endl;
using std::cout;
using std::list;
using std::ostream_iterator;

// 1. Function definition.

void out(const char *s, const list<long> &L)
{
    cout << s;
    copy(L.begin(), L.end(), ostream_iterator<long>(cout, " "));
    cout << endl;
}

int main()
{
    const long LI_DIM = 1E8;
    long i;
    long k;

    for (k = 0; k < 1000; k++) {

        cout << " ------------------------------------------------>>> " << k << endl;

        cout << " 1 --> Building the list." << endl;

        list<long> *pL = new list<long>;

        for (i = 0; i < LI_DIM; i++) {
            pL -> push_back(123);
        }

        (*pL).push_back(101);
        (*pL).push_back(102);
        (*pL).push_back(103);

        // Commented out for large outputs.
        //out("Initial contents: ", *pL);

        cout << " 2 --> Sorting the list." << endl;

        (*pL).sort();

        cout << " 3 --> Eliminating the next-to identical elements." << endl;

        (*pL).unique();

        cout << " 4 --> Some output." << endl;

        out("After (*pL).unique(): ", *pL);

        cout << " 5 --> Sorting the list." << endl;

        (*pL).sort();

        cout << " 6 --> Some output." << endl;

        out("After (*pL).sort():   ", *pL);

        cout << " 7 --> Building the list again." << endl;

        for (i = 0; i < LI_DIM; i++) {
            pL -> push_back(124);
        }

        cout << " 8 --> Deletion of the list." << endl;

        delete pL;
    }

    return 0;
}

//==============//
// End of code. //
//==============//