//==========================================//
// Hexadecimal and decimal input an output. //
//==========================================//

#include <iostream>

using std::cout;
using std::cin;
using std::endl;
using std::hex;
using std::dec;

int main()
{
    cout << " Enter a hexadecimal integer x and a decimal integer y: " << endl;

    int x;
    int y;

    cin >> hex >> x;
    cin >> dec >> y;

    cout << "0x" << hex << x << " = " << dec << x << endl;

    int s = x + y;

    cout << " Their sum (hexadecimal): " << hex << s << endl;
    cout << " Their sum (decimal):     " << dec << s << endl;

    return 0;
}

//==============//
// End of code. //
//==============//
