//=============================//
// Virtual function in action. //
//=============================//

#include <iostream>

using std::cout;
using std::endl;

// 1. animal class declaration and definition.

class animal {
public:
    virtual void print() const // virtual print function
    {
        cout << " Class 'animal' --> Unknown animal type." << endl;
    }

    virtual ~animal() {} // virtual destructor

protected:
    int nlegs;
};

// 2. fish class declaration and definition.

class fish: public animal {
public:
    fish(int n)
    {
        nlegs = n;
    }

    virtual void print() const // virtual keyword is not needed
    {
        cout << " A fish has " << nlegs << " legs." << endl;
    }
};

// 3. bird class declaration and definition.

class bird: public animal {
public:
    bird(int n)
    {
        nlegs = n;
    }

    virtual void print() const // virtual keyword is not needed
    {
        cout << " A bird has " << nlegs << " legs." << endl;
    }
};

// 4. mammal class declaration and definition.

class mammal: public animal {
public:
    mammal(int n)
    {
        nlegs = n;
    }

    virtual void print() const // virtual keyword is not needed
    {
        cout << " A mammal has " << nlegs << " legs." << endl;
    }
};

// 5. The main function.

int main()
{
    animal *p[4];
    int i;

    p[0] = new fish(0);
    p[1] = new bird(2);
    p[2] = new mammal(4);
    p[3] = new animal;

    for (i = 0; i < 4; i++) {
        p[i] -> print();
    }

    for (i = 0; i < 4; i++) {
        delete p[i];
    }

    return 0;
}

//==============//
// End of code. //
//==============//
