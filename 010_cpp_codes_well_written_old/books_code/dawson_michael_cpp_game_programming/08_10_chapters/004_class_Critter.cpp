// Private Critter
// Demonstrates setting access levels

#include <iostream>

using namespace std;

// class definition

class Critter {
public:                          // begin public section
    Critter(int hunger = 0);       // default constructor declaration
    int GetHunger() const;         // function declaration - constant member function
    void SetHunger( int hunger);   // function declaration

private:                         // begin private section
    int m_Hunger;
};

// constructor definition

Critter::Critter(int hunger):
    m_Hunger(hunger)
{
    cout << "A new critter has been born!" << endl;
}

// function definition

int Critter::GetHunger() const
{
    return m_Hunger;
}

// function definition

void Critter::SetHunger(int hunger)
{
    if (hunger < 0) {
        cout << "You can't set a critter's hunger to a negative number.\n\n";
    } else {
        m_Hunger = hunger;
    }
}

// main function

int main()
{
    Critter crit(5);

    //  cout << crit.m_Hunger; // illegal, m_Hunger is private!

    cout << "Calling GetHunger(): " << crit.GetHunger() << "\n\n";

    cout << "Calling SetHunger() with -1.\n";
    crit.SetHunger(-1);

    cout << "Calling SetHunger() with 9.\n";
    crit.SetHunger(9);
    cout << "Calling GetHunger(): " << crit.GetHunger() << "\n\n";

    return 0;
}

// end of code
