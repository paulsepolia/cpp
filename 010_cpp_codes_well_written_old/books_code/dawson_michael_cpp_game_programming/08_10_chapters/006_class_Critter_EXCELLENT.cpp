// Criter Caretaker
// Simulates caring for a virtual pet

#include <iostream>

using namespace std;

// class definition

class Critter {
public:
    Critter(int hunger = 0, int boredom = 0);  // class constructor declaration
    void Talk();                               // public member function declaration
    void Eat(int food = 4);                    // public member function declaration
    void Play(int fun = 4);                    // public member function declaration

private:
    int m_Hunger;                 // private data member
    int m_Boredom;                // private data member
    int GetMood() const;          // private constant member function
    void PassTime(int time = 1);  // private member function declaration
};

// class constructor definition

Critter::Critter(int hunger, int boredom):
    m_Hunger(hunger),
    m_Boredom(boredom)
{ }

// private constant member function definition

inline int Critter::GetMood() const
{
    return (m_Hunger + m_Boredom);
}

// private member function definition

void Critter::PassTime(int time)
{
    m_Hunger += time;
    m_Boredom += time;
}

// public member function definition

void Critter::Talk()
{
    cout << "I'm a critter and I feel ";

    int mood = GetMood();

    if (mood > 15) {
        cout << "mad." << endl;
    } else if (mood > 10) {
        cout << "frustrated." << endl;
    } else if (mood > 5) {
        cout << "okay." << endl;
    } else {
        cout << "happy." << endl;
    }

    PassTime();
}

// public member function definition

void Critter::Eat(int food)
{
    cout << "Brruppp." << endl;

    m_Hunger -= food;

    if (m_Hunger < 0) {
        m_Hunger = 0;
    }

    PassTime();
}

// public member function definition

void Critter::Play(int fun)
{
    cout << "Wheee!" << endl;

    m_Boredom -= fun;

    if (m_Boredom < 0) {
        m_Boredom = 0;
    }

    PassTime();
}

// main function

int main()
{
    Critter crit;

    int choice = 1;  //start the critter off talking
    while (choice != 0) {
        cout << endl;
        cout << "  Critter Caretaker" << endl << endl;
        cout << "  0 - Quit" << endl;
        cout << "  1 - Listen to your critter" << endl;
        cout << "  2 - Feed your critter" << endl;
        cout << "  3 - Play with your critter" << endl << endl;

        cout << "Choice: ";
        cin >> choice;

        switch (choice) {
        case 0:
            cout << "Good-bye." << endl;
            break;
        case 1:
            crit.Talk();
            break;
        case 2:
            crit.Eat();
            break;
        case 3:
            crit.Play();
            break;
        default:
            cout << "\nSorry, but " << choice << " isn't a valid choice.\n";
        }
    }

    return 0;
}

// end of code
