// Simple Critter
// Demonstrates creating a new type

#include <iostream>

using namespace std;

// class definition

class Critter { // class definition - defines a new type
public:
    int m_Hunger; // data member
    void Greet(); // member function prototype
};

// member function definition

void Critter::Greet()
{
    cout << "Hi. I am a critter. My hunger level is " << m_Hunger << ".\n";
}

// main function

int main()
{
    Critter crit1;
    Critter crit2;

    crit1.m_Hunger = 9;
    crit2.m_Hunger = 3;

    cout << "crit1's hunger level is " << crit1.m_Hunger << ".\n";
    cout << "crit2's hunger level is " << crit2.m_Hunger << ".\n\n";

    crit1.Greet();
    crit2.Greet();

    return 0;
}

// end of code
