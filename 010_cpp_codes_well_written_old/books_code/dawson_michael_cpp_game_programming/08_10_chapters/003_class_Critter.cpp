// Constructor Critter
// Demonstrates constructors

#include <iostream>

using namespace std;

// class definition

class Critter {
public:
    int m_Hunger;
    Critter (int hunger = 0); // constructor prototype
    void Greet();
};

// constructor definition

Critter::Critter(int hunger):
    m_Hunger(hunger)
{
    cout << "A new critter has been born!" << endl;
}

// function definition

void Critter::Greet()
{
    cout << "Hi. I am a critter. My hunger level is " << m_Hunger << ".\n\n";
}

// main function

int main()
{
    Critter crit1(7);
    crit1.Greet();

    Critter crit2;
    crit2.Greet();

    Critter crit3;
    crit3.Greet();

    crit3.m_Hunger = 10;  // modify the m_Hunger public data member
    crit3.Greet();

    return 0;
}

// end of code
