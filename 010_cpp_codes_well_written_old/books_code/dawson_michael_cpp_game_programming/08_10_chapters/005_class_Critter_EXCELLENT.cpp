// Static Critter
// Demonstrates static member variables and functions

#include <iostream>

using namespace std;

// class definition

class Critter {
public:
    static int s_Total; // static member variable declaration
    // total number of Critter objects in existence

    Critter(int hunger = 0); // constructor declaration
    static int GetTotal();   // static member function prototype

private:
    int m_Hunger;
};

// static member variable initialization

int Critter::s_Total = 0;

// constructor definition

Critter::Critter(int hunger):
    m_Hunger(hunger)
{
    cout << "A critter has been born!" << endl;
    ++s_Total;
    cout << "The total number is: " << s_Total << endl;
}

// static member function definition

int Critter::GetTotal()
{
    return s_Total;
}

// main function

int main()
{
    cout << endl;
    cout << "The total number of critters is: ";
    cout << Critter::s_Total << endl;

    cout << endl;
    Critter crit1;
    Critter crit2;
    Critter crit3;

    cout << endl;
    cout << "The total number of critters is: ";
    cout << Critter::GetTotal() << endl;

    cout << endl;
    cout << "The total number of critters is: ";
    cout << crit1.s_Total << endl;
    cout << crit2.s_Total << endl;
    cout << crit3.s_Total << endl;
    cout << crit1.GetTotal() << endl;
    cout << crit2.GetTotal() << endl;
    cout << crit3.GetTotal() << endl;
    cout << Critter::s_Total << endl;

    cout << endl;
    return 0;
}

// end of code

