// Friend Critter
// Demonstrates friend function and operator overloading

#include <iostream>
#include <string>

using namespace std;

// class Critter definition

class Critter {
    // make following GLOBAL functions friends of the Critter class
    friend void Peek(const Critter& aCritter);  // GLOBAL FUNCTION
    friend ostream& operator<<(ostream& os, const Critter& aCritter); // GLOBAL FUNCTION

public:
    Critter(const string& name=""); // constructor

private:
    string m_Name;
};

// constructor definition

Critter::Critter(const string& name):
    m_Name(name)
{}

// GLOBAL FUNCTIONS

void Peek(const Critter& aCritter);

ostream& operator<<(ostream& os, const Critter& aCritter);

// main function

int main()
{
    Critter crit("POO");

    cout << "Calling Peek() to access crit's private data member, m_Name: \n";
    Peek(crit);

    cout << "\nSending crit object to cout with the << operator:\n";
    cout << crit;

    cout << endl;
    return 0;
}

// global friend function which can access all of a Critter object's members

void Peek(const Critter& aCritter)
{
    cout << aCritter.m_Name << endl;
}

// global friend function which can access all of a Critter object's members
// overloads the << operator so you can send a Critter object to cout

ostream& operator<<(ostream& os, const Critter& aCritter)
{
    os << "Critter Object - ";
    os << "m_Name: " << aCritter.m_Name;

    return os;
}

// end of code
