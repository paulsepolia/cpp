// demonstrates constant reference

#include <iostream>
#include <string>
#include <vector>

using namespace std;

// parameter vec is a constant reference to a vector of strings

// function declaration

void display(const vector<string> &vv);

// main function

int main()
{
    vector<string> vv;

    vv.push_back("sword");
    vv.push_back("armor");
    vv.push_back("shield");

    display(vv);

    return 0;
}

// function definition
// parameter vec is a constant reference to a vector of strings

void display(const vector<string> &vec)
{
    cout << " Your items:\n";
    vector<string>::const_iterator iter;

//  vec[1] = "21"; can not do this
    for (iter = vec.begin(); iter != vec.end(); iter++) {
        cout << *iter << endl;
    }
}

// end of code
