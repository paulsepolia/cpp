// Referencing
// Demonstrates using references

#include <iostream>

using namespace std;

int main()
{
    int myScore = 1000;
    int &mikesScore = myScore; // create a reference

    cout << endl;
    cout << " myScore is: " << myScore << endl;
    cout << " mikesScore is: " << mikesScore << endl;

    cout << endl;
    cout << " Adding 500 to myScore. " << endl;
    myScore += 500;
    cout << endl;
    cout << " myScore is: " << myScore << endl;
    cout << " mikesScore is: " << mikesScore << endl;

    cout << endl;
    cout << " Adding 500 to mikesScore. " << endl;
    mikesScore += 500;
    cout << endl;
    cout << " myScore is: " << myScore << endl;
    cout << " mikesScore is: " << mikesScore << endl;

    int larrysScore = 2500;
    mikesScore = larrysScore;
    cout << endl;
    cout << " myScore is: " << myScore << endl;
    cout << " mikesScore is: " << mikesScore << endl;

//  int &my_error;

    cout << endl;
    return 0;
}

// end of code

