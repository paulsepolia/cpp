//==============//
// Header file. //
// EmployeeCL.h //
//==============//

//=================================================================//
// This is the interface for the class EmployeeCL.                 //
// This is primarily intended to be used as a base class to derive //
// classes for different kinds of employees.                       //
//=================================================================//

#ifndef EMPLOYEE_CL_H
#define EMPLOYEE_CL_H

#include <string>
using std::string;

namespace PGG {

class EmployeeCL {
public:
    EmployeeCL();
    EmployeeCL(string theName, string theSsn);
    string getName() const;
    string getSsn() const;
    double getNetPay() const;
    void setName(string newName);
    void setSsn(string newSsn);
    void setNetPay(double newNetPay);
    virtual void printCheck() = 0; // a pure virtual function

private:
    string name;
    string ssn;
    double netPay;
};

} // end of namespace PGG

#endif // end of guard EMPLOYEE_CL_H

//==============//
// End of code. //
//==============//
