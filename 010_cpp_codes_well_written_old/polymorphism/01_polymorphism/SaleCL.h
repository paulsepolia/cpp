//===========================//
// Header file.              //
// SaleCL class declaration. //
//===========================//

#ifndef SALE_CL_H
#define SALE_CL_H

namespace PGG {
class SaleCL {
public:
    SaleCL();
    SaleCL(double thePrice);
    double getPrice() const;
    void setPrice(double newPrice);
    virtual double bill() const;
    double savings(const SaleCL& other) const;

private:
    double price;
};

bool operator < (const SaleCL& first, const SaleCL& second);
// compares two sales to see which is larger.

} // end of namespace PGG

#endif // end of guard SALE_CL_H

//==============//
// End of code. //
//==============//
