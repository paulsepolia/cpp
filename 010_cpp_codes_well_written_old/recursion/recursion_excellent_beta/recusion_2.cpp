// this program demonstrates a simple recursive function.

#include <iostream>

using namespace std;

// function prototype

void message(long int);

// main function
int main()
{
    message(15);

    return 0;
}

// definition of function

void message(long int times)
{
    if (times > 0) {
        if (times%1 == 0) {
            cout << "This is a recursive function.\n";
            cout << " ---> " << times << endl;
        }
        message(times - 1);
    }

    cout << "Message returning with " << times << endl;

}

