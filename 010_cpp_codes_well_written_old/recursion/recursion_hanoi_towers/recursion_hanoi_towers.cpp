// this program displays a solution to the towers of Hanoi game.

#include <iostream>
#include <string>

using namespace std;

// function prototype

void moveDisks(int, string, string, string);

// the main function

int main()
{

    const int numDisks = 100;
    // Play the game with n disks

    moveDisks(numDisks, "peg 1", "peg 3","peg 2");

    cout << "All the disks have been moved!";
    cout << endl;

    return 0;
}

// function definition
// n : the number of disks to move from.
// source : the peg to move from.
// dest   : the peg to move to.
// temp   : the temporary peg.

void moveDisks(int n, string source, string dest, string temp)
{
    if (n > 0) {
        // move n-1 disks from source to temp
        // using dest as the temporary peg.

        moveDisks(n-1, source, temp, dest);

        // move adisk from source to dest.

//    cout << "Move a disk from " << source
//	 << " to " << dest << endl;

        // move n-1 disks from temp to dest
        // using source as the temporary peg.

        moveDisks(n-1, temp, dest, source);
    }
}

// this is the end of the source code
