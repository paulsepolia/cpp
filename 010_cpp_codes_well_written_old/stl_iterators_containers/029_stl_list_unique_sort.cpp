//===========================//
// The list member functions //
// 'unique' and 'sort'.      //
//===========================//

#include <iostream>
#include <list>
#include <iterator>

using std::endl;
using std::cout;
using std::list;
using std::ostream_iterator;

// 1. Function definition.

void out(const char *s, const list<long> &L)
{
    cout << s;
    copy(L.begin(), L.end(), ostream_iterator<long>(cout, " "));
    cout << endl;
}

int main()
{
    list<long> L(10, 123);
    L.push_back(100);
    L.push_back(123);
    L.push_back(123);

    out("Initial contents: ", L);

    L.unique();

    out("After L.unique(): ", L);

    L.sort();

    out("After L.sort():   ", L);

    return 0;
}

//==============//
// End of code. //
//==============//