//=====================================================//
// Program to demonstrate the STL template class list. //
//=====================================================//

#include <iostream>
#include <list>

using std::cout;
using std::endl;
using std::list;

int main()
{
    list<int> listObject;

    for (int i = 1; i <= 10; i++) {
        listObject.push_back(i);
    }

    cout << " List contains: " << endl;

    list<int>::iterator iter;

    for (iter = listObject.begin(); iter != listObject.end(); iter++) {
        cout << *iter << " ";
    }

    cout << endl;

    cout << " setting all entries to 0. " << endl;

    for (iter = listObject.begin(); iter != listObject.end(); iter++) {
        *iter = 0;
    }

    cout << " List contains: " << endl;

    for (iter = listObject.begin(); iter != listObject.end(); iter++) {
        cout << *iter << " ";
    }

    cout << endl;

    return 0;
}

//==============//
// End of code. //
//==============//

