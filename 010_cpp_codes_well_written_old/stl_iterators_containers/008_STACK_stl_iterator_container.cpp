//==================================================================//
// Program to demonstrate use of stack template class from the STL. //
//==================================================================//

#include <iostream>
#include <stack>

using std::cin;
using std::cout;
using std::endl;
using std::stack;

int main()
{
    const long STACK_SIZE = 2*1E8;
    const long NUM_DO = 1E2;

    stack<double> sta1;
    stack<double> sta2;

    long i;
    long k;
    long tmp1 = 0;

    for (k = 0; k < NUM_DO; k++) {

        cout << " 1 --> Building the stack. " << endl;

        for (i = 0; i < STACK_SIZE; i++) {
            sta1.push(static_cast<double>(i));
        }

        cout << " 2 --> Equating stack 1 and stack 2. " << endl;

        sta2 = sta1;

        cout << " 3 --> The top element of stacks. " << endl;

        cout << sta1.top() << endl;
        cout << sta2.top() << endl;

        cout << " 4 --> Size of stacks. " << endl;

        cout << sta1.size() << endl;
        cout << sta2.size() << endl;

        cout << " 5 --> Remove an element from the top of the stack. " << endl;

        sta1.pop();

        cout << " 6 --> Remove an element from the top of the stack. " << endl;

        sta1.pop();

        cout << " 7 --> Size of the stacks. " << endl;

        cout << sta1.size() << endl;
        cout << sta2.size() << endl;

        cout << " 8 --> The top element of stacks. " << endl;

        cout << sta1.top() << endl;
        cout << sta2.top() << endl;

        cout << " 9 --> Removing all the elements from the stack 1. " << endl;

        tmp1 = sta1.size();
        for (i = 0; i < tmp1; i++) {
            sta1.pop();
        }

        cout << " 10 --> Size of the stacks. " << endl;

        cout << sta1.size() << endl;
        cout << sta2.size() << endl;

        cout << " 11 --> Equating the empty stack 1 to stack 2." << endl;

        sta2 = sta1;

        cout << " 12 --> Size of the stacks. " << endl;

        cout << sta1.size() << endl;
        cout << sta2.size() << endl;
    }

    return 0;
}

//==============//
// End of code. //
//==============//
