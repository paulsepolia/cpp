//==================================//
// Finding a given value in a list. //
//==================================//

#include <iostream>
#include <list>
#include <algorithm>

using std::cin;
using std::cout;
using std::endl;
using std::list;

int main()
{
    int x;
    list<int> v;

    v.push_back(2);
    v.push_back(5);
    v.push_back(8);

    cout << "Enter an integer to be found in {2, 5, 8}:" << endl;

    cin >> x;

    list<int>::iterator i;

    i = find(v.begin(), v.end(), x);

    if (i == v.end()) {
        cout << "Not found." << endl;
    } else {
        cout << "Found, " << endl;
        if (i == v.begin()) {
            cout << "as the first element.";
        } else {
            cout << " after " << *(--i);
        }
    }

    cout << endl;

    return 0;
}

//==============//
// End of code. //
//==============//
