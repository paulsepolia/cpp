//=======================================================//
// Program to demonstrate use of the set template class. //
//=======================================================//

#include <iostream>
#include <set>

using std::cout;
using std::endl;
using std::set;

int main()
{
    set<char> s;

    s.insert('A');
    s.insert('B');
    s.insert('B');
    s.insert('B');
    s.insert('B');
    s.insert('C');
    s.insert('C');
    s.insert('D');

    cout << " The set contains: " << endl;

    set<char>::const_iterator p;

    for (p = s.begin(); p != s.end(); p++) {
        cout << *p << " ";
    }

    cout << endl;

    cout << " Set contains 'C': ";

    if (s.find('C') == s.end()) {
        cout << " no " << endl;
    } else {
        cout << " yes " << endl;
    }

    cout << " Removing C. " << endl;

    s.erase('C');

    for (p = s.begin(); p != s.end(); p++) {
        cout << *p << " ";
    }

    cout << endl;

    cout << " Set contains 'C': ";

    if (s.find('C') == s.end()) {
        cout << " no " << endl;
    } else {
        cout << " yes " << endl;
    }

    return 0;
}

//==============//
// End of code. //
//==============//

