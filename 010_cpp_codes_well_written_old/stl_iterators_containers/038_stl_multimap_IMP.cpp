//==========//
// multimap //
//==========//

#include <iostream>
#include <string>
#include <map>
#include <sstream>

using std::endl;
using std::cout;
using std::string;
using std::multimap;
using std::stringstream;

// 1. Function definition.

string convertLongInt(long a)
{
    stringstream ss;   // 1. Create a stringstream
    ss << a;           // 2. Add number to the stream

    return ss.str();   // 3. Return a string with the contents of the stream
}

// 2. Type definition.

typedef multimap<string, long>   mmasL;
typedef multimap<string, double> mmasD;

// 3. The main function.

int main()
{
    // 1. Local constants.

    const long DIM_MA_A = 5.0 * 1E6;
    const long NUM_DO_A = 10;
    const long NUM_DO_B = 1E4;

    // 2. Local variables.

    long i;
    long k;
    string s;
    mmasL::iterator itMMSL;
    mmasD::iterator itMMSD;

    // 3. The main code.

    for (k = 1; k <= NUM_DO_B; k++) {

        cout << "------------------------------------------------>>> " << k << endl;

        mmasL *pMSL1 = new mmasL;
        mmasL *pMSL2 = new mmasL;

        cout << "  1 --> Building the *pMSL1." << endl;

        for (i = 0; i < DIM_MA_A; i++) {
            s = convertLongInt(DIM_MA_A-i);

            (*pMSL1).insert(mmasL::value_type(s, DIM_MA_A-i));
            (*pMSL1).insert(mmasL::value_type(s, DIM_MA_A-i+1));
        }

        cout << "  2 --> Building the *pMSL2." << endl;

        for (i = 0; i < DIM_MA_A; i++) {
            s = convertLongInt(DIM_MA_A-i);

            (*pMSL2).insert(mmasL::value_type(s, DIM_MA_A-i));
            (*pMSL2).insert(mmasL::value_type(s, DIM_MA_A-i+1));
        }

        cout << "  3 --> Test for equality *pMSL1 and *pMSL2." << endl;

        for (i = 1; i <= NUM_DO_A; i++) {
            if (*pMSL1 == *pMSL2) {
                cout << " Equal --> " << i << endl;
            } else {
                cout << " Not Equal --> " << i << endl;
            }
        }

        cout << "  4 --> Size of *pMSL1." << endl;

        cout << pMSL1 -> size() << endl;

        cout << "  5 --> Size of *pMSL2." << endl;

        cout << pMSL2 -> size() << endl;

        cout << "  6 --> Find and erase element by element in pMSL1." << endl;

        for (i = 0; i < DIM_MA_A; i++) {
            s = convertLongInt(DIM_MA_A-i);

            itMMSL = pMSL1 -> find(s);  // 1.

            pMSL1 -> erase(itMMSL);     // 1.

            itMMSL = pMSL1 -> find(s);  // 2.

            pMSL1 -> erase(itMMSL);     // 2.
        }

        cout << "  7 --> Find and erase element by element in pMSL2." << endl;

        for (i = 0; i < DIM_MA_A; i++) {
            s = convertLongInt(DIM_MA_A-i);

            itMMSL = pMSL2 -> find(s);  // 1.

            pMSL2 -> erase(itMMSL);     // 1.

            itMMSL = pMSL2 -> find(s);  // 2.

            pMSL2 -> erase(itMMSL);     // 2.

        }

        cout << "  8 --> Size of *pMSL1." << endl;

        cout << pMSL1 -> size() << endl;

        cout << "  9 --> Size of *pMSL2." << endl;

        cout << pMSL2 -> size() << endl;

        // free up memory here

        cout << " 10 --> Free up RAM." << endl;

        delete pMSL1;
        delete pMSL2;

        // about map and doubles now

        mmasD *pMSD1 = new mmasD;
        mmasD *pMSD2 = new mmasD;

        cout << " 11 --> Building the *pMSD1." << endl;

        for (i = 0; i < DIM_MA_A; i++) {
            s = convertLongInt(DIM_MA_A-i);

            (*pMSD1).insert(mmasD::value_type(s, static_cast<double>(DIM_MA_A-i)));
            (*pMSD1).insert(mmasD::value_type(s, static_cast<double>(DIM_MA_A-i+1)));
        }

        cout << " 12 --> Building the *pMSD2." << endl;

        for (i = 0; i < DIM_MA_A; i++) {
            s = convertLongInt(DIM_MA_A-i);

            (*pMSD2).insert(mmasD::value_type(s, static_cast<double>(DIM_MA_A-i)));
            (*pMSD2).insert(mmasD::value_type(s, static_cast<double>(DIM_MA_A-i+1)));
        }

        cout << " 13 --> Test for equality *pMSD1 and *pMSD2." << endl;

        for (i = 1; i <= NUM_DO_A; i++) {
            if (*pMSD1 == *pMSD2) {
                cout << " Equal --> " << i << endl;
            } else {
                cout << " Not Equal --> " << i << endl;
            }
        }

        cout << " 14 --> Size of *pMSD1." << endl;

        cout << pMSD1 -> size() << endl;

        cout << " 15 --> Size of *pMSD2." << endl;

        cout << pMSD2 -> size() << endl;

        cout << " 16 --> Find and erase element by element in pMSD1." << endl;

        for (i = 0; i < DIM_MA_A; i++) {
            s = convertLongInt(DIM_MA_A-i);

            itMMSD = pMSD1 -> find(s);  // 1st find

            pMSD1 -> erase(itMMSD);     // 1st erase

            itMMSD = pMSD1 -> find(s);  // 2nd find

            pMSD1 -> erase(itMMSD);     // 2nd erase
        }

        cout << " 17 --> Find and erase element by element in pMSL2." << endl;

        for (i = 0; i < DIM_MA_A; i++) {
            s = convertLongInt(DIM_MA_A-i);

            itMMSD = pMSD2 -> find(s); // 1st fist

            pMSD2 -> erase(itMMSD);    // 1st erase

            itMMSD = pMSD2 -> find(s); // 2nd find

            pMSD2 -> erase(itMMSD);    // 2nd erase

        }

        cout << " 18 --> Size of *pMSD1." << endl;

        cout << pMSD1 -> size() << endl;

        cout << " 19 --> Size of *pMSD2." << endl;

        cout << pMSD2 -> size() << endl;

        // free up memory here

        cout << " 20 --> Free up RAM." << endl;

        delete pMSD1;
        delete pMSD2;

    }

    return 0;
}

//==============//
// End of code. //
//==============//