// dynamic structure allocation
#include <iostream>
#include <string>
using namespace std;

// 1.
struct TeleType {
    string name;
    string phoneNo;
};

// 2.
void populate( TeleType * ); // function prototype
void dispOne( TeleType * );  // function prototype

// 3.
int main()
{
    char key;
    TeleType * recPoint;  // recPoint is a pointer to a structure of type TeleType

    cout << " Do you wish to create a new record (y/n): ";

    key = cin.get();

    if ( key == 'y' ) {
        key = cin.get(); // get the Enter key in buffered input
        recPoint = new TeleType;
        populate( recPoint );
        dispOne( recPoint );
    } else {
        cout << endl;
        cout << " No record has been created. ";
    }

    return 0;
}

// 4.

// input name and phone number
void populate ( TeleType * record ) // record is a pointer to a TeleType
{
    cout << " Enter a name: ";
    getline( cin, record -> name );
    cout << " Enter a phone number: ";
    getline( cin, record -> phoneNo );

    return ;
}

// display the contents of one record
void dispOne( TeleType * contents )
{
    cout << endl;
    cout << " The contents of the record just created are: " << endl;
    cout << " Name: " << contents -> name << endl;
    cout << " Phone Number: " << contents -> phoneNo << endl;

    return;
}
