// This program illustrates how to use a vector object

#include<iostream>
#include<vector>

using namespace std;

int main()
{
    vector<int> intList;
    unsigned int i;
    int j;
    int k;
    const unsigned int dimen = 550000000;
    const int jLim = 10;
    const int kLim = 100;

    for ( k = 0; k < kLim; k++ ) {
        for ( i = 0; i < dimen; i++ ) {
            intList.push_back(static_cast<double>(i) );
        }

        cout << " the size of the vector is "
             << intList.size() << endl;

        cout << intList[1] << endl;

        cout << endl;

        for ( j = 0; j < jLim; j++ ) {

            for ( i = 0; i < intList.size(); i++ ) {
                intList[i] = intList[i] * 2;
            }

            cout << j << " -- " << intList[1] << endl;
        }

        cout << " the first element is " << intList.front() << endl;
        cout << " the last element is " << intList.back() << endl;

        cout << " deleting the manually all the elements " << endl;

        for ( i = 0; i < dimen; i++ ) {
            intList.pop_back();
        }

        cout << " now the last elements is " << intList.back() << endl;

        cout << " the size of the vector is "
             << intList.size() << endl;

    }


    return 0;
}
