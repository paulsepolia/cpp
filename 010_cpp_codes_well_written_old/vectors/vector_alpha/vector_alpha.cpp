
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

// main function.
int main()
{

// 1. create an initialize an array

    const int dimen=5;
    int nums[dimen];
    int i;

    for ( i=0; i<dimen; i++) {
        nums[i]=100-i;
    }

// 2. create a vector of strings using the above array

    vector<int> vecAlpha(nums, nums+dimen);

// 3. output the size of the array.

    cout << " the vector initially has a size of "
         << int( vecAlpha.size() ) << endl;

// 4. output the contents of the array.

    cout << " and the elements are: " << endl ;
    for ( i=0; i<int(vecAlpha.size()); i++ ) {
        cout << vecAlpha[i] << "  " ;
    }

// 5. replace an element.

    vecAlpha[3] = 144;

    cout << endl ;
    cout << " after replacing the 4th element, the vector has a size of " ;
    cout << int( vecAlpha.size() ) << endl ;

    for ( i=0; i<int(vecAlpha.size()); i++ ) {
        cout << vecAlpha[i] << "  " ;
    }

    cout << endl;

// 6. insert an element at position 2

    vecAlpha.insert(vecAlpha.begin()+1, 142 );

    cout << " after inserting an element at position 2 the vector has a size of " ;
    cout << int( vecAlpha.size() ) << endl ;

    for ( i=0; i<int(vecAlpha.size()); i++ ) {
        cout << vecAlpha[i] << "  " ;
    }

    cout << endl ;

// 7. add an element to the end of the vector

    vecAlpha.push_back(157);

    cout << " after adding an element at the end, the vector has a size of " ;
    cout << int( vecAlpha.size() ) << endl ;

    for ( i=0; i<int(vecAlpha.size()); i++ ) {
        cout << vecAlpha[i] << "  " ;
    }

    cout << endl ;

// 8. sorting the list.

    sort(vecAlpha.begin(), vecAlpha.end());

    cout << " after sorting the vector has a size of " ;
    cout << int( vecAlpha.size() ) << endl ;

    for ( i=0; i<int(vecAlpha.size()); i++ ) {
        cout << vecAlpha[i] << "  " ;
    }

    cout << endl ;

    return 0;
}

// fini.
