//=====================================//
// Catch with base class matches throw //
// with derived class.                 //
//=====================================//

#include <iostream>

using std::endl;
using std::cout;

// 1. Class B.

class B {};

// 2. Class D.

class D: public B {};

// 3. Main function.

int main()
{
    try {
        throw D();
    } catch (B) {
        cout << "Exception thrown." << endl;
    }

    return 0;
}

//==============//
// End of code. //
//==============//
