//=================================//
// Function throwing an exception. //
//=================================//

#include <iostream>
#include <cstdlib>

using std::cin;
using std::cout;
using std::endl;

// 1. DivideByZero class declaration and definition.

class DivideByZero {
};

// 2. Function declaration.

double safeDivide(int top, int bottom) throw (DivideByZero);

// 3. The main function.

int main()
{
    int numerator;
    int denominator;
    double quotient;

    cout << " Enter numerator: " << endl;
    cin >> numerator;
    cout << " Enter denominator: " << endl;
    cin >> denominator;

    try {
        quotient = safeDivide(numerator, denominator);
    } catch(DivideByZero) {
        cout << " Error: Division by zero." << endl;
        cout << " Program aborting." << endl;
        exit(1);
    }

    cout << numerator << " / " << denominator << " = "
         << quotient << endl;

    cout << " End of program." << endl;

    return 0;
}

// 4. Function definition.

double safeDivide(int top, int bottom) throw (DivideByZero)
{
    if (bottom == 0) {
        throw DivideByZero();
    }

    return (top / static_cast<double>(bottom));
}

//==============//
// End of code. //
//==============//

