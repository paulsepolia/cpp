//=============================//
// Exception handling example. //
//=============================//

#include <iostream>

using std::cout;
using std::endl;
using std::cin;

int main()
{
    int i;
    char ch;

    cout << " Enter an integer, followed by some "
         "nonnumeric character:" << endl;

    try {
        cin >> i >> ch;

        if (i == 0) throw 0;
        if (ch == '?') throw '?';
    } catch (int) {
        cout << "Zero entered." << endl;
    } catch (char) {
        cout << "Question mark entered." << endl;
    }

    cout << endl;

    return 0;
}

//==============//
// End of code. //
//==============//
