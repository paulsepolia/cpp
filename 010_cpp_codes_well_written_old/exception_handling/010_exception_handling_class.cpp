//========================================//
// Exception handling class.              //
// The simplest possible exception class. //
//========================================//

#include <iostream>

using std::endl;
using std::cout;

// 1. Exception class.

class MyException {};

// 2. Main function.

int main()
{
    try {
        throw MyException();
    } catch (MyException) {
        cout << " Exception thrown." << endl; // This text is displayed
    }

    return 0;
}

//==============//
// End of code. //
//==============//
