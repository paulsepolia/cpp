// assert

#define NDEBUG      // disables the function of assert.
#include <iostream>
#include <cassert>


using namespace std;

int main()
{
    // declare variables; step 1

    int denominator;
    double numerator;
    double res;

    denominator = 0;
    numerator = 100.0;

    assert(denominator);
    res = numerator/denominator;

    cout << res << endl;
    return 0;
}