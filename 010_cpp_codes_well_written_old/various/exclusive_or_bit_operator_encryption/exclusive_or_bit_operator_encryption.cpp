#include <iostream>
using namespace std;

void encrypt( char * ); // function prototype

int main()
{
    char message[81];  // enough storage for a complete line

    cout << " Enter a sentence: " << endl;
    cin.getline(message,80,'\n');
    cout << endl;
    cout << " The sentence just entered is: "
         << message << endl;
    encrypt(message);
    cout << endl;
    cout << " The encrypted version of this sentence is " << endl;
    cout << message << endl;

    return 0;
}

void encrypt( char * message )
{
    while ( * message != '\0' ) {
        *message++ ^= 52;
    }
}
