// this is the code file of the recursive fibonacci function

#include <iostream>

using namespace std;

// the declaration of the recursive fibonacci number

int rFibNum(int a, int b, int n);

// the main function

int main()
{
    int firstFibNum;
    int secondFibNum;
    int nth;

    cout << " Enter the first Fibonacci number: ";
    cin >> firstFibNum;
    cout << endl;

    cout << " Enter the second Fibonacci number: ";
    cin >> secondFibNum;
    cout << endl;

    cout << " Enter the position of the desired Fibonacci number: ";
    cin >> nth;
    cout << endl;

    cout << " The Fibonacci number at position " << nth
         << " is: " << rFibNum( firstFibNum, secondFibNum, nth)
         << endl;

    return 0;
}

// the definition of the recursive fibonacci number

int rFibNum(int a, int b, int n)
{
    if (n == 1) {
        return a;
    } else if (n == 2) {
        return b;
    } else {
        return rFibNum(a, b, n -1) + rFibNum(a, b, n -2 );
    }

}
