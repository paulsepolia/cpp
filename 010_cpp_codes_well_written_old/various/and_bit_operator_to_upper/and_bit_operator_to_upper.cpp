#include <iostream>
using namespace std;

const int TOUPPER = 0xDF;
void upper( char * ); // function prototype

int main()
{
    char word[81];  // storage for a complete line

    cout << " Enter a string of both uppercase and lowercase letters: " << endl;
    cin.getline(word,80,'\n');
    cout << endl;
    cout << " The string of letters just entered is:\n" << word << endl;
    upper(word);
    cout << endl;
    cout << " This string, in uppercase letters, is:" << endl;
    cout << word << endl;

    return 0;
}

void upper( char * word )
{
    while ( *word != '\0' ) {
        *word++ &= TOUPPER;
    }
}
