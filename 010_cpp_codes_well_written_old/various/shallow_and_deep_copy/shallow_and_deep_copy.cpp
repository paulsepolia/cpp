// shallow and deep copy with pointers

#include <iostream>

using namespace std;

int main()
{
    long int *first;
    long int *second;
    const long int dimen = 200000000;
    const long int dimenCout = 2;
    long int i;

    // shallow copy demonstration

    first = new long int[dimen];  // 1st step of shallow copy
    second = new long int[dimen]; // 2nd step fo shallow copy - NOT A MUST STEP

    for (i = 0; i < dimen; i++) {
        first[i] = dimen-i;
    }

    for (i = 0; i < dimenCout; i++) {
        cout << " first --> " << i << " " << first[i] << endl;
    }

    second = first;  // shallow copy // the fundamental step of shallow copy

    for (i = 0; i < dimenCout; i++) {
        cout << " second --> " << i << " " << second[i] << endl;
    }

    delete [] first;
//  delete [] second; // you can not delete the second or vice-versa

    // deep copy demonstration

    first = new long int[dimen];  // 1st step of deep copy - MUST
    second = new long int[dimen]; // 2nd step of deep copy - MUST

    for (i = 0; i < dimen; i++) {
        first[i] = dimen-2*i;
    }

    for (i = 0; i < dimenCout; i++) {
        cout << " first --> " << i << " " << first[i] << endl;
    }

    for (i = 0; i < dimen; i++) {
        second[i] = first[i];    // 3rd step of deep copy - MUST
    }

    for (i = 0; i < dimenCout; i++) {
        cout << " second --> " << i << " " << second[i] << endl;
    }

    delete [] first;  // now you can delete both of them
    delete [] second; // now you can delete both of them

    return 0;
}




