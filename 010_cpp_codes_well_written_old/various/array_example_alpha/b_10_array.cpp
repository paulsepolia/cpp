
#include <iostream>
using namespace std;

// function prototype.
int findMax( int [], int );
int findMaxRef ( int *, int *);

// main function.
int main()
{
    int dimen=300000000;
    int nums[dimen];
    int i;
    int j;

    for ( i=0; i<dimen; i++) {
        nums[i]=i+1;
    }

    int maxNum;
    int maxNumRef;

    for ( i=0; i<200; i++) {
        for ( j=0; j<10; j++) {
            maxNumRef= findMaxRef( &nums[0], &dimen);
        }
        cout << " ref " << i << endl;
    }

    for ( i=0; i<200; i++) {
        for (j=0; j<10; j++) {
            maxNum = findMax( nums, dimen);
        }
        cout << " val " << i << endl;
    }

    cout << " the max value val    is " << maxNum    << endl;
    cout << " the max value ref    is " << maxNumRef << endl;
    cout << " dimen                is " << dimen     << endl;

    int sentinel;

    cin >> sentinel ;

    return 0;
}

// function to find the maximum value (by value).
int findMax( int vals[], int dimen)
{
    int i;
    int max=vals[0];

    for ( i=1; i < dimen; i++ ) {
        if (max < vals[i]) max=vals[i];
    }

    return max;
}

// function to find the maximum value (by reference).
int findMaxRef( int *vals , int *dimen)
{
    int i;
    int max=vals[0];

    for ( i=1; i < *dimen; i++ ) {
        if (max < vals[i]) max=vals[i];
    }

    return max;
}

// fini.
