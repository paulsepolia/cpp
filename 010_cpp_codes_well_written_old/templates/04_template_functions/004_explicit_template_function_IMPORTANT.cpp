//================================================//
// Two power functions resulting from a template. //
//================================================//

#include <iostream>

using std::cout;
using std::endl;

// 1.

template <class basetype, class resulttype>
resulttype power(basetype a, int n)
{
    resulttype x = 1;

    for (int i = 1; i <= n; i++) {
        x *= a;
    }

    return x;
}

// 2.

int main()
{
    cout << power<int, long>(10, 9) << endl;
    cout << power<double, double>(10, 9) << endl;
    cout << power<double, double>(10, 30) << endl;

    return 0;
}

//==============//
// End of code. //
//==============//



