//=========================================//
// Driver program to swapObjects function. //
//=========================================//

#include <iostream>
#include "040_swap_template_function.h"

using std::cout;
using std::endl;

int main()
{
    int i = 1;
    int j = 2;

    float u = 3.4F;
    float v = 5.6F;

    cout << " i = " << i << endl;
    cout << " j = " << j << endl;
    cout << " u = " << u << endl;
    cout << " v = " << v << endl;

    swapObjects(i, j);

    swapObjects(u, v);

    cout << " i = " << i << endl;
    cout << " j = " << j << endl;
    cout << " u = " << u << endl;
    cout << " v = " << v << endl;

    return 0;
}

//==============//
// End of code. //
//==============//
