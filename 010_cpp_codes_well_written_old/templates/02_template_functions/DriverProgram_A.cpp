// Demonstrates a template function
// that implements a generic version
// of the selection sort algorithm.

#include <iostream>
using std::cout;
using std::endl;

// 1. Functions declaration.

template<class T, class P>
void sort(T a[], P numberUsed);

template<class T>
void swapValues(T& variable1, T& variable2);

template<class T, class P>
P indexOfSmallest(const T a[], P startIndex, P numberUsed);

// 2. Include functions definition.

#include "SortFunction.h"

// 3. Main function.

int main()
{
    // Sorting integers.

    int i;
    int a[10] = {9, 8, 7, 6, 5, 1, 2, 3, 0, 4};
    cout << "Unsorted integers:" << endl;

    for (i = 0; i < 10; i++) {
        cout << a[i] << " " ;
    }

    cout << endl;

    sort(a, 10);

    cout << "In sorted order the integers are:" << endl;

    for (i = 0; i < 10; i++) {
        cout << a[i] << " " ;
    }

    cout << endl;

    // Sorting doubles.

    double b[5] = {1.1, 0.1, 5.5, 3.3, 4.0 };
    cout << "Unsorted doubles:" << endl;

    for (i = 0; i < 5; i++) {
        cout << b[i] << " " ;
    }

    cout << endl;

    sort(b, 5);

    cout << "In sorted order the integers are:" << endl;

    for (i = 0; i < 5; i++) {
        cout << b[i] << " " ;
    }

    cout << endl;

    // Sorting characters.

    char c[7] = {'G', 'E', 'N', 'E', 'R', 'I', 'C'};
    cout << "Unsorted chars:" << endl;

    for (i = 0; i < 7; i++) {
        cout << c[i] << " " ;
    }

    cout << endl;

    sort(c, 7);

    cout << "In sorted order the chars are:" << endl;

    for (i = 0; i < 7; i++) {
        cout << c[i] << " " ;
    }

    cout << endl;

    return 0;
}

//==============//
// End of code. //
//==============//
