// this is a demostratoion of the quick sort recursively defined

#include <iostream>
#include <algorithm> // needed for the swap function;
#include <iomanip>

using namespace std;

// function prototypes
void quickSort(long double [],
               long int,
               long int);

long double partition(long double [],
                      long int,
                      long int);

// the main() function

int main()
{
    const long int SIZE=70000;
    long int i;
    long double* array;
    array = new long double[SIZE];

    for (i = 0; i < SIZE; i++) {
        array[i] = static_cast<long double>(SIZE-i);
    }

    // sort the array using quickSort

    quickSort(array, 0, SIZE-1);

    // display some values

    cout << fixed << showpoint << setprecision(5) << endl;

    for (i = 0; i < 10; i++) {
        cout << array[i] << endl;
    }
}

// function definitions
// a.

void quickSort(long double arr[],
               long int start,
               long int end)
{
    if (start < end) {
        // partition the array and get the pivot point
        long double p = partition(arr, start, end);

        // sort the portion before the pivot point
        quickSort(arr, start, p - 1);

        // sort the portion after the pivot point
        quickSort(arr, p + 1, end);
    }
}

// b.

long double partition( long double arr[],
                       long int start,
                       long int end)
{
    // the pivot of the element is taken to be the
    // element at the start of the subrange to be partitioned.

    long double pivotValue = arr[start];
    long int pivotPosition = start;

    // rearrange the rest of the array elements to
    // partition the subrange from the start to end.

    for ( long int pos = start + 1; pos <= end; pos++ ) {
        if (arr[pos] < pivotValue) {
            // arr[scan] is the "current" item.
            // swap the current item with the item to the
            // right of the pivot element
            swap(arr[pivotPosition + 1], arr[pos]);
            // swap the current item with the pivot element.
            swap(arr[pivotPosition], arr[pivotPosition + 1]);
            // adjust the pivot position so it stays with the
            // pivot element.
            pivotPosition++;
        }
    }
    return pivotPosition;
}

// this is the end of the code file
