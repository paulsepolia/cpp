// insertion sort

#include <iostream>

using namespace std;

void insertionSort( int list[], int listLength );

int main()
{
    const int dimen = 400000;

    int *list  = new int [dimen];

    int i;

    for ( int i=0; i<dimen; i++ ) {
        list[i] = dimen-i;
    }

    insertionSort( list, dimen );

    cout << "After sorting, the list elements are:" << endl;

    for ( i=0; i < dimen; i++ ) {
        cout << list[i] << endl;
    }

    cout << endl;

    return 0;
}


void insertionSort( int list[], int listLength )
{
    int firstOutOfOrder;
    int location;
    int temp;

    for( firstOutOfOrder=1; firstOutOfOrder < listLength; firstOutOfOrder++ ) {
        temp = list[firstOutOfOrder];
        location = firstOutOfOrder;

        do {
            list[location] = list[location-1];
            location--;
        } while ( location > 0 && list[location-1] > temp );

        list[location] = temp;
    }
}


