
#include <iostream>
using namespace std;

// function prototype.
long int selectionSort( long int * , long int  );

// main function
int main()
{
    // 1. the variables.

    const long int numelem=100000;
    long int i;
    long int * array = new long int[ numelem ];
    long int sentinel;
    long int moves;

    // 2. building the array to be sorted.

    for ( i=0; i<numelem; i++ ) {
        array[i] = numelem-i;
    }

    // 3. sorting the array.

    moves = selectionSort( array, numelem ) ;

    // 4. some output.

    cout << " some output of the sorted list is " << endl;

    cout << endl;

    for( i=0; i<10; i++ ) {
        cout << array[i] << endl;
    }

    cout << endl;

    cout << " moves that done are: " << moves << endl;

    cout << array[1] << endl;

    cin >> sentinel;

    return 0;
}

// 5. function definition.

long int selectionSort( long int * num, long int  numelem )
{
    long int i,j,min,minidx, temp, moves=0;

    for ( i=0; i<(numelem-1); i++ ) {
        min = num[i]; // assume minimum is the first array element
        minidx = i;   // index of minimum element

        for ( j = i+1; j< numelem; j++ ) {
            if( num[j] < min ) { // if you have located a lower value
                min = num[j];    // capture it
                minidx = j;
            }
        }

        if ( min < num[i] ) { // check whether you have a new minimum
            // and if you do, swap values
            temp = num[i];
            num[i] = min;
            num[minidx] = temp;
            moves++;
        }
    }

    return moves;
}
