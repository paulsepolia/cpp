//====================================================//
// This is the file merge_sort.cpp.                   //
// The merge sort realization of the Sorting pattern. //
//====================================================//

// 1. Template function split.

template <class T1, class T2>
T2 split(T1 a[], T2 begin, T2 end)
{
    return ((begin + end)/2);
}

// 2. Template function join.

template <class T1, class T2>
void join(T1 a[], T2 begin, T2 splitPt, T2 end)
{
    T1 *temp;
    T2 intervalSize = (end - begin);
    temp = new T1[intervalSize];
    T2 nextLeft = begin; // index for first chunk
    T2 nextRight = splitPt; // index for second chunk
    T2 i = 0; // index for temp

    // Merge till one side is exhausted:

    while ((nextLeft < splitPt) && (nextRight < end)) {
        if (a[nextLeft] < a[nextRight]) {
            temp[i] = a[nextLeft];
            i++;
            nextLeft++;
        } else {
            temp[i] = a[nextRight];
            i++;
            nextRight++;
        }
    }

    while (nextLeft < splitPt) { // copy rest of left chunk, if any.
        temp[i] = a[nextLeft];
        i++;
        nextLeft++;
    }

    while (nextRight < end) { // copy rest of right chunk, if any.
        temp[i] = a[nextRight];
        i++;
        nextRight++;
    }

    for (i = 0; i < intervalSize; i++) {
        a[begin + i] = temp[i];
    }

    delete [] temp;
}

//==============//
// End of code. //
//==============//
