//====================================================//
// This is the file quick_sort.cpp.                   //
// The quick sort realization of the Sorting pattern. //
//====================================================//

// 1. Template function split.

template <class T1, class T2>
T2 split(T1 a[], T2 begin, T2 end)
{
    T1 *temp;
    T2 size = (end - begin);
    temp = new T1[size];

    T1 splitV = a[begin];
    T2 up = 0;
    T2 down = size - 1;
    // Note that a[begin] = splitV is skipped.
    for (T2 i = begin + 1; i < end; i++) {
        if (a[i] <= splitV) {
            temp[up] = a[i];
            up++;
        } else {
            temp[down] = a[i];
            down--;
        }
    }
    // 0 <= up = down < size

    temp[up] = a[begin]; // positions the split value, splitV.

    // temp[i] << splitV for i < up;
    // temp[up] = splitV;
    // temp[i] > splitV for i > up.
    // So, temp[i] <= temp[j] for i in [0, up) and j in [up, end).

    for (T2 i = 0; i < size; i++) {
        a[begin + i] = temp[i];
    }

    delete [] temp;

    if (up > 0) {
        return (begin + up);
    } else {
        return (begin + 1);    // Ensures that both pieces are nonempty.
    }

}

// 2. Template function join.

template <class T1, class T2>
void join(T1 a[], T2 begin, T2 splitPt, T2 end)
{
    // do nothing
}

//==============//
// End of code. //
//==============//
