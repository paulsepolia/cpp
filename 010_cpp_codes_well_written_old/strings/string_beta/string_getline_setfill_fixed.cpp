// getline, setfill, setw, fixed

#include <iostream>
#include <iomanip>
#include <string>

using namespace std;

int main()
{
    // step 1
    string movieName;
    double adultTicketPrice;
    double childTicketPrice;
    int noOfAdultTicketsSold;
    int noOfChildTicketsSold;
    double percentDonation;
    double grossAmount;
    double amountDonated;
    double netSaleAmount;

    cout << fixed << showpoint << setprecision(2) ;   // step 2

    cout << "Enter the movie name: ";                 // step 3
    getline(cin,movieName);
    cout << endl;

    cout << "Enter the price of an adult ticket: ";   // step 5
    cin >> adultTicketPrice;                          // step 6
    cout << endl;

    cout << "Enter the price of a child ticket: ";    // step 7
    cin >> childTicketPrice;                          // step 8
    cout << endl;

    cout << "Enter the number of adult tickets "      // step 9
         << " sold: ";
    cin >> noOfAdultTicketsSold;                      // step 10

    cout << "Enter the number of child tickets "      // step 11
         << " sold: ";
    cin >> noOfChildTicketsSold;                      // step 12

    cout << "Enter the percentage of donation: ";     // step 13
    cin >> percentDonation;                           // step 14
    cout << endl << endl;

    // step 15

    grossAmount = adultTicketPrice * noOfAdultTicketsSold +
                  childTicketPrice * noOfChildTicketsSold;

    // step 16

    amountDonated = grossAmount * percentDonation / 100;

    netSaleAmount = grossAmount - amountDonated; // step 17

    // step 18: output results

    cout << "-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*" << endl;

    cout << setfill('.') << left << setw(35) << "Movie Name: "
         << right << " " << movieName << endl;

    cout << left << setw(35) << "Number of tickets Sold: "
         << setfill(' ') << right << setw(10)
         << noOfAdultTicketsSold + noOfChildTicketsSold << endl;

    cout << setfill('.') << left << setw(35)
         << "Gross Amount: "
         << setfill(' ') << right << " $"
         << setw(8) << grossAmount << endl;




    return 0;
}