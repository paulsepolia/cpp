//=================//
// Header file.    //
// PFArrayDBakCL.h //
//=================//

// This is the header file of the class PFArrayDBakCL.
// This version allows the programmer to make a
// backup copy and restore to the last saved copy
// of the partially filled array.

#ifndef PFARRAYDBAK_CL_H
#define PFARRAYDBAK_CL_H

#include "PFArrayDCL.h"

namespace PGG {

class PFArrayDBakCL : public PFArrayDCL {
public:
    PFArrayDBakCL();
    // initializes with a capacity of 50.

    PFArrayDBakCL(int capacityValue);

    PFArrayDBakCL(const PFArrayDBakCL& Object);

    void backup();
    // Makes a backup copy of the partially filled array.

    void restore();
    // Restores the partially filled array to the last saved version.
    // If backup has never been invoked, this empties the partially filled
    // array.

    PFArrayDBakCL& operator=(const PFArrayDBakCL& rightSide);

    ~PFArrayDBakCL();

private:
    double *b; // for a backup of main array.
    int usedB; // backup for inherited member variable used.
};

} // end of namespace PGG

#endif // end of guard PFARRAYDBAK_CL_H

//==============//
// End of code. //
//==============//
