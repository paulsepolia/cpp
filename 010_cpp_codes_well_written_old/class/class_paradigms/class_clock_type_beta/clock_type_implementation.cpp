// clock_type_impementation.cpp
// this is the implementation file of the class clockType.

#include <iostream>
#include "clock_type_header.h"

using namespace std;

// setTime

void clockType::setTime( int hours, int minutes, int seconds )
{
    if ( 0 <= hours && hours < 24 ) {
        hr = hours;
    } else {
        hr = 0;
    }

    if( 0 <= minutes && minutes < 60 ) {
        min = minutes;
    } else {
        min = 0;
    }

    if ( 0 <= seconds && seconds < 60 ) {
        sec = seconds;
    } else {
        sec = 0;
    }
}


// getTime

void clockType::getTime( int& hours, int& minutes, int& seconds ) const
{
    hours = hr;
    minutes = min;
    seconds = sec;
}

// incrementHours

void clockType::incrementHours()
{
    hr++;

    if ( hr > 23 ) {
        hr = 0 ;
    }
}

// incrementMinutes

void clockType::incrementMinutes()
{
    min++;

    if ( min > 59 ) {
        min = 0;
        incrementHours();
    }
}


// incrementSeconds

void clockType::incrementSeconds()
{
    sec++;

    if ( sec > 59 ) {
        sec = 0;
        incrementMinutes();
    }
}

// printTime

void clockType::printTime() const
{
    if ( hr < 10 ) {
        cout << "0";
    }

    cout << hr << ":";

    if ( min < 10 ) {
        cout << "0";
    }

    cout << min << ":";

    if (sec < 10 ) {
        cout << "0";
    }

    cout << sec;
}

// equalTime

bool clockType::equalTime( const clockType& otherClock ) const
{
    return ( hr == otherClock.hr &&
             min == otherClock.min &&
             sec == otherClock.sec ) ;

}

// constructor with parameters

clockType::clockType( int hours, int minutes, int seconds )
{
    setTime( hours, minutes, seconds );
}

// constructor default

clockType::clockType()
{
    hr = 0;
    min = 0;
    sec = 0;
}

// end of the implementation file: "clock_type_implementation.cpp"
