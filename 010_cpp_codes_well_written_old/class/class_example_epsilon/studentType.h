// this is the header file of the class: studentType

#ifndef H_studentType
#define H_studentType

#include <fstream>
#include <string>
#include "personType.h"
#include "courseType.h"

using namespace std;

class studentType: public personType {
public:
    void setInfo(string fname,
                 string lname,
                 int ID,
                 int nOfCourses,
                 bool isTPaid,
                 courseType courses[],
                 char courseGrades[]);
    // function to set the student's information.
    // postcondition: the member variables are set
    //                accordig to the parameters.

    void print(ostream & outF,
               double tuitionRate);
    // function to print the student's grade report
    // if the member variable isTuitionPaid is true, the grades
    // are shown, otherwise three stars are printed.
    // if the actual parameter corresponding to outF is the object
    // cout, then the output is shown on the standard output device.
    // if the actual parameter corresponding to outF
    // is an ofstream object, say outFile, then the output
    // goes to the file specified by outFile.

    studentType();
    // defaults constructor
    // the member variables are initialized.

    int getHoursEnrolled();
    // function to return the credit hours a student is enrolled in.
    // postcondition: the number of credit hours are calculated
    // and returned.

    double getGpa();
    // function to return the grade point average.
    // postcondition: the gpa is calculated and returned.

    double billingAmount(double tuitionRate);
    // function to return the tuition fees.
    // postcondition: the billing amount is calculated and returned.

private:
    void sortCourses();
    // function to sort the courses.
    // postcondition: the array coursesEnrolled is sorted.
    // for each course, its grade is stored in the array courseGrade.
    // therefore, when the array coursesEnrolled is sorted, the
    // corresponding entries is the array coursesGrade are adjusted.

    int sId; // variable to store the student ID
    int numberOfCourses; // variable to store the number of courses
    bool isTuitionPaid; // variable to indicate whether the tuition is paid
    courseType coursesEnrolled[6]; // array to store the courses
    char coursesGrade[6]; // array to store the course grades
};

#endif

// this is the end of the header file of the class: studentType
