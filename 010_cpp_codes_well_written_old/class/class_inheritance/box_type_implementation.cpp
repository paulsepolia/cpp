// this is the implementation file of the class: boxType

#include <iostream>
#include "rectangle_type_header.h"
#include "box_type_header.h"

using namespace std;

// 1.

void boxType::print() const
{
    rectangleType::print();
    cout << "; Height = " << height;
}

// 2.

void boxType::setDimension( double l, double w, double h)
{
    rectangleType::setDimension(l, w);

    if (h >= 0) {
        height = h;
    } else {
        height = 0;
    }
}

// 3.

double boxType::getHeight() const
{
    return height;
}

// 4.

double boxType::area() const
{
    return 2 * ( getLength() * getWidth() +
                 getLength() * height     +
                 getWidth()  * height );
}

// 5.

double boxType::volume() const
{
    return rectangleType::area() * height;
}

// 6.

boxType::boxType()
{
    height = 0.0;
}

// 7.

boxType::boxType(double l, double w, double h) : rectangleType(l, w)
{
    if (h >= 0) {
        height = h;
    } else {
        height = 0;
    }
}
