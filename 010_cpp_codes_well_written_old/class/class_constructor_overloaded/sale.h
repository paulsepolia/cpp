// this is the header file of the class: sale

#ifndef SALE_H
#define SALE_H

class sale {
public:
    // constructor with 2 parameters
    sale(double rate, double cost);

    // constructor with 1 parameter
    sale(double cost);

    // default constructor
    sale();

    // function
    double getTotal();

private:
    double taxRate;
    double total;
    void calcSale(double cost);
};

#endif
