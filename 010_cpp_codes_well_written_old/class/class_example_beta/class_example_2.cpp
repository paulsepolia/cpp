#include <iostream>
#include <iomanip>
using namespace std;

// 1. class declaration section

class Date {
private:
    int month;
    int day;
    int year;
public:
    Date( int=7, int=4, int=2005 ); // constructor
};

// 2. class implementation section

Date::Date( int mm, int dd, int yyyy )
{
    month = mm;
    day = dd;
    year = yyyy;
    cout << " Created a new date object with data values "
         << month << ", " << day << ", " << year << endl;

}

// 3. main program

int main()
{
    Date a, b, c(4,1,2000); //declare 3 objects
    Date d;
    Date e;
    int i, dimen=10;
    for( i=0; i<dimen; i++) {
        Date f;
    }
    Date g;

    return 0;
}

