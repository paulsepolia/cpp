// this is the header file of the class: cashRegister

class cashRegister {
public:
    int getCurrentBalance() const;
    // function to show the current amount in the cash register.
    // postcondition: the value of the cashOnHand is returned.

    void acceptAmount( int amountIn );
    // function to receive the amount deposited by
    // the customer and update the amount in the register.
    // postcondition: cashOnHand = cashOnHand + amountIn;

    cashRegister( int cashIn = 500 );
    // constructor
    // sets the cash in the register to a specific amount
    // postcondition: cashOnHand = cashIn;
    // if no value is specified when the object is declared,
    // the default value assigned to cashOnHand is 500.

private:
    int cashOnHand;
};


