// this is the implementation file of the class: pointerDataClass

#include <iostream>
#include <cassert>
#include "pointerDataClass.h"

using namespace std;

// 1.
void pointerDataClass::print() const
{
    for (int i = 0; i < length; i++) {
        cout << p[i] << " ";
    }
}

// 2.
void pointerDataClass::insertAt(int index, int num)
{
    // if index is out of bounds, terminate the program
    assert(index >= 0 && index < maxSize);

    if (index < length) {
        p[index] = num;
    } else {
        p[length] = num;
        length++;
    }
}

// 3.
pointerDataClass::pointerDataClass(int size)
{
    if (size <= 0) {
        cout << "The array size must be positive." << endl;
        cout << "Creating an array of the size 10." << endl;

        maxSize = 10;
    } else {
        maxSize = size;
    }

    length = 0;

    p = new int[maxSize];
}

// 4.
pointerDataClass::~pointerDataClass()
{
    delete [] p;
}

// 5.
// copy constructor
pointerDataClass::pointerDataClass
(const pointerDataClass& otherObject)
{
    maxSize = otherObject.maxSize;
    length = otherObject.length;
    p = new int [maxSize];

    for (int i = 0; i < length; i++) {
        p[i] = otherObject.p[i];
    }
}

// this is the end of the implementation file

