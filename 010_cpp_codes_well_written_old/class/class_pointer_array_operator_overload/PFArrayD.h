//=============================//
// Header file.                //
// PFArrayD class declaration. //
//=============================//

class PFArrayD {
public:

    // 1.
    // Constructor. Initializes with capacity of 50.

    PFArrayD();

    // 2.
    // Constructor.

    PFArrayD(int capacityValue);

    // 3.
    // Copy constructor.

    PFArrayD(const PFArrayD& pfaObject);

    // 4.
    // Precondition: The array is not full.
    // Postcondition: The element has been added.

    void addElement(double element);

    // 5.
    // Returns true if the array is full, false otherwise.
    // Declaration and definition.

    bool full() const
    {
        return (capacity == used);
    }

    // 6.
    // Declaration adn definition.

    int getCapacity() const
    {
        return capacity;
    }

    // 7.
    // Declaration and definition.

    int getNumberUsed() const
    {
        return used;
    }

    // 8.
    // Empties the array.
    // Declaration and definition.

    void emptyArray()
    {
        used = 0;
    }

    // 9.
    // Read and change access to elements 0 through numberUsed - 1.

    double& operator[](int index);

    // 10.
    // The assignment operator.

    PFArrayD& operator=(const PFArrayD& rightSide);

    // 11.
    // Destructor

    ~PFArrayD();

private:

    double *a; // for an array of doubles
    int capacity; // for the size of the array
    int used; // for the numer of array positions currently in use
};

//==============//
// End of code. //
//==============//
