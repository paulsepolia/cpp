//=============================//
// Driver Program to PFArrayD. //
//=============================//

#include <iostream>
#include <cstdlib>

using std::cin;
using std::cout;
using std::endl;

#include "PFArrayD.h"
#include "PFArrayDMemberFunctions.h"
#include "PFArrayDExternalFunctions.h"

int main()
{
    cout << " This program tests the class PFArrayD." << endl;
    char ans;
    do {
        testPFArrayD();
        cout << " Test again? (y/n)";
        cin >> ans;
    } while ((ans == 'y') || (ans == 'Y'));

    return 0;
}

//==============//
// End of code. //
//==============//


