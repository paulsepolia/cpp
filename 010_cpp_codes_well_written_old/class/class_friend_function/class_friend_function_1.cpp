#include <iostream>
#include <cmath>
using namespace std;

// 1. class declaration section

class Complex {
    // friends list
    friend double addreal( Complex &, Complex & );
    friend double addimag( Complex &, Complex & );

private:
    double real;
    double imag;
public:
    Complex( double = 0, double = 0 ); // constructor
    void display();
};

// 2. class implememtation statement

Complex::Complex( double rl, double im )
{
    real = rl;
    imag = im;
}

void Complex::display()
{
    char sign = '+';

    if ( imag < 0 ) sign = '-';

    cout << real << sign << abs(imag) << "i" << endl;

    return;

}

// 3. friend implementation

double addreal( Complex & a, Complex & b )
{
    return ( a.real + b.real );
}

double addimag( Complex & a, Complex & b )
{
    return ( a.imag + b.imag );
}

// 4. main function

int main()
{
    Complex a(3.2, 5.6), b(1.1, -8.4);
    double re, im;

    cout << endl;
    cout << " The first complex number is ";
    a.display();

    cout << " The second complex number is ";
    b.display();

    re = addreal(a,b);
    im = addimag(a,b);
    Complex c(re,im);  // create a new Complex object

    cout << endl;
    cout << endl;

    cout << " The sum of these two complex number is: ";

    c.display();

    return 0;
}
