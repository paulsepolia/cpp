// this is the header file of the class: classExample

#ifndef H_classExample
#define H_classExample

class classExample {
public:
    void setX(int a);
    // function to set the value of x
    // postcondition: x = a;

    void print() const;
    // function to output the value of x

private:
    int x;
};

#endif

// this is the end of the header file
