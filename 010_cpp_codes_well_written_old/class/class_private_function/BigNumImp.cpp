// this is the implementation file of the class : BigNum

#include "BigNum.h"

// 1.
// the private function

void BigNum::determineBiggest(int num)
{
    if (num > biggest) {
        biggest = num;
    }
}

// 2.
// default constructor

BigNum::BigNum()
{
    biggest = 0;
}

// 3.
//

bool BigNum::examineNum(int n)
{
    bool goodValue = true;

    if (n > 0) {
        determineBiggest(n);
    } else {
        goodValue = false;
    }

    return goodValue;
}

// 4.
//

int BigNum::getBiggest()
{
    return biggest;
}
