// this is the header of the class: BigNum
// this class ilustrates the use of a private member function

#ifndef H_BigNum
#define H_BigNum

class BigNum {
private:

    int biggest;

    void determineBiggest(int num);

public:

    BigNum(); // default constructor

    bool examineNum(int);

    int getBiggest();
};

#endif

// this is the end of the header file

