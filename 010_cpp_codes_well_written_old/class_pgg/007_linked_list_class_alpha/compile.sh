#!/bin/bash

icpc -O3 -xHost -static -static-intel -Bstatic  \
     -parallel -par-threshold0 -par-report1     \
     DriverElementCL_C.cpp -o x_element
