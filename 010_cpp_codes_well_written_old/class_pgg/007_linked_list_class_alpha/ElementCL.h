//==============================//
// Header file.                 //
// ElementCL Class Declaration. //
//==============================//

template <typename T>
class ElementCL {
public:
    ElementCL<T>(const T& elem=0);   // constructor
    ~ElementCL<T>();                 // destructor

    //=======================//
    // Member functions here. //
    //=======================//

    void SetElement(const T& elem=0);
    T GetElement() const;
    void SetElementNext(ElementCL<T>*);
    ElementCL<T>* GetElementNext() const;

    //========================//
    // Friend functions here. //
    //========================//

private:
    T m_Elem;
    ElementCL<T>* m_pElem;

public:
    //============================//
    // Overloaded operators here. //
    //============================//

    // '='

    template <typename T>
    ElementCL<T>& operator=(const ElementCL<T>& elem)
    {
        if (this != &elem) { // 'this' points to object which calls the
            // function 'operator=()'.
            m_Elem = elem.GetElement();
        }

        return *this;
    }

    // '+'

    template <typename T>
    ElementCL<T> operator+(ElementCL<T> elem)
    {
        elem.SetElement(m_Elem + elem.GetElement());

        return elem;
    }

    // '-'

    template <typename T>
    ElementCL<T> operator-(ElementCL<T> elem)
    {
        elem.SetElement(m_Elem - elem.GetElement());

        return elem;
    }

    // '*'

    template <typename T>
    ElementCL<T> operator*(ElementCL<T> elem)
    {
        elem.SetElement(m_Elem * elem.GetElement());

        return elem;
    }

    // '/'

    template <typename T>
    ElementCL<T> operator/(ElementCL<T> elem)
    {
        elem.SetElement(m_Elem / elem.GetElement());

        return elem;
    }

    // '+='

    template <typename T>
    ElementCL<T> operator+=(ElementCL<T> elem)
    {
        m_Elem = m_Elem + elem.GetElement();

        elem.SetElement(m_Elem);

        return elem;
    }

    // '-='

    template <typename T>
    ElementCL<T> operator-=(ElementCL<T> elem)
    {
        m_Elem = m_Elem - elem.GetElement();

        elem.SetElement(m_Elem);

        return elem;
    }

    // '*='

    template <typename T>
    ElementCL<T> operator*=(ElementCL<T> elem)
    {
        m_Elem = m_Elem * elem.GetElement();

        elem.SetElement(m_Elem);

        return elem;
    }

    // '/='

    template <typename T>
    ElementCL<T> operator/=(ElementCL<T> elem)
    {
        m_Elem = m_Elem / elem.GetElement();

        elem.SetElement(m_Elem);

        return elem;
    }

};

//==============//
// End of code. //
//=============//
