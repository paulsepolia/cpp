//=============================================//
// Header file.                                //
// VectorCL class member functions definition. //
//=============================================//

//  1. Constructor definition.

template <typename T, typename P>
VectorCL<T,P>::VectorCL() {}

//  2. Destructor definition
//     This is an empty destructor,
//     since I delete manually the vectors.

template <typename T, typename P>
VectorCL<T,P>::~VectorCL() {}

//  3. CreateF.

template <typename T, typename P>
void VectorCL<T,P>::CreateF(P dim)
{
    m_Dim = dim;
    m_Vec = new T[m_Dim];
}

//  4. DeleteF.

template <typename T, typename P>
void VectorCL<T,P>::DeleteF()
{
    delete [] m_Vec;
}

//  5. GetElementF. Constant Member Function.

template <typename T, typename P>
T VectorCL<T,P>::GetElementF(P i) const
{
    return *(m_Vec+i);
}

//  6. SetElementF.

template <typename T, typename P>
void VectorCL<T,P>::SetElementF(P iElem, const T& vElem)
{
    *(m_Vec+iElem) = vElem;
}

//  7. SetEqualF.

template <typename T, typename P>
void VectorCL<T,P>::SetEqualF(const VectorCL<T,P>& vec)
{
    P i;

    for (i = 0; i < m_Dim; i++) {
        *(m_Vec+i) = vec.GetElementF(i);
    }
}

//  8. SetEqualZeroF.

template <typename T, typename P>
void VectorCL<T,P>::SetEqualZeroF()
{
    P i;
    const T ZERO = 0.0;

    for (i = 0; i < m_Dim; i++) {
        *(m_Vec+i) = ZERO;
    }
}

//  9. SetEqualNumberF.

template <typename T, typename P>
void VectorCL<T,P>::SetEqualNumberF(const T& num)
{
    P i;

    for (i = 0; i < m_Dim; i++) {
        *(m_Vec+i) = num;
    }
}

// 10. AddF.

template <typename T, typename P>
void VectorCL<T,P>::AddF(const VectorCL<T,P>& vecA, const VectorCL<T,P>& vecB)
{
    P i;

    for (i = 0; i < m_Dim; i++) {
        *(m_Vec+i) = vecA.GetElementF(i) + vecB.GetElementF(i);
    }
}

// 11. SubtractF.

template <typename T, typename P>
void VectorCL<T,P>::SubtractF(const VectorCL<T,P>& vecA, const VectorCL<T,P>& vecB)
{
    P i;

    for (i = 0; i < m_Dim; i++) {
        *(m_Vec+i) = vecA.GetElementF(i) - vecB.GetElementF(i);
    }
}

// 12. TimesF.

template <typename T, typename P>
void VectorCL<T,P>::TimesF(const VectorCL<T,P>& vecA, const VectorCL<T,P>& vecB)
{
    P i;

    for (i = 0; i < m_Dim; i++) {
        *(m_Vec+i) = vecA.GetElementF(i) * vecB.GetElementF(i);
    }
}

// 13. DivideF.

template <typename T, typename P>
void VectorCL<T,P>::DivideF(const VectorCL<T,P>& vecA, const VectorCL<T,P>& vecB)
{
    P i;

    for (i = 0; i < m_Dim; i++) {
        *(m_Vec+i) = vecA.GetElementF(i) / vecB.GetElementF(i);
    }
}

// 13. NormalizeF.

template <typename T, typename P>
void VectorCL<T,P>::NormalizeF(const VectorCL<T,P>& vec)
{
    P i;
    T atmp;

    atmp = DotF(vec, vec);

    for (i = 0; i < m_Dim; i++) {
        *(m_Vec+i) = vec.GetElementF(i)/pow(atmp,0.5);
    }
}

// 14. ProjectF.

template <typename T, typename P>
void VectorCL<T,P>::ProjectF(const VectorCL<T,P>& vecA, const VectorCL<T,P>& vecB)
{
    P i;
    T atmp;
    T btmp;
    T ctmp;

    atmp = DotF(vecA, vecB);
    btmp = DotF(vecB, vecB);
    ctmp = atmp / btmp;

    for (i = 0; i < m_Dim; i++) {
        *(m_Vec+i) = ctmp * vecB.GetElementF(i);
    }
}

// 15. MGSF.

template <typename T, typename P>
void VectorCL<T,P>::MGSF(VectorCL<T,P>* vecArray, const VectorCL<T,P>& vec, P arrayDim, P vecDim)
{
    P i;

    VectorCL<T,P> vecTmpA;
    VectorCL<T,P> vecTmpB;

    vecTmpA.CreateF(vecDim);
    vecTmpB.CreateF(vecDim);

    vecTmpA.SetEqualF(vec);

    for (i = 0; i < arrayDim; i++) {
        vecTmpB.ProjectF(vecTmpA, vecArray[i]);
        vecTmpA.SubtractF(vecTmpA, vecTmpB);
        // this is needed to get back orthogonalized vectors
        // vecTmpA.Normalize(vecTmpA);
    }

    for (i = 0; i < m_Dim; i++) {
        *(m_Vec+i) = vecTmpA.GetElementF(i);
    }

    vecTmpA.DeleteF();
    vecTmpB.DeleteF();
}

// 16. GSF.

template <typename T, typename P>
void VectorCL<T,P>::GSF(VectorCL<T,P>* vecArray, const VectorCL<T,P>& vec, P arrayDim, P vecDim)
{
    P i;

    VectorCL<T,P> vecTmpA;
    VectorCL<T,P> vecTmpB;

    vecTmpA.CreateF(vecDim);
    vecTmpB.CreateF(vecDim);

    vecTmpB.SetEqualZeroF();

    for (i = 0; i < arrayDim; i++) {
        vecTmpA.ProjectF(vec, vecArray[i]);
        vecTmpB.AddF(vecTmpB, vecTmpA);
    }

    vecTmpA.SubtractF(vec, vecTmpB);

    for (i = 0; i < m_Dim; i++) {
        *(m_Vec+i) = vecTmpA.Element(i);
    }

    vecTmpA.DeleteF();
    vecTmpB.DeleteF();
}

// 17. NegationF.

template <typename T, typename P>
void VectorCL<T,P>::NegationF(const VectorCL<T,P>& vec)
{
    P i;

    for (i = 0; i < m_Dim; i++) {
        *(m_Vec+i) = (-1) * vec.GetElementF(i);
    }
}

// 18. InverseF.

template <typename T, typename P>
void VectorCL<T,P>::InverseF(const VectorCL<T,P>& vec)
{
    P i;

    for (i = 0; i < m_Dim; i++) {
        *(m_Vec+i) = 1.0 / vec.GetElementF(i);
    }
}

// 19. AbsF.

template <typename T, typename P>
void VectorCL<T,P>::AbsF(const VectorCL<T,P>& vec)
{
    P i;

    for (i = 0; i < m_Dim; i++) {
        *(m_Vec+i) = fabs(vec.GetElementF(i));
    }
}

//==============//
// End of code. //
//==============//
