//=======================================//
// Main function.                        //
// Driver program to the VectorCL class. //
//=======================================//

#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cmath>

#include "VectorCL.h"
#include "VectorCLMemberFunctions.h"
#include "VectorCLOverloadedOperators.h"
#include "VectorCLFriendFunctions.h"

#include "FunctionsFA.h"

#include "TypeDefinitions.h"

using namespace std;

int main()
{
    // local constants

    const TB  DIMVEC  = 1E2;
    const TB  TOTVEC  = 1E2;
    const TB  MAXTEST = 10;
    const TB  TRIALS  = 1E1;
    const TB  DIGITS  = 20;
    const TA  DLOW    = 0.0;
    const TA  DHIGH   = 1.0;
    const TA  THRES   = 1E-9;
    const TB  MYSEED  = 20;

    // local variables

    TB i, j, k;
    TA atmp, btmp, ctmp;
    TA dtmp, etmp;

    // array of objects

    VectorCL<TA, TB> *vectorArray;
    vectorArray = new VectorCL<TA, TB>[TOTVEC];

    // seeding the random generator

    srand(static_cast<unsigned>(MYSEED));

    // setting the precision of the outputs

    cout << setprecision(DIGITS) << fixed << endl;

    // main test loop

    for (i = 0; i < TRIALS; i++) {
        cout << "------------------------------------->>> " << i << endl;

        //   1. Create the vectors

        cout << "  01-1 ----> Create the vectors." << endl;
        for (j = 0; j < TOTVEC; j++) {
            vectorArray[j].CreateF(DIMVEC);
        }

        //   2. Initialize the vectors

        cout << "  02-1 ----> Initialize the vectors." << endl;
        for (k = 0; k < TOTVEC; k++) {
            for (j = 0; j < DIMVEC; j++) {
                atmp = random_number(DLOW, DHIGH);
                vectorArray[k].SetElementF(j, atmp);
            }
        }

        //  3. Reorthogonalize the vectors array

        cout << "  03-1 ----> Reorthogonalize the vectors." << endl;
        for (k = 0; k < TOTVEC-1; k++) {
            vectorArray[k+1].MGSF(vectorArray, vectorArray[k+1], k+1, DIMVEC);
        }

        for (k = 0; k < TOTVEC; k++) {
            vectorArray[k].NormalizeF(vectorArray[k]);
        }

        //  4. Dot product some orthogonalized vectors

        cout << "  04-1 ----> Dot products for some orthogonalized vectors." << endl;

        for(k = 0; k < MAXTEST; k++) {
            atmp = DotF(vectorArray[k], vectorArray[k+1]);
            cout << "  05-1 ----> Some output: " << pow(abs(atmp), 0.5) << endl;
            btmp = DotF(vectorArray[k], vectorArray[TOTVEC-1]);
            cout << "  05-2 ----> Some output: " << pow(abs(btmp), 0.5) << endl;
            ctmp = DotF(vectorArray[k], vectorArray[k]);
            cout << "  05-3 ----> Some output: " << pow(abs(ctmp), 0.5) << endl;
        }

        //  6. Test for orhogonality.

        cout << "  06-1 ----> Test for orthogonality. Threshold = " << THRES << endl;
        for(j = 0; j < TOTVEC; j++) {
            for(k = 0; k < j; k++) {
                atmp = DotF(vectorArray[j], vectorArray[k]);
                if (atmp > THRES) {
                    cout << atmp;
                    return 1;
                }
            }
        }

        cout << "  06-2 ----> Passed. " << endl;

        //  7. Use of overloaded '=' operator.

        cout << "  07-1 ----> Overloaded '=' operator." << endl;
        for (j = 0; j < TOTVEC; j++) {
            for (k = 0; k < TOTVEC; k++) {
                vectorArray[k] = vectorArray[j];
            }
        }

        cout << "  07-2 ----> Test of the oveloaded '=' operator." << endl;
        cout << "  07-3 ----> The result should be: 1" << endl;
        for(k = 0; k < MAXTEST; k++) {
            atmp = DotF(vectorArray[k], vectorArray[k+1]);
            cout << "  07-3 ----> Some output: " << pow(abs(atmp), 0.5) << endl;
        }

        //  8. Use of overloaded '+=' operator.

        cout << "  08-1 ----> Test the overloaded '+=' operator." << endl;
        cout << "BEFORE" << endl;
        cout << vectorArray[0].GetElementF(1) << endl;
        cout << vectorArray[1].GetElementF(1) << endl;
        cout << vectorArray[0].GetElementF(DIMVEC-1) << endl;
        cout << vectorArray[1].GetElementF(DIMVEC-1) << endl;

        vectorArray[1] += vectorArray[0];

        cout << "  08-2 ----> Test the overloaded '+=' operator." << endl;
        cout << "AFTER" << endl;
        cout << vectorArray[0].GetElementF(1) << endl;
        cout << vectorArray[1].GetElementF(1) << endl;
        cout << vectorArray[0].GetElementF(DIMVEC-1) << endl;
        cout << vectorArray[1].GetElementF(DIMVEC-1) << endl;

        //  9. Use of the TotalF, MaxF, MinF, MaxAbsF, MinAbsF functions.

        cout << "  09-1 ----> Use of the TotalF, MaxF, MinF, MaxAbsF, MinAbsF functions." << endl;

        for (k = 0; k < MAXTEST; k++) {
            cout << " 09-2 ----> TotalF  = " << TotalF(vectorArray[k])  << endl;
            cout << " 09-2 ----> MaxF    = " << MaxF(vectorArray[k])    << endl;
            cout << " 09-2 ----> MaxAbsF = " << MaxAbsF(vectorArray[k]) << endl;
            cout << " 09-2 ----> MinF    = " << MinF(vectorArray[k])    << endl;
            cout << " 09-2 ----> MinAbsF = " << MinAbsF(vectorArray[k]) << endl;
        }

        cout << " 10-1 ----> " << vectorArray[0].GetElementF(1) << endl;
        vectorArray[0].NegationF(vectorArray[0]);
        vectorArray[0].InverseF(vectorArray[0]);
        cout << " 10-2 ----> " << vectorArray[0].GetElementF(1) << endl;

        cout << " 10-3 ----> " << vectorArray[1].GetElementF(1) << endl;
        vectorArray[1].NegationF(vectorArray[1]);
        vectorArray[1].InverseF(vectorArray[1]);
        cout << " 10-4 ----> " << vectorArray[1].GetElementF(1) << endl;

        //  X. RAM is given back to system

        cout << endl;
        cout << "  XXXX ----> RAM is given back to system." << endl;
        cout << "  XXXX ----> Vectors deletion." << endl;
        cout << endl;

        for (k = 0; k < TOTVEC; k++) {
            vectorArray[k].DeleteF();
        }

    }

    return 0;

}

//==============//
// End of code. //
//==============//
