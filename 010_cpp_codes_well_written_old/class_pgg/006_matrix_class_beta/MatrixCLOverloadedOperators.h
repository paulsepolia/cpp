//=======================================================//
// Header file.                                          //
// MatrixCL class member functions overloaded operators. //
//=======================================================//

#include <iostream>

//  1. '=' operator.

template <typename T, typename P>
MatrixCL<T,P>& MatrixCL<T,P>::operator=(const MatrixCL<T,P>& mat)
{
    P i, j;

    if (this != &mat) {                     // 'this' points to the object
        // which calls the function operator=().
        for (i = 0; i < mat.m_DimA; i++) {
            for (j = 0; j < mat.m_DimB; j++) {
                m_Mat[i][j] = mat.GetElementF(i, j);
            }
        }
    }

    return *this;
}

//  2. '==' operator.

template <typename T, typename P>
bool MatrixCL<T,P>::operator==(const MatrixCL<T,P>& mat)
{
    P i, j;
    bool btmp;

    if (this == &mat) {
        return true;
    }

    if (this != &mat) {                     // 'this' points to the object
        // which calls the function operator=().
        for (i = 0; i < mat.m_DimA; i++) {
            for (j = 0; j < mat.m_DimB; j++) {
                btmp = (m_Mat[i][j] == mat.GetElementF(i, j));
                if (btmp == false) {
                    return false;
                }
            }
        }
    }

    return true;
}

//==============//
// End of code. //
//==============//
