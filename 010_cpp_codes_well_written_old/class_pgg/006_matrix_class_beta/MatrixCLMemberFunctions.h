//=============================================//
// Header File.                                //
// MatrixCL class member functions definition. //
//=============================================//

#include <iostream>

using namespace std;

//  1. Constructor definition.

template <typename T, typename P>
MatrixCL<T,P>::MatrixCL() {}

//  2. Destructor definition
//     This is an empty destructor,
//     since I delete manually the objects.

template <typename T, typename P>
MatrixCL<T,P>::~MatrixCL() {}

//  3. CreateF.

template <typename T, typename P>
void MatrixCL<T,P>::CreateF(P dimA, P dimB)
{
    m_DimA = dimA;
    m_DimB = dimB;
    P i;
    P j;

    m_Mat = new T* [dimA];

    for (i = 0; i < dimA; i++) {
        m_Mat[i] = new T [dimB];
    }
}

//  4. DeleteF.

template <typename T, typename P>
void MatrixCL<T,P>::DeleteF()
{
    P i;

    for (i = 0; i < m_DimA; i++) {
        delete [] m_Mat[i];
    }

    delete [] m_Mat;
}

//  5. SetElementF.

template <typename T, typename P>
void MatrixCL<T,P>::SetElementF(P i, P j, const T& mElem)
{
    m_Mat[i][j] = mElem;
}

//  6. GetElementF.

template <typename T, typename P>
T MatrixCL<T,P>::GetElementF(P i, P j) const
{
    T atmp;

    atmp = m_Mat[i][j];

    return atmp;
}

//  7. SetEqualF.

template <typename T, typename P>
void MatrixCL<T,P>::SetEqualF(const MatrixCL<T,P>& mat)
{
    P i;
    P j;

    for (i = 0; i < m_DimA; i++) {
        for (j = 0; j < m_DimB; j++) {
            m_Mat[i][j] = mat.GetElementF(i,j);
        }
    }
}

//  8. SetEqualZeroF.

template <typename T, typename P>
void MatrixCL<T,P>::SetEqualZeroF()
{
    P i;
    P j;

    const T ZERO = 0.0;

    for (i = 0; i < m_DimA; i++) {
        for (j = 0; j < m_DimB; j++) {
            m_Mat[i][j] = ZERO;
        }
    }
}

//  9. SetEqualNumberF.

template <typename T, typename P>
void MatrixCL<T,P>::SetEqualNumberF(const T& num)
{
    P i;
    P j;

    for (i = 0; i < m_DimA; i++) {
        for (j = 0; j < m_DimB; j++) {
            m_Mat[i][j] = num;
        }
    }
}

// 10. AddF.

template <typename T, typename P>
void MatrixCL<T,P>::AddF(const MatrixCL<T,P>& matA, const MatrixCL<T,P>& matB)
{
    P i;
    P j;

    P dimAA = matA.m_DimA;
    P dimAB = matA.m_DimB;
    P dimBA = matB.m_DimA;
    P dimBB = matB.m_DimB;

    if ( (dimAA != dimBA) || (dimAB != dimBB)) {
        system("PAUSE");
    }

    for (i = 0; i < dimAA; i++) {
        for (j = 0; j < dimAB; j++) {
            m_Mat[i][j] = matA.GetElementF(i,j) + matB.GetElementF(i,j);
        }
    }
}

// 11. SubtractF.

template <typename T, typename P>
void MatrixCL<T,P>::SubtractF(const MatrixCL<T,P>& matA,const MatrixCL<T,P>& matB)
{
    P i;
    P j;

    P dimAA = matA.m_DimA;
    P dimAB = matA.m_DimB;
    P dimBA = matB.m_DimA;
    P dimBB = matB.m_DimB;

    if ( (dimAA != dimBA) || (dimAB != dimBB)) {
        system("PAUSE");
    }

    for (i = 0; i < dimAA; i++) {
        for (j = 0; j < dimAB; j++) {
            m_Mat[i][j] = matA.GetElementF(i,j) - matB.GetElementF(i,j);
        }
    }
}

// 12. TimesF.

template <typename T, typename P>
void MatrixCL<T,P>::TimesF(const MatrixCL<T,P>& matA, const MatrixCL<T,P>& matB)
{
    P i;
    P j;

    P dimAA = matA.m_DimA;
    P dimAB = matA.m_DimB;
    P dimBA = matB.m_DimA;
    P dimBB = matB.m_DimB;

    if ( (dimAA != dimBA) || (dimAB != dimBB)) {
        system("PAUSE");
    }

    for (i = 0; i < dimAA; i++) {
        for (j = 0; j < dimAB; j++) {
            m_Mat[i][j] = matA.GetElementF(i,j) * matB.GetElementF(i,j);
        }
    }
}

// 13. DotF.

template <typename T, typename P>
void MatrixCL<T,P>::DotF(const MatrixCL<T,P>& matA, const MatrixCL<T,P>& matB)
{
    const T ZERO = 0.0;

    P i;
    P j;
    P k;

    P dimAA = matA.m_DimA;
    P dimBB = matB.m_DimB;
    P dimAB = matA.m_DimB;
    P dimBA = matB.m_DimA;

    T sumTmp = ZERO;

    if (dimAB != dimBA) {
        system("PAUSE");
    }

    for (i = 0; i < dimAA; i++) {
        for (j = 0; j < dimBB; j++) {
            sumTmp = ZERO;

            for (k = 0; k < dimAB; k++) {
                sumTmp = sumTmp + matA.GetElementF(i,k) * matB.GetElementF(j,k);
            }

            m_Mat[i][j] = sumTmp;

        }
    }
}

// 14. TotalF.

template <typename T, typename P>
T MatrixCL<T,P>::TotalF()
{
    const T ZERO = 0.0;
    P i, j, k;

    T sumTmp = ZERO;

    for (i = 0; i < m_DimA; i++) {
        for (j = 0; j < m_DimB; j++) {
            sumTmp = + sumTmp + m_Mat[i][j];
        }
    }

    return sumTmp;
}

//==============//
// End of code. //
//==============//
