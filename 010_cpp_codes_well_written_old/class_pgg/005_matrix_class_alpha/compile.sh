#!/bin/bash

icpc -O3 -xHost -static -static-intel -Bstatic  \
     -parallel -par-threshold0 -par-report1     \
     -w1                                        \
     DriverProgram_A.cpp -o x_matrix


