// Simple Boss
// Demonstrates inheritance

#include <iostream>
using namespace std;

//==========================//
// Enemy class declaration. //
//==========================//

class Enemy { // is the base class
public:
    int m_Damage; // public so as to be accessible from the derived class
    Enemy();
    void Attack() const;
};

Enemy::Enemy():
    m_Damage(10)
{
    cout << " ----> An Enemy class object has been born." << endl;
}

//=========================================//
// Enemy class member function definition. //
//=========================================//

void Enemy::Attack() const
{
    cout << " ----> Attack inflicts " << m_Damage << " damage points!" << endl;;
}

//=========================//
// Boss class declaration. //
//=========================//

class Boss: public Enemy {
public:
    int m_DamageMultiplier;
    Boss();
    void SpecialAttack() const;
};

//========================================//
// Boss class member function definition. //
//========================================//

Boss::Boss():
    m_DamageMultiplier(3)
{
    cout << " ----> An Boss class object has been born." << endl;
}

void Boss::SpecialAttack() const
{
    cout << " ----> Special Attack inflicts " << (m_DamageMultiplier * m_Damage);
    cout << " damage points!" << endl;
}

//================//
// main function. //
//================//

int main()
{
    cout << " ----> Creating an enemy." << endl;
    Enemy enemy1;
    enemy1.Attack();

    cout << " ----> Creating a boss." << endl;
    Boss boss1;
    boss1.Attack();
    boss1.SpecialAttack();

    cout << " ----> Creating an enemy." << endl;
    Enemy enemy2;
    enemy2.m_Damage = 200;

    enemy2.Attack();

    cout << " ----> Creating a boss." << endl;
    Boss boss2;
    boss2.m_Damage = 300;
    boss2.m_DamageMultiplier = 20;
    boss2.Attack();
    boss2.SpecialAttack();

    return 0;
}
//
