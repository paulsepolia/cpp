//====================================//
// Driver Program to IntPairCL class. //
//====================================//

#include <iostream>
#include <cstdlib>

#include "IntPairCL.h"
#include "IntPairCLMemberFunctions.h"

using namespace std;

int main()
{
    IntPairCL a(1,2);

    cout << "Postfix a++: Start value of object a: " << endl;
    cout << a.getFirst() << " " << a.getSecond() << endl;

    IntPairCL b = a++;

    cout << "Value returned: " << endl;
    cout << b.getFirst() << " " << b.getSecond() << endl;

    cout << "Changed object: " << endl;
    cout << a.getFirst() << " " << a.getSecond() << endl;

    a = IntPairCL(1,2);

    cout << "Prefix ++a: Start value of object a: " << endl;
    cout << a.getFirst() << " " << a.getSecond() << endl;

    IntPairCL c = a++;

    cout << "Value returned: " << endl;
    cout << c.getFirst() << " " << c.getSecond() << endl;

    cout << "Changed object: " << endl;
    cout << a.getFirst() << " " << a.getSecond() << endl;

    return 0;
}

//==============//
// End of code. //
//==============//
