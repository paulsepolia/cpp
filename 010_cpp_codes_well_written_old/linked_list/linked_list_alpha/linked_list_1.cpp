#include <iostream>
#include <string>
using namespace std;

struct TeleType {
    string name;
    string phoneNo;
    TeleType * nextaddr;
};

int main()
{
    TeleType t1 = { "Acme, Sam", "(555) 898-2392" };
    TeleType t2 = { "Dolan, Edith", "(555) 682-3104" };
    TeleType t3 = { "Lanfrank, John", "(555) 718-4581" };
    TeleType * first; // create a pointer to a structure

    first = &t1;        // store t1's   address in first
    t1.nextaddr = &t2;  // store t2's   address in t1.nextaddr
    t2.nextaddr = &t3;  // store t3's   address in t2.nextaddr
    t3.nextaddr = NULL; // store a NULL address in t3.nextaddr

    cout << endl << first -> name
         << endl << t1.nextaddr -> name
         << endl << t2.nextaddr -> name
         << endl;

    return 0;
}

