// array inside a struct

#include <iostream>

using namespace std;

const int ARRAY_SIZE = 330000000;
const int iLim = 1;
const int kLim = 10;
const int jLim = 100;

struct listType {
    int listElem[ARRAY_SIZE];
    int listLength;
};

int main()
{

    listType * struct_a = new listType [kLim];
    int i;
    int j;
    int k;

    for ( j = 0; j < jLim; j++ ) {
        listType * struct_a = new listType [kLim];

        for ( k = 0; k < kLim; k++ ) {
            cout << " The array size is " << ARRAY_SIZE << endl;
            cout << " The contents of the array inside the struct are " << endl;

            for ( i = ARRAY_SIZE-iLim; i < ARRAY_SIZE; i++ ) {
                cout << i << " -- " << struct_a[1].listElem[i] << endl;
            }

            cout << " The contents of the array after initialization are " << endl;

            for ( i = 0; i < ARRAY_SIZE; i++ ) {
                struct_a[1].listElem[i] = 0;
            }

            for ( i = ARRAY_SIZE-iLim; i < ARRAY_SIZE; i++ ) {
                cout << i << " -- " << struct_a[1].listElem[i] << endl;
            }

            for ( i = 0; i < ARRAY_SIZE; i++ ) {
                struct_a[1].listElem[i] = i ;
            }

            for ( i = ARRAY_SIZE-iLim; i < ARRAY_SIZE; i++ ) {
                cout << j << " -- " << k << " -- "
                     << i << " -- " << struct_a[1].listElem[i] << endl;
            }
        }

        delete [] struct_a ;
    }

    return 0;
}



