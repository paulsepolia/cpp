// Program to demonstrate
// the recursive function
// for binary search.

#include <iostream>
using std::cin;
using std::cout;
using std::endl;

const long ARRAY_SIZE = 3 * 1E8;
const long NUM_DO = 1E8;

// 1. Function declaration.

void search(const long* a, long first, long last, long key, bool& found, long& location);

// Precondition: a[first] through a[last] are sorted in increasing order.

// Postcondition: If key is not one of the values a[first] through a[last]
// then found == false; otherwise, a[location] == key and found == true.

// 2. The main function.

int main()
{
    long* a;
    a = new long [ARRAY_SIZE];
    const long finalIndex = ARRAY_SIZE - 1;

    // create and sort the array --> starts here.
    long i;
    for (i = 0; i < ARRAY_SIZE; i++) {
        a[i] = i;
    }
    // create and sort the array --> ends here.

    long key;
    long location;
    bool found;

    cout << "Enter number to be located: ";
    cin >> key;

    for (i = 0; i < NUM_DO; i++) {
        search(a, 0, finalIndex, key, found, location);
    }

    if (found) {
        cout << key << " is in index location "
             << location << endl;
    } else {
        cout << key << " is not in the array." << endl;
    }

    return 0;
}

// 3. Function declaration.

void search(const long* a, long first, long last, long key, bool& found, long& location)
{
    long mid;
    if (first > last) {
        found = false;
    } else {
        mid = (first + last) / 2;

        if (key == a[mid]) {
            found = true;
            location = mid;
        } else if (key < a[mid]) {
            search(a, first, mid - 1, key, found, location);
        } else if (key > a[mid]) {
            search(a, mid + 1, last, key, found, location);
        }
    }
}

//==============//
// End of code. //
//==============//
