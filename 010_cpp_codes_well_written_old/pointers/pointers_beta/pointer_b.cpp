
#include <iostream>

using namespace std;

int main()
{
    int *p; // declaration of the pointer p to int
    int x = 37;

    cout << " x = " << x << endl;

    p = &x; // definition of the ponter p to hold the address of x int.

    cout << " *p = " << *p << ", x = " << x << endl;

    *p = 58;

    cout << " *p = " << *p << ", x = " << x << endl;

    cout << " Address of p = " << &p << endl;

    cout << " Value of p = " << p << endl;

    cout << " Value of memory location "
         << " pointed to by *p = " << *p << endl;

    cout << " Address of x = " << &x << endl;

    cout << " Value of x = " << x << endl;

    return 0;
}

