#include <iostream>
#include <cmath>
#include <vector>
#include <list>

using std::cout;
using std::endl;
using std::pow;
using std::vector;
using std::list;

// type definitions

typedef unsigned long int uli;
typedef const uli culi;

// class A

class A {

public:

    // constructor

    A()
    {
        static uli index;
        index++;
        cout << " --> default constructor called --> " << index << endl;
    }

    // destructor

    virtual ~A()
    {
        static uli index;
        index++;
        cout << " --> destructor called --> " << index <<  endl;
    }

    // copy constructor

    A(const A & other)
    {
        static uli index;
        index++;
        cout << " --> copy constructor called --> " << index << endl;
    }

    // copy assignement operator

    A & operator=(const A & other)
    {
        static uli index;
        index++;
        cout << " --> copy assignment operator called" << index << endl;
        *this = other;
        return *this;
    }
};

// the main program

int main()
{
    culi DIMEN(static_cast<uli>(pow(10.0, 1.0)));

    //========//
    // vector //
    //========//

    cout << " --> 1" << endl;

    vector<A> vec1;

    cout << " --> 2" << endl;

    vec1.reserve(DIMEN);

    cout << " --> 3" << endl;

    for(uli i = 0; i != DIMEN; i++) {
        cout << "-------------------------------------------------->> " << i << endl;
        cout << " --> 4" << endl;
        A objA;
        cout << " --> 5" << endl;
        vec1.push_back(objA);
        cout << " --> 6" << endl;
    }

    cout << " --> 7" << endl;

    vec1.clear();

    cout << " --> 8" << endl;

    vec1.shrink_to_fit();

    cout << " --> 9" << endl;

    //======//
    // list //
    //======//

    cout << " --> 10" << endl;

    list<A> lis1;

    cout << " --> 11" << endl;

    lis1.resize(0);

    cout << " --> 12" << endl;

    for(uli i = 0; i != DIMEN; i++) {
        cout << "-------------------------------------------------->> " << i << endl;
        cout << " --> 13" << endl;
        A objA;
        cout << " --> 14" << endl;
        lis1.push_back(objA);
        cout << " --> 15" << endl;
    }

    cout << " --> 16" << endl;

    lis1.clear();

    cout << " --> 17" << endl;

    return 0;
}

// end
