#include <iostream>
#include <string>
#include <cstring>
#include <iomanip>
#include <cmath>

using std::cout;
using std::endl;
using std::string;
using std::strcpy;
using std::boolalpha;
using std::pow;

#include <unistd.h>

typedef unsigned long int uli;
typedef const unsigned long int culi;

// the main function

int main()
{
    // local settings and parameters

    cout << boolalpha;
    culi TRIALS(static_cast<uli>(pow(10.0, 4.0)));

    // # 1

    string s1("");

    cout << " s1.size()   = " << s1.size() << endl;
    cout << " s1.length() = " << s1.length() << endl;
    cout << " s1.empty() = " << s1.empty() << endl;
    cout << " s1.capacity() = " << s1.capacity() << endl;
    cout << " s1.max_size() = " << s1.max_size() << endl;

    // # 2 resize the string

    for(uli i = 0; i != TRIALS; i++) {
        cout << "-------------------------------------------------->> A -->> " << i << endl;
        s1 = s1 + "12";
        cout << " s1.size() = " << s1.size() << endl;
        cout << " s1.capacity() = " << s1.capacity() << endl;
        cout << "-------------------------------------------------->> B -->> " << i << endl;
    }

    cout << " s1.size()   = " << s1.size() << endl;
    cout << " s1.length() = " << s1.length() << endl;
    cout << " s1.empty() = " << s1.empty() << endl;
    cout << " s1.capacity() = " << s1.capacity() << endl;
    cout << " s1.max_size() = " << s1.max_size() << endl;

    s1.clear();
    cout << " s1.size()   = " << s1.size() << endl;
    cout << " s1.capacity() = " << s1.capacity() << endl;

    // # 3

    double pages_all(sysconf(_SC_PHYS_PAGES));
    double pages_free(sysconf(_SC_AVPHYS_PAGES));
    double pages_size(sysconf(_SC_PAGE_SIZE));

    double total_RAM = pages_all * pages_size;
    double free_RAM = pages_free * pages_size;

    cout << " --> pages_all(sysconf(_SC_PHYS_PAGES))     = " << pages_all << endl;
    cout << " --> pages_free(sysconf(_SC_AVPHYS_PAGES))  = " << pages_free << endl;
    cout << " --> pages_size(sysconf(_SC_PAGE_SIZE))     = " << pages_size << endl;
    cout << " --> total_RAM = " << total_RAM/(1024.0*1024*1024) << " GBytes"<< endl;
    cout << " --> free_RAM = " << free_RAM/(1024.0*1024*1024) << " GBytes"<< endl;

    return 0;
}

// end
