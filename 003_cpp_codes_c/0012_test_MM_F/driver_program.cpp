#include <iostream>
#include <cmath>
#include <memory>
#include <vector>
#include <iomanip>

using std::cout;
using std::endl;
using std::pow;
using std::vector;
using std::shared_ptr;
using std::fixed;
using std::setprecision;

// type definitions

typedef unsigned long int uli;
typedef const uli culi;

// the main program

int main()
{
    culi DIMEN(static_cast<uli>(pow(10.0, 1.0)));

    cout << fixed;
    cout << setprecision(10);

    vector<shared_ptr<double>> spv1;

    for(uli i = 0; i != DIMEN; i++) {
        double * pd(new double(i));
        shared_ptr<double> spd;
        spd.reset(pd);
        spv1.push_back(spd);
    }

    for(uli i = 0; i != DIMEN; i++) {
        cout << " -------------------------------------------------------------->> " << i << endl;
        cout << " -->  spv1[" << i << "]             = " << spv1[i] << endl;
        cout << " -->  spv1[" << i << "].get()       = " << spv1[i].get() << endl;
        cout << " -->  spv1[" << i << "].use_count() = " << spv1[i].use_count() << endl;
        cout << " --> *spv1[" << i << "].get()       = " << *spv1[i].get() << endl;
    }

    return 0;
}

// end
