#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::string;

int main()
{
    // # 1

    char * c1 = "a";

    cout << "  c1 = " <<  c1 << endl;
    cout << " *c1 = " << *c1 << endl;
    cout << " &c1 = " << &c1 << endl;

    // # 2

    char * c2 = "abcd";

    cout << "  c2 = " <<  c2 << endl;
    cout << " *c2 = " << *c2 << endl;
    cout << " &c2 = " << &c2 << endl;

    cout << " c2[0] = " << c2[0] << endl;
    cout << " c2[1] = " << c2[1] << endl;
    cout << " c2[2] = " << c2[2] << endl;
    cout << " c2[3] = " << c2[3] << endl;
    cout << " c2[4] = " << c2[4] << endl;
    //cout << " c2[4000] = " << c2[4000] << endl;

    // # 3

    char * c3("abcd");

    cout << "  c3 = " <<  c3 << endl;
    cout << " *c3 = " << *c3 << endl;
    cout << " &c3 = " << &c3 << endl;

    cout << " c3[0] = " << c3[0] << endl;
    cout << " c3[1] = " << c3[1] << endl;
    cout << " c3[2] = " << c3[2] << endl;
    cout << " c3[3] = " << c3[3] << endl;
    cout << " c3[4] = " << c3[4] << endl;
    //cout << " c3[4000] = " << c3[4000] << endl;

    // # 4

    // char * c4('k'); // this is an error

    //cout << "  c4 = " <<  c4 << endl;
    //cout << " *c4 = " << *c4 << endl;
    //cout << " &c4 = " << &c4 << endl;

    //cout << " c4[0] = " << c4[0] << endl;
    //cout << " c4[1] = " << c4[1] << endl;
    //cout << " c4[2] = " << c4[2] << endl;

    // # 5

    char c5('k');
    char c6('a');
    char c7('b');
    char c8('c');

    cout << "  c5 = " <<  c5 << endl;
    cout << "  c6 = " <<  c6 << endl;
    cout << "  c7 = " <<  c7 << endl;
    cout << "  c8 = " <<  c8 << endl;

    cout << "  &c5 = " <<  &c5 << endl;
    cout << "  &c6 = " <<  &c6 << endl;
    cout << "  &c7 = " <<  &c7 << endl;
    cout << "  &c8 = " <<  &c8 << endl;

    return 0;
}

// end
