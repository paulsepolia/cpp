#include <iostream>
#include <string>
#include <climits>
#include <unistd.h>

using std::cout;
using std::endl;

std::string getexepath()
{
    char result[PATH_MAX];
    ssize_t count = readlink("/proc/self/exe", result, PATH_MAX);
    return std::string(result, (count > 0) ? count : 0);
}

int main()
{
    cout << getexepath() << endl;
}
