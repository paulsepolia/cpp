#include <iostream>
#include <string>
#include <cstring>
#include <iomanip>

using std::cout;
using std::endl;
using std::string;
using std::strcpy;
using std::boolalpha;

int main()
{
    cout << boolalpha;

    // # 1

    string s1("a");

    const char * c1(s1.c_str());

    cout << "  c1 = " <<  c1 << endl;
    cout << " *c1 = " << *c1 << endl;
    cout << " &c1 = " << &c1 << endl;

    // # 2

    string s2("abcd");

    const char * c2(s2.c_str());

    cout << "  c2 = " <<  c2 << endl;
    cout << " *c2 = " << *c2 << endl;
    cout << " &c2 = " << &c2 << endl;

    cout << " c2[0] = " << c2[0] << endl;
    cout << " c2[1] = " << c2[1] << endl;
    cout << " c2[2] = " << c2[2] << endl;
    cout << " c2[3] = " << c2[3] << endl;
    cout << " c2[4] = " << c2[4] << endl;

    // # 3

    const char c5('k');
    const char c6('a');
    const char c7('b');
    const char c8('c');

    cout << "  c5 = " <<  c5 << endl;
    cout << "  c6 = " <<  c6 << endl;
    cout << "  c7 = " <<  c7 << endl;
    cout << "  c8 = " <<  c8 << endl;

    // # 4

    char * a1;
    a1 = const_cast<char*>(s2.c_str());

    cout << "  a1   = " <<  a1   << endl;
    cout << "  a1+1 = " <<  a1+1 << endl;
    cout << "  a1+2 = " <<  a1+2 << endl;

    cout << "  a1[0] = " <<  a1[0] << endl;
    cout << "  a1[1] = " <<  a1[1] << endl;
    cout << "  a1[2] = " <<  a1[2] << endl;

    // 5

    char * a2(new char [s2.length()+1]);
    strcpy(a2, s2.c_str());

    cout << "  a2   = " <<  a2   << endl;
    cout << "  a2+1 = " <<  a2+1 << endl;
    cout << "  a2+2 = " <<  a2+2 << endl;

    cout << "  a2[0] = " <<  a2[0] << endl;
    cout << "  a2[1] = " <<  a2[1] << endl;
    cout << "  a2[2] = " <<  a2[2] << endl;

    cout << " (a1 == a2) = " << (a1 == a2) << endl;

    // 6

    string s3("abcd");
    cout << " (s2 == s3) = " << (s2 == s3) << endl;

    cout << " s1[0] = " << s1[0] << endl;
    cout << " s2[0] = " << s2[0] << endl;
    cout << " s2[1] = " << s2[1] << endl;
    cout << " s2[2] = " << s2[2] << endl;
    cout << " s2[3] = " << s2[3] << endl;
    cout << " s2.length() = " << s2.length() << endl;
    cout << " s2.size() = " << s2.size() << endl;

    return 0;
}

// end
