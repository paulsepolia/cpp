//=======================//
// class Vec declaration //
//=======================//

#ifndef VEC_DECLARATION_H
#define VEC_DECLARATION_H

#include "parameters.hpp"
#include <memory>
#include <string>
#include <new>

using std::unique_ptr;
using std::string;
using std::bad_alloc;

template<typename T>
class Vec {
public:

    // construnctor

    Vec();

    // destructor

    virtual ~Vec();

    // copy constructor

    Vec(const Vec &);

    // move constructor

    Vec(Vec &&);

    // copy assignment operator

    Vec operator=(const Vec &);

    // move assignment operator

    Vec & operator=(Vec &&);

    // public  member functions

    void allocate(culi &);
    bool check_allocation() const;
    void deallocate();
    T get(culi &) const throw(string);
    void set(culi &, const T &) throw(string);
    T operator[](culi &);
    uli size() const;

private:

    // private member functions

    string throw_error_and_quit(string) const;

private:

    uli _size;
    unique_ptr<T> _p;
};

#endif // VEC_DECLARATION_H
