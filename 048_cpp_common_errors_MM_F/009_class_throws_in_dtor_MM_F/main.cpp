#include <iostream>

// BAD class throws exception from destructor
class Bomb
{
public:
    int x;

    Bomb() : x(0)
    {}

    ~Bomb()
    {
        throw std::exception();
    }
};

// Global variable that will throw exception when
// torn down during program exit
Bomb myBomb;

int main()
{
    std::cout << "Goodbye, World" << std::endl;

    // !!!
    // program blows up after return from main
    // !!!
}
