#include <iostream>

// This function throws exception
void function1() noexcept(false)
{
    std::cout << "-->> Entering into function 1" << std::endl;
    throw std::string("I was inside function1 when that happened!");
    std::cout << "-->> Exiting function 1" << std::endl;
}

// This function calls function 1
void function2() noexcept(false)
{
    std::cout << "-->> Entering into function 2" << std::endl;
    function1();
    std::cout << "-->> Exiting function 2" << std::endl;
}

// Function to call function2, and handle
// exception thrown by function1
void function3()
{
    std::cout << "-->> Entering function 3" << std::endl;
    try
    {
        function2(); // try to execute function 2
    }
    catch (const std::string & msg)
    {
        std::cout << "-->> Caught Exception: " << msg << std::endl;
    }

    std::cout << "-->> Exiting function 3" << std::endl;
}

int main()
{
    function3();
}