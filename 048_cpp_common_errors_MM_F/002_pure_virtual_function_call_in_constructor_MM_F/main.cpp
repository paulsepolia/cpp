#include <iostream>

class A
{
public:
    A()
    {
        std::cout << "in -->> 1" << std::endl;
        intermediate_call();
        std::cout << "in -->> 2" << std::endl;
    }

    void intermediate_call()
    {
        std::cout << "in -->> 3" << std::endl;
        // BAD: virtual function call during object construction
        virtual_function();
        std::cout << "in -->> 4" << std::endl;
    }

    virtual void virtual_function() = 0;

    virtual ~A() = default;
};

class B final : public A
{
public:
    // override virtual function in A
    void virtual_function() final
    {
        std::cout << "B::virtual_function called" << std::endl;
    }
};

int main()
{
    std::cout << "out --> 1" << std::endl;

    A *myObject;

    std::cout << "out --> 2" << std::endl;

    // Call to virtual_function during construction doesn't
    // behave like normal virtual function call.
    // Program will attempt to call pure virtual function
    // Since no definition exists, program will either fail
    // to link or fail catastrophically at run time.

    std::cout << "out --> 3" << std::endl;

    myObject = new B();

    std::cout << "out --> 4" << std::endl;

    delete myObject;

    std::cout << "out --> 5" << std::endl;
}
