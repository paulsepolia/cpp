#include <iostream>

// BAD class throws exception from constructor
class Bomb
{
public:
    int x;

    Bomb() : x(0)
    {
        throw std::exception();
    }

    ~Bomb() = default;
};

int main()
{
    try
    {
        Bomb b;
    }
    catch(...)
    {
        std::cout << "Something went wrong!" << std::endl;
    }

    std::cout << "Hello, world" << std::endl;
}