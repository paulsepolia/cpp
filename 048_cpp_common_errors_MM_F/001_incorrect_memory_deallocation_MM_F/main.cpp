#include <iostream>

// A class with non-trivial destructor

class NonTrivial
{
private:
    char *m_string;

public:
    // Constructor allocates m_string
    NonTrivial()
    {
        m_string = new char[10];
    }

    // Destructor frees m_string
    ~NonTrivial()
    {
        delete m_string;
    }

    // Copy constructor doesn't copy m_string field
    NonTrivial(const NonTrivial &source) : m_string(new char[10])
    {
        for (int i = 0; i < 10; i++)
        {
            m_string[i] = source.m_string[i];
        }
    }

    void set_char(size_t i)
    {
        m_string[i] = std::to_string(i)[0];
    }

    [[nodiscard]] char get_char(size_t i) const
    {
        return m_string[i];
    }

    // Assignment operator doesn't assign m_string field
    NonTrivial &operator=(const NonTrivial &source)
    {
        if (this == &source)
        {
            return *this;
        }

        for (int i = 0; i < 10; i++)
        {
            m_string[i] = source.m_string[i];
        }

        return *this;
    }
};

NonTrivial glob;

int main(int argc, char **argv)
{
    {
        std::cout << "------------------------------------->> 1" << std::endl;
        auto *p = new NonTrivial[10];

        p[0].set_char(2);
        p[1].set_char(3);
        p[2].set_char(4);

        std::cout << p[0].get_char(2) << std::endl;
        std::cout << p[1].get_char(3) << std::endl;
        std::cout << p[2].get_char(4) << std::endl;

        //delete p; // BAD: should use delete [] p;
        delete[] p; // that is the proper way to delete the p
    }

    {
        std::cout << "------------------------------------->> 2" << std::endl;

        auto *p = new NonTrivial(glob); // copy constructor

        p[0].set_char(2);
        p[0].set_char(3);
        p[0].set_char(4);

        std::cout << p->get_char(2) << std::endl;
        std::cout << p->get_char(3) << std::endl;
        std::cout << p->get_char(4) << std::endl;

        //free(p);  // BAD: should use delete p;
        delete p;
    }

    {
        std::cout << "------------------------------------->> 3" << std::endl;

        auto *p = new NonTrivial(); // copy constructor

        p[0].set_char(2);
        p[0].set_char(3);
        p[0].set_char(4);

        std::cout << p->get_char(2) << std::endl;
        std::cout << p->get_char(3) << std::endl;
        std::cout << p->get_char(4) << std::endl;

        //free(p);  // BAD: should use delete p;
        delete p;
    }
}