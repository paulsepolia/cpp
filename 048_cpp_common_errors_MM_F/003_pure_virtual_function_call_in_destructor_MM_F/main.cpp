#include <iostream>

class A
{
public:
    A() = default;

    void intermediate_call()
    {
        std::cout << "in --> 3" << std::endl;
        // BAD: virtual function call during object destruction
        virtual_function();
        std::cout << "in --> 4" << std::endl;
    }

    virtual void virtual_function() = 0;

    virtual ~A()
    {
        std::cout << "in --> 1" << std::endl;
        intermediate_call();
        std::cout << "in --> 2" << std::endl;
    }
};

class B final : public A
{
public:
    // override virtual function in A
    void virtual_function() final
    {
        std::cout << "B::virtual_function called" << std::endl;
    }
};

int main()
{
    std::cout << "out --> 1" << std::endl;
    B myObject;
    std::cout << "out --> 2" << std::endl;

    // This call behaves like a normal virtual function call.
    // Print statement shows it invokes B::virtual_function.
    myObject.virtual_function();
    std::cout << "out --> 3" << std::endl;

    // Call to virtual_function during destruction doesn't
    // behave like normal virtual function call.
    // Tries to invoke A::virtual_function even though we
    // are destroying an instance of B, not A.  Since this
    // doesn't exist the program fails catastrophically.
}