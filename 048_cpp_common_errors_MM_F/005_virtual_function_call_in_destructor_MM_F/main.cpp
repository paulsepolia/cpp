#include <iostream>

class A
{
public:

    A() = default;

    void intermediate_call()
    {
        // BAD: virtual function call during object destruction
        virtual_function();
    }

    virtual void virtual_function()
    {
        std::cout << "A::virtual_function called" << std::endl;
    }

    virtual ~A()
    {
        intermediate_call();
    }
};

class B : public A
{
public:
    // override virtual function in A
    void virtual_function() final
    {
        std::cout << "B::virtual_function called" << std::endl;
    }
};

int main()
{
    B myObject;

    // This call behaves like a normal virtual function call.
    // Print statement shows it invokes B::virtual_function.
    myObject.virtual_function();

    // Call to virtual_function during destruction doesn't
    // behave like normal virtual function call.
    // Print statement shows it invokes A::virtual_function,
    // even though we are destroying an instance of B, not A.
}