#include <iostream>

// BAD class throws exception from constructor
class Bomb
{
public:
    int x;

    Bomb() : x(0)
    {
        throw std::exception();
    }

    ~Bomb() = default;
};

// Variable declaration that will throw exception
// during static elaboration before main is called
Bomb myBomb;

int main()
{
    std::cout << "Hello, world" << std::endl;
}
