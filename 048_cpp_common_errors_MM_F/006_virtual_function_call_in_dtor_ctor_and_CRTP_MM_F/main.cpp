#include <iostream>

template<typename T>
class A
{
public:

    A()
    {
        std::cout << "--> A() --> 1" << std::endl;
        intermediate_call();
        std::cout << "--> A() --> 2" << std::endl;
    }

    void intermediate_call()
    {
        // Using CRTP to be able to get proper virtual function calls
        // in constructor and destructor
        std::cout << "--> A() --> 3" << std::endl;
        static_cast<T *>(this)->virtual_function();
        std::cout << "--> A() --> 4" << std::endl;
    }

    virtual void virtual_function()
    {
        std::cout << "A::virtual_function called" << std::endl;
    }

    virtual ~A()
    {
        std::cout << "--> ~A() --> 1" << std::endl;
        intermediate_call();
        std::cout << "--> ~A() --> 2" << std::endl;
    }
};

class B : public A<B>
{
public:
    void virtual_function() final
    {
        std::cout << "B::virtual_function called" << std::endl;
    }
};

int main()
{
    {
        std::cout << "------------------------------------->> 1" << std::endl;
        std::cout << "out --> 1" << std::endl;
        B myObject;
        std::cout << "out --> 2" << std::endl;
        myObject.virtual_function();
        std::cout << "out --> 3" << std::endl;
    }

    {
        std::cout << "------------------------------------->> 2" << std::endl;
        std::cout << "out --> 1" << std::endl;
        A<B>* p = new B();
        std::cout << "out --> 2" << std::endl;
        p->virtual_function();
        std::cout << "out --> 3" << std::endl;
        delete p;
        std::cout << "out --> 4" << std::endl;
    }

    {
        std::cout << "------------------------------------->> 3" << std::endl;
        std::cout << "out --> 1" << std::endl;
        A<B> a;
        std::cout << "out --> 2" << std::endl;
        a.virtual_function();
        std::cout << "out --> 3" << std::endl;
    }
}