#include "NanosecondsClockSyntonized.h"

#if _WIN32 || _WIN64

#include <Windows.h>

#elif __linux__

#include <ctime>

#else // generic Unix

#include <sys/time.h>

#endif

auto
NanosecondsClockSyntonized::GetNanosecondsSinceFirstCall()
-> int64_t {

#if _WIN32 || _WIN64

    auto timePointNow{(int64_t) 0LL};
    LARGE_INTEGER dt;
    LPFILETIME ft{new FILETIME};

    GetSystemTimePreciseAsFileTime(ft);
    dt.LowPart = ft->dwLowDateTime;
    dt.HighPart = ft->dwHighDateTime;

    timePointNow = (dt.QuadPart - OA_ZERO_TICKS);
    delete ft;

    // call it only once
    auto flg{onlyOnce.load(std::memory_order_acquire)};
    if (!flg) {
        std::lock_guard<std::mutex> lck(mtx);
        flg = onlyOnce.load(std::memory_order_relaxed);
        if (!flg) {
            SetNanosecondsSinceWindowsEpochAtFirstCall(timePointNow);
            onlyOnce.store(true, std::memory_order_release);
        }
    }

    return timePointNow * H1 - nanosecondsSinceWindowsEpochAtFirstCall;

#elif __linux__

    auto timePointNow{(int64_t) 0LL};
    struct timespec ts{};
    const auto clockMonotonicStatus{(int) clock_gettime(CLOCK_MONOTONIC, &ts)};

    if (clockMonotonicStatus == 0) {

        clock_gettime(CLOCK_MONOTONIC, &ts);
        timePointNow = B1 * (int64_t) ts.tv_sec + (int64_t) ts.tv_nsec;

        // call it only once
        auto flg{onlyOnce.load(std::memory_order_acquire)};
        if (!flg) {
            std::lock_guard<std::mutex> lck(mtx);
            flg = onlyOnce.load(std::memory_order_relaxed);
            if (!flg) {
                SetNanosecondsSinceUnixEpochAtFirstCallLinuxMonotonic(timePointNow);
                onlyOnce.store(true, std::memory_order_release);
            }
        }

    } else {

        clock_gettime(CLOCK_REALTIME, &ts);
        timePointNow = B1 * (int64_t) ts.tv_sec + (int64_t) ts.tv_nsec;

        // call it only once
        auto flg{onlyOnce.load(std::memory_order_acquire)};
        if (!flg) {
            std::lock_guard<std::mutex> lck(mtx);
            flg = onlyOnce.load(std::memory_order_relaxed);
            if (!flg) {
                SetNanosecondsSinceUnixEpochAtFirstCallLinuxRealtime(timePointNow);
                onlyOnce.store(true, std::memory_order_release);
            }
        }
    }

    return (timePointNow - beginOfTimeInNanoseconds);

#else // generic Unix

    auto timePointNow{ (int64_t)0LL };
    struct timeval tv {};
    gettimeofday(&tv, nullptr);
    timePointNow = B1 * (int64_t)tv.tv_sec + K1 * (int64_t)tv.tv_usec;

    // call it only once
    auto flg{ onlyOnce.load(std::memory_order_acquire) };
    if (!flg) {
        std::lock_guard<std::mutex> lck(mtx);
        flg = onlyOnce.load(std::memory_order_relaxed);
        if (!flg) {
            SetNanosecondsSinceUnixEpochAtFirstCallUnix(timePointNow);
            onlyOnce.store(true, std::memory_order_release);
        }
    }

    return (timePointNow - beginOfTimeInNanoseconds);
#endif
}

auto
NanosecondsClockSyntonized::GetNanosecondsSinceFirstCallTimePoint()
-> std::chrono::duration<int64_t, std::nano> {
    return std::chrono::nanoseconds(GetNanosecondsSinceFirstCall());
}

auto
NanosecondsClockSyntonized::GetBeginOfTimeInNanoseconds()
-> int64_t {
    return beginOfTimeInNanoseconds;
}

auto
NanosecondsClockSyntonized::GetBeginOfTimeInNanosecondsTimePoint()
-> std::chrono::duration<int64_t, std::nano> {
    return std::chrono::nanoseconds(beginOfTimeInNanoseconds);
}

auto NanosecondsClockSyntonized::GetNanosecondsSinceUnixEpochAtFirstCall()
-> int64_t {
    return nanosecondsSinceUnixEpochAtFirstCall;
}

auto
NanosecondsClockSyntonized::GetNanosecondsSinceUnixEpochAtFirstCallTimePoint()
-> std::chrono::duration<int64_t, std::nano> {
    return std::chrono::nanoseconds(nanosecondsSinceUnixEpochAtFirstCall);
}

auto NanosecondsClockSyntonized::GetNanosecondsSinceUnixEpochSyntonized()
-> int64_t {
    return nanosecondsSinceUnixEpochAtFirstCall + GetNanosecondsSinceFirstCall();
}

auto
NanosecondsClockSyntonized::GetNanosecondsSinceUnixEpochSyntonizedTimePoint()
-> std::chrono::duration<int64_t, std::nano> {
    return std::chrono::nanoseconds(
            nanosecondsSinceUnixEpochAtFirstCall + GetNanosecondsSinceFirstCall()
    );
}

auto NanosecondsClockSyntonized::GetNanosecondsSinceSystemEpochSyntonized()
-> int64_t {
    return beginOfTimeInNanoseconds + GetNanosecondsSinceFirstCall();
}

auto
NanosecondsClockSyntonized::GetNanosecondsSinceSystemEpochSyntonizedTimePoint()
-> std::chrono::duration<int64_t, std::nano> {
    return std::chrono::nanoseconds(
            beginOfTimeInNanoseconds + GetNanosecondsSinceFirstCall()
    );
}

#if _WIN32 || _WIN64

auto
NanosecondsClockSyntonized::SetNanosecondsSinceWindowsEpochAtFirstCall(int64_t &timePointNow)
-> void {

   LARGE_INTEGER dt;
   LPFILETIME ft{new FILETIME};

   GetSystemTimePreciseAsFileTime(ft);
   dt.LowPart = ft->dwLowDateTime;
   dt.HighPart = ft->dwHighDateTime;

   timePointNow = (dt.QuadPart - OA_ZERO_TICKS);

   beginOfTimeInNanoseconds = H1 * timePointNow;
   nanosecondsSinceWindowsEpochAtFirstCall = beginOfTimeInNanoseconds;
   nanosecondsSinceUnixEpochAtFirstCall =
           std::chrono::duration_cast<std::chrono::nanoseconds>(
                   std::chrono::system_clock::now().time_since_epoch()).count();
}

#elif __linux__

auto
NanosecondsClockSyntonized::SetNanosecondsSinceUnixEpochAtFirstCallLinuxMonotonic(int64_t &timePointNow)
-> void {

    struct timespec ts{};
    clock_gettime(CLOCK_MONOTONIC, &ts);

    timePointNow = B1 * (int64_t) ts.tv_sec + (int64_t) ts.tv_nsec;
    beginOfTimeInNanoseconds = timePointNow;
    nanosecondsSinceUnixEpochAtFirstCall =
            std::chrono::duration_cast<std::chrono::nanoseconds>(
                    std::chrono::system_clock::now().time_since_epoch()).count();
}

auto
NanosecondsClockSyntonized::SetNanosecondsSinceUnixEpochAtFirstCallLinuxRealtime(int64_t &timePointNow)
-> void {

    struct timespec ts{};
    clock_gettime(CLOCK_REALTIME, &ts);

    timePointNow = B1 * (int64_t) ts.tv_sec + (int64_t) ts.tv_nsec;
    beginOfTimeInNanoseconds = timePointNow;
    nanosecondsSinceUnixEpochAtFirstCall = timePointNow;
}

#else

auto
NanosecondsClockSyntonized::SetNanosecondsSinceUnixEpochAtFirstCallUnix(int64_t& timePointNow)
-> void {

   struct timeval tv {};
   gettimeofday(&tv, nullptr);

   timePointNow = B1 * (int64_t)tv.tv_sec + K1 * (int64_t)tv.tv_usec;
   nanosecondsSinceUnixEpochAtFirstCall = timePointNow;
   beginOfTimeInNanoseconds = timePointNow;
}
#endif

int64_t NanosecondsClockSyntonized::beginOfTimeInNanoseconds = 0;
int64_t NanosecondsClockSyntonized::nanosecondsSinceUnixEpochAtFirstCall = 0;
int64_t NanosecondsClockSyntonized::nanosecondsSinceWindowsEpochAtFirstCall = 0;
std::atomic<bool> NanosecondsClockSyntonized::onlyOnce = false;
std::mutex NanosecondsClockSyntonized::mtx;
