#pragma once

#include <mutex>
#include <chrono>
#include <atomic>

class NanosecondsClockSyntonized {

private:

    static constexpr auto H1{(int64_t) 100LL};
    static constexpr auto K1{(int64_t) 1'000LL};
    static constexpr auto B1{(int64_t) 1'000'000'000LL};
    static constexpr auto OA_ZERO_TICKS{(int64_t) 94'353'120'000'000'000LL};

    static int64_t beginOfTimeInNanoseconds;
    static int64_t nanosecondsSinceUnixEpochAtFirstCall;
    static int64_t nanosecondsSinceWindowsEpochAtFirstCall;
    static std::atomic<bool> onlyOnce;
    static std::mutex mtx;

public:

    NanosecondsClockSyntonized() = delete;

    NanosecondsClockSyntonized(const NanosecondsClockSyntonized &) = delete;

    NanosecondsClockSyntonized(NanosecondsClockSyntonized &&) = delete;

    NanosecondsClockSyntonized &operator=(const NanosecondsClockSyntonized &) = delete;

    NanosecondsClockSyntonized &operator=(NanosecondsClockSyntonized &&) = delete;

public:

    /**
     * A member function to give back the timestamp in nanoseconds
     * since the time of its first call.
     * We use that function to measure time intervals in nanoseconds.
     * Those time intervals are syntonized with the system clock.
     * We can also add the current time stamp: GetNanosecondsSinceFirstCall() with the
     * GetBeginOfTimeInNanoseconds() to get the "now" timestamp,
     * but the actual value differs based on which clock is used and which OS.
     */
    static auto
    GetNanosecondsSinceFirstCall() -> int64_t;

    /**
     * Returns what "GetNanosecondsSinceFirstCall()" does
     * but in std::chrono::duration<int64_t, std::nano>
     */
    static auto
    GetNanosecondsSinceFirstCallTimePoint() -> std::chrono::duration<int64_t, std::nano>;

    /**
     * Gives back the beginning of time in nanoseconds.
     * That time varies depending which clock is being and which OS.
     * For example under Linux could be the boot time of the machine if the
     * MONOTONIC_CLOCK is available or the Unix Epoch UTC time
     * if the MONOTONIC_CLOCK is not available.
     * On Windows OS is always the time since 12/30/1899 12:00am UTC.
     * On a Unix system is the Unix Epoch UTC time.
     * The exact value is captured when we call for the first time the
     * function: GetNanosecondsSinceFirstCall()
     * If we have not call yet the above function then is the value is always zero.
     */
    static auto
    GetBeginOfTimeInNanoseconds() -> int64_t;

    /**
     * Returns what "GetBeginOfTimeInNanoseconds()" does
     * but in std::chrono::duration<int64_t, std::nano>
     */
    static auto
    GetBeginOfTimeInNanosecondsTimePoint() -> std::chrono::duration<int64_t, std::nano>;

    /**
     * Gives back the nanoseconds since Unix Epoch
     * at the time of first call of the function GetNanosecondsSinceFirstCall().
     * The function used is a std::chrono function, so a system time.
     */
    static auto
    GetNanosecondsSinceUnixEpochAtFirstCall() -> int64_t;

    /**
    * Returns what "GetNanosecondsSinceUnixEpochAtFirstCall()" does
    * but in std::chrono::duration<int64_t, std::nano>
    */
    static auto
    GetNanosecondsSinceUnixEpochAtFirstCallTimePoint() -> std::chrono::duration<int64_t, std::nano>;

    /**
     * Gives back the nanoseconds since Unix Epoch "now".
     * It is syntonized with system clock.
     */
    static auto
    GetNanosecondsSinceUnixEpochSyntonized() -> int64_t;

    /**
    * Returns what "GetNanosecondsSinceUnixEpochSyntonized()" does
    * but in std::chrono::duration<int64_t, std::nano>
    */
    static auto
    GetNanosecondsSinceUnixEpochSyntonizedTimePoint() -> std::chrono::duration<int64_t, std::nano>;

    /**
     * Gives back the nanoseconds since system epoch "now".
     * So, on Linux and with CLOCK_MONOTONIC is the time passed since boot time.
     * On Linux and with CLOCK_REALTIME is the time passed since Unix Epoch.
     * On Windows is the time passed since Windows Epoch (1899/12/30 midnight, UTC).
     * It is syntonized with system clock.
     */
    static auto
    GetNanosecondsSinceSystemEpochSyntonized() -> int64_t;

    /**
    * Returns what "GetNanosecondsSinceSystemEpochSyntonized()" does
    * but in std::chrono::duration<int64_t, std::nano>
    */
    static auto
    GetNanosecondsSinceSystemEpochSyntonizedTimePoint() -> std::chrono::duration<int64_t, std::nano>;

private:

#if _WIN32 || _WIN64

    /**
     * Sets nanoseconds since Windows Epoch "now".
     */
    static auto
    SetNanosecondsSinceWindowsEpochAtFirstCall(int64_t &timePointNow) -> void;

#elif __linux__

    /**
     * Sets nanoseconds since boot time "now" using CLOCK_MONOTONIC.
     */
    static auto
    SetNanosecondsSinceUnixEpochAtFirstCallLinuxMonotonic(int64_t &timePointNow) -> void;

    /**
     * Sets nanoseconds since Unix Epoch "now" using CLOCK_REALTIME.
     */
    static auto
    SetNanosecondsSinceUnixEpochAtFirstCallLinuxRealtime(int64_t &timePointNow) -> void;

#else
    /**
     * Sets nanoseconds since Unix Epoch "now" using Unix clock.
     */
    static auto
    SetNanosecondsSinceUnixEpochAtFirstCallUnix(int64_t& timePointNow) -> void;

#endif
};
