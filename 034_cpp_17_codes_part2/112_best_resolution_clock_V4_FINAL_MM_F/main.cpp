#include "NanosecondsClockSyntonized.h"
#include <iostream>
#include <thread>
#include <vector>
#include <algorithm>

auto main() -> int {

    constexpr auto NUM_THREADS{(size_t) 4};
    constexpr auto DIM_MAX{(size_t) 10'000'000};

    {
        std::cout << std::endl;
        std::cout << "-->> test 1 --> NanosecondsClockSyntonized::GetNanosecondsSinceFirstCall()" << std::endl;
        std::cout << std::endl;

        std::mutex mtx{};

        auto ld{[&]() {

            mtx.lock();
            std::cout << "-->> thread id = " << std::this_thread::get_id() << std::endl;
            mtx.unlock();

            auto a{std::vector<int64_t>(DIM_MAX)};

            for (size_t i = 0; i < DIM_MAX; i++) {
                a[i] = NanosecondsClockSyntonized::GetNanosecondsSinceFirstCall();
            }

            auto ave{(double) 0.0};

            for (size_t i = 1; i < DIM_MAX; i++) {
                ave += (double) (a[i] - a[i - 1]);
            }

            mtx.lock();
            std::cout << "-->> average time between consecutive calls is (ns) = "
                      << (int64_t) (ave / (DIM_MAX - 1)) << std::endl;
            mtx.unlock();
        }
        };

        auto vecTh{std::vector<std::thread>(NUM_THREADS)};

        for (auto &el : vecTh) {
            el = std::thread(ld);
        }

        for (auto &el : vecTh) {
            el.join();
        }
    }

    {
        std::cout << std::endl;
        std::cout << "-->> test 2 --> NanosecondsClockSyntonized::BeginOfTimeInNanoseconds()" << std::endl;
        std::cout << std::endl;

        std::mutex mtx{};

        auto ld{[&]() {

            mtx.lock();

            std::cout << "-->> thread id = " << std::this_thread::get_id() << std::endl;

            mtx.unlock();

            auto a{std::vector<int64_t>(DIM_MAX)};

            for (size_t i = 0; i < DIM_MAX; i++) {
                a[i] = NanosecondsClockSyntonized::GetBeginOfTimeInNanoseconds();
            }

            std::sort(a.begin(), a.end());
            const auto it{std::unique(a.begin(), a.end())};
            a.resize(std::distance(a.begin(), it));

            mtx.lock();
            std::cout << "-->> the value should be exact one (1) = " << a.size() << std::endl;
            mtx.unlock();
        }
        };

        auto vecTh{std::vector<std::thread>(NUM_THREADS)};

        for (auto &el : vecTh) {
            el = std::thread(ld);
        }

        for (auto &el : vecTh) {
            el.join();
        }
    }

    {
        std::cout << std::endl;
        std::cout << "-->> test 3 --> NanosecondsClockSyntonized::GetNanosecondsSinceUnixEpochAtFirstCall()"
                  << std::endl;
        std::cout << std::endl;

        std::cout << NanosecondsClockSyntonized::GetNanosecondsSinceUnixEpochAtFirstCall() << std::endl;
    }

    {
        std::cout << std::endl;
        std::cout << "-->> test 4 --> NanosecondsClockSyntonized::GetNanosecondsSinceUnixEpochSyntonized()"
                  << std::endl;
        std::cout << std::endl;

        std::cout << NanosecondsClockSyntonized::GetNanosecondsSinceUnixEpochSyntonized() << std::endl;
    }

    {
        std::cout << std::endl;
        std::cout << "-->> test 5 --> NanosecondsClockSyntonized::GetNanosecondsSinceSystemEpochSyntonized()"
                  << std::endl;
        std::cout << std::endl;

        std::cout << NanosecondsClockSyntonized::GetNanosecondsSinceSystemEpochSyntonized() << std::endl;
    }

    {
        std::cout << std::endl;
        std::cout << "-->> test 6 --> NanosecondsClockSyntonized::GetNanosecondsSinceFirstCall()" << std::endl;
        std::cout << std::endl;

        std::cout << NanosecondsClockSyntonized::GetNanosecondsSinceFirstCall() << std::endl;

        std::this_thread::sleep_for(std::chrono::seconds(1));

        std::cout << NanosecondsClockSyntonized::GetNanosecondsSinceFirstCall() << std::endl;

        std::this_thread::sleep_for(std::chrono::seconds(1));

        std::cout << NanosecondsClockSyntonized::GetNanosecondsSinceFirstCall() << std::endl;
    }

    {
        std::cout << std::endl;
        std::cout << "-->> test 7 --> NanosecondsClockSyntonized::GetNanosecondsSinceUnixEpochAtFirstCallTimePoint()"
                  << std::endl;
        std::cout << "-->>        --> NanosecondsClockSyntonized::GetNanosecondsSinceUnixEpochAtFirstCall()"
                  << std::endl;
        std::cout << std::endl;

        std::cout << NanosecondsClockSyntonized::GetNanosecondsSinceUnixEpochAtFirstCallTimePoint().count()
                  << std::endl;
        std::cout << NanosecondsClockSyntonized::GetNanosecondsSinceUnixEpochAtFirstCall() << std::endl;
    }

    {
        std::cout << std::endl;
        std::cout
                << "-->> test 8 --> NanosecondsClockSyntonized::GetNanosecondsSinceSystemEpochSyntonizedTimePoint().count()"
                << std::endl;
        std::cout << "-->>        --> NanosecondsClockSyntonized::GetNanosecondsSinceSystemEpochSyntonized()"
                  << std::endl;
        std::cout << std::endl;

        std::cout << NanosecondsClockSyntonized::GetNanosecondsSinceSystemEpochSyntonizedTimePoint().count()
                  << std::endl;
        std::cout << NanosecondsClockSyntonized::GetNanosecondsSinceSystemEpochSyntonized() << std::endl;
    }

    {
        std::cout << std::endl;
        std::cout << "-->> test 9 --> NanosecondsClockSyntonized::GetNanosecondsSinceFirstCallTimePoint()" << std::endl;
        std::cout << "-->>        --> NanosecondsClockSyntonized::GetNanosecondsSinceFirstCall()" << std::endl;
        std::cout << std::endl;

        std::cout << NanosecondsClockSyntonized::GetNanosecondsSinceFirstCallTimePoint().count() << std::endl;
        std::cout << NanosecondsClockSyntonized::GetNanosecondsSinceFirstCall() << std::endl;
    }

    {
        std::cout << std::endl;
        std::cout << "-->> test10 --> NanosecondsClockSyntonized::GetBeginOfTimeInNanosecondsTimePoint()" << std::endl;
        std::cout << "-->>        --> NanosecondsClockSyntonized::GetBeginOfTimeInNanoseconds()" << std::endl;
        std::cout << std::endl;

        std::cout << NanosecondsClockSyntonized::GetBeginOfTimeInNanosecondsTimePoint().count() << std::endl;
        std::cout << NanosecondsClockSyntonized::GetBeginOfTimeInNanoseconds() << std::endl;
    }

    {
        std::cout << std::endl;
        std::cout << "-->> test11 --> NanosecondsClockSyntonized::GetNanosecondsSinceUnixEpochSyntonizedTimePoint()"
                  << std::endl;
        std::cout << "-->>        --> NanosecondsClockSyntonized::GetNanosecondsSinceUnixEpochSyntonized()"
                  << std::endl;
        std::cout << std::endl;

        std::cout << NanosecondsClockSyntonized::GetNanosecondsSinceUnixEpochSyntonizedTimePoint().count() << std::endl;
        std::cout << NanosecondsClockSyntonized::GetNanosecondsSinceUnixEpochSyntonized() << std::endl;
    }
}