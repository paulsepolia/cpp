#pragma once

#include "IClockSyntonized.h"

class ClockSyntonized final : public IClockSyntonized{

private:

    static constexpr auto H1{(int64_t) 100LL};
    static constexpr auto K1{(int64_t) 1'000LL};
    static constexpr auto B1{(int64_t) 1'000'000'000LL};
    static constexpr auto OA_ZERO_TICKS{(int64_t) 94'353'120'000'000'000LL};

    int64_t nanosecondsSinceSystemEpochAtFirstCall{-1};
    int64_t nanosecondsSinceUnixEpochAtFirstCall{-1};
    int clockMonotonicStatus{-1};

    ClockSyntonized();

    ~ClockSyntonized() = default;

public:

    static ClockSyntonized &GetInstance() {
        static ClockSyntonized instance;
        return instance;
    }

    ClockSyntonized(const ClockSyntonized &) = delete;

    ClockSyntonized(ClockSyntonized &&) = delete;

    ClockSyntonized &operator=(const ClockSyntonized &) = delete;

    ClockSyntonized &operator=(ClockSyntonized &&) = delete;

private:

    //=======//
    // SINCE //
    //=======//

    /**
     * A member function to give back the timestamp in nanoseconds
     * since the time of its first call.
     * We use that function to measure time intervals in nanoseconds.
     * Those time intervals are syntonized with the system clock.
     * We can also add the current time stamp: GetNanosecondsSinceFirstCall() with the
     * GetBeginOfTimeInNanoseconds() to get the "now" timestamp,
     * but the actual value differs based on which clock is used and which OS.
     */
    auto
    GetNanosecondsSinceFirstCallHelp()
    -> int64_t;

public:
    /**
     * Returns what "GetNanosecondsSinceFirstCallHelp()" does
     * but in std::chrono::duration<int64_t, std::nano>
     */
    auto
    GetNanosecondsSinceFirstCall()
    -> std::chrono::duration<int64_t, std::nano> final;

    //==========//
    // SINCE+AT //
    //==========//

    /**
     * Gives back the nanoseconds since Unix Epoch
     * at the time of first call of the singleton.
     * Returns std::chrono::duration<int64_t, std::nano>
     */
    auto
    GetNanosecondsSinceUnixEpochAtFirstCall()
    -> std::chrono::duration<int64_t, std::nano>  final;

    /**
     * Gives back the nanoseconds since System Epoch
     * at the time of first call of the singleton.
     * Returns std::chrono::duration<int64_t, std::nano>
     */
    auto
    GetNanosecondsSinceSystemEpochAtFirstCall()
    -> std::chrono::duration<int64_t, std::nano>  final;

    //===========//
    // SINCE+NOW //
    //===========//

    /**
     * Gives back the nanoseconds since Unix Epoch "now".
     * It is syntonized with system clock.
     * Returns std::chrono::duration<int64_t, std::nano>
     */
    auto
    GetNanosecondsSinceUnixEpochSyntonized()
    -> std::chrono::duration<int64_t, std::nano>  final;

    /**
     * Gives back the nanoseconds since system epoch "now".
     * So, on Linux and with CLOCK_MONOTONIC is the time passed since boot time.
     * On Linux and with CLOCK_REALTIME is the time passed since Unix Epoch.
     * On Windows is the time passed since Windows Epoch (1899/12/30 midnight, UTC).
     * It is syntonized with system clock.
     * Returns std::chrono::duration<int64_t, std::nano>
     */
    auto
    GetNanosecondsSinceSystemEpochSyntonized()
    -> std::chrono::duration<int64_t, std::nano> final;

private:

#if _WIN32 || _WIN64

    /**
     * Sets nanoseconds since System (Windows) Epoch "now".
     */
    auto
    SetNanosecondsSinceSystemEpochAtFirstCall()
    -> void;

#elif __linux__

    /**
     * Sets nanoseconds since boot time "now" using CLOCK_MONOTONIC.
     */
    auto
    SetNanosecondsSinceUnixEpochAtFirstCallLinuxMonotonic()
    -> void;

    /**
     * Sets nanoseconds since Unix Epoch "now" using CLOCK_REALTIME.
     */
    auto
    SetNanosecondsSinceUnixEpochAtFirstCallLinuxRealtime()
    -> void;

#else
    /**
     * Sets nanoseconds since Unix Epoch "now" using Unix clock.
     */
    auto
    SetNanosecondsSinceUnixEpochAtFirstCallUnix()
    -> void;

#endif

    auto
    SetNanosecondsAtFirstCall()
    -> void;
};
