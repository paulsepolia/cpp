#include "ClockSyntonized.h"

#if _WIN32 || _WIN64

#include <Windows.h>

#elif __linux__

#include <ctime>

#else // generic Unix

#include <sys/time.h>

#endif

ClockSyntonized::ClockSyntonized() {
    SetNanosecondsAtFirstCall();
}

auto
ClockSyntonized::GetNanosecondsSinceFirstCallHelp()
-> int64_t {

#if _WIN32 || _WIN64

    LARGE_INTEGER dt;
    LPFILETIME ft{new FILETIME};

    GetSystemTimePreciseAsFileTime(ft);
    dt.LowPart = ft->dwLowDateTime;
    dt.HighPart = ft->dwHighDateTime;

    const auto timeNow{(int64_t) (dt.QuadPart - OA_ZERO_TICKS)};
    delete ft;

    return timeNow * H1 - nanosecondsSinceSystemEpochAtFirstCall;

#elif __linux__

    auto timeNow{(int64_t) 0LL};
    struct timespec ts{};

    if (clockMonotonicStatus == 0) {

        clock_gettime(CLOCK_MONOTONIC, &ts);
        timeNow = B1 * (int64_t) ts.tv_sec + (int64_t) ts.tv_nsec;

    } else {

        clock_gettime(CLOCK_REALTIME, &ts);
        timeNow = B1 * (int64_t) ts.tv_sec + (int64_t) ts.tv_nsec;
    }

    return (timeNow - nanosecondsSinceSystemEpochAtFirstCall);

#else // generic Unix

    struct timeval tv {};
    gettimeofday(&tv, nullptr);
    const auto timeNow {(int64_t) (B1 * (int64_t)tv.tv_sec + K1 * (int64_t)tv.tv_usec )};

    return (timeNow - nanosecondsSinceSystemEpochAtFirstCall);

#endif
}

auto
ClockSyntonized::GetNanosecondsSinceFirstCall()
-> std::chrono::duration<int64_t, std::nano> {
    return std::chrono::nanoseconds(GetNanosecondsSinceFirstCallHelp());
}

auto
ClockSyntonized::GetNanosecondsSinceUnixEpochAtFirstCall()
-> std::chrono::duration<int64_t, std::nano> {
    return std::chrono::nanoseconds(nanosecondsSinceUnixEpochAtFirstCall);
}

auto
ClockSyntonized::GetNanosecondsSinceSystemEpochAtFirstCall()
-> std::chrono::duration<int64_t, std::nano> {
    return std::chrono::nanoseconds(nanosecondsSinceSystemEpochAtFirstCall);
}

auto
ClockSyntonized::GetNanosecondsSinceUnixEpochSyntonized()
-> std::chrono::duration<int64_t, std::nano> {
    return std::chrono::nanoseconds(
            nanosecondsSinceUnixEpochAtFirstCall + GetNanosecondsSinceFirstCall().count()
    );
}

auto
ClockSyntonized::GetNanosecondsSinceSystemEpochSyntonized()
-> std::chrono::duration<int64_t, std::nano> {
    return std::chrono::nanoseconds(
            nanosecondsSinceSystemEpochAtFirstCall + GetNanosecondsSinceFirstCall().count()
    );
}

#if _WIN32 || _WIN64

auto
ClockSyntonized::SetNanosecondsSinceSystemEpochAtFirstCall()
-> void {

    LARGE_INTEGER dt;
    LPFILETIME ft{new FILETIME};

    GetSystemTimePreciseAsFileTime(ft);
    dt.LowPart = ft->dwLowDateTime;
    dt.HighPart = ft->dwHighDateTime;

    const auto timeNow{(int64_t) (dt.QuadPart - OA_ZERO_TICKS)};

    nanosecondsSinceSystemEpochAtFirstCall =
            H1 * timeNow;
    nanosecondsSinceUnixEpochAtFirstCall =
            std::chrono::duration_cast<std::chrono::nanoseconds>(
                    std::chrono::system_clock::now().time_since_epoch()).count();
}

#elif __linux__

auto
ClockSyntonized::SetNanosecondsSinceUnixEpochAtFirstCallLinuxMonotonic()
-> void {

    struct timespec ts{};
    clock_gettime(CLOCK_MONOTONIC, &ts);

    const auto timeNow{(int64_t) (B1 * (int64_t) ts.tv_sec + (int64_t) ts.tv_nsec)};
    nanosecondsSinceSystemEpochAtFirstCall =
            timeNow;
    nanosecondsSinceUnixEpochAtFirstCall =
            std::chrono::duration_cast<std::chrono::nanoseconds>(
                    std::chrono::system_clock::now().time_since_epoch()).count();
}

auto
ClockSyntonized::SetNanosecondsSinceUnixEpochAtFirstCallLinuxRealtime()
-> void {

    struct timespec ts{};
    clock_gettime(CLOCK_REALTIME, &ts);

    const auto timeNow{(int64_t) (B1 * (int64_t) ts.tv_sec + (int64_t) ts.tv_nsec)};
    nanosecondsSinceSystemEpochAtFirstCall = timeNow;
    nanosecondsSinceUnixEpochAtFirstCall = timeNow;
}

#else

auto
ClockSyntonized::SetNanosecondsSinceUnixEpochAtFirstCallUnix()
-> void {

    struct timeval tv {};
    gettimeofday(&tv, nullptr);

    const auto timeNow{(int64_t)(B1 * (int64_t)tv.tv_sec + K1 * (int64_t)tv.tv_usec)};
    nanosecondsSinceUnixEpochAtFirstCall = timeNow;
    nanosecondsSinceSystemEpochAtFirstCall = timeNow;
}
#endif

auto
ClockSyntonized::SetNanosecondsAtFirstCall()
-> void {

#if _WIN32 || _WIN64

    SetNanosecondsSinceSystemEpochAtFirstCall();

#elif __linux__

    struct timespec ts{};
    clockMonotonicStatus = (int) clock_gettime(CLOCK_MONOTONIC, &ts);

    if (clockMonotonicStatus == 0) {
        clock_gettime(CLOCK_MONOTONIC, &ts);
        SetNanosecondsSinceUnixEpochAtFirstCallLinuxMonotonic();
    } else {
        clock_gettime(CLOCK_REALTIME, &ts);
        SetNanosecondsSinceUnixEpochAtFirstCallLinuxRealtime();
    }

#else // generic Unix

    SetNanosecondsSinceUnixEpochAtFirstCallUnix();

#endif
}
