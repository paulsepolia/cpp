#include <iostream>
#include <cmath>
#include <chrono>
#include <queue>
#include <random>
#include <mutex>
#include <thread>

class benchmark_timer {
public:

    explicit benchmark_timer(const double &res) :
            _res(res),
            t1{std::chrono::steady_clock::now()},
            t2{std::chrono::steady_clock::now()},
            t_prev{std::chrono::steady_clock::now()} {};

    auto print_time() const -> void {
        const auto t_loc{std::chrono::steady_clock::now()};
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t_loc - t_prev).count()};
        t_prev = t_loc;
        std::cout << " --> time used since last reset (secs) = " << total_time << std::endl;
    }

    auto reset_timer() -> void {
        const auto t_loc{std::chrono::steady_clock::now()};
        t_prev = t_loc;
        t1 = t_loc;
        t2 = t_loc;
    }

    ~benchmark_timer() {
        t2 = std::chrono::steady_clock::now();
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count()};
        std::cout << " --> res = " << _res << std::endl;
        std::cout << " --> total time used since last reset (secs) = " << total_time << std::endl;
    }

    auto set_res(const double &res) -> void {
        _res = res;
    }

private:

    double _res{};
    decltype(std::chrono::steady_clock::now()) t1{};
    decltype(std::chrono::steady_clock::now()) t2{};
    mutable decltype(std::chrono::steady_clock::now()) t_prev{};
};

const auto DIM_MAX{(size_t) (std::pow(10.0, 7.0))};
const auto DO_MAX{(size_t) (std::pow(10.0, 12.0))};
const auto MOD1{(size_t) (std::pow(10.0, 6.0))};

//=============================================================//
// Put and consume and consume data in a queue by many threads //
//=============================================================//

auto mtx{std::mutex{}};

auto main() -> int {

    auto cont{std::queue<double>{}};

    const auto l1{[&]() -> void {

        for(size_t i = 0; i < DO_MAX; i++) {

            const auto lck{std::lock_guard<std::mutex>(mtx)};

            if(cont.size() > DIM_MAX) continue;

            for (size_t j = 0; j < DIM_MAX; j++) {
                cont.push((double)j);
            }
        }
    }};

    const auto l2{[&](){

        for(size_t i = 0; i < DO_MAX; i++) {

            const auto lck{std::lock_guard<std::mutex>(mtx)};

            while (!cont.empty()) {
                cont.pop();
            }
        }
    }};

    const auto l3{[&](){

        for(size_t i = 0; i < DO_MAX; i++) {

            const auto lck{std::lock_guard<std::mutex>(mtx)};

            if(!cont.empty() && i % MOD1 == 0) {
                std::cout << " --> the front element = " << cont.front() << std::endl;
                std::cout << " --> the back element = " << cont.back() << std::endl;
            }
        }
    }};

    auto t1{std::thread{l1}};
    auto t2{std::thread{l2}};
    auto t3{std::thread{l3}};

    t1.join();
    t2.join();
    t3.join();
}
