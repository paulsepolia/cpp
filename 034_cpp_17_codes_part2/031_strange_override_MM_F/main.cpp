#include <iostream>
#include <iomanip>
#include <memory>
#include <cmath>

struct A {
};

struct B : A {
};

struct Parent {

    virtual A *foo() {
        std::cout << "--> parent" << std::endl;
        A * res = new A;
        return res;
    }
};

struct Child : Parent {

    B *foo() final {
        std::cout << "--> child" << std::endl;
        B * res = new B;
        return res;
    }
};

int main() {

    Parent p;
    p.foo();

    Child c;
    c.foo();
}