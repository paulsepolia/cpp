#include <iostream>
#include <memory>
#include <vector>
#include <thread>
#include <chrono>

constexpr size_t DIM = 100;

int main()
{
    std::cout << "pgg --> 1" << std::endl;
    std::this_thread::sleep_for(std::chrono::seconds(2));
    std::cout << "pgg --> 2" << std::endl;
    auto * p1 = new double (100);
    std::cout << p1 << std::endl;
    std::cout << *p1 << std::endl;
    delete p1;
    p1 = nullptr;
    std::cout << *p1 << std::endl;
    std::cout << "pgg --> 3" << std::endl;
}