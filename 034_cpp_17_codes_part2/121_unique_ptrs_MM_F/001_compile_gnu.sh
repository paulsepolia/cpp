#!/bin/bash

  g++-9.3.0  -O0 -g       \
             -Wall        \
             -std=gnu++2a \
             -pthread     \
             main.cpp \
             -o x_gnu
