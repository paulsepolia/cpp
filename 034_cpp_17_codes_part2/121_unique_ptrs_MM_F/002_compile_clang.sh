#!/bin/bash

  clang++   -O0 -g       \
            -Wall        \
            -std=gnu++2a \
            -pthread     \
            main.cpp \
            -o x_clang
