#include <iostream>
#include <iomanip>

class less_than {
public:

    explicit less_than(double x) : _x(x) {}

    auto operator()(double arg) {
        return arg < _x;
    }

private:

    double _x;
};

class less_equal_than {
public:

    explicit less_equal_than(double x) : _x(x) {}

    auto operator()(double arg) {
        return arg <= _x;
    }

private:

    double _x;
};


class greater_than {
public:

    explicit greater_than(double x) : _x(x) {}

    auto operator()(double arg) {
        return arg > _x;
    }

private:

    double _x;
};

class greater_equal_than {
public:

    explicit greater_equal_than(double x) : _x(x) {}

    auto operator()(double arg) {
        return arg >= _x;
    }

private:

    double _x;
};

int main() {

    std::cout << std::fixed;
    std::cout << std::boolalpha;

    {
        std::cout << "--> example --> 1" << std::endl;

        less_than less_than_42(42);

        std::cout << less_than_42(10) << std::endl;

        std::cout << less_than_42(50) << std::endl;
    }

    {
        std::cout << "--> example --> 2" << std::endl;

        less_equal_than less_equal_than_42(42);

        std::cout << less_equal_than_42(10) << std::endl;

        std::cout << less_equal_than_42(42) << std::endl;

        std::cout << less_equal_than_42(50) << std::endl;
    }

    {
        std::cout << "--> example --> 3" << std::endl;

        greater_than greater_than_42(42);

        std::cout << greater_than_42(10) << std::endl;

        std::cout << greater_than_42(42) << std::endl;

        std::cout << greater_than_42(50) << std::endl;
    }

    {
        std::cout << "--> example --> 4" << std::endl;

        greater_equal_than greater_equal_than_42(42);

        std::cout << greater_equal_than_42(10) << std::endl;

        std::cout << greater_equal_than_42(42) << std::endl;

        std::cout << greater_equal_than_42(50) << std::endl;
    }
}