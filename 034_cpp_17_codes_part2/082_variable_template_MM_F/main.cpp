#include <iostream>
#include <iomanip>

template<typename T>
constexpr T pi{4.12345678901234567890123456789012345678901234567890L};

auto main() -> int {

    std::cout.precision(30);
    std::cout << std::fixed;

    std::cout << pi<float> << std::endl;
    std::cout << pi<double> << std::endl;
    std::cout << pi<long double> << std::endl;
}

