#include <iostream>
#include <future>
#include <vector>
#include <chrono>

class benchmark_timer {
public:

    explicit benchmark_timer(const double &res) :
            _res(res),
            t1{std::chrono::steady_clock::now()},
            t2{std::chrono::steady_clock::now()} {};

    ~benchmark_timer() {
        t2 = std::chrono::steady_clock::now();
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count()};
        std::cout << " --> res = " << _res << std::endl;
        std::cout << " --> time used (secs) = " << total_time << std::endl;
    }

    auto set_res(const double &res) -> void {
        _res = res;
    }

private:

    double _res{};
    decltype(std::chrono::steady_clock::now()) t1{};
    decltype(std::chrono::steady_clock::now()) t2{};
};

auto set_promise_value(std::promise<std::vector<double>> &prom, std::vector<double> &&val) -> void {

    std::cout << " --> I am about to set a value to the promise I gave but I am sleeping right now..." << std::endl;
    std::this_thread::sleep_for(std::chrono::milliseconds(10000));
    std::cout << " --> I am setting a value to my promise..." << std::endl;
    prom.set_value(std::move(val));
    std::cout << " --> My promised is fulfilled!" << std::endl;
}

auto get_future_value(std::future<std::vector<double>> &fut, std::vector<double> &val) -> void {

    std::cout << " --> I am waiting for my future to get its promise..." << std::endl;
    val = std::move(fut.get());
}

constexpr auto DIM_MAX{(size_t) 50'000'000};

auto main() -> int {

    {
        std::cout << "------------------------------------------->> 1" << std::endl;

        auto prom{std::promise<std::vector<double>>{}};
        auto fut1{std::future<std::vector<double>>{prom.get_future()}};
        try {
            auto fut2{std::future<std::vector<double>>{prom.get_future()}};

        }
        catch (const std::exception &e) {
            std::cout << e.what() << std::endl;
        }

        auto vv{std::vector<double>()};

        {
            std::cout << " -->> build data" << std::endl;
            auto ot{benchmark_timer(0.0)};
            for (size_t i = 0; i < DIM_MAX; i++) {
                vv.push_back((double) i);
            }
            ot.set_res(123.456);
        }

        {
            std::cout << " -->> set promise" << std::endl;
            auto ot{benchmark_timer(0.0)};
            prom.set_value(vv);
            ot.set_res(123.456);
        }

        {
            auto ot{benchmark_timer(0.0)};
            std::cout << " -->> get promise" << std::endl;
            const auto val1{fut1.get()};
            ot.set_res(123.456);
            std::cout << " --> val1[1] = " << val1[1] << std::endl;
        }
    }
}
