#include <ctime>
#include <iostream>
#include <chrono>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/chrono/chrono_io.hpp>

#if _WIN32 || _WIN64
#include "machine/windows_api.h"
#elif __linux__

#include <ctime>

#else // generic Unix
#include <sys/time.h>
#endif

auto extractIntegersFromString(const std::string &str) -> std::string {

    auto res{std::string{}};

    for (const auto &el: str) {
        if (std::isdigit(el)) {
            res += el;
        }
    }

    return res;
}

auto getTimeStampUTCString() -> std::string {

    const auto tp1{std::chrono::system_clock::now()};
    const auto tp2{std::chrono::system_clock::to_time_t(tp1)};

    return std::asctime(std::gmtime(&tp2));
}

auto getTimeStampUTCSecondsString() -> std::string {

    const auto time_utc{boost::posix_time::second_clock::universal_time()};

    return boost::posix_time::to_iso_string(time_utc);
}

auto getTimeStampUTCMicrosecondsString() -> std::string {

    const auto time_utc{boost::posix_time::microsec_clock::universal_time()};

    return boost::posix_time::to_iso_string(time_utc);
}

auto getTimeStampLocalTimeString() -> std::string {

    const auto tp1{std::chrono::system_clock::now()};
    const auto tp2{std::chrono::system_clock::to_time_t(tp1)};

    return std::asctime(std::localtime(&tp2));
}

auto getTimeStampLocalTimeSecondsString() -> std::string {

    const auto time_local{boost::posix_time::second_clock::local_time()};

    return boost::posix_time::to_iso_string(time_local);
}

auto getTimeStampLocalTimeMicrosecondsString() -> std::string {

    const auto time_local{boost::posix_time::microsec_clock::local_time()};

    return boost::posix_time::to_iso_string(time_local);
}

auto getNanosecondsSinceUnixEpoch() -> size_t {

    const auto nanoseconds_since_epoch{
            std::chrono::duration_cast<std::chrono::nanoseconds>(
                    std::chrono::system_clock::now().time_since_epoch()).count()};

    return nanoseconds_since_epoch;
}

auto getNanosecondsSinceBoot() -> size_t {

    using T = boost::chrono::time_point<boost::chrono::steady_clock, boost::chrono::duration<size_t, boost::nano>>;
    T tp = boost::chrono::steady_clock::now();

    auto ss{std::stringstream{}};
    ss << tp;

    return std::stoul(extractIntegersFromString(ss.str()));
}

auto getNanosecondsSinceBootOrEpoch() -> size_t {

    constexpr auto B1{(size_t) 1'000'000'000UL};

#if _WIN32 || _WIN64

    LARGE_INTEGER qpcnt;
    int rval = QueryPerformanceCounter(&qpcnt);
    __TBB_ASSERT_EX(rval, "QueryPerformanceCounter failed");
    result.my_count = qpcnt.QuadPart;

#elif __linux__

    struct timespec ts{};

    const auto clockMonotonicStatus{(int) clock_gettime(CLOCK_MONOTONIC, &ts)};

    if (clockMonotonicStatus == 0) {

        const auto nanosecondsSinceBoot{(size_t) B1 * (size_t) ts.tv_sec + (size_t) ts.tv_nsec};

        return nanosecondsSinceBoot;

    } else {

        const auto clockRealTimeStatus{(int) clock_gettime(CLOCK_REALTIME, &ts)};

        if (clockRealTimeStatus == -1) {
            std::cout << "CLOCK_REALTIME is not supported. No CLOCK for the Linux platform!"
                      << std::endl;
        }

        assert(clockRealTimeStatus == 0);
        const auto nanosecondsSinceEpoch{(size_t) B1 * (size_t) ts.tv_sec + (size_t) ts.tv_nsec};

        return nanosecondsSinceEpoch;
    }

#else // generic Unix

    struct timeval tv{};
    auto status{(int) gettimeofday(&tv, NULL)};

    if (status == -1) {
        std::cout << "NO CLOCK is supported for the Unix platform!" << std::endl;
    }

    assert(status == 0);
    const auto nanosecondsSinceEpoch{(size_t) B1 * (size_t) tv.tv_sec + (size_t) tv.tv_usec};

    return nanosecondsSinceEpoch;

#endif
}

auto getNanosecondsResolutionV1() -> size_t {

    using T = boost::chrono::time_point<boost::chrono::steady_clock,
            boost::chrono::duration<size_t, boost::nano>>;

    const auto tp1{(T) boost::chrono::steady_clock::now()};
    const auto tp2{(T) boost::chrono::steady_clock::now()};

    auto ss{std::stringstream{}};
    ss << (tp2 - tp1);

    return std::stoul(extractIntegersFromString(ss.str()));
}

auto getNanosecondsResolutionV2() -> size_t {

    const auto p1{getNanosecondsSinceUnixEpoch()};
    const auto p2{getNanosecondsSinceUnixEpoch()};

    return p2 - p1;
}

auto makeEpochTimeString(int y, int m, int d) -> std::string {

    const auto time_t_epoch{boost::posix_time::ptime(boost::gregorian::date(y, m, d))};

    return boost::posix_time::to_iso_string(time_t_epoch);
}

auto makeEpochTime(int y, int m, int d) -> boost::posix_time::ptime {

    const auto time_t_epoch{boost::posix_time::ptime(boost::gregorian::date(y, m, d))};

    return time_t_epoch;
}

auto getTimeUTCSecondsNow() -> boost::posix_time::ptime {

    const auto time_utc{boost::posix_time::second_clock::universal_time()};

    return time_utc;
}

auto getTimeUTCMicrosecondsNow() -> boost::posix_time::ptime {

    const auto time_utc{boost::posix_time::microsec_clock::universal_time()};

    return time_utc;
}

auto main() -> int {

    {
        std::cout << "---->> time stamp UTC string" << std::endl;

        std::cout << getTimeStampUTCString() << std::endl;

        std::cout << getTimeStampUTCSecondsString() << std::endl;

        std::cout << getTimeStampUTCMicrosecondsString() << std::endl;
    }

    {
        std::cout << "---->> time stamp local time string" << std::endl;

        std::cout << getTimeStampLocalTimeString() << std::endl;

        std::cout << getTimeStampLocalTimeSecondsString() << std::endl;

        std::cout << getTimeStampLocalTimeMicrosecondsString() << std::endl;
    }

    {
        std::cout << "---->> nanoseconds since Unix Epoch" << std::endl;

        std::cout << getNanosecondsSinceUnixEpoch() << std::endl;
    }

    {
        std::cout << " ---->> maths with epoch times" << std::endl;

        std::cout << makeEpochTimeString(1970, 1, 1) << std::endl;

        std::cout << makeEpochTimeString(2010, 1, 1) << std::endl;

        std::cout << makeEpochTime(1970, 1, 1) << std::endl;

        std::cout << makeEpochTime(2010, 1, 1) << std::endl;

        std::cout << (makeEpochTime(2010, 1, 1) - makeEpochTime(1970, 1, 1))
                  << std::endl;
    }

    {
        std::cout << " ---->> maths with epoch times + time now" << std::endl;

        std::cout << getTimeUTCSecondsNow() << std::endl;

        std::cout << (getTimeUTCSecondsNow() - makeEpochTime(2010, 1, 1)) << std::endl;

        std::cout << getTimeUTCMicrosecondsNow() << std::endl;

        std::cout << (getTimeUTCMicrosecondsNow() - makeEpochTime(2010, 1, 1)) << std::endl;
    }

    {
        std::cout << " ---->> get nanoseconds since boot or Epoch" << std::endl;

        std::cout << getNanosecondsSinceBoot() << std::endl;

        std::cout << getNanosecondsSinceBootOrEpoch() << std::endl;

        std::cout << getNanosecondsResolutionV1() << std::endl;

        std::cout << getNanosecondsResolutionV2() << std::endl;
    }
}
