#include <iostream>
#include <variant>
#include <vector>
#include <chrono>
#include <iomanip>

class benchmark_timer {
public:

    explicit benchmark_timer(const double &res) :
            _res(res),
            t1{std::chrono::steady_clock::now()},
            t2{std::chrono::steady_clock::now()},
            t_prev{std::chrono::steady_clock::now()} {};

    auto print_time() const -> void {
        const auto t_loc{std::chrono::steady_clock::now()};
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t_loc - t_prev).count()};
        t_prev = t_loc;
        std::cout << " --> time used (secs) = " << total_time << std::endl;
    }

    auto reset_timer() -> void {
        const auto t_loc{std::chrono::steady_clock::now()};
        t_prev = t_loc;
        t1 = t_loc;
        t2 = t_loc;
    }

    ~benchmark_timer() {
        t2 = std::chrono::steady_clock::now();
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count()};
        std::cout << " --> res = " << _res << std::endl;
        std::cout << " --> total time used (secs) = " << total_time << std::endl;
    }

    auto set_res(const double &res) -> void {
        _res = res;
    }

private:

    double _res{};
    decltype(std::chrono::steady_clock::now()) t1{};
    decltype(std::chrono::steady_clock::now()) t2{};
    mutable decltype(std::chrono::steady_clock::now()) t_prev{};
};

//====================================//
// classes for variant + visitor test //
//====================================//

class A1 {
public:

    void DoIt(double &a) const {
        a += v;
    }

private:
    constexpr static double v{1};
};

class A2 {
public:

    void DoIt(double &a) const {
        a += v;
    }

private:
    constexpr static double v{2};
};

class A3 {
public:

    void DoIt(double &a) const {
        a += v;
    }

private:
    constexpr static double v{3};
};

class A4 {
public:

    void DoIt(double &a) const {
        a += v;
    }

private:
    constexpr static double v{4};
};

//==============================//
// classes for polymorphic test //
//==============================//

class B0 {
public:

    virtual void DoIt(double &a) const = 0;

    virtual ~B0() = default;
};

class B1 : public B0 {
public:

    void DoIt(double &a) const override {
        a += v;
    }

private:
    constexpr static double v{1};
};

class B2 : public B1 {
public:

    void DoIt(double &a) const override {
        a += v;
    }

private:
    constexpr static double v{2};
};

class B3 : public B2 {
public:

    void DoIt(double &a) const override {
        a += v;
    }

private:
    constexpr static double v{3};
};

class B4 : public B3 {
public:

    void DoIt(double &a) const final {
        a += v;
    }

private:
    constexpr static double v{4};
};

//=======================================================//
// classes for polymorphic test + one level of hierarchy //
//=======================================================//

class C0 {
public:

    virtual void DoIt(double &a) const = 0;

    virtual ~C0() = default;
};

class C1 final : public C0 {
public:

    void DoIt(double &a) const final {
        a += v;
    }

private:
    constexpr static double v{1};
};

class C2 final : public C0 {
public:

    void DoIt(double &a) const final {
        a += v;
    }

private:
    constexpr static double v{2};
};

class C3 final : public C0 {
public:

    void DoIt(double &a) const final {
        a += v;
    }

private:
    constexpr static double v{3};
};

class C4 final : public C0 {
public:

    void DoIt(double &a) const final {
        a += v;
    }

private:
    constexpr static double v{4};
};

//==============================//
// classes non-polymorphic test //
//==============================//

class D0 {
public:

    void DoIt(double &a) const {
    }

    virtual ~D0() = default;
};

class D1 final : public D0 {
public:

    void DoIt(double &a) const {
        a += v;
    }

private:
    constexpr static double v{1};
};

class D2 final : public D0 {
public:

    void DoIt(double &a) const {
        a += v;
    }

private:
    constexpr static double v{2};
};

class D3 final : public D0 {
public:

    void DoIt(double &a) const {
        a += v;
    }

private:
    constexpr static double v{3};
};

class D4 final : public D0 {
public:

    void DoIt(double &a) const {
        a += v;
    }

private:
    constexpr static double v{4};
};

auto main() -> int {

    constexpr auto DO_MAX{(size_t) 1'000'000'000ULL};

    auto sum_loc1{(double) 0};
    auto sum_loc2{(double) 0};
    auto sum_loc3{(double) 0};
    auto sum_loc4{(double) 0};

    {
        std::cout << " --> test -->> variant + visit version                   -->> 1 " << std::endl;

        auto vec{std::vector<std::variant<A1, A2, A3, A4>>{A1(), A2(), A3(), A4()}};
        auto ot{benchmark_timer(0)};

        for (size_t i = 0; i < DO_MAX; i++) {
            for (const auto &el : vec) {
                auto a{(double) 1};

                std::visit([&](auto &&arg) -> double {
                    arg.DoIt(a);
                    return a;
                }, el);

                sum_loc1 += a;
            }
        }

        ot.set_res(sum_loc1);
        std::cout << " --> sum_loc1 -->> " << sum_loc1 << std::endl;
    }

    {
        std::cout << " --> test -->> polymorphic version                       -->> 2" << std::endl;

        auto vec{std::vector<B0 *>{new B1(), new B2(), new B3(), new B4()}};
        auto ot{benchmark_timer(0)};

        for (size_t i = 0; i < DO_MAX; i++) {
            for (const auto &el : vec) {
                auto a{(double) 1};
                el->DoIt(a);
                sum_loc2 += a;
            }
        }

        for (auto &el: vec) {
            delete el;
        }

        ot.set_res(sum_loc2);
        std::cout << " --> sum_loc2 -->> " << sum_loc2 << std::endl;
    }

    {
        std::cout << " --> test -->> polymorphic version + one level           -->> 3" << std::endl;

        auto vec{std::vector<C0 *>{new C1(), new C2(), new C3(), new C4()}};
        auto ot{benchmark_timer(0)};

        for (size_t i = 0; i < DO_MAX; i++) {
            for (const auto &el : vec) {
                auto a{(double) 1};
                el->DoIt(a);
                sum_loc3 += a;
            }
        }

        for (auto &el: vec) {
            delete el;
        }

        ot.set_res(sum_loc3);
        std::cout << " --> sum_loc3 -->> " << sum_loc3 << std::endl;
    }

    {
        std::cout << " --> test -->> non-polymorphic version                   -->> 4" << std::endl;

        auto vec{std::vector<std::variant<D1, D2, D3, D4>>{D1(), D2(), D3(), D4()}};
        auto ot{benchmark_timer(0)};

        for (size_t i = 0; i < DO_MAX; i++) {
            for (const auto &el : vec) {
                auto a{(double) 1};

                std::visit([&](auto &&arg) -> double {
                    arg.DoIt(a);
                    return a;
                }, el);

                sum_loc4 += a;
            }
        }

        ot.set_res(sum_loc4);
        std::cout << " --> sum_loc4 -->> " << sum_loc3 << std::endl;
    }

    std::cout << std::boolalpha;
    std::cout << " --> should be true -->>  " << (sum_loc1 == sum_loc2) << std::endl;
    std::cout << " --> should be true -->>  " << (sum_loc1 == sum_loc3) << std::endl;
    std::cout << " --> should be true -->>  " << (sum_loc1 == sum_loc4) << std::endl;
}
