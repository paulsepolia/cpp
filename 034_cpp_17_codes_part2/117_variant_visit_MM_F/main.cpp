#include <iostream>
#include <variant>
#include <vector>

class A1 {
public:

    void DoIt(double &a) const {
        a += v;
    }

private:
    constexpr static double v{1};
};

class A2 {
public:

    void DoIt(double &a) const {
        a += v;
    }

private:
private:
    constexpr static double v{2};
};

class A3 {
public:

    void DoIt(double &a) const {
        a += v;
    }

private:
    constexpr static double v{3};
};

class A4 {
public:

    void DoIt(double &a) const {
        a += v;
    }

private:
    constexpr static double v{4};
};

auto main() -> int {

    {
        std::cout << "test -->> 1" << std::endl;

        auto vec{std::vector<std::variant<A1, A2, A3, A4>>{A1(), A2(), A3(), A4()}};

        for (const auto &el: vec) {
            double a{10};

            std::visit([&](auto &&arg) -> double {
                arg.DoIt(a);
                return a;
            }, el);

            std::cout << a << std::endl;
        }
    }
}
