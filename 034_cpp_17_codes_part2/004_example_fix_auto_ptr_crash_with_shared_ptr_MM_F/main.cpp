#include <iostream>
#include <memory>

void do_something(const std::shared_ptr<int> &my_ptr) {
    *my_ptr = 11;
    std::cout << "  --> *my_ptr = " << *my_ptr << std::endl;
}

void auto_ptr_test() {
    std::shared_ptr<int> my_test(new int(10));
    do_something(my_test);
    *my_test = 12;
    std::cout << "  --> *my_test = " << *my_test << std::endl;
}

int main() {

    auto_ptr_test();
}
