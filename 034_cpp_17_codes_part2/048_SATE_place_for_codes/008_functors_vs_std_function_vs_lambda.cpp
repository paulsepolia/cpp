#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>
#include <chrono>
#include <functional>

class is_equal_pgg_functor {
public:

    explicit is_equal_pgg_functor(double val) : _val{val} {}

    auto operator()(double x) -> bool {
        return x == _val;
    }

private:

    double _val{0.0};
};

bool is_equal_pgg_function(double x) {
    return x == -1.0;
}

auto main() -> int {

    const auto DOM_MAX{(size_t) std::pow(10.0, 6.0)};

    for (size_t kk = 0; kk < DOM_MAX; kk++) {

        std::cout << "----------------------------------------->> kk = " << kk << std::endl;

        const auto DIM_MAX1{(size_t) (std::pow(10.0, 9.0))};
        auto v1 = std::vector<double>{};

        for (size_t i = 0; i < DIM_MAX1; i++) {
            v1.push_back((double) i);
        }

        {
            std::cout << "-------------------------------------------->> 1" << std::endl;
            const auto t1 = std::chrono::steady_clock::now();

            const auto c{-1.0};

            const auto res{std::find_if(v1.begin(), v1.end(), [c](double x) {
                return x == c;
            })};

            if (res == v1.end()) {
                std::cout << " --> no result" << std::endl;
            }

            std::cout << res.base() << std::endl;

            const auto t2{std::chrono::steady_clock::now()};
            const auto time_span1{std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count()};

            std::cout << "--> time used = " << time_span1 << " seconds" << std::endl;
        }

        {
            std::cout << "-------------------------------------------->> 2" << std::endl;
            const auto t1 = std::chrono::steady_clock::now();

            const auto res{std::find_if(v1.begin(), v1.end(), is_equal_pgg_function)};

            if (res == v1.end()) {
                std::cout << " --> no result" << std::endl;
            }

            std::cout << res.base() << std::endl;

            const auto t2 = std::chrono::steady_clock::now();
            const auto time_span1 = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count();

            std::cout << "--> time used = " << time_span1 << " seconds" << std::endl;
        }

        {
            std::cout << "-------------------------------------------->> 3" << std::endl;
            const auto t1 = std::chrono::steady_clock::now();

            const auto c{-1.0};

            const auto res{std::find_if(v1.begin(), v1.end(), is_equal_pgg_functor(c))};

            if (res == v1.end()) {
                std::cout << " --> no result" << std::endl;
            }

            std::cout << res.base() << std::endl;

            const auto t2 = std::chrono::steady_clock::now();
            const auto time_span1 = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count();

            std::cout << "--> time used = " << time_span1 << " seconds" << std::endl;
        }

        {
            std::cout << "-------------------------------------------->> 4" << std::endl;
            const auto t1 = std::chrono::steady_clock::now();

            const auto c{-1.0};

            std::function<bool(double)> FF = [&](double x) {
                return x == c;
            };

            const auto res{std::find_if(v1.begin(), v1.end(), FF)};

            if (res == v1.end()) {
                std::cout << " --> no result" << std::endl;
            }

            std::cout << res.base() << std::endl;

            const auto t2 = std::chrono::steady_clock::now();
            const auto time_span1 = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count();

            std::cout << "--> time used = " << time_span1 << " seconds" << std::endl;
        }
    }
}