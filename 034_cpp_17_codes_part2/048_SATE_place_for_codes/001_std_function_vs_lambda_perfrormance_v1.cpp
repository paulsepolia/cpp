#include <vector>
#include <functional>
#include <iostream>
#include <chrono>

const uint64_t DO_MAX = 1000UL;
const uint64_t DIM_MAX = 1000000UL;
const uint64_t ONE_VAL = 1UL;

auto test_direct_lambda() -> std::vector<double> {

    auto lbd = [](double v) {
        return v + ONE_VAL;
    };

    using L = decltype(lbd);

    auto fs = std::vector<L>{};

    const auto t1 = std::chrono::steady_clock::now();

    fs.resize(DIM_MAX, lbd);

    const auto t2 = std::chrono::steady_clock::now();
    const auto time_span1 = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);

    auto res{ 0.0 };

    const auto t3 = std::chrono::steady_clock::now();

    for (const auto& f : fs) {
        res = f(res);
    }

    const auto t4 = std::chrono::steady_clock::now();
    const auto time_span2 = std::chrono::duration_cast<std::chrono::duration<double>>(t4 - t3);

    return { res, time_span1.count(), time_span2.count() };
}

auto test_std_function() -> std::vector<double> {

    auto lbd = [](double v) {
        return v + ONE_VAL;
    };

    using F = std::function<double(double)>;

    auto fs = std::vector<F>{};

    const auto t1 = std::chrono::steady_clock::now();

    fs.resize(DIM_MAX, lbd);

    const auto t2 = std::chrono::steady_clock::now();
    const auto time_span1 = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);

    auto res = 0.0;

    const auto t3 = std::chrono::steady_clock::now();

    for (const auto& f : fs) {
        res = f(res);
    }

    const auto t4 = std::chrono::steady_clock::now();
    const auto time_span2 = std::chrono::duration_cast<std::chrono::duration<double>>(t4 - t3);

    return { res, time_span1.count(), time_span2.count() };
}

auto main() -> int {

    {
        std::cout << "------------------------------------------------>> 1" << std::endl;

        auto res_all{ 0.0 };
        auto time_resize{ 0.0 };
        auto time_call{ 0.0 };

        for (uint64_t i = 0; i < DO_MAX; i++)
        {
            const auto tmp = test_direct_lambda();

            res_all += tmp[0];
            time_resize += tmp[1];
            time_call += tmp[2];

        }

        std::cout << "--> res_all = " << res_all << std::endl;
        std::cout << "--> 1Bn        --> time_resize = " << time_resize << " seconds." << std::endl;
        std::cout << "--> 1Bn lambda --> time_calls =  " << time_call << " seconds." << std::endl;
    }

    {
        std::cout << "------------------------------------------------>> 2" << std::endl;

        auto res_all{ 0.0 };
        auto time_resize{ 0.0 };
        auto time_call{ 0.0 };

        for (uint64_t i = 0; i < DO_MAX; i++)
        {
            const auto tmp = test_std_function();

            res_all += tmp[0];
            time_resize += tmp[1];
            time_call += tmp[2];

        }

        std::cout << "--> res_all = " << res_all << std::endl;
        std::cout << "--> 1Bn               --> time_resize = " << time_resize << " seconds." << std::endl;
        std::cout << "--> 1Bn std::function --> time_calls =  " << time_call << " seconds." << std::endl;
    }

    std::cout << "-------------------->> wait here" << std::endl;

    int sentinel;
    std::cin >> sentinel;
}