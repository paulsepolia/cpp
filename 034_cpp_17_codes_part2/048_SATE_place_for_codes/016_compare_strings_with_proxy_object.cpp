#include <iostream>
#include <vector>
#include <memory>
#include <functional>
#include <chrono>
#include <iomanip>
#include <cmath>

class benchmark_timer {
public:

    explicit benchmark_timer(const double &res) :
            _res(res),
            t1{std::chrono::steady_clock::now()},
            t2{std::chrono::steady_clock::now()} {};

    ~benchmark_timer() {
        t2 = std::chrono::steady_clock::now();
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count()};
        std::cout << " --> res = " << _res << std::endl;
        std::cout << " --> time used (secs) = " << total_time << std::endl;
    }

    auto set_res(const double &res) -> void {
        _res = res;
    }

private:

    double _res{};
    decltype(std::chrono::steady_clock::now()) t1{};
    decltype(std::chrono::steady_clock::now()) t2{};
};

class A {
public:

    explicit A(std::string name) : _name(std::move(name)) {}

    [[nodiscard]] auto get_name() const -> std::string {
        return _name;
    }

    auto set_name(std::string name) -> void {
        _name = std::move(name);
    }

    virtual ~A() = default;

private:
    std::string _name;
};

inline auto is_equal_fast(const std::string &a, const std::string &b, const std::string &c) {

    return a.size() + b.size() == c.size() &&
           std::equal(a.begin(), a.end(), c.begin()) &&
           std::equal(b.begin(), b.end(), c.begin() + a.size());
}

inline auto is_equal_slow(const std::string &a, const std::string &b, const std::string &c) {

    return a + b == c;
}

struct ContactProxy {
    const std::string &a;
    const std::string &b;
};

class String {
public:
    String() = default;

    explicit String(std::string s) : s_{std::move(s)} {}

    std::string s_{};
};

auto operator+(const String &a, const String &b) -> ContactProxy {
    return ContactProxy{a.s_, b.s_};
}

auto operator==(ContactProxy &&concat, const String &b) -> bool {
    return is_equal_fast(concat.a, concat.b, b.s_);
}

auto main() -> int {

    const auto DO_MAX((size_t) (std::pow(10.0, 8.0)));

    {
        std::cout << "--------------------------------------------------->> 1" << std::endl;
        const auto s1{std::string("111111111111111")};
        const auto s2{std::string("222222222222222")};
        const auto s3{s1 + s2};

        benchmark_timer ot(0.0);
        auto num_equal{0.0};
        for (size_t kk = 0; kk < DO_MAX; kk++) {
            num_equal += (s1 + s2 == s3);
        }
        ot.set_res(num_equal);
    }
    {
        std::cout << "--------------------------------------------------->> 2" << std::endl;
        const auto s1{std::string("111111111111111")};
        const auto s2{std::string("222222222222222")};
        const auto s3{std::string("111111111111111") + std::string("222222222222223")};

        benchmark_timer ot(0.0);
        auto num_equal{0.0};
        for (size_t kk = 0; kk < DO_MAX; kk++) {
            num_equal += is_equal_fast(s1, s2, s3);
        }
        ot.set_res(num_equal);
    }
    {
        std::cout << "--------------------------------------------------->> 3" << std::endl;
        const auto s1{std::string("111111111111111")};
        const auto s2{std::string("222222222222222")};
        const auto s3{"3"};

        benchmark_timer ot(0.0);
        auto num_equal{0.0};
        for (size_t kk = 0; kk < DO_MAX; kk++) {
            num_equal += is_equal_fast(s1, s2, s3);
        }
        ot.set_res(num_equal);
    }
    {
        std::cout << "--------------------------------------------------->> 4" << std::endl;
        const auto s1{std::string("111111111111111")};
        const auto s2{std::string("222222222222222")};
        const auto s3{s1 + s2};

        benchmark_timer ot(0.0);
        auto num_equal{0.0};
        for (size_t kk = 0; kk < DO_MAX; kk++) {
            num_equal += (s1 + s2 == s3);
        }
        ot.set_res(num_equal);
    }
    {
        std::cout << "--------------------------------------------------->> 5" << std::endl;
        const auto s1{std::string("111111111111111")};
        const auto s2{std::string("222222222222222")};
        const auto s3{std::string("111111111111111") + std::string("222222222222223")};

        benchmark_timer ot(0.0);
        auto num_equal{0.0};
        for (size_t kk = 0; kk < DO_MAX; kk++) {
            num_equal += (s1 + s2 == s3);
        }
        ot.set_res(num_equal);
    }
    {
        std::cout << "--------------------------------------------------->> 6" << std::endl;
        const auto s1{std::string("111111111111111")};
        const auto s2{std::string("222222222222222")};
        const auto s3{"3"};

        benchmark_timer ot(0.0);
        auto num_equal{0.0};
        for (size_t kk = 0; kk < DO_MAX; kk++) {
            num_equal += (s1 + s2 == s3);
        }
        ot.set_res(num_equal);
    }
}