#include <iostream>
#include <vector>
#include <algorithm>
#include <chrono>
#include <iomanip>
#include <cmath>

template<typename TIterator>
auto contains_duplicates_v1(TIterator first, TIterator last) -> bool {
    using ValueType = std::decay_t<decltype((*first))>;
    auto c{std::vector<ValueType>(first, last)};
    std::sort(c.begin(), c.end());
    return std::adjacent_find(c.begin(), c.end()) != c.end();
}

template<typename TIterator>
auto contains_duplicates_v2(TIterator first, TIterator last) -> bool {
    for (auto it = first; it != last; it++) {
        if (std::find(std::next(it), last, *it) != last) return true;
    }
    return false;
}

auto main() -> int {

    std::cout << std::boolalpha;

    auto v{std::vector<double>{}};
    const auto DIM_MAX{(size_t) (2 * std::pow(10.0, 5.0))};
    const auto DO_MAX{(size_t) std::pow(10.0, 5.0)};

    v.resize(DIM_MAX);

    for (size_t i = 0; i < DIM_MAX; i++) {
        v[i] = (double) i;
    }

    for (size_t kk = 0; kk < DO_MAX; kk++) {

        std::cout << "--------------------------------------------------->> kk = " << kk << std::endl;

        {
            const auto t1{std::chrono::steady_clock::now()};
            const auto res{contains_duplicates_v1(v.begin(), v.end())};
            const auto t2{std::chrono::steady_clock::now()};
            const auto time_span{std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count()};
            std::cout << " res = " << res << " --> time used fast = " << time_span << std::endl;
        }
        {
            const auto t1{std::chrono::steady_clock::now()};
            const auto res{contains_duplicates_v2(v.begin(), v.end())};
            const auto t2{std::chrono::steady_clock::now()};
            const auto time_span{std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count()};
            std::cout << " res = " << res << " --> time used slow = " << time_span << std::endl;
        }
    }
}