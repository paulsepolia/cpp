#include <iostream>
#include <vector>
#include <algorithm>
#include <random>
#include <chrono>

template<typename T1, typename T2>
class equalBySizeFunctor {
public:

    equalBySizeFunctor() = default;

    ~equalBySizeFunctor() = default;

    equalBySizeFunctor(const equalBySizeFunctor &) = default;

    auto operator=(const equalBySizeFunctor &) -> equalBySizeFunctor & = default;

    equalBySizeFunctor(equalBySizeFunctor &&) noexcept = default;

    auto operator=(equalBySizeFunctor &&) noexcept -> equalBySizeFunctor & = default;

    explicit equalBySizeFunctor(T1 size) : _size{size} {}

    auto operator()(const T2 &s) -> bool {
        return s.size() == _size;
    }

private:
    T1 _size;
};

auto equalBySizeLambda = [](auto size) {
    return [size](const auto &v) {
        return size == v.size();
    };
};

template<typename T>
class lessBySizeFunctor {
public:

    lessBySizeFunctor() = default;

    lessBySizeFunctor(const lessBySizeFunctor &) = default;

    auto operator=(const lessBySizeFunctor &) -> lessBySizeFunctor & = default;

    lessBySizeFunctor(lessBySizeFunctor &&) noexcept = default;

    auto operator=(lessBySizeFunctor &&) noexcept -> lessBySizeFunctor & = default;

    ~lessBySizeFunctor() = default;

    auto operator()(const T &a, const T &b) -> bool {
        return a.size() < b.size();
    }
};

auto lessBySizeLambda{[](const auto &a, const auto &b) -> bool {
    return a.size() < b.size();
}};


auto main() -> int {

    std::vector<std::string> v;
    v.emplace_back("1");
    v.emplace_back("12");
    v.emplace_back("123");
    v.emplace_back("2");
    v.emplace_back("");
    v.emplace_back("1234567890");
    v.emplace_back("1234567");

    std::cout << "------------------------------------------>> 1" << std::endl;
    for (const auto &el: v) {
        std::cout << el << std::endl;
    }

    std::cout << "------------------------------------------>> 2" << std::endl;

    std::sort(v.begin(), v.end(), lessBySizeFunctor<std::string>());

    for (const auto &el: v) {
        std::cout << el << std::endl;
    }

    std::cout << "------------------------------------------>> 3" << std::endl;

    const auto x1{std::find_if(v.begin(), v.end(), equalBySizeLambda(1))};

    if (x1 != v.end()) {
        std::cout << *x1 << std::endl;
    }

    const auto x2{std::find_if(v.begin(), v.end(), equalBySizeFunctor<size_t, std::string>(1))};

    if (x2 != v.end()) {
        std::cout << *x2 << std::endl;
    }

    std::cout << "------------------------------------------>> 4" << std::endl;

    const auto seed{std::chrono::system_clock::now().time_since_epoch().count()};

    std::shuffle(v.begin(), v.end(), std::default_random_engine(seed));

    std::sort(v.begin(), v.end(), lessBySizeLambda);

    for (const auto &el: v) {
        std::cout << el << std::endl;
    }
}