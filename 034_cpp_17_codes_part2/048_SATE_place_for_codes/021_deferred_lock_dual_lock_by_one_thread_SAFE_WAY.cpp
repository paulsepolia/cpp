#include <iostream>
#include <cmath>
#include <thread>
#include <mutex>
#include <cassert>
#include <iomanip>
#include <vector>

struct BankAcount {
	BankAcount() = default;
	double balance{ 0.0 };
	std::mutex m_{};
};

void transfer_money(BankAcount& from, BankAcount& to, double amount)
{
	// get the lock for the account 'from'
	auto lock1{ std::unique_lock<std::mutex>(from.m_, std::defer_lock) };

	// get the lock for the account 'to'
	auto lock2{ std::unique_lock<std::mutex>(to.m_, std::defer_lock) };

	// lock both 'locks' at the same time
	std::lock(lock1, lock2);

	// do the transfer here

	from.balance -= amount;
	to.balance += amount;
}

auto main() -> int {

	const auto PAR_TRA{ (uint64_t) std::pow(10.0, 3.0) };

	BankAcount rich;
	BankAcount poor;

	rich.balance = 100000;
	poor.balance = 0.0;

	const auto amount{ 1.0 };

	// do parallel transfers

	auto v{ std::vector<std::thread>() };

	for (uint64_t kk = 0; kk < PAR_TRA; kk++)
	{
		v.emplace_back(std::thread(transfer_money, rich, poor, amount));
	}

	for (auto& el : v)
	{
		el.join();
	}

	std::cout << rich.balance << std::endl;
	std::cout << poor.balance << std::endl;
}