#include <iostream>
#include <cmath>
#include <string>

auto str_to_lower(std::string s) -> std::string {
    for (auto &el : s) {
        el = std::tolower(el);
    }
    return s;
}

auto main() -> int {

    std::cout.precision(10);
    std::cout << std::fixed;

    {
        std::cout << "-------------------------------------------->> 1" << std::endl;
        const auto s1 = std::string{"QWERTYUIOPASDFGHJKLZXCVBNM"};
        std::cout << str_to_lower(s1) << std::endl;
    }

    {
        std::cout << "-------------------------------------------->> 2" << std::endl;
        std::cout << str_to_lower(std::string{"QWERTYUIOPASDFGHJKLZXCVBNM"}) << std::endl;
    }

    {
        std::cout << "-------------------------------------------->> 3" << std::endl;
        auto s1{std::string{"QWERTYUIOPASDFGHJKLZXCVBNM"}};
        std::cout << str_to_lower(std::move(s1)) << std::endl;
    }
}