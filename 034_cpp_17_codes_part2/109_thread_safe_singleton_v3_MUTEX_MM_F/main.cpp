#include <chrono>
#include <iostream>
#include <future>
#include <iomanip>
#include <vector>
#include <mutex>

std::mutex mtx;

class SingletonThreadSafe {
public:
    static SingletonThreadSafe &getInstance() {

        std::lock_guard<std::mutex> lck(mtx);

        if(!instance) {
            instance = new SingletonThreadSafe;
        }

        volatile int dummy{};

        return *instance;
    }

private:

    SingletonThreadSafe() = default;

    ~SingletonThreadSafe() = default;

    static SingletonThreadSafe *instance;

public:

    SingletonThreadSafe(const SingletonThreadSafe &) = delete;

    SingletonThreadSafe &operator=(const SingletonThreadSafe &) = delete;

    SingletonThreadSafe(SingletonThreadSafe &&) = delete;

    SingletonThreadSafe &operator=(SingletonThreadSafe &&) = delete;
};

SingletonThreadSafe *SingletonThreadSafe::instance = nullptr;

constexpr auto M1{1'000'000LL};
constexpr auto M400{400LL * M1};

auto getTimeV1() -> double {

    const auto begin{std::chrono::steady_clock::now()};

    for (size_t i = 0; i <= M400; ++i) {
        SingletonThreadSafe::getInstance();
    }

    const auto end{std::chrono::steady_clock::now()};
    const auto res{std::chrono::duration_cast<std::chrono::duration<double>>(end - begin).count()};

    return res;
}

auto getTimeV2(double &res) -> void {

    const auto begin{std::chrono::steady_clock::now()};

    for (size_t i = 0; i <= M400; ++i) {
        SingletonThreadSafe::getInstance();
    }

    const auto end{std::chrono::steady_clock::now()};

    res = std::chrono::duration_cast<std::chrono::duration<double>>(end - begin).count();
}

auto main() -> int {

    const auto NT{(size_t) 4};
    std::cout << std::fixed;
    std::cout << std::setprecision(10);

    {
        std::cout << "------------------------------------------------>> 1" << std::endl;

        auto futures{std::vector<std::future<double>>(NT)};

        for (auto &el : futures) {
            el = std::async(std::launch::async, getTimeV1);
        }

        auto res{(double) 0};

        for (auto &el : futures) {
            res += el.get();
        }

        std::cout << (res / NT) << std::endl;
    }

    {
        std::cout << "------------------------------------------------>> 2" << std::endl;

        auto futuresAndResults{std::vector<std::pair<std::future<void>, double>>(NT)};

        for (auto &el : futuresAndResults) {
            el.first = std::async(std::launch::async, getTimeV2, std::ref(el.second));
        }

        for (auto &el : futuresAndResults) {
            el.first.get();
        }

        auto res{(double) 0};

        for (auto &el : futuresAndResults) {
            res += el.second;
        }

        std::cout << (res / NT) << std::endl;
    }
}