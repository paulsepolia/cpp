#include "TrivialClock.h"

TrivialClock::time_point TrivialClock::now_us_{};
const bool TrivialClock::is_steady{ false };

void TrivialClock::advance(duration d) noexcept {
    now_us_ += d;
}

void TrivialClock::reset_to_epoch() noexcept {
    now_us_ -= (now_us_ - time_point());
}

TrivialClock::time_point TrivialClock::now() noexcept {
    return now_us_;
}