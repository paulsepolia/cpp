#include "TrivialClock.h"
#include <iostream>
#include <iomanip>
#include <climits>

auto main() -> int {

    std::cout << std::boolalpha << std::endl;

    {
        std::cout << "--> test --> 1" << std::endl;
        TrivialClock::reset_to_epoch();

        const auto t0{TrivialClock::now()};
        const auto t1{TrivialClock::now()};

        std::cout << (std::chrono::microseconds(0) == (t1 - t0)) << std::endl;

        std::cout << "--> test --> 2" << std::endl;

        TrivialClock::advance(std::chrono::microseconds(10));
        const auto t2 { TrivialClock::now()};

        std::cout << (std::chrono::microseconds(10) == (t2 - t0)) << std::endl;

        std::cout << "--> test --> 3" << std::endl;

        TrivialClock::reset_to_epoch();
        const auto t3 { TrivialClock::now()};
        std::cout << (std::chrono::microseconds(0) == (t3 - t0)) << std::endl;
    }

    {
        std::cout << "--> test --> 4" << std::endl;

        const auto t1 { TrivialClock::now() };
        TrivialClock::advance(std::chrono::milliseconds(100));
        const auto t2 {TrivialClock::now()};
        const auto elapsed_us {(int64_t) std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count()};

        std::cout << (100000 == elapsed_us) << std::endl;
    }

    {
        std::cout << "--> test --> 5" << std::endl;

        std::cout << (sizeof(TrivialClock::rep) * CHAR_BIT >= 62) << std::endl;
    }
}
