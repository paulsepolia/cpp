#!/bin/bash

  clang++   -O3          \
            -Wall        \
            -std=gnu++2a \
            -pthread     \
            TrivialClock.cpp \
            main.cpp \
            -o x_clang
