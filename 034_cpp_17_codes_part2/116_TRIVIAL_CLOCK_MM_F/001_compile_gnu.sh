#!/bin/bash

  g++-9.2.0  -O3          \
             -Wall        \
             -std=gnu++2a \
             -pthread     \
             TrivialClock.cpp \
             main.cpp \
             -o x_gnu
