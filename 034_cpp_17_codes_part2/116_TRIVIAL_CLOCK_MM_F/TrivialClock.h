#pragma once

/** This class satisfies the TrivialClock requirement
 * (http://en.cppreference.com/w/cpp/concept/TrivialClock) and as such
 * can be used in place of any standard clock
 * (e.g. std::chrono::system_clock).
 * The clock uses an uint64_t internally, so it can store all
 * nanoseconds in a century. This is consistent with the precision
 * required of std::chrono::nanoseconds in C++11.
 * Example usage:
 * TrivialClock::time_point t1 = TrivialClock::now();
 * TrivialClock::advance(std::chrono::milliseconds(100));
 * TrivialClock::time_point t2 = TrivialClock::now();
 * auto elapsed_us = std::chrono::duration_cast<
 * std::chrono::microseconds>(t2 - t1).count();
 * assert(100000 == elapsed_us);
 */

#include <chrono>
#include <cstdint>

class TrivialClock {
public:

    using rep = int64_t;
    using period = std::ratio<1LL, 1'000'000'000LL>;
    using duration = std::chrono::duration<rep, period>;
    using time_point = std::chrono::time_point<TrivialClock>;

    static void advance(duration d) noexcept;

    static void reset_to_epoch() noexcept;

    static time_point now() noexcept;

    TrivialClock() = delete;

    ~TrivialClock() = delete;

    TrivialClock(TrivialClock const &) = delete;

    TrivialClock(TrivialClock &&) = delete;

    TrivialClock&operator=(const TrivialClock &) = delete;

    TrivialClock&operator=(TrivialClock &&) = delete;

private:

    static time_point now_us_;
    static const bool is_steady;
};
