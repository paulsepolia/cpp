#include <iostream>
#include <cstdint>
#include <iomanip>
#include <chrono>
#include <memory>
#include <vector>


int main() {

    {
        std::cout << " --> example --> 1 --> start" << std::endl;

        double d1 = 111.11;
        double d2 = 222.22;
        const double *dp = &d1;

        std::cout << "  dp = " << dp << std::endl;
        std::cout << " *dp = " << *dp << std::endl;

        *const_cast<double *>(dp) = d2;

        std::cout << "  dp = " << dp << std::endl;
        std::cout << " *dp = " << *dp << std::endl;
        std::cout << " d1 = " << d1 << std::endl;
        std::cout << " d2 = " << d2 << std::endl;

        std::cout << " --> example --> 1 --> end" << std::endl;
    }

    {
        std::cout << " --> example --> 2 --> start" << std::endl;

        const double d1 = 111.11;
        double d2 = 222.22;
        const double *dp = &d1;

        std::cout << "  dp = " << dp << std::endl;
        std::cout << " *dp = " << *dp << std::endl;

        *const_cast<double *>(dp) = d2;

        std::cout << "  dp = " << dp << std::endl;
        std::cout << " *dp = " << *dp << std::endl;
        std::cout << " d1 = " << d1 << std::endl;
        std::cout << " d2 = " << d2 << std::endl;

        std::cout << " --> example --> 2 --> end" << std::endl;
    }

}