#include <ctime>
#include <cmath>
#include <iostream>

const auto B1{(double)std::pow(10.0, 9.0)};

static double timespec_to_seconds(struct timespec* ts) {
    return (double)ts->tv_sec + (double)ts->tv_nsec / B1;
}

const auto DO_MAX{(size_t) (std::pow(10.0, 8.0))};

auto main() -> int {

    {
        std::cout << "------------------------------------------------------->> 1" << std::endl;

        struct timespec start{};
        struct timespec end{};

        auto i{(size_t) 1};
        auto dt{(double) 0.0};
        auto dt_min{(double) std::numeric_limits<double>::max()};
        auto dt_max{(double) std::numeric_limits<double>::min()};

        auto idx_max{(size_t) 0};
        auto idx_min{(size_t) 0};

        while (i <= DO_MAX) {

            if (clock_gettime(CLOCK_MONOTONIC, &start)) {
                std::cout << "--> ERROR invoking the CLOCK_MONOTONIC - start" << std::endl;
            }

            if (clock_gettime(CLOCK_MONOTONIC, &end)) {
                std::cout << "--> ERROR invoking the CLOCK_MONOTONIC - end" << std::endl;
            }

            dt = timespec_to_seconds(&end) - timespec_to_seconds(&start);

            if(dt < dt_min) {
                dt_min = dt;
                idx_min = i;
            }

            if(dt > dt_max) {
                dt_max = dt;
                idx_max = i;
            }

            ++i;
        }

        std::cout << " --> the minimum delta detected after many runs = " << int64_t (dt_min*B1) << ", at position: " << idx_min << std::endl;
        std::cout << " --> the maximum delta detected after many runs = " << int64_t (dt_max*B1) << ", at position: " << idx_max << std::endl;
    }
}
