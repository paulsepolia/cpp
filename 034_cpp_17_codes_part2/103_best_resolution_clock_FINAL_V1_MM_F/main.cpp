#include <iostream>
#include <mutex>
#include <cassert>
#include <chrono>
#include <thread>

#if _WIN32 || _WIN64

#include <windows.h>

#elif __linux__

#include <ctime>

#else // generic Unix

#include <sys/time.h>

#endif

class ClockNano {

private:

    static std::mutex mtx;

public:

    ClockNano() = delete;

    ClockNano(const ClockNano &) = delete;

    ClockNano(ClockNano &&) = delete;

    ClockNano &operator=(const ClockNano &) = delete;

    ClockNano &operator=(ClockNano &&) = delete;

public:

    static int64_t BeginOfTimeInNanoseconds;

    // The only member function to give back the timestamp
    // since the time fo its first call

    static auto getNanosecondsSinceFirstCall() -> int64_t {

        std::lock_guard<std::mutex> lck(mtx);

#if _WIN32 || _WIN64

        constexpr auto OA_ZERO_TICKS{(int64_t) 94'353'120'000'000'000LL}; // 12/30/1899 12:00am in ticks
        static auto firstTimeFlag{(bool) true};
        auto timePointNow{(int64_t) 0LL};
        LARGE_INTEGER dt;

        if (firstTimeFlag) {

            LPFILETIME ft{new FILETIME};

            GetSystemTimePreciseAsFileTime(ft);
            dt.LowPart = ft->dwLowDateTime;
            dt.HighPart = ft->dwHighDateTime;

            timePointNow = BeginOfTimeInNanoseconds = (dt.QuadPart - OA_ZERO_TICKS) * 10LL;
            firstTimeFlag = false;
            delete ft;
        } else {

            LPFILETIME ft{new FILETIME};

            GetSystemTimePreciseAsFileTime(ft);
            dt.LowPart = ft->dwLowDateTime;
            dt.HighPart = ft->dwHighDateTime;

            timePointNow = (dt.QuadPart - OA_ZERO_TICKS) * 10LL;
            delete ft;
        }

        return timePointNow - BeginOfTimeInNanoseconds;

#elif __linux__

        constexpr auto B1{ (int64_t)1'000'000'000LL };
        static auto firstTimeFlag{ (bool)true };
        auto timePointNow{ (int64_t)0LL };
        struct timespec ts {};
        const auto clockMonotonicStatus{ (int)clock_gettime(CLOCK_MONOTONIC, &ts) };

        if (clockMonotonicStatus == 0) {

            if (firstTimeFlag) {
                timePointNow = BeginOfTimeInNanoseconds = B1 * (int64_t)ts.tv_sec + (int64_t)ts.tv_nsec;
                firstTimeFlag = false;
            }
            else {
                timePointNow = B1 * (int64_t)ts.tv_sec + (int64_t)ts.tv_nsec;
            }
        }
        else {

            const auto clockRealTimeStatus{ (int)clock_gettime(CLOCK_REALTIME, &ts) };

            assert(clockRealTimeStatus == 0);

            if (firstTimeFlag) {
                timePointNow = BeginOfTimeInNanoseconds = B1 * (int64_t)ts.tv_sec + (int64_t)ts.tv_nsec;
                firstTimeFlag = false;
            }
            else {
                timePointNow = B1 * (int64_t)ts.tv_sec + (int64_t)ts.tv_nsec;
            }
        }

        return (timePointNow - BeginOfTimeInNanoseconds);

#else // generic Unix

        constexpr auto K1{ (int64_t)1'000 };
        constexpr auto B1{ (int64_t)1'000'000'000LL };
        static auto firstTimeFlag{ (bool)true };
        auto timePointNow{ (int64_t)0LL };
        struct timeval tv {};
        auto status{ (int)gettimeofday(&tv, nullptr) };

        assert(status == 0);

        if (firstTimeFlag) {
            timePointNow = BeginOfTimeInNanoseconds = B1 * (int64_t)tv.tv_sec + K1 * (int64_t)tv.tv_usec;
            firstTimeFlag = false;
        }
        else {
            timePointNow = B1 * (int64_t)tv.tv_sec + K1 * (int64_t)tv.tv_usec;
        }

        return (timePointNow - BeginOfTimeInNanoseconds);
#endif
    }
};

std::mutex ClockNano::mtx{};
int64_t ClockNano::BeginOfTimeInNanoseconds = 0;

// driver program

auto main() -> int {

    {
        std::cout << "------> seconds" << std::endl;

        constexpr auto DO_MAX{(size_t) 10};

        for (size_t i = 0; i < DO_MAX; i++) {

            std::cout << " ----------------------------------------------------->> i = " << i << std::endl;

            std::cout << ClockNano::getNanosecondsSinceFirstCall() << std::endl;
            std::cout << ClockNano::getNanosecondsSinceFirstCall() << std::endl;
            std::cout << ClockNano::getNanosecondsSinceFirstCall() << std::endl;
            std::cout << ClockNano::getNanosecondsSinceFirstCall() << std::endl;

            std::cout << ClockNano::BeginOfTimeInNanoseconds << std::endl;

            std::this_thread::sleep_for(std::chrono::seconds(1));
        }
    }

    {
        std::cout << "------> milliseconds" << std::endl;

        constexpr auto DO_MAX{(size_t) 100};

        for (size_t i = 0; i < DO_MAX; i++) {

            std::cout << " ----------------------------------------------------->> i = " << i << std::endl;

            std::cout << ClockNano::getNanosecondsSinceFirstCall() << std::endl;
            std::cout << ClockNano::getNanosecondsSinceFirstCall() << std::endl;
            std::cout << ClockNano::getNanosecondsSinceFirstCall() << std::endl;
            std::cout << ClockNano::getNanosecondsSinceFirstCall() << std::endl;

            std::cout << ClockNano::BeginOfTimeInNanoseconds << std::endl;

            std::this_thread::sleep_for(std::chrono::milliseconds(1));
        }
    }

    {
        std::cout << "------> microseconds" << std::endl;

        constexpr auto DO_MAX{(size_t) 100};

        for (size_t i = 0; i < DO_MAX; i++) {

            std::cout << " ----------------------------------------------------->> i = " << i << std::endl;

            std::cout << ClockNano::getNanosecondsSinceFirstCall() << std::endl;
            std::cout << ClockNano::getNanosecondsSinceFirstCall() << std::endl;
            std::cout << ClockNano::getNanosecondsSinceFirstCall() << std::endl;
            std::cout << ClockNano::getNanosecondsSinceFirstCall() << std::endl;

            std::cout << ClockNano::BeginOfTimeInNanoseconds << std::endl;

            std::this_thread::sleep_for(std::chrono::microseconds(1));
        }
    }

    {
        std::cout << "------> nanoseconds" << std::endl;

        constexpr auto DO_MAX{(size_t) 100};

        for (size_t i = 0; i < DO_MAX; i++) {

            std::cout << " ----------------------------------------------------->> i = " << i << std::endl;

            std::cout << ClockNano::getNanosecondsSinceFirstCall() << std::endl;
            std::cout << ClockNano::getNanosecondsSinceFirstCall() << std::endl;
            std::cout << ClockNano::getNanosecondsSinceFirstCall() << std::endl;
            std::cout << ClockNano::getNanosecondsSinceFirstCall() << std::endl;

            std::cout << ClockNano::BeginOfTimeInNanoseconds << std::endl;

            std::this_thread::sleep_for(std::chrono::nanoseconds(1));
        }
    }
}
