#include <iostream>

class A1 {
public:
    virtual auto f() const -> void {
        std::cout << " --> A1" << std::endl;
    }
};

class A2 {
public:
    auto f() const -> void {
        std::cout << " --> A1" << std::endl;
    }
};

auto main() -> int {

    A1 a1;
    A2 a2;
    A1 *pa1{&a1};
    A2 *pa2{&a2};

    std::cout << "--> sizeof(A1) = " << sizeof(A1) << std::endl;
    std::cout << "--> sizeof(A2) = " << sizeof(A2) << std::endl;
    std::cout << "--> sizeof(a1) = " << sizeof(a1) << std::endl;
    std::cout << "--> sizeof(a2) = " << sizeof(a2) << std::endl;
    std::cout << "--> sizeof(pa1) = " << sizeof(pa1) << std::endl;
    std::cout << "--> sizeof(pa2) = " << sizeof(pa2) << std::endl;
}

