#include <iostream>
#include <mutex>
#include <chrono>
#include <thread>
#include <vector>
#include <algorithm>
#include <chrono>

#if _WIN32 || _WIN64

#include <Windows.h>

#elif __linux__

#include <ctime>

#else // generic Unix

#include <sys/time.h>

#endif

class ClockNano {

    static constexpr auto H1{(int64_t) 100LL};
    static constexpr auto K1{(int64_t) 1'000};
    static constexpr auto B1{(int64_t) 1'000'000'000LL};
    static constexpr auto OA_ZERO_TICKS{(int64_t) 94'353'120'000'000'000LL};

private:

    static int64_t beginOfTimeInNanoseconds;
    static int64_t nanosecondsSinceBootTimeAtFirstCall;
    static int64_t nanosecondsSinceUnixEpochAtFirstCall;
    static int64_t nanosecondsSinceWindowsEpochAtFirstCall;
    static std::once_flag beginOfTimeOnlyOnceFlag;

public:

    ClockNano() = delete;

    ClockNano(const ClockNano &) = delete;

    ClockNano(ClockNano &&) = delete;

    ClockNano &operator=(const ClockNano &) = delete;

    ClockNano &operator=(ClockNano &&) = delete;

public:

    /**
     * The only member function to give back the timestamp in nanoseconds
     * since the time of its first call.
     * We use that function to measure time intervals in nanoseconds.
     * Those time intervals are syntonized with the system clock.
     * We can also add the current time stamp: GetNanosecondsSinceFirstCall() with the
     * GetBeginOfTimeInNanoseconds() to get the "now" timestamp,
     * but the actual value differs based on which clock is used and which OS.
     */
    inline static auto GetNanosecondsSinceFirstCall() -> int64_t {

#if _WIN32 || _WIN64

        auto timePointNow{(int64_t) 0LL};
        LARGE_INTEGER dt;
        LPFILETIME ft{new FILETIME};

        GetSystemTimePreciseAsFileTime(ft);
        dt.LowPart = ft->dwLowDateTime;
        dt.HighPart = ft->dwHighDateTime;

        timePointNow = (dt.QuadPart - OA_ZERO_TICKS);
        delete ft;

        std::call_once(beginOfTimeOnlyOnceFlag,
                       ClockNano::SetNanosecondsSinceWindowsEpochAtFirstCall,
                       timePointNow);

        return timePointNow * H1 - nanosecondsSinceWindowsEpochAtFirstCall;

#elif __linux__

        auto timePointNow{ (int64_t)0LL };
        struct timespec ts {};
        const auto clockMonotonicStatus{ (int)clock_gettime(CLOCK_MONOTONIC, &ts) };

        if (clockMonotonicStatus == 0) {

            clock_gettime(CLOCK_MONOTONIC, &ts);
            timePointNow = B1 * (int64_t)ts.tv_sec + (int64_t)ts.tv_nsec;

            std::call_once(beginOfTimeOnlyOnceFlag,
                           ClockNano::SetNanosecondsSinceUnixEpochAtFirstCallLinuxMonotonic,
                           timePointNow);
        }
        else {

            clock_gettime(CLOCK_REALTIME, &ts);
            timePointNow = B1 * (int64_t)ts.tv_sec + (int64_t)ts.tv_nsec;

            std::call_once(beginOfTimeOnlyOnceFlag,
                           ClockNano::SetNanosecondsSinceUnixEpochAtFirstCallLinuxRealtime,
                           timePointNow);
        }

        return (timePointNow - beginOfTimeInNanoseconds);

#else // generic Unix

        auto timePointNow{(int64_t) 0LL};
        struct timeval tv{};
        gettimeofday(&tv, nullptr);
        timePointNow = B1 * (int64_t) tv.tv_sec + K1 * (int64_t) tv.tv_usec;

        std::call_once(beginOfTimeOnlyOnceFlag,
                       ClockNano::SetNanosecondsSinceUnixEpochAtFirstCallUnix,
                       timePointNow);

        return (timePointNow - beginOfTimeInNanoseconds);
#endif
    }

    /**
     * Gives back the beginning of time in nanoseconds.
     * That time varies depending which clock is being and which OS.
     * For example under Linux could be the boot time of the machine if the
     * MONOTONIC_CLOCK is available or the Unix Epoch UTC time
     * if the MONOTONIC_CLOCK is not available.
     * On Windows OS is always the time since 12/30/1899 12:00am UTC.
     * On a Unix system is the Unix Epoch UTC time.
     * The exact value is captured when we call for the first time the
     * function: GetNanosecondsSinceFirstCall()
     * If we have not call yet the above function then is the value is always zero.
     */
    inline static auto GetBeginOfTimeInNanoseconds() -> int64_t {
        return beginOfTimeInNanoseconds;
    }

    /**
     * Gives back the nanoseconds since Unix Epoch
     * at the time of first call of the function GetNanosecondsSinceFirstCall().
     * The function used is a std::chrono function, so a system time.
     */
    inline static auto GetNanosecondsSinceUnixEpochAtFirstCall() -> int64_t {
        return nanosecondsSinceUnixEpochAtFirstCall;
    }

    /**
     * Gives back the nanoseconds since Unix Epoch "now".
     * It is syntonized with system clock.
     */
    inline static auto GetNanosecondsSinceUnixEpochSyntonized() -> int64_t {

        return nanosecondsSinceUnixEpochAtFirstCall + GetNanosecondsSinceFirstCall();
    }

    /**
     * Gives back the nanoseconds since system epoch "now".
     * So, on Linux and with CLOCK_MONOTONIC is the time passed since boot time.
     * On Linux and with CLOCK_REALTIME is the time passed since Unix Epoch.
     * On Windows is the time passed since Windows Epoch (1899/12/30 midnight, UTC).
     * It is syntonized with system clock.
     */
    inline static auto GetNanosecondsSinceSystemEpochSyntonized() -> int64_t {

        return GetBeginOfTimeInNanoseconds() + GetNanosecondsSinceFirstCall();
    }

private:

#if _WIN32 || _WIN64

    /**
     * Sets nanoseconds since Windows Epoch "now".
     */
    inline static auto SetNanosecondsSinceWindowsEpochAtFirstCall(int64_t &timePointNow) -> void {

        LARGE_INTEGER dt;
        LPFILETIME ft{new FILETIME};

        GetSystemTimePreciseAsFileTime(ft);
        dt.LowPart = ft->dwLowDateTime;
        dt.HighPart = ft->dwHighDateTime;

        timePointNow = (dt.QuadPart - OA_ZERO_TICKS);

        nanosecondsSinceWindowsEpochAtFirstCall = H1 * timePointNow;
        beginOfTimeInNanoseconds = H1 * timePointNow;
        nanosecondsSinceUnixEpochAtFirstCall = std::chrono::duration_cast<std::chrono::nanoseconds>(
                std::chrono::system_clock::now().time_since_epoch()).count();
    }

#elif __linux__

    /**
     * Sets nanoseconds since boot time "now" using CLOCK_MONOTONIC.
     */
    inline static auto SetNanosecondsSinceUnixEpochAtFirstCallLinuxMonotonic(int64_t &timePointNow) -> void {

        struct timespec ts {};
        clock_gettime(CLOCK_MONOTONIC, &ts);

        timePointNow = B1 * (int64_t)ts.tv_sec + (int64_t)ts.tv_nsec;

        nanosecondsSinceBootTimeAtFirstCall = timePointNow;
        beginOfTimeInNanoseconds = timePointNow;
        nanosecondsSinceUnixEpochAtFirstCall = std::chrono::duration_cast<std::chrono::nanoseconds>(
                std::chrono::system_clock::now().time_since_epoch()).count();
    }

    /**
     * Sets nanoseconds since Unix Epoch "now" using CLOCK_REALTIME.
     */
    inline static auto SetNanosecondsSinceUnixEpochAtFirstCallLinuxRealtime(int64_t &timePointNow) -> void {

        struct timespec ts {};
        clock_gettime(CLOCK_REALTIME, &ts);

        timePointNow = B1 * (int64_t)ts.tv_sec + (int64_t)ts.tv_nsec;
        nanosecondsSinceUnixEpochAtFirstCall = timePointNow;
        beginOfTimeInNanoseconds = timePointNow;
    }
#else

    /**
     * Sets nanoseconds since Unix Epoch "now" using Unix clock.
     */
    inline static auto SetNanosecondsSinceUnixEpochAtFirstCallUnix(int64_t &timePointNow) -> void {

        struct timeval tv{};
        gettimeofday(&tv, nullptr);

        timePointNow = B1 * (int64_t) tv.tv_sec + K1 * (int64_t) tv.tv_usec;
        nanosecondsSinceUnixEpochAtFirstCall = timePointNow;
        beginOfTimeInNanoseconds = timePointNow;
    }

#endif
};

int64_t ClockNano::beginOfTimeInNanoseconds = 0;
int64_t ClockNano::nanosecondsSinceBootTimeAtFirstCall = 0;
int64_t ClockNano::nanosecondsSinceUnixEpochAtFirstCall = 0;
int64_t ClockNano::nanosecondsSinceWindowsEpochAtFirstCall = 0;
std::once_flag ClockNano::beginOfTimeOnlyOnceFlag;

auto main() -> int {

    constexpr auto NUM_THREADS{(size_t) 4};
    constexpr auto DIM_MAX{(size_t) 10'000};

    {
        std::cout << std::endl;
        std::cout << "-->> test 1 --> ClockNano::GetNanosecondsSinceFirstCall()" << std::endl;
        std::cout << std::endl;

        std::mutex mtx{};

        auto ld{[&]() {

            mtx.lock();
            std::cout << "-->> thread id = " << std::this_thread::get_id() << std::endl;
            mtx.unlock();

            auto a{std::vector<int64_t>(DIM_MAX)};

            for (size_t i = 0; i < DIM_MAX; i++) {
                a[i] = ClockNano::GetNanosecondsSinceFirstCall();
            }

            auto ave{(double) 0.0};

            for (size_t i = 1; i < DIM_MAX; i++) {
                ave += (double) (a[i] - a[i - 1]);
            }

            mtx.lock();
            std::cout << "-->> average time between consecutive calls is (ns) = "
                      << (int64_t) (ave / (DIM_MAX - 1)) << std::endl;
            mtx.unlock();
        }
        };

        auto vecTh{std::vector<std::thread>(NUM_THREADS)};

        for (auto &el : vecTh) {
            el = std::thread(ld);
        }

        for (auto &el : vecTh) {
            el.join();
        }
    }

    {
        std::cout << std::endl;
        std::cout << "-->> test 2 --> ClockNano::BeginOfTimeInNanoseconds()" << std::endl;
        std::cout << "-->>        --> ClockNano::BeginOfTimeInNanosecondsDetailsString()" << std::endl;
        std::cout << std::endl;

        std::mutex mtx{};

        auto ld{[&]() {

            mtx.lock();

            std::cout << "-->> thread id = " << std::this_thread::get_id() << std::endl;

            mtx.unlock();

            auto a{std::vector<int64_t>(DIM_MAX)};

            for (size_t i = 0; i < DIM_MAX; i++) {
                a[i] = ClockNano::GetBeginOfTimeInNanoseconds();
            }

            std::sort(a.begin(), a.end());
            const auto it{std::unique(a.begin(), a.end())};
            a.resize(std::distance(a.begin(), it));

            mtx.lock();
            std::cout << "-->> the value should be exact one (1) = " << a.size() << std::endl;
            mtx.unlock();
        }
        };

        auto vecTh{std::vector<std::thread>(NUM_THREADS)};

        for (auto &el : vecTh) {
            el = std::thread(ld);
        }

        for (auto &el : vecTh) {
            el.join();
        }
    }

    {
        std::cout << std::endl;
        std::cout << "-->> test 3 --> ClockNano::GetNanosecondsSinceUnixEpochAtFirstCall()" << std::endl;
        std::cout << std::endl;

        std::cout << ClockNano::GetNanosecondsSinceUnixEpochAtFirstCall() << std::endl;
    }

    {
        std::cout << std::endl;
        std::cout << "-->> test 4 --> ClockNano::GetNanosecondsSinceUnixEpochSyntonized()" << std::endl;
        std::cout << std::endl;

        std::cout << ClockNano::GetNanosecondsSinceUnixEpochSyntonized() << std::endl;
    }

    {
        std::cout << std::endl;
        std::cout << "-->> test 5 --> ClockNano::GetNanosecondsSinceSystemEpochSyntonized()" << std::endl;
        std::cout << std::endl;

        std::cout << ClockNano::GetNanosecondsSinceSystemEpochSyntonized() << std::endl;
    }

    {
        std::cout << std::endl;
        std::cout << "-->> test 6 --> ClockNano::GetNanosecondsSinceFirstCall()" << std::endl;
        std::cout << std::endl;

        std::cout << ClockNano::GetNanosecondsSinceFirstCall() << std::endl;

        std::this_thread::sleep_for(std::chrono::seconds(1));

        std::cout << ClockNano::GetNanosecondsSinceFirstCall() << std::endl;

        std::this_thread::sleep_for(std::chrono::seconds(1));

        std::cout << ClockNano::GetNanosecondsSinceFirstCall() << std::endl;
    }
}