#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <cmath>
#include <iomanip>

const auto NUM_ELEMS_TO_CREATE{(size_t) 1'000'000};
const auto NUM_ELEMS_TO_GET{(size_t) 10'000'000};
const auto DO_MAX{(size_t) 10'000'000};
const auto NT_TO_CREATE_DATA{(size_t) 10};
const auto NT_TO_GET_DATA{(size_t) 2};
const auto NT_TO_USE_DATA{(size_t) 3};
const auto COEF_CREATE{(size_t) 50};
const auto COEF_USE{(size_t) 10};

auto data1{std::vector<double>{}};
auto mtx{std::mutex{}};

auto create_data() -> void {

    if (data1.size() <= NUM_ELEMS_TO_CREATE * COEF_CREATE) {
        for (size_t i = 0; i < NUM_ELEMS_TO_CREATE; i++) {
            data1.push_back(std::sin((double) i));
        }
    }
}

auto get_data() -> std::vector<double> {

    auto data_local{std::vector<double>{}};

    for (size_t i = 0; i < NUM_ELEMS_TO_GET; i++) {
        data_local.push_back(data1[i]);
    }

    return data_local;
}

auto delete_data() -> void {

    data1.erase(data1.begin(), data1.begin() + NUM_ELEMS_TO_GET);
    data1.shrink_to_fit();
}

auto main() -> int {

    std::cout << std::fixed;
    std::cout.precision(10);

    auto my_data{std::vector<double>{}};
    auto data_not_ready_cntr{(size_t) 0};

    auto create_data_L = [&]() {

        std::lock_guard<std::mutex> lock(mtx);
        create_data();
    };

    auto get_data_L = [&]() {

        std::lock_guard<std::mutex> lock(mtx);

        if (data1.size() >= NUM_ELEMS_TO_GET) {

            const auto tmp{get_data()};
            my_data.insert(my_data.end(), tmp.begin(), tmp.end());
            delete_data();

        } else {
            data_not_ready_cntr++;
        }
    };

    auto use_data_L = [&]() {

        std::lock_guard<std::mutex> lock(mtx);

        if (my_data.size() >= COEF_USE * NUM_ELEMS_TO_GET) {
            std::cout << " --> my_data.size() = " << my_data.size() << std::endl;
            std::cout << my_data[1] << std::endl;
            std::cout << my_data[2] << std::endl;
            my_data.clear();
            my_data.shrink_to_fit();

            std::cout << " --> thread id = " << std::this_thread::get_id() << std::endl;
            std::cout << " --> got all the data ..." << std::endl;
            std::cout.flush();
        }
    };

    auto vec_create_data_threads{std::vector<std::thread>{}};
    vec_create_data_threads.resize(NT_TO_CREATE_DATA);

    auto vec_get_data_threads{std::vector<std::thread>{}};
    vec_get_data_threads.resize(NT_TO_GET_DATA);

    auto vec_use_data_threads{std::vector<std::thread>{}};
    vec_use_data_threads.resize(NT_TO_USE_DATA);

    auto l1 = [&]() {

        for (size_t i = 0; i < DO_MAX; i++) {

            for (auto &el: vec_create_data_threads) {
                el = std::thread(create_data_L);
            }

            for (auto &el: vec_create_data_threads) {
                el.join();
            }
        }
    };

    auto l2 = [&]() {

        for (size_t i = 0; i < DO_MAX; i++) {

            for (auto &el: vec_get_data_threads) {
                el = std::thread(get_data_L);
            }

            for (auto &el: vec_get_data_threads) {
                el.join();
            }
        }
    };

    auto l3 = [&]() {

        for (size_t i = 0; i < DO_MAX; i++) {

            for (auto &el: vec_use_data_threads) {
                el = std::thread(use_data_L);
            }

            for (auto &el: vec_use_data_threads) {
                el.join();
            }
        }
    };

    std::thread th1(l1);
    std::thread th2(l2);
    std::thread th3(l3);

    th1.join();
    th2.join();
    th3.join();
}
