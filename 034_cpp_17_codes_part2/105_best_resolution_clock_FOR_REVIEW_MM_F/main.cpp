#include <iostream>
#include <mutex>
#include <cassert>
#include <chrono>
#include <thread>
#include <vector>
#include <algorithm>
#include <string>

#if _WIN32 || _WIN64

#include <Windows.h>

#elif __linux__

#include <ctime>

#else // generic Unix

#include <sys/time.h>

#endif

class ClockNano {

private:

    static std::mutex mtx;
    static int64_t beginOfTimeInNanoseconds;
    static int64_t beginOfTimeIn100Nanoseconds;
    static int64_t nanosecondsSinceUnixEpochAtFirstCall;
    static std::string clockUsedDetails;

public:

    ClockNano() = delete;

    ClockNano(const ClockNano &) = delete;

    ClockNano(ClockNano &&) = delete;

    ClockNano &operator=(const ClockNano &) = delete;

    ClockNano &operator=(ClockNano &&) = delete;

public:

    /**
     * The only member function to give back the timestamp in nanoseconds
     * since the time of its first call.
     * We use that function to measure time intervals in nanoseconds.
     * Those time intervals are syntonized with the system clock.
     * We can also add the current time stamp: GetNanosecondsSinceFirstCall() with the
     * GetBeginOfTimeInNanoseconds() to get the "now" timestamp,
     * but the actual value differs based on which clock is used and which OS.
     */
    inline static auto GetNanosecondsSinceFirstCall() -> int64_t {

        std::lock_guard<std::mutex> lck(mtx);

#if _WIN32 || _WIN64

        const auto StartDateWindows{(std::string) (" nanoseconds since 1899-12-30T00:00:00Z")};
        constexpr auto OA_ZERO_TICKS{(int64_t) 94'353'120'000'000'000LL}; // 12/30/1899 12:00am in ticks
        static auto firstTimeFlag{(bool) true};
        auto timePointNow{(int64_t) 0LL};
        LARGE_INTEGER dt;

        if (firstTimeFlag) {

            clockUsedDetails = StartDateWindows;
            LPFILETIME ft{new FILETIME};

            nanosecondsSinceUnixEpochAtFirstCall = GetNanosecondsSinceUnixEpochHelp();
            GetSystemTimePreciseAsFileTime(ft);
            dt.LowPart = ft->dwLowDateTime;
            dt.HighPart = ft->dwHighDateTime;

            timePointNow = beginOfTimeIn100Nanoseconds = (dt.QuadPart - OA_ZERO_TICKS);
            firstTimeFlag = false;
            delete ft;
        } else {

            LPFILETIME ft{new FILETIME};

            GetSystemTimePreciseAsFileTime(ft);
            dt.LowPart = ft->dwLowDateTime;
            dt.HighPart = ft->dwHighDateTime;

            timePointNow = (dt.QuadPart - OA_ZERO_TICKS);
            delete ft;
        }

        return (timePointNow - beginOfTimeIn100Nanoseconds) * 100LL;

#elif __linux__

        const auto StartDateUnixEpoch{ (std::string)(" nanoseconds since 1970-01-01T00:00:00Z") };
        const auto StartDateBootTime{ (std::string)(" nanoseconds since boot time") };
        constexpr auto B1{ (int64_t)1'000'000'000LL };
        static auto firstTimeFlag{ (bool)true };
        auto timePointNow{ (int64_t)0LL };
        struct timespec ts {};
        const auto clockMonotonicStatus{ (int)clock_gettime(CLOCK_MONOTONIC, &ts) };

        if (clockMonotonicStatus == 0) {

            if (firstTimeFlag) {
                clockUsedDetails = StartDateBootTime;
                nanosecondsSinceUnixEpochAtFirstCall = GetNanosecondsSinceUnixEpochHelp();
                clock_gettime(CLOCK_MONOTONIC, &ts);
                timePointNow = beginOfTimeInNanoseconds = B1 * (int64_t)ts.tv_sec + (int64_t)ts.tv_nsec;
                firstTimeFlag = false;
            }
            else {
                timePointNow = B1 * (int64_t)ts.tv_sec + (int64_t)ts.tv_nsec;
            }
        }
        else {

            const auto clockRealTimeStatus{ (int)clock_gettime(CLOCK_REALTIME, &ts) };

            assert(clockRealTimeStatus == 0);

            if (firstTimeFlag) {
                nanosecondsSinceUnixEpochAtFirstCall = GetNanosecondsSinceUnixEpochHelp();
                clock_gettime(CLOCK_REALTIME, &ts);
                clockUsedDetails = StartDateUnixEpoch;
                timePointNow = beginOfTimeInNanoseconds = B1 * (int64_t)ts.tv_sec + (int64_t)ts.tv_nsec;
                firstTimeFlag = false;
            }
            else {
                timePointNow = B1 * (int64_t)ts.tv_sec + (int64_t)ts.tv_nsec;
            }
        }

        return (timePointNow - beginOfTimeInNanoseconds);

#else // generic Unix

        const auto StartDateUnixEpoch{(std::string) (" nanoseconds since 1970-01-01T00:00:00Z")};
        constexpr auto K1{(int64_t) 1'000};
        constexpr auto B1{(int64_t) 1'000'000'000LL};
        static auto firstTimeFlag{(bool) true};
        auto timePointNow{(int64_t) 0LL};
        struct timeval tv{};
        auto status{(int) gettimeofday(&tv, nullptr)};

        assert(status == 0);

        if (firstTimeFlag) {
            clockUsedDetails = StartDateUnixEpoch;
            nanosecondsSinceUnixEpochAtFirstCall = GetNanosecondsSinceUnixEpochHelp();
            gettimeofday(&tv, nullptr);
            timePointNow = beginOfTimeInNanoseconds = B1 * (int64_t) tv.tv_sec + K1 * (int64_t) tv.tv_usec;
            firstTimeFlag = false;
        } else {
            timePointNow = B1 * (int64_t) tv.tv_sec + K1 * (int64_t) tv.tv_usec;
        }

        return (timePointNow - beginOfTimeInNanoseconds);
#endif
    }

    /**
     * Gives back the beginning of time in nanoseconds.
     * That time varies depending which clock is being and which OS.
     * For example under Linux could be the boot time of the machine if the
     * MONOTONIC_CLOCK is available or the Unix Epoch UTC time
     * if the MONOTONIC_CLOCK is not available.
     * On Windows OS is always the time since 12/30/1899 12:00am UTC.
     * On a Unix system is the Unix Epoch UTC time.
     * The exact value is captured when we call for the first time the
     * function: GetNanosecondsSinceFirstCall()
     * If we have not call yet the above function then is the value is always zero.
     */
    inline static auto GetBeginOfTimeInNanoseconds() -> int64_t {

        auto res{(int64_t) 0LL};

        if (beginOfTimeInNanoseconds == 0 && beginOfTimeIn100Nanoseconds != 0) {
            res = beginOfTimeIn100Nanoseconds * 100LL;
        } else if (beginOfTimeInNanoseconds != 0 && beginOfTimeIn100Nanoseconds == 0) {
            res = beginOfTimeInNanoseconds;
        }

        return res;
    }

    /**
     * Gives back the beginning of time in nanoseconds,
     * and the system of reference.
     */
    inline static auto GetBeginOfTimeInNanosecondsDetailsString() -> std::string {

        if (beginOfTimeInNanoseconds != 0 || beginOfTimeIn100Nanoseconds != 0) {
            return std::to_string(GetBeginOfTimeInNanoseconds()) + clockUsedDetails;
        }

        return std::string();
    }

    /**
     * Gives back the nanoseconds since Unix Epoch
     * at the time of first call of the function GetNanosecondsSinceFirstCall().
     * The function used is a std::chrono function, so a system time.
     */
    inline static auto GetNanosecondsSinceUnixEpochAtFirstCall() -> int64_t {

        return nanosecondsSinceUnixEpochAtFirstCall;
    }

    /**
     * Gives back the nanoseconds since Unix Epoch "now".
     * It is syntonized with system clock.
     */
    inline static auto GetNanosecondsSinceUnixEpochSyntonized() -> int64_t {

        return nanosecondsSinceUnixEpochAtFirstCall + GetNanosecondsSinceFirstCall();
    }

    /**
     * Gives back the nanoseconds since system epoch "now".
     * So, on Linux and with CLOCK_MONOTONIC is the time passed since boot time.
     * On Linux and with CLOCK_REALTIME is the time passed since Unix Epoch.
     * On Windows is the time passed since Windows Epoch (1899/12/30 midnight, UTC).
     * It is syntonized with system clock.
     */
    inline static auto GetNanosecondsSinceSystemEpochSyntonized() -> int64_t {

        return GetBeginOfTimeInNanoseconds() + GetNanosecondsSinceFirstCall();
    }

private:

    /**
     * Gives back the nanoseconds since Unix Epoch "now" using std::chrono.
     */
    inline static auto GetNanosecondsSinceUnixEpochHelp() -> int64_t {

        return std::chrono::duration_cast<std::chrono::nanoseconds>(
                std::chrono::system_clock::now().time_since_epoch()).count();
    }
};

std::mutex ClockNano::mtx{};
int64_t ClockNano::beginOfTimeInNanoseconds = 0;
int64_t ClockNano::beginOfTimeIn100Nanoseconds = 0;
int64_t ClockNano::nanosecondsSinceUnixEpochAtFirstCall = 0;
std::string ClockNano::clockUsedDetails{};

// driver program

auto main() -> int {

    constexpr auto NUM_THREADS{(size_t) 4};
    constexpr auto DIM_MAX{(size_t) 10'000};

    {
        std::cout << std::endl;
        std::cout << "-->> test 1 --> ClockNano::GetNanosecondsSinceFirstCall()" << std::endl;
        std::cout << std::endl;

        std::mutex mtx{};

        auto ld{[&]() {

            mtx.lock();
            std::cout << "-->> thread id = " << std::this_thread::get_id() << std::endl;
            mtx.unlock();

            auto a{std::vector<int64_t>(DIM_MAX)};

            for (size_t i = 0; i < DIM_MAX; i++) {
                a[i] = ClockNano::GetNanosecondsSinceFirstCall();
            }

            auto ave{(double) 0.0};

            for (size_t i = 1; i < DIM_MAX; i++) {
                ave += (double) (a[i] - a[i - 1]);
            }

            mtx.lock();
            std::cout << "-->> average time between consecutive calls is (ns) = "
                      << (int64_t) (ave / (DIM_MAX - 1)) << std::endl;
            mtx.unlock();
        }
        };

        auto vecTh{std::vector<std::thread>(NUM_THREADS)};

        for (auto &el : vecTh) {
            el = std::thread(ld);
        }

        for (auto &el : vecTh) {
            el.join();
        }
    }

    {
        std::cout << std::endl;
        std::cout << "-->> test 2 --> ClockNano::BeginOfTimeInNanoseconds()" << std::endl;
        std::cout << "-->>        --> ClockNano::BeginOfTimeInNanosecondsDetailsString()" << std::endl;
        std::cout << std::endl;

        std::mutex mtx{};

        auto ld{[&]() {

            mtx.lock();

            std::cout << "-->> thread id = " << std::this_thread::get_id() << std::endl;

            mtx.unlock();

            auto a{std::vector<int64_t>(DIM_MAX)};

            for (size_t i = 0; i < DIM_MAX; i++) {
                a[i] = ClockNano::GetBeginOfTimeInNanoseconds();
            }

            std::sort(a.begin(), a.end());
            const auto it{std::unique(a.begin(), a.end())};
            a.resize(std::distance(a.begin(), it));

            mtx.lock();
            std::cout << "-->> the value should be exact one (1) = " << a.size() << std::endl;
            mtx.unlock();
        }
        };

        auto vecTh{std::vector<std::thread>(NUM_THREADS)};

        for (auto &el : vecTh) {
            el = std::thread(ld);
        }

        for (auto &el : vecTh) {
            el.join();
        }
    }

    {
        std::cout << std::endl;
        std::cout << "-->> test 3 --> ClockNano::GetBeginOfTimeInNanoseconds()" << std::endl;
        std::cout << "            --> ClockNano::GetBeginOfTimeInNanosecondsDetailsString()" << std::endl;
        std::cout << std::endl;

        std::cout << ClockNano::GetBeginOfTimeInNanoseconds() << std::endl;
        std::cout << ClockNano::GetBeginOfTimeInNanosecondsDetailsString() << std::endl;
    }

    {
        std::cout << std::endl;
        std::cout << "-->> test 4 --> ClockNano::GetNanosecondsSinceUnixEpochAtFirstCall()" << std::endl;
        std::cout << std::endl;

        std::cout << ClockNano::GetNanosecondsSinceUnixEpochAtFirstCall() << std::endl;
    }

    {
        std::cout << std::endl;
        std::cout << "-->> test 5 --> ClockNano::GetNanosecondsSinceUnixEpochSyntonized()" << std::endl;
        std::cout << std::endl;

        std::cout << ClockNano::GetNanosecondsSinceUnixEpochSyntonized() << std::endl;
    }

    {
        std::cout << std::endl;
        std::cout << "-->> test 6 --> ClockNano::GetNanosecondsSinceSystemEpochSyntonized()" << std::endl;
        std::cout << std::endl;

        std::cout << ClockNano::GetNanosecondsSinceSystemEpochSyntonized() << std::endl;
    }
}
