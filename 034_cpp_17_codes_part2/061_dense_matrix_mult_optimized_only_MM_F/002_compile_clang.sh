#!/bin/bash

  clang++   -O3            \
            -Wall          \
            -std=c++17     \
            -stdlib=libc++ \
            -fopenmp       \
            -pthread       \
            -pedantic      \
            main.cpp       \
            -L/opt/intel/mkl/lib/intel64                                      \
            -Wl,-R/opt/intel/mkl/lib/intel64  -lmkl_lapack95_lp64             \
            -lmkl_intel_lp64 -lmkl_gnu_thread -lmkl_core -lmkl_sequential -lm \
            -o x_clang_mkl -lc++fs

