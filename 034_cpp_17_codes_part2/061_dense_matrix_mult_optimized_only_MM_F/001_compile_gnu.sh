#!/bin/bash

  g++-9.1   -O3         \
            -Wall       \
            -std=gnu++2a\
            -pthread    \
            -fopenmp    \
            -pedantic   \
            main.cpp \
            -L/opt/intel/mkl/lib/intel64                                      \
            -Wl,-R/opt/intel/mkl/lib/intel64  -lmkl_lapack95_lp64             \
            -lmkl_intel_lp64 -lmkl_gnu_thread -lmkl_core -lmkl_sequential -lm \
            -o x_gnu_mkl
