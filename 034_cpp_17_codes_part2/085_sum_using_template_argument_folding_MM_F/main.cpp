#include <iostream>
#include <utility>
#include <vector>

template<typename ...T>
auto sum_loc(const T &...x) {
    return (x +...);
}

template<std::size_t... S>
auto unpack_vector(const std::vector<double> &vec, std::index_sequence<S...>) {
    return sum_loc(vec[S]...);
}

template<std::size_t size>
auto unpack_vector(const std::vector<double> &vec) {

    if (vec.size() != size) {
        throw;
    }

    return unpack_vector(vec, std::make_index_sequence<size>());
}

auto main() -> int {

    {
        std::cout << "---------------------------------->> 1" << std::endl;
        std::cout << "---> 1 --> " << sum_loc(1, 2, 3) << std::endl;
        std::cout << "---> 2 --> " << sum_loc(1, 2, 3.1) << std::endl;
        std::cout << "---> 3 --> " << sum_loc('1', 2, 3.1) << std::endl;
        std::cout << "---> 4 --> " << sum_loc('1') << std::endl;
        std::cout << "---> 5 --> " << sum_loc("1") << std::endl;
        std::cout << "---> 5 --> " << sum_loc(std::string("12"),
                                              std::string("34")) << std::endl;
    }

    {
        std::cout << "---------------------------------->> 2" << std::endl;
        std::cout << "---> 1 --> " << sum_loc(1, 2, 3, 4, 5) << std::endl;
        std::cout << "---> 2 --> " << sum_loc(1, 2, 3.1, 4, 5) << std::endl;
    }

    {
        std::cout << "---------------------------------->> 3" << std::endl;
        std::cout << "---> 1 --> " << sum_loc(1, 2, 3, 4, 5) << std::endl;
        std::cout << "---> 2 --> " << sum_loc(1, 2, 3.1, 4, 5) << std::endl;
    }

    {
        std::cout << "---------------------------------->> 4" << std::endl;

        std::vector<double> v1;

        constexpr auto DIM_MAX{(size_t) 1'000};

        for (size_t i = 0; i < DIM_MAX; i++) {
            v1.push_back(i);
        }

        const auto res{unpack_vector<DIM_MAX>(v1)};
        std::cout << res << std::endl;
    }

}

