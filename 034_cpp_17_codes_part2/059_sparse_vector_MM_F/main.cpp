#include <iostream>
#include <vector>
#include <chrono>
#include <map>
#include <unordered_map>
#include <thread>

class benchmark_timer {
public:

    explicit benchmark_timer(const double &res) :
            _res(res),
            t1{std::chrono::steady_clock::now()},
            t2{std::chrono::steady_clock::now()} {};

    ~benchmark_timer() {
        t2 = std::chrono::steady_clock::now();
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count()};
        std::cout << " --> res = " << _res << std::endl;
        std::cout << " --> time used (secs) = " << total_time << std::endl;
    }

    auto set_res(const double &res) -> void {
        _res = res;
    }

private:

    double _res{};
    decltype(std::chrono::steady_clock::now()) t1{};
    decltype(std::chrono::steady_clock::now()) t2{};
};

class sparse_vector_v1 {

public:

    auto build_vector(size_t dim) -> void {

        _indxs.resize(dim);
        _vals.resize(dim);

        for (size_t kk = 0; kk < dim; kk++) {
            _indxs[kk] = kk;
            _vals[kk] = (double) kk;
        }
    }

    friend auto dot_vectors(const sparse_vector_v1 &spv1, const sparse_vector_v1 &spv2) -> double;

    std::vector<double> _vals;
    std::vector<size_t> _indxs;
};

[[nodiscard]] auto dot_vectors(const sparse_vector_v1 &spv1, const sparse_vector_v1 &spv2) -> double {

    const auto dim{spv1._indxs.size()};
    auto res{0.0};

    for (size_t kk = 0; kk < dim; kk++) {
        res += spv1._vals[spv1._indxs[kk]] * spv2._vals[spv2._indxs[kk]];
    }

    return res;
}

class sparse_vector_v2 {

public:

    auto build_vector(size_t dim) -> void {

        for (size_t kk = 0; kk < dim; kk++) {
            _spv[kk] = (double) kk;
        }
    }

    friend auto dot_vectors(const sparse_vector_v2 &spv1, const sparse_vector_v2 &spv2) -> double;

    std::map<size_t, double> _spv;
};

[[nodiscard]] auto dot_vectors(const sparse_vector_v2 &spv1, const sparse_vector_v2 &spv2) -> double {

    const auto dim{spv1._spv.size()};

    auto res{0.0};

    for (size_t kk = 0; kk < dim; kk++) {
        res += spv1._spv.at(kk) * spv2._spv.at(kk);
    }

    return res;
}

class sparse_vector_v3 {

public:

    auto build_vector(size_t dim) -> void {

        for (size_t kk = 0; kk < dim; kk++) {
            _spv[kk] = (double) kk;
        }
    }

    friend auto dot_vectors(const sparse_vector_v3 &spv1, const sparse_vector_v3 &spv2) -> double;

    std::unordered_map<size_t, double> _spv;
};

[[nodiscard]] auto dot_vectors(const sparse_vector_v3 &spv1, const sparse_vector_v3 &spv2) -> double {

    const auto dim{spv1._spv.size()};

    auto res{0.0};

    for (size_t kk = 0; kk < dim; kk++) {
        res += spv1._spv.at(kk) * spv2._spv.at(kk);
    }

    return res;
}

constexpr auto DIM_SPV{(size_t) 40000000};
constexpr auto DO_MAX{(size_t) 100000000};

auto main() -> int {

    auto b1{[] {

        for (size_t i = 0; i < DO_MAX; i++) {

            std::cout << "--> sparse vector --> v1 --> i = " << i << std::endl;
            benchmark_timer ot(0.0);

            sparse_vector_v1 spv1;
            sparse_vector_v1 spv2;

            spv1.build_vector(DIM_SPV);
            spv2.build_vector(DIM_SPV);

            const auto res{dot_vectors(spv1, spv2)};

            ot.set_res(res);
        }
    }};

    auto b2{[] {

        for (size_t i = 0; i < DO_MAX; i++) {

            std::cout << "--> sparse vector --> v2 --> i = " << i << std::endl;

            benchmark_timer ot(0.0);

            sparse_vector_v2 spv1;
            sparse_vector_v2 spv2;

            spv1.build_vector(DIM_SPV);
            spv2.build_vector(DIM_SPV);

            const auto res{dot_vectors(spv1, spv2)};

            ot.set_res(res);
        }
    }};

    auto b3{[] {

        for (size_t i = 0; i < DO_MAX; i++) {

            std::cout << "--> sparse vector --> v3 --> i = " << i << std::endl;

            benchmark_timer ot(0.0);

            sparse_vector_v3 spv1;
            sparse_vector_v3 spv2;

            spv1.build_vector(DIM_SPV);
            spv2.build_vector(DIM_SPV);

            const auto res{dot_vectors(spv1, spv2)};

            ot.set_res(res);
        }
    }};

    auto t1{std::thread(b1)};
    auto t2{std::thread(b2)};
    auto t3{std::thread(b3)};

    t1.join();
    t2.join();
    t3.join();
}
