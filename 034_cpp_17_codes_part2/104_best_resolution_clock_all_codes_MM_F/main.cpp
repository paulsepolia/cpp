//#include <iostream>
//#include <vector>
//#include <cmath>
//#include <chrono>
//#include <windows.h>
//
//const auto DIM_MAX{ (size_t)(20 * std::pow(10.0, 8.0)) };
//const auto DO_MAX{ (size_t)(20 * std::pow(10.0, 8.0)) };
//
//using DateTime = size_t;
//
//DateTime UtcNowPreciseDays()
//{
//    const uint64_t OA_ZERO_TICKS = 94'353'120'000'000'000; // 12/30/1899 12:00am in ticks
//    const uint64_t TICKS_PER_DAY = 864'000'000'000;        // ticks per day
//
//    LPFILETIME ft{ new FILETIME };
//
//    GetSystemTimePreciseAsFileTime(ft);
//
//    ULARGE_INTEGER dt;
//    dt.LowPart = ft->dwLowDateTime;
//    dt.HighPart = ft->dwHighDateTime;
//
//    delete ft;
//
//    return (dt.QuadPart - OA_ZERO_TICKS) / TICKS_PER_DAY;
//
//}
//
//DateTime UtcNowPreciseSeconds()
//{
//    constexpr uint64_t OA_ZERO_TICKS = 94'353'120'000'000'000; // 12/30/1899 12:00am in ticks
//    constexpr uint64_t TICKS_PER_DAY = 864'000'000'000;        // ticks per day
//    constexpr uint64_t SECONDS_PER_DAY = 86'400;               // seconds per day
//
//    LPFILETIME ft{ new FILETIME };
//
//    GetSystemTimePreciseAsFileTime(ft);
//
//    ULARGE_INTEGER dt;
//    dt.LowPart = ft->dwLowDateTime;
//    dt.HighPart = ft->dwHighDateTime;
//
//    delete ft;
//
//    return (dt.QuadPart - OA_ZERO_TICKS) / 10'000'000;
//
//}
//
//DateTime UtcNowPreciseMilliseconds()
//{
//    constexpr uint64_t OA_ZERO_TICKS = 94'353'120'000'000'000; // 12/30/1899 12:00am in ticks
//
//    LPFILETIME ft{ new FILETIME };
//
//    GetSystemTimePreciseAsFileTime(ft);
//
//    ULARGE_INTEGER dt;
//    dt.LowPart = ft->dwLowDateTime;
//    dt.HighPart = ft->dwHighDateTime;
//
//    delete ft;
//
//    return (dt.QuadPart - OA_ZERO_TICKS) / 10'000;
//
//}
//
//DateTime UtcNowPreciseMicroseconds()
//{
//    constexpr uint64_t OA_ZERO_TICKS = 94'353'120'000'000'000; // 12/30/1899 12:00am in ticks
//
//    LPFILETIME ft{ new FILETIME };
//
//    GetSystemTimePreciseAsFileTime(ft);
//
//    ULARGE_INTEGER dt;
//    dt.LowPart = ft->dwLowDateTime;
//    dt.HighPart = ft->dwHighDateTime;
//
//    delete ft;
//
//    return (dt.QuadPart - OA_ZERO_TICKS) / 10;
//}
//
//DateTime UtcNowPreciseNanoseconds()
//{
//    constexpr auto OA_ZERO_TICKS{ (uint64_t)94'353'120'000'000'000 }; // 12/30/1899 12:00am in ticks
//
//    LPFILETIME ft{ new FILETIME };
//
//    GetSystemTimePreciseAsFileTime(ft);
//
//    ULARGE_INTEGER dt;
//    dt.LowPart = ft->dwLowDateTime;
//    dt.HighPart = ft->dwHighDateTime;
//
//    delete ft;
//
//    return (dt.QuadPart - OA_ZERO_TICKS) * 10;
//}
//
//auto main() -> int
//{
//    {
//        LARGE_INTEGER StartingTime;
//        LARGE_INTEGER EndingTime;
//        LARGE_INTEGER ElapsedMicroseconds;
//        LARGE_INTEGER Frequency;
//
//        QueryPerformanceFrequency(&Frequency);
//        QueryPerformanceCounter(&StartingTime);
//
//        // Activity to be timed
//
//        QueryPerformanceCounter(&EndingTime);
//        ElapsedMicroseconds.QuadPart = EndingTime.QuadPart - StartingTime.QuadPart;
//
//        // We now have the elapsed number of ticks, along with the
//        // number of ticks-per-second. We use these values
//        // to convert to the number of elapsed microseconds.
//        // To guard against loss-of-precision, we convert
//        // to microseconds *before* dividing by ticks-per-second.
//
//        ElapsedMicroseconds.QuadPart *= 1000000;
//        ElapsedMicroseconds.QuadPart /= Frequency.QuadPart;
//
//        std::cout << ElapsedMicroseconds.QuadPart << std::endl;
//    }
//
//    {
//        std::cout << "--> pgg --> A" << std::endl;
//        std::cout << UtcNowPreciseDays() << std::endl;
//        std::cout << UtcNowPreciseSeconds() << std::endl;
//        std::cout << UtcNowPreciseMilliseconds() << std::endl;
//        std::cout << UtcNowPreciseMicroseconds() << std::endl;
//        std::cout << UtcNowPreciseNanoseconds() << std::endl;
//        std::cout << "--> pgg --> B" << std::endl;
//    }
//}

#include <iostream>
#include <mutex>
#include <cassert>
#include <chrono>
#include <thread>

#if _WIN32 || _WIN64

#include <windows.h>

#elif __linux__

#include <ctime>

#else // generic Unix

#include <sys/time.h>

#endif

class ClockEtas {

private:

    static std::mutex mtx;

public:

    ClockEtas() = delete;

    ClockEtas(const ClockEtas &) = delete;

    ClockEtas(ClockEtas &&) = delete;

    ClockEtas &operator=(const ClockEtas &) = delete;

    ClockEtas &operator=(ClockEtas &&) = delete;

public:

    static int64_t BeginOfTimeInNanoseconds;

    // The only member function to give back the timestamp
    // since the time fo its first call

    static auto getNanosecondsSinceFirstCall() -> int64_t {

        std::lock_guard<std::mutex> lck(mtx);

#if _WIN32 || _WIN64

        constexpr auto OA_ZERO_TICKS{(int64_t) 94'353'120'000'000'000LL}; // 12/30/1899 12:00am in ticks
        static auto firstTimeFlag{(bool) true};
        auto timePointNow{(int64_t) 0LL};
        LARGE_INTEGER dt;

        if (firstTimeFlag) {

            LPFILETIME ft{new FILETIME};

            GetSystemTimePreciseAsFileTime(ft);
            dt.LowPart = ft->dwLowDateTime;
            dt.HighPart = ft->dwHighDateTime;

            timePointNow = BeginOfTimeInNanoseconds = (dt.QuadPart - OA_ZERO_TICKS) * 10LL;

            firstTimeFlag = false;
            delete ft;
        } else {

            LPFILETIME ft{new FILETIME};

            GetSystemTimePreciseAsFileTime(ft);
            dt.LowPart = ft->dwLowDateTime;
            dt.HighPart = ft->dwHighDateTime;

            timePointNow = (dt.QuadPart - OA_ZERO_TICKS) * 10LL;

            delete ft;
        }

        return timePointNow - BeginOfTimeInNanoseconds;

#elif __linux__

        constexpr auto B1{ (int64_t)1'000'000'000LL };
        static auto firstTimeFlag{ (bool)true };
        auto timePointNow{ (int64_t)0LL };
        struct timespec ts {};
        const auto clockMonotonicStatus{ (int)clock_gettime(CLOCK_MONOTONIC, &ts) };

        if (clockMonotonicStatus == 0) {

            if (firstTimeFlag) {
                timePointNow = BeginOfTimeInNanoseconds = B1 * (int64_t)ts.tv_sec + (int64_t)ts.tv_nsec;
                firstTimeFlag = false;

            }
            else {
                timePointNow = B1 * (int64_t)ts.tv_sec + (int64_t)ts.tv_nsec;
            }
        }
        else {

            const auto clockRealTimeStatus{ (int)clock_gettime(CLOCK_REALTIME, &ts) };

            assert(clockRealTimeStatus == 0);

            if (firstTimeFlag) {
                timePointNow = BeginOfTimeInNanoseconds = B1 * (int64_t)ts.tv_sec + (int64_t)ts.tv_nsec;
                firstTimeFlag = false;
            }
            else {
                timePointNow = B1 * (int64_t)ts.tv_sec + (int64_t)ts.tv_nsec;
            }
        }

        return (timePointNow - BeginOfTimeInNanoseconds);

#else // generic Unix

        constexpr auto K1{(int64_t) 1'000};
        constexpr auto B1{(int64_t) 1'000'000'000LL};
        static auto firstTimeFlag{(bool) true};
        auto timePointNow{(int64_t) 0LL};
        struct timeval tv{};
        auto status{(int) gettimeofday(&tv, nullptr)};

        assert(status == 0);

        if (firstTimeFlag) {
            timePointNow = BeginOfTimeInNanoseconds = B1 * (int64_t) tv.tv_sec + K1 * (int64_t) tv.tv_usec;
            firstTimeFlag = false;

        } else {
            timePointNow = B1 * (int64_t) tv.tv_sec + K1 * (int64_t) tv.tv_usec;
        }

        return (timePointNow - BeginOfTimeInNanoseconds);
#endif
    }
};

std::mutex ClockEtas::mtx{};
int64_t ClockEtas::BeginOfTimeInNanoseconds = 0;

// driver program

auto main() -> int {

    constexpr auto DO_MAX{(size_t) 10};

    for (size_t i = 0; i < DO_MAX; i++) {

        std::cout << " ----------------------------------------------------->> i = " << i << std::endl;

        std::cout << ClockEtas::getNanosecondsSinceFirstCall() << std::endl;
        std::cout << ClockEtas::getNanosecondsSinceFirstCall() << std::endl;
        std::cout << ClockEtas::getNanosecondsSinceFirstCall() << std::endl;
        std::cout << ClockEtas::getNanosecondsSinceFirstCall() << std::endl;

        std::cout << ClockEtas::BeginOfTimeInNanoseconds << std::endl;

        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
}
