#!/bin/bash

  g++-9.2.0  -c           \
             -O3          \
             -Wall        \
             -std=gnu++2a \
             -I/mnt/xdata/opt/boost/1.70.0/include/ \
             main.cpp

  g++-9.2.0  -pthread \
             /mnt/xdata/opt/boost/1.70.0/lib/*.so \
             main.o \
             -o x_gnu

  rm *.o
