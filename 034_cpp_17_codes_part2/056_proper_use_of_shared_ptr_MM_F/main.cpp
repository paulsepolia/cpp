#include <iostream>
#include <vector>
#include <memory>
#include <functional>
#include <chrono>
#include <iomanip>
#include <cmath>

class benchmark_timer {
public:

    explicit benchmark_timer(const double &res) :
            _res(res),
            t1{std::chrono::steady_clock::now()},
            t2{std::chrono::steady_clock::now()} {};

    ~benchmark_timer() {
        t2 = std::chrono::steady_clock::now();
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count()};
        std::cout << " --> res = " << _res << std::endl;
        std::cout << " --> time used (secs) = " << total_time << std::endl;
    }

    auto set_res(const double &res) -> void {
        _res = res;
    }

private:

    double _res{};
    decltype(std::chrono::steady_clock::now()) t1{};
    decltype(std::chrono::steady_clock::now()) t2{};
};

constexpr bool DEBUG_LOG{true};

auto operator new(size_t size) -> void * {
    void *p{std::malloc(size)};
    if (DEBUG_LOG) {
        std::cout << "new: allocated " << size << " bytes" << std::endl;
    }
    return p;
}

auto operator delete(void *p) noexcept -> void {
    if (DEBUG_LOG) {
        std::cout << "delete: deleted memory" << std::endl;
    }
    return std::free(p);
}

auto operator new[](size_t size) -> void * {
    void *p{std::malloc(size)};
    if (DEBUG_LOG) {
        std::cout << "new: allocated " << size << " bytes" << std::endl;
    }
    return p;
}

auto operator delete[](void *p) noexcept -> void {
    if (DEBUG_LOG) {
        std::cout << "delete: deleted memory" << std::endl;
    }
    return std::free(p);
}

class A {
public:
    A() = default;

    explicit A(std::string name) : _name(std::move(name)) {}

    [[nodiscard]] auto get_name() const -> std::string {
        return _name;
    }

    auto set_name(std::string name) -> void {
        _name = std::move(name);
    }

    virtual ~A() = default;

private:
    std::string _name;
};


int main() {

    {
        std::cout << "--->> 1 --> efficient way --> start" << std::endl;
        auto p1{std::make_shared<double>(100.0)};
        std::cout << "--->> 1 --> efficient way --> end" << std::endl;
    }
    {
        std::cout << "--->> 2 --> not efficient way --> start" << std::endl;
        auto p2{std::shared_ptr<double>(new double(100.0))};
        std::cout << "--->> 2 --> not efficient way --> end" << std::endl;
    }
    {
        std::cout << "--->> 3 --> efficient way --> start" << std::endl;
        auto p1{std::make_shared<A>("12345")};
        std::cout << "--->> 3 --> efficient way --> end" << std::endl;
    }
    {
        std::cout << "--->> 4 --> not efficient way --> start" << std::endl;
        auto p2{std::shared_ptr<A>(new A("12345"))};
        std::cout << "--->> 4 --> not efficient way --> end" << std::endl;
    }

}
