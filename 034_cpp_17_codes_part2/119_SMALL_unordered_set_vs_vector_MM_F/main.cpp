#include <iostream>
#include <chrono>
#include <iomanip>
#include <random>
#include <unordered_set>
#include <algorithm>
#include <cassert>

class benchmark_timer {
public:

    explicit benchmark_timer() :
            res(0.0),
            t1{std::chrono::steady_clock::now()},
            t2{std::chrono::steady_clock::now()},
            t_prev{std::chrono::steady_clock::now()} {};

    auto print_time() const -> void {
        const auto t_loc{std::chrono::steady_clock::now()};
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t_loc - t_prev).count()};
        t_prev = t_loc;
        std::cout << "--> time used (secs) = " << total_time << std::endl;
    }

    auto reset_timer() -> void {
        const auto t_loc{std::chrono::steady_clock::now()};
        t_prev = t_loc;
        t1 = t_loc;
        t2 = t_loc;
    }

    ~benchmark_timer() {
        t2 = std::chrono::steady_clock::now();
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count()};
        std::cout << "--> res = " << res << std::endl;
        std::cout << "--> total time used (secs) = " << total_time << std::endl;
    }

    auto set_res(const double &res_loc) -> void {
        res = res_loc;
    }

private:

    double res{};
    decltype(std::chrono::steady_clock::now()) t1{};
    decltype(std::chrono::steady_clock::now()) t2{};
    mutable decltype(std::chrono::steady_clock::now()) t_prev{};
};

template<typename T>
class random_number {

public:

    static auto get(T start = 0.0, T end = 1.0) -> T {

        auto dev{std::random_device()};
        auto rng{std::mt19937(dev())};

        if constexpr(std::is_same<double, T>::value) {
            auto dist{std::uniform_real_distribution<double>(start, end)};
            return dist(rng);
        } else if constexpr(std::is_same<int64_t, T>::value) {
            auto dist{std::uniform_int_distribution<int64_t>(start, end)};
            return dist(rng);
        } else {
            assert(false);
        }
    }
};

template<typename T>
class A {

private:

    static constexpr auto dim{(size_t) 1000};
    double x1[dim] = {};
    double x2[dim] = {};
    double x3[dim] = {};

public:

    A() {
        for (size_t i = 0; i < dim; i++) {
            x1[i] = random_number<T>::get();
            x2[i] = random_number<T>::get();
            x3[i] = random_number<T>::get();
        }
    }

    [[nodiscard]] auto get_x1(size_t idx) const -> T {
        return x1[idx];
    }

    [[nodiscard]] auto get_x2(size_t idx) const -> T {
        return x1[idx];
    }

    [[nodiscard]] auto get_x3(size_t idx) const -> T {
        return x1[idx];
    }

    bool operator==(const A &obj) const {
        return x1[0] == obj.x1[0] &&
               x2[0] == obj.x2[0] &&
               x3[0] == obj.x3[0];
    }
};

namespace std {
    template<typename T>
    struct hash<A<T>> {
        size_t operator()(const A<T> &n) const noexcept {

            const auto h1{(size_t) std::hash<T>{}(n.get_x1(0))};
            const auto h2{(size_t) std::hash<T>{}(n.get_x2(0))};
            const auto h3{(size_t) std::hash<T>{}(n.get_x3(0))};

            return (h1 ^ (h2 << 1ULL) >> 1ULL) ^ (h3 << 1ULL);
        }
    };
}

auto main() -> int {

    std::cout << std::fixed;
    std::cout << std::setprecision(10);
    constexpr auto NUM_ELEMS{100};
    constexpr auto DO_MAX{100'000};

    std::cout << "--> building the const vector..." << std::endl;
    auto ot{benchmark_timer()};
    const auto v{std::vector<A<double>>(NUM_ELEMS)};
    ot.print_time();

    auto res{(double) 0.0};

    {
        std::cout << "--------------------------------------------------------->> 1" << std::endl;

        std::cout << "--> building the local vector..." << std::endl;
        auto v1{std::vector<A<double>>(v)};
        ot.print_time();

        std::cout << "--> random shuffle the local vector..." << std::endl;
        std::random_device rd;
        std::mt19937 g(rd());
        std::shuffle(v1.begin(), v1.end(), g);
        ot.print_time();

        std::cout << "--> looking for all the elements in to the local vector..." << std::endl;
        for (size_t i = 0; i < DO_MAX; i++) {
            for (const auto &el: v) {
                const auto it{std::find(v1.begin(), v1.end(), el)};
                if (it != v1.end()) {
                    res++;
                }
            }
        }
        ot.set_res(res);
        ot.print_time();

        std::cout << "--> erasing the vector..." << std::endl;
        while (!v1.empty()) {
            v1.erase(v1.begin());
        }
        ot.print_time();
    }

    {
        std::cout << "--------------------------------------------------------->> 2" << std::endl;

        std::cout << "--> building the local unordered set..." << std::endl;
        auto s1{std::unordered_set<A<double>>(NUM_ELEMS)};
        ot.print_time();

        std::cout << "--> looking for all the elements in to the local unordered set..." << std::endl;
        for (size_t i = 0; i < DO_MAX; i++) {
            for (const auto &el: v) {
                const auto it{std::find(s1.begin(), s1.end(), el)};
                if (it != s1.end()) {
                    res++;
                }
            }
        }
        ot.set_res(res);
        ot.print_time();

        std::cout << "--> erasing the unordered set..." << std::endl;
        while (!s1.empty()) {
            s1.erase(s1.begin());
        }
        ot.print_time();
    }
}