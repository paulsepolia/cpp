#!/bin/bash

  clang++   -O3          \
            -Wall        \
            -std=gnu++2a \
            -pthread     \
            main.cpp \
            -o x_clang
