#include <ctime>
#include <cmath>
#include <iostream>
#include <ctime>
#include <chrono>
#include <thread>
#include <limits>
#include <iomanip>

const auto B1{(double) std::pow(10.0, 9.0)};

static double timespec_to_seconds(struct timespec *ts) {
    return (double) ts->tv_sec + (double) ts->tv_nsec / B1;
}

const auto DO_MAX{(size_t) (std::pow(10.0, 3.0))};
const auto DO_MAX2{(size_t) (std::pow(10.0, 8.0))};

auto main() -> int {

    {
        std::cout << "------------------------>> 1" << std::endl;

        struct timespec start{};
        struct timespec end{};

        auto i{(size_t) 1};
        auto dt{(double) 0.0};
        auto dt_min{(double) std::numeric_limits<double>::max()};
        auto dt_max{(double) std::numeric_limits<double>::min()};

        auto idx_max{(size_t) 0};
        auto idx_min{(size_t) 0};

        while (i <= DO_MAX) {

            if (clock_gettime(CLOCK_MONOTONIC, &start)) {
                std::cout << "--> ERROR invoking the CLOCK_MONOTONIC - start" << std::endl;
            }

            if (clock_gettime(CLOCK_MONOTONIC, &end)) {
                std::cout << "--> ERROR invoking the CLOCK_MONOTONIC - end" << std::endl;
            }

            dt = timespec_to_seconds(&end) - timespec_to_seconds(&start);

            if (dt < dt_min) {
                dt_min = dt;
                idx_min = i;
            }

            if (dt > dt_max) {
                dt_max = dt;
                idx_max = i;
            }

            ++i;
        }

        std::cout << " --> the minimum delta detected after many runs = " << int64_t(dt_min * B1) << ", at position: "
                  << idx_min << std::endl;
        std::cout << " --> the maximum delta detected after many runs = " << int64_t(dt_max * B1) << ", at position: "
                  << idx_max << std::endl;
    }

    {
        std::cout << std::dec << std::numeric_limits<std::size_t>::max() << std::endl;

        constexpr auto ns_now{(size_t) 1'573'832'220'647'120'700UL};
        std::cout << " --> ns_now --> " << ns_now << std::endl;

        std::cout << "----------->> ns to double and back" << std::endl;

        constexpr auto ns_now_dbl{(double) ns_now};
        std::cout << " --> ns since epoch to double               --> " << ns_now_dbl << std::endl;
        std::cout << std::fixed;
        std::cout << " --> ns since epoch to back to size_t       --> " << ns_now_dbl << std::endl;
        std::cout << " --> ns_now (true original value)           --> " << ns_now << std::endl;
        std::cout << " --> error (from size_t to double and back) --> "
                  << std::abs((double) (ns_now - (size_t) ns_now_dbl))
                  << " --> nanoseconds lost ...." << std::endl;

        std::cout << "----------->> ns to long double and back" << std::endl;

        constexpr auto ns_now_ldbl{(long double) ns_now};
        std::cout << " --> ns since epoch to long double               --> " << ns_now_ldbl << std::endl;
        std::cout << std::fixed;
        std::cout << " --> ns since epoch to back to size_t            --> " << ns_now_ldbl << std::endl;
        std::cout << " --> ns_now (true original value)                --> " << ns_now << std::endl;
        std::cout << " --> error (from size_t to long double and back) --> "
                  << std::abs((long double) (ns_now - (size_t) ns_now_ldbl))
                  << " --> nanoseconds lost ...." << std::endl;
    }

    for (size_t i = 0; i < DO_MAX; i++) {

        std::this_thread::sleep_for(std::chrono::seconds(1));

        std::cout << "--------------------------------------------------------------------------->> i = " << i
                  << std::endl;

        {
            std::cout << "------------------------>> 2" << std::endl;

            // Here system_clock is wall clock time from
            // the system-wide realtime clock

            const auto time_now{std::chrono::system_clock::to_time_t(std::chrono::system_clock::now())};
            std::cout << ctime(&time_now) << std::endl;
        }

        {
            std::cout << "------------------------>> 3" << std::endl;

            const auto seconds_since_epoch{
                    std::chrono::duration_cast<std::chrono::seconds>(
                            std::chrono::system_clock::now().time_since_epoch()).count()};

            std::cout << " --> seconds         since epoch      --> " << seconds_since_epoch << std::endl;

            const auto milliseconds_since_epoch{
                    std::chrono::duration_cast<std::chrono::milliseconds>(
                            std::chrono::system_clock::now().time_since_epoch()).count()};

            std::cout << " --> milliseconds    since epoch      --> " << milliseconds_since_epoch << std::endl;

            const auto microseconds_since_epoch{
                    std::chrono::duration_cast<std::chrono::microseconds>(
                            std::chrono::system_clock::now().time_since_epoch()).count()};

            std::cout << " --> microseconds    since epoch      --> " << microseconds_since_epoch << std::endl;

            const auto nanoseconds_since_epoch{
                    std::chrono::duration_cast<std::chrono::nanoseconds>(
                            std::chrono::system_clock::now().time_since_epoch()).count()};

            std::cout << " --> nanoseconds     since epoch      --> " << nanoseconds_since_epoch << std::endl;
        }
    }
}

// max size_t value : 18446744073709551615
// nanoseconds now  :  1573832220647120700
// microseconds     :     1573832220647120
// milliseconds     :        1573832220647
// seconds          :           1573832220

// seconds/year      :            31556952
// milliseconds/year :         31556952000
// microseconds/year :      31556952000000
// nanoseconds/year  :   31556952000000000