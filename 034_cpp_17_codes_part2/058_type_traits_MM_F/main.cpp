#include <iostream>
#include <vector>
#include <memory>
#include <functional>
#include <chrono>
#include <iomanip>
#include <cmath>
#include <cassert>

class benchmark_timer {
public:

    explicit benchmark_timer(const double &res) :
            _res(res),
            t1{std::chrono::steady_clock::now()},
            t2{std::chrono::steady_clock::now()} {};

    ~benchmark_timer() {
        t2 = std::chrono::steady_clock::now();
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count()};
        std::cout << " --> res = " << _res << std::endl;
        std::cout << " --> time used (secs) = " << total_time << std::endl;
    }

    auto set_res(const double &res) -> void {
        _res = res;
    }

private:

    double _res{};
    decltype(std::chrono::steady_clock::now()) t1{};
    decltype(std::chrono::steady_clock::now()) t2{};
};

constexpr bool DEBUG_LOG{true};

auto operator new(size_t size) -> void * {
    void *p{std::malloc(size)};
    if (DEBUG_LOG) {
        std::cout << "new: allocated " << size << " bytes" << std::endl;
    }
    return p;
}

auto operator delete(void *p) noexcept -> void {
    if (DEBUG_LOG) {
        std::cout << "delete: deleted memory" << std::endl;
    }
    return std::free(p);
}

auto operator new[](size_t size) -> void * {
    void *p{std::malloc(size)};
    if (DEBUG_LOG) {
        std::cout << "new: allocated " << size << " bytes" << std::endl;
    }
    return p;
}

auto operator delete[](void *p) noexcept -> void {
    if (DEBUG_LOG) {
        std::cout << "delete: deleted memory" << std::endl;
    }
    return std::free(p);
}

class test1 {
};

class test2 : public test1 {
};

int main() {

    std::cout << std::boolalpha;

    {
        std::cout << "---> 1" << std::endl;
        using T = std::remove_pointer_t<int *>;
        std::cout << (typeid(T).name() == typeid(int).name()) << std::endl;
        T a = 100;
        std::cout << a << std::endl;
    }
    {
        std::cout << "--->> 2" << std::endl;
        using T = std::add_pointer_t<double>;
        std::cout << (typeid(T).name() == typeid(double *).name()) << std::endl;
        T a{new double(111.1)};
        std::cout << *a << std::endl;
    }
    {
        std::cout << "--->> 3" << std::endl;
        auto v = std::is_same_v<uint8_t, unsigned char>;
        std::cout << v << std::endl;
    }
    {
        std::cout << "--->> 4" << std::endl;
        auto v = std::is_floating_point_v<uint8_t>;
        std::cout << v << std::endl;
        v = std::is_floating_point_v<long double>;
        std::cout << v << std::endl;
    }
    {
        std::cout << "--->> 5" << std::endl;
        auto v = std::is_base_of_v<test1, test2>;
        std::cout << v << std::endl;
        v = std::is_base_of_v<test2, test1>;
        std::cout << v << std::endl;
        static_assert(std::is_base_of_v<test1, test2>);
    }

}
