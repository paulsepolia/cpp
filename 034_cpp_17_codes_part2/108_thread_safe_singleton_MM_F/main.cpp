#include <chrono>
#include <iostream>
#include <future>
#include <thread>
#include <iomanip>

class SingletonThreadSafe {
public:
    static SingletonThreadSafe &getInstance() {
        static SingletonThreadSafe instance;
        volatile int dummy{};
        return instance;
    }

private:

    SingletonThreadSafe() = default;

    ~SingletonThreadSafe() = default;

public:

    SingletonThreadSafe(const SingletonThreadSafe &) = delete;

    SingletonThreadSafe &operator=(const SingletonThreadSafe &) = delete;

    SingletonThreadSafe(SingletonThreadSafe &&) = delete;

    SingletonThreadSafe &operator=(SingletonThreadSafe &&) = delete;
};

constexpr auto M100{100'000'000LL};
constexpr auto M400{400LL * M100};

double getTime(double & res) {

    auto begin{std::chrono::steady_clock::now()};

    for (size_t i = 0; i <= M400; ++i) {
        SingletonThreadSafe::getInstance();
    }

    res = std::chrono::duration_cast<std::chrono::duration<double>>(std::chrono::steady_clock::now() - begin).count();

    return res;
}

auto main() -> int {

    std::cout << std::fixed;
    std::cout << std::setprecision(10);

    {
        std::cout << "------------------------------------------------>> 2" << std::endl;

        auto res1{(double) 0.0};
        auto res2{(double) 0.0};
        auto res3{(double) 0.0};
        auto res4{(double) 0.0};

        auto fut1{std::async(std::launch::async, getTime, std::ref(res1))};
        auto fut2{std::async(std::launch::async, getTime, std::ref(res2))};
        auto fut3{std::async(std::launch::async, getTime, std::ref(res3))};
        auto fut4{std::async(std::launch::async, getTime, std::ref(res4))};

        const auto total1{fut1.get() + fut2.get() + fut3.get() + fut4.get()};

        const auto total2{res1 + res2 + res3 + res4};

        std::cout << total1 / 4.0 << std::endl;
        std::cout << total2 / 4.0 << std::endl;
    }

    {
        std::cout << "------------------------------------------------>> 3" << std::endl;

        auto res1{(double) 0.0};
        auto res2{(double) 0.0};
        auto res3{(double) 0.0};
        auto res4{(double) 0.0};

        std::thread th1(getTime, std::ref(res1));
        std::thread th2(getTime, std::ref(res2));
        std::thread th3(getTime, std::ref(res3));
        std::thread th4(getTime, std::ref(res4));

        th1.join();
        th2.join();
        th3.join();
        th4.join();

        const auto total{res1 + res2 + res3 + res4};

        std::cout << total / 4.0 << std::endl;
    }
}