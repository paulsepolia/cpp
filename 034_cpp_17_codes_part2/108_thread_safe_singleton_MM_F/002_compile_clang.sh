#!/bin/bash

  clang++    -c           \
             -O3          \
             -Wall        \
             -std=gnu++2a \
             main.cpp

  clang++    -pthread \
             main.o \
             -o x_clang

  rm *.o

