#!/bin/bash

  clang++   -O3          \
            -Wall        \
            -std=gnu++2a \
            -pthread     \
            ClockSyntonized.cpp \
            main.cpp \
            -o x_clang
