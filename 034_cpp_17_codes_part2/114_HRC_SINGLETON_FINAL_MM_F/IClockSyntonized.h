#pragma once

#include <chrono>

class IClockSyntonized {

public:

    virtual auto
    GetNanosecondsSinceFirstCall()
    -> std::chrono::duration<int64_t, std::nano> = 0;

    virtual auto
    GetNanosecondsSinceUnixEpochAtFirstCall()
    -> std::chrono::duration<int64_t, std::nano> = 0;

    virtual auto
    GetNanosecondsSinceSystemEpochAtFirstCall()
    -> std::chrono::duration<int64_t, std::nano> = 0;

    virtual auto
    GetNanosecondsSinceUnixEpochSyntonized()
    -> std::chrono::duration<int64_t, std::nano> = 0;

    virtual auto
    GetNanosecondsSinceSystemEpochSyntonized()
    -> std::chrono::duration<int64_t, std::nano> = 0;

    virtual ~IClockSyntonized() = default;
};
