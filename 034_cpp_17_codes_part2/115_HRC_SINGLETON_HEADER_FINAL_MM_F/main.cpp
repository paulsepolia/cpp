#include "ClockSyntonized.h"
#include <iostream>
#include <thread>
#include <vector>
#include <algorithm>
#include <mutex>

auto main() -> int {

    constexpr auto NUM_THREADS{(size_t) 4};
    constexpr auto DIM_MAX{(size_t) 10'000'000};

    ClockSyntonized *ins{&ClockSyntonized::GetInstance()};

    {
        std::cout << std::endl;
        std::cout << "-->> test 1A --> GetNanosecondsSinceFirstCall()" << std::endl;
        std::cout << std::endl;

        std::mutex mtx{};

        auto ld{[&]() {

            mtx.lock();
            std::cout << "-->> thread id = " << std::this_thread::get_id() << std::endl;
            mtx.unlock();

            auto a{std::vector<int64_t>(DIM_MAX)};

            for (size_t i = 0; i < DIM_MAX; i++) {
                a[i] = ins->GetNanosecondsSinceFirstCall().count();
            }

            auto ave{(double) 0.0};

            for (size_t i = 1; i < DIM_MAX; i++) {
                ave += (double) (a[i] - a[i - 1]);
            }

            mtx.lock();
            std::cout << "-->> average time between consecutive calls is (ns) = "
                      << (int64_t) (ave / (DIM_MAX - 1)) << std::endl;
            mtx.unlock();
        }
        };

        auto vecTh{std::vector<std::thread>(NUM_THREADS)};

        for (auto &el : vecTh) {
            el = std::thread(ld);
        }

        for (auto &el : vecTh) {
            el.join();
        }
    }

    {
        std::cout << std::endl;
        std::cout << "-->> test 1B --> std::steady_clock" << std::endl;
        std::cout << std::endl;

        std::mutex mtx{};

        auto ld{[&]() {

            mtx.lock();
            std::cout << "-->> thread id = " << std::this_thread::get_id() << std::endl;
            mtx.unlock();

            auto a{std::vector<int64_t>(DIM_MAX)};

            for (size_t i = 0; i < DIM_MAX; i++) {
                const auto tp1{std::chrono::steady_clock::now()};
                const auto tp2{std::chrono::steady_clock::now()};
                a[i] = (tp2 - tp1).count();
            }

            auto ave{(double) 0.0};

            for (size_t i = 0; i < DIM_MAX; i++) {
                ave += (double) a[i];
            }

            mtx.lock();
            std::cout << "-->> average time between consecutive calls is (ns) = "
                      << (int64_t) (ave / DIM_MAX) << std::endl;
            mtx.unlock();
        }
        };

        auto vecTh{std::vector<std::thread>(NUM_THREADS)};

        for (auto &el : vecTh) {
            el = std::thread(ld);
        }

        for (auto &el : vecTh) {
            el.join();
        }
    }

    {
        std::cout << std::endl;
        std::cout << "-->> test 2 --> BeginOfTimeInNanoseconds()" << std::endl;
        std::cout << std::endl;

        std::mutex mtx{};

        auto ld{[&]() {

            mtx.lock();

            std::cout << "-->> thread id = " << std::this_thread::get_id() << std::endl;

            mtx.unlock();

            auto a{std::vector<int64_t>(DIM_MAX)};

            for (size_t i = 0; i < DIM_MAX; i++) {
                a[i] = ins->GetNanosecondsSinceSystemEpochAtFirstCall().count();
            }

            std::sort(a.begin(), a.end());
            const auto it{std::unique(a.begin(), a.end())};
            a.resize(std::distance(a.begin(), it));

            mtx.lock();
            std::cout << "-->> the value should be exact one (1) = " << a.size() << std::endl;
            mtx.unlock();
        }
        };

        auto vecTh{std::vector<std::thread>(NUM_THREADS)};

        for (auto &el : vecTh) {
            el = std::thread(ld);
        }

        for (auto &el : vecTh) {
            el.join();
        }
    }

    {
        std::cout << std::endl;
        std::cout << "-->> test 3 --> GetNanosecondsSinceUnixEpochAtFirstCall()" << std::endl;
        std::cout << std::endl;

        std::cout << ins->GetNanosecondsSinceUnixEpochAtFirstCall().count() << std::endl;
    }

    {
        std::cout << std::endl;
        std::cout << "-->> test 4 --> GetNanosecondsSinceUnixEpochSyntonized()" << std::endl;
        std::cout << std::endl;

        std::cout << ins->GetNanosecondsSinceUnixEpochSyntonized().count() << std::endl;
    }

    {
        std::cout << std::endl;
        std::cout << "-->> test 5 --> GetNanosecondsSinceSystemEpochSyntonized()" << std::endl;
        std::cout << std::endl;

        std::cout << ins->GetNanosecondsSinceSystemEpochSyntonized().count() << std::endl;
    }

    {
        std::cout << std::endl;
        std::cout << "-->> test 6 --> GetNanosecondsSinceFirstCall()" << std::endl;
        std::cout << std::endl;

        std::cout << ins->GetNanosecondsSinceFirstCall().count() << std::endl;

        std::this_thread::sleep_for(std::chrono::seconds(1));

        std::cout << ins->GetNanosecondsSinceFirstCall().count() << std::endl;

        std::this_thread::sleep_for(std::chrono::seconds(1));

        std::cout << ins->GetNanosecondsSinceFirstCall().count() << std::endl;
    }

    {
        std::cout << std::endl;
        std::cout << "-->> test 7 --> GetNanosecondsSinceUnixEpochAtFirstCall()" << std::endl;
        std::cout << std::endl;

        std::cout << ins->GetNanosecondsSinceUnixEpochAtFirstCall().count() << std::endl;
    }

    {
        std::cout << std::endl;
        std::cout << "-->> test 8 --> GetNanosecondsSinceSystemEpochAtFirstCall()" << std::endl;
        std::cout << std::endl;

        std::cout << ins->GetNanosecondsSinceSystemEpochAtFirstCall().count() << std::endl;
    }

    {
        std::cout << std::endl;
        std::cout << "-->> test 9 --> GetNanosecondsSinceSystemEpochSyntonized()" << std::endl;
        std::cout << std::endl;

        std::cout << ins->GetNanosecondsSinceSystemEpochSyntonized().count() << std::endl;
    }

    {
        std::cout << std::endl;
        std::cout << "-->> test10 --> GetNanosecondsSinceFirstCall()" << std::endl;
        std::cout << std::endl;

        std::cout << ins->GetNanosecondsSinceFirstCall().count() << std::endl;
    }

    {
        std::cout << std::endl;
        std::cout << "-->> test11 --> GetNanosecondsSinceUnixEpochSyntonized()" << std::endl;
        std::cout << std::endl;

        std::cout << ins->GetNanosecondsSinceUnixEpochSyntonized().count() << std::endl;
    }
}
