#pragma once

#include <chrono>

#if _WIN32 || _WIN64

#include <Windows.h>

#elif __linux__

#include <ctime>

#else // generic Unix

#include <sys/time.h>

#endif

class ClockSyntonized {

private:

    static constexpr auto H1{(int64_t) 100LL};
    static constexpr auto K1{(int64_t) 1'000LL};
    static constexpr auto B1{(int64_t) 1'000'000'000LL};
    static constexpr auto OA_ZERO_TICKS{(int64_t) 94'353'120'000'000'000LL};

    int64_t nanosecondsSinceSystemEpochAtFirstCall{-1};
    int64_t nanosecondsSinceUnixEpochAtFirstCall{-1};
    int clockMonotonicStatus{-1};

    ClockSyntonized();

    ~ClockSyntonized() = default;

public:

    static ClockSyntonized &GetInstance() {
        static ClockSyntonized instance;
        return instance;
    }

    ClockSyntonized(const ClockSyntonized &) = delete;

    ClockSyntonized(ClockSyntonized &&) = delete;

    ClockSyntonized &operator=(const ClockSyntonized &) = delete;

    ClockSyntonized &operator=(ClockSyntonized &&) = delete;

    //=======//
    // SINCE //
    //=======//

    /**
     * Gives the timestamp in nanoseconds
     * since the time of first call of singleton.
     * We use that function to measure time intervals in nanoseconds.
     * Those time intervals are syntonized with the system clock.
     * Return type: std::chrono::duration<int64_t, std::nano>.
     */
    auto
    GetNanosecondsSinceFirstCall()
    -> std::chrono::duration<int64_t, std::nano>;

    //==========//
    // SINCE+AT //
    //==========//

    /**
     * Gives the nanoseconds since Unix Epoch
     * at the time of first call of the singleton.
     * Return type: std::chrono::duration<int64_t, std::nano>.
     */
    auto
    GetNanosecondsSinceUnixEpochAtFirstCall()
    -> std::chrono::duration<int64_t, std::nano>;

    /**
     * Gives the nanoseconds since System Epoch
     * at the time of first call of the singleton.
     * Return type: std::chrono::duration<int64_t, std::nano>.
     */
    auto
    GetNanosecondsSinceSystemEpochAtFirstCall()
    -> std::chrono::duration<int64_t, std::nano>;

    //===========//
    // SINCE+NOW //
    //===========//

    /**
     * Gives the "now" nanoseconds since Unix Epoch.
     * It is syntonized with system clock.
     * Return type: std::chrono::duration<int64_t, std::nano>.
     */
    auto
    GetNanosecondsSinceUnixEpochSyntonized()
    -> std::chrono::duration<int64_t, std::nano>;

    /**
     * Gives the nanoseconds since System Epoch "now".
     * So, on Linux and with CLOCK_MONOTONIC is the time passed since boot time.
     * On Linux and with CLOCK_REALTIME is the time passed since Unix Epoch.
     * On Windows is the time passed since Windows Epoch (1899/12/30 midnight, UTC).
     * It is syntonized with system clock.
     * Return type: std::chrono::duration<int64_t, std::nano>.
     */
    auto
    GetNanosecondsSinceSystemEpochSyntonized()
    -> std::chrono::duration<int64_t, std::nano>;

private:

#if _WIN32 || _WIN64

    /**
     * Sets "now" nanoseconds since System Epoch.
     */
    auto
    SetNanosecondsSinceSystemEpochAtFirstCall()
    -> void;

#elif __linux__

    /**
     * Sets "now" nanoseconds since boot time using CLOCK_MONOTONIC.
     */
    auto
    SetNanosecondsSinceUnixEpochAtFirstCallLinuxMonotonic()
    -> void;

    /**
     * Sets "now" nanoseconds since Unix Epoch using CLOCK_REALTIME.
     */
    auto
    SetNanosecondsSinceUnixEpochAtFirstCallLinuxRealtime()
    -> void;

#else
    /**
     * Sets "now" nanoseconds since Unix Epoch using Unix clock.
     */
    auto
    SetNanosecondsSinceUnixEpochAtFirstCallUnix()
    -> void;

#endif

    auto
    SetNanosecondsAtFirstCall()
    -> void;
};

ClockSyntonized::ClockSyntonized() {
    SetNanosecondsAtFirstCall();
}

auto
ClockSyntonized::GetNanosecondsSinceFirstCall()
-> std::chrono::duration<int64_t, std::nano> {

#if _WIN32 || _WIN64

    LARGE_INTEGER dt;
    LPFILETIME ft{new FILETIME};

    GetSystemTimePreciseAsFileTime(ft);
    dt.LowPart = ft->dwLowDateTime;
    dt.HighPart = ft->dwHighDateTime;

    const auto timeNow{(int64_t) (dt.QuadPart - OA_ZERO_TICKS)};
    delete ft;

    return std::chrono::nanoseconds(timeNow * H1 - nanosecondsSinceSystemEpochAtFirstCall);

#elif __linux__

    auto timeNow{(int64_t) 0LL};
    struct timespec ts{};

    if (clockMonotonicStatus == 0) {

        clock_gettime(CLOCK_MONOTONIC, &ts);
        timeNow = B1 * (int64_t) ts.tv_sec + (int64_t) ts.tv_nsec;

    } else {

        clock_gettime(CLOCK_REALTIME, &ts);
        timeNow = B1 * (int64_t) ts.tv_sec + (int64_t) ts.tv_nsec;
    }

    return std::chrono::nanoseconds(timeNow - nanosecondsSinceSystemEpochAtFirstCall);

#else // generic Unix

    struct timeval tv {};
    gettimeofday(&tv, nullptr);
    const auto timeNow {(int64_t) (B1 * (int64_t)tv.tv_sec + K1 * (int64_t)tv.tv_usec )};

    return std::chrono::nanoseconds(timeNow - nanosecondsSinceSystemEpochAtFirstCall);

#endif
}

auto
ClockSyntonized::GetNanosecondsSinceUnixEpochAtFirstCall()
-> std::chrono::duration<int64_t, std::nano> {
    return std::chrono::nanoseconds(nanosecondsSinceUnixEpochAtFirstCall);
}

auto
ClockSyntonized::GetNanosecondsSinceSystemEpochAtFirstCall()
-> std::chrono::duration<int64_t, std::nano> {
    return std::chrono::nanoseconds(nanosecondsSinceSystemEpochAtFirstCall);
}

auto
ClockSyntonized::GetNanosecondsSinceUnixEpochSyntonized()
-> std::chrono::duration<int64_t, std::nano> {
    return std::chrono::nanoseconds(
            nanosecondsSinceUnixEpochAtFirstCall + GetNanosecondsSinceFirstCall().count()
    );
}

auto
ClockSyntonized::GetNanosecondsSinceSystemEpochSyntonized()
-> std::chrono::duration<int64_t, std::nano> {
    return std::chrono::nanoseconds(
            nanosecondsSinceSystemEpochAtFirstCall + GetNanosecondsSinceFirstCall().count()
    );
}

#if _WIN32 || _WIN64

auto
ClockSyntonized::SetNanosecondsSinceSystemEpochAtFirstCall()
-> void {

    LARGE_INTEGER dt;
    LPFILETIME ft{new FILETIME};

    GetSystemTimePreciseAsFileTime(ft);
    dt.LowPart = ft->dwLowDateTime;
    dt.HighPart = ft->dwHighDateTime;

    const auto timeNow{(int64_t) (dt.QuadPart - OA_ZERO_TICKS)};
    delete ft;

    nanosecondsSinceSystemEpochAtFirstCall =
            H1 * timeNow;
    nanosecondsSinceUnixEpochAtFirstCall =
            std::chrono::duration_cast<std::chrono::nanoseconds>(
                    std::chrono::system_clock::now().time_since_epoch()).count();
}

#elif __linux__

auto
ClockSyntonized::SetNanosecondsSinceUnixEpochAtFirstCallLinuxMonotonic()
-> void {

    struct timespec ts{};
    clock_gettime(CLOCK_MONOTONIC, &ts);

    const auto timeNow{(int64_t)(B1 * (int64_t) ts.tv_sec + (int64_t) ts.tv_nsec)};
    nanosecondsSinceSystemEpochAtFirstCall =
            timeNow;
    nanosecondsSinceUnixEpochAtFirstCall =
            std::chrono::duration_cast<std::chrono::nanoseconds>(
                    std::chrono::system_clock::now().time_since_epoch()).count();
}

auto
ClockSyntonized::SetNanosecondsSinceUnixEpochAtFirstCallLinuxRealtime()
-> void {

    struct timespec ts{};
    clock_gettime(CLOCK_REALTIME, &ts);

    const auto timeNow{(int64_t)(B1 * (int64_t) ts.tv_sec + (int64_t) ts.tv_nsec)};
    nanosecondsSinceSystemEpochAtFirstCall = timeNow;
    nanosecondsSinceUnixEpochAtFirstCall = timeNow;
}

#else

auto
ClockSyntonized::SetNanosecondsSinceUnixEpochAtFirstCallUnix()
-> void {

    struct timeval tv {};
    gettimeofday(&tv, nullptr);

    const auto timeNow{(int64_t)(B1 * (int64_t)tv.tv_sec + K1 * (int64_t)tv.tv_usec)};
    nanosecondsSinceUnixEpochAtFirstCall = timeNow;
    nanosecondsSinceSystemEpochAtFirstCall = timeNow;
}
#endif

auto
ClockSyntonized::SetNanosecondsAtFirstCall()
-> void {

#if _WIN32 || _WIN64

    SetNanosecondsSinceSystemEpochAtFirstCall();

#elif __linux__

    struct timespec ts{};
    clockMonotonicStatus = (int) clock_gettime(CLOCK_MONOTONIC, &ts);

    if (clockMonotonicStatus == 0) {
        clock_gettime(CLOCK_MONOTONIC, &ts);
        SetNanosecondsSinceUnixEpochAtFirstCallLinuxMonotonic();
    } else {
        clock_gettime(CLOCK_REALTIME, &ts);
        SetNanosecondsSinceUnixEpochAtFirstCallLinuxRealtime();
    }

#else // generic Unix

    SetNanosecondsSinceUnixEpochAtFirstCallUnix();

#endif
}
