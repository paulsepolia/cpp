#include <vector>
#include <iostream>
#include <atomic>
#include <thread>
#include <iomanip>
#include <chrono>
#include <mutex>
#include <cassert>

class benchmark_timer {
public:

    explicit benchmark_timer(const double &res) :
            _res(res),
            t1{std::chrono::steady_clock::now()},
            t2{std::chrono::steady_clock::now()} {};

    ~benchmark_timer() {
        t2 = std::chrono::steady_clock::now();
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count()};
        std::cout << " --> res = " << _res << std::endl;
        std::cout << " --> time used (secs) = " << total_time << std::endl;
    }

    auto set_res(const double &res) -> void {
        _res = res;
    }

private:

    double _res{};
    decltype(std::chrono::steady_clock::now()) t1{};
    decltype(std::chrono::steady_clock::now()) t2{};
};

constexpr auto DO_MAX{(size_t) 10'000};
constexpr auto NUM_THREADS{(size_t) 4};
constexpr auto ZERO_DBL{(size_t) 0.0};
constexpr auto ONE_DBL{(double) 1.0};
constexpr auto DEBUG_FLG{false};

// holds true when locked
// holds false when unlocked
std::atomic<bool> lock(false);
auto sum_loc{(double) ZERO_DBL};
auto mtx{std::mutex{}};

void f1() {

    while (std::atomic_exchange_explicit(&lock, true, std::memory_order_acquire)) {
        if (DEBUG_FLG) {
            std::cout << "spinning now --> " << std::this_thread::get_id() << std::endl;
            std::cout.flush();
        }
    }

    // do job here

    if (DEBUG_FLG) {
        std::cout << " --> do job here from thread --> " << std::this_thread::get_id() << std::endl;
    }

    for (size_t cnt1 = 0; cnt1 < DO_MAX; ++cnt1) {
        for (size_t cnt2 = 0; cnt2 < DO_MAX; ++cnt2) {
            sum_loc += ONE_DBL;
        }
    }
    std::atomic_store_explicit(&lock, false, std::memory_order_release);
}

void f2() {

    // do job here

    for (size_t cnt1 = 0; cnt1 < DO_MAX; ++cnt1) {
        for (size_t cnt2 = 0; cnt2 < DO_MAX; ++cnt2) {

            while (std::atomic_exchange_explicit(&lock, true, std::memory_order_acquire)) {

                if (cnt1 == 0 && cnt2 == 0 && DEBUG_FLG) {
                    std::cout << " --> do job here from thread --> " << std::this_thread::get_id() << std::endl;
                }

                if (DEBUG_FLG) {
                    std::cout << "spinning now --> " << std::this_thread::get_id() << std::endl;
                    std::cout.flush();
                }
            }

            sum_loc += ONE_DBL;
            std::atomic_store_explicit(&lock, false, std::memory_order_release);
        }
    }
}

void f3() {

    // do job here

    mtx.lock();

    if (DEBUG_FLG) {
        std::cout << " --> do job here from thread --> " << std::this_thread::get_id() << std::endl;
    }

    for (size_t cnt1 = 0; cnt1 < DO_MAX; ++cnt1) {
        for (size_t cnt2 = 0; cnt2 < DO_MAX; ++cnt2) {
            sum_loc += ONE_DBL;
        }
    }

    mtx.unlock();
}

void f4() {

    // do job here

    for (size_t cnt1 = 0; cnt1 < DO_MAX; ++cnt1) {
        for (size_t cnt2 = 0; cnt2 < DO_MAX; ++cnt2) {

            std::lock_guard<std::mutex> guard(mtx);

            if (cnt1 == 0 && cnt2 == 0 && DEBUG_FLG) {
                std::cout << " --> do job here from thread --> " << std::this_thread::get_id() << std::endl;
            }
            sum_loc += ONE_DBL;
        }
    }
}

void f5() {

    // do job here

    for (size_t cnt1 = 0; cnt1 < DO_MAX; ++cnt1) {
        for (size_t cnt2 = 0; cnt2 < DO_MAX; ++cnt2) {

            mtx.lock();

            if (cnt1 == 0 && cnt2 == 0 && DEBUG_FLG) {
                std::cout << " --> do job here from thread --> " << std::this_thread::get_id() << std::endl;
            }

            sum_loc += ONE_DBL;
            mtx.unlock();
        }
    }
}

auto main() -> int {

    std::cout << std::boolalpha;

    {
        sum_loc = ZERO_DBL;

        std::cout << " -->> 1 --> ONE THREAD AT A TIME --> ALL ADDS --> std::atomic_exchange_explicit" << std::endl;
        std::cout.flush();

        auto ot{benchmark_timer(0.0)};
        auto v{std::vector<std::thread>()};

        for (size_t n = 0; n < NUM_THREADS; ++n) {
            v.emplace_back(f1);
        }

        for (auto &t : v) {
            t.join();
        }

        std::cout << " --> sum_loc --> " << sum_loc << std::endl;
        assert(sum_loc == (double) (NUM_THREADS * DO_MAX) * DO_MAX);

        ot.set_res(sum_loc);
    }

    {
        sum_loc = ZERO_DBL;

        std::cout << " -->> 2 --> ONE THREAD AT A TIME --> ONE ADD --> std::atomic_exchange_explicit" << std::endl;
        std::cout.flush();

        auto ot{benchmark_timer(0.0)};
        auto v{std::vector<std::thread>()};

        for (size_t n = 0; n < NUM_THREADS; ++n) {
            v.emplace_back(f2);
        }

        for (auto &t : v) {
            t.join();
        }

        std::cout << " --> sum_loc --> " << sum_loc << std::endl;
        assert(sum_loc == (double) (NUM_THREADS * DO_MAX) * DO_MAX);

        ot.set_res(sum_loc);
    }

    {
        sum_loc = ZERO_DBL;

        std::cout << " -->> 3 --> ONE THREAD AT A TIME --> ALL ADDS --> std::mutex" << std::endl;
        std::cout.flush();

        auto ot{benchmark_timer(0.0)};
        auto v{std::vector<std::thread>()};

        for (size_t n = 0; n < NUM_THREADS; ++n) {
            v.emplace_back(f3);
        }

        for (auto &t : v) {
            t.join();
        }

        std::cout << " --> sum_loc --> " << sum_loc << std::endl;
        assert(sum_loc == (double) (NUM_THREADS * DO_MAX) * DO_MAX);

        ot.set_res(sum_loc);
    }

    {
        sum_loc = ZERO_DBL;

        std::cout << " -->> 4 --> ONE THREAD AT A TIME --> ONE ADD(guard_lock) --> std::mutex" << std::endl;
        std::cout.flush();

        auto ot{benchmark_timer(0.0)};
        auto v{std::vector<std::thread>()};

        for (size_t n = 0; n < NUM_THREADS; ++n) {
            v.emplace_back(f4);
        }

        for (auto &t : v) {
            t.join();
        }

        std::cout << " --> sum_loc --> " << sum_loc << std::endl;
        assert(sum_loc == (double) (NUM_THREADS * DO_MAX) * DO_MAX);

        ot.set_res(sum_loc);
    }

    {
        sum_loc = ZERO_DBL;

        std::cout << " -->> 5 --> ONE THREAD AT A TIME --> ONE ADD(mtx.lock/mtx.unlock) --> std::mutex" << std::endl;
        std::cout.flush();

        auto ot{benchmark_timer(0.0)};
        auto v{std::vector<std::thread>()};

        for (size_t n = 0; n < NUM_THREADS; ++n) {
            v.emplace_back(f5);
        }

        for (auto &t : v) {
            t.join();
        }

        std::cout << " --> sum_loc --> " << sum_loc << std::endl;
        assert(sum_loc == (double) (NUM_THREADS * DO_MAX) * DO_MAX);

        ot.set_res(sum_loc);
    }
}
