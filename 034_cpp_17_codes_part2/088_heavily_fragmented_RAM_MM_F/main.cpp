#include <iostream>
#include <vector>
#include <chrono>
#include <set>
#include <unordered_set>
#include <algorithm>

class benchmark_timer {
public:

    explicit benchmark_timer(const double &res) :
            _res(res),
            t1{std::chrono::steady_clock::now()},
            t2{std::chrono::steady_clock::now()},
            t_prev{std::chrono::steady_clock::now()} {};

    auto print_time() const -> void {
        const auto t_loc{std::chrono::steady_clock::now()};
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t_loc - t_prev).count()};
        t_prev = t_loc;
        std::cout << " --> time used (secs) = " << total_time << std::endl;
    }

    auto reset_timer() -> void {
        const auto t_loc{std::chrono::steady_clock::now()};
        t_prev = t_loc;
        t1 = t_loc;
        t2 = t_loc;
    }

    ~benchmark_timer() {
        t2 = std::chrono::steady_clock::now();
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count()};
        std::cout << " --> res = " << _res << std::endl;
        std::cout << " --> total time used (secs) = " << total_time << std::endl;
    }

    auto set_res(const double &res) -> void {
        _res = res;
    }

private:

    double _res{};
    decltype(std::chrono::steady_clock::now()) t1{};
    decltype(std::chrono::steady_clock::now()) t2{};
    mutable decltype(std::chrono::steady_clock::now()) t_prev{};
};

const auto DIM1{(size_t) 120'000'000};
const auto REC1{(size_t) 24};

int main() {

    {
        std::cout << "------------------------------>> 1 -->> unordered_set" << std::endl;

        auto ot{benchmark_timer(0.0)};
        auto cont{std::unordered_set<void *>()};

        for (size_t i = 0; i < DIM1; i++) {

            void *ptr{malloc(REC1)};
            cont.emplace(ptr);
        }

        void *ptr{malloc(REC1)};
        auto it{cont.find(ptr)};

        std::cout << "out --> " << &it << std::endl;
        ot.print_time();
        ot.reset_timer();
    }

    {
        std::cout << "------------------------------>> 2 -->> set" << std::endl;

        auto ot{benchmark_timer(0.0)};
        auto cont{std::set<void *>()};

        for (size_t i = 0; i < DIM1; i++) {

            void *ptr{malloc(REC1)};
            cont.emplace(ptr);
        }

        void *ptr{malloc(REC1)};
        auto it{cont.find(ptr)};

        std::cout << "out --> " << &it << std::endl;
        ot.print_time();
        ot.reset_timer();
    }

    {
        std::cout << "------------------------------>> 3 -->> vector" << std::endl;

        auto ot{benchmark_timer(0.0)};
        auto cont{std::vector<void *>()};

        for (size_t i = 0; i < DIM1; i++) {

            void *ptr{malloc(REC1)};
            cont.push_back(ptr);
        }

        void *ptr{malloc(REC1)};
        auto it{std::find(cont.begin(), cont.end(), ptr)};

        std::cout << "out --> " << &it << std::endl;
        ot.print_time();
        ot.reset_timer();
    }
}
