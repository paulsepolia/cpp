#include <iostream>
#include <vector>
#include <memory>
#include <functional>
#include <chrono>
#include <iomanip>
#include <cmath>

class benchmark_timer {
public:

    explicit benchmark_timer(const double &res) :
            _res(res),
            t1{std::chrono::steady_clock::now()},
            t2{std::chrono::steady_clock::now()} {};

    ~benchmark_timer() {
        t2 = std::chrono::steady_clock::now();
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count()};
        std::cout << " --> res = " << _res << std::endl;
        std::cout << " --> time used (secs) = " << total_time << std::endl;
    }

    auto set_res(const double &res) -> void {
        _res = res;
    }

private:

    double _res{};
    decltype(std::chrono::steady_clock::now()) t1{};
    decltype(std::chrono::steady_clock::now()) t2{};
};

class A {
public:
    A() = default;

    explicit A(std::string name) : _name(std::move(name)) {}

    [[nodiscard]] auto get_name() const -> std::string {
        return _name;
    }

    auto set_name(std::string name) -> void {
        _name = std::move(name);
    }

    virtual ~A() = default;

    auto operator new(size_t size) -> void * {
        void *p{std::malloc(size)};
        if (DEBUG_LOG) {
            std::cout << "new: allocated " << size << " bytes" << std::endl;
        }
        return p;
    }

    auto operator delete(void *p) noexcept -> void {
        if (DEBUG_LOG) {
            std::cout << "delete: deleted memory" << std::endl;
        }
        return std::free(p);
    }

    auto operator new[](size_t size) -> void * {
        void *p{std::malloc(size)};
        if (DEBUG_LOG) {
            std::cout << "new: allocated " << size << " bytes" << std::endl;
        }
        return p;
    }

    auto operator delete[](void *p) noexcept -> void {
        if (DEBUG_LOG) {
            std::cout << "delete: deleted memory" << std::endl;
        }
        return std::free(p);
    }

private:
    std::string _name;
public:
    static bool DEBUG_LOG;
};

bool A::DEBUG_LOG = false;

int main() {

    std::cout << std::setprecision(5);
    std::cout << std::scientific;

    std::cout << " --> sizeof(A)             = " << sizeof(A) << std::endl;
    std::cout << " --> sizeof(A[2])          = " << sizeof(A[2]) << std::endl;
    std::cout << " --> sizeof(A[9])          = " << sizeof(A[9]) << std::endl;
    std::cout << " --> sizeof(A(1234567890)) = " << sizeof(A("1234567890")) << std::endl;
    std::string long_name{"2222"};

    for (size_t kk = 0; kk < 100; kk++) {
        long_name += "22222222";
    }

    std::cout << " --> name size = " << long_name.size() << std::endl;
    std::cout << " --> sizeof(A(long_name))  = " << sizeof(A(long_name)) << std::endl;
}
