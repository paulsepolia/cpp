#include <iostream>
#include <chrono>
#include <iomanip>

class benchmark_timer {
public:

    explicit benchmark_timer(const double &res) :
            _res(res),
            t1{std::chrono::steady_clock::now()},
            t2{std::chrono::steady_clock::now()} {};

    ~benchmark_timer() {
        t2 = std::chrono::steady_clock::now();
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count()};
        std::cout << " --> res = " << _res << std::endl;
        std::cout << " --> time used (secs) = " << total_time << std::endl;
    }

    auto set_res(const double &res) -> void {
        _res = res;
    }

private:

    double _res{};
    decltype(std::chrono::steady_clock::now()) t1{};
    decltype(std::chrono::steady_clock::now()) t2{};
};


auto main() -> int {

    {
        const double *p1;
        double x1 = 10;
        double x2 = 11;

        p1 = &x1;

        std::cout << " -->  p1 = " << p1 << std::endl;
        std::cout << " --> *p1 = " << *p1 << std::endl;

        p1 = &x2;

        std::cout << " -->  p1 = " << p1 << std::endl;
        std::cout << " --> *p1 = " << *p1 << std::endl;

        *(const_cast<double *>(p1)) = 100;

        std::cout << " x1 = " << x1 << std::endl;
        std::cout << " x2 = " << x2 << std::endl;

        *(const_cast<double *>(p1)) = 101;

        std::cout << " x1 = " << x1 << std::endl;
        std::cout << " x2 = " << x2 << std::endl;

        *(const_cast<double *>(p1)) = 102;

        std::cout << " x1 = " << x1 << std::endl;
        std::cout << " x2 = " << x2 << std::endl;
    }

    {
        double x1 = 10;
        double x2 = 11;
        const double * const p1 = &x1;

        std::cout << " -->  p1 = " << p1 << std::endl;
        std::cout << " --> *p1 = " << *p1 << std::endl;

        //p1 = &x2;

        std::cout << " -->  p1 = " << p1 << std::endl;
        std::cout << " --> *p1 = " << *p1 << std::endl;

        *(const_cast<double *>(p1)) = 111;

        std::cout << " x1 = " << x1 << std::endl;

        *(const_cast<double *>(p1)) = 222;

        std::cout << " x1 = " << x1 << std::endl;

        *(const_cast<double *>(p1)) = 333;

        std::cout << " x1 = " << x1 << std::endl;
    }

    {
        // undefined behaviour

        const double x1 = 10;
        const double * const p1 = &x1;

        std::cout << " -->  p1 = " << p1 << std::endl;
        std::cout << " --> *p1 = " << *p1 << std::endl;

        std::cout << " -->  p1 = " << p1 << std::endl;
        std::cout << " --> *p1 = " << *p1 << std::endl;

        *(const_cast<double *>(p1)) = 111;

        std::cout << " x1 = " << x1 << std::endl;

        *(const_cast<double *>(p1)) = 222;

        std::cout << " x1 = " << x1 << std::endl;

        *(const_cast<double *>(p1)) = 333;

        std::cout << " x1 = " << x1 << std::endl;
    }
}
