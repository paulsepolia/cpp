#include <iostream>
#include <future>
#include <vector>
#include <chrono>
#include <thread>

class benchmark_timer {
public:

    explicit benchmark_timer(const double &res) :
            _res(res),
            t1{std::chrono::steady_clock::now()},
            t2{std::chrono::steady_clock::now()},
            t_prev{std::chrono::steady_clock::now()} {};

    auto print_time() const -> void {
        const auto t_loc{std::chrono::steady_clock::now()};
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t_loc - t_prev).count()};
        t_prev = t_loc;
        std::cout << " --> time used (secs) = " << total_time << std::endl;
    }

    auto reset_timer() -> void {
        const auto t_loc{std::chrono::steady_clock::now()};
        t_prev = t_loc;
        t1 = t_loc;
        t2 = t_loc;
    }

    ~benchmark_timer() {
        t2 = std::chrono::steady_clock::now();
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count()};
        std::cout << " --> res = " << _res << std::endl;
        std::cout << " --> total time used (secs) = " << total_time << std::endl;
    }

    auto set_res(const double &res) -> void {
        _res = res;
    }

private:

    double _res{};
    decltype(std::chrono::steady_clock::now()) t1{};
    decltype(std::chrono::steady_clock::now()) t2{};
    mutable decltype(std::chrono::steady_clock::now()) t_prev{};
};

constexpr auto DIM_MAX{(size_t) 50'000};
constexpr auto DO_MAX{(size_t) 10'000'000};
constexpr auto NT{(size_t) 10};

auto main() -> int {

    auto test1 = []() {
        std::cout << "--------------------------------->> test-type = 1" << std::endl;

        try {

            auto ot{benchmark_timer(0)};

            auto proms{std::vector<std::promise<double>>{}};
            auto futs{std::vector<std::future<double>>{}};

            ot.print_time();

            std::cout << " --> building vectors of promises..." << std::endl;

            for (size_t kk = 0; kk < DIM_MAX; kk++) {

                proms.emplace_back(std::promise<double>{});
            }

            ot.print_time();

            std::cout << " --> building vectors of their corresponding futures..." << std::endl;

            for (size_t kk = 0; kk < DIM_MAX; kk++) {

                futs.emplace_back(proms[kk].get_future());
            }

            ot.print_time();

            std::cout << " --> deleting vector of promises... --> clear()" << std::endl;

            proms.clear();

            ot.print_time();

            std::cout << " --> deleting vector of promises... --> shrink_to_fit()" << std::endl;

            proms.shrink_to_fit();

            ot.print_time();

            std::cout << " --> deleting vector of futures... --> clear()" << std::endl;

            futs.clear();

            ot.print_time();

            std::cout << " --> deleting vector of futures... --> shrink_to_fit()" << std::endl;

            futs.shrink_to_fit();

            ot.print_time();
        }
        catch (const std::exception &e) {
            std::cout << "ERROR = " << e.what() << std::endl;
        }
    };

    auto test2 = []() {
        std::cout << "--------------------------------->> test-type = 2 (reversed order the deletion)" << std::endl;

        auto ot{benchmark_timer(0)};

        auto proms{std::vector<std::promise<double>>{}};
        auto futs{std::vector<std::future<double>>{}};

        ot.print_time();

        std::cout << " --> building vectors of promises..." << std::endl;

        for (size_t kk = 0; kk < DIM_MAX; kk++) {

            proms.emplace_back(std::promise<double>{});
        }

        ot.print_time();

        std::cout << " --> building vectors of their corresponding futures..." << std::endl;

        for (size_t kk = 0; kk < DIM_MAX; kk++) {

            futs.emplace_back(proms[kk].get_future());
        }

        ot.print_time();

        std::cout << " --> deleting vector of futures... --> clear()" << std::endl;

        futs.clear();

        ot.print_time();

        std::cout << " --> deleting vector of futures... --> shrink_to_fit()" << std::endl;

        futs.shrink_to_fit();

        ot.print_time();

        std::cout << " --> deleting vector of promises... --> clear()" << std::endl;

        proms.clear();

        ot.print_time();

        std::cout << " --> deleting vector of promises... --> shrink_to_fit()" << std::endl;

        proms.shrink_to_fit();

        ot.print_time();
    };

    auto test3 = []() {
        std::cout << "--------------------------------->> test-type = 3" << std::endl;

        auto ot{benchmark_timer(0)};

        std::cout << " --> building vectors of promises..." << std::endl;

        auto proms{std::vector<std::promise<double>>{}};

        proms.resize(DIM_MAX);

        std::cout << " --> building vectors of futures(no corresponding promises)..." << std::endl;

        auto futs{std::vector<std::future<double>>{}};

        futs.resize(DIM_MAX);

        ot.print_time();

        std::cout << " --> deleting vector of promises... --> clear()" << std::endl;

        proms.clear();

        ot.print_time();

        std::cout << " --> deleting vector of promises... --> shrink_to_fit()" << std::endl;

        proms.shrink_to_fit();

        ot.print_time();

        std::cout << " --> deleting vector of futures... --> clear()" << std::endl;

        futs.clear();

        ot.print_time();

        std::cout << " --> deleting vector of futures... --> shrink_to_fit()" << std::endl;

        futs.shrink_to_fit();

        ot.print_time();
    };

    for (size_t i = 0; i < DO_MAX; i++) {

        std::cout << "------------------------------------------------------------------------------>> run = "
                  << i << std::endl;

        auto vt{std::vector<std::thread>{}};

        for (size_t k1 = 0; k1 < NT; k1++) {
            vt.emplace_back(std::thread(test1));
        }

        for (size_t k1 = 0; k1 < NT; k1++) {
            vt.emplace_back(std::thread(test2));
        }

        for (size_t k1 = 0; k1 < NT; k1++) {
            vt.emplace_back(std::thread(test3));
        }

        for (auto &el: vt) {
            el.join();
        }
    }
}
