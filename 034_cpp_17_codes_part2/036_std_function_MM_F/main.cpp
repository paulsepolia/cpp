#include <iostream>
#include <cstdint>
#include <iomanip>
#include <functional>

auto mult2(const double &arg) {
    return 2 * arg;
}

template<typename T>
T mult2_T(const T &arg) {
    return 2 * arg;
}

template<typename T>
double mult_T(const T &arg, const double &coef) {
    return coef * arg;
}

int main() {

    {
        std::cout << " --> example --> 1 --> start" << std::endl;

        std::function<double(const double &)> mult2_f = mult2;

        std::cout << mult2_f(10) << std::endl;
        std::cout << mult2_f(11) << std::endl;

        std::cout << " --> example --> 1 --> end" << std::endl;
    }

    {
        std::cout << " --> example --> 2 --> start" << std::endl;

        std::function<double(double)> mult2_f = mult2;

        std::cout << mult2_f(10) << std::endl;
        std::cout << mult2_f(11) << std::endl;

        std::cout << " --> example --> 2 --> end" << std::endl;
    }

    {
        std::cout << " --> example --> 3 --> start" << std::endl;

        std::function<double(double)> mult2_f = mult2_T<double>;

        std::cout << mult2_f(10) << std::endl;
        std::cout << mult2_f(11) << std::endl;

        std::cout << " --> example --> 3 --> end" << std::endl;
    }

    {
        std::cout << " --> example --> 4 --> start" << std::endl;

        std::function<double(const double &)> mult2_f = mult2_T<double>;

        std::cout << mult2_f(10) << std::endl;
        std::cout << mult2_f(11) << std::endl;

        std::cout << " --> example --> 4 --> end" << std::endl;
    }

    {
        std::cout << " --> example --> 5 --> start" << std::endl;

        std::function<double(const double &)> mult2_f = [](const double &val) {
            return 2 * val;
        };

        std::cout << mult2_f(10) << std::endl;
        std::cout << mult2_f(11) << std::endl;

        std::cout << " --> example --> 5 --> end" << std::endl;
    }

    {
        std::cout << " --> example --> 6 --> start" << std::endl;

        std::function<double(const double &)> mult2_f = [](double val) {
            return 2 * val;
        };

        std::cout << mult2_f(10) << std::endl;
        std::cout << mult2_f(11) << std::endl;

        std::cout << " --> example --> 6 --> end" << std::endl;
    }

    {
        std::cout << " --> example --> 7 --> start" << std::endl;
        const std::uint64_t fac = 3;
        std::function<double(const double &)> mult2_f = [=](const double &val) {
            return fac * val;
        };

        std::cout << mult2_f(10) << std::endl;
        std::cout << mult2_f(11) << std::endl;

        std::cout << " --> example --> 7 --> end" << std::endl;
    }

    {
        std::cout << " --> example --> 8 --> start" << std::endl;
        std::uint64_t fac = 4;
        std::function<double(const double &)> mult2_f = [&](const double &val) {
            fac = 5;
            return fac * val;
        };

        std::cout << mult2_f(10) << std::endl;
        std::cout << mult2_f(11) << std::endl;
        std::cout << fac << std::endl;

        std::cout << " --> example --> 8 --> end" << std::endl;
    }

    {
        std::cout << " --> example --> 9 --> start" << std::endl;

        std::function<double(const double &, const double &)> mult_f = mult_T<double>;

        std::cout << mult_f(10, 2.0) << std::endl;
        std::cout << mult_f(11, 3.0) << std::endl;

        std::cout << " --> example --> 9 --> end" << std::endl;
    }

}