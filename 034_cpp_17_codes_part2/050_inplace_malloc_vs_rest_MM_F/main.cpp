#include <iostream>
#include <vector>
#include <memory>
#include <functional>
#include <chrono>
#include <iomanip>
#include <cmath>

class benchmark_timer {
public:

    explicit benchmark_timer(const double &res) :
            _res(res),
            t1{std::chrono::steady_clock::now()},
            t2{std::chrono::steady_clock::now()} {};

    ~benchmark_timer() {
        t2 = std::chrono::steady_clock::now();
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count()};
        std::cout << " --> res = " << _res << std::endl;
        std::cout << " --> time used (secs) = " << total_time << std::endl;
    }

    auto set_res(const double &res) -> void {
        _res = res;
    }

private:

    double _res{};
    decltype(std::chrono::steady_clock::now()) t1{};
    decltype(std::chrono::steady_clock::now()) t2{};
};

int main() {

    std::cout << std::setprecision(5);
    std::cout << std::scientific;
    const auto TRY_MAX{size_t(std::pow(10.0, 0.0))};
    const auto DO_MAX{size_t(std::pow(10.0, 1.0))};
    const auto SIZE_LOC{size_t(std::pow(10.0, 1.0))};

    for (size_t ii = 0; ii < TRY_MAX; ii++) {

        std::cout << "-------------------------------------------->> # " << ii << std::endl;

        {
            std::cout << "-->> 1 --> inplace only" << std::endl;
            auto sum_tot{0.0};
            auto ot{benchmark_timer(0.0)};

            auto memory{std::malloc(SIZE_LOC * sizeof(double))};

            for (size_t kk = 0; kk < DO_MAX; kk++) {

                auto p{new(memory) double[SIZE_LOC]};

                for (size_t jj = 0; jj < SIZE_LOC; jj++) {
                    p[jj] = (double) jj;
                }

                sum_tot += p[1];
            }
            ot.set_res(sum_tot);
            std::free(memory);
        }
        {
            std::cout << "-->> 2 --> malloc + inplace + free" << std::endl;

            auto sum_tot{0.0};
            auto ot{benchmark_timer(0.0)};

            for (size_t kk = 0; kk < DO_MAX; kk++) {

                auto memory{std::malloc(SIZE_LOC * sizeof(double))};

                auto p{new(memory) double[SIZE_LOC]};

                for (size_t jj = 0; jj < SIZE_LOC; jj++) {
                    p[jj] = (double) jj;
                }

                sum_tot += p[1];
                std::free(memory);
            }
            ot.set_res(sum_tot);
        }
        {
            std::cout << "-->> 3 --> malloc + free" << std::endl;

            auto sum_tot{0.0};
            auto ot{benchmark_timer(0.0)};

            for (size_t kk = 0; kk < DO_MAX; kk++) {

                auto p{(double *) std::malloc(SIZE_LOC * sizeof(double))};

                for (size_t jj = 0; jj < SIZE_LOC; jj++) {
                    p[jj] = (double) jj;
                }

                sum_tot += p[1];
                std::free(p);
            }
            ot.set_res(sum_tot);
        }
        {
            std::cout << "-->> 4 --> new + delete" << std::endl;

            auto sum_tot{0.0};
            auto ot{benchmark_timer(0.0)};

            for (size_t kk = 0; kk < DO_MAX; kk++) {

                auto p{new double[SIZE_LOC]};

                for (size_t jj = 0; jj < SIZE_LOC; jj++) {
                    p[jj] = (double) jj;
                }

                sum_tot += p[1];
                delete[]p;
            }
            ot.set_res(sum_tot);
        }
    }
}
