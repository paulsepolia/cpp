#include <iostream>
#include <chrono>
#include <limits>
#include <vector>
#include <algorithm>

inline static auto getNanosecondsSinceUnixEpoch() -> int64_t {

    return std::chrono::duration_cast<std::chrono::nanoseconds>(
            std::chrono::system_clock::now().time_since_epoch()).count();
}

inline static auto getMaxInt64() -> int64_t {

    return std::numeric_limits<int64_t>::max();
}

inline static auto getMinInt64() -> int64_t {

    return std::numeric_limits<int64_t>::min();
}

auto main() -> int {

    // Unix Epoch magnitude is a 19 digits integer: 1574259040982859424
    // Windows Epoch magnitude is a 19 digits integer: 2*1574259040982859424

    std::vector<int64_t> res;

    for(int64_t i = 0; i < 1000; i++)
    {
        int64_t a1 = 1574259040982859424 + i;
        double a2 = a1;
        int64_t a3 = a2;

        res.push_back(a1-a3);
    }

    std::sort(res.begin(), res.end());
    auto it = std::unique(res.begin(), res.end());
    res.resize(std::distance(res.begin(),it));
    std::cout << "size = " << res.size() << std::endl;
    for(const auto & el: res)
    {
        std::cout << el << std::endl;
    }
}
