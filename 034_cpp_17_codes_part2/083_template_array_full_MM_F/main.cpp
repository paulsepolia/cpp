#include <iostream>
#include <typeinfo>

template<typename T, size_t N>
auto f1() -> void {
    std::cout << " --> the template argument N provided is = " << N << std::endl;
    std::cout << " --> the size is = " << sizeof(T) << std::endl;
    std::cout << " --> the type-id is = " << typeid(T).name() << std::endl;
}

template<typename T, size_t N>
class ArrayStack {
public:

    T &operator[](size_t i) {
        return _a[i];
    }

private:
    T _a[N]{};
};

template<typename T, size_t N>
class ArrayDyn {
public:

    ArrayDyn() : _a(new T[N]) {
    }

    T &operator[](size_t i) {
        return _a[i];
    }

    ArrayDyn(const ArrayDyn &a) : _a(new T[N]) {
        for (size_t i = 0; i < N; i++) {
            _a[i] = a._a[i];
        }
    }

    ArrayDyn(ArrayDyn &&a) noexcept {
        this->_a = a._a;
        a._a = nullptr;
    }

    ArrayDyn &operator=(const ArrayDyn &a) {
        if (this != &a) {
            for (size_t i = 0; i < N; i++) {
                _a[i] = a._a[i];
            }
        }
        return *this;
    }

    ArrayDyn &operator=(ArrayDyn &&a) noexcept {
        if (this != &a) {
            for (size_t i = 0; i < N; i++) {
                _a[i] = std::move(a._a[i]);
            }
        }
        return *this;
    }

    virtual ~ArrayDyn() {
        delete[] _a;
    }

private:
    T *_a{nullptr};
};

auto main() -> int {

    {
        std::cout << "------------------------------>> 1" << std::endl;
        f1<double, 100>();
        f1<float, 200>();
    }

    {
        std::cout << "------------------------------>> 2" << std::endl;
        ArrayStack<double, 100> a1{};
        a1[0] = 1;
        a1[1] = 2;
        a1[2] = 3;

        std::cout << a1[0] << std::endl;
        std::cout << a1[1] << std::endl;
        std::cout << a1[2] << std::endl;

        ArrayStack<double, 100> a2{};

        a2 = a1;

        std::cout << a2[0] << std::endl;
        std::cout << a2[1] << std::endl;
        std::cout << a2[2] << std::endl;
    }

    {
        std::cout << "------------------------------>> 3" << std::endl;
        ArrayDyn<double, 100> a1{};
        for (size_t i = 0; i < 100; i++) {
            a1[i] = i;
        }

        std::cout << a1[0] << std::endl;
        std::cout << a1[1] << std::endl;
        std::cout << a1[2] << std::endl;

        ArrayDyn<double, 100> a2{};
        a2 = a1;

        std::cout << a2[3] << std::endl;
        std::cout << a2[4] << std::endl;
        std::cout << a2[5] << std::endl;

        ArrayDyn<double, 100> a3{a2};

        std::cout << a3[6] << std::endl;
        std::cout << a3[7] << std::endl;
        std::cout << a3[8] << std::endl;

        ArrayDyn<double, 100> a4{};
        a4 = std::move(a3);

        std::cout << a4[9] << std::endl;
        std::cout << a4[10] << std::endl;
        std::cout << a4[11] << std::endl;

        ArrayDyn<double, 100> a5{std::move(a4)};

        std::cout << a5[12] << std::endl;
        std::cout << a5[13] << std::endl;
        std::cout << a5[14] << std::endl;
    }
}

