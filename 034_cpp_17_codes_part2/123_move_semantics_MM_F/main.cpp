#include <iostream>
#include <vector>
#include <chrono>
#include <algorithm>

class A
{
public:

    A() : data{nullptr}, dim{0}
    {}

    A(const A &obj)
    {
        if (obj.data != nullptr)
        {
            data = new double[obj.dim];
            std::copy(obj.data, obj.data + obj.dim, data);
            dim = obj.dim;
        }
        else
        {
            data = nullptr;
            dim = obj.dim;
        }
    }

    A(A &&obj) noexcept
    {
        delete[] data;
        dim = 0;

        data = obj.data;
        dim = obj.dim;

        obj.data = nullptr;
        obj.dim = 0;
    }

    A &operator=(const A &obj)
    {
        if (this != &obj)
        {
            delete[] data;
            data = new double[obj.dim];
            std::copy(obj.data, obj.data + obj.dim, data);
            dim = obj.dim;
        }
        return *this;
    }

    A &operator=(A &&obj) noexcept
    {
        if (this != &obj)
        {
            delete[] data;
            data = obj.data;
            dim = obj.dim;

            obj.data = nullptr;
            obj.dim = 0;
        }

        return *this;
    }

    virtual ~A()
    {
        delete[] data;
        data = nullptr;
        dim = 0;
    }

    double *data = nullptr;
    size_t dim = 0;
};

class B
{
public:

    B() : data{nullptr}, dim{0}
    {}

    B(const B &obj)
    {
        if (obj.data != nullptr)
        {
            data = new double[obj.dim];
            std::copy(obj.data, obj.data + obj.dim, data);
            dim = obj.dim;
        }
        else
        {
            data = nullptr;
            dim = obj.dim;
        }
    }

    B &operator=(const B &obj)
    {
        if (this != &obj)
        {
            delete[] data;
            data = new double[obj.dim];
            std::copy(obj.data, obj.data + obj.dim, data);
            dim = obj.dim;
        }
        return *this;
    }

    virtual ~B()
    {
        delete[] data;
        data = nullptr;
        dim = 0;
    }

    double *data = nullptr;
    size_t dim = 0;
};

int main()
{
    constexpr size_t NUM_ELEMS = 10000;
    constexpr size_t DIM = 5000;
    std::vector<A> v1;
    std::vector<B> v2;

    for (size_t i = 0; i < NUM_ELEMS; i++)
    {
        A tmpA;
        B tmpB;
        tmpA.dim = DIM + i;
        tmpA.data = new double[tmpA.dim];
        tmpB.dim = DIM + i;
        tmpB.data = new double[tmpB.dim];

        for (size_t j = 0; j < tmpA.dim; j++)
        {
            tmpA.data[j] = (double) j + (double) i;
        }

        for (size_t j = 0; j < tmpB.dim; j++)
        {
            tmpB.data[j] = (double) j + (double) i;
        }

        v1.push_back(std::move(tmpA));
        v2.push_back(tmpB);
    }

    auto start = std::chrono::steady_clock::now();
    std::sort(v1.begin(), v1.end(), [](const A &x, const A &y)
              {
                  return x.data[0] > y.data[0];
              }
    );
    auto end = std::chrono::steady_clock::now();
    std::chrono::duration<double> elapsed_seconds = end - start;
    std::cout << "elapsed time: " << elapsed_seconds.count() << std::endl;
    std::cout << v1[0].data[0] << std::endl;

    start = std::chrono::steady_clock::now();
    std::sort(v2.begin(), v2.end(), [](const B &x, const B &y)
              {
                  return x.data[0] < y.data[0];
              }
    );
    end = std::chrono::steady_clock::now();
    elapsed_seconds = end - start;
    std::cout << "elapsed time: " << elapsed_seconds.count() << std::endl;
    std::cout << v2[0].data[0] << std::endl;
}
