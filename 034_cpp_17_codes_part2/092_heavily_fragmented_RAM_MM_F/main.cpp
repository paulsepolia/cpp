#include <iostream>
#include <vector>
#include <chrono>
#include <set>
#include <unordered_set>
#include <algorithm>
#include <thread>
#include <mutex>

class benchmark_timer {
public:

    explicit benchmark_timer(const double &res) :
            _res(res),
            t1{std::chrono::steady_clock::now()},
            t2{std::chrono::steady_clock::now()},
            t_prev{std::chrono::steady_clock::now()} {};

    auto print_time() const -> void {
        const auto t_loc{std::chrono::steady_clock::now()};
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t_loc - t_prev).count()};
        t_prev = t_loc;
        std::cout << " --> time used since last reset (secs) = " << total_time << std::endl;
    }

    auto reset_timer() -> void {
        const auto t_loc{std::chrono::steady_clock::now()};
        t_prev = t_loc;
        t1 = t_loc;
        t2 = t_loc;
    }

    ~benchmark_timer() {
        t2 = std::chrono::steady_clock::now();
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count()};
        std::cout << " --> res = " << _res << std::endl;
        std::cout << " --> total time used since last reset (secs) = " << total_time << std::endl;
    }

    auto set_res(const double &res) -> void {
        _res = res;
    }

private:

    double _res{};
    decltype(std::chrono::steady_clock::now()) t1{};
    decltype(std::chrono::steady_clock::now()) t2{};
    mutable decltype(std::chrono::steady_clock::now()) t_prev{};
};

const auto DIM{(size_t) 30'000'000};
const auto REC{(size_t) 8};
const auto NUM_THREADS{(size_t) 8};
auto mtx{std::mutex()};

int main() {

    {
        std::cout << "------------------------------>> 1 -->> unordered_set" << std::endl;

        auto la = [=]() {

            auto ot{benchmark_timer(0.0)};

            auto cont{std::unordered_set<void *>()};

            for (size_t i = 0; i < DIM; i++) {
                void *ptr{malloc(REC)};
                cont.emplace(ptr);
            }

            void *ptr{malloc(REC)};
            const auto it{cont.find(ptr)};

            std::lock_guard<std::mutex> lck(mtx);
            std::cout << " --> out --> " << &it << " --> " << std::this_thread::get_id() << std::endl;
            ot.print_time();
            ot.reset_timer();
        };

        auto th_vec{std::vector<std::thread>{}};

        for (size_t i = 0; i < NUM_THREADS; i++) {
            std::cout << "------------------>> starting thread execution --> " << i << std::endl;
            th_vec.emplace_back(la);
        }

        std::cout << "------------------>> joining the threads now..." << std::endl;

        for (auto &el: th_vec) {
            static auto i{int(0)};
            el.join();
            std::cout << "------------------>> joined thread = " << i++ << std::endl;
        }
    }
}
