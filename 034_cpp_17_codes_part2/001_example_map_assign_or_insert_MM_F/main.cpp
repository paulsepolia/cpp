#include <iostream>
#include <map>

int main() {

    std::map<std::string, int> map_users_age{{"Alex", 45},
                                             {"John", 25}};

    std::map map_copy{map_users_age};

    if (auto[iter, was_added] = map_copy.insert_or_assign("John", 26); was_added) {
        std::cout << iter->first << " reassigned..." << std::endl;
    }

    if (auto[iter, was_added] = map_copy.insert_or_assign("Nick", 27); was_added) {
        std::cout << iter->first << " inserted..." << std::endl;
    }

    for (const auto&[key, value] : map_copy) {
        std::cout << key << ", " << value << std::endl;
    }
}

// The above example uses the following features:

// Line 9: Template Argument Deduction for Class Templates -
// mapCopy type is deduced from the type of mapUsersAge.
// No need to declare std::map<std::string, int> map_copy{...}

// Line 11: New inserting method for maps - insert_or_assign

// Line 11: Structured Bindings - captures a returned pair from
// insert_or_assign into separate names.

// Line 11: init if statement - iter and wasAdded are visible
// only in the scope of the surrounding if statement.

// Line 19: Structured Bindings inside a range-based for loop -
// we can iterate using key and value rather than pair.first and pair.second.