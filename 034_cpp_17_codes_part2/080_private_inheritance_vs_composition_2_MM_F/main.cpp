#include <iostream>

class A1 {
};

class A2 {
};

class A3 {
    double x;
    A1 a1;
    A2 a2;
};

class A4 : private A1, A2 {
    double x;
};

auto main() -> int {

    std::cout << "--> sizeof(A3) = " << sizeof(A3) << std::endl;
    std::cout << "--> sizeof(A4) = " << sizeof(A4) << std::endl;
}

