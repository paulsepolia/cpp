#include <chrono>
#include <iomanip>
#include <iostream>
#include <vector>
#include <numeric>
#include <fstream>
#include <algorithm>
#include <cmath>

class benchmark_timer {
public:

    explicit benchmark_timer(const double &res) :
            _res(res),
            t1{std::chrono::steady_clock::now()},
            t2{std::chrono::steady_clock::now()},
            t_prev{std::chrono::steady_clock::now()} {};

    auto print_time() const -> void {
        const auto t_loc{std::chrono::steady_clock::now()};
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t_loc - t_prev).count()};
        t_prev = t_loc;
        std::cout << " --> time used since last reset (secs) = " << total_time << std::endl;
    }

    auto reset_timer() -> void {
        const auto t_loc{std::chrono::steady_clock::now()};
        t_prev = t_loc;
        t1 = t_loc;
        t2 = t_loc;
    }

    ~benchmark_timer() {
        t2 = std::chrono::steady_clock::now();
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count()};
        std::cout << " --> res = " << _res << std::endl;
        std::cout << " --> total time used since last reset (secs) = " << total_time << std::endl;
    }

    auto set_res(const double &res) -> void {
        _res = res;
    }

private:

    double _res{};
    decltype(std::chrono::steady_clock::now()) t1{};
    decltype(std::chrono::steady_clock::now()) t2{};
    mutable decltype(std::chrono::steady_clock::now()) t_prev{};
};

const auto DIM_MAX{(size_t) (std::pow(10.0, 8.0))};
const auto SAMPLE_DIM{(size_t) (std::pow(10.0, 2.0))};

template<typename T>
void printRatio() {

    std::cout << "  precision: " << T::num << "/" << T::den << " second " << std::endl;

    typedef typename std::ratio_multiply<T, std::kilo>::type MillSec;
    typedef typename std::ratio_multiply<T, std::mega>::type MicroSec;
    typedef typename std::ratio_multiply<T, std::giga>::type NanoSec;

    std::cout << std::fixed;
    std::cout << "             " << static_cast<double>(MillSec::num) / MillSec::den << " milliseconds " << std::endl;
    std::cout << "             " << static_cast<double>(MicroSec::num) / MicroSec::den << " microseconds " << std::endl;
    std::cout << "             " << static_cast<double>(NanoSec::num) / NanoSec::den << " nanoseconds " << std::endl;
}

auto main() -> int {

    std::cout << std::boolalpha << std::endl;

    std::cout << "std::chrono::system_clock: " << std::endl;
    std::cout << "  is steady: " << std::chrono::system_clock::is_steady << std::endl;

    printRatio<std::chrono::system_clock::period>();

    std::cout << std::endl;

    std::cout << "std::chrono::steady_clock: " << std::endl;
    std::cout << "  is steady: " << std::chrono::steady_clock::is_steady << std::endl;

    printRatio<std::chrono::steady_clock::period>();

    std::cout << std::endl;

    std::cout << "std::chrono::high_resolution_clock: " << std::endl;
    std::cout << "  is steady: " << std::chrono::high_resolution_clock::is_steady << std::endl;

    printRatio<std::chrono::high_resolution_clock::period>();

    std::cout << std::endl;

    auto results{std::vector<int64_t>(DIM_MAX)};

    auto i{(size_t) 0};
    auto dt{(int64_t) 0};

    while (i < DIM_MAX) {

        const auto start{std::chrono::steady_clock::now()};
        const auto end{std::chrono::steady_clock::now()};

        dt = (std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count());
        if (0 < dt) {
            results[i] = dt;
            ++i;
        }
    }

    std::cout << "Sampling std::chrono::steady_clock as fast as possible"
              << " to measure the resolution (first 100 results) [nanoseconds]:"
              << std::endl;

    for (size_t k = 0; k < SAMPLE_DIM; ++k) {
        std::cout << results[k] << ',';
    }

    std::cout << std::endl << std::endl;

//    auto my_file{std::ofstream{}};
//    my_file.open("Std_Steady_Clock.csv");
//
//    for (const auto &el: results) {
//        my_file << el << ',';
//    }
//
//    my_file.flush();
//    my_file.close();

    const auto sum{(double) std::accumulate(results.begin(), results.end(), 0.0)};

    const auto mean{(double) sum / results.size()};

    auto diff{std::vector<double>(results.size())};

    std::transform(results.begin(), results.end(), diff.begin(), [mean](double x) { return x - mean; });

    const auto sq_sum{(double) std::inner_product(diff.begin(), diff.end(), diff.begin(), 0.0)};

    const auto stdev{(double) std::sqrt(sq_sum / results.size())};

    std::cout << "Mean of " << results.size() << " samples:" << std::endl;
    std::cout << mean << std::endl << std::endl;

    std::cout << "Standard deviation:" << std::endl;
    std::cout << stdev << std::endl << std::endl;

    std::cout << "Median:" << std::endl;
    std::sort(results.begin(), results.end());

    const auto median{(int64_t) results[results.size() / 2]};

    std::cout << median << std::endl << std::endl;

    std::cout << "Max:" << std::endl;
    std::cout << *std::max_element(results.begin(), results.end()) << std::endl << std::endl;

    std::cout << "Min:" << std::endl;
    std::cout << *std::min_element(results.begin(), results.end()) << std::endl << std::endl;
}
