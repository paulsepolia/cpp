#include <iostream>
#include <thread>
#include <cassert>
#include <windows.h>


class ProcessorLimiter {
public:
    explicit ProcessorLimiter(uint32_t numberOfRequestedProcessors) {
        auto numberOfAvailableProcessors = GetNumberOfAvailableProcessorsInternal(_processAffinityMaskOriginal);

        if (numberOfAvailableProcessors == numberOfRequestedProcessors) {
            return;
        }

        if (numberOfAvailableProcessors < numberOfRequestedProcessors) {
            std::cout << "--> error --> 1" << std::endl;
            exit(-1);
        }

        // Create the processor mask
        DWORD_PTR newProcessorMask = 0;

        for (uint32_t i = 0; i < numberOfRequestedProcessors; ++i) {
            newProcessorMask |= 1ull << i;
        }

        // Set to first processor
        if (SetProcessAffinityMask(GetCurrentProcess(), newProcessorMask) == 0) {
            std::cout << "--> error --> 2" << std::endl;
            exit(-1);
        }
    }

    ~ProcessorLimiter() {
        // Reset to original affinity settings
        auto returnValue = SetProcessAffinityMask(GetCurrentProcess(), _processAffinityMaskOriginal);
        assert(returnValue != 0);
        (void) returnValue; // [[maybe_unused]]
    }

    // Return the number of available processors for this process
    static uint32_t GetNumberOfAvailableProcessors() {
        DWORD_PTR processAffinityMask = 0;
        return GetNumberOfAvailableProcessorsInternal(processAffinityMask);
    }

    DWORD_PTR _processAffinityMaskOriginal = 0;

    static uint32_t GetNumberOfAvailableProcessorsInternal(DWORD_PTR &processAffinityMask) {

        DWORD_PTR systemAffinityMask = 0;

        if (GetProcessAffinityMask(GetCurrentProcess(), &processAffinityMask, &systemAffinityMask) == 0) {
            std::cout << "--> error --> 3" << std::endl;
            exit(-1);
        }

        // Determine the number of processors
        uint32_t numberOfAvailableProcessors = 0;
        for (uint32_t shiftIndex = 0; shiftIndex < 64; ++shiftIndex) {
            if ((processAffinityMask >> shiftIndex) & 1ull) {
                ++numberOfAvailableProcessors;
            } else {
                break;
            }
        }
        return numberOfAvailableProcessors;
    }
};

auto main() -> int {

    {
        std::cout << " ----------------------->> example --> 1" << std::endl;
        std::cout << " pgg --> 1 --> std::thread::hardware_concurrency() = "
                  << std::thread::hardware_concurrency() << std::endl;

        SYSTEM_INFO sysinfo;
        GetSystemInfo(&sysinfo);;
        std::cout << " pgg --> 2 --> sysinfo.dwNumberOfProcessors = "
                  << sysinfo.dwNumberOfProcessors << std::endl;
    }

    {
        std::cout << " ----------------------->> example --> 2" << std::endl;

        auto ob1{ProcessorLimiter(2)};

        std::cout << " pgg --> 0 --> ob1.GetNumberOfAvailableProcessors() = "
                  << ob1.GetNumberOfAvailableProcessors() << std::endl;

        std::cout << " pgg --> 2 --> std::thread::hardware_concurrency() = "
                  << std::thread::hardware_concurrency() << std::endl;

        SYSTEM_INFO sysinfo;
        GetSystemInfo(&sysinfo);;
        std::cout << " pgg --> 3 --> sysinfo.dwNumberOfProcessors = "
                  << sysinfo.dwNumberOfProcessors << std::endl;
    }

    {
        std::cout << " ----------------------->> example --> 3" << std::endl;

        auto ob1{ProcessorLimiter(8)};

        std::cout << " pgg --> 0 --> ob1.GetNumberOfAvailableProcessors() = "
                  << ob1.GetNumberOfAvailableProcessors() << std::endl;

        std::cout << " pgg --> 2 --> std::thread::hardware_concurrency() = "
                  << std::thread::hardware_concurrency() << std::endl;

        SYSTEM_INFO sysinfo;
        GetSystemInfo(&sysinfo);;
        std::cout << " pgg --> 3 --> sysinfo.dwNumberOfProcessors = "
                  << sysinfo.dwNumberOfProcessors << std::endl;
    }

    {
        std::cout << " ----------------------->> example --> 4" << std::endl;

        auto ob1{ProcessorLimiter(4)};

        std::cout << " pgg --> 0 --> ob1.GetNumberOfAvailableProcessors() = "
                  << ob1.GetNumberOfAvailableProcessors() << std::endl;

        std::cout << " pgg --> 2 --> std::thread::hardware_concurrency() = "
                  << std::thread::hardware_concurrency() << std::endl;

        SYSTEM_INFO sysinfo;
        GetSystemInfo(&sysinfo);;
        std::cout << " pgg --> 3 --> sysinfo.dwNumberOfProcessors = "
                  << sysinfo.dwNumberOfProcessors << std::endl;
    }

    {
        std::cout << " ----------------------->> example --> 4" << std::endl;


        std::cout << " pgg --> 0 --> ProcessorLimiter::GetNumberOfAvailableProcessors() = "
                  << ProcessorLimiter::GetNumberOfAvailableProcessors() << std::endl;

        std::cout << " pgg --> 2 --> std::thread::hardware_concurrency() = "
                  << std::thread::hardware_concurrency() << std::endl;

        SYSTEM_INFO sysinfo;
        GetSystemInfo(&sysinfo);;
        std::cout << " pgg --> 3 --> sysinfo.dwNumberOfProcessors = "
                  << sysinfo.dwNumberOfProcessors << std::endl;
    }

    {
        std::cout << " ----------------------->> example --> 5" << std::endl;

        std::cout << " pgg --> 0 --> ProcessorLimiter::GetNumberOfAvailableProcessors() = "
                  << ProcessorLimiter::GetNumberOfAvailableProcessors() << std::endl;

        std::cout << " pgg --> 2 --> std::thread::hardware_concurrency() = "
                  << std::thread::hardware_concurrency() << std::endl;

        SYSTEM_INFO sysinfo;
        GetSystemInfo(&sysinfo);;
        std::cout << " pgg --> 3 --> sysinfo.dwNumberOfProcessors = "
                  << sysinfo.dwNumberOfProcessors << std::endl;

        auto ob1{ProcessorLimiter(4)};

        std::cout << " pgg --> 4 --> ob1.GetNumberOfAvailableProcessors() = "
                  << ob1.GetNumberOfAvailableProcessors() << std::endl;

        std::cout << " pgg --> 6 --> std::thread::hardware_concurrency() = "
                  << std::thread::hardware_concurrency() << std::endl;

        GetSystemInfo(&sysinfo);;
        std::cout << " pgg --> 7 --> sysinfo.dwNumberOfProcessors = "
                  << sysinfo.dwNumberOfProcessors << std::endl;
    }
}