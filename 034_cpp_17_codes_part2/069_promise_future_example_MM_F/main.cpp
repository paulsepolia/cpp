#include <iostream>
#include <future>

auto set_promise_value(std::promise<double> &prom, const double val) -> void {

    std::cout << "I am the SET_PROMISE_THREAD!" << std::endl;
    std::cout << "I am about to set a value to my promise but I am sleeping right now..." << std::endl;
    std::this_thread::sleep_for(std::chrono::milliseconds(10000));
    std::cout << "I am setting a value to my promise right now!" << std::endl;
    prom.set_value(val);
    std::cout << "PROMISE GOT A VALUE!!" << std::endl;
}

auto get_future_value(std::future<double> &fut, double &val) -> double {

    std::cout << "I am waiting for  my future to get a value..." << std::endl;

    return val = fut.get();
}

auto main() -> int {

    auto prom{std::promise<double>{}};
    auto fut{std::future<double>{prom.get_future()}};
    auto val{(double) -1.0};

    std::thread th1(get_future_value, std::ref(fut), std::ref(val));

    std::this_thread::sleep_for(std::chrono::seconds(2));

    std::thread th2(set_promise_value, std::ref(prom), 100);

    th1.join();
    th2.join();

    std::cout << "--> val = " << val << std::endl;
}
