// dead lock

#include <iostream>
#include <thread>
#include <mutex>
#include <vector>

auto mtx1{std::mutex{}};
auto mtx2{std::mutex{}};

auto print_id1(uint32_t id) -> void {

    mtx1.lock();

    std::cout << " --> before sleep --> after the lock --> mtx1 --> id = " << id << std::endl;

    std::this_thread::sleep_for(std::chrono::seconds(10));

    std::cout << " --> after sleep --> after the lock --> mtx1 --> id = " << id << std::endl;

    mtx2.lock();

    mtx2.unlock();
    mtx1.unlock();
}

auto print_id2(uint32_t id) -> void {

    mtx2.lock();

    std::cout << " --> before sleep --> after the lock --> mtx2 --> id = " << id << std::endl;

    std::this_thread::sleep_for(std::chrono::seconds(10));

    std::cout << " --> after sleep --> after the lock --> mtx2 --> id = " << id << std::endl;

    mtx1.lock();

    mtx1.unlock();
    mtx2.unlock();
}

auto main() -> int {

    auto th1{std::thread(print_id1, 1)};
    auto th2{std::thread(print_id2, 2)};

    th1.join();
    th2.join();
}