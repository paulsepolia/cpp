#include <chrono>
#include <iomanip>
#include <iostream>
#include <vector>
#include <numeric>
#include <fstream>
#include <algorithm>
#include <cmath>
#include <limits>

class benchmark_timer {
public:

    explicit benchmark_timer(const double &res) :
            _res(res),
            t1{std::chrono::steady_clock::now()},
            t2{std::chrono::steady_clock::now()},
            t_prev{std::chrono::steady_clock::now()} {};

    auto print_time() const -> void {
        const auto t_loc{std::chrono::steady_clock::now()};
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t_loc - t_prev).count()};
        t_prev = t_loc;
        std::cout << " --> time used since last reset (secs) = " << total_time << std::endl;
    }

    auto reset_timer() -> void {
        const auto t_loc{std::chrono::steady_clock::now()};
        t_prev = t_loc;
        t1 = t_loc;
        t2 = t_loc;
    }

    ~benchmark_timer() {
        t2 = std::chrono::steady_clock::now();
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count()};
        std::cout << " --> res = " << _res << std::endl;
        std::cout << " --> total time used since last reset (secs) = " << total_time << std::endl;
    }

    auto set_res(const double &res) -> void {
        _res = res;
    }

private:

    double _res{};
    decltype(std::chrono::steady_clock::now()) t1{};
    decltype(std::chrono::steady_clock::now()) t2{};
    mutable decltype(std::chrono::steady_clock::now()) t_prev{};
};

const auto DO_MAX{(size_t) (std::pow(10.0, 10.0))};

template<typename T>
void print_ratio() {

    std::cout << "  precision: " << T::num << "/" << T::den << " seconds" << std::endl;

    using MillSec = typename std::ratio_multiply<T, std::kilo>::type;
    using MicroSec = typename std::ratio_multiply<T, std::mega>::type;
    using NanoSec = typename std::ratio_multiply<T, std::giga>::type;

    std::cout << std::fixed;
    std::cout << "             " << static_cast<double>(MillSec::num) / MillSec::den << " milliseconds" << std::endl;
    std::cout << "             " << static_cast<double>(MicroSec::num) / MicroSec::den << " microseconds"  << std::endl;
    std::cout << "             " << static_cast<double>(NanoSec::num) / NanoSec::den << " nanoseconds" << std::endl;
}

auto main() -> int {

    {
        std::cout << "------------------------------------------------------->> 1" << std::endl;

        std::cout << std::boolalpha << std::endl;

        std::cout << " --> std::chrono::system_clock: " << std::endl;
        std::cout << " --> is steady: " << std::chrono::system_clock::is_steady << std::endl;

        print_ratio<std::chrono::system_clock::period>();

        std::cout << std::endl;

        std::cout << " --> std::chrono::steady_clock: " << std::endl;
        std::cout << " --> is steady: " << std::chrono::steady_clock::is_steady << std::endl;

        print_ratio<std::chrono::steady_clock::period>();

        std::cout << std::endl;

        std::cout << " --> std::chrono::high_resolution_clock: " << std::endl;
        std::cout << " --> is steady: " << std::chrono::high_resolution_clock::is_steady << std::endl;

        print_ratio<std::chrono::high_resolution_clock::period>();

        std::cout << std::endl;
    }

    {
        std::cout << "------------------------------------------------------->> 2" << std::endl;

        auto i{(size_t) 1};
        auto dt{(int64_t) 0};
        auto dt_min{(int64_t) std::numeric_limits<int64_t>::max()};
        auto dt_max{(int64_t) std::numeric_limits<int64_t>::min()};

        auto idx_max{(size_t) 0};
        auto idx_min{(size_t) 0};

        while (i <= DO_MAX) {

            const auto start{std::chrono::steady_clock::now()};
            const auto end{std::chrono::steady_clock::now()};

            dt = (std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count());

            if(dt < dt_min) {
                dt_min = dt;
                idx_min = i;
            }

            if(dt > dt_max) {
                dt_max = dt;
                idx_max = i;
            }

            ++i;
        }

        std::cout << " --> the minimum delta detected after many runs = " << dt_min << ", at position: " << idx_min << std::endl;
        std::cout << " --> the maximum delta detected after many runs = " << dt_max << ", at position: " << idx_max << std::endl;
    }
}
