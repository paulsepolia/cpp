#!/bin/bash

  clang++    -c           \
             -O3          \
             -Wall        \
             -std=gnu++2a \
             -I/mnt/xdata/opt/boost/1.70.0/include/ \
             main.cpp

  clang++    -pthread \
             /mnt/xdata/opt/boost/1.70.0/lib/*.so \
             main.o \
             -o x_clang

  rm *.o

