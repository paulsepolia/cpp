#include <iostream>

struct S {
    int n{-1};
    std::string s{"here i am"};
    double d{1.2345};
};

template<std::size_t I>
const auto &get(const S &s) {
    if constexpr (I == 0) {
        return s.n;
    } else if constexpr (I == 1) {
        return s.s;
    } else if constexpr (I == 2) {
        return s.d;
    }
}

auto main() -> int {

    const auto s{S()};

    std::cout << get<0>(s) << std::endl;
    std::cout << get<1>(s) << std::endl;
    std::cout << get<2>(s) << std::endl;
}