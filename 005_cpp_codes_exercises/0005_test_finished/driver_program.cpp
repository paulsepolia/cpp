
//===============//
// test --> 0005 //
//===============//

// Status --> FINISHED

//==============================================================================
//
// --> the main points are:
//
// --> 1 --> reinterpret_cast<void*> of a pointer of a class A is not equal
//           to the reinterpret_cast<void*> of a pointer of a class B
//           even if the pointer points to the same derived c object
//
// --> 2 --> dynamic_cast<void*> of a pointer of a class A is EQUAL
//           to the dynamic_cast<void*> of a pointer of a class B
//           WHEN the pointer points to the same derived c object
//==============================================================================

#include <iostream>

using std::cin;
using std::cout;
using std::endl;

// class --> A

class A {
public:

    // constructor

    A() : m_i(0)
    {
        cout << " --> constructor --> A" << endl;
    }

    // destructor

    virtual ~A()
    {
        cout << " --> destructor --> ~A" << endl;
    }

protected:

    int m_i;
};

// class --> B

class B {
public:

    // constructor

    B() : m_d(0.0)
    {
        cout << " --> constructor --> B" << endl;
    }

    // destructor

    virtual ~B()
    {
        cout << " --> destructor --> ~B" << endl;
    }

protected:

    double m_d;
};

// the class --> C

class C : public A, public B {
public:

    // default constructor
    // the order of calling the base(inherited) constructors
    // is given from the statement: public A, public B
    // so it is constructor A, then constructor B
    // and then constructor C
    // GNU C++ compiler warns for that reorder of the initialization process

    C() : B(), A(), m_c('a')
    {
        cout << " --> constructor --> C" << endl;
    }

private:

    char m_c;
};

// the main function

int main()
{
    cout << " --> 1" << endl;
    C c; // calls the constructors A, B, C

    cout << " --> 2" << endl;
    A *pa = &c; // DOES NOT CALL ANYTHING
    // since *pa is a placeholder for the already built
    // object c

    cout << " --> 3" << endl;
    B *pb = &c; // DOES NOT CALL ANYTHING
    // since *pb is a placeholder for the already built
    // object c

    cout << " --> 4" << endl;
    const int x = (pa == &c) ? 1 : 2; // it is true so --> 1

    cout << " --> 5" << endl;
    const int y = (pb == &c) ? 3 : 4; // it is true so --> 3

    cout << " --> 6" << endl; // it is false so --> 6
    const int z = (reinterpret_cast<void*>(pa) == reinterpret_cast<void*>(pb)) ? 5 : 6;

    cout << " --> 7" << endl; // it is true so --> 6
    const int k = (dynamic_cast<void*>(pa) == dynamic_cast<void*>(pb)) ? 5 : 6;

    cout << " --> 8" << endl;
    cout << x << y << z << k << endl; // it is --> 136

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

