
//===============//
// test --> 0022 //
//===============//

// Status --> FINISHED

#include <iostream>
#include <set>
#include <iterator>

using std::cout;
using std::cin;
using std::endl;
using std::set;
using std::ostream_iterator;

// class --> C

struct C {
    // constructor

    C()
    {
        cout << " --> constructor --> C" << endl;
    }

    // destructor

    ~C()
    {
        cout << " --> destructor --> ~C" << endl;
    }

    bool operator()(const int &a, const int &b) const
    {
        return a % 10 < b % 10;
    }
};

// the main function

int main()
{
    {
        // local innermost scope --> starts

        cout << " --> 1" << endl;

        set<int> x({ 4, 2, 7, 11, 12, 14, 17, 2 }); // sorts the elements
        // and keeps only the unique
        // so "2" is taken only once

        cout << " --> 2" << endl;

        cout << x.size() << endl; // size is 7 since the x set contains
        // {2, 4, 7, 11, 12, 14, 17}

        cout << " --> 3" << endl;

        set<int, C> y(x.begin(), x.end()); // the set to compare and order are
        // { 2, 4, 7, 11, 12, 14, 17 }
        // { 2, 4, 7,  1,  2,  4,  7 } ==>
        // { 1, 2, 4, 7} ==>
        // { 11, 2, 4, 7}

        cout << " --> 4" << endl;

        cout << y.size() << endl; // size is 4

        cout << " --> 5" << endl;

        copy(y.begin(), y.end(), ostream_iterator<int>(cout, "\n"));
        // the contents of the set are {11, 2, 4, 7}

        cout << " --> 6" << endl;
        // HERE the destructir is beig called ??

    } // local innermost scope --> ends

    cout << " --> 7 --> exit" << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    cout << " --> 8 --> exit" << endl;

    return 0;
}

//======//
// FINI //
//======//

