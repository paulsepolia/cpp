
//=====================//
// test --> 0010 --> a //
//=====================//

// Status --> FINISHED

//==============================================================================
//
// --> the main points here are:
//
// --> 1 --> if a function is declared virtual in the parent class
//	     then in can be overwritten by a related function from the derived class
//	     and the derived version if prefered always by the elements of type A
//	     of the following forms:
//           A * c = &b;
//	     and
//	     A & d = b;
//
// --> 2 --> if a function is NOT declared virtual is the parent class
//	     but still exits an equivalent version of it in the derived class
//	     then for B type objects the derived type is used,
//	     for A type objects the parent type is used
//	     BUT for A type object of the forms
//	     A * c = &b;
//	     and
//	     A & d = b;
//	     the parent type function is used
//	     since the c and d are treated as A type object placeholders
//
// --> 3 --> const qualifier makes a different version
//
//==============================================================================

#include <iostream>

using std::cout;
using std::cin;
using std::endl;

// class --> A

class A {
public:

    // constructor

    A()
    {
        cout << " --> constructor --> A" << endl;
    }

    // function --> f --> A --> 1

    virtual void f(int n)
    {
        cout << " --> function --> A --> 1" << endl;
        cout << n << 1 << endl;
    }

    // destructor

    virtual ~A()
    {
        cout << " --> destructor --> ~A" << endl;
    }

    // function --> f --> A --> 2

    void f(int n) const
    {
        cout << " --> function --> A --> 2" << endl;
        cout << n << endl;
    }
};

// class --> B

class B : public A {
public:

    // constructor

    B() : A()
    {
        cout << " --> constructor --> B" << endl;
    }

    // function --> f --> B --> 1

    void f(int n)
    {
        cout << " --> function --> B --> 1" << endl;
        cout << (n << 1) << endl;
    }

    // function --> f --> B --> 2

    void f(int n) const
    {
        cout << " --> function --> B --> 2" << endl;
        cout << n + 1;
    }

    // destructor --> ~B

    virtual ~B()
    {
        cout << " --> destructor --> ~B" << endl;
    }
};

// the main function

int main()
{
    {
        // local innermost scope --> starts

        cout << " --> 1" << endl;

        const A a; // creates the object a of type A
        // to do so it calls the constructor A

        cout << " --> 2" << endl;

        B b; // creates the object b of type B
        // to do so it
        // calls the constructor A and then
        // calls the constructor B

        cout << " --> 3" << endl;

        A & c = b; // DOES NOT CALL ANYTHING
        // since does not create anything
        // c is another name for the object b
        // but treats c as of type A

        cout << " --> 4" << endl;

        const A * d = &b; // DOES NOT CALL ANYTHING
        // since does not create anything
        // d is a type A pointer to object b of type B
        // but treats it always of type A
        // so it calls the non-virtual functions of class A
        // and for the virtual declared functions
        // that of class B

        cout << " --> 5" << endl;

        a.f(2); // calls the f --> A --> 2
        // because of the const qualifier
        // in both places of function f and objects a declaration

        cout << " --> 6" << endl;

        b.f(2); // calls the f --> B --> 1
        // because of the type matching
        // f is not const qualified and b is not const qualified
        // the (n<<1) is (2<<1) so the value 2 is bit-shifted left
        // by 1 position
        // the results is 4

        cout << "(2<<1) = " << (2<<1) << endl;

        cout << " --> 7" << endl;

        c.f(1); // calls the f --> B --> 1,
        // because the related function f of A class is virtual


        cout << " --> 8" << endl;

        d->f(1); // calls the f --> A --> 2
        // because of the const qualifier
        // and because function A --> 2 is not declared virtual

        cout << endl;

    } // local innermost scope --> ends

    cout << " --> 9 --> exit" << endl;

    // At the exit of the local innermost scope
    // 2 object are being destroyed, a and b
    // The rest objects, c and d, are placeholders of the c
    // So, the destructor A is being called twice for a and b
    // and the destructor B is being called once for b

    // sentineling

    int sentinel;
    cin >> sentinel;

    cout << " --> 10 --> exit" << endl;

    return 0;
}

//======//
// FINI //
//======//

