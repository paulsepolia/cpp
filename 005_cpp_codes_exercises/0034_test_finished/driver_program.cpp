
//===============//
// test --> 0034 //
//===============//

// Status --> FINISHED

#include <algorithm>
#include <iostream>
#include <vector>

using std::adjacent_find;
using std::sort;
using std::cout;
using std::cin;
using std::endl;
using std::vector;

// function --> compare

bool compare(int a, int b)
{
    cout << " --> function --> compare" << endl;

    return a % 3 < b % 3;
}

// the main function

int main()
{
    {
        // local innermost scope --> start

        cout << " --> 1" << endl;

        typedef vector<int> V;

        cout << " --> 2" << endl;

        V v({7, 0, 8, 5, 2, 11});

        cout << " --> 3" << endl;

        sort(v.begin(), v.end()); // v = {0,2,5,7,8,11}

        cout << " --> 4" << endl;

        typedef V::const_reverse_iterator R;

        cout << " --> 5" << endl;

        const R i = adjacent_find(v.rbegin(), v.rend(), compare);
        //	{2, 2, 1, 2, 2, 0 } reversed and module 3
        //      {0, 2, 5, 7, 8, 11}
        //            i+1 i

        cout << " --> 6" << endl;

        if (i == v.rend()) { // i is not v.rend()
            cout << " --> 7" << endl;

            cout << -1 << endl;

            cout << " --> 8" << endl;
        } else {

            cout << " --> 9" << endl;

            cout << *i << *(i + 1) << endl;

            // so *i = 7 and *(i+1) = 5


            cout << " --> 10" << endl;

        }

        cout << " --> 11" << endl;

    } // local innermost scope --> ends

    cout << " --> 12 --> exit" << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    cout << " --> 13 --> exit" << endl;

    return 0;
}

//======//
// FINI //
//======//

