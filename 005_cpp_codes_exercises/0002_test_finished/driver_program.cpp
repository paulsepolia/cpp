
//===============//
// test --> 0002 //
//===============//

// Status --> FINISHED

//==============================================================================
// The main points are the following
//
// 1 --> A e = b; --> calls the copy constructor and creates the object e
//
// 2 --> A c(a); --> calls the copy constructor and creates the object c
//
// 3 --> c = a; --> calls the assignment operator provided by the system or
//		    the developer
//
// 4 --> const A &d = c; --> does not create any object
//
// 5 --> A * p = new A(c); --> creates an object of type A to which p points to
//                         --> to do so calls the copy constructor
//
// 6 --> delete p; --> deletes the object to which p points
//		       to do so calls the destructor
//
// 7 --> void f(const A & a1, const A & a2 = A());
//	 f(3);  --> calls the constructor twice and the destructor twice
// 		    at the exit of the function
//	            the conversion of 3 to an object of type A is implicit, since
//		    the constructor is not declared as 'expicit'
//
//==============================================================================

#include <iostream>

using std::cout;
using std::endl;
using std::cin;

// class --> A

class A {
public:

    // default constructor

    A(int n = 0) : m_n(n)
    {
        cout << " --> constructor --> A" << endl;
    }

    // copy constructor

    A(const A & a) : m_n (a.m_n)
    {
        cout << " --> copy constructor --> A" << endl;
    }

    // destructor

    virtual ~A()
    {
        cout << " --> destructor --> ~A" << endl;
    }

    // assignment operator

    A & operator = (const A & a)
    {
        cout << " --> operator --> =" << endl;
        this->m_n = a.m_n;
        return *this;
    }

private:

    int m_n;
};

// function --> f

void f(const A & a1, const A & a2 = A())
{
    cout << " --> function --> f" << endl;
}

int main()
{
    {
        // local innermost scope --> starts

        cout << " --> 1" << endl;

        A a(2); // creates the object a
        // so calls the constructor A

        cout << " --> 2" << endl;

        A b; // creates the object b
        // so calls the constructor A

        cout << " --> 3" << endl;

        const A c(a); // creates the constant object c
        // to do so, calls the copy constructor

        cout << " --> 4" << endl;

        const A &d = c; // does not create any object
        // so does not call any constructor

        cout << " --> 5" << endl;

        const A e = b; // creates the constant object e
        // to do so, calls the copy constructor

        cout << " --> 6" << endl;

        b = d; // does not create any object
        // calls the assignment operator
        // does not call any constructor

        cout << " --> 7" << endl;

        A * p = new A(c); // creates a new object to which p points
        // to do so calls the copy constructor

        cout << " --> 8" << endl;

        A * q = &a; // does not create any new object
        // pointer q points to a

        cout << " --> 9" << endl;

        static_cast<void>(q); // does nothing

        cout << " --> 10" << endl;

        delete p; // deletes the object to which p points
        // the system calls the destructor A

        cout << " --> 11" << endl;

        f(3); // implicit conversion of 3 to object of type A
        // by calling the constructor
        // So, in total the default constructor is being called two times
        // and the destructor two times
        // after the execution of the function

        cout << " --> 12" << endl;

    } // local innermost scope --> ends

    // at the exit of the local innermost scope
    // the destructor A is being called four times for the local objects
    // a, b, c, and e

    cout << " --> 13 --> exit" << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    cout << " --> 14 --> exit" << endl;

    return 0;
}

//======//
// FINI //
//======//

