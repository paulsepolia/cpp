
//===============//
// test --> 0025 //
//===============//

// Status --> FINISHED

#include <iostream>

using std::cout;
using std::cin;
using std::endl;

// class --> A

class A {
public:

    // constructor

    A()
    {
        cout << " --> constructor --> A" << endl;
        f();
    }

    // destructor

    virtual ~A()
    {
        cout << " --> destructor --> ~A" << endl;
    }

public:

    virtual void f() const
    {
        cout << " --> function --> f --> A" << endl;
        cout << 1 << endl;
    }
};

// class --> B

class B : public A {
public:

    // constructor

    B()
    {
        cout << " --> constructor --> B" << endl;
        f();
    }

    // destructor

    ~B()
    {
        cout << " --> destructor --> ~B" << endl;
    }

private:

    virtual void f() const
    {
        cout << " --> function --> f --> B" << endl;
        cout << 2 << endl;
    }
};

// the main function

int main()
{
    {
        // local innermost scope --> starts

        cout << " --> 1" << endl;

        const A * a = new B; // creates an object of type B
        // the rest easy to follow

        cout << " --> 2" << endl;

        delete a;

        cout << " --> 3" << endl;

    } // local innermost scope --> ends

    // Exiting the local innermost scope does not produce any results
    // because the object have been already destroyed in the
    // above statement "delete a"

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

