
//===============//
// test --> 0003 //
//===============//

// Status --> FINISHED

//==============================================================================
// the main points are:
//
// B is a child of class A via public inheritance
//
// 1 --> A a1 = a2; --> use of the copy constructor of the class A
//
// 2 --> B b1 = b2; --> use of the copy constructor of the class B
//
// 3 --> A a1(a2); --> use of the copy constructor of the class A
//
// 4 --> B b1(b2); --> use of the copy constructor of the class B
//
// 5 --> A a1 = b1; --> use of the copy constructor of the class A
//			b1 is treated as of type A, here
//
// 6 --> A a1(b1); --> same as above. use of the copy constructor of the class A
//                     b1 is treated as of type A, here
//
// 7 --> a1 = a2; --> use of the provided assignment operator of class A
//		      the assignment operator can be provided either by the
// 		      compiler or by the developer
//
// 8 --> b1 = b2; --> use of the provided assignment operator of class B
//		      the assignment operator can be provided either by the
//		      compiler or the developer
//
// 9 --> a1 = b1; --> use of the provided assignment operator of class A
//		      since b1 is treated as of type A
//		      the assignment operator can be provided either by
//		      the compiler or by the developer
//
// 10 --> b1 = a1; --> such a statement MAKES SENCE ONLY if the developer
//		       has provided in class A as member function a related
//		       assignment operator overloaded function
//
// 11 --> A * x[2] = {a,b}; --> x[0] == a, x[1] == b; both elements are
//		                treated as of type A, under any operations
//
// 12 --> vector<A> ya = {a,b,b,a}; --> ya[0] == a, ya[1] == b, ya[2] == b, ya[3] == a
//				        All the elements of the ya container are treated
//					as of type A, under any operations
//
// 13 --> vector<B> yb = {b,b}; --> yb[0] == b, yb[1] == b
//				    All the elements of the yb container are treated
//				    as of type B, under any operations
//
//==============================================================================

#include <iostream>
#include <vector>

using std::endl;
using std::cout;
using std::cin;
using std::vector;

// class --> A

class A {
public:

    // constructor

    A(int n = 0) : m_n (n)
    {
        cout << " --> constructor --> A" << endl;
    }

    // copy constructor

    A(const A & a): m_n (a.m_n)
    {
        cout << " --> copy constructor --> A" << endl;
    }

    // operator =

    A & operator = (const A & a)
    {
        cout << " --> A & operator = (const A &) --> A" << endl;
        this->m_n = a.m_n;
        return *this;
    }

    // function --> f

    virtual int f() const
    {
        cout << " --> f --> A" << endl;
        return m_n;
    }

    // destructor

    virtual ~A()
    {
        cout << " --> destructor --> ~A" << endl;
    }

    // function get_m_n

    int get_m_n() const
    {
        return m_n;
    }

protected:

    int m_n;
};

// class --> B
// public inheritance of A

class B : public A {
public:

    // constructor

    B(int n = 0) : A(n)
    {
        cout << " --> constructor --> B" << endl;
    }

    // copy constructor

    B(const B & b) : A(b)
    {
        cout << " --> copy constructor --> B" << endl;
    }

    // operator =

    B & operator = (const B & b)
    {
        cout << " --> B & operator = (const B &) --> B" << endl;
        this->m_n = b.m_n;
        return *this;
    }

    // operator =

    B & operator = (const A & a)
    {
        cout << " --> B & operator = (const A &) --> B" << endl;
        this->m_n = a.get_m_n();
        return *this;
    }

    // function f

    virtual int f() const
    {
        cout << " --> f --> B" << endl;
        return m_n + 1;
    }

    // destructor

    virtual ~B()
    {
        cout << " --> destructor --> ~B" << endl;
    }
};

// the main function

int main()
{
    {
        // local innermost scope --> 1

        cout << " --> 1" << endl;

        const A a(1); // creates object a of type A
        // to do so calls the constructor A

        cout << " --> 2" << endl;

        const B b(3); // creates object b of type B
        // to do so calls the constructor A and then
        // the constructor B

        cout << " --> 3" << endl;

        const A * x[2] = {&a, &b}; // DOES NOT CALL ANYTHING
        // x is a pointer to a type A object
        // a and b objects are already built

        cout << " --> 4" << endl;

        typedef vector<A> VA; // DOES NOT CALL ANYTHING
        // it is a type definition

        cout << " --> 5" << endl;

        typedef vector<B> VB; // DOES NOT CALL ANYTHING
        // it is a type definition

        cout << " --> 6" << endl;

        VA ya({a, b, b, a}); // creates a vector<A> type object
        // initializes it with four values {a,b,b,a}
        // to do so
        // calls eight times the copy constructor A
        // calls fout times the destructor A
        // so the four objects are being passed by value
        // and some tmp objects are also being created
        // and then destroyed by calling the destructor A

        cout << " --> 7" << endl;

        VB yb({b, b}); // creates a vector<B> type object
        // initializes it with two values {b,b}
        // to do so
        // calls 4 times the copy constructor A
        // calls 4 times the copy constructor B
        // calls 2 times the destructor B
        // calls 2 times the destructor A
        // so the two objects are being passed by value
        // two tmp objects are being created
        // and then are being destroyed

        cout << " --> 8" << endl;

        VA::const_iterator ia = ya.begin(); // DOES NOT CALL ANYTHING LOCAL

        cout << " --> 9" << endl;

        VB::const_iterator ib = yb.begin(); // DOES NOT CALL ANYTHING LOCAL

        cout << " --> 10" << endl;

        cout << x[0]->f() << endl; // x[0] is a pointer of type A
        // which points to the object a
        // so the statement x[0]->f() is equivalent to
        // the statement a.f(), so the member function f
        // of the class A is being called,
        // private member variable m_n of a is equal to 1
        // so it outputs the value 1

        cout << " --> 11" << endl;

        cout << x[1]->f() << endl; // x[0] is a pointer of type A
        // which points to the object b of type B
        // b is also of type A, since B class
        // is public inherited from class A
        // x[1]->f() is NOT equivalent to b.f()
        // so the member function f of class A is being called
        // m_n of object b is 3 so it is output of 4

        cout << " --> 12" << endl;

        cout << ia->f() << endl; // ia is an iterator for the container ya of type vector<A>
        // ia points to the first element of ya
        // so it is equivalent to a.f()
        // and the f member function of class A is being called
        // and outputs the value 1

        cout << " --> 13" << endl;

        cout <<	(ia + 1)->f() << endl; // ia is an iterator for the container ya of type vector<A>
        // ia+1 points to the second element of ya, b
        // the second element b is treated as of type A
        // so b here is of type A,
        // so the f member function of class A is being called
        // the value of the private member variable m_n is 3
        // because of B b(3) initialization which calls the
        // the constructor A(3)
        // so the output is 3

        cout << " --> 14" << endl;

        cout << (ia + 2)->f() << endl; // ia is an iterator for the container ya of type vector<A>
        // ia+2 points to the third element of ya, b
        // the third element b is treated as of type A
        // so b here is of type A,
        // so the f member function of class A is being called
        // the values of the private member variable m_n is 3
        // because of B b(3) initialization which calls the
        // the constructor A(3) so the output is 3

        cout << " --> 15" << endl;
        cout << (ia + 3)->f() << endl; // SAME TO COMMENTS 12

        cout << " --> 16" << endl;

        cout << ya[0].f() << endl; // SAME TO COMMENTS 12

        cout << " --> 17" << endl;

        cout << ya[1].f() << endl; // SAME TO COMMENTS 13

        cout << " --> 18" << endl;

        cout << ya[2].f() << endl; // SAME TO COMMENTS 14

        cout << " --> 19" << endl;

        cout << ya[3].f() << endl; // SAME TO COMMENTS 15

        cout << " --> 20" << endl;

        cout << a.f() << endl; // calls the f member function of class A

        cout << " --> 21" << endl;

        cout << b.f() << endl; // calls the f member function of class B

        cout << " --> 22" << endl;

        cout << ib->f() << endl; // ib is an iterator of the container yb of type vector<B>
        // ib points to the first element of yb which is of type B
        // so ib->f() is equivalent to b.f()

        cout << " --> 23" << endl;

        cout << (ib + 1)->f() << endl; // ib+1 is an iterator of the container yb of type vector<B>
        // ib+1 points to the second elemnt of yb which is of type B
        // so (ib+1)->f() is equivalent to b.f()

        // at the exit of this scope
        // 6 objects are being destroyed
        // a, b, ya[0], ya[1], ya[2], ya[3]
        // so the destructor A is called 6 times
        // and the destructor B is called 1 time (object b)

        // also
        // yb[0] and yb[1] are being destroyed
        // so destructor B and then A are being called twice

    } // end of local innermost scope --> 1

    //

    {
        // local innermost scope --> 2

        cout << " --> 24" << endl;

        A a1; // creates the object a1 of type A
        // to do so calls the constructor A
        // the private member variable m_n is set to zero

        cout << " --> 25" << endl;

        A a2(a1); // creates the object a2 of type A
        // to do so DOES NOT CALL the constructor A
        // but instead the copy constructor of the class A

        cout << " --> 26" << endl;

        B b1(12); // creates the object b1 of type B
        // to do so calls the constructor B, which in practice
        // means call what the constructor B wants
        // call of the constructor A, set m_n to 12 and
        // then execute the rest part of the constructor B

        cout << " --> 27" << endl;

        B b2(b1); // creates the object b2 of type B
        // to do so calls the copy constructor of class B
        // which in turn calls the copy constructor of class A
        // with argument b1 and then the rest of the body of the
        // copy constructor of class B

        cout << " --> 28" << endl;

        B b3 = b1; // creates the object b3 of type B
        // to do so calls the copy constructor of class B
        // which in turn calls the copy constructor of class A
        // with argumant b1 and then the rest of the body of the
        // copy constructor of class B, so same actions as of
        // B b3(b1);
        // DOES NOT CALL THE PROVIDED ASSIGNMENT OPERATOR of class B

        cout << " --> 29" << endl;

        A a3(b3); // creates the object a3 of type A
        // to do so, calls the copy constructor of class A
        // treats the object b3 of type B as object of type A
        // since b3 is of type A and of type B
        // DOES NOT CALL ANY OTHER CONSTRUCTOR

        cout << " --> 30" << endl;

        A a4 = b3; // creates the object a4 of type A
        // to do so, calls the copy constructor of class A
        // treats the object b3 of type B as object of type A
        // since b3 is of type A and of type B
        // DOES NOT CALL ANY OTHER CONSTRUCTOR


        cout << " --> 31" << endl;

        a1 = a2; // calls the provided assignment operator of the class A

        cout << " --> 31" << endl;

        b1 = b2; // calls the provided assignment operator of the class B

        cout << " --> 32" << endl;

        a1 = b1; // calls the provided assignment operator of the class A
        // DOES NOT CALL ANYTHING ELSE

        cout << " --> 33" << endl;

        b1 = a1; // calls the provided assignment operator of the class B
        // the appropriate version of it
        // DOES NOT CALL ANYTHING ELSE

        // at the exit of the scope 7 objects are being destroyed
        // so 10 destructors are called
        // a1 calls destructor A,
        // a2 calls destructor A,
        // a3 calls destructor A,
        // a4 calls destructor A,
        // b1 calls destructor B and then destructor A,
        // b2 calls destructor B and then destructor A,
        // b3 calls destructor B and then destructor A

    } // end of local innermost scope --> 2

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

