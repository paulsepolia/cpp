
//===============//
// test --> 0004 //
//===============//

// Status --> FINISHED

//==============================================================================
//
// the main points are:
//
// 1 --> Class C: public A, B --> the order in which parents class A and B
//				  are given in the definition of the class C
//				  defines the order of the constructors are being
//				  called while initiating an object of class type C
//				  So in that case, everytime you create an object
//				  of type class C, the constructor A is being called first
//				  then the constructor B is being called second in order
//				  and then the rest part of the C constructor
//				  The destructors are being called in ~C --> ~B --> ~A order
//				  The constructors are being called in A --> B --> C order
//
// 2 --> C c; A * pa = &c; B * pb = &c;
//
//   --> dynamic_cast<void*>(pa) == dynamic_cast<void*>(pb); THEY ARE EQUAL
//   so the dynamic_cast<void*> value of pointer pa and pb of &c are the equal;
//
//   --> reinterpret_cast<void*>(pa) != reinterpret_cast<void*>(pb); THEY ARE NOT EQUAL
//   so the reinterpet_cast<void*> value of the pointer pa and pf of &c are NOT EQUAL;
//
//==============================================================================

#include <iostream>
#include <typeinfo>

using std::cin;
using std::cout;
using std::endl;

// class --> A

class A {
public:

    // constructor

    A() : m_i(0)
    {
        cout << " --> constructor --> A" << endl;
    }

    // destructor

    virtual ~A()
    {
        cout << " --> destructor --> ~A" << endl;
    }

protected:

    int m_i;
};

// class --> B

class B {
public:

    // constructor

    B() : m_d(0.0)
    {
        cout << " --> constructor --> B" << endl;
    }

    // destructor

    virtual ~B()
    {
        cout << " --> destructor --> ~B" << endl;
    }

protected:

    double m_d;
};

// class --> C

class C : public A, public B {
public:

    // constructor

    // the order of calling the base(inherited) constructors
    // is given from the statement: public A, public B
    // so it is constructor A, then constructor B
    // and then constructor C which are being called
    // GNU C++ compiler warns for that reorder of the initialization process
    // intensionaly I have placed B() first and then A()

    C() : B(), A(), m_c('a')
    {
        cout << " --> constructor --> C" << endl;
    }

    // destructor

    virtual ~C()
    {
        cout << " --> destructor --> ~C" << endl;
    }

private:

    char m_c;
};

// the main function

int main()
{
    {
        // local innermost scope --> starts

        cout << " --> 1" << endl;

        C c; // creates the object c of type C
        // to do so calls the constructor C whcih in turn calls the
        // constructor A, then the constructor B and then the rest
        // part of the constructor C
        // The order of the constructors calls are taken from the
        // inheritance statement class C: public A, B
        // and not from the way I have defined the constructor C : B(), A() etc
        // GNU C++ warns about the forced reordering

        cout << " --> 2" << endl;

        A * pa = &c; // pa is a pointer of type A
        // pa points to object c
        // using pa pointer the object c is being treated as of type A
        // DOES NOT CALL ANYTHING


        cout << " --> 3" << endl;

        B * pb = &c; // DOES NOT CALL ANYTHING
        // pb is a pointer of type B
        // pb points to object c
        // using pb pointer the object c is being treated as of type B

        cout << " --> 4" << endl;

        const int x = ((pa == &c) ? 1 : 2); // it is true so --> 1
        // since that is the way I defined pa

        cout << " --> x = " << x << endl;

        cout << " --> 5" << endl;

        const int y = ((pb == &c) ? 3 : 4); // it is true so --> 3
        // since that is the way I definded pb

        cout << " --> y = " << y << endl;

        //const int k1 = (pa == pb) ? 2 : 3 // IT IS A COMPILER ERROR
        // since operand types are incompatible
        // A* and B*
        // to make them compatible you need
        // to use reinterpret_cast

        cout << " --> 6" << endl; // it is true so --> 7
        // dynamic_cast<void*> of a pointer
        // which points to a derived class object c,
        // makes it to point to c BUT AS c to be of
        // parent type class A

        const int k = ((dynamic_cast<void*>(pa) == dynamic_cast<void*>(pb)) ? 7 : 8);

        cout << " --> k = " << k << endl;

        cout << " --> 7" << endl; // it is wrong so --> 6
        // reinterpret_cast<void*> of a pointer
        // which points to a derived class object c
        // KEEP the information and the
        // object keeps point to something related to the
        // derived class

        const int z = (reinterpret_cast<void*>(pa) == reinterpret_cast<void*>(pb)) ? 5 : 6;

        cout << " --> z = " << z << endl;

        cout << " --> 8" << endl;

        cout << " --> typeid(reinterpret_cast<void*>(pa)).name() = "
             << typeid(reinterpret_cast<void*>(pa)).name() << endl;

        cout << " --> 9" << endl;

        cout << " --> typeid(reinterpret_cast<void*>(pb)).name() = "
             << typeid(reinterpret_cast<void*>(pb)).name() << endl;

        cout << " --> 10" << endl;

        cout << " --> reinterpret_cast<void*>(pa) = "
             << reinterpret_cast<void*>(pa) << endl;

        cout << " --> 11" << endl;

        cout << " --> reinterpret_cast<void*>(pb) = "
             << reinterpret_cast<void*>(pb) << endl;

        cout << " --> 12" << endl;

        cout << " --> typeid(dynamic_cast<void*>(pa)).name() = "
             << typeid(dynamic_cast<void*>(pa)).name() << endl;

        cout << " --> 13" << endl;

        cout << " --> typeid(dynamic_cast<void*>(pb)).name() = "
             << typeid(dynamic_cast<void*>(pb)).name() << endl;

        cout << " --> 14" << endl;

        cout << " --> dynamic_cast<void*>(pa) = "
             << dynamic_cast<void*>(pa) << endl;

        cout << " --> 15" << endl;

        cout << " --> dynamic_cast<void*>(pb) = "
             << dynamic_cast<void*>(pb) << endl;

        cout << " --> 16" << endl;

        cout << x << y << z << k <<endl; // it is --> 1367

    } // end of local innermost scope --> ends


    // at the exit of the local innermost scope
    // the object c is being destroyed so the related
    // destructors are being called in the following order
    // destructor C, then destructor B, then destructor A

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

