
//=====================//
// test --> 0010 --> b //
//=====================//

// Status --> FINISHED

//==============================================================================
//
// --> the main points here are:
//
// --> 1 --> a base object always uses base functions ONLY
//
// --> 2 --> a derived object uses both base and derived functions
//	     --> when a function is only declared in base class
//           	 uses that function
//	     --> when a function is only declared in the derived class
//		 uses that function
//	     --> when a function is declared both in base and derived class
//		 uses the derived only version of the function
//		 the virtual qualifier does not play any role here
//
// --> 3 --> a pointer or a reference of base type to an object of derived type
//	     --> uses functions from both base and derived class
//	     --> for virtual declared functions uses the derived version
//	     --> for non virtual declared function uses the base version if exists
//		 or the derived version if the base does not exists
//
//==============================================================================

#include <iostream>

using std::cout;
using std::cin;
using std::endl;

// class --> A

class A {
public:

    // constructor

    A()
    {
        cout << " --> constructor --> A" << endl;
    }

    // function --> f1 --> A

    virtual void f1(int n)
    {
        cout << " --> function --> f1 --> virtual --> A" << endl;
        cout << n << endl;
    }

    // function --> f2 --> A

    void f2(int n)
    {
        cout << " --> function --> f2 --> A" << endl;
        cout << n << endl;
    }

    // function --> f3 --> A

    void f3(int n)
    {
        cout << " --> function --> f3 --> A" << endl;
        cout << n << endl;
    }

    // destructor

    virtual ~A()
    {
        cout << " --> destructor --> ~A" << endl;
    }

};

// class --> B

class B : public A {
public:

    // constructor

    B() : A()
    {
        cout << " --> constructor --> B" << endl;
    }

    // function --> f1 --> B

    void f1(int n)
    {
        cout << " --> function --> f1 --> B" << endl;
        cout << n << endl;
    }

    // function --> f2 --> B

    void f2(int n)
    {
        cout << " --> function --> f2 --> B" << endl;
        cout << n << endl;
    }

    // function --> f4 --> B

    void f4(int n)
    {
        cout << " --> function --> f4 --> B" << endl;
        cout << n << endl;
    }

    // destructor --> ~B

    virtual ~B()
    {
        cout << " --> destructor --> ~B" << endl;
    }
};

// the main function

int main()
{
    {
        // local innermost scope --> starts

        cout << " --> 1" << endl;

        A a; // creates the object a of type A
        // to do so calls the constructor A

        cout << " --> 2" << endl;

        B b; // creates the object b of type B
        // to do so calls the constructos A and the B

        cout << " --> 3" << endl;

        A & c = b; // DOES NOT CALL ANY CONSTRUCTOR
        // since does not create anything
        // c is another name for b, but treated as of type A
        // when calls non-virtual functions,
        // but when calls virtual functions is treated as type B

        cout << " --> 4" << endl;

        A * d = &b; // DOES NOT CALL ANY CONSTRUCTOR
        // since does not create anything
        // d is a pointer of type A to an object of type B
        // so any function access via d pointer
        // is that of the class A unless the function is
        // declared virtual and then the function of class B
        // is used. IT IS THE SAME CASE AS THAT OF case 3

        cout << " --> 5" << endl;

        B & c2 = b; // NOTHING SPECIAL HERE
        // c2 is equivalent to b
        // and always treated as of type B
        // which derived from A

        cout << " --> 6" << endl;

        B * d2 = &b; // NOTING SPECIAL HERE
        // d2 is equivalent to b
        // and always treated as of type B
        // which derived from A

        cout << " --> 7" << endl;

        a.f1(1); // calls the f1 of class A

        cout << " --> 8" << endl;

        a.f2(2); // calls the f2 of class A

        cout << " --> 9" << endl;

        b.f1(3); // calls the f1 of class B

        cout << " --> 10" << endl;

        b.f2(4); // calls the f2 of class B

        cout << " --> 11" << endl;

        c.f1(5); // calls the f1 of class B
        // since f1 is declared virtual in class A
        //

        cout << " --> 12" << endl;

        c.f2(6); // calls the f2 of class A
        // since c is treated as of type A and B
        // and f2 is NOT virtual in class A

        cout << " --> 13" << endl;

        d->f1(7); // calls f1 of class B
        // since d is a pointer of type A to an object of type B
        // and f1 is declared virtual in class A

        cout << " --> 14" << endl;

        d->f2(8); // calls f2 of class A
        // since d is a pointer f type A to an object of type B
        // and f2 is not declared virtual in class A

        cout << " --> 15" << endl;

        c2.f1(9); // c2 is another name for the object c
        // so calls only class B functions
        // or class A functions if there are no equivalent,
        // virtual or non-virtual, to the class B

        cout << " --> 16" << endl;

        c2.f2(10); // SAME AS STEP 15

        cout << " --> 17" << endl;

        d2->f1(11); // SAME AS STEP 15

        cout << " --> 18" << endl;

        d2->f2(12); // SAME AS STEP 15

        cout << " --> 19" << endl;

        B e; // creates the object e of type B

        cout << " --> 20" << endl;

        e.f3(13); // calls the f3 of class A
        // since it is of type B derived by A

        cout << " --> 21" << endl;

        e.f4(14); // calls the f4 of class B
        // since it is of type B derived by A

        cout << " --> 22 --> end" << endl;

    } // local innermost scope --> ends

    cout << " --> 23 --> exit" << endl;

    // At the exit of the local innermost scope

    // sentineling

    int sentinel;
    cin >> sentinel;

    cout << " --> 24 --> exit" << endl;

    return 0;
}

//======//
// FINI //
//======//

