
//===============//
// test --> 0028 //
//===============//

#include <iostream>

using std::cout;
using std::cin;
using std::endl;

// class A

class A {
public:

    // destructor

    A()
    {
        cout << " --> constructor --> A" << endl;
    }

    // constructor

    virtual ~A()
    {
        cout << " --> destructor --> ~A" << endl;
        f();
    }

public:

    virtual void f() const
    {
        cout << " --> function --> f --> A" << endl;
        cout << 1 << endl;
    }
};

// class B

class B : public A {
public:

    // constructor

    B()
    {
        cout << " --> constructor --> B" << endl;
    }

    // destructor

    ~B()
    {
        cout << " --> destructor --> ~B" << endl;
        f();
    }

private:

    virtual void f() const
    {
        cout << " --> function --> f --> B" << endl;
        cout << 2 << endl;
    }
};

int main()
{
    {
        // local innermost scope --> starts

        cout << " --> 1" << endl;

        const A * a = new B; // constant pointer of type A
        // which creates an object of type B
        // so the constructor A and the the constructor B
        // are being called

        cout << " --> 2" << endl;

        delete a; // Deletes the object of type B
        // so the destructors B and A are being called
        // Destructor B calls the local private function f
        // which replaces the inherited
        // Destructor A calls the local public function f

        cout << " --> 3" << endl;

    } // local innermost scope --> ends

    // the local innermost scope does not produce
    // any output since ALL the obkect have already been
    // deleted in the "delete a" statement

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

