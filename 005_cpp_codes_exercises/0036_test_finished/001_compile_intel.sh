#!/bin/bash

  icpc 	-O3                \
       	-Wall              \
       	-std=c++11         \
		-ftemplate-depth-10000 \
       	driver_program.cpp \
       	-o x_intel
