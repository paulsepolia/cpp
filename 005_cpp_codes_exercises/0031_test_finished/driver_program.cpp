
//===============//
// test --> 0031 //
//===============//

// Status --> FINISHED

#include <iostream>
#include <memory>
#include <iomanip>

using std::cout;
using std::cin;
using std::endl;
using std::showpoint;
using std::showpos;
using std::setprecision;
using std::fixed;

// the class --> A

class A {
public:

    // constructor

    A() : m_n(++m_object_id)
    {
        cout << " --> constructor --> A" << endl;
        cout << " --> m_object_id --> " << m_object_id << endl;

    }

    // destructor

    ~A()
    {
        cout << " destructor --> ~A" << endl;
        cout << m_n << endl;
    }

private:

    const int m_n;
    static int m_object_id;
};

// initializing the static variable

int A::m_object_id = 0;

// the main function

int main()
{
    // adjust the output format

    cout << fixed;
    cout << showpos;
    cout << showpoint;
    cout << setprecision(15);

    {
        // local innermost scope --> starts

        cout << " --> 1" << endl;

        A * const p = new A[2];

        cout << " --> 2" << endl;

        A * const q = reinterpret_cast<A * const>(new char[2 * sizeof(A)]);

        cout << " --> 3" << endl;

        new (q) A;

        cout << " --> 4" << endl;

        new (q + 1) A;

        cout << " --> 5" << endl;

        q->~A();

        cout << " --> 6" << endl;

        q[1].~A();

        cout << " --> 7" << endl;

        delete [] reinterpret_cast<char *>(q);

        cout << " --> 8" << endl;

        delete [] p;

    } // local innermost scope --> ends

    float flo1 = 200.0;
    float flo2 = 1000000000.0;

    cout << " --> flo1 + flo2 = " << flo1+flo2
         << " --> ROUND OFF ERROR HERE!" << endl;

    double dou1 = 200.0;
    double dou2 = 1000000000.0;

    cout << " --> dou1 + dou2 = " << dou1 + dou2 << endl;

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

