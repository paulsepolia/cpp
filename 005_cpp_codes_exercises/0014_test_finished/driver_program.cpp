
//===============//
// test --> 0014 //
//===============//

// Status --> FINISHED

//==============================================================================
//
// --> the main points here are:
//
// --> 1 --> use of the polymorhism
//
// --> 2 --> a base function declared as virtual is being replaced
//	     by the derived version when using a pointer or a reference
//           of base type, which points or references to an object of
//           the derived class
//
// --> 3 --> a derived type pointer and object always calls the
//	     derived versions of the functions if available
//	     It can also call the inherited base class functions
//
// --> 4 --> a "const" qualifier produces different signatures for functions
//
//==============================================================================

#include <iostream>

using std::cout;
using std::cin;
using std::endl;

// class --> A

class A {
public:

    // constructor

    A()
    {
        cout << " --> constructor --> A" << endl;
    }

    // f --> A --> 1

    virtual void f()
    {
        cout << " --> function --> f --> A --> 1" << endl;
    }

    // f --> A --> 2

    virtual void f() const
    {
        cout << " --> function --> f --> A --> 2" << endl;
    }

    // destructor

    virtual ~A()
    {
        cout << " --> destructor --> ~A" << endl;
    }


};

// class --> B

class B : public A {
public:

    // constructor --> B

    B() : A()
    {
        cout << " --> constructor --> B" << endl;
    }

    // function --> f --> B --> 1

    void f()
    {
        cout << " --> function --> f --> B --> 1" << endl;
    }

    // function --> f --> B --> 2

    void f() const
    {
        cout << " --> function --> f --> B --> 2" << endl;
    }

    // destructor --> ~B

    virtual ~B()
    {
        cout << " --> destructor --> ~B" << endl;
    }
};

// the main function

int main()
{
    {
        // local innermost scope --> starts

        cout << " --> 1" << endl;

        const A a1; // creates the object a1 of type A
        // to do so, calls the constructor A
        // a1 can not be modified;

        cout << " --> 2" << endl;

        A a2; // creates the object a2 of type A
        // to do so, calls the constructor A

        cout << " --> 3" << endl;

        const B b1; // creates the object b1 of type B
        // to do so, calls the constructor A
        // and then calls the constructor B
        // b1 can not be modified

        cout << " --> 4" << endl;

        B b2; // creates the object b2 of type B
        // to do so, calls the constructor A
        // and the the constructor B

        cout << " --> 5" << endl;

        const A & c1 = b1; // DOES NOT CALL ANYTHING
        // c1 has the same address as of b1
        // but c1 is of type A

        cout << " --> 6" << endl;

        A & c2 = b2; // DOES NOT CALL ANYTHING

        cout << " --> 7" << endl;

        const A * d1 = &b1; // DOES NOT CALL ANYTHING
        // since d1 points to object b1
        // but d1 is of type A*

        cout << " --> 8" << endl;

        A * d2 = &b2; // DOES NOT CALL ANYTHING

        cout << " --> 9" << endl;

        a1.f(); // calls the f --> A --> 2
        // because of the const qualifier

        cout << " --> 10" << endl;

        a2.f(); // calls the f --> A --> 1
        // because of the non-const qualifier

        cout << " --> 11" << endl;

        b1.f(); // calls the f --> B --> 2
        // because of the const qualifier

        cout << " --> 12" << endl;

        b2.f(); // calls the f --> B --> 1
        // because of the non-const qualifier

        cout << " --> 13" << endl;

        c1.f(); // calls the f --> B --> 2
        // because the base f --> A --> 2 is virtual
        // so the derived is being called

        cout << " --> 14" << endl;

        c2.f(); // calls the f --> B --> 1
        // because the base f --> A --> 1 is virtual
        // so the derived is being called

        cout << " --> 15" << endl;

        d1->f(); // calls the f --> B --> 2
        // because the base f --> A --> 2 is virtual
        // so the derived is being called

        cout << " --> 16" << endl;

        d2->f(); // calls the f --> B --> 1
        // because the base f --> A --> 1 is virtual
        // so the derived is being called

        cout << " --> 17 --> end" << endl;

    } // local innermost scope --> ends

    cout << " --> 18 --> exit" << endl;

    // At the exit of the local innermost scope
    // 4 objects are being destroyed, a1, a2 and b1, b2
    // so the related destructors are being called

    // sentineling

    int sentinel;
    cin >> sentinel;

    cout << " --> 19 --> exit" << endl;

    return 0;
}

//======//
// FINI //
//======//

