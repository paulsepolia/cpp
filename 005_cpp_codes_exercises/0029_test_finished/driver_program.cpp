
//===============//
// test --> 0029 //
//===============//

// Status --> FINISHED

#include <exception>
#include <iostream>

using std::exception;
using std::cout;
using std::cin;
using std::endl;

// class --> A

class A {
public:

    // constructor

    A()
    {
        cout << " --> constructor --> A" << endl;
    }

    // destructor

    virtual ~A()
    {
        cout << " --> destructor --> ~A" << endl;
    }
};

// class --> B

class B : public A {
public:

    // constructor

    B()
    {
        cout << " --> constructor --> B" << endl;
    }

    // destructor

    ~B()
    {
        cout << " --> destructor --> ~B" << endl;
    }
};

// class --> C

class C : public A {
public:

    // constructor

    C()
    {
        cout << " --> constructor --> C" << endl;
    }

    // destructor

    ~C()
    {
        cout << " --> destructor --> ~C" << endl;
    }

};

// the main function

int main()
{
    {
        // local innermost scope --> starts

        cout << " --> 1" << endl;

        try {
            cout << " --> 2" << endl;

            B b; // creates object of type B, so calls
            // constructor A and then constructor B

            cout << " --> 3" << endl;

            C c; // creates object of type C, so calls
            // constructor A and then constructor C

            cout << " --> 4" << endl;

            A * pa1 = &b; // DOES NOT CALL ANYTHING
            // since b is already created

            cout << " --> 5" << endl;

            A * pa2 = &c; // DOES NOT CALL ANYTHING
            // since c is already created

            cout << " --> 6" << endl;

            cout << ((dynamic_cast<B*>(pa1) != nullptr) ? 1 : 0) << endl;
            // it is true, so the value is 1

            cout << " --> 7" << endl;

            cout << ((dynamic_cast<B*>(pa2) != nullptr) ? 1 : 0) << endl;
            // it is false, so the value is 0

            cout << " --> 8" << endl;

            B &b1 = dynamic_cast<B&>(*pa2);
            // throws an excption here - i do not know why
            // An important point is that the objects b and c are
            // being destroyed immediately when the exception is being thrown
            // so the destructors ~C, ~A and then ~B, ~A are being called

            cout << " --> 9" << endl;

            pa1 = &b1;

            cout << " --> 10" << endl;

            cout << ((static_cast<C*>(pa1) != nullptr) ? 1 : 0) << endl;

            cout << " --> 11" << endl;
        } catch(const exception &) {
            // exception is being cought here
            cout << " --> 12" << endl;

            cout << 2 << endl;

            cout << " --> 13" << endl;
        }

    } // local innermost scope --> ends

    cout << " --> 14 --> exit" << endl;

    int sentinel;
    cin >> sentinel;

    cout << " --> 15 --> exit" << endl;

    return 0;
}

//======//
// FINI //
//======//

