
//=====================//
// test --> 0017 --> a //
//=====================//

// Status --> FINISHED

//==============================================================================
//
// --> the main points here are:
//
// --> 1 --> operator int() const { return m_i; }
//
//           The above definition of the operator int()
//           inside a class converts an object A to int
//           so you can write e.g int i = A;
//
// --> 2 --> the copy constructor is being called
//           only when initializing/declaring new objects.
//           NOWHERE ELSE
//
//==============================================================================

#include <iostream>

using std::cout;
using std::cin;
using std::endl;

// class --> A

class A {
public:

    // constructor

    explicit A(int i) : m_i(i)
    {
        cout << " --> constructor --> A" << endl;
    }

    // destructor

    ~A()
    {
        cout << " --> destructor --> ~A" << endl;
    }

    // operator overload

    int operator() (int i = 0) const
    {
        cout << " --> operator --> op --> 1" << endl;
        cout << " --> m_i = " << m_i << endl;

        return m_i + i;
    }

    // operator int() overload
    // THIS IS A CONVERION OPERATOR

    operator int() const
    {
        cout << " --> operator --> op --> 2" << endl;
        cout << " --> m_i = " << m_i << endl;

        return m_i;
    }

    // copy constructor

    A(const A & a)
    {
        cout << " --> copy constructor --> A" << endl;
        this->m_i = a.m_i;
    }

    // friend function

    friend int g1(const A&);

    // friend function

    friend int g(const A&);

private:

    int m_i;
};

// function --> f

int f(char c)
{
    cout << " --> function --> f" << endl;

    return c;
}

// friend function --> g1

int g1(const A& a)
{
    cout << " --> function --> g1" << endl;

    return a.m_i;
}

// friend function --> g

int g(const A& a)
{
    cout << " --> function --> g" << endl;

    return a.m_i;
}

// the main function

int main()
{
    {
        // local innermost scope --> starts

        cout << " --> 1" << endl;

        A f(2); // creates the object f of type A
        // to do so, calls the constructor A
        // and then sets m_i equal to 2

        cout << " --> 2" << endl;

        A g(4); // creates the object g of type A
        // to do so, calls the constructor A
        // and then sets m_i equal to 4

        cout << " --> 3" << endl;

        cout << f(20) << endl; // calls the above created object f
        // DOES NOT CALL THE function int f(char)
        // to do so, calls the overloaded int operator(int = 0)
        // so, outputs all the related statements
        // and m_i + i = 2 + 20 = 22

        cout << " --> 4" << endl;

        cout << g(f(10)) << endl; // initialy calls the f object with m_i = 2
        // so f(10) --> 2 + 10 + 12
        // then calls g object, g(12) with m_i = 4
        // so g(12) = 12 + 4 = 16

        cout << " --> 5" << endl;

        cout << g(f) << endl; // DOES NOT CALL THE COPY CONSTRUCTOR
        // DOES NOT CALL THE FUNCTION g
        // does call the overloaded operator () (int)
        // but the argument is of type A,
        // there must be able to convert f object to an integer
        // so it calls the operator int() and the rest are
        // easy to follow

        cout << " --> 6" << endl;

        A k(f); // calls the copy constructor

        cout << " --> 7" << endl;

        g1(k); // calls the friend function g1

        cout << " --> 8 --> end" << endl;

    } // local innermost scope --> ends

    // at the exit of the innermost local scope
    // the objects f, g, k are being destroyed
    // by calling the destructor ~A

    cout << " --> 9 --> exit" << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    cout << " --> 10 --> exit" << endl;

    return 0;
}

//======//
// FINI //
//======//

