
//===============//
// test --> 0008 //
//===============//

// Status --> FINISHED

//==============================================================================
// --> the main points are the following
//
// --> 1 --> int() equals 0
//
// --> 2 --> double() equals 0
//
// --> 3 --> bool() equals 0 = false
//
// --> 4 --> char() equals the null character '\0'
//
// --> 5 --> a template can have the forms:
//
//           template<typename T, T t>, so <int, 10>
//	     template<typename T, T t>, so <char, 'a'>
//   	     template<typename T, T t = T()>, so <char>
//					 or <int, -9 >
//					 or <bool, true> etc
//
//==============================================================================


#include <iostream>

using std::endl;
using std::cout;
using std::cin;

// class --> A

template<typename T, T t = T()>
class A {
public:

    // constructor

    A()
    {
        cout << " --> constructor --> A" << endl;
    }

    // destructor

    virtual ~A()
    {
        cout << " --> destructor --> ~A" << endl;
    }

private:

    template<bool b>
    class B {
    public:
        // constructor

        B()
        {
            cout << " --> constructor --> B" << endl;
        }

        // destructor

        ~B()
        {
            cout << " --> destructor --> ~B" << endl;
        }

        // public member variable

        static const int m_n = (b ? 1 : 0);
    };

public:

    static const int m_value = B<(t > T())>::m_n - B<(t < T())>::m_n;
};

// the main function

int main()
{
    {
        // local innermost scope --> starts

        cout << " --> 1" << endl;

        cout << A<int, -9>::m_value << endl;

        // T is int, and t = -9, so
        // T() = int() = 0 and
        // t > T() is false --> -9 > 0
        // t < T() is true --> -9 < 0
        // so m_value = B<false> - B(true) = 0 - 1 ==> m_value = -1

        cout << " --> 2" << endl;

        cout << A<bool, true>::m_value << endl;

        // T is bool, and t = true, so
        // T() = bool() = 0 and
        // t > T() is true --> true > 0
        // t < T() is false --> true < 0
        // so m_value = B<true> - B<false> = 1 - 0 ==> m_value = 1

        cout << " --> 3" << endl;

        cout << A<char>::m_value << endl;

        // T is char, and t = 0, so
        // T() = char() = null character and
        // t < T() is false --> 0 < null character
        // t > T() is false --> 0 > null character
        // so m_value = B(false) - B(false) = 0 - 0 ==> m_value = 0

        cout << " --> 4" << endl;

    } // local innermost scope --> ends

    cout << " --> 5 --> exit" << endl;

    // sentineling

    cout << " --> int()       = " << int()       << endl;
    cout << " --> double()    = " << double()    << endl;
    cout << " --> char()      = " << char()      << endl;
    cout << " --> int(char()) = " << int(char()) << endl;
    cout << " --> bool()      = " << bool()      << endl;
    cout << " --> the null character is equal to char()" << (char() == '\0') << endl;

    int sentinel;
    cin >> sentinel;

    cout << " --> 6 --> exit" << endl;

    return 0;
}

//======//
// FINI //
//======//

