
//===============//
// test --> 0024 //
//===============//

// Status --> FINISHED

#include <iostream>

using std::cout;
using std::cin;
using std::endl;

// class --> A

class A {
public:

    // constructor

    A(int n = 0) : m_i(n)
    {
        cout << " --> constructor --> A" << endl;
        cout << m_i << endl;
    }

    // destructor

    virtual ~A()
    {
        cout << " --> destructor --> ~A" << endl;
    }

protected:

    int m_i;
};

// class --> B

class B : public A {
public:

    // constructor

    B(int n) : m_j(n), m_a(--m_j), m_b()
    {
        cout << " --> constructor --> B" << endl;
        cout << m_j << endl;
    }

    // destructor

    virtual ~B()
    {
        cout << " --> destructor --> B" << endl;
    }

private:

    int m_j;
    A m_a;
    A m_b;
    static A m_c; // is being called
};

// the main function

int main()
{
    {
        // local innermost scope --> starts

        cout << " --> 1" << endl;

        B b(2);

        cout << " --> 2" << endl;

    } // local innermost scope --> ends

    cout << " --> 3" << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    cout << " --> 4" << endl;

    return 0;
}

A B::m_c(3);

//======//
// FINI //
//======//

