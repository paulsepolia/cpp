#include "../rapidjson/writer.h"
#include "../rapidjson/stringbuffer.h"
#include "../rapidjson/document.h"
#include <iostream>
#include <string>
#include <cmath>

int main() {

    //=============//
    // Example # 1 //
    //=============//
    //==================================//
    // Document                         //
    // Creates a document and parses it //
    //==================================//

    {
        const std::string res = R"({"hello": "world",
                "t": true,
                "f": false,
                "n": null,
                "i": 123,
                "pi": 3.1416,
                "a": [1, 2, 3, 4]})";

        rapidjson::Document document;

        document.Parse(res.c_str());

        // probable this is the way it is stored internally in rapidjson lib

        const char *kTypeNames[] =
                {"Null", "False", "True", "Object", "Array", "String", "Number"};

        for (rapidjson::Value::ConstMemberIterator it = document.MemberBegin(); it != document.MemberEnd(); ++it) {

            std::cout << "Type of member: " << it->name.GetString() << std::endl;
            std::cout << "is this: " << it->value.GetType() << std::endl;
            std::cout << "and this: " << kTypeNames[it->value.GetType()] << std::endl;
        }
    }

    //=============//
    // Example # 2 //
    //=============//
    //==================================//
    // Document                         //
    // Creates a document and parses it //
    //==================================//

    {
        const std::string res = R"({"hello": "world",
                "t": true,
                "f": false,
                "n": null,
                "i": 123,
                "pi": 3.1416,
                "a": [1, 2, 3, 4]})";

        rapidjson::Document document;

        document.Parse(res.c_str());

        // probable this is the way it is stored internally in rapidjson lib

        const char *kTypeNames[] =
                {"Null", "False", "True", "Object", "Array", "String", "Number"};

        for (const auto &m : document.GetObject()) {

            std::cout << "Type of member: " << m.name.GetString() << std::endl;
            std::cout << "is this: " << m.value.GetType() << std::endl;
            std::cout << "and this: " << kTypeNames[m.value.GetType()] << std::endl;
        }
    }

    return 0;
}
