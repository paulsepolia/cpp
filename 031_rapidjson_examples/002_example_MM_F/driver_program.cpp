#include "../rapidjson/writer.h"
#include "../rapidjson/stringbuffer.h"
#include "../rapidjson/document.h"
#include <iostream>
#include <string>
#include <cmath>

int main() {

    //=============//
    // Example # 1 //
    //=============//
    //==================================//
    // StringBuffer and Writer          //
    // Creates an array in json message //
    //==================================//

    {
        rapidjson::StringBuffer s;
        rapidjson::Writer<rapidjson::StringBuffer> writer(s);

        const auto DIM_MAX = static_cast<uint64_t>(std::pow(10.0, 3.0));

        writer.StartObject();
        writer.Key("array");
        writer.StartArray();

        for (uint64_t i = 0; i < DIM_MAX; i++) {
            writer.Uint64(i);
        }

        writer.EndArray();
        writer.EndObject();
        const std::string res(s.GetString());

        rapidjson::Document document;

        document.Parse(res.c_str());

        assert(document.IsObject());

        // Using a reference for consecutive access is handy and faster

        const rapidjson::Value &a = document["array"];

        assert(a.IsArray());

        for (const auto& val : a.GetArray()) {
            std::cout << val.GetUint64() << std::endl;
        }
    }

    return 0;
}
