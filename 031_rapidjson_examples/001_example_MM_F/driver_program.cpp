#include "../rapidjson/writer.h"
#include "../rapidjson/stringbuffer.h"
#include "../rapidjson/document.h"
#include <iostream>
#include <string>
#include <cmath>

int main() {

    //=============//
    // Example # 1 //
    //=============//
    //============================//
    // Writer && StringBuffer     //
    // Creates a json type string //
    //============================//

    {
        rapidjson::StringBuffer s;
        rapidjson::Writer<rapidjson::StringBuffer> writer(s);

        writer.StartObject();               // Between StartObject()/EndObject(),
        writer.Key("hello");                // output a key,
        writer.String("world");             // follow by a value.
        writer.Key("t");
        writer.Bool(true);
        writer.Key("f");
        writer.Bool(false);
        writer.Key("n");
        writer.Null();
        writer.Key("i");
        writer.Uint(123456789);
        writer.Key("pi");
        writer.Double(3.1234567890123456789);
        writer.Key("a");
        writer.StartArray();                // Between StartArray()/EndArray(),

        for (unsigned i = 0; i < 10; i++) {
            writer.Uint(i);                 // all values are elements of the array.
        }

        writer.EndArray();
        writer.EndObject();

        const std::string res(s.GetString());

        std::cout << res << std::endl;
    }

    //=============//
    // Example # 2 //
    //=============//
    //==================================//
    // Document                         //
    // Creates a document ada parses it //
    //==================================//

    {

        const std::string res = R"({"hello": "world",
                "t": true,
                "f": false,
                "n": null,
                "i": 123,
                "pi": 3.1416,
                "a": [1, 2, 3, 4]})";

        rapidjson::Document document;

        document.Parse(res.c_str());

        assert(document.IsObject());
        assert(document.HasMember("hello"));
        assert(document["hello"].IsString());

        std::cout << "hello = " << document["hello"].GetString() << std::endl;

        assert(document["t"].IsBool());

        std::cout << "t = " << (document["t"].GetBool() ? "true" : "false") << std::endl;

        std::cout << "n = " << (document["n"].IsNull() ? "null" : "?") << std::endl;

        assert(document["i"].IsNumber());
        assert(document["i"].IsInt());

        std::cout << "i = " << document["i"].GetInt() << std::endl;

        assert(document["pi"].IsNumber());
        assert(document["pi"].IsDouble());

        std::cout << "pi = " << document["pi"].GetDouble() << std::endl;

        // Using a reference for consecutive access is handy and faster.

        const rapidjson::Value &a = document["a"];

        assert(a.IsArray());

        for (rapidjson::SizeType i = 0; i < a.Size(); i++) {
            std::cout << "a[" << i << "] = " << a[i].GetInt() << std::endl;
        }
    }

    //=============//
    // Example # 3 //
    //=============//
    //==================================//
    // StringBuffer and Writer          //
    // Creates an array in json message //
    //==================================//

    {
        rapidjson::StringBuffer s;
        rapidjson::Writer<rapidjson::StringBuffer> writer(s);

        const auto DIM_MAX = static_cast<uint64_t>(std::pow(10.0, 3.0));

        writer.StartObject();
        writer.Key("array");
        writer.StartArray();

        for (uint64_t i = 0; i < DIM_MAX; i++) {
            writer.Uint64(i);
        }

        writer.EndArray();
        writer.EndObject();
        const std::string res(s.GetString());

        rapidjson::Document document;

        document.Parse(res.c_str());

        assert(document.IsObject());

        // Using a reference for consecutive access is handy and faster

        const rapidjson::Value &a = document["array"];

        assert(a.IsArray());

        for (rapidjson::SizeType i = 0; i < a.Size(); i++) {
            std::cout << "array[" << i << "] = " << a[i].GetInt() << std::endl;
        }
    }

    return 0;
}
