//  Title: The Dot product function for vectors
//
//  Interface: inline double Dot_F(double* vectorA,
//		                           double* vectorB,
//							       int dim)
//
//  Input: (1) double* vectorA -> The first vector
//		   (2) double* vectorB -> The second vector
//		   (3) int dim -> The dimension of each vector
//
//  Output: (1) double sum -> The resultant dot product scalar
//
//  Purpose: Gives back the scalar dot product of the two vectors
//           vectorA[0...dim-1] and vectorB[0...dim-1]

inline double Dot_F(const double *vectorA,
                    const double *vectorB,
                    int dim) {
    double sum = 0.0;

    // OpenMP code starts here

    int i; // OpenMP variable

# pragma omp parallel for\
                                       if(ompB)\
                                       default(none)\
                                       shared(vectorA, vectorB, dim)\
                                       private(i)\
                                       num_threads(NT)\
                                       schedule(static)\
                                       reduction(+ : sum)

    // OpenMP code ends here

    for (i = 0; i < dim; i++) {
        sum += vectorA[i] * vectorB[i];
    }

    return sum;
}

//  Title: The Subtraction for vectors
//
//  Interface: inline void SubVe_F(double* RESTA,
//		                           double* vectorA,
//								   double* vectorB,
//								   int dim)
//
//  Input:  (1) double* vectorA -> The first vector
//		    (2) double* vectorB -> The second vector
//		    (3) int dim -> The dimension of each vector
//
//  Output: (1) double* RESTA -> The resultant vector
//
//  Purpose: Gives back the vector RESTA[0...dim-1], which is the vector
//           subtraction of vectorB[0...dim-1] from vectorA[0...dim-1]

inline void SubVe_F(double *RESTA,
                    const double *vectorA,
                    const double *vectorB,
                    int dim) {

    // OpenMP code starts here

    int i;  // OpenMP variable

# pragma omp parallel for\
                                       if(ompB)\
                                       default(none)\
                                       shared(RESTA, vectorA, vectorB, dim)\
                                       private(i)\
                                       num_threads(NT)\
                                       schedule(static)

    // OpenMP code ends here

    for (i = 0; i < dim; i++) {
        RESTA[i] = vectorA[i] - vectorB[i];
    }
}

//  Title: The Addition for vectors
//
//  Interface : inline void AddVe_F(double* RESTA,
//		                            double* vectorA,
//								    double* vectorB,
//								    int dim)
//
//  Input: (1) double* vectorA -> The first vector
//		   (2) double* vectorB -> The second vector
//		   (3) int dim -> The dimension of each vector
//
//  Output: (1) double* RESTA -> The resultant vector
//
//  Purpose: Gives back the vector RESTA[0...dim-1], which is
//          the vector addition of vectorA[0...dim-1] plus vectorB[0...dim-1]

inline void AddVe_F(double *RESTA,
                    const double *vectorA,
                    const double *vectorB,
                    int dim) {
    // OpenMP code starts here

    long int i;  // OpenMP variable

# pragma omp parallel for\
                                       if(ompB)\
                                       default(none)\
                                       shared(RESTA, vectorA, vectorB, dim)\
                                       private(i)\
                                       num_threads(NT)\
                                       schedule(static)

    // OpenMP code ends here

    for (i = 0; i < dim; i++) {
        RESTA[i] = vectorA[i] + vectorB[i];
    }
}

//  Title: The Multiplication of a vector by a scalar
//
//  Interface: inline void ScaVe_F(double* RESTA,
//		                           double scalar,
//								   double* vector,
//								   int dim)
//
//  Input: (1) double scalar -> The scalar variable
//		   (2) double* vector -> The vector variable
//		   (3) int dim -> The dimension of vector
//
//  Output: (1) double* RESTA -> The resultant vector
//
//  Purpose: Gives back the vector RESTA[0...dim-1], which is the
//           multiplication of vector[0...dim-1] by the scalar

inline void ScaVe_F(double *RESTA,
                    double scalar,
                    const double *vector,
                    int dim) {

    // OpenMP code starts here

    int i;  // OpenMP variable

# pragma omp parallel for\
                                       if(ompB)\
                                       default(none)\
                                       shared(RESTA, vector, scalar, dim)\
                                       private(i)\
                                       num_threads(NT)\
                                       schedule(static)

    // OpenMP code ends here

    for (i = 0; i < dim; i++) {
        RESTA[i] = vector[i] * scalar;
    }
}

//  Title: The Dot product between a matrix and a vector
//
//  Interface: inline void Dot_F(double* RESTA,
//		                         double** Matrix,
//							     double* vector,
//							     int dim)
//
//  Input: (1) double** Matrix -> The matrix variable
//		   (2) double* vector -> The vector variable
//		   (3) int dim -> The dimension of vector
//
//  Output: (1) double* RESTA -> the resultant vector
//
//  Purpose: Gives back the vector RESTA[0...dim-1], which is the
//           multiplication of matrix[0...dim-1][0...dim-1]
//		     by vector[0...dim-1]

inline void Dot_F(double *RESTA,
                  double **matrix,
                  const double *vector,
                  int dim) {

    // OpenMP code 1. starts here

    int i, j; // OpenMP variables

# pragma omp parallel for\
                                       if(ompA)\
                                       default(none)\
                                       shared(RESTA, matrix, vector, dim)\
                                       private(i, j)\
                                       num_threads(NT)\
                                       schedule(static)

    // OpenMP code 1. Ends here

    for (i = 0; i < dim; i++) {

        // Inside loop the initialization to have
        // as many RESTA[i] in L2 cache, as possible

        RESTA[i] = 0.0;

        for (j = 0; j < dim; j++) {
            RESTA[i] += matrix[i][j] * vector[j];
        }
    }
}
