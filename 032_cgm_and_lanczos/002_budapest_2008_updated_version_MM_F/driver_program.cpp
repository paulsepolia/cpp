//  Lanczos Iterative Diagonaliser

# include <iostream>
# include <ctime>
# include "F_Input_Screen.h"

int main() {

    // 1. Wikipedia's Lanczos' algorithm version.
    // 2. Use of complete re-orthogonalisation.
    // 3. Use of Conjugate Gradient algorithm.
    // 4. Use of 'double' precision arithmetic.
    // 5. Storing into ram the Lanczos' vectors.
    // 6. No use of any kind of sparse matrix format.

    Input_Screen_F();

    int sentinel;
    std::cin >> sentinel;

    return 0;
}
