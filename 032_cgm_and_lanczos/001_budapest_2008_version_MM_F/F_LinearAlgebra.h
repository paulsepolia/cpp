//=========================================================
//
//  The Lancos Algorithm.
//
//  Author :: Pavlos G. Galiatsatos.
//
//  Date :: 2008, November.
//
//=========================================================

/* 1.  

Title : The Dot product function for vectors.

Interface : inline double Dot_F( double* vectorA ,
		                         double* vectorB ,
							     int& dim )
								 
Input : (1) double* vectorA -> The first vector.
		(2) double* vectorB -> The second vector.
		(3) int& dim -> The dimension of each vector.
    
Output : (1) double sum -> The resultant dot product scalar.

Purpose : Gives back the scalar dot product of the two vectors 
          vectorA[0...dim-1] and vectorB[0...dim-1].

*/

inline double Dot_F ( double* vectorA ,
			          double* vectorB ,
					  int& dim )
{  
  double sum = 0.0 ;

                                       
                                       // OpenMP code starts here.

                                       int i ; // OpenMP variable.   

                                       # pragma omp parallel for\
                                       if( ompB )\
									   default( none )\
									   shared( vectorA, vectorB, dim )\
	                                   private( i )\
	                                   num_threads( NT )\
	                                   schedule( static )\
		                               reduction( + : sum )

                                       // OpenMP code ends here.

  for ( int i = 0 ; i < dim ; i++ )
  { sum = sum + vectorA[i] * vectorB[i] ; } 
  
  return sum ;
}


/* 2.  

Title : The Subtraction for vectros.
    
Interface : inline void SubVe_F ( double* RESTA , 
		                          double* vectorA , 
								  double* vectorB , 
								  int& dim )
    
Input : (1) double* vectorA -> The first vector.
		(2) double* vectorB -> The second vector.
		(3) int& dim -> The dimension of each vector.
	      
Output : (1) double* RESTA -> The resultant vector.

Purpose : Gives back the vector RESTA[0...dim-1], which is the vector
          subtraction of vectorB[0...dim-1] from vectorA[0...dim-1].  

*/

inline void SubVe_F ( double* RESTA ,
					  double* vectorA ,
				      double* vectorB ,
				      int& dim )
{
                                       // OpenMP code starts here.
	                                    
	                                   int i ;  // OpenMP variable.

                                       # pragma omp parallel for\
									   if( ompB )\
	                                   default( none )\
									   shared( RESTA, vectorA, vectorB, dim )\
	                                   private( i )\
	                                   num_threads( NT )\
	                                   schedule( static )

                                       // OpenMP code ends here.

  for ( int i = 0 ; i < dim ; i++  )
  { RESTA[i] = vectorA[i] - vectorB[i] ; }
}


/* 3.  

Title : The Addition for vectors.
    
Interface : inline void AddVe_F ( double* RESTA , 
		                          double* vectorA , 
								  double* vectorB , 
								  int& dim )
    
Input : (1) double* vectorA -> The first vector.
		(2) double* vectorB -> The second vector.
		(3) int& dim -> The dimension of each vector.
	      
Output : (1) double* RESTA -> The resultant vector.

Purpose : Gives back the vector RESTA[0...dim-1], which is 
          the vector addition of vectorA[0...dim-1] plus vectorB[0...dim-1].

*/

inline void AddVe_F ( double* RESTA ,
					  double* vectorA ,
		        	  double* vectorB ,
				      int& dim )
{ 	
                                       // OpenMP code starts here.

	                                   int i ;  // OpenMP variable.

                                       # pragma omp parallel for\
									   if( ompB )\
	                                   default( none )\
									   shared( RESTA, vectorA, vectorB, dim )\
	                                   private( i )\
	                                   num_threads( NT )\
	                                   schedule( static )

                                       // OpenMP code ends here.

  for ( long i = 0 ; i < dim ; i++  )
  { RESTA[i] = vectorA[i] + vectorB[i] ; }
}


/* 4.  

Title : The Multiplication of a vector by a scalar.
    
Interface : inline void ScaVe_F ( double* RESTA , 
		                          double scalar , 
								  double* vector , 
								  int& dim )
   
Input : (1) double scalar -> The scalar variable.
		(2) double* vector -> The vector variable.
		(3) int& dim -> The dimension of vector.
	      
Output : (1) double* RESTA -> The resultant vector.

Purpose : Gives back the vector RESTA[0...dim-1], which is the 
          multiplication of vector[0...dim-1] by the scalar.

*/

inline void ScaVe_F( double* RESTA ,
					 double scalar ,
					 double* vector , 
				     int& dim )
{
 
                                       // OpenMP code starts here.

	                                   int i ;  // OpenMP variable.
                                       
                                       # pragma omp parallel for\
									   if( ompB )\
									   default( none )\
	                                   shared( RESTA, vector, scalar, dim )\
	                                   private( i )\
	                                   num_threads( NT )\
	                                   schedule( static )

                                       // OpenMP code ends here.

  for ( int i = 0 ; i < dim ; i++ )
  {  RESTA[i] = vector[i] * scalar ; }
}


/* 5.  

Title : The Dot product between a matrix and a vector.

Interface : inline void Dot_F ( double* RESTA , 
		                        double** Matrix , 
							    double* vector , 
							    int& dim )
    
Input : (1) double** Matrix -> The matrix variable.
		(2) double* vector -> The vector variable.
		(3) int& dim -> The dimension of vector.
	      
Output : (1) double* RESTA -> the resultant vector.

Purpose : Gives back the vector RESTA[0...dim-1], which is the
          multiplication of matrix[0...dim-1][0...dim-1]
		  by vector[0...dim-1].

*/

inline void Dot_F ( double* RESTA ,
					double** matrix ,
					double* vector ,
					int& dim )
{  
                                       
                                       // OpenMP code 1. starts here.

                                       int i , j ; // OpenMP variables.
                            
                                       # pragma omp parallel for\
									   if( ompA )\
									   default( none )\
	                                   shared( RESTA, matrix, vector, dim )\
	                                   private( i, j )\
	                                   num_threads( NT )\
	                                   schedule( static )

                                       // OpenMP code 1. ends here.

  for ( int i = 0 ; i < dim ; i++ )
  { 
    RESTA[i] = 0. ;  // Inside loop the initialization to have
	                 // as many RESTA[i] in L2 cache, as possible.

    for ( int j = 0 ; j < dim ; j++ )
	{
      RESTA[i] = RESTA[i] + matrix[i][j] * vector[j] ; 	  
	}
  }
  
}


/* FINI. */
