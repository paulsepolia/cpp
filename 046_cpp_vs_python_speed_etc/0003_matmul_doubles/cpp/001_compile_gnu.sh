#!/bin/bash

  g++-9.2.0  -c           \
             -O3          \
             -Wall        \
             -std=gnu++2a \
             -fopenmp \
             main.cpp

  g++-9.2.0  -pthread \
             -fopenmp \
             main.o \
             -o x_gnu

  rm *.o
