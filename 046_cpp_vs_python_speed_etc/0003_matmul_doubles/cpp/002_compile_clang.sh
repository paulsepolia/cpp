#!/bin/bash

  clang++   -c           \
            -O3          \
            -Wall        \
            -std=gnu++2a \
            -fopenmp     \
            main.cpp

  clang++   -pthread \
            -fopenmp \
            main.o \
            -o x_clang

  rm *.o

