#include <iostream>
#include <chrono>
#include <iomanip>
#include <omp.h>

class benchmark_timer {
public:

    explicit benchmark_timer(const double &res) :
            _res(res),
            t1{std::chrono::steady_clock::now()},
            t2{std::chrono::steady_clock::now()},
            t_prev{std::chrono::steady_clock::now()} {};

    auto print_time() const -> void {
        const auto t_loc{std::chrono::steady_clock::now()};
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t_loc - t_prev).count()};
        t_prev = t_loc;
        std::cout << " --> time used (secs) = " << total_time << std::endl;
    }

    auto reset_timer() -> void {
        const auto t_loc{std::chrono::steady_clock::now()};
        t_prev = t_loc;
        t1 = t_loc;
        t2 = t_loc;
    }

    ~benchmark_timer() {
        t2 = std::chrono::steady_clock::now();
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count()};
        std::cout << " --> res = " << _res << std::endl;
        std::cout << " --> total time used (secs) = " << total_time << std::endl;
    }

    auto set_res(const double &res) -> void {
        _res = res;
    }

private:

    double _res{};
    decltype(std::chrono::steady_clock::now()) t1{};
    decltype(std::chrono::steady_clock::now()) t2{};
    mutable decltype(std::chrono::steady_clock::now()) t_prev{};
};


#ifdef USE_THR

void multiply3(int msize, int tidx, int numt, TYPE a[][NUM], TYPE b[][NUM], TYPE c[][NUM], TYPE t[][NUM])
{
    int i,j,k,i0,j0,k0,ibeg,ibound,istep,mblock;

// Step3: Cache blocking
// Add current platform optimization for Windows: /QxHost Linux: -xHost
// Define the ALIGNED in the preprocessor definitions and compile option Windows: /Oa Linux: -fno-alias
    istep = msize / numt;
    ibeg = tidx * istep;
    ibound = ibeg + istep;
    mblock = MATRIX_BLOCK_SIZE;

    for (i0 = ibeg; i0 < ibound; i0 +=mblock) {
        for (k0 = 0; k0 < msize; k0 += mblock) {
            for (j0 =0; j0 < msize; j0 += mblock) {
                for (i = i0; i < i0 + mblock; i++) {
                    for (k = k0; k < k0 + mblock; k++) {
#pragma ivdep
#ifdef ALIGNED
#pragma vector aligned
#endif //ALIGNED
                        for (j = j0; j < j0 + mblock; j++) {
                            c[i][j]  = c[i][j] + a[i][k] * b[k][j];
                        }
                    }
                }
            }
        }
    }
}

void multiply4(int msize, int tidx, int numt, TYPE a[][NUM], TYPE b[][NUM], TYPE c[][NUM], TYPE t[][NUM])
{
    int i,j,k,istep,ibeg,ibound;
//transpose b
    for(i=0;i<msize;i++) {
        for(k=0;k<msize;k++) {
        t[i][k] = b[k][i];
        }
    }

    istep = msize / numt;
    ibeg = tidx * istep;
    ibound = ibeg + istep;
/*  for(i=0;i<msize;i+=4) { // use instead for single threaded impl.*/
    for(i=ibeg;i<ibound;i+=4) {
        for(j=0;j<msize;j+=4) {
#pragma loop count (NUM)
#pragma ivdep
            for(k=0;k<msize;k++) {
                c[i][j] = c[i][j] + a[i][k] * t[j][k];
                c[i+1][j] = c[i+1][j] + a[i+1][k] * t[j][k];
                c[i+2][j] = c[i+2][j] + a[i+2][k] * t[j][k];
                c[i+3][j] = c[i+3][j] + a[i+3][k] * t[j][k];

                c[i][j+1] = c[i][j+1] + a[i][k] * t[j+1][k];
                c[i+1][j+1] = c[i+1][j+1] + a[i+1][k] * t[j+1][k];
                c[i+2][j+1] = c[i+2][j+1] + a[i+2][k] * t[j+1][k];
                c[i+3][j+1] = c[i+3][j+1] + a[i+3][k] * t[j+1][k];

                c[i][j+2] = c[i][j+2] + a[i][k] * t[j+2][k];
                c[i+1][j+2] = c[i+1][j+2] + a[i+1][k] * t[j+2][k];
                c[i+2][j+2] = c[i+2][j+2] + a[i+2][k] * t[j+2][k];
                c[i+3][j+2] = c[i+3][j+2] + a[i+3][k] * t[j+2][k];

                c[i][j+3] = c[i][j+3] + a[i][k] * t[j+3][k];
                c[i+1][j+3] = c[i+1][j+3] + a[i+1][k] * t[j+3][k];
                c[i+2][j+3] = c[i+2][j+3] + a[i+2][k] * t[j+3][k];
                c[i+3][j+3] = c[i+3][j+3] + a[i+3][k] * t[j+3][k];
              }
        }
    }
}
#endif // USE_THR

#ifdef USE_MKL
// DGEMM way of matrix multiply using Intel MKL
// Link with Intel MKL library: With MSFT VS and Intel Composer integration: Select build components in the Project context menu.
// For command line - check out the Intel Math Kernel Library Link Line Advisor
void multiply5(int msize, int tidx, int numt, TYPE a[][NUM], TYPE b[][NUM], TYPE c[][NUM], TYPE t[][NUM])
{

    double alpha = 1.0, beta = 0.;
    cblas_dgemm(CblasRowMajor,CblasNoTrans,CblasNoTrans,NUM,NUM,NUM,alpha,(const double *)b,NUM,(const double *)a,NUM,beta,(double *)c,NUM);
}
#endif //USE_MKL

void build_matrices(size_t N,
                    size_t M,
                    size_t K,
                    double **mat1,
                    double **mat2,
                    double **mat3) {

    for (size_t i = 0; i < N; i++) {
        for (size_t j = 0; j < M; j++) {
            mat1[i][j] = double(i) * M + j + 1;
        }
    }

    for (size_t i = 0; i < M; i++) {
        for (size_t j = 0; j < K; j++) {
            mat2[i][j] = double(i) * K + j + 1;
        }
    }

    for (size_t i = 0; i < N; i++) {
        for (size_t j = 0; j < K; j++) {
            mat3[i][j] = 0.0;
        }
    }
}

void mat_mult1(size_t N,
               size_t M,
               size_t K,
               double **mat1,
               double **mat2,
               double **mat3) {

    // naive implementation

    for (size_t i1 = 0; i1 < N; i1++) {
        for (size_t i2 = 0; i2 < M; i2++) {
            for (size_t i3 = 0; i3 < K; i3++) {
                mat3[i1][i3] = mat3[i1][i3] + mat1[i1][i2] * mat2[i2][i3];
            }
        }
    }
}

void mat_mult2(size_t N,
               size_t M,
               size_t K,
               double **mat1,
               double **mat2,
               double **mat3) {

    // naive implementation + openmp parallelization
#pragma omp parallel for\
            default(none)\
            collapse (2)\
            num_threads(4)\
            shared(mat1, mat2, mat3)\
            shared(N, M, K)
    for (size_t i1 = 0; i1 < N; i1++) {
        for (size_t i2 = 0; i2 < M; i2++) {
            for (size_t i3 = 0; i3 < K; i3++) {
                mat3[i1][i3] = mat3[i1][i3] + mat1[i1][i2] * mat2[i2][i3];
            }
        }
    }
}

auto main() -> int {

    constexpr auto N{(size_t) 4'000};
    constexpr auto M{(size_t) N};
    constexpr auto K{(size_t) N};

    std::cout << std::fixed;
    std::cout << std::setprecision(10);

    {
        std::cout << "----------------------------------------------->> 1" << std::endl;
        std::cout << "-->> mult1 --> naive implementation" << std::endl;

        auto ot{benchmark_timer(0)};

        double **mat1{nullptr};
        double **mat2{nullptr};
        double **mat3{nullptr};

        mat1 = new double *[N];
        for (size_t i = 0; i < N; i++) {
            mat1[i] = new double[M];
        }

        mat2 = new double *[M];
        for (size_t i = 0; i < M; i++) {
            mat2[i] = new double[K];
        }

        mat3 = new double *[N];
        for (size_t i = 0; i < N; i++) {
            mat3[i] = new double[K];
        }

        std::cout << "-->> building the matrices" << std::endl;
        build_matrices(N, M, K, mat1, mat2, mat3);

        
        ot.print_time();

        std::cout << "-->> multiplying the matrices" << std::endl;
        mat_mult1(N, M, K, mat1, mat2, mat3);

        ot.print_time();

        std::cout << "-->> mat3[0][0] = " << mat3[0][0] << std::endl;
    }

    {
        std::cout << "----------------------------------------------->> 1" << std::endl;
        std::cout << "-->> mult2 --> naive implementation + OpenMP" << std::endl;

        auto ot{benchmark_timer(0)};

        double **mat1{nullptr};
        double **mat2{nullptr};
        double **mat3{nullptr};

        mat1 = new double *[N];
        for (size_t i = 0; i < N; i++) {
            mat1[i] = new double[M];
        }

        mat2 = new double *[M];
        for (size_t i = 0; i < M; i++) {
            mat2[i] = new double[K];
        }

        mat3 = new double *[N];
        for (size_t i = 0; i < N; i++) {
            mat3[i] = new double[K];
        }

        std::cout << "-->> building the matrices" << std::endl;
        build_matrices(N, M, K, mat1, mat2, mat3);

        ot.print_time();
        
        mat_mult2(N, M, K, mat1, mat2, mat3);
        
        ot.print_time();

        std::cout << "-->> mat3[0][0] = " << mat3[0][0] << std::endl;
    }
}
