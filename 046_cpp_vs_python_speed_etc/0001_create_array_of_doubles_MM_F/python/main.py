import array
import time

dim_max = 10000000


def f1(dim):
    a1 = array.array('d', [])
    a2 = array.array('d', [])

    for x in range(0, dim_max):
        a1.append(x)
        a2.append(x)

    a3 = array.array('d', [])

    for x in range(0, dim_max):
        a3.append(a1[x] + a2[x])
        a3.append(a1[x] + a2[x])
        a3.append(a1[x] + a2[x])

    return a3[10]


t0 = time.time()
res = f1(dim_max)
t1 = time.time()
print("res =", "{0:.10f}".format(res))
print("time used =", "{0:.10f}".format(t1 - t0))
