#include <iostream>
#include <chrono>
#include <vector>
#include <iomanip>

double f1(size_t dim_max) {

    auto a1{std::vector<double>()};
    auto a2{std::vector<double>()};

    for (size_t i = 0; i < dim_max; i++) {
        a1.emplace_back((double) i);
        a2.emplace_back((double) i);
    }

    auto a3{std::vector<double>()};

    for (size_t i = 0; i < dim_max; i++) {
        a3.emplace_back(a1[i] + a2[i]);
        a3.emplace_back(a1[i] + a2[i]);
        a3.emplace_back(a1[i] + a2[i]);
    }

    return a3[10];
}

auto main() -> int {

    std::cout << std::fixed;
    std::cout << std::setprecision(10);

    const auto dim_max{(size_t) 10'000'000};

    const auto t0{std::chrono::steady_clock::now()};

    const auto res{f1(dim_max)};

    const auto t1{std::chrono::steady_clock::now()};

    std::cout << " res = " << res << std::endl;
    
    std::cout << " time used = "
              << std::chrono::duration_cast<std::chrono::duration<double>>(t1 - t0).count()
              << std::endl;
}
