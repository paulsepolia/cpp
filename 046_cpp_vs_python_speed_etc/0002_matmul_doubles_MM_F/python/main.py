import numpy as np
import time


def np_dot_matmul(n, m, k):
    t0 = time.time()

    mat1 = np.array(np.arange(1.0, n * m + 1).reshape((n, m)))
    mat2 = np.array(np.arange(1.0, m * k + 1).reshape((m, k)))

    t1 = time.time()

    print("-->> mat1[0, 0] =", "{0:.10f}".format(mat1[0, 0]))
    print("-->> mat2[0, 1] =", "{0:.10f}".format(mat2[0, 1]))
    print("-->> mat1[n-1, m-1] =", "{0:.10f}".format(mat1[n - 1, m - 1]))
    print("-->> mat2[m-1, k-1] =", "{0:.10f}".format(mat2[m - 1, k - 1]))
    print("-->> time used to build the matrices =", "{0:.10f}".format(t1 - t0))

    t0 = time.time()

    mat3 = np.matmul(mat1, mat2)

    t1 = time.time()

    print("-->> mat3[0, 0] =", "{0:.10f}".format(mat3[0, 0]))
    print("-->> mat3[1, 1] =", "{0:.10f}".format(mat3[1, 1]))
    print("-->> time used to np.matmul the matrices =", "{0:.10f}".format(t1 - t0))

    return mat3[0, 0]


def np_dot_dot(n, m, k):
    t0 = time.time()

    mat1 = np.array(np.arange(1.0, n * m + 1).reshape((n, m)))
    mat2 = np.array(np.arange(1.0, m * k + 1).reshape((m, k)))

    t1 = time.time()

    print("-->> mat1[0, 0] =", "{0:.10f}".format(mat1[0, 0]))
    print("-->> mat2[0, 1] =", "{0:.10f}".format(mat2[0, 1]))
    print("-->> mat1[n-1, m-1] =", "{0:.10f}".format(mat1[n - 1, m - 1]))
    print("-->> mat2[m-1, k-1] =", "{0:.10f}".format(mat2[m - 1, k - 1]))
    print("-->> time used to build the matrices =", "{0:.10f}".format(t1 - t0))

    t0 = time.time()

    mat3 = np.dot(mat1, mat2)

    t1 = time.time()

    print("-->> mat3[0, 0] =", "{0:.10f}".format(mat3[0, 0]))
    print("-->> mat3[1, 1] =", "{0:.10f}".format(mat3[1, 1]))
    print("-->> time used to np.matmul the matrices =", "{0:.10f}".format(t1 - t0))

    return mat3[0, 0]


def np_dot_intel(n, m, k):
    t0 = time.time()

    a1 = np.array(np.arange(1.0, n * m + 1).reshape((n, m)), dtype=np.double, order='C', copy=False)
    a2 = np.array(np.arange(1.0, n * m + 1).reshape((n, m)), dtype=np.double, order='C', copy=False)

    mat1 = np.array(a1, dtype=np.double, copy=False)
    mat2 = np.array(a2, dtype=np.double, copy=False)

    t1 = time.time()

    print("-->> mat1[0, 0] =", "{0:.10f}".format(mat1[0, 0]))
    print("-->> mat2[0, 1] =", "{0:.10f}".format(mat2[0, 1]))
    print("-->> mat1[n-1, m-1] =", "{0:.10f}".format(mat1[n - 1, m - 1]))
    print("-->> mat2[m-1, k-1] =", "{0:.10f}".format(mat2[m - 1, k - 1]))
    print("-->> time used to build the matrices =", "{0:.10f}".format(t1 - t0))

    t0 = time.time()

    mat3 = np.dot(mat1, mat2)

    t1 = time.time()

    print("-->> mat3[0, 0] =", "{0:.10f}".format(mat3[0, 0]))
    print("-->> mat3[1, 1] =", "{0:.10f}".format(mat3[1, 1]))
    print("-->> time used to np.matmul the matrices =", "{0:.10f}".format(t1 - t0))

    return mat3[0, 0]


def main():
    n = m = k = 4000

    print("---------------------------------------------------------->> 1")
    res = np_dot_matmul(n, m, k)

    print("-->> res =", res)

    print("---------------------------------------------------------->> 2")
    res = np_dot_dot(n, m, k)

    print("-->> res =", res)

    print("---------------------------------------------------------->> 3")
    res = np_dot_intel(n, m, k)

    print("-->> res =", res)

    return


# ================ #
# execution part   #
# ================ #

main()
