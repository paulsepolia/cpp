#include <iostream>
#include <chrono>
#include <iomanip>
#include <omp.h>

class benchmark_timer {
public:

    explicit benchmark_timer(const double &res) :
            _res(res),
            t1{std::chrono::steady_clock::now()},
            t2{std::chrono::steady_clock::now()},
            t_prev{std::chrono::steady_clock::now()} {};

    auto print_time() const -> void {
        const auto t_loc{std::chrono::steady_clock::now()};
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t_loc - t_prev).count()};
        t_prev = t_loc;
        std::cout << " --> time used (secs) = " << total_time << std::endl;
    }

    auto reset_timer() -> void {
        const auto t_loc{std::chrono::steady_clock::now()};
        t_prev = t_loc;
        t1 = t_loc;
        t2 = t_loc;
    }

    ~benchmark_timer() {
        t2 = std::chrono::steady_clock::now();
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count()};
        std::cout << " --> res = " << _res << std::endl;
        std::cout << " --> total time used (secs) = " << total_time << std::endl;
    }

    auto set_res(const double &res) -> void {
        _res = res;
    }

private:

    double _res{};
    decltype(std::chrono::steady_clock::now()) t1{};
    decltype(std::chrono::steady_clock::now()) t2{};
    mutable decltype(std::chrono::steady_clock::now()) t_prev{};
};


void build_matrices(size_t N,
                    size_t M,
                    size_t K,
                    double **mat1,
                    double **mat2,
                    double **mat3) {

    for (size_t i = 0; i < N; i++) {
        for (size_t j = 0; j < M; j++) {
            mat1[i][j] = double(i) * M + j + 1;
        }
    }

    for (size_t i = 0; i < M; i++) {
        for (size_t j = 0; j < K; j++) {
            mat2[i][j] = double(i) * K + j + 1;
        }
    }

    for (size_t i = 0; i < N; i++) {
        for (size_t j = 0; j < K; j++) {
            mat3[i][j] = 0.0;
        }
    }
}

void mat_mult1(size_t N,
               size_t M,
               size_t K,
               double **mat1,
               double **mat2,
               double **mat3) {

    // naive implementation

    for (size_t i1 = 0; i1 < N; i1++) {
        for (size_t i2 = 0; i2 < M; i2++) {
            for (size_t i3 = 0; i3 < K; i3++) {
                mat3[i1][i3] = mat3[i1][i3] + mat1[i1][i2] * mat2[i2][i3];
            }
        }
    }
}

void mat_mult2(size_t N,
               size_t M,
               size_t K,
               double **mat1,
               double **mat2,
               double **mat3) {

    // naive implementation + openmp parallelization
#pragma omp parallel for\
            default(none)\
            collapse (2)\
            num_threads(4)\
            shared(mat1, mat2, mat3)\
            shared(N, M, K)
    for (size_t i1 = 0; i1 < N; i1++) {
        for (size_t i2 = 0; i2 < M; i2++) {
            for (size_t i3 = 0; i3 < K; i3++) {
                mat3[i1][i3] = mat3[i1][i3] + mat1[i1][i2] * mat2[i2][i3];
            }
        }
    }
}

auto main() -> int {

    constexpr auto N{(size_t) 4'000};
    constexpr auto M{(size_t) N};
    constexpr auto K{(size_t) N};

    std::cout << std::fixed;
    std::cout << std::setprecision(10);

    {
        std::cout << "----------------------------------------------->> 1" << std::endl;
        std::cout << "-->> mult1 --> naive implementation" << std::endl;

        auto ot{benchmark_timer(0)};

        double **mat1{nullptr};
        double **mat2{nullptr};
        double **mat3{nullptr};

        mat1 = new double *[N];
        for (size_t i = 0; i < N; i++) {
            mat1[i] = new double[M];
        }

        mat2 = new double *[M];
        for (size_t i = 0; i < M; i++) {
            mat2[i] = new double[K];
        }

        mat3 = new double *[N];
        for (size_t i = 0; i < N; i++) {
            mat3[i] = new double[K];
        }

        std::cout << "-->> building the matrices" << std::endl;
        build_matrices(N, M, K, mat1, mat2, mat3);

        
        ot.print_time();

        std::cout << "-->> multiplying the matrices" << std::endl;
        mat_mult1(N, M, K, mat1, mat2, mat3);

        ot.print_time();

        std::cout << "-->> mat3[0][0] = " << mat3[0][0] << std::endl;
        std::cout << "-->> mat3[1][1] = " << mat3[1][1] << std::endl;
    }

    {
        std::cout << "----------------------------------------------->> 1" << std::endl;
        std::cout << "-->> mult2 --> naive implementation + OpenMP" << std::endl;

        auto ot{benchmark_timer(0)};

        double **mat1{nullptr};
        double **mat2{nullptr};
        double **mat3{nullptr};

        mat1 = new double *[N];
        for (size_t i = 0; i < N; i++) {
            mat1[i] = new double[M];
        }

        mat2 = new double *[M];
        for (size_t i = 0; i < M; i++) {
            mat2[i] = new double[K];
        }

        mat3 = new double *[N];
        for (size_t i = 0; i < N; i++) {
            mat3[i] = new double[K];
        }

        std::cout << "-->> building the matrices" << std::endl;
        build_matrices(N, M, K, mat1, mat2, mat3);

        ot.print_time();
        
        mat_mult2(N, M, K, mat1, mat2, mat3);
        
        ot.print_time();

        std::cout << "-->> mat3[0][0] = " << mat3[0][0] << std::endl;
        std::cout << "-->> mat3[1][1] = " << mat3[1][1] << std::endl;
    }
}
