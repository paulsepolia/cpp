#include <iostream>
#include <chrono>
#include <thread>
#include "ASingleton.h"
#include "BSingleton.h"

auto main() -> int {

	{
		std::cout << "----------------------------------->> 1" << std::endl;

		ASingleton::getInstance().PrintTimeUsed();
		ASingleton::getInstance().PrintTotalTimeUsed();

		std::this_thread::sleep_for(std::chrono::seconds(2));

		ASingleton::getInstance().PrintTimeUsed();
		ASingleton::getInstance().PrintTotalTimeUsed();

		std::this_thread::sleep_for(std::chrono::seconds(3));

		ASingleton::getInstance().PrintTimeUsed();
		ASingleton::getInstance().PrintTotalTimeUsed();

		std::this_thread::sleep_for(std::chrono::seconds(4));

		ASingleton::getInstance().PrintTimeUsed();
		ASingleton::getInstance().PrintTotalTimeUsed();

		std::this_thread::sleep_for(std::chrono::seconds(5));
	}

	{
		std::cout << "----------------------------------->> 2" << std::endl;

		BSingleton::getInstance()->PrintTimeUsed();
		BSingleton::getInstance()->PrintTotalTimeUsed();

		std::this_thread::sleep_for(std::chrono::seconds(2));

		BSingleton::getInstance()->PrintTimeUsed();
		BSingleton::getInstance()->PrintTotalTimeUsed();

		std::this_thread::sleep_for(std::chrono::seconds(3));

		BSingleton::getInstance()->PrintTimeUsed();
		BSingleton::getInstance()->PrintTotalTimeUsed();

		std::this_thread::sleep_for(std::chrono::seconds(4));

		BSingleton::getInstance()->PrintTimeUsed();
		BSingleton::getInstance()->PrintTotalTimeUsed();

		std::this_thread::sleep_for(std::chrono::seconds(5));
	}
}
