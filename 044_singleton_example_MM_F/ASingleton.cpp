#include <chrono>
#include <iomanip>
#include <iostream>
#include "ASingleton.h"

auto ASingleton::getInstance() -> ASingleton&
{
	static auto anInstance = ASingleton();
	return anInstance;
}

auto ASingleton::PrintTimeUsed() const -> void {

	const auto t_loc { std::chrono::steady_clock::now() };

	const auto total_time { std::chrono::duration_cast<
			std::chrono::duration<double>>(t_loc - _t_prev).count() };

	_t_prev = t_loc;

	std::cout.precision(5);
	std::cout << std::fixed;
	std::cout << " --> time used (seconds) = " << total_time << std::endl;
}

auto ASingleton::PrintTotalTimeUsed() const -> void {

	const auto t_loc { std::chrono::steady_clock::now() };

	const auto total_time { std::chrono::duration_cast<
			std::chrono::duration<double>>(t_loc - _t1).count() };

	std::cout.precision(5);
	std::cout << std::fixed;
	std::cout << " --> total time used up to now (seconds) = " << total_time
			<< std::endl;
}

ASingleton::~ASingleton() {

	_t2 = std::chrono::steady_clock::now();

	const auto total_time { std::chrono::duration_cast<
			std::chrono::duration<double>>(_t2 - _t1).count() };

	std::cout.precision(5);
	std::cout << std::fixed;
	std::cout << " --> total time used until exit of the scope (seconds) = "
			<< total_time << std::endl;
}

ASingleton::ASingleton() :
		_t1 { std::chrono::steady_clock::now() }, _t2 {
				std::chrono::steady_clock::now() }, _t_prev {
				std::chrono::steady_clock::now() } {
}

