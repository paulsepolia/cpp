#!/bin/bash

  g++   -O3        \
        -Wall      \
        -std=c++11 \
        -pthread   \
        main.cpp   \
        -o x_gnu

  valgrind --leak-check=full --show-leak-kinds=all  ./x_gnu
