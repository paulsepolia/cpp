#include <iostream>
#include <chrono>
#include <thread>

int main() {

    const uint32_t I_MAX(10000);
    const uint32_t DIM(100000);

    for (uint32_t i = 0; i != I_MAX; i++) {

        std::cout << "------------->> i = " << i << std::endl;

        double *vec = new double[DIM];
        for (uint32_t j = 0; j != DIM; j++) {
            vec[j] = static_cast<double>(j);
        }
        std::this_thread::sleep_for(std::chrono::seconds(20));
        //delete [] vec;
    }

}
