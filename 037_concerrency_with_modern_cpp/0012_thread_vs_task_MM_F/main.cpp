// asyncVersusThread.cpp

#include <future>
#include <thread>
#include <iostream>
#include <chrono>

int main() {

    std::cout << std::endl;

    int res;
    std::thread t([&] {

        std::cout << " --> sleeping thread..." << std::endl;

        std::this_thread::sleep_for(std::chrono::seconds(10));

        res = 2000 + 11;
    });

    std::cout << " --> before joining..." << std::endl;

    t.join();

    std::cout << " --> res: " << res << std::endl;

    auto fut = std::async([] {

        std::cout << " --> sleeping async..." << std::endl;

        std::this_thread::sleep_for(std::chrono::seconds(10));

        return 2000 + 11;
    });

    std::cout << " --> fut.get(): " << fut.get() << std::endl;

    std::cout << std::endl;
}