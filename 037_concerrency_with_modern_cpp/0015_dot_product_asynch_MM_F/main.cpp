
// dotProductAsync.cpp

#include <chrono>
#include <iostream>
#include <future>
#include <random>
#include <vector>
#include <numeric>
#include <iomanip>

static const uint64_t NUM = static_cast<uint64_t>(std::pow(10.0, 7.0));
static const uint64_t DO_MAX = static_cast<uint64_t>(std::pow(10.0, 3.0));

long double getDotProduct(std::vector<long double> &v, std::vector<long double> &w) {

    auto future1 = std::async([&] {
        return std::inner_product(&v[0], &v[v.size() / 4], &w[0], 0.0L);
    });

    auto future2 = std::async([&] {
        return std::inner_product(&v[v.size() / 4], &v[v.size() / 2], &w[v.size() / 4], 0.0L);
    });

    auto future3 = std::async([&] {
        return std::inner_product(&v[v.size() / 2], &v[v.size() * 3 / 4], &w[v.size() / 2], 0.0L);
    });

    auto future4 = std::async([&] {
        return std::inner_product(&v[v.size() * 3 / 4], &v[v.size()], &w[v.size() * 3 / 4], 0.0L);
    });

    return future1.get() + future2.get() + future3.get() + future4.get();
}

int main() {

    std::cout << std::endl;

    // get NUM random numbers from 0 .. 100
    std::random_device r;
    std::seed_seq seed{r()};

    // generator
    std::mt19937 engine(seed);

    // distribution
    std::uniform_real_distribution<long double> dist(0, 1);

    // fill the vectors
    std::vector<long double> v, w;

    v.reserve(NUM);
    w.reserve(NUM);

    for (uint64_t i = 0; i < NUM; ++i) {
        v.push_back(dist(engine));
        w.push_back(dist(engine));
    }

    // measure the execution time
    const auto start = std::chrono::system_clock::now();

    long double sum = 0.0L;

    for (uint64_t k = 0; k < DO_MAX; k++) {
        sum += getDotProduct(v, w);
    }

    std::cout << std::fixed;
    std::cout << std::setprecision(10);

    std::cout << sum << std::endl;
    std::chrono::duration<double> dur = std::chrono::system_clock::now() - start;
    std::cout << "Parallel Execution: " << dur.count() << std::endl;

    std::cout << std::endl;
}