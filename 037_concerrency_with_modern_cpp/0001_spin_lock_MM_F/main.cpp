// spin_lock.cpp

#include <atomic>
#include <thread>

class Spinlock {

    std::atomic_flag flag = ATOMIC_FLAG_INIT;

public:

    void lock() {
        while (flag.test_and_set());
    }

    void unlock() {
        flag.clear();
    }
};

Spinlock spin;

void workOnResource() {
    spin.lock();
    // shared resource
    spin.unlock();
}


int main() {

    std::thread t1(workOnResource);
    std::thread t2(workOnResource);

    t1.join();
    t2.join();
}