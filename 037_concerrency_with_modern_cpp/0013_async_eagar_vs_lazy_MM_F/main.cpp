// asyncLazy.cpp

#include <chrono>
#include <future>
#include <iostream>

int main() {

    std::cout << std::endl;

    const auto t1 = std::chrono::system_clock::now();

    auto async_lazy = std::async(std::launch::deferred, [] { return std::chrono::system_clock::now(); });

    auto async_eager = std::async(std::launch::async, [] { return std::chrono::system_clock::now(); });

    std::this_thread::sleep_for(std::chrono::seconds(5));

    const auto lazy_start = async_lazy.get() - t1;
    const auto eager_start = async_eager.get() - t1;

    const auto lazy_duration = std::chrono::duration<double>(lazy_start).count();
    const auto eager_duration = std::chrono::duration<double>(eager_start).count();

    std::cout << "async_lazy evaluated after : " << lazy_duration << " seconds." << std::endl;
    std::cout << "async_eager evaluated after: " << eager_duration << " seconds." << std::endl;

    std::cout << std::endl;
}