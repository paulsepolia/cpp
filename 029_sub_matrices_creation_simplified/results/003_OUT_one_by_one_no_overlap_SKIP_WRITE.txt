rows = 64; // input matrix dimension
columns = 60000; // input matrix dimension
K_MAX = 10000; // number to execute the experiment
SUB_NUM = 100; // number of submatrices to create
offset = 200; // offset for each submatrix
columns_sub = 200; // number of columns for each submatrix

--> total time = 1.57755
--> sub-matrices creation per sec = 1.57755e-06 
