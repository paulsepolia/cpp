#include <iostream>
#include <chrono>
#include <vector>
#include <cmath>
#include <random>
#include <algorithm>
#include <cstring>

typedef short ELEM_TYPE;

std::vector<ELEM_TYPE> create_matrix(const uint64_t &rows,
                                     const uint64_t &columns) {

    std::vector<ELEM_TYPE> vd;
    vd.resize(rows * columns);

    for (uint64_t i = 0; i < vd.size(); i++) {
        vd[i] = static_cast<ELEM_TYPE>(i);
    }

    return vd;
}

std::vector<ELEM_TYPE> create_submatrix_from_matrix(const std::vector<ELEM_TYPE> &matrix_in,
                                                    const uint64_t &rows,
                                                    const uint64_t &columns,
                                                    const uint64_t &columns_sub,
                                                    const uint64_t &offset) {

    std::vector<ELEM_TYPE> matrix_out;
    matrix_out.resize(rows * columns_sub);

    double sum = 0;

    for (uint64_t i = 0; i < rows; i++) {
        for (uint64_t j = 0; j < columns_sub; j++) {

            sum += matrix_in[i * columns + offset + j];
        }
    }

    return matrix_out;
}


void shuffle_matrix(std::vector<ELEM_TYPE> &matrix) {

    auto seed = std::chrono::system_clock::now().time_since_epoch().count();

    std::shuffle(matrix.begin(), matrix.end(), std::default_random_engine(seed));
}

int main() {

    const uint64_t K_MAX = 10000;
    const uint64_t SUBS_NUM = 100;
    double total_time = 0;

    for (uint64_t k = 0; k < K_MAX; k++) {

        // parameters

        const auto rows = static_cast<uint64_t>(64);
        const auto columns = static_cast<uint64_t>(60000);

        std::vector<uint64_t> columns_sub;
        columns_sub.resize(SUBS_NUM);

        std::vector<uint64_t> off_sets;
        off_sets.resize(SUBS_NUM);

        std::random_device rd; // obtain a random number from hardware
        std::mt19937 eng(rd()); // seed the generator

        std::uniform_int_distribution<> distr_columns(200, 200);
        std::uniform_int_distribution<> distr_offsets(200, 200);

        for (uint64_t i = 0; i < SUBS_NUM; i++) {
            columns_sub[i] = static_cast<uint64_t>(distr_columns(eng));
        }

        for (uint64_t i = 0; i < SUBS_NUM; i++) {
            off_sets[i] = i * static_cast<uint64_t>(distr_offsets(eng));
        }

        std::cout << std::boolalpha;

        // create matrix

        std::vector<ELEM_TYPE> matrix_in = create_matrix(rows, columns);

        // many sub-matrices

        std::vector<std::vector<ELEM_TYPE>> sub_matrices;

        // shuffle the input matrix

        shuffle_matrix(matrix_in);

        // create many sub-matrices

        sub_matrices.clear();

        auto t1 = std::chrono::high_resolution_clock::now();

        for (uint64_t j = 0; j < SUBS_NUM; j++) {

            std::vector<ELEM_TYPE> matrix_sub = create_submatrix_from_matrix(matrix_in,
                                                                             rows,
                                                                             columns,
                                                                             columns_sub[j],
                                                                             off_sets[j]);

            sub_matrices.push_back(std::move(matrix_sub));
        }

        auto t2 = std::chrono::high_resolution_clock::now();

        const auto time_to_create_sub_matrices =
                std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);

        total_time = total_time + time_to_create_sub_matrices.count();
    }

    std::cout << " --> total time = " << total_time << std::endl;
    std::cout << " --> sub-matrices creation per sec = " << total_time / K_MAX / SUBS_NUM << std::endl;
}
