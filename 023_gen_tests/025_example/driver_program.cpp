#include <iostream>

class A {

public:

    virtual void print_fun() {
        std::cout << " -->> A" << std::endl;
    }
};

class B : public A {

public:

    void print_fun() {
        std::cout << " -->> B" << std::endl;
    }
};

class C : public B {

public:

    void print_fun() {
        std::cout << " -->> C" << std::endl;
    }
};


int main() {

    A a;
    std::cout << " -->> a.print_fun(); " << std::endl;
    a.print_fun();

    B b;
    std::cout << " -->> b.print_fun(); " << std::endl;
    b.print_fun();

    C c;
    std::cout << " -->> c.print_fun(); " << std::endl;
    c.print_fun();

    A * pa = &c;

    std::cout << " -->> pa = &c; pa->print_fun(); " << std::endl;
    pa->print_fun();

    pa = &b;
    std::cout << " -->> pa = &b;  pa->print_fun(); " << std::endl;
    pa->print_fun();

    pa = &a;
    std::cout << " -->> pa = &a;  pa->print_fun(); " << std::endl;
    pa->print_fun();

    return 0;
}
