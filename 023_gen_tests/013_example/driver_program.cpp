#include <iostream>

class A {
public:
    virtual void fun() { std::cout << "A::fun() " << std::endl; }
};

class B : public A {
public:
    void fun() { std::cout << "B::fun() " << std::endl; }
};

class C : public B {
public:
    void fun() { std::cout << "C::fun() " << std::endl; }
};

int main() {

    std::cout << " --> 0" << std::endl;
    B *bp0(new C);
    bp0->fun();

    std::cout << " --> 1" << std::endl;
    A * bp1(new A);
    bp1->fun();

    std::cout << " --> 2" << std::endl;
    A * bp2(new B);
    bp2->fun();

    std::cout << " --> 3" << std::endl;
    A * bp3(new C);
    bp3->fun();

    return 0;
}