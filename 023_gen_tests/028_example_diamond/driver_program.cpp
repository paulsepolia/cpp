#include<iostream>

class Person {
    // data members of person
public:
    Person(int x) { std::cout << "Person::Person(int) called" << std::endl; }
};

class Faculty : public Person {
    // data members of Faculty
public:
    Faculty(int x) : Person(x) {
        std::cout << "Faculty::Faculty(int) called" << std::endl;
    }
};

class Student : public Person {
    // data members of Student
public:
    Student(int x) : Person(x) {
        std::cout << "Student::Student(int) called" << std::endl;
    }
};

class TA : public Faculty, public Student {
public:
    TA(int x) : Faculty(x), Student(x) {
        std::cout << "TA::TA(int) called" << std::endl;
    }
};

int main() {
    std::cout << "--------------------------->> 1" << std::endl;
    TA ta1(30);
    std::cout << "--------------------------->> 2" << std::endl;
}