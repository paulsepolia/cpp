// CPP program to illustrate
// Different methods to find length
// of a string

#include <string>
#include <string.h>
#include <iostream>
#include <cstdlib>

int main() {

    // String obj
    std::string str("Pavlos G. Galiatsatos");

    // size of string object using size() method
    std::cout << " --> str.size() --> " << str.size() << std::endl;

    // size of string object using length method
    std::cout << " --> str.length() --> " << str.length() << std::endl;

    // size using old style
    // size of string object using strlen function
    std::cout << " --> strlen(str.c_str()) --> " << strlen(str.c_str()) << std::endl;

    // The constructor of string will set it to the C-style string,
    // which ends at the '\0'

    // size of string object Using while loop
    int i = 0;
    while (str[i] != '\0') {
        ++i;
    }

    std::cout << " --> while loop --> " << i << std::endl;

    // size of string object using for loop
    for (i = 0; str[i] != '\0'; i++) {
    }
    std::cout << " --> for loop --> " << i << std::endl;

    i = 0;
    for(auto it = str.begin(); it != str.end(); it++) {
        i++;
    }

    std::cout << " --> for loop with iterators --> " << i << std::endl;

    return 0;
}