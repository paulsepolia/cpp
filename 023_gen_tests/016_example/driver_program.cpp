#include<iostream>
#include<stdio.h>

int f(int x) {
    return x + 10;
}

//int f(const int x) { // COMPILER ERROR
//    return x + 10;
//}

int f(const int & x) { // IT IS OKAY
    return x + 10;
}

int main() {

    getchar();
    return 0;
}