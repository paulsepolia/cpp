#include<iostream>

class A {
    int x;
public:

    A() {
        std::cout << "--------------->> A()" << std::endl;
        x = 11;
    }

    A(int i) {
        std::cout << "--------------->> A(int)" << std::endl;
        x = i;
    }

    void print() { std::cout << x << std::endl; }
};

class B : virtual public A {
public:
    B() : A(10) { // A(10) is NOT BEING CALLED IN VIRTUAL INHERITANCE
        // ONLY THE DEFAULT CONSTRUCTOR IS BEING CALLED
        std::cout << "--------------->> B" << std::endl;
    }
};

class C : virtual public A {
public:
    C() : A(10) { // A(10) is NOT BEING CALLED IN VIRTUAL INHERITANCE
        // ONLY THE DEFAULT CONSTRUCTOR IS BEING CALLED
        std::cout << "--------------->> C" << std::endl;
    }
};

class D : public B, public C {
};

int main() {
    D d;
    d.print();
    return 0;
}