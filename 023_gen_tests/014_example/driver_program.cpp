#include<iostream>

class Base {
public:
    virtual void show() { std::cout << " --> In Base " << std::endl; }
};

class Derived : public Base {
public:
    void show() { std::cout << " --> In Derived" << std::endl; }
};

int main() {

    Base *bp(new Derived);

    std::cout << " --> 1" << std::endl;
    bp->Base::show();  // Note the use of scope resolution here

    std::cout << " --> 2" << std::endl;
    bp->show();  // Note the use of scope resolution here

    return 0;
}