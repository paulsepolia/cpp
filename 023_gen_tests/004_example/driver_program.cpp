#include <iostream>

class set_print_class {
protected:
    int _x;
public:
    void set_values(const int & x) {
        _x = x;
    }

    virtual void print_values() {
        std::cout << " ---> old -->> " << _x << std::endl;
    }
};

void print_fun(set_print_class * p_base)
{
    p_base->print_values();
}


//=============================================================


class print_plus : public set_print_class {
public:
    void print_values() {
        std::cout << " ---> new -->> " << _x << std::endl;
    }
};

int main() {

    // old

    set_print_class o1;

    o1.set_values(10);
    o1.print_values();
    print_fun(&o1);

    // new

    print_plus n1;

    n1.set_values(20);
    n1.print_values();
    print_fun(&n1);

    return 0;
}



