#include <iostream>

class A {

public:

    virtual void print_fun() {
        std::cout << " -->> A" << std::endl;
    }
};

class B : public A {

public:

    virtual void print_fun() {
        std::cout << " -->> B" << std::endl;
    }
};


int main() {

    A a;
    std::cout << " -->> a.print_fun(); " << std::endl;
    a.print_fun();

    B b;
    std::cout << " -->> b.print_fun(); " << std::endl;
    b.print_fun();

    std::cout << " -->> A *pa = &b; " << std::endl;
    A *pa = &b;

    std::cout << " -->> pa->print_fun(); " << std::endl;
    pa->print_fun();

    std::cout << " -->> pa->A::print_fun(); " << std::endl;
    pa->A::print_fun();

    return 0;
}
