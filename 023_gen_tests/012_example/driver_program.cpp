#include <iostream>

class A {
public:
    virtual void fun();
};

class B {
public:
    void fun();
};

int main() {

    int a(sizeof(A));
    int b(sizeof(B));

    std::cout << " --> sizeof(A) = " << a << std::endl;
    std::cout << " --> sizeof(B) = " << b << std::endl;

    if (a == b) {
        std::cout << " --> a == b" << std::endl;
    }

    if (a > b) {
        std::cout << " --> a > b" << std::endl;
    }

    if (a < b) {
        std::cout << " --> a < b" << std::endl;
    }

    return 0;
}