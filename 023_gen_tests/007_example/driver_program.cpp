#include<iostream>

class Base {
public:
    virtual void show() = 0;
};

class Derived : public Base {
};

int main() {
    Derived q; // COMPILER ERROR
    return 0;
}