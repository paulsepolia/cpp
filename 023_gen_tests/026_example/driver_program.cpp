#include <iostream>

class A {

public:

    virtual void print_funA() {
        std::cout << " -->> A" << std::endl;
    }
};

class B : private A {

public:

    void print_funB() {
        print_funA();
    }
};


int main() {

    A a;
    std::cout << " -->> a.print_fun(); " << std::endl;
    a.print_funA();

    B b;
    std::cout << " -->> b.print_funB(); " << std::endl;
    b.print_funB();

    A *pa = &b; // ERROR HERE

    pa->print_funA();

    return 0;
}
