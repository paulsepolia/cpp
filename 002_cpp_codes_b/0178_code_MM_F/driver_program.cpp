//================//
// memcpy example //
//================//

#include <cstdio>
#include <cstring>
#include <iostream>

using std::cout;
using std::endl;

// a struct

struct {
    char name[40000];
    int age;
} person, person_copy;

// the main function

int main()
{
    const char * myname("Pavlos G. Galiatsatos");

    // using memcpy to copy string

    memcpy(person.name, myname, strlen(myname)+1);
    person.age = 46;

    // using memcpy to copy structure

    cout << " --> sizeof(person)      = " << sizeof(person) << " bytes" << endl;
    cout << " --> sizeof(person_copy) = " << sizeof(person_copy) << " bytes" << endl;

    memcpy(&person_copy, &person, sizeof(person));

    cout << " --> person_copy.name = " << person_copy.name << endl;
    cout << " --> person_copy.age  = " << person_copy.age << endl;

    return 0;
}

// end
