//=================//
// dynamic binding //
//=================//

#include <iostream>

using std::cout;
using std::endl;

struct V {

    V()
    {
        cout << " --> V --> constructor" << endl;
    }

    virtual void f()
    {
        cout << " --> V --> f()" << endl;
    };

    virtual ~V()
    {
        cout << " --> ~V --> destructor" << endl;
    }
};

struct A : virtual V {

    A() : V()
    {
        cout << " --> A --> constructor" << endl;
    }

    ~A()
    {
        cout << " --> ~A --> destructor" << endl;
    }
};

struct B : virtual V {

    B(V* v, A* a)
    {

        cout << " --> B --> constructor" << endl;

        // casts during construction (see the call in the constructor of D below)

        dynamic_cast<B*>(v); // well-defined: v of type V*, V base of B, results in B*
        dynamic_cast<B*>(a); // undefined behavior: a has type A*, A not a base of B
    }

    virtual ~B()
    {
        cout << " --> ~B --> destructor" << endl;
    }
};

struct D : A, B {

    D() : A(), B((A*)this, this)
    {
        cout << " --> D --> constructor" << endl;
    }

    virtual ~D()
    {
        cout << " --> ~D --> destructor" << endl;
    }
};

// the main function

int main()
{
    cout << " --> 1" << endl;
    cout << " D d; // the most derived object" << endl;

    D d; // the most derived object

    cout << " --> 2" << endl;
    cout << " A & a1 = d;" << endl;

    A & a1 = d; // upcast, dynamic_cast may be used, but unnecessary

    cout << " --> 3" << endl;
    cout << " A a2;" << endl;

    A a2; // upcast, dynamic_cast may be used, but unnecessary

    cout << " a2 = d;" << endl;

    a2 = d;

    cout << " --> 4" << endl;
    cout << " A a3 = d;" << endl;

    A a3 = d;

    cout << " --> 5" << endl;
    cout << " D & new_d = dynamic_cast<D&>(a1); // downcast" << endl;

    D & new_d = dynamic_cast<D&>(a1); // downcast

    cout << " new_d.f();" << endl;
    new_d.f();

    cout << " --> 6" << endl;
    cout << " B & new_b = dynamic_cast<B&>(a1); // sidecast" << endl;

    B & new_b = dynamic_cast<B&>(a1); // sidecast

    cout << " new_b.f();" << endl;
    new_b.f();

    cout << " --> XX --> LAST" << endl;
}

// end
