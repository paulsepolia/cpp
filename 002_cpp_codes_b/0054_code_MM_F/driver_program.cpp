//======//
// list //
//======//

#include <iostream>
#include <list>
#include <cmath>
#include <ctime>
#include <iomanip>

using std::endl;
using std::cout;
using std::list;
using std::pow;
using std::clock;
using std::fixed;
using std::setprecision;

// type definition

typedef unsigned long int uli;

// the main function

int main()
{
    const auto TRIALS = 10;

    for (auto ik = 0; ik != TRIALS; ik++) {

        cout << "--------------------------------------------------->> "
             << ik+1 << " / " << TRIALS << endl;

        // local parameters

        const uli DIM1 = 5 * uli(pow(10.0, 7.0));
        const uli ELEM = 3UL;

        list<uli> * L1 = new list<uli> [1];
        time_t t1;
        time_t t2;

        // output adjust

        cout << fixed;
        cout << setprecision(10);

        // # 1

        L1[0].assign(DIM1, ELEM);

        cout << " --> L1[0].remove(ELEM);" << endl;

        t1 = clock();

        L1[0].remove(ELEM);

        t2 = clock();

        cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

        // # 2

        L1[0].assign(DIM1, ELEM);

        cout << " --> L1[0].unique();" << endl;
        cout << " --> L1[0].remove(ELEM);" << endl;

        t1 = clock();

        L1[0].unique();
        L1[0].remove(ELEM);

        t2 = clock();

        cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

        // # 3

        cout << " --> L1 contains:";

        for (auto it = L1[0].begin(); it != L1[0].end(); ++it) {
            cout << ' ' << *it;
        }

        cout << endl;
        cout << " --> L1[0].size() = " << L1[0].size() << endl;

        delete [] L1;
    }

    return 0;
}

// end
