//===========================//
// Solution class definition //
//===========================//

#include <algorithm>

using std::find;

#include "Solution.hpp"

// lengthOfLongestSubstringSTL_v1

int Solution::lengthOfLongestSubstringSTL_v1(string str)
{
    // step # 1
    // cover the corner cases

    const int SIZE_STR = static_cast<int>(str.length());

    if(SIZE_STR == 0) {
        return 0;
    }

    if(SIZE_STR == 1) {
        return 1;
    }

    // step # 2
    // starting from the begin, create substrings and
    // try to find the next to the substring element
    // if found stop increasing the size
    // if not continue increasing

    int size_loc(0); // size of the current substring
    int size_max(0); // size of the max substring
    int pos(0);
    string str_help {};

    for(int i = 0; i != SIZE_STR; i++) {
        for(int k = 1; k <= SIZE_STR-i-1; k++) {

            // get the substring
            str_help = str.substr(i,k);

            // try to find in the previous substring
            // the next element to it
            pos = str_help.find(str[i+k]);

            // if found then take the size of the substring
            // and compare it to the max size of the previous substring
            // then break
            if (pos != static_cast<int>(string::npos)) {
                size_loc = k;

                if(size_loc >= size_max) {
                    size_max = size_loc;
                }
                break;
            }
            // if not found then increase the size of the
            // substring by one
            else if(pos == static_cast<int>(string::npos)) {
                size_loc = k+1;
                if(size_loc >= size_max) {
                    size_max = size_loc;
                }
            }
        }
    }

    return size_max;
}

// lengthOfLongestSubstringNoSTL_v1

int Solution::lengthOfLongestSubstringNoSTL_v1(string str)
{
    return 0;
}

//=====//
// end //
//=====//
