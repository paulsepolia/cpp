#!/bin/bash

  # 1. compile

  icpc  -O3                \
        -Wall              \
        -std=c++11         \
	   Solution.cpp       \
        driver_program.cpp \
        -o x_intel
