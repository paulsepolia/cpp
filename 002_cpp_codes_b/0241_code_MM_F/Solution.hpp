//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <string>

using std::string;

class Solution {
public:

    // lengthOfLongestSubstringSTL_v1

    int lengthOfLongestSubstringSTL_v1(string);

    // lengthOfLongestSubstringNoSTL_v1

    int lengthOfLongestSubstringNoSTL_v1(string);
};

#endif

//=====//
// end //
//=====//
