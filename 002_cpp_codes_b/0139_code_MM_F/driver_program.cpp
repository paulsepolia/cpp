//===================//
// function pointers //
//===================//

#include <iostream>

using std::cout;
using std::cin;
using std::endl;

// the main function

int main()
{
    int i1 = 11;
    const int * const p1(&i1);

    cout << " -->  i1 = " <<  i1 << endl;
    cout << " --> &i1 = " << &i1 << endl;
    cout << " -->  p1 = " <<  p1 << endl;
    cout << " --> *p1 = " << *p1 << endl;

    //p1 = NULL;
    //*p1 = 11;

    const int * p2(nullptr);
    p2 = NULL;
    //*p2 = 11;

    return 0;
}
