//===================//
// function pointers //
//===================//

#include <iostream>
#include <typeinfo>
#include <memory>

using std::cout;
using std::endl;
using std::move;

// the main function

int main()
{
    // SCOPE # 1

    {

        void * v1(nullptr);
        int * px1(nullptr);
        int x1 {100};

        px1 = &x1;
        v1 = px1;

        cout << " --> *px1                          = " << *px1 << endl;
        cout << " --> *(static_cast<int*>(v1))      = " << *(static_cast<int*>(v1)) << endl;
        cout << " --> *((int*)(v1))                 = " << *((int*)(v1)) << endl;
        cout << " --> *(reinterpret_cast<int*>(v1)) = " << *(reinterpret_cast<int*>(v1)) << endl;

    }

    // SCOPE # 2

    {
        char c1;

        // # 1

        int x1 = 'a';
        c1 = x1;
        cout << " x1 = " << x1 << endl;
        cout << " c1 = " << c1 << endl;

        // # 2

        x1 = 'A';
        c1 = x1;
        cout << " x1 = " << x1 << endl;
        cout << " c1 = " << c1 << endl;

        // # 3

        x1 = 'A' + 32;
        c1 = x1;
        cout << " x1 = " << x1 << endl;
        cout << " c1 = " << c1 << endl;
    }

    // SCOPE # 3

    {
        const int I_MAX(10000);
        for(int i = 0; i != I_MAX; i++) {
            char c1 = i;
            cout << i << " --> " << c1 << endl;
        }
    }

    return 0;
}

// end
