//=======================//
// sort and keep indexes //
//=======================//

#include <iostream>
#include <vector>
#include <algorithm>

using std::endl;
using std::cout;
using std::vector;
using std::pair;
using std::sort;

// the comparison functor

typedef pair<int,int> mypair;

struct comp_functor {
    bool operator()(const mypair & l, const mypair & r)
    {
        return (l.first < r.first);
    }
};

// the main function

int main()
{
    // local variables

    vector<int> vA( {2, 3, 4, 1, -1});
    pair<int,int> pA;
    vector<pair<int,int>> vB;

    // get the dimension of the vector

    unsigned int DIM = vA.size();

    // build the vector vB with the pairs

    for (unsigned int i = 0; i != DIM; i++) {
        pA.first = vA[i];
        pA.second = i;
        vB.push_back(pA);
    }

    // some output

    cout << " --> vA" << endl;

    cout << " vA[0] = " << vA[0] << endl;
    cout << " vA[1] = " << vA[1] << endl;
    cout << " vA[2] = " << vA[2] << endl;
    cout << " vA[3] = " << vA[3] << endl;
    cout << " vA[4] = " << vA[4] << endl;

    // some output

    cout << " --> vB --> before sorting" << endl;

    cout << " vB[0].first  = " << vB[0].first << endl;
    cout << " vB[0].second = " << vB[0].second << endl;
    cout << " vB[1].first  = " << vB[1].first << endl;
    cout << " vB[1].second = " << vB[1].second << endl;
    cout << " vB[2].first  = " << vB[2].first << endl;
    cout << " vB[2].second = " << vB[2].second << endl;
    cout << " vB[3].first  = " << vB[3].first << endl;
    cout << " vB[3].second = " << vB[3].second << endl;
    cout << " vB[4].first  = " << vB[4].first << endl;
    cout << " vB[4].second = " << vB[4].second << endl;

    // sort the vector B, using the first part of the pair elements

    sort(vB.begin(), vB.end(), comp_functor());

    // some output

    cout << " --> vB --> after sorting" << endl;

    cout << " vB[0].first  = " << vB[0].first << endl;
    cout << " vB[0].second = " << vB[0].second << endl;
    cout << " vB[1].first  = " << vB[1].first << endl;
    cout << " vB[1].second = " << vB[1].second << endl;
    cout << " vB[2].first  = " << vB[2].first << endl;
    cout << " vB[2].second = " << vB[2].second << endl;
    cout << " vB[3].first  = " << vB[3].first << endl;
    cout << " vB[3].second = " << vB[3].second << endl;
    cout << " vB[4].first  = " << vB[4].first << endl;
    cout << " vB[4].second = " << vB[4].second << endl;

    return 0;
}

// END
