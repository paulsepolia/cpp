//=======================//
// use of move symantics //
//=======================//

#include <iostream>
#include <map>
#include <new>
#include <cmath>
#include <iomanip>
#include <chrono>

using std::cout;
using std::endl;
using std::map;
using std::move;
using std::pow;
using std::boolalpha;
using namespace std::chrono;

// type definitions

typedef unsigned long int uli;
typedef const uli culi;

// the main program

int main()
{
    // local parameters

    culi TRIALS(static_cast<culi>(pow(10.0, 5.0)));
    culi DIMEN(static_cast<culi>(pow(10.0, 4.0)));
    cout << boolalpha;
    auto t1 = system_clock::now();
    auto t2 = system_clock::now();
    duration<double> time_span;

    // main game

    map<uli, double> mp1;
    mp1.clear();

    // build map --> mp1

    for(uli i = 0; i != DIMEN; i++) {
        mp1[i] = static_cast<double>(i);
    }

    cout << " mp1.size() = " << mp1.size() << endl;

    map<uli, double> mp2;
    mp2.clear();

    cout << " mp2.size() = " << mp2.size() << endl;

    cout << " mp2 = mp1;" << endl;

    t1 = system_clock::now();

    uli i = 0;
    while(i != TRIALS) {
        mp2 = mp1;
        i++;
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    cout << " i = " << i << endl;
    cout << " mp1.size() = " << mp1.size() << endl;
    cout << " mp2.size() = " << mp2.size() << endl;
    cout << " (mp1 == mp2) = " << (mp1 == mp2) << endl;

    t1 = system_clock::now();

    i = 0;
    while(i != TRIALS) {
        mp2 = move(mp1);
        mp1 = move(mp2);
        i++;
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    cout << " i = " << i << endl;
    cout << " mp1.size() = " << mp1.size() << endl;
    cout << " mp2.size() = " << mp2.size() << endl;
    cout << " (mp1 == mp2) = " << (mp1 == mp2) << endl;

    return 0;
}

// end
