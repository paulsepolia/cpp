//============//
// shared_ptr //
//============//

#include <memory>
#include <iostream>
#include <cmath>

using std::endl;
using std::cout;

typedef unsigned long int uli;

// the main function

int main()
{
    const uli DIM1(std::pow(10.0, 8.0));

    {
        cout << " --> part --> 1 --> start" << endl;
        for(uli i = 0; i != DIM1; i++) {
            std::shared_ptr<double> sp(new double);
            *sp = static_cast<double>(i);
        }
        cout << " --> part --> 1 --> end" << endl;
    }
}

// end
