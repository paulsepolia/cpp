//===============================//
// move example                  //
// and get address of a function //
//===============================//

#include <utility>   // std::move
#include <iostream>  // std::cout

using std::endl;
using std::cout;
using std::move;

// global variable

int x = 10;

// function --> int getInt()

int getInt()
{
    return x;
}

// function --> int && getRvalueInt()

int && getRvalueInt()
{
    // notice that it's fine to move a primitive type--remember, std::move is just a cast
    return move(x);
}

// function --> printAddress(const int & v)

void printAddress(const int & v) // const ref to allow binding to rvalues
{
    cout << reinterpret_cast<const void*>(& v) << endl;
}

// the main function

int main()
{
    cout << " 1 --> &x = " << &x << endl;

    cout << " 2 --> printAddress(getInt()) = ";

    printAddress(getInt());

    cout << " 3 --> printAddress(x) = ";

    printAddress(x);

    cout << " 4 --> printAddress(getRvalueInt()) = ";

    printAddress(getRvalueInt());

    cout << " 5 --> printAddress(x) = ";

    printAddress(x);

    cout << " 6 --> move(&x) = " << move(&x) << endl;

    cout << " 7 --> (void*)(printAddress) = " << (void*)(printAddress) << endl;

    cout << " 8 --> (void*)(getInt)       = " << (void*)(getInt) << endl;

    cout << " 9 --> (void*)(getRvalueInt) = " << (void*)(getRvalueInt) << endl;

    return 0;
}

// end
