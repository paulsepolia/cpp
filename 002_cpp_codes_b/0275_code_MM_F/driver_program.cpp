//=============//
// linked list //
//=============//

#include <cmath>
#include <iostream>
using std::endl;
using std::cout;

#define DEBUG
#undef DEBUG

// list definition

class list {
public:
    int * data1;
    int * data2;
    list * next;
    list * previous;
};

typedef unsigned long int uli;

// the main function

int main()
{
    // local parameters

    const uli DIM0(std::pow(10.0, 2.0));
    const uli DIM1(std::pow(10.0, 1.0));
    const uli DIM2(std::pow(10.0, 1.0));
    const uli TR = 2;//(std::pow(10.0, 0.0));

    list * head(new list);
    list * node(new list);
    for(int k = 0; k != TR; k++) {

        cout << "------------------->> " << k << endl;

        // initialize the head

        //     list * head(new list);
        //     list * node(new list);
        head->next = node;

        // build the nodes

        for(uli i = 0; i != DIM0; i++) {
            node->next = new list;
            node->data1 = new int [DIM1];
            node->data2 = new int [DIM2];
            for(uli j = 0; j != DIM1; j++) {
                node->data1[j] = i;
            }
            for(uli j = 0; j != DIM2; j++) {
                node->data2[j] = i;
            }

            node = node->next;
        }

        // finilize the nodes

        node->next = 0;

        // traverse the nodes

#ifdef DEBUG
        node = head->next;
        while(node->next) {
            cout << node->data1[0] << endl;
            cout << node->data2[0] << endl;
            node = node->next;
        }
#endif

        // delete the nodes and the data elements

        static int i(0);
        node = head->next;
        while(node->next) {
            i++;
            list * q;
            q = node->next;
            node->next = q->next;
            cout << " --> L" << endl;
            delete [] q->data1;
            cout << " --> K" << endl;
            delete [] q->data2;
            cout << " --> M" << endl;
            delete q;
            cout << " --> N" << endl;
            cout << " ----------------------->> i = " << i << endl;
        }

        i = 0;
//		delete [] node->data1;
//		delete [] node->data2;
        //     delete head;
    }

    return 0;
}

// END
