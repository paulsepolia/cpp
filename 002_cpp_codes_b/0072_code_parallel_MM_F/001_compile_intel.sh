#!/bin/bash

  # 1. compile

  icpc -O3                 \
	  -ipo                \
       -Wall               \
	  -wd1202             \
       -std=c++11          \
	  -openmp             \
	  -D_GLIBCXX_PARALLEL \
       driver_program.cpp  \
       -o x_intel
