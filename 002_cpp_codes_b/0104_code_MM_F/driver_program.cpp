//==================//
// INHERITANCE GAME //
//==================//

// 1 --> WHEN YOU HAVE VIRTUAL FUNCTION
//   --> THE DESTRUCTOR MUST BE VIRTUAL
// 2 --> THE DERIVED FUNCTIONS FROM POLYMORPHIC CLASS ARE VIRTUAL
// 3 --> THE DEFIVED DESTRUCTORS FROM POLYMORPHIC CLASS ARE VIRTUAL

#include <iostream>

using std::endl;
using std::cout;

// class A

class A {
public:

    // # 1 --> constructor

    A()
    {
        cout << " --> constructor --> A" << endl;
    }

    // # 2 --> fun

    virtual void fun()
    {
        cout << " --> HELLOW FROM CLASS --> A" << endl;
    }

    // # 3 --> destructor

    virtual ~A()
    {
        cout << " --> destructor --> A" << endl;
    }
};

// class B

class B {
public:

    // # 1 --> constructor

    B()
    {
        cout << " --> constructor --> B" << endl;
    }

    // # 2 --> fun

    virtual void fun()
    {
        cout << " --> HELLOW FROM CLASS --> B" << endl;
    }

    // # 3 --> destructor

    virtual ~B()
    {
        cout << " --> destructor --> B" << endl;
    }
};

// class C

class C: public A, public B {
public:

    // # 1 --> constructor

    C()
    {
        cout << " --> constructor --> C" << endl;
    }

    // # 2 --> fun

    virtual void fun()
    {
        cout << " --> HELLOW FROM CLASS --> C" << endl;
    }

    // # 3 --> destructor

    virtual ~C()
    {
        cout << " --> destructor --> C" << endl;
    }
};

// the main function

int main()
{
    cout << " --> MIDDLE -------------------------------------------> 0" << endl;

    {
        cout << " --> A a1;" << endl;
        A a1;
        cout << " --> B b1;" << endl;
        B b1;
        cout << " --> C c1;" << endl;
        C c1;
        cout << " --> exit --> 1" << endl;
    }

    cout << " --> MIDDLE -------------------------------------------> 1" << endl;

    {
        cout << " --> A a1;" << endl;
        A a1;
        cout << " --> a1.fun();" << endl;
        a1.fun();

        cout << " --> B b1;" << endl;
        B b1;
        cout << " --> b1.fun();" << endl;
        b1.fun();

        cout << " --> C c1;" << endl;
        C c1;
        cout << " --> c1.fun();" << endl;
        c1.fun();
        cout << " --> exit --> 2" << endl;
    }

    cout << " --> MIDDLE -------------------------------------------> 2" << endl;

    {
        cout << " --> C c1;" << endl;
        C c1;
        cout << " --> A a1;" << endl;
        A a1;
        cout << " --> A a2(a1);" << endl;
        A a2(a1);
        cout << " --> A a3(c1);" << endl;
        A a3(c1);
        cout << " --> B b1;" << endl;
        B b1;
        cout << " --> B b2(b1);" << endl;
        B b2(b1);
        cout << " --> B b3(c1);" << endl;
        B b3(c1);
        cout << " --> exit --> 3" << endl;
    }

    cout << " --> MIDDLE -------------------------------------------> 3" << endl;

    {
        cout << " --> A a1;" << endl;
        A a1;
        cout << " --> B b1;" << endl;
        B b1;
        cout << " --> C c1;" << endl;
        C c1;
        cout << " --> a1 = c1;" << endl;
        a1 = c1;
        cout << " --> a1.fun();" << endl;
        a1.fun();
        cout << " --> b1 = c1;" << endl;
        b1 = c1;
        cout << " --> b1.fun();" << endl;
        b1.fun();
        cout << " --> exit --> 4" << endl;
    }

    cout << " --> MIDDLE -------------------------------------------> 4" << endl;

    {
        cout << " --> A * pa1 = new A;" << endl;
        A * pa1 = new A;
        cout << " --> B * pb1 = new B;" << endl;
        B * pb1 = new B;
        cout << " --> C * pc1 = new C;" << endl;
        C * pc1 = new C;
        cout << " --> pa1->fun();" << endl;
        pa1->fun();
        cout << " --> pb1->fun();" << endl;
        pb1->fun();
        cout << " --> pc1->fun();" << endl;
        pc1->fun();
        cout << " --> delete pa1;" << endl;
        delete pa1;
        cout << " --> delete pb1;" << endl;
        delete pb1;
        cout << " --> delete pc1;" << endl;
        delete pc1;
        cout << " --> exit --> 5" << endl;
    }

    cout << " --> MIDDLE -------------------------------------------> 5" << endl;

    {
        cout << " --> A * pa1 = new C;" << endl;
        A * pa1 = new C;
        cout << " --> B * pb1 = new C;" << endl;
        B * pb1 = new C;
        cout << " --> C * pc1 = new C;" << endl;
        C * pc1 = new C;
        cout << " --> pa1->fun();" << endl;
        pa1->fun();
        cout << " --> pb1->fun();" << endl;
        pb1->fun();
        cout << " --> pc1->fun();" << endl;
        pc1->fun();
        cout << " --> delete pa1;" << endl;
        delete pa1;
        cout << " --> delete pb1;" << endl;
        delete pb1;
        cout << " --> delete pc1;" << endl;
        delete pc1;
        cout << " --> exit --> 6" << endl;
    }

    cout << " --> MIDDLE -------------------------------------------> 6" << endl;

    return 0;
}

// end
