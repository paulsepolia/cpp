//===================//
// function pointers //
//===================//

#include <iostream>
#include <iomanip>
#include <string>

using std::cout;
using std::endl;
using std::setw;
using std::right;
using std::string;
using std::to_string;

// the main function

int main()
{
    // # 1 --> convert integers to characters

    const int I_MAX(256);

    for(int i = 0; i != I_MAX; i++) {
        char c1 = i;
        cout << setw(6) << right << i << " --> "
             << setw(8) << right << c1 << endl;
    }

    // # 2 --> integer to string

    string s1;

    s1 = to_string(1);
    cout << " s1 = " << s1 << endl;

    return 0;
}

// end
