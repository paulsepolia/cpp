//======================//
// MULTIPLE INHERITANCE //
//======================//

#ifndef MULTIPLE_INHERITANCE_H
#define MULTIPLE_INHERITANCE_H

#include <iostream>

using std::endl;
using std::cout;

//=========//
// class A //
//=========//

class A {
public:

    // # 1 --> constructor

    A()
    {
        cout << " --> constructor --> A" << endl;
    }

    // # 2 --> copy constructor

    A(const A & other)
    {
        cout << " --> copy constructor --> A" << endl;
    }

    // # 3 --> fun

    void fun()
    {
        cout << " --> fun --> A" << endl;
    }

    // # 4 --> destructor

    virtual ~A()
    {
        cout << " --> destructor --> ~A" << endl;
    }
};

//=========//
// class B //
//=========//

class B : virtual public A { // virtual keyword solves the multiple inheritance problem
public:

    // # 1 --> constructor

    B()
    {
        cout << " --> constructor --> B" << endl;
    }

    // # 2 --> copy constructor

    B(const B & other)
    {
        cout << " --> copy constructor --> B" << endl;
    }

    // # 3 --> fun

    void fun()
    {
        cout << " --> fun --> B" << endl;
    }

    // # 4 --> destructor

    virtual ~B()
    {
        cout << " --> destructor --> ~B" << endl;
    }
};

//=========//
// class C //
//=========//

class C : public virtual A { // the virtual keyword solves the multiple inheritance problem
public:

    // # 1 --> constructor

    C()
    {
        cout << " --> constructor --> C" << endl;
    }

    // # 2 --> copy constructor

    C(const C & other)
    {
        cout << " --> copy constructor --> C" << endl;
    }

    // # 3 --> fun

    void fun()
    {
        cout << " --> fun --> C" << endl;
    }

    // # 4 --> destructor

    virtual ~C()
    {
        cout << " --> destructor --> ~C" << endl;
    }
};

//=========//
// class D //
//=========//

class D : public B, public C {
public:

    // # 1 --> constructor

    D()
    {
        cout << " --> constructor --> D" << endl;
    }

    // # 2 --> copy constructor

    D(const D & other)
    {
        cout << " --> copy constructor --> D" << endl;
    }

    // # 3 --> fun

    void fun()
    {
        cout << " --> fun --> D" << endl;
    }

    // # 4 --> destructor

    virtual ~D()
    {
        cout << " --> destructor --> ~D" << endl;
    }
};

#endif // MULTIPLE_INHERITANCE_H

// end
