//===============//
// parallel copy //
//===============//

#include <parallel/algorithm>
#include <iostream>
#include <vector>
#include <iterator>
#include <numeric>

using std::cout;
using std::endl;
using std::ostream_iterator;
using std::vector;

// the main function

int main()
{
    vector<int> from_vector(10);

    iota(from_vector.begin(), from_vector.end(), 0);

    vector<int> to_vector;

    copy(from_vector.begin(), from_vector.end(), back_inserter(to_vector));

    cout << "to_vector contains: ";

    copy(to_vector.begin(), to_vector.end(), ostream_iterator<int>(cout, " "));

    cout << endl;

    return 0;
}

// end
