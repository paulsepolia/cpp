//=============//
// copy memory //
//=============//

#include <iostream>
#include <ctime>
#include <iomanip>
#include <cmath>
#include <typeinfo>

using std::cout;
using std::cin;
using std::endl;
using std::clock;
using std::fixed;
using std::setprecision;
using std::pow;

// type definition

typedef unsigned long int uli;

// global parameters

const uli DIM = static_cast<uli>(pow(10.0, 4.0));

// struct A

struct A {

public:

    // default constructor

    A(): dim(DIM)
    {
        p = new double [dim];
        for (uli i = 0; i != dim; i++) {
            p[i] = i;
        }
    }

    // destructor

    virtual ~A()
    {
        delete [] p;
        p = nullptr;
    }

    // variables

    uli dim;
    double * p;

};

// struct B

struct B: private A {
    B(): A() {}
    virtual ~B() {}
};

// struct C

struct C: private B {
    C(): B() {}
    virtual ~C() {}
};

// struct D

struct D: private C {
    D(): C() {}
    virtual ~D() {}
};


// the main function

int main ()
{
    const int K_MAX = 10;
    time_t t1;
    time_t t2;

    cout << fixed;
    cout << setprecision(10);

    {
        A * a1 = new A;

        cout << " sizeof(a1) = " << sizeof(a1) << endl;
        cout << " sizeof(A)  = " << sizeof(A) << endl;
        cout << " typeid(A).name()  = " << typeid(A).name() << endl;
        cout << " typeid(a1).name() = " << typeid(a1).name() << endl;

        cout << " sizeof(*a1)        = " << sizeof(*a1) << endl;
        cout << " typeid(*a1).name() = " << typeid(*a1).name() << endl;

        delete a1;
    }

    cout << "--> derived --> level --> 1" << endl;

    t1 = clock();
    for (int ik = 0; ik != K_MAX; ik++) {

        cout << "------------------------------------------------>> " << ik << endl;

        B * b1 = new B;
        B * b2 = new B;
        B * b3 = new B;
    }

    t2 = clock();
    cout << "--> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

    cout << "--> base" << endl;

    t1 = clock();
    for (int ik = 0; ik != K_MAX; ik++) {

        cout << "------------------------------------------------>> " << ik << endl;

        A * a1 = new A;
        A * a2 = new A;
        A * a3 = new A;

        int * p = new int;
    }

    t2 = clock();
    cout << "--> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

    cout << "--> derived --> level --> 2" << endl;

    t1 = clock();
    for (int ik = 0; ik != K_MAX; ik++) {

        cout << "------------------------------------------------>> " << ik << endl;

        C * c1 = new C;
        C * c2 = new C;
        C * c3 = new C;
    }

    t2 = clock();
    cout << "--> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

    cout << "--> derived --> level --> 3" << endl;

    t1 = clock();
    for (int ik = 0; ik != K_MAX; ik++) {

        cout << "------------------------------------------------>> " << ik << endl;

        D * d1 = new D;
        D * d2 = new D;
        D * d3 = new D;
    }

    t2 = clock();
    cout << "--> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

    int * p = new int;

    return 0;
}

// END
