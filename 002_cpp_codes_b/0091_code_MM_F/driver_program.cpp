//============================//
// condition_variable example //
//============================//

#include <iostream>
#include <thread>
#include <mutex>
#include <condition_variable>

using std::cout;
using std::endl;
using std::mutex;
using std::thread;
using std::unique_lock;
using std::condition_variable;

// global variables

mutex mtx;
condition_variable cv;
bool ready = false;

// function # 1

void print_id (int id)
{
    unique_lock<mutex> lck(mtx);

    while (!ready) {
        cv.wait(lck);
    }

    cout << "thread " << id << endl;
}

// function # 2

void go()
{
    unique_lock<std::mutex> lck(mtx);
    ready = true;
    cv.notify_all();
}

// the main function

int main ()
{
    const int DIM = 50;

    thread threads[DIM];

    // spawn 10 threads:

    for (int i = 0; i < DIM; ++i) {
        threads[i] = thread(print_id, i);
    }

    cout << "10 threads ready to race..." << endl;

    go();                       // go!

    for(auto& th : threads) {
        th.join();
    }

    return 0;
}

// end
