//=====================//
// erasing from vector //
//=====================//

#include <iostream>
#include <vector>
#include <algorithm>

using std::cout;
using std::endl;

int main()
{
    // # 1

    cout << " --> 1" << endl;

    std::vector<int> my_vec;

    for (int i = 20; i <= 30; i++) {
        my_vec.push_back(i);
    }

    for (auto& i : my_vec) {
        cout << i << endl;
    }

    // # 2

    cout << " --> 2" << endl;

    int * p(new int [10]);

    for(int i = 0; i < 10; i++) {
        p[i] = i+3;
    }

    std::for_each(p, p+10, [](int i) {
        cout << i << endl;
    });

    return 0;
}

// end
