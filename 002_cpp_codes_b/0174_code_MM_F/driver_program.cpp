//==============//
// char pointer //
//==============//

#include <cstring>
#include <iostream>
#include <cmath>

using std::cout;
using std::endl;
using std::pow;

// type definitions

typedef unsigned long int uli;
typedef const uli culi;

// the main function

int main()
{
    // 0 --> local parameters

    culi I_MAX(static_cast<uli>(pow(10.0, 4.0)));

    // 1 --> char pointer

    char * var1 = "1234"; // WARNING: string to char* is deprecated

    cout << "---------------------------------------> 1" << endl;

    cout << "&var1     = " << &var1 << endl;
    cout << " var1     = " <<  var1 << endl;
    cout << "*var1     = " << *var1 << endl;
    cout << "*(var1+1) = " << *(var1+1) << endl;
    cout << "var1[0] = " << var1[0] << endl;
    cout << "var1[1] = " << var1[1] << endl;
    cout << "var1[2] = " << var1[2] << endl;
    cout << "var1[3] = " << var1[3] << endl;
    cout << "var1[4] = " << var1[4] << endl;
    cout << "var1[5] = " << var1[5] << endl;
    cout << "var1[6] = " << var1[6] << endl;
    cout << "strlen(var1) = " << strlen(var1) << endl;

    // 2 --> const char pointer

    const char * var2 = "56789";

    cout << "---------------------------------------> 2" << endl;

    cout << "&var2     = " << &var2 << endl;
    cout << " var2     = " <<  var2 << endl;
    cout << "*var2     = " << *var2 << endl;
    cout << "*(var2+1) = " << *(var2+1) << endl;
    cout << "var2[0] = " << var2[0] << endl;
    cout << "var2[1] = " << var2[1] << endl;
    cout << "var2[2] = " << var2[2] << endl;
    cout << "var2[3] = " << var2[3] << endl;
    cout << "var2[4] = " << var2[4] << endl;
    cout << "var2[5] = " << var2[5] << endl;
    cout << "var2[6] = " << var2[6] << endl;
    cout << "strlen(var2) = " << strlen(var2) << endl;

    // 3 --> var1 = var2;

    //var1 = var2; // ERROR: type of 'const char *' can not be assigned to type of 'char *'
    var1 = const_cast<char*>(var2);

    cout << "---------------------------------------> 3" << endl;

    cout << "&var1     = " << &var1 << endl;
    cout << " var1     = " <<  var1 << endl;
    cout << "*var1     = " << *var1 << endl;
    cout << "*(var1+1) = " << *(var1+1) << endl;
    cout << "var1[0]   = " << var1[0] << endl;
    cout << "var1[1]   = " << var1[1] << endl;
    cout << "var1[2]   = " << var1[2] << endl;
    cout << "var1[3]   = " << var1[3] << endl;
    cout << "var1[4]   = " << var1[4] << endl;
    cout << "var1[5]   = " << var1[5] << endl;
    cout << "var1[6]   = " << var1[6] << endl;
    cout << "strlen(var1) = " << strlen(var1) << endl;

    // 4 --> var2 = var1;

    var2 = var1;

    cout << "---------------------------------------> 4" << endl;

    cout << "&var2     = " << &var2 << endl;
    cout << " var2     = " <<  var2 << endl;
    cout << "*var2     = " << *var2 << endl;
    cout << "*(var2+1) = " << *(var2+1) << endl;
    cout << "var2[0]   = " << var2[0] << endl;
    cout << "var2[1]   = " << var2[1] << endl;
    cout << "var2[2]   = " << var2[2] << endl;
    cout << "var2[3]   = " << var2[3] << endl;
    cout << "var2[4]   = " << var2[4] << endl;
    cout << "var2[5]   = " << var2[5] << endl;
    cout << "var2[6]   = " << var2[6] << endl;
    cout << "strlen(var2) = " << strlen(var2) << endl;

    // 5 --> test size --> var1

    for(uli i = 0; i != I_MAX; i++) {
        cout << "--> " << i << endl;
        //var1[i] = 'X'; // SEGMENTATION fault immediate
        cout << "var1[" << i << "] = " << var1[i] << endl; // SEGMENTATION fault after some point
    }

    // 6 --> test size --> var2

    for(uli i = 0; i != I_MAX; i++) {
        //*(var2+sizeof(i)) = 'X'; // ERROR: *(var2+sizeof(i)) is constant
    }

    cout << "---------------------------------------> END" << endl;

    return 0;
}

// end
