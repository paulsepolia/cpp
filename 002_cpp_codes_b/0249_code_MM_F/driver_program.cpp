//===========//
// recursion //
//===========//

#include <iostream>
#include <iomanip>
#include <cmath>
#include <chrono>

using std::cout;
using std::endl;
using namespace std::chrono;

// # 1

double my_sum_rec(const unsigned long int LIM)
{
    static double sum(0);
    static unsigned long int counter_loc(0);

    sum = sum + 1;
    counter_loc++;

    if(counter_loc >= LIM) {
        return 0;
    }

    my_sum_rec(LIM);

    return sum;
}

// # 2

double my_sum_iter(const unsigned long int LIM)
{
    double sum(0);

    for(unsigned long int i = 0; i < LIM; i++) {
        sum = sum + 1;
    }

    return sum;
}

// # 3

double my_sum_rec2(int num)
{
    if (num == 0) {
        return 0;
    }
    return (my_sum_rec2(num-1)+1);
}

// the main function

int main()
{
    const unsigned long int LIM(std::pow(10.0, 8.0));
    auto t1 = system_clock::now();
    auto t2 = system_clock::now();
    duration<double> time_span;

    cout << std::fixed;
    cout << std::setprecision(10);
    cout << std::showpos;

    // my_sum_rec

    t1 = system_clock::now();
    cout << " --> my_sum_rec  = " << std::setw(30) << std::right << my_sum_rec(LIM) << endl;
    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used   = " << std::setw(30) << std::right << time_span.count() << endl;

    // my_sum_iter

    t1 = system_clock::now();
    cout << " --> my_sum_iter = " << std::setw(30) << std::right << my_sum_iter(LIM) << endl;
    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used   = " << std::setw(30) << std::right << time_span.count() << endl;

    // my_sum_rec2

    t1 = system_clock::now();
    cout << " --> my_sum_rec2 = " << std::setw(30) << std::right << my_sum_rec2(LIM) << endl;
    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used   = " << std::setw(30) << std::right << time_span.count() << endl;

    return 0;
}

// end
