
//============//
// queue test //
//============//

#include <iostream>
#include <queue>
#include <iomanip>

using std::cout;
using std::endl;
using std::queue;
using std::boolalpha;

// the main function

int main ()
{
    // local parameters

    const unsigned int DIM = 1000;

    // local variables

    queue<int> q1;
    queue<int> q2;

    // adjust the output

    cout << boolalpha;

    // push elements to q1

    for (unsigned int i = 0; i != DIM ; i++) {
        q1.push(i);
    }

    // set q2 to q1

    q2 = q1;

    cout << " --> q1.size() = " << q1.size() << endl;
    cout << " --> q2.size() = " << q2.size() << endl;

    // check is they are equal

    bool val1 = (q2 == q1);

    cout << " --> (q2 == q1) = " << val1 << endl;

    // use the copy constructor

    queue<int> q3(q1);

    cout << " --> q1.size() = " << q1.size() << endl;
    cout << " --> q3.size() = " << q3.size() << endl;

    // check is they are equal

    val1 = (q3 == q1);

    cout << " --> (q3 == q1) = " << val1 << endl;

    return 0;
}

// end
