//=============//
// memory leak //
//=============//

// using intel's -xHost THERE IS NO MEMORY LEAK IN HEAP SPACE
// detected by Valgrind,
// BUT IN PRACTICE THERE IS.
// IT IS A MISTAKE OF VALGRIND.

#include <iostream>
#include <cmath>

using std::cout;
using std::cin;
using std::endl;
using std::pow;

// type definition

typedef unsigned long int uli;

// global parameters

const uli DIM = static_cast<uli>(pow(10.0, 4.0));

// the main function

int main ()
{

    for (uli i = 0; i != DIM; i++) {
        int * p = new int;
        *p = 10;
    }

    int * p = new int;
    *p = 11;
    cout << "*p = " << *p << endl;

    return 0;
}

// END
