//=======================//
// operators overloading //
//=======================//

#include <iostream>
#include <utility>

using std::endl;
using std::cout;
using std::move;

// class A

class A {
public:

    // # 1 --> constructor

    A() : m_val1(-1)
    {
        cout << " --> constructor --> A" << endl;
    }

    // # 2 --> copy constructor

    A(const A & obj)
    {
        cout << " --> copy constructor --> A" << endl;
        m_val1 = obj.m_val1;
    }

    // # 3 --> move constructor

    A(A && obj)
    {
        cout << " --> move constructor --> A" << endl;
        m_val1 = move(obj.m_val1);
    }

    // # 4 --> copy assignment operator

    virtual void operator = (const A & obj)
    {
        cout << " --> copy assignment operator --> A" << endl;
        m_val1 = obj.m_val1;
    }

    // # 5 --> move assignment operator

    virtual void operator = (A && obj)
    {
        cout << " --> move assignment operator --> A" << endl;
        m_val1 = move(obj.m_val1);
    }

    // # 6 --> get function

    virtual int get()
    {
        cout << " --> get --> A" << endl;
        return m_val1;
    }

    // # 7 --> set function

    virtual void set(const int & val)
    {
        cout << " --> set --> A" << endl;
        m_val1 = val;
    }

    // # 8 --> destructor

    virtual ~A()
    {
        cout << " --> destructor --> A" << endl;
    }


private:

    int m_val1;
};

// class B

class B: public A {

public:

    // # 1 --> constructor

    B() : A(), m_val2(-2)
    {
        cout << " --> constructor --> B" << endl;
    }

    // # 2 --> copy constructor

    B(const B & obj)
    {
        cout << " --> copy constructor --> B" << endl;
        m_val2 = obj.m_val2;
    }

    // # 3 --> move constructor

    B(B && obj)
    {
        cout << " --> move constructor --> B" << endl;
        m_val2 = move(obj.m_val2);
    }

    // # 4 --> copy assignment operator

    virtual void operator = (const B & obj)
    {
        cout << " --> copy assignment operator --> B" << endl;
        m_val2 = obj.m_val2;
    }

    // # 5 --> move assignment operator

    virtual void operator = (B && obj)
    {
        cout << " --> move assignment operator --> B" << endl;
        m_val2 = move(obj.m_val2);
    }

    // # 6 --> get function

    virtual int get()
    {
        cout << " --> get --> B" << endl;
        return m_val2;
    }

    // # 7 --> set function

    virtual void set(const int & val)
    {
        cout << " --> set --> B" << endl;
        m_val2 = val;
    }

    // # 8 --> destructor

    virtual ~B()
    {
        cout << " --> destructor --> B" << endl;
    }

private:

    int m_val2;
};

// the main function

int main()
{
    // class A

    cout << " -->  1 --> A a1;" << endl;
    A a1;

    cout << " -->  2 --> x1 = a1.get();" << endl;
    int x1 = a1.get();

    cout << " -->  3 --> x1 = " << x1 << endl;

    cout << " -->  4 --> A a2(a1);" << endl;
    A a2(a1);

    cout << " -->  5 --> x1 = a2.get(); " << endl;

    x1 = a2.get();

    cout << " -->  6 --> x1 = " << x1 << endl;

    cout << " -->  7 --> a2.set(11);" << endl;

    a2.set(11);

    cout << " -->  8 --> a1 = a2;" << endl;

    a1 = a2;

    cout << " -->  9 --> x1 = a1.get(); " << endl;

    x1 = a1.get();

    cout << " --> 10 --> x1 = " << x1 << endl;

    // class B

    cout << " --> 11 --> B b1;" << endl;
    B b1;

    cout << " --> 12 --> x1 = b1.get();" << endl;
    x1 = b1.get();

    cout << " --> 13 --> x1 = " << x1 << endl;

    cout << " --> 14 --> B b2(b1);" << endl;
    B b2(b1);

    cout << " --> 15 --> x1 = b2.get(); " << endl;

    x1 = b2.get();

    cout << " --> 16 --> x1 = " << x1 << endl;

    cout << " --> 17 --> b2.set(-11);" << endl;

    b2.set(-11);

    cout << " --> 18 --> b1 = b2;" << endl;

    b1 = b2;

    cout << " --> 19 --> x1 = b1.get(); " << endl;

    x1 = b1.get();

    cout << " --> 20 --> x1 = " << x1 << endl;

    cout << " --> 21 --> A * pA = &b1;" << endl;

    A * pA = &b1;

    cout << " --> 22 --> x1 = pA->get();" << endl;

    x1 = pA->get();

    cout << " --> 23 --> x1 = " << x1 << endl;

    cout << " --> 24 --> B * pB = &b1;" << endl;

    B * pB = &b1;

    cout << " --> 25 --> x1 = pB->get();" << endl;

    x1 = pB->get();

    cout << " --> 26 --> x1 = " << x1 << endl;

    cout << " --> 27 --> exit" << endl;

    return 0;
}

// end
