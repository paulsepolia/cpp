
//================//
// all_of example //
//================//

#include <iostream>
#include <algorithm>
#include <ctime>

using std::cout;
using std::cin;
using std::endl;
using std::all_of;

// define lamda also here

#define KK [](const int & i) { return i%2; }

// the function

bool f_oddA(const int & i)
{
    return bool(i%2);
}

// the functor

template<class T>
struct f_oddB {
    bool operator()(const T & i)
    {
        return bool(i%2);
    }
};

// the main function

int main ()
{
    const int DIM = 1000000000;

    int * foo = new int [DIM];

    time_t t1;
    time_t t2;

    // build the array

    foo[0] = 1;

    for (int i = 1; i != DIM; i++) {
        foo[i] = foo[i-1]+2;
    }

    // lamda function

    t1 = clock();

    if (all_of(foo, foo+DIM, [](const int & i) {
    return i%2;
})) {
        cout << "All the elements are odd numbers." << endl;
    }

    t2 = clock();

    cout << "-->> 1 --> time used (lamda function) = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

    //  plain function

    t1 = clock();

    if (all_of(foo, foo+DIM, f_oddA)) {
        cout << "All the elements are odd numbers." << endl;
    }

    t2 = clock();

    cout << "-->> 2 --> time used (plain function) = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

    t1 = clock();

    // functor

    if (all_of(foo, foo+DIM, f_oddB<int>())) {
        cout << "All the elements are odd numbers." << endl;
    }

    t2 = clock();

    cout << "-->> 3 --> time used (functor) = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

    // #define

    if (all_of(foo, foo+DIM, KK)) {
        cout << "All the elements are odd numbers." << endl;
    }

    t2 = clock();

    cout << "-->> 4 --> time used (#define lamda) = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

    // free up heap

    delete [] foo;

    return 0;
}

// END
