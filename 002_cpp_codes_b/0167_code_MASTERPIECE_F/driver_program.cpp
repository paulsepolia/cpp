//=====================================//
// overloaded new and delete operators //
//=====================================//

#include <iostream>
#include <cmath>
#include <vector>

using std::cout;
using std::endl;
using std::pow;
using std::vector;
using std::free;

// type definitions

typedef unsigned long int uli;
typedef const uli culi;

// overlod the new operator

void* operator new(size_t sz) throw(std::bad_alloc)
{
    static uli index(0);
    index++;
    cout << " --> inside new overloaded operator, index = " << index << endl;
    return malloc(sz);
}

// overload the delete operator

void operator delete(void* ptr) noexcept
{

    static uli index1(0);
    index1++;
    cout << " --> inside delete overloaded operator, index1 = " << index1 << endl;
    if(ptr != 0) {
        static uli index2(0);
        index2++;
        cout << " --> overloaded delete, index2 = " << index2 << endl;
        free(ptr);
        ptr = 0;
    }
}

// the main function

int main()
{
    // local parameters

    culi	DIMEN(1*static_cast<uli>(pow(10.0, 1.0)));

    cout << "------------------------------------------------>>  1" << endl;

    double * pd1 = new double(10.0);

    cout << "------------------------------------------------>>  2" << endl;

    vector<double*> vpd1 {};

    vpd1.resize(0);
    vpd1.reserve(100);

    cout << "------------------------------------------------>>  3" << endl;

    for(uli i = 0; i != DIMEN; i++) {
        cout << "------------------------------------------------>>  4, " << i << endl;
        vpd1.push_back(pd1);
    }

    cout << "------------------------------------------------>>  5" << endl;

    vpd1.clear();

    cout << "------------------------------------------------>>  6" << endl;

    vpd1.shrink_to_fit();

    cout << "------------------------------------------------>>  7" << endl;

    delete pd1;
    pd1 = 0;

    cout << "------------------------------------------------>>  END" << endl;

    return 0;
}

// end
