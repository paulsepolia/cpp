
//================//
// all_of example //
//================//

#include <iostream>
#include <algorithm>
#include <array>

using std::cout;
using std::cin;
using std::endl;
using std::array;
using std::all_of;

// the main function

int main ()
{
    array<int,8> foo = {3,5,7,11,13,17,19,23};

    if (all_of(foo.begin(), foo.end(), [](int i) {
    return i%2;
}) )
    cout << "All the elements are odd numbers." << endl;

    return 0;
}

// END
