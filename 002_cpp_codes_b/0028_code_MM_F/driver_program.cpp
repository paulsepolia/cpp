//================//
// move symantics //
//================//

#include <iostream>
#include <utility>
#include <string>
#include <vector>
#include <list>

using std::cout;
using std::endl;
using std::move;
using std::string;
using std::vector;
using std::list;

// the main function

int main()
{
    // integers

    int x1 = 10;
    int x2;

    cout << " -->  x1 = " <<  x1 << endl;
    cout << " --> &x1 = " << &x1 << endl;
    cout << " -->  x2 = " <<  x2 << endl;
    cout << " --> &x2 = " << &x2 << endl;

    x2 = move(x1);

    cout << " -->  x1 = " <<  x1 << endl;
    cout << " --> &x1 = " << &x1 << endl;
    cout << " -->  x2 = " <<  x2 << endl;
    cout << " --> &x2 = " << &x2 << endl;

    int x3 = move(x1);

    cout << " -->  x1 = " <<  x1 << endl;
    cout << " --> &x1 = " << &x1 << endl;
    cout << " -->  x3 = " <<  x3 << endl;
    cout << " --> &x3 = " << &x3 << endl;

    // strings

    string s1 = "test";
    string s2;

    cout << " -->  s1 = " <<  s1 << endl;
    cout << " --> &s1 = " << &s1 << endl;
    cout << " -->  s2 = " <<  s2 << endl;
    cout << " --> &s2 = " << &s2 << endl;

    s2 = move(s1);

    cout << " -->  s1 = " <<  s1 << endl;
    cout << " --> &s1 = " << &s1 << endl;
    cout << " -->  s2 = " <<  s2 << endl;
    cout << " --> &s2 = " << &s2 << endl;

    string s3 = move(s1);

    cout << " -->  s1 = " <<  s1 << endl;
    cout << " --> &s1 = " << &s1 << endl;
    cout << " -->  s3 = " <<  s3 << endl;
    cout << " --> &s3 = " << &s3 << endl;

    // doubles

    double d1 = 1234.56789;
    double d2;

    cout << " -->  d1 = " <<  d1 << endl;
    cout << " --> &d1 = " << &d1 << endl;
    cout << " -->  d2 = " <<  d2 << endl;
    cout << " --> &d2 = " << &d2 << endl;

    d2 = move(d1);

    cout << " -->  d1 = " <<  d1 << endl;
    cout << " --> &d1 = " << &d1 << endl;
    cout << " -->  d2 = " <<  d2 << endl;
    cout << " --> &d2 = " << &d2 << endl;

    double d3 = move(d1);

    cout << " -->  d1 = " <<  d1 << endl;
    cout << " --> &d1 = " << &d1 << endl;
    cout << " -->  d3 = " <<  d3 << endl;
    cout << " --> &d3 = " << &d3 << endl;

    // vectors

    vector<double> v1 = {1234.56789};
    vector<double> v2;

    cout << " -->  v1[0] = " <<  v1[0] << endl;
    cout << " --> &v1[0] = " << &v1[0] << endl;

    v2 = move(v1);

    cout << " -->  v2[0] = " <<  v2[0] << endl;
    cout << " --> &v2[0] = " << &v2[0] << endl;

    vector<double> v3 = move(v2);

    cout << " -->  v3[0] = " <<  v3[0] << endl;
    cout << " --> &v3[0] = " << &v3[0] << endl;

    // lists

    list<double> l1 = {1234.56789};
    list<double> l2;

    cout << " -->  l1.front() = " <<  l1.front() << endl;
    cout << " --> &l1.front() = " << &l1.front() << endl;

    l2 = move(l1);

    cout << " -->  l2.front() = " <<  l2.front() << endl;
    cout << " --> &l2.front() = " << &l2.front() << endl;

    list<double> l3 = move(l2);

    cout << " -->  l3.front() = " <<  l3.front() << endl;
    cout << " --> &l3.front() = " << &l3.front() << endl;

    return 0;
}

// END
