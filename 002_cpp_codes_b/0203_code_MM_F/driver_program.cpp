//=================//
// diamond problem //
//=================//

#include <iostream>
#include <iomanip>

using std::endl;
using std::cout;
using std::boolalpha;

// the main function

int main()
{
    cout << boolalpha;

    // (int) and (int*) types

    int x1(10);

    cout << "  x1 = " <<  x1 << endl;
    cout << " &x1 = " << &x1 << endl;

    int x2(20);

    cout << "  x2 = " <<  x2 << endl;
    cout << " &x2 = " << &x2 << endl;

    int * px1(nullptr);

    px1 = &x1;

    cout << " (px1 == &x1) = " << (px1 == &x1) << endl;
    cout << " (px1 == &x2) = " << (px1 == &x2) << endl;

    // const int

    const int x3(11);

    cout << "  x3 = " << x3 << endl;

    int * px3 = const_cast<int*>(&x3);

    cout << "*px3 = " << *px3 << endl;

    *px3 = 12; // undefined behaviour

    cout << " (px3 == &x3) = " << (px3 == &x3) << endl;
    cout << "  x3 = " <<   x3 << endl;
    cout << "*px3 = " << *px3 << endl;
    cout << " &x3 = " <<  &x3 << endl;
    cout << " px3 = " <<  px3 << endl;

    // a possible use of const_cast

    int k1 = 40;
    const int* pk1 = &k1;
    int* pk2 = const_cast<int*>(pk1);

    cout << "   k1 = " <<   k1 << endl;
    cout << " *pk1 = " << *pk1 << endl;
    cout << " *pk2 = " << *pk2 << endl;

    *pk2 = 60;

    cout << "   k1 = " <<   k1 << endl;
    cout << " *pk1 = " << *pk1 << endl;
    cout << " *pk2 = " << *pk2 << endl;

    // reference

    const int & p1(23);
    cout << " (p1 == 23) = " << (p1 == 23) << endl;
    //p1 = 24; // ERROR
    cout << " &p1 = " << &p1 << endl;
    int * p2(nullptr);
    p2 = const_cast<int*>(&p1);
    cout << " *p2 = " << *p2 << endl;
    *p2 = 111;
    cout << " *p2 = " << *p2 << endl;
    cout << " p1 = " << p1 << endl; // p1 = 111
    cout << " (p2 == &p1) = " << (p2 == &p1) << endl;

    return 0;
}

// end
