//=================//
// diamond problem //
//=================//

#include <iostream>

using std::endl;
using std::cout;

// class --> B

class B {
public:
};

// class --> D1

class D1: public B {
public:
};

// class --> D2

class D2: public B {
public:
};

// class --> E1

class E1: public D1, public D2 {
public:
};

// the main function

int main()
{
    B b1;
    D1 d1;
    D2 d2;
    E1 e1;

    // I GOT BACK WARNINGS FOR ALL THE UNUSED VARIABLES:
    // THE UNUSED VARIABLES ARE: b1, d1, d2, e1
    // IF I DECLARED VIRTUAL THE INHERITANCE OF B BY D1 AND D2
    // THEN I DO NOT GET BACK WARNING FOR THE d1, d2, e1
    // WHICH MEANS THAT the objects d1, d2, e1 ARE NOT CONSTRUCTED IN COMPILATION TIME!

    return 0;
}

// end
