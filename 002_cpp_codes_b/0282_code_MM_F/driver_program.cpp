//=============//
// linked list //
//=============//

#include <cmath>
#include <iostream>
using std::endl;
using std::cout;

#define DEBUG
//#undef DEBUG

// list definition

class list {
public:
    int * data1;
    int * data2;
    int * data3;
    list * next;
};

typedef unsigned long int uli;

// the main function

int main()
{
    const uli DIM0(std::pow(10.0, 3.0));
    const uli DIM1(3*std::pow(10.0, 2.0));
    const uli DIM2(3*std::pow(10.0, 2.0));
    const uli DIM3(3*std::pow(10.0, 4.0));
    const uli TR(std::pow(10.0, 1.0));

    for(uli k = 0; k != TR; k++) {
        cout << "---------------------------------------->> k = " << k << endl;

        // # 1 create nodes

        list * head(new list);
        list * node(new list);

        // initialize the head

        head->next = node;

        // build the nodes

        for(uli i = 0; i != DIM0; i++) {
            node->data1 = new int[DIM1];
            for(uli j = 0; j != DIM1; j++) {
                node->data1[j] = j;
            }
            node->data2 = new int[DIM2];
            for(uli j = 0; j != DIM2; j++) {
                node->data2[j] = j;
            }
            node->data3 = new int[DIM3];
            for(uli j = 0; j != DIM3; j++) {
                node->data3[j] = j;
            }
            node->next = new list;
            node = node->next;
        }

        // finalize the nodes

        node->next = 0;
        node->data1 = new int[DIM1];
        node->data2 = new int[DIM2];
        node->data3 = new int[DIM3];

        // delete the nodes
        node = head;
        while(node->next) {
            list * q;
            q = node->next;
            node->next = q->next;
            delete [] q->data1;
            delete [] q->data2;
            delete [] q->data3;
            delete q;
        }

        // delete the head
        delete head;
    }

    return 0;
}
// END
