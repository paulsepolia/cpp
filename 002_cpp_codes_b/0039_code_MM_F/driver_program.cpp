
//===========================//
// get address of a function //
//===========================//

#include <cstdio>
#include <iostream>

using std::cout;
using std::endl;

// function --> foo

double foo (double x)
{
    return x*x;
}

// the main function

int main ()
{
    // define new function based on an already defined function

    double (*fun1)(double) = &foo;
    double (*fun2)(double) =  foo;
    double (*fun3)(double) =  fun2;

    //double (*fun4)(double) = &fun2; // that is WRONG

    cout << " --> fun1(10) = " << fun1(10) << endl;
    cout << " --> fun2(10) = " << fun2(10) << endl;
    cout << " --> fun3(10) = " << fun3(10) << endl;

    //cout << " --> fun4(10) = " << fun4(10) << endl; // DOES NOT WORK

    // way to get the address of the head of the function

    cout << " --> reinterpret_cast<void*>(foo)  = " << reinterpret_cast<void*>(foo)  << endl;
    cout << " --> reinterpret_cast<void*>(&foo) = " << reinterpret_cast<void*>(&foo) << endl;
    cout << " --> reinterpret_cast<void*>(fun1) = " << reinterpret_cast<void*>(fun1) << endl;
    cout << " --> reinterpret_cast<void*>(fun2) = " << reinterpret_cast<void*>(fun2) << endl;
    cout << " --> reinterpret_cast<void*>(fun3) = " << reinterpret_cast<void*>(fun3) << endl;

    //cout << " --> reinterpret_cast<void*>(fun4) = "
    //     << reinterpret_cast<void*>(fun4) << endl; // DOES NOT WORK

    return 0;
}

// end
