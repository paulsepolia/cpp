#!/bin/bash

  # 1. compile

  g++-5.2.0  -O3                \
             -Wall              \
             -std=c++14         \
             -pthread           \
             driver_program.cpp \
             -o x_gnu_520
