//==================//
// add two integers //
//==================//

#include <iostream>
using std::endl;
using std::cout;

typedef unsigned long int uli;

// the main function

int main()
{
    int x(0);
    int y(0);
    int res(0);

    // # 1

    cout << "------------------------------------>> 1" << endl;
    x = 1;
    for(int i = 0 ; i < 10; i++) {
        x = x << 1;
        cout << x << endl;
    }

    // # 2

    cout << "------------------------------------>> 2" << endl;
    x = 1;
    y = 0;
    res = x&y;
    cout << " x = " << x << endl;
    cout << " y = " << y << endl;
    cout << " x & y = " << res << endl;


    // # 3

    cout << "------------------------------------>> 3" << endl;
    x = 1;
    y = 1;
    res = x&y;
    cout << " x = " << x << endl;
    cout << " y = " << y << endl;
    cout << " x & y = " << res << endl;

    // # 4

    cout << "------------------------------------>> 4" << endl;
    x = 2;
    y = 1;
    res = x&y;
    cout << " x = " << x << endl;
    cout << " y = " << y << endl;
    cout << " x & y = " << res << endl;

    // # 5

    cout << "------------------------------------>> 5" << endl;
    x = 2;
    y = 2;
    res = x&y;
    cout << " x = " << x << endl;
    cout << " y = " << y << endl;
    cout << " x & y = " << res << endl;

    // # 6

    cout << "------------------------------------>> 6" << endl;
    x = 3;
    y = 3;
    res = x&y;
    cout << " x = " << x << endl;
    cout << " y = " << y << endl;
    cout << " x & y = " << res << endl;


    return 0;
}

// end
