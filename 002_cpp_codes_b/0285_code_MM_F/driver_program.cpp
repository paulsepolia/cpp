//=========//
// bit set //
//=========//

#include <bitset>
#include <iostream>
using std::endl;
using std::cout;

// the main function

int main()
{
    std::bitset<32> bVal1("11111111");
    cout << " bitset = " << bVal1 << endl;

    std::bitset<32> bVal2("111111111111111");
    cout << " bitset = " << bVal2 << endl;

    cout << " int val = " << bVal1.to_ulong() << endl;
    cout << " int val = " << bVal2.to_ulong() << endl;

    return 0;
}

// end
