//====================//
// use of make_unique //
//====================//

#include <iostream>
#include <memory>

using std::cout;
using std::endl;
using std::ostream;
using std::unique_ptr;
using std::make_unique;

// class Vec3 declaration

class Vec3 {

private:

    int x;
    int y;
    int z;

public:

    Vec3();
    Vec3(int, int, int);
    friend ostream& operator<<(ostream& , Vec3& );
};

// class Vec3 definition

// constructor # 1

Vec3::Vec3() :
    x(0), y(0), z(0)
{}

// constructor # 2

Vec3::Vec3(int x, int y, int z) :
    x(x), y(y), z(z)
{}

// friend function

ostream& operator<<(ostream & os, Vec3 & v)
{
    return os << '{' << "x:" << v.x << " y:" << v.y << " z:" << v.z << '}';
}

// the main function

int main()
{
    // Use the default constructor

    unique_ptr<Vec3> v1(make_unique<Vec3>());
    unique_ptr<Vec3> vB1(new Vec3()); // same as above

    // Use the constructor that matches these arguments

    unique_ptr<Vec3> v2(make_unique<Vec3>(0, 1, 2));
    unique_ptr<Vec3> vB2(new Vec3(0,1,2)); // same as above

    // Create a unique_ptr to an array of 5 elements

    unique_ptr<Vec3[]> v3 = make_unique<Vec3[]>(5);
    unique_ptr<Vec3[]> vB3(new Vec3 [5]); // same as above

    cout << "make_unique<Vec3>():      " << *v1 << endl;
    cout << "make_unique<Vec3>(0,1,2): " << *v2 << endl;
    cout << "make_unique<Vec3[]>(5):   " << endl;

    for (int i = 0; i < 5; i++) {
        cout << "     " << v3[i] << endl;
    }

    cout << "new Vec3():      " << *vB1 << endl;
    cout << "new Vec3(0,1,2): " << *vB2 << endl;
    cout << "new Vec3[5]:     " << endl;

    for (int i = 0; i < 5; i++) {
        cout << "     " << vB3[i] << endl;
    }
}

// end
