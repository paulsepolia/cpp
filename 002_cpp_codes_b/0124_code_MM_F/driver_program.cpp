//===================//
// function pointers //
//===================//

#include <iostream>
#include <sstream>

using std::cout;
using std::cin;
using std::endl;
using std::stringstream;
using std::hex;

// function --> funA # 1

void funA(void * x)
{
    int y;
    stringstream stream_loc;
    stream_loc << x;
    stream_loc >> hex >> y;

    cout << " --> funA # 1 --> x = " << y << endl;
}

// the main function

int main()
{
    //=======================//
    // function --> funA # 1 //
    //=======================//

    // declare a pointer to a function which takes an integer
    // and returns a void

    void (*foo1)(void *);
    void (*foo2)(void *);

    // the ampersand is actually optional

    foo1 = &funA;
    foo2 = funA;

    int x1 = 14;
    int x2 = 15;

    foo1(reinterpret_cast<void*>(x1));
    foo2(reinterpret_cast<void*>(x2));

    return 0;
}

// end
