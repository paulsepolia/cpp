//=============//
// memory leak //
//=============//

#include <iostream>
#include <cmath>

using std::cout;
using std::cin;
using std::endl;
using std::pow;


int main ()
{
    int val1 = 10;

    const int & p1 = val1;

    cout << p1 << endl;

    int * p2 = &p1;

    *p2  = 100;

    cout << p1 << endl;

    return 0;
}

// END
