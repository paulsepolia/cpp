//===========//
// recursion //
//===========//

#include <iostream>
#include <iomanip>
#include <cmath>
#include <chrono>

using std::cout;
using std::endl;
using namespace std::chrono;

// # 1

double rec_v1()
{
    // # 1

    static auto t1 = system_clock::now();
    static auto t2 = system_clock::now();
    duration<double> time_span;
    static unsigned long int counter_loc(0);
    counter_loc++;
    const unsigned long int DIA(pow(10.0, 8.0));
    const unsigned long int LIM(2*pow(10.0, 9.0));

    cout << std::fixed;
    cout << std::showpos;
    cout << std::setprecision(8);

    // # 2

    t1 = system_clock::now();

    if(counter_loc%DIA == 0) {
        cout << " --> " << std::setw(12) << std::right << counter_loc << endl;
        time_span = duration_cast<duration<double>>(t1-t2);
        cout << " --> " << std::setw(10) << std::right << time_span.count() << endl;
        t2 = system_clock::now();
    }

    if(counter_loc >= LIM) {
        return 0.0;
    }

    return rec_v1();
}

// the main function

int main()
{
    rec_v1();
    return 0;
}

// end
