#!/bin/bash

  # 1. compile

  g++  -g -O0             \
       -Wall              \
       -std=c++0x         \
       driver_program.cpp \
       -o x_gnu_ubu
