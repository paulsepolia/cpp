#!/bin/bash

  # 1. compile

  icpc  -O0                \
        -Wall              \
        -std=c++11         \
        driver_program.cpp \
        -o x_intel
