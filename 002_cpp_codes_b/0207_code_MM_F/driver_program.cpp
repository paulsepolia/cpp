//===================//
// drop constantness //
//===================//

#include <iostream>
#include <iomanip>
#include <typeinfo>

using std::endl;
using std::cout;
using std::boolalpha;

// function --> 1

const int & fun1(const int & x)
{
    cout << " fun1 --> x = " << x << endl;
    return x;
}

// the main function

int main()
{
    cout << boolalpha;

    //=============//
    // SCOPE --> 1 //
    //=============//

    {
        // # 1 --> declare and define const int

        const int x1(10);
        int const x2 {
            20
        };

        cout << "  x1 = " << x1 << endl;
        cout << "  x2 = " << x2 << endl;

        // # 2 --> try to modify via a (int*) and const_cast<int*>

        int * px1(nullptr);
        px1 = const_cast<int*>(&x1);

        cout << " (px1 == &x1) = " << (px1 == &x1) << endl;
        *px1 = 30;
        cout << " (px1 == &x1) = " << (px1 == &x1) << endl;
        cout << "  x1 = " <<   x1 << endl; // UNDEFINED BEHAVIOR
        cout << "*px1 = " << *px1 << endl;

        // # 3 --> try to modify via a (const int*) and const_cast<int*>

        const int * px2(nullptr);
        px2 = &x1;
        px1 = const_cast<int*>(px2);
        *px1 = 40;
        cout << " (px1 == &x1) = " << (px1 == &x1) << endl;
        cout << " (px2 == px1) = " << (px1 == px2) << endl;

        cout << "  x1 = " <<   x1 << endl; // UNDEFINED BEHAVIOR
        cout << "*px1 = " << *px1 << endl;
        cout << "*px2 = " << *px2 << endl;

        // # 4

        const int & x3(x1);

        cout << " x1 = " << x1 << endl;
        cout << " x3 = " << x3 << endl;

        // the output below is strange since
        // x1 and x3 are using the same adrress in RAM
        // but they are holding different values!

        cout << " ( x3 ==  x1) = " << ( x3 ==  x1) << endl;
        cout << " (&x3 == &x1) = " << (&x3 == &x1) << endl;

        // further

        int * px3 {const_cast<int*>(&x3)};
        *px3 = 111;
        cout << " (px3 == &x3) = " << (px3 == &x3) << endl;
        cout << " *px3 = " << *px3 << endl;
        cout << "   x3 = " <<   x3 << endl;
        cout << "   x1 = " <<   x1 << endl;

        // CONCLUDING:
        // const int & x1 --> can be changed
        // const int x1 --> can not be changed (UNDEFINED behaviour)

    }

    //=============//
    // SCOPE --> 2 //
    //=============//

    {
        int x1 = 10;
        cout << " --> typeid(x1).name()       = " << typeid(x1).name() << endl;
        cout << " --> typeid(fun1).name()     = " << typeid(fun1).name() << endl;
        cout << " --> typeid(fun1(x1)).name() = " << typeid(fun1(x1)).name() << endl;

        x1 = fun1(x1);
        int * px1(nullptr);
        //px1 = const_cast<int*>(fun1(x1)); // ERROR
        px1 = &x1;
        cout << " px1 --> A = " << px1 << endl;
        px1 = reinterpret_cast<int*>(fun1(x1));
        cout << " px1 --> B = " << px1 << endl;
        //*px1 = 40; // THIS IS SEGMENTATION FAULT

        cout << " --> typeid(x1).name()       = " << typeid(x1).name() << endl;
        cout << " --> typeid(fun1).name()     = " << typeid(fun1).name() << endl;
        cout << " --> typeid(fun1(x1)).name() = " << typeid(fun1(x1)).name() << endl;
        cout << " --> typeid(px1).name()      = " << typeid(px1).name() << endl;

        const int x2 = 11;
        const int & x3 = 12;

        cout << " --> typeid(x2).name()       = " << typeid(x2).name() << endl;
        cout << " --> typeid(x3).name()       = " << typeid(x3).name() << endl;

        int * px2(nullptr);
        px2 = const_cast<int*>(&x3);
        *px2 = 13;

        cout << " (&x3 == px2) = " << (&x3 == px2) << endl;
        cout << "   x3 = " <<   x3 << endl;
        cout << " *px2 = " << *px2 << endl;
    }

    return 0;
}

// end
