//========================//
// Stack class definition //
//========================//

#ifndef STACK_DEFINITION_H
#define STACK_DEFINITION_H

#include <assert.h>

//  constructor

template <class T>
Stack<T>::Stack(int MaxSize) : MAX_NUM(MaxSize) // Initialize the constant
{
    Data = new T [MAX_NUM];
    CurrElemNum = 0;
}

// destructor

template <class T>
Stack<T>::~Stack(void)
{
    delete [] Data;
}

// Push function

template <class T>
inline void Stack<T>::Push(const T & Item)
{
    // Error Check
    // Make sure we aren't exceeding the maximum storage space

    assert(CurrElemNum < MAX_NUM);

    Data[CurrElemNum++] = Item;
}

// Pop function

template <class T>
inline T Stack<T>::Pop(void)
{
    // Error Check
    // Make sure we aren't popping from an empty Stack

    assert(CurrElemNum > 0);

    return Data[--CurrElemNum];
}

// Peek function

template <class T>
inline const T & Stack<T>::Peek(int Depth) const
{
    // Error Check
    // Make sure the depth doesn't exceed the number of elements

    assert(Depth < CurrElemNum);

    return Data[CurrElemNum - (Depth + 1)];
}

#endif // STACK_DEFINITION_H
