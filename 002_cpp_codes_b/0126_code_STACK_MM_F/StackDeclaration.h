//=========================//
// Stack class declaration //
//=========================//

//  This Stack has been implemented with templates to allow it
//  to accomodate virtually any data type, and the size of the
//  Stack is determined dynamically at runtime
//  There is also a new function: peek(), which, given a whole
//  number 'Depth', returns the Stack element which is 'Depth'
//  levels from the top

#ifndef STACK_DECLARATION_H
#define STACK_DECLARATION_H

// declaration

template <class T>
class Stack {
public:

    // constructor

    Stack(int MaxSize = 500);

    // copy constructor

    Stack(const Stack<T> &);

    // destructor

    virtual ~Stack(void);

    // member functions

    inline void Push(const T &);            // adds item to the top
    inline T Pop(void);                     // returns item from the top
    inline const T & Peek(int Depth) const; // peek a depth downwards

private:

    T * Data;          // the actual data array
    int CurrElemNum;   // the current number of elements
    const int MAX_NUM; // maximum number of elements
};

#endif // STACK_DECLARATION_H
