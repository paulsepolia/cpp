//===================//
// atomic operations //
//===================//

#include <vector>
#include <iostream>
#include <thread>
#include <atomic>
#include <cmath>
#include <chrono>

using std::vector;
using std::cout;
using std::endl;
using std::thread;
using std::atomic;
using std::memory_order_relaxed;
using std::memory_order_acquire;
using std::pow;
using namespace std::chrono;

// global variables

typedef long long int lli;
atomic<lli> cnt1 = {0};
atomic<lli> cnt2 = {0};
const lli DIM = static_cast<lli>(pow(10.0, 8.0));

// functions

// # 1

void f1()
{
    for (lli n = 0; n < DIM; ++n) {
        cnt1.fetch_add(1, memory_order_relaxed);
    }
}

// # 2

void f2()
{
    for (lli n = 0; n < DIM; ++n) {
        cnt2.fetch_add(1, memory_order_acquire);
    }
}

// the main function

int main()
{
    vector<thread> * v = new vector<thread>;
    const lli NT = 4;

    // f1

    auto t1 = system_clock::now();

    // spawn threads

    for (lli n = 0; n < NT; ++n) {
        v->emplace_back(f1);
    }

    // join threads

    for (auto& t : *v) {
        t.join();
    }

    auto t2 = system_clock::now();
    auto time_span = duration_cast<duration<double>>(t2-t1);

    cout << " --> Final counter value is " << cnt1 << endl;
    cout << " --> time used = " << time_span.count() << endl;

    // clear

    v->clear();
    v->shrink_to_fit();

    // f2

    t1 = system_clock::now();

    // spawn threads

    for (lli n = 0; n < NT; ++n) {
        v->emplace_back(f2);
    }

    // join threads

    for (auto& t : *v) {
        t.join();
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);

    cout << " --> Final counter value is " << cnt2 << endl;
    cout << " --> time used = " << time_span.count() << endl;

    // delete pointer

    delete v;

    return 0;
}

// end
