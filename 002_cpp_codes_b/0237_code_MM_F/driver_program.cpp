//================//
// move symantics //
//================//

#include <iostream>

using std::cout;
using std::endl;

// class --> A

class A {
public:
    A(): x1(1), x2(2), x3(3)
    {
        cout << " --> A" << endl;
        cout << " x1 = " << x1 << endl;
        cout << " x2 = " << x2 << endl;
        cout << " x3 = " << x3 << endl;
    }
public:
    int x1;
protected:
    int x2;
private:
    int x3;
};

// class --> B

class B: public A {
public:
    B(): A()
    {
        cout << " --> B" << endl;
        cout << " x1 = " << x1 << endl;
        cout << " x2 = " << x2 << endl;
        //cout << " x3 = " << x3 << endl;
    }
};

// class --> C

class C: protected A {
public:
    C(): A()
    {
        cout << " --> C" << endl;
        cout << " x1 = " << x1 << endl;
        cout << " x2 = " << x2 << endl;
        //cout << " x3 = " << x3 << endl;
    }
};

// class --> D

class D: private A {
public:
    D(): A()
    {
        cout << " --> D" << endl;
        cout << " x1 = " << x1 << endl;
        cout << " x2 = " << x2 << endl;
        //cout << " x3 = " << x3 << endl;
    }
};


// the main function

int main()
{
    A a;

    cout << " a1.x1 = " << a.x1 << endl;
    //cout << a.x2 << endl;
    //cout << a.x3 << endl;

    B b;

    // x1 became public
    // x2 became protected
    // x3 became private

    cout << " b.x1 = " << b.x1 << endl;
    //cout << b.x2 << endl;
    //cout << b.x3 << endl;

    C c;

    // x1 became protected
    // x2 became protected
    // x3 became private

    //cout << c.x1 << endl;
    //cout << c.x2 << endl;
    //cout << c.x3 << endl;

    D d;

    // x1 became private
    // x2 became private
    // x3 became private

    //cout << d.x1 << endl;
    //cout << d.x2 << endl;
    //cout << d.x3 << endl;

    return 0;
}

// end
