//=============//
// static cast //
//=============//

#include <iostream>

using std::cout;
using std::endl;

// class --> A

class A {
public:
    A(): xA(1)
    {
        cout << " --> A" << endl;
    }
public:
    int xA;
};

// class --> B

class B: public A {
public:
    B(): A(), xB(2)
    {
        cout << " --> B" << endl;
    }
public:
    int xB;
};

// function --> fun1

void fun1(const A * val)
{
    const B * pb = static_cast<const B*>(val);
    cout << "pb->xA = " << pb->xA << endl;
    cout << "pb->xB = " << pb->xB << endl;
}

// function --> fun2

void fun2(A * val)
{
    B * pb = static_cast<B*>(val);
    cout << "pb->xA = " << pb->xA << endl;
    cout << "pb->xB = " << pb->xB << endl;
}

// function --> fun3

void fun3(A * val)
{
    B * pb = (B*)(val);
    cout << "pb->xA = " << pb->xA << endl;
    cout << "pb->xB = " << pb->xB << endl;
}

// the main function

int main()
{
    cout << " A a;" << endl;

    A a;
    A * pa = &a;

    cout << " B b;" << endl;

    B b;
    B * pb = &b;

    cout << " --------------------------------------> fun1" << endl;

    cout << " fun1(pa);" << endl;

    fun1(pa);

    cout << " fun1(pb);" << endl;

    fun1(pb);

    cout << " --------------------------------------> fun2" << endl;

    cout << " fun2(pa);" << endl;

    fun2(pa);

    cout << " fun2(pb);" << endl;

    fun2(pb);

    cout << " --------------------------------------> fun3" << endl;

    cout << " fun3(pa);" << endl;

    fun3(pa);

    cout << " fun3(pb);" << endl;

    fun3(pb);

    return 0;
}

// end
