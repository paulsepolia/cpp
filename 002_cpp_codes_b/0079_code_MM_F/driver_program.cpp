//============//
// stack calc //
//============//

#include <iostream>
#include <string>
#include <algorithm>
#include <stack>
#include <exception>

using std::cout;
using std::endl;
using std::cerr;
using std::string;
using std::stack;
using std::exception;

// the main function

int main()
{
    // local variables

    const string s1("12*34*56*78*99*99*99*99*99*99*99******++++");
    stack<int> my_stack;

    // stack calculator

    try {

        for(unsigned int i = 0; i != s1.length(); i++) {
            if(('0' <= s1[i]) && (s1[i] <= '9')) {
                my_stack.push(int(s1[i] - '0'));
            } else if (s1[i] == '+') {
                if (my_stack.size() < 2) {
                    cout << " --> BREAK --> +;" << endl;
                    break;
                    return -1;
                }
                int i1 = my_stack.top();
                my_stack.pop();
                int i2 = my_stack.top();
                my_stack.pop();
                int i3 = i1 + i2;
                my_stack.push(i3);
            } else if (s1[i] == '*') {
                if (my_stack.size() < 2) {
                    cout << " --> BREAK --> *;" << endl;
                    break;
                    return -1;
                }
                int i1 = my_stack.top();
                my_stack.pop();
                int i2 = my_stack.top();
                my_stack.pop();
                int i3 = i1 * i2;
                my_stack.push(i3);
            }
        }

        while (!my_stack.empty()) {
            cout << " elem = " << my_stack.top() << endl;
            my_stack.pop();
        }
    } catch (exception& e) {
        cerr << "Exception catched : " << e.what() << endl;
    }

    return 0;
}

// end
