#!/bin/bash

  # 1. compile

  g++  -O3                \
       -Wall              \
       -std=c++0x         \
	  memory_block.cpp   \
       driver_program.cpp \
       -o x_gnu_ubu
