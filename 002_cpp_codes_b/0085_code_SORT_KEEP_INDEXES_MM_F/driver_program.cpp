//=======================//
// sort and keep indexes //
//=======================//
// BEST OF ALL //
//=============//

#include <iostream>
#include <vector>
#include <algorithm>

using std::endl;
using std::cout;
using std::vector;
using std::sort;

// the main function

int main()
{
    // local variables

    vector<int> vA( {2, 3, 4, 1, -1});
    vector<int> vB;

    // get the dimension of the vector

    unsigned int DIM = vA.size();

    // build the vector vB with the pairs

    for (unsigned int i = 0; i != DIM; i++) {
        vB.push_back(i);
    }

    // some output

    cout << " --> vA" << endl;

    cout << " vA[0] = " << vA[0] << endl;
    cout << " vA[1] = " << vA[1] << endl;
    cout << " vA[2] = " << vA[2] << endl;
    cout << " vA[3] = " << vA[3] << endl;
    cout << " vA[4] = " << vA[4] << endl;

    // some output

    cout << " --> vB --> before sorting" << endl;

    cout << " vB[0] = " << vB[0] << endl;
    cout << " vB[1] = " << vB[1] << endl;
    cout << " vB[2] = " << vB[2] << endl;
    cout << " vB[3] = " << vB[3] << endl;
    cout << " vB[4] = " << vB[4] << endl;

    // sort indexes vector vB based on comparing values in vA

    sort(vB.begin(), vB.end(),
    [&vA](const int & i1, const int & i2) {
        return vA[i1] < vA[i2];
    });

    // some output

    cout << " --> vB --> after sorting" << endl;

    cout << " vB[0]  = " << vB[0] << endl;
    cout << " vB[1]  = " << vB[1] << endl;
    cout << " vB[2]  = " << vB[2] << endl;
    cout << " vB[3]  = " << vB[3] << endl;
    cout << " vB[4]  = " << vB[4] << endl;

    return 0;
}

// END
