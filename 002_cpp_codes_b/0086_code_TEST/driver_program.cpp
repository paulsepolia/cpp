
#include <iostream>

using std::endl;
using std::cout;

// the main function

int main()
{
    int k1 = 10;

    int * const a = &k1 ;

    cout << "a = " << a << endl;
    cout << "*a = " << *a << endl;

    *a = 20;

    cout << "*a = " << *a << endl;


    int p[2] = {2,4};
    (*p)++;
    cout << p[0] << endl;

    unsigned int bmf = 48;
    cout << (bmf == 38) ? 15:10;

    int i;
    for (int i = 0; i < 4 ; i++);
    cout << i << endl;

    int aa[10] = {1};

    cout << aa[5] << endl;

    return 0;
}

// END
