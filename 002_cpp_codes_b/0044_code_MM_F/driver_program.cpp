
//===========================//
// usage of move constructo  //
// move assignment operator  //
//===========================//

#include <iostream>
#include <cmath>
#include <cstring>
#include <ctime>
#include <iomanip>
#include <utility>

using std::cout;
using std::endl;
using std::pow;
using std::clock;
using std::fixed;
using std::setprecision;
using std::move;

// type definition

typedef unsigned long int uli;

// the container class

class Array {
public:

    // conctructor

    Array(uli dim) : m_dim(dim)
    {
        p = new double [m_dim];
    }

    // copy constructor

    Array(const Array & other)
    {
        this->m_dim = other.m_dim;
        this->p = new double [this->m_dim];
        for (uli i = 0; i != this->m_dim; i++) {
            this->p[i] = other.p[i];
        }
    }

    // move construnctor

    Array(Array && other)
    {
        cout << " --> move constructor" << endl;
        this->m_dim = move(other.m_dim);
        this->p = move(other.p);
        other.p = 0;
    }

    // copy assignment operator # 1

    const Array & operator = (const Array & other)
    {
        for (uli i = 0; i != this->m_dim; i++) {
            this->p[i] = other.p[i];
        }
        return *this;
    }

    // copy assignment operator # 2

    const Array & operator = (const double & elem)
    {
        for (uli i = 0; i != this->m_dim; i++) {
            this->p[i] = elem;
        }
        return *this;
    }

    // move assignment operator # 1

    Array & operator = (Array && other)
    {
        this->m_dim = move(other.m_dim);
        this->p = move(other.p);
        other.p = 0;
        return *this;
    }

    // destructor

    ~Array()
    {
        if (p != 0) {
            delete [] p;
            p = 0;
        }
    }

private:

    uli m_dim;
    double * p;

};

// the main function

int main()
{
    // local parameters and variables

    uli DIM = 2*static_cast<uli>(pow(10.0, 8.0));
    uli TRIALS1 = 2*static_cast<uli>(pow(10.0, 0.0));
    uli TRIALS2 = 1*static_cast<uli>(pow(10.0, 1.0));
    uli TRIALS3 = 2*static_cast<uli>(pow(10.0, 7.0));

    time_t t1;
    time_t t2;

    // set the output field

    cout << fixed;
    cout << setprecision(10);

    // play game

    Array A1(DIM);
    Array A2(DIM);

    cout << " --> build A1 and A2" << endl;

    t1 = clock();

    for (uli i = 0; i != TRIALS1; i++) {
        A1 = 10.0;
        A2 = 11.1;
    }

    t2 = clock();
    cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

    // copy assignment operator

    cout << " --> A1 = A2;" << endl;

    t1 = clock();

    for (uli i = 0; i != TRIALS2; i++) {
        A1 = A2;
        A2 = A1;
    }

    t2 = clock();
    cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

    // move assignment operator

    cout << " --> A1 = move(A2);" << endl;
    cout << " --> A2 = move(A1);" << endl;

    t1 = clock();

    for (uli i = 0; i != TRIALS3; i++) {
        A1 = move(A2);
        A2 = move(A1);
    }

    t2 = clock();
    cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

    return 0;
}

// end
