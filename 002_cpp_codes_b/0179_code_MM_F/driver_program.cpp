//================//
// memcpy example //
//================//

#include <iostream>
#include <vector>
#include <cmath>
#include <cstring>

using std::cout;
using std::endl;
using std::vector;
using std::pow;

// the main function

int main()
{
    // local parameters

    const unsigned int DIM(static_cast<unsigned int>(pow(10.0, 4.0)));

    vector<int> v1 {};
    vector<int> v2 {};

    cout << "------------------------------------------------->> 1" << endl;
    cout << " v1.size()     = " << v1.size() << endl;
    cout << " v2.size()     = " << v2.size() << endl;
    cout << " v1.capacity() = " << v1.capacity() << endl;
    cout << " v2.capacity() = " << v2.capacity() << endl;

    // build the vector

    for(unsigned int i = 0; i != DIM; i++) {
        v1.push_back(i);
    }

    cout << "------------------------------------------------->> 2" << endl;

    cout << " v1.size()     = " << v1.size() << endl;
    cout << " v2.size()     = " << v2.size() << endl;
    cout << " v1.capacity() = " << v1.capacity() << endl;
    cout << " v2.capacity() = " << v2.capacity() << endl;


    // shrink to fit the vector

    v1.shrink_to_fit();
    v2.resize(v1.size());

    cout << "------------------------------------------------->> 3" << endl;

    cout << " v1.size()     = " << v1.size() << endl;
    cout << " v2.size()     = " << v2.size() << endl;
    cout << " v1.capacity() = " << v1.capacity() << endl;
    cout << " v2.capacity() = " << v2.capacity() << endl;

    // using memcpy to copy vector<int>

    memcpy(&v2[0], &v1[0], sizeof(v1));

    cout << " --> v1[0] = " << v1[0] << endl;
    cout << " --> v1[1] = " << v1[1] << endl;
    cout << " --> v1[2] = " << v1[2] << endl;
    cout << " --> v1[3] = " << v1[3] << endl;

    cout << " --> v2[0] = " << v2[0] << endl;
    cout << " --> v2[1] = " << v2[1] << endl;
    cout << " --> v2[2] = " << v2[2] << endl;
    cout << " --> v2[3] = " << v2[3] << endl;

    cout << "------------------------------------------------->> 4" << endl;

    cout << " v1.size()     = " << v1.size() << endl;
    cout << " v2.size()     = " << v2.size() << endl;
    cout << " v1.capacity() = " << v1.capacity() << endl;
    cout << " v2.capacity() = " << v2.capacity() << endl;

    return 0;
}

// end
