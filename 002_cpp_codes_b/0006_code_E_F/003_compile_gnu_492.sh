#!/bin/bash

  # 1. compile

  g++-4.9.2  -O3                \
             -Wall              \
             -std=c++0x         \
             driver_program.cpp \
             -o x_gnu_492
