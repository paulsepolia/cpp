//===================//
// function pointers //
//===================//

#include <iostream>
#include <iomanip>
#include <string>
#include <cmath>
#include <sstream>

using std::cout;
using std::endl;
using std::setw;
using std::showpos;
using std::showpoint;
using std::fixed;
using std::boolalpha;
using std::setprecision;
using std::right;
using std::string;
using std::to_string;
using std::sin;
using std::stringstream;

// the main function

int main()
{
    // # 1 --> real to string

    const int I_MAX(1000);

    string s1;
    stringstream ss1;

    for(int i = 0; i != I_MAX; i++) {

        // set the output field of the stringstream

        ss1 << fixed;
        ss1 << setprecision(20);
        ss1 << showpos;
        ss1 << showpoint;

        // fill the stream

        ss1 << sin(i);

        // convert stringstream to string

        ss1 >> s1;

        // output

        cout << "sin(" << setw(5) << right << i << ")= "
             << setw(30) << right << s1 << endl;

        // clear the string stream

        ss1.clear();
    }

    return 0;
}

// end
