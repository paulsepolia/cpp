#ifndef MEMORY_BLOCK_H
#define MEMORY_BLOCK_H

class MemoryBlock {
public:

    // simple constructor that initializes the resource

    explicit MemoryBlock(const unsigned long int &);

    // destructor

    virtual ~MemoryBlock();

    // copy constructor

    MemoryBlock(const MemoryBlock &);

    // copy assignment operator

    MemoryBlock & operator=(const MemoryBlock &);

    // Retrieves the length of the data resource

    unsigned long int Length() const;

    // Move constructor

    MemoryBlock(MemoryBlock &&);

    // Move assignment operator

    MemoryBlock & operator=(MemoryBlock &&);

private:

    unsigned long int m_length; // the length of the resource
    int * m_data; // the resource
};

#endif // MEMORY_BLOCK_H
