//==============//
// late binding //
//==============//

#include <iostream>

using std::endl;
using std::cout;

// class --> B1

class B1 {
public:

    // constructor

    B1(): x1(1), x2(2), x3(3) {}

    int x1;
    int x2;
    int x3;

    virtual  ~B1() {};
    // WARNING: A VIRTUAL DETSRUCT MAKES THE CLASS POLYMORPHIC
};

// class --> D1

class D1: public B1 {
public:

    // constructor

    D1(): B1(), x4(4)
    {
        x2 = 22;
    }

    int x4;
};

// class --> D2

class D2: public D1 {
public:

    // constructor

    D2(): D1(), x5(5)
    {
        x2 = 222;
        x4 = 444;
    }

    int x5;
};

// the main function

int main()
{
    // local variables

    B1 b1;
    D1 d1;
    D2 d2;
    B1 * pb1(nullptr);
    D1 * pd1(nullptr);
    D2 * pd2(nullptr);

    // # 1

    cout << "--------------------------------------->>  1" << endl;

    cout << " --> b1.x1 = " << b1.x1 << endl;
    cout << " --> b1.x2 = " << b1.x2 << endl;
    cout << " --> b1.x3 = " << b1.x3 << endl;

    // # 2

    cout << "--------------------------------------->>  2" << endl;

    cout << " --> d1.x1 = " << d1.x1 << endl;
    cout << " --> d1.x2 = " << d1.x2 << endl;
    cout << " --> d1.x3 = " << d1.x3 << endl;
    cout << " --> d1.x4 = " << d1.x4 << endl;

    // # 3

    cout << "--------------------------------------->>  3" << endl;

    cout << " --> d2.x1 = " << d2.x1 << endl;
    cout << " --> d2.x2 = " << d2.x2 << endl;
    cout << " --> d2.x3 = " << d2.x3 << endl;
    cout << " --> d2.x4 = " << d2.x4 << endl;
    cout << " --> d2.x5 = " << d2.x5 << endl;

    // # 4

    cout << "--------------------------------------->>  4" << endl;

    pb1 = &b1;

    cout << " --> pb1->x1 = " << pb1->x1 << endl;
    cout << " --> pb1->x2 = " << pb1->x2 << endl;
    cout << " --> pb1->x3 = " << pb1->x3 << endl;

    // # 5

    cout << "--------------------------------------->>  5" << endl;

    pd1 = &d1;

    cout << " --> pd1->x1 = " << pd1->x1 << endl;
    cout << " --> pd1->x2 = " << pd1->x2 << endl;
    cout << " --> pd1->x3 = " << pd1->x3 << endl;
    cout << " --> pd1->x4 = " << pd1->x4 << endl;

    // # 6

    cout << "--------------------------------------->>  6" << endl;

    pd2 = &d2;

    cout << " --> pd2->x1 = " << pd2->x1 << endl;
    cout << " --> pd2->x2 = " << pd2->x2 << endl;
    cout << " --> pd2->x3 = " << pd2->x3 << endl;
    cout << " --> pd2->x4 = " << pd2->x4 << endl;
    cout << " --> pd2->x5 = " << pd2->x5 << endl;

    // # 7

    cout << "--------------------------------------->>  7" << endl;

    //===========//
    // UPCASTING //
    //===========//

    // ======================================================//
    // The way to treat a derived pointer via a base pointer //
    // The automatic UPCAST ALWAYS WORKS                     //
    // static/dynamic/explicit cast do not needed            //
    //=======================================================//

    cout << " --> pb1 = &d1;" << endl;

    pb1 = &d1;

    cout << " --> pb1->x1 = " << pb1->x1 << endl;
    cout << " --> pb1->x2 = " << pb1->x2 << endl;
    cout << " --> pb1->x3 = " << pb1->x3 << endl;

    cout << "--------------------------------------->>  8" << endl;

    cout << " --> pb1 = dynamic_cast<B1*>(&d1);" << endl;

    pb1 = dynamic_cast<B1*>(&d1);

    cout << " --> pb1->x1 = " << pb1->x1 << endl;
    cout << " --> pb1->x2 = " << pb1->x2 << endl;
    cout << " --> pb1->x3 = " << pb1->x3 << endl;

    cout << "--------------------------------------->>  9" << endl;

    cout << " --> pb1 = static_cast<B1*>(&d1);" << endl;

    pb1 = static_cast<B1*>(&d1);

    cout << " --> pb1->x1 = " << pb1->x1 << endl;
    cout << " --> pb1->x2 = " << pb1->x2 << endl;
    cout << " --> pb1->x3 = " << pb1->x3 << endl;

    cout << "--------------------------------------->> 10" << endl;

    cout << " --> pb1 = (B1*)(&d1);" << endl;

    pb1 = (B1*)(&d1);

    cout << " --> pb1->x1 = " << pb1->x1 << endl;
    cout << " --> pb1->x2 = " << pb1->x2 << endl;
    cout << " --> pb1->x3 = " << pb1->x3 << endl;

    cout << "--------------------------------------->> 11" << endl;

    cout << " --> pd1 = &d2;" << endl;

    pd1 = &d2;

    cout << " --> pd1->x1 = " << pd1->x1 << endl;
    cout << " --> pd1->x2 = " << pd1->x2 << endl;
    cout << " --> pd1->x3 = " << pd1->x3 << endl;
    cout << " --> pd1->x4 = " << pd1->x4 << endl;

    cout << "--------------------------------------->> 12" << endl;

    cout << " --> pd1 = dynamic_cast<D1*>(&d2);" << endl;

    pd1 = dynamic_cast<D1*>(&d2);

    cout << " --> pd1->x1 = " << pd1->x1 << endl;
    cout << " --> pd1->x2 = " << pd1->x2 << endl;
    cout << " --> pd1->x3 = " << pd1->x3 << endl;
    cout << " --> pd1->x4 = " << pd1->x4 << endl;

    cout << "--------------------------------------->> 13" << endl;

    cout << " --> pd1 = static_cast<D1*>(&d2);" << endl;

    pd1 = static_cast<D1*>(&d2);

    cout << " --> pd1->x1 = " << pd1->x1 << endl;
    cout << " --> pd1->x2 = " << pd1->x2 << endl;
    cout << " --> pd1->x3 = " << pd1->x3 << endl;
    cout << " --> pd1->x4 = " << pd1->x4 << endl;

    cout << "--------------------------------------->> 14" << endl;

    cout << " --> pd1 = (D1*)(&d2);" << endl;

    pd1 = (D1*)(&d2);

    cout << " --> pd1->x1 = " << pd1->x1 << endl;
    cout << " --> pd1->x2 = " << pd1->x2 << endl;
    cout << " --> pd1->x3 = " << pd1->x3 << endl;
    cout << " --> pd1->x4 = " << pd1->x4 << endl;

    //=============//
    // DOWNCASTING //
    //=============//

    // ======================================================//
    // The way to treat a base pointer via a derived pointer //
    // The DOWNCAST needs attension                          //
    // static/dynamic are the tools to do the job            //
    //=======================================================//

    cout << "--------------------------------------->> 15" << endl;

    cout << " --> pd2 = static_cast<D2*>(&b1);" << endl;

    pd2 = static_cast<D2*>(&b1);

    if(pd2) {
        cout << " --> pd2->x1 = " << pd2->x1 << endl;
        cout << " --> pd2->x2 = " << pd2->x2 << endl;
        cout << " --> pd2->x3 = " << pd2->x3 << endl;
        cout << " --> pd2->x4 = " << pd2->x4 << endl; // RUBBISH VALUES
        cout << " --> pd2->x5 = " << pd2->x5 << endl; // RUBBISH VALUES
    }

    cout << "--------------------------------------->> 16" << endl;

    cout << " --> pd2 = dynamic_cast<D2*>(&b1);" << endl;

    pd2 = dynamic_cast<D2*>(&b1); // NEVER SUCCEEDS

    if(pd2) {
        cout << " --> pd2->x1 = " << pd2->x1 << endl;
        cout << " --> pd2->x2 = " << pd2->x2 << endl;
        cout << " --> pd2->x3 = " << pd2->x3 << endl;
        cout << " --> pd2->x4 = " << pd2->x4 << endl; // RUBBISH VALUES
        cout << " --> pd2->x5 = " << pd2->x5 << endl; // RUBBISH VALUES
    }

    cout << "--------------------------------------->> 17" << endl;

    cout << " --> pd2 = (D2*)(&b1);" << endl;

    pd2 = (D2*)(&b1);

    if(pd2) {
        cout << " --> pd2->x1 = " << pd2->x1 << endl;
        cout << " --> pd2->x2 = " << pd2->x2 << endl;
        cout << " --> pd2->x3 = " << pd2->x3 << endl;
        cout << " --> pd2->x4 = " << pd2->x4 << endl; // RUBBISH VALUES
        cout << " --> pd2->x5 = " << pd2->x5 << endl; // RUBBISH VALUES
    }

    return 0;
}

// end
