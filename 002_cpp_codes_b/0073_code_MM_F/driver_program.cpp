//============================//
// mismatch algorithm example //
//============================//

#include <iostream>
#include <algorithm>
#include <vector>
#include <utility>

using std::cout;
using std::endl;
using std::mismatch;
using std::vector;
using std::pair;

// function

template <typename T>
bool comp_fun (const T & i, const T & j)
{
    return (i == j);
}

// functor

template <typename T>
struct comp_functor {
    bool operator()(const T & i, const T & j)
    {
        return (i == j);
    }
};

// the main function

int main ()
{

    vector<int> myvector;

    for (int i=1; i<6; i++) myvector.push_back (i*10); // myvector: 10 20 30 40 50

    int myints[] = {10,20,80,320,1024};                //   myints: 10 20 80 320 1024

    pair<vector<int>::iterator,int*> mypair;

    // using default comparison:

    mypair = mismatch(myvector.begin(), myvector.end(), myints);

    cout << " --> First mismatching elements: " << *mypair.first;
    cout << " and " << *mypair.second << endl;

    ++mypair.first;
    ++mypair.second;

    // using predicate comparison

    mypair = mismatch(mypair.first, myvector.end(), mypair.second, &comp_fun<int>);

    cout << " --> Second mismatching elements: " << *mypair.first;
    cout << " and " << *mypair.second << endl;

    // using predicate comparison

    ++mypair.first;
    ++mypair.second;

    mypair = mismatch(mypair.first, myvector.end(), mypair.second, comp_functor<int>());

    cout << " --> Third mismatching elements: " << *mypair.first;

    cout << " and " << *mypair.second << endl;

    return 0;
}

// end

