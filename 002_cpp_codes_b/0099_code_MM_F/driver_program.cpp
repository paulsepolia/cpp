//=======================//
// operators overloading //
//=======================//

#include <iostream>
#include <utility>

using std::endl;
using std::cout;
using std::move;

// class A

class A {
public:

    // # 1 --> constructor

    A() : m_val1(-1)
    {
        cout << " --> constructor --> A --> m_val1 = " << m_val1 << endl;
    }

    // # 2 --> copy constructor

    A(const A & obj)
    {
        cout << " --> copy constructor --> A" << endl;
        m_val1 = obj.m_val1;
    }

    // # 3 --> move constructor

    A(A && obj)
    {
        cout << " --> move constructor --> A" << endl;
        m_val1 = move(obj.m_val1);
    }

    // # 4 --> copy assignment operator

    virtual void operator = (const A & obj)
    {
        cout << " --> copy assignment operator --> A" << endl;
        m_val1 = obj.m_val1;
    }

    // # 5 --> move assignment operator

    virtual void operator = (A && obj)
    {
        cout << " --> move assignment operator --> A" << endl;
        m_val1 = move(obj.m_val1);
    }

    // # 6 --> get function

    virtual int get()
    {
        cout << " --> get --> A" << endl;
        return m_val1;
    }

    // # 7 --> set function

    virtual void set(const int & val)
    {
        cout << " --> set --> A" << endl;
        m_val1 = val;
    }

    // # 8 --> destructor

    virtual ~A()
    {
        cout << " --> destructor --> A --> m_val1 = " << m_val1 << endl;
    }

private:

    int m_val1;
};

// class B

class B: public A {

public:

    // # 1 --> constructor

    B() : A(), m_val2(-2)
    {
        cout << " --> constructor --> B --> m_val2 = " << m_val2 << endl;
    }

    // # 2 --> copy constructor

    B(const B & obj)
    {
        cout << " --> copy constructor --> B" << endl;
        m_val2 = obj.m_val2;
    }

    // # 3 --> move constructor

    B(B && obj)
    {
        cout << " --> move constructor --> B" << endl;
        m_val2 = move(obj.m_val2);
    }

    // # 4 --> copy assignment operator

    virtual void operator = (const B & obj)
    {
        cout << " --> copy assignment operator --> B" << endl;
        m_val2 = obj.m_val2;
    }

    // # 5 --> move assignment operator

    virtual void operator = (B && obj)
    {
        cout << " --> move assignment operator --> B" << endl;
        m_val2 = move(obj.m_val2);
    }

    // # 6 --> get function

    virtual int get()
    {
        cout << " --> get --> B" << endl;
        return m_val2;
    }

    // # 7 --> set function

    virtual void set(const int & val)
    {
        cout << " --> set --> B" << endl;
        m_val2 = val;
    }

    // # 8 --> destructor

    virtual ~B()
    {
        cout << " --> destructor --> B --> m_val2 = " << m_val2 << endl;
    }

private:

    int m_val2;
};

// class C

class C: public B {

public:

    // # 1 --> constructor

    C() : B(), m_val3(-3)
    {
        cout << " --> constructor --> C --> m_val3 = " << m_val3 << endl;
    }

    // # 2 --> copy constructor

    C(const C & obj)
    {
        cout << " --> copy constructor --> C" << endl;
        m_val3 = obj.m_val3;
    }

    // # 3 --> move constructor

    C(C && obj)
    {
        cout << " --> move constructor --> C" << endl;
        m_val3 = move(obj.m_val3);
    }

    // # 4 --> copy assignment operator

    virtual void operator = (const C & obj)
    {
        cout << " --> copy assignment operator --> C" << endl;
        m_val3 = obj.m_val3;
    }

    // # 5 --> move assignment operator

    virtual void operator = (C && obj)
    {
        cout << " --> move assignment operator --> C" << endl;
        m_val3 = move(obj.m_val3);
    }

    // # 6 --> get function

    virtual int get()
    {
        cout << " --> get --> C" << endl;
        return m_val3;
    }

    // # 7 --> set function

    virtual void set(const int & val)
    {
        cout << " --> set --> C" << endl;
        m_val3 = val;
    }

    // # 8 --> destructor

    virtual ~C()
    {
        cout << " --> destructor --> C --> m_val3 = " << m_val3 << endl;
    }

private:

    int m_val3;
};

// the main function

int main()
{
    //==============================================//
    // about calling of constructors and destructor //
    // detect the order of calling them             //
    //==============================================//

    // CALL:
    // # 1 --> constructor
    // # 2 --> destructor

    {
        cout << "  0 --> start ----------------------------------------------> 1" << endl;
        cout << "  1 --> A a1;" << endl;
        A a1;
        cout << "  2 --> B b1;" << endl;
        B b1;
        cout << "  3 --> C c1;" << endl;
        C c1;
        cout << "  4 --> exit -----------------------------------------------> 1" << endl;
    }

    cout << " --> MIDDLE --> 1" << endl;

    // CALL:
    // # 1 --> constructor
    // # 2 --> destructor
    // # 3 --> copy constructor
    {
        cout << "  0 --> start ----------------------------------------------> 2" << endl;
        cout << "  1 --> A a1;" << endl;
        A a1;
        cout << "  2 --> A a2(a1);" << endl;
        A a2(a1);
        cout << "  3 --> B b1;" << endl;
        B b1;
        cout << "  4 --> B b2(b1);" << endl;
        B b2(b1);
        cout << "  5 --> C c1;" << endl;
        C c1;
        cout << "  6 --> C c2(c1);" << endl;
        C c2(c1);
        cout << "  7 --> exit -----------------------------------------------> 2" << endl;
    }

    cout << " --> MIDDLE --> 2" << endl;

    // CALL:
    // # 1 --> constructor
    // # 2 --> destructor
    // # 3 --> copy constructor
    // # 4 --> get
    {
        int x;
        cout << "  0 --> start ----------------------------------------------> 3" << endl;
        cout << "  1 --> C c1;" << endl;
        C c1;
        cout << "  2 --> x = c1.get();" << endl;
        x = c1.get();
        cout << "  3 --> x = " << x << endl;
        cout << "  4 --> B b1(c1);" << endl;
        B b1(c1);
        cout << "  5 --> x = b1.get();" << endl;
        x = b1.get();
        cout << "  6 --> x = " << x << endl;
        cout << "  7 --> A a1(b1);" << endl;
        A a1(b1);
        cout << "  8 --> x = a1.get();" << endl;
        x = a1.get();
        cout << "  9 --> x = " << x << endl;
        cout << " 10 --> A a2(c1);" << endl;
        A a2(c1);
        cout << " 11 --> x = a2.get();" << endl;
        x = a2.get();
        cout << " 12 --> x = " << x << endl;
        cout << " 13 --> exit -----------------------------------------------> 3" << endl;
    }

    cout << " --> MIDDLE --> 3" << endl;

    // CALL:
    // # 1 --> constructor
    // # 2 --> destructor
    // # 3 --> set
    // # 4 --> get
    // # 5 --> move
    {
        int x;
        cout << "  0 --> start ----------------------------------------------> 4" << endl;
        cout << "  1 --> C c1;" << endl;
        C c1;
        cout << "  2 --> c1.set(11);" << endl;
        c1.set(11);
        cout << "  3 --> C c2;" << endl;
        C c2;
        cout << "  4 --> c2.set(22);" << endl;
        c2.set(22);
        cout << "  5 --> x = c1.get();" << endl;
        x = c1.get();
        cout << "  6 --> x = " << x << endl;
        cout << "  7 --> x = c2.get();" << endl;
        x = c2.get();
        cout << "  8 --> x = " << x << endl;
        cout << "  9 --> c1 = move(c2);" << endl;
        c1 = move(c2);
        cout << " 10 --> x = c1.get();" << endl;
        x = c1.get();
        cout << " 11 --> x = " << x << endl;
        cout << " 12 --> x = c2.get();" << endl;
        x = c2.get();
        cout << " 13 --> x = " << x << endl;
        cout << " 14 --> A a1;" << endl;
        A a1;
        cout << " 15 --> a1 = move(c1);" << endl;
        a1 = move(c1);
        cout << " 16 --> x = c1.get();" << endl;
        x = c1.get();
        cout << " 17 --> x = " << x << endl;
        cout << " 18 --> x = a1.get();" << endl;
        x = a1.get();
        cout << " 19 --> x = " << x << endl;
    }

    cout << " --> MIDDLE --> 4" << endl;

    return 0;
}

// end
