//=================//
// diamond problem //
//=================//

#include <iostream>

using std::endl;
using std::cout;

// the main function

int main()
{
    const int x1(10);
    const int * px1(&x1);

    cout << "-------------------------------------->> 1" << endl;

    cout << " -->   x1 = " <<   x1 << endl;
    cout << " -->  &x1 = " <<  &x1 << endl;
    cout << " -->  px1 = " <<  px1 << endl;
    cout << " --> *px1 = " << *px1 << endl;
    cout << " --> &px1 = " << &px1 << endl;

    cout << "-------------------------------------->> 2" << endl;

    int & x2 = const_cast<int&>(x1);

    cout << " -->   x2 = " <<   x2 << endl;
    cout << " -->  &x2 = " <<  &x2 << endl;

    cout << "-------------------------------------->> 3" << endl;

    x2 = 20;

    cout << " -->   x1 = " <<   x1 << endl;
    cout << " -->  &x1 = " <<  &x1 << endl;
    cout << " -->  px1 = " <<  px1 << endl;
    cout << " --> *px1 = " << *px1 << endl;
    cout << " --> &px1 = " << &px1 << endl;
    cout << " -->   x2 = " <<   x2 << endl;
    cout << " -->  &x2 = " <<  &x2 << endl;

    return 0;
}

// end
