//=================//
// dynamic binding //
//=================//

#include <iostream>
#include <memory>
#include <string>

using std::cout;
using std::endl;
using std::move;
using std::string;

#define DEBUG_FUN
#undef DEBUG_FUN

// function --> DEBUG

void DEBUG(const string & str)
{
#ifdef DEBUG_FUN
    cout << str << endl;
#endif
}

// class --> point

template<typename T>
class point_basic {
public:

    // default constructor

    point_basic() : _point(0)
    {
        DEBUG("--> point_basic --> default constructor --> 1/1");
    }

    // destructor

    virtual ~point_basic()
    {

        DEBUG("--> point_basic --> destructor --> 1/3");

        if(this->_point) {
            DEBUG("--> point_basic --> destructor --> 2/3");
            delete this->_point;
            DEBUG("--> point_basic --> destructor --> 3/3");
            this->_point = 0;
        }
        DEBUG("--> point_basic --> destructor --> 4/4");
    }

    // copy constructor

    point_basic(const point_basic & obj)
    {
        DEBUG("--> point_basic --> copy constructor --> 1/2");
        this->_point = obj._point;
        DEBUG("--> point_basic --> copy constructor --> 2/2");
    }

    // copy assignment operator

    point_basic & operator = (const point_basic & obj)
    {
        if((this->_point != 0) && (obj._point != 0)) {
            DEBUG("--> point_basic --> copy assignment operator --> 1/2");
            *this->_point = *obj._point;
            DEBUG("--> point_basic --> copy assignment operator --> 2/2");
            return *this;
        } else {
            cout << "ERROR: DEREFERENCE NULL POINTER" << endl;
            exit(-1);
            return *this;
        }
    }

    // move constructor

    point_basic(point_basic && obj)
    {
        DEBUG("--> point_basic --> move constructor --> 1/2");
        this->_point = move(obj._point);
        obj._point = 0;
        DEBUG("--> point_basic --> move constructor --> 2/2");
    }

    // move assignment operator

    point_basic & operator = (point_basic && obj)
    {
        if((this->_point != 0) && (obj._point != 0)) {
            DEBUG("--> point_basic --> move assignment constructor --> 1/2");
            this->_point = move(obj._point);
            obj._point = 0;
            DEBUG("--> point_basic --> move assignment constructor --> 2/2");
            return *this;
        } else {
            cout << "ERROR: DEREFERENCE NULL POINTER" << endl;
            exit(-1);
            return *this;
        }
    }

protected:

    T * _point;
};

// class --> point

template<typename T>
class point: public point_basic<T> {
public:

    // create a point

    bool Create()
    {
        DEBUG("--> Create --> 1/3");
        if(!this->_point) {
            DEBUG("--> Create --> 2/3");
            this->_point = new T(0);
            DEBUG("--> Create --> 3/3");
            return true;
        } else {
            DEBUG("--> Create --> 4/4");
            return false;
        }
    }

    // delete the point

    bool Delete()
    {
        DEBUG("--> Delete --> 1/4");
        if(this->_point) {
            DEBUG("--> Delete --> 2/4");
            delete this->_point;
            DEBUG("--> Delete --> 3/4");
            this->_point = 0;
            DEBUG("--> Delete --> 4/4");
            return true;
        } else {
            DEBUG("--> Delete --> 5/5");
            return false;
        }
    }

    // set the point

    bool Set(const T & val)
    {
        DEBUG("--> Set --> 1/3");
        if(this->_point) {
            DEBUG("--> Set --> 2/3");
            *this->_point = val;
            DEBUG("--> Set --> 3/3");
            return true;
        } else {
            DEBUG("--> Set --> 4/4");
            return false;
        }
    }

    // get the point

    const T & Get()
    {
        DEBUG("--> Get --> 1/2");
        if(this->_point) {
            DEBUG("--> Get --> 2/2");
            return *this->_point;
        } else {
            DEBUG("--> Get --> 3/3");
            cout << "ERROR: DEREFERENCE NULL POINTER" << endl;
            exit(-1);
            T local(-1);
            return move(local);
        }
    }
};

// the main function

int main()
{
    // # 1

    cout << "------------------------------------------------------------>> 1" << endl;

    cout << "point<double> p1" << endl;
    point<double> p1;

    cout << "p1.Create();" << endl;
    p1.Create();

    cout << "p1.Set(10);" << endl;
    p1.Set(10);

    cout << "double v1 = p1.Get();" << endl;
    double v1 = p1.Get();

    cout << " --> v1 = " << v1 << endl;

    cout << "p1.Delete();" << endl;
    p1.Delete();

    cout << "p1.Create();" << endl;
    p1.Create();

    cout << "p1.Set(20);" << endl;
    p1.Set(20);

    cout << "p1.Get();" << endl;
    v1 = p1.Get();

    cout << " --> v1 = " << v1 << endl;

    cout << "p1.Delete();" << endl;
    p1.Delete();

    // # 2

    cout << "------------------------------------------------------------>> 2" << endl;

    cout << "point<double> p2" << endl;
    point<double> p2;

    cout << "p1.Create();" << endl;
    p1.Create();

    cout << "p2.Create();" << endl;
    p2.Create();

    cout << "p1.Set(21);" << endl;
    p1.Set(21);

    cout << "p2 = p1;" << endl;
    p2 = p1;

    cout << "v1 = p1.Get();" << endl;
    v1 = p1.Get();

    cout << "double v2 = p2.Get();" << endl;
    double v2 = p2.Get();

    cout << " --> v1 = " << v1 << endl;
    cout << " --> v2 = " << v2 << endl;

    cout << "p1.Delete();" << endl;
    p1.Delete();

    cout << "p2.Delete();" << endl;
    p2.Delete();

    cout << "----------------------------------------------------------->> 3" << endl;

    cout << "p1.Create();" << endl;
    p1.Create();

    cout << "p2.Create();" << endl;
    p2.Create();

    cout << "p1.Get();" << endl;
    cout << p1.Get() << endl;

    cout << "p2.Get();" << endl;
    cout << p2.Get() << endl;

    cout << "p1 = move(p2);" << endl;
    p1 = move(p2);

    cout << "p1.Delete();" << endl;
    p1.Delete();

    cout << "p2.Delete();" << endl;
    p2.Delete();

    //cout << "p1.Get();" << endl;
    //cout << p1.Get() << endl;

    //cout << "p2.Get();" << endl;
    //cout << p2.Get() << endl;

    cout << "----------------------------------------------------------->> EXIT" << endl;

    return 0;
}

// end
