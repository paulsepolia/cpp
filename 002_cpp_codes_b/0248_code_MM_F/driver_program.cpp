//====================//
// char to ascii code //
//====================//

#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int main()
{
    char ascii;
    cout << "Enter a character: ";
    cin >> ascii;
    cout << "Its ascii value is: " << static_cast<int>(ascii) << endl;

    return 0;
}

// end
