//======================//
// class Vec definition //
//======================//

#include "vec_declaration.hpp"
#include <utility>
#include <iostream>
#include <string>
#include <cstdlib>

using std::move;
using std::cout;
using std::endl;
using std::string;

// constructor

template<typename T>
Vec<T>::Vec() : _size(0), _p()
{}

// destructor

template<typename T>
Vec<T>::~Vec()
{
    cout << " --> destructor --> 1" << endl;
    cout << " --> this->_p.get() = " << this->_p.get() << endl;

    if(_p.get()) {
        _p.reset();
    }

    cout << " --> destructor --> 2" << endl;
    cout << " --> this->_p.get() = " << this->_p.get() << endl;
}

// copy constructor

template<typename T>
Vec<T>::Vec(const Vec<T> & other)
{
    cout << " --> copy constructor --> 1" << endl;
    cout << " --> this->_p.get() = " << this->_p.get() << endl;
    cout << " --> other._p.get() = " << other._p.get() << endl;

    if(this != &other) {
        if(other._p.get()) {
            this->deallocate();
            this->allocate(other._size);
            for(uli i = 0; i != other._size; i++) {
                this->set(i, other.get(i));
            }
        } else if(other._p.get() == 0) {
            this->deallocate();
        }
    }

    cout << " --> copy constructor --> 2" << endl;
    cout << " --> this->_p.get() = " << this->_p.get() << endl;
    cout << " --> other._p.get() = " << other._p.get() << endl;
}

// move constructor

template<typename T>
Vec<T>::Vec(Vec<T> && other)
{
    cout << " --> move constructor --> 1" << endl;
    cout << " --> this->_p.get() = " << this->_p.get() << endl;
    cout << " --> other._p.get() = " << other._p.get() << endl;

    this->_p = move(other._p);

    cout << " --> move constructor --> 2" << endl;
    cout << " --> this->_p.get() = " << this->_p.get() << endl;
    cout << " --> other._p.get() = " << other._p.get() << endl;
}

// copy assignment operator

template<typename T>
Vec<T> & Vec<T>::operator=(const Vec<T> & other)
{
    cout << " --> copy operator= --> 1" << endl;
    cout << " --> this->_p.get() = " << this->_p.get() << endl;
    cout << " --> other._p.get() = " << other._p.get() << endl;

    if(this != &other) {
        if(other._p.get()) {
            this->deallocate();
            this->allocate(other._size);
            for(uli i = 0; i != other._size; i++) {
                this->set(i, other.get(i));
            }
        } else if(other._p.get() == 0) {
            this->deallocate();
        }
    }

    cout << " --> copy operator= --> 2" << endl;
    cout << " --> this->_p.get() = " << this->_p.get() << endl;
    cout << " --> other._p.get() = " << other._p.get() << endl;

    return *this;
}

// move assignment operator

template<typename T>
Vec<T> & Vec<T>::operator=(Vec<T> && other)
{

    cout << " --> move operator= --> 1" << endl;
    cout << " --> this->_p.get() = " << this->_p.get() << endl;
    cout << " --> other._p.get() = " << other._p.get() << endl;

    this->deallocate();
    this->allocate(other._size);
    this->_p = move(other._p);

    cout << " --> move operator= --> 2" << endl;
    cout << " --> this->_p.get() = " << this->_p.get() << endl;
    cout << " --> other._p.get() = " << other._p.get() << endl;

    return *this;
}

//==================//
// member functions //
//==================//

// allocate

template<typename T>
void Vec<T>::allocate(const uli & val_size)
{
    this->deallocate();
    this->_p.reset(new T [val_size]);
    this->_size = val_size;
}

// check_allocation

template<typename T>
bool Vec<T>::check_allocation() const
{
    bool tmp_val = false;
    if(this->_p.get()) {
        tmp_val = true;
    }

    return tmp_val;
}

// deallocate

template<typename T>
void Vec<T>::deallocate()
{
    cout << " --> deallocate --> 1" << endl;
    cout << " --> this->_p.get() = " << this->_p.get() << endl;

    if(this->_p) {
        this->_p.reset();
    }

    cout << " --> deallocate --> 2" << endl;
    cout << " --> this->_p.get() = " << this->_p.get() << endl;
}

// get

template<typename T>
T Vec<T>::get(const uli & index) const throw(string)
{
    if(!this->check_allocation()) {
        throw_error_and_quit(ERROR_001);
    }

    if(((this->get_size()-1) < index) || (index < 0)) {
        throw_error_and_quit(ERROR_002);
    }

    return this->_p.get()[index];
}

// set

template<typename T>
void Vec<T>::set(const uli & index, const T & val)
{
    if(!this->check_allocation()) {
        throw_error_and_quit(ERROR_001);
    }

    if(((this->get_size()-1) < index) || (index < 0)) {
        throw_error_and_quit(ERROR_002);
    }

    this->_p.get()[index] = val;
}

// operator[]

template<typename T>
T Vec<T>::operator[](const uli & index)
{
    return this->get(index);
}

template<typename T>
uli Vec<T>::get_size() const
{
    return this->_size;
}

//==========================//
// private member functions //
//==========================//

// throw_error_and_quit

template<typename T>
string Vec<T>::throw_error_and_quit(string my_str) const
{
    try {
        throw my_str;
    } catch(string & error_code) {
        cout << error_code << endl;
        exit(EXIT_FAILURE);
    }
}

// end
