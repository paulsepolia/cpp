//=============//
// int pointer //
//=============//

#include <cstring>
#include <iostream>
#include <cmath>

using std::cout;
using std::endl;
using std::pow;

// type definitions

typedef unsigned long int uli;
typedef const uli culi;

// the main function

int main()
{
    // int value

    int v1 = 10;

    // int pointer

    int * iptr1(nullptr);

    cout << " -->  iptr1 = &v1" << endl;

    iptr1 = &v1;

    cout << " -->  iptr1 = " <<  iptr1 << endl;
    cout << " --> &iptr1 = " << &iptr1 << endl;
    cout << " --> *iptr1 = " << *iptr1 << endl;

    cout << " -->  iptr1 = 11" << endl;

    *iptr1 = 11;

    cout << " -->  iptr1 = " <<  iptr1 << endl;
    cout << " --> &iptr1 = " << &iptr1 << endl;
    cout << " --> *iptr1 = " << *iptr1 << endl;

    cout << " --> v1 = " << v1 << endl;

    // const int pointer

    // WE CAN change the address where it points to any address of our choice
    // WE CAN NOT change the value of element which is pointer by the above address
    // via the pointer

    const int * iptr2(nullptr);

    iptr2 = &v1;

    cout << " -->  iptr2 = " <<  iptr2 << endl;
    cout << " --> &iptr2 = " << &iptr2 << endl;
    cout << " --> *iptr2 = " << *iptr2 << endl;

    //*iptr2 = 13; // ERROR:: must be a modifiable lvalue

    // const int pointer

    int const * iptr3(nullptr);

    iptr3 = &v1;

    cout << " -->  iptr3 = " <<  iptr3 << endl;
    cout << " --> &iptr3 = " << &iptr3 << endl;
    cout << " --> *iptr3 = " << *iptr3 << endl;

    //*iptr3 = 14; // ERROR:: must be a modifiable lvalue

    // int const pointer

    int * const iptr4(&v1);

    cout << " -->  iptr4 = " <<  iptr4 << endl;
    cout << " --> &iptr4 = " << &iptr4 << endl;
    cout << " --> *iptr4 = " << *iptr4 << endl;

    //iptr4 = &v1; // ERROR:: must be a modifiable lvalue

    cout << " --> *iptr4 = 15" << endl;

    *iptr4 = 15;

    cout << " -->  iptr4 = " <<  iptr4 << endl;
    cout << " --> &iptr4 = " << &iptr4 << endl;
    cout << " --> *iptr4 = " << *iptr4 << endl;

    // const int const pointer

    const int * const iptr5(&v1);

    cout << " -->  iptr5 = " <<  iptr5 << endl;
    cout << " --> &iptr5 = " << &iptr5 << endl;
    cout << " --> *iptr5 = " << *iptr5 << endl;

    //*iptr5 = 16; // ERROR:: must be a modifiable lvalue
    //iptr5 = &v1; // ERROR:: must be a modifiable lvalue

    cout << "---------------------------------------> END" << endl;

    return 0;
}

// end
