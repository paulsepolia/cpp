//==================//
// set_intersection //
//==================//

#include <iostream>
#include <algorithm>
#include <vector>

using std::cout;
using std::endl;
using std::set_intersection;
using std::sort;
using std::vector;

// the main function

int main()
{
    int first[] = {5, 10, 15, 20, 25};
    int second[] = {50, 40, 30, 20, 10};
    vector<int> v(10);  // 0  0  0  0  0  0  0  0  0  0
    vector<int>::iterator it;

    // sort

    sort(first, first+5);     //  5 10 15 20 25
    sort(second, second+5);   // 10 20 30 40 50

    // set_intersection

    it = set_intersection(first, first+5, second, second+5, v.begin());

    // 10 20  0  0  0  0  0  0  0  0

    v.resize(it-v.begin()); // 10 20

    cout << " --> the intersection has " << (v.size()) << " elements: " << endl;

    for (it = v.begin(); it != v.end(); ++it) {
        cout << ' ' << *it;
    }

    cout << endl;

    return 0;
}

// end
