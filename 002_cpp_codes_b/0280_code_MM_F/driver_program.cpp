//=============//
// linked list //
//=============//

#include <cmath>
#include <iostream>
using std::endl;
using std::cout;

#define DEBUG
//#undef DEBUG

// list definition

class list {
public:
    int data;
    list * next;
};

typedef unsigned long int uli;

// the main function

int main()
{
    const uli DIM(std::pow(10.0, 8.0));
    const uli TR(std::pow(10.0, 1.0));

    for(uli k = 0; k != TR; k++) {
        cout << "---------------------------------------->> k = " << k << endl;

        // # 1 create nodes

        list * head(new list);
        list * node(new list);

        // initialize the head

        head->next = node;

        // build the nodes

        for(uli i = 0; i != DIM; i++) {
            node->data = i;
            node->next = new list;
            node = node->next;
        }

        // finalize the nodes

        node->next = 0;

        // delete the nodes
        node = head;
        while(node->next) {
            list * q;
            q = node->next;
            node->next = q->next;
            delete q;
        }

        // delete the head
        delete head;
    }

    return 0;
}

// END
