//===================//
// function pointers //
//===================//

#include <iostream>

using std::cout;
using std::cin;
using std::endl;

// function --> funA # 1

void funA(int x)
{
    cout << " --> funA # 1 --> x = " << x << endl;
}

// function --> funA # 2

void funA(const int & x)
{
    cout << " --> funA # 2 --> x = " << x << endl;
}

// function --> funB # 1

double funB(double x)
{
    cout << " --> funB # 1 --> x = " << x << endl;
    return x;
}

// function --> funB # 2

double funB(const double & x)
{
    cout << " --> funB # 2 --> x = " << x << endl;
    return x;
}

// the main function

int main()
{
    //=======================//
    // function --> funA # 1 //
    //=======================//

    // declare a pointer to a function which takes an integer
    // and returns a void

    void (*foo1)(int);
    void (*foo2)(int);

    // the ampersand is actually optional

    foo1 = &funA;
    foo2 = funA;

    foo1(3);
    foo2(4);

    //=======================//
    // function --> funA # 2 //
    //=======================//

    void (*foo3)(const int &);
    void (*foo4)(const int &);

    // the ampersand is actually optional

    foo3 = &funA;
    foo4 = funA;

    foo3(5);
    foo4(6);

    //=======================//
    // function --> funB # 1 //
    //=======================//

    double (*foo5)(double);
    double (*foo6)(double);

    // the ampersand is actually optional

    foo5 = &funB;
    foo6 = funB;

    foo5(7);
    foo6(8);

    //=======================//
    // function --> funB # 2 //
    //=======================//

    double (*foo7)(const double &);
    double (*foo8)(const double &);

    // the ampersand is actually optional

    foo7 = &funB;
    foo8 = funB;

    foo7(9);
    foo8(10);

    return 0;
}

// end
