//===========//
// recursion //
//===========//

#include <iostream>
#include <iomanip>
#include <cmath>
#include <chrono>

using std::cout;
using std::endl;
using namespace std::chrono;

// # 1
// function pow

double pow_v1(double x, int n)
{
    // # 1
    // corner case

    if(n == 0) {
        return 1.0;
    }

    // # 2
    // implementation

    double res(1.0);

    for(int i = 0; i < n; i++) {
        res = res * x;
    }

    if(n < 0) {
        res = 1/res;
    }

    return res;
}

// # 2
// function pow

double pow_v2(double x, int n)
{
    // # 1
    // corner case

    if(n == 0) {
        return 1.0;
    }

    // # 2
    // implementation

    double temp(0);

    temp = pow_v2(x, n/2);

    if(n%2 == 0) {
        return temp * temp;
    } else {
        if(n > 0) {
            return temp * temp * x;
        } else {
            return (temp * temp) / x;
        }
    }
}

// the main function

int main()
{
    double res(0);
    double x(0);
    int n(0);
    auto t1 = system_clock::now();
    auto t2 = system_clock::now();
    duration<double> time_span;
    cout << std::fixed;
    cout << std::showpos;
    cout << std::setprecision(10);

    // # 1

    x = 0.0001;
    n = 10;
    res = pow_v1(x, n);
    cout << "  1 --> res       = " << std::setw(15) << std::right << res << endl;

    // # 2

    x = 0.0001;
    n = std::pow(2.0, 30);
    t1 = system_clock::now();
    res = pow_v1(x, n);
    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << "  2 --> res       = " << std::setw(15) << std::right << res << endl;
    cout << "    --> time used = " << std::setw(15) << std::right << time_span.count() << endl;

    // # 3

    x = 0.0001;
    n = std::pow(2.0, 30);
    t1 = system_clock::now();
    res = pow_v2(x, n);
    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << "  3 --> res       = " << std::setw(15) << std::right << res << endl;
    cout << "    --> time used = " << std::setw(15) << std::right << time_span.count() << endl;

    // # 4

    x = 1.0;
    n = -std::pow(2.0, 29);
    t1 = system_clock::now();
    res = pow_v1(x, n);
    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << "  4 --> res       = " << std::setw(15) << std::right << res << endl;
    cout << "    --> time used = " << std::setw(15) << std::right << time_span.count() << endl;

    // # 5

    x = 1.0;
    n = -std::pow(2.0, 29);
    t1 = system_clock::now();
    res = pow_v2(x, n);
    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << "  5 --> res       = " << std::setw(15) << std::right << res << endl;
    cout << "    --> time used = " << std::setw(15) << std::right << time_span.count() << endl;
    return 0;
}

// end
