//=========================================//
// late binding 					   //
// I DO NOT THINK IT IS LATE BINDING SINCE //
// THE COMPILER DETECTS IF THERE IS NO     //
// TYPES TO BE ABLE TO ASSIGN              //
//=========================================//

#include <iostream>
#include <utility>

using std::endl;
using std::cout;
using std::move;

// function definition

// # 1

const int & add_fun(int x, int y)
{
    cout << " --> add_fun(int, int)" << endl;

    return move(x+y);
}

// # 2

const int & add_fun(int & x, int & y)
{
    cout << " --> add_fun(int &, int &)" << endl;

    return move(x+y);
}

// # 3

const int & add_fun(const int & x, const int & y)
{
    cout << " --> add_fun(const int &, const int &)" << endl;

    return move(x+y);
}

// the main function

int main()
{
    // create a function pointer
    // and make it point to the add function

    int vFive(5);
    int vThree(3);

    // # 1

    const int & (*p1_fun)(int, int)(nullptr); // function pointer

    cout << "  p1_fun = " <<  p1_fun << endl;
    cout << " &p1_fun = " << &p1_fun << endl;
    cout << " *p1_fun = " << *p1_fun << endl;

    p1_fun = add_fun;

    int val(p1_fun(vFive, vThree));

    cout << " --> val = " << val << endl;

    // # 2

    const int & (*p2_fun)(int &, int &)(nullptr); // function pointer

    cout << " --> Before" << endl;

    cout << "  p2_fun = " <<  p2_fun << endl;
    cout << " &p2_fun = " << &p2_fun << endl;
    cout << " *p2_fun = " << *p2_fun << endl;

    p2_fun = add_fun;

    cout << " --> After --> 1" << endl;

    cout << "  p2_fun = " <<  p2_fun << endl;
    cout << " &p2_fun = " << &p2_fun << endl;
    cout << " *p2_fun = " << *p2_fun << endl;

    val = p2_fun(vFive, vThree);

    cout << " --> After --> 2" << endl;

    cout << "  p2_fun = " <<  p2_fun << endl;
    cout << " &p2_fun = " << &p2_fun << endl;
    cout << " *p2_fun = " << *p2_fun << endl;

    cout << " --> val = " << val << endl;

    // # 3

    const int & (*p3_fun)(const int &, const int &)(nullptr); // function pointer

    cout << "  p3_fun = " <<  p3_fun << endl;
    cout << " &p3_fun = " << &p3_fun << endl;
    cout << " *p3_fun = " << *p3_fun << endl;

    p3_fun = add_fun;

    val = p3_fun(vFive, vThree);

    cout << " --> val = " << val << endl;

    return 0;
}

// end
