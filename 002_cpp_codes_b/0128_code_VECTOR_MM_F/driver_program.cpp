//================================//
// driver program to Vector class //
//================================//

#include "Vector.h"
#include <iostream>
#include <iomanip>

using std::cout;
using std::cin;
using std::endl;
using std::fixed;
using std::setprecision;
using std::boolalpha;

// the main function

int main()
{
    // # 0

    cout << fixed;
    cout << setprecision(5);
    cout << boolalpha;

    return 0;
}

// end
