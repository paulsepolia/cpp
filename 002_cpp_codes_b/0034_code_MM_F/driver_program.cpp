//==============//
// move example //
//==============//

#include <utility>      // std::move
#include <iostream>     // std::cout
#include <vector>       // std::vector
#include <string>       // std::string

using std::endl;
using std::cout;
using std::vector;
using std::string;
using std::move;

// the main function

int main ()
{
    string foo = "foo-string";
    string bar = "bar-string";
    vector<string> myvector;

    myvector.push_back(foo);               // copies
    myvector.push_back(move(bar));         // moves

    cout << " --> myvector contains: " << endl;

    for (string& x:myvector) {
        cout << ' ' << x << endl;
    }

    cout << " foo = " << foo << endl;
    cout << " bar = " << bar << endl;

    return 0;
}

// END
