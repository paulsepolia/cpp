#include <iostream>
#include <cstdint>
#include <memory>
#include <iomanip>

// Tabulated version to find factorial x

const uint64_t MAXN = 10000;

uint64_t dp[MAXN];

long double dp_ld[MAXN];

uint64_t factorial(const uint64_t &n) {

    dp[0] = 1;

    for (std::uint64_t i = 1; i <= n; i++) {
        dp[i] = dp[i - 1] * i;
    }

    return dp[n];
}

long double factorial_ld(const uint64_t &n) {

    dp_ld[0] = 1;

    for (std::uint64_t i = 1; i <= n; i++) {
        dp_ld[i] = dp_ld[i - 1] * static_cast<long double>(i);
    }

    return dp_ld[n];
}

// return fact x!
uint64_t solve(uint64_t x) {

    if (x == 0) {
        return 1;
    }

    if (dp[x] != 0) {
        return dp[x];
    }

    return (dp[x] = x * solve(x - 1));
}

// return fact x!
long double solve_ld(std::uint64_t x) {

    if (x == 0) {
        return 1;
    }

    if (dp_ld[x] != -1) {
        return dp_ld[x];
    }

    return (dp_ld[x] = x * solve_ld(x - 1));
}

int main() {

    {
        std::cout << " --> example --> 1 -------------------> start" << std::endl;

        std::cout << " --> factorial(10) = " << factorial(10) << std::endl;
        std::cout << " --> factorial(11) = " << factorial(11) << std::endl;
        std::cout << " --> factorial(12) = " << factorial(12) << std::endl;

        std::cout << " --> example --> 1 -------------------> end" << std::endl;
    }

    {
        std::cout << " --> example --> 2 -------------------> start" << std::endl;

        for (auto &el: dp) {
            el = -1;
        }

        std::cout << " --> factorial(10) = " << solve(10) << std::endl;
        std::cout << " --> factorial(11) = " << solve(11) << std::endl;
        std::cout << " --> factorial(12) = " << solve(12) << std::endl;
        std::cout << " --> factorial(20) = " << solve(20) << std::endl;

        std::cout << " --> example --> 2 -------------------> end" << std::endl;
    }

    {
        std::cout << " --> example --> 3 -------------------> start" << std::endl;

        for (auto &el: dp) {
            el = 0;
        }

        std::cout << " --> factorial(20) = " << solve(20) << std::endl;

        std::cout << " --> example --> 3 -------------------> end" << std::endl;
    }

    {
        std::cout << " --> example --> 4 -------------------> start" << std::endl;

        std::cout << " --> factorial(10) = " << factorial_ld(10) << std::endl;
        std::cout << " --> factorial(11) = " << factorial_ld(11) << std::endl;
        std::cout << " --> factorial(12) = " << factorial_ld(12) << std::endl;

        for (std::uint64_t i = 0; i < 1000; i++) {
            std::cout << std::setprecision(15) << " --> factorial("
                      << i << ") = " << std::setw(30)
                      << std::right << std::scientific << factorial_ld(i) << std::endl;
        }

        std::cout << " --> example --> 4 -------------------> end" << std::endl;
    }

    {
        std::cout << " --> example --> 5 -------------------> start" << std::endl;

        std::cout << " --> solve_ld(10) = " << solve_ld(10) << std::endl;
        std::cout << " --> solve_ld(11) = " << solve_ld(11) << std::endl;
        std::cout << " --> solve_ld(12) = " << solve_ld(12) << std::endl;

        for (std::uint64_t i = 0; i < 1000; i++) {
            std::cout << std::setprecision(15) << " --> factorial("
                      << i << ") = " << std::setw(30)
                      << std::right << std::scientific << solve_ld(i) << std::endl;
        }

        std::cout << " --> example --> 5 -------------------> end" << std::endl;
    }
}