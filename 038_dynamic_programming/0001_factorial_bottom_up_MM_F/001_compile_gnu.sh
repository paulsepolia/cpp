#!/bin/bash

  g++-9.1   -O3         \
            -Wall       \
            -std=c++2a  \
            -pthread    \
            -fopenmp    \
            -pedantic   \
            main.cpp    \
            -o x_gnu

