#include <iostream>
#include <string>
#include <sqlite3.h>
#include <chrono>

using namespace std::chrono;

int callback(void *NotUsed, int32_t argc, char **argv, char **azColName) {

    for (int32_t i = 0; i < argc; i++) {

        std::cout << azColName[i] << " " << (argv[i] ? argv[i] : "NULL") << std::endl;
    }

    return 0;
}

int main() {

    const std::string flag("A");
    const uint32_t I_MAX(200000);
    sqlite3 *db(0);
    char *zErrMsg(0);
    int32_t rc(0);
    const char *sql(0);

    // Open database

    rc = sqlite3_open("test.db", &db);

    if (rc) {
        std::cout << "Can't open database: " << sqlite3_errmsg(db) << std::endl;
        return -1;
    } else {
        std::cout << "Opened database successfully" << std::endl;
    }

    auto t1(system_clock::now());
    auto t2(system_clock::now());
    duration<double> time_span{};

    for (uint32_t i = 1; i <= I_MAX; i++) {

        if(i%1000 == 0) {

            std::cout << "----------------------------------->> i = " << i << std::endl;
            t2 = system_clock::now();
            time_span = duration_cast<duration<double>>(t2-t1);
            std::cout << " --> time used = " << time_span.count() << std::endl;
            t1 = t2;
        }

        //==============//
        // CREATE TABLE //
        //==============//

        // Create SQL statement

        const std::string sql_str1("CREATE TABLE COMPANY" + std::string(flag) + std::to_string(i) + "("  \
        "ID INT PRIMARY KEY     NOT NULL," \
        "NAME           TEXT    NOT NULL," \
        "AGE            INT     NOT NULL," \
        "ADDRESS        CHAR(50)," \
        "SALARY         REAL );");

        sql = &sql_str1[0];

        // Execute SQL statement

        rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);

        if (rc != SQLITE_OK) {

            std::cout << "SQL error: " << zErrMsg << std::endl;
            sqlite3_free(zErrMsg);

        } else {

            //std::cout << "Table created successfully ---->> i = " << i << std::endl;
        }

        //==========================//
        // INSERT DATA TO THE TABLE //
        //==========================//

        // Create SQL statement

        const std::string sql_str2("INSERT INTO COMPANY" +
                                           std::string(flag) +
                                           std::to_string(i) +
                                           " (ID,NAME,AGE,ADDRESS,SALARY) "  \
         "VALUES (1, 'Paul', 32, 'California', 20000.00 ); " \
         "INSERT INTO COMPANY" + std::string(flag) + std::to_string(i) + " (ID,NAME,AGE,ADDRESS,SALARY) "  \
         "VALUES (2, 'Allen', 25, 'Texas', 15000.00 ); "     \
         "INSERT INTO COMPANY" + std::string(flag) + std::to_string(i) + " (ID,NAME,AGE,ADDRESS,SALARY) " \
         "VALUES (3, 'Teddy', 23, 'Norway', 20000.00 );" \
         "INSERT INTO COMPANY" + std::string(flag) + std::to_string(i) + " (ID,NAME,AGE,ADDRESS,SALARY) " \
         "VALUES (4, 'Mark', 25, 'Rich-Mond', 65000.00 );");

        sql = &sql_str2[0];

        // Execute SQL statement

        rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);

        if (rc != SQLITE_OK) {

            std::cout << "SQL error: " << zErrMsg << std::endl;
            sqlite3_free(zErrMsg);

        } else {

            //std::cout << "Insert data is done successfully ---->> i = " << i << std::endl;
        }

    }

    t1 = system_clock::now();
    t2 = system_clock::now();

    for (uint32_t i = 1; i <= I_MAX; i++) {

        if(i%1000 == 0) {

            std::cout << "----------------------------------->> i = " << i << std::endl;
            t2 = system_clock::now();
            time_span = duration_cast<duration<double>>(t2-t1);
            std::cout << " --> time used = " << time_span.count() << std::endl;
            t1 = t2;
        }


        //==============//
        // DELETE TABLE //
        //==============//

        // Create SQL statement

        const std::string sql_str3("DROP TABLE COMPANY"  + std::string(flag) + std::to_string(i) + ";");

        sql = &sql_str3[0];

        // Execute SQL statement

        rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);

        if (rc != SQLITE_OK) {

            std::cout << "SQL error: " << zErrMsg << std::endl;
            sqlite3_free(zErrMsg);

        } else {

            //std::cout << "Table deleted successfully ---->> i = " << i << std::endl;
        }
    }

    sqlite3_close(db);

    return 0;
}
