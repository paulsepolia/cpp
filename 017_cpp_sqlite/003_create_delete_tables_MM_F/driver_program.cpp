#include <iostream>
#include <string>
#include <sqlite3.h>

int callback(void *NotUsed, int32_t argc, char **argv, char **azColName) {

    for (int32_t i = 0; i < argc; i++) {

        std::cout << azColName[i] << " " << (argv[i] ? argv[i] : "NULL") << std::endl;
    }

    return 0;
}

int main() {

    const uint32_t I_MAX(10000);
    sqlite3 *db(0);
    char *zErrMsg(0);
    int32_t rc;
    const char *sql(0);

    // Open database

    rc = sqlite3_open("test.db", &db);

    if (rc) {
        std::cout << "Can't open database: " << sqlite3_errmsg(db) << std::endl;
        return -1;
    } else {
        std::cout << "Opened database successfully" << std::endl;
    }

    //==============//
    // create table //
    //==============//

    // Create SQL statement

    for (uint32_t i = 0; i < I_MAX; i++) {

        const std::string sql_str("CREATE TABLE COMPANY" + std::to_string(i) + "("  \
        "ID INT PRIMARY KEY     NOT NULL," \
        "NAME           TEXT    NOT NULL," \
        "AGE            INT     NOT NULL," \
        "ADDRESS        CHAR(50)," \
        "SALARY         REAL );");

        sql = &sql_str[0];

        // Execute SQL statement

        rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);

        if (rc != SQLITE_OK) {

            std::cout << "SQL error: " << zErrMsg << std::endl;
            sqlite3_free(zErrMsg);

        } else {

            std::cout << "Table created successfully ---->> i = " << i << std::endl;
        }
    }

    //==============//
    // delete table //
    //==============//

    // Create SQL statement

    for (uint32_t i = 0; i < I_MAX; i++) {

        const std::string sql_str2("DROP TABLE COMPANY" + std::to_string(i) + ";");

        sql = &sql_str2[0];

        // Execute SQL statement

        rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);

        if (rc != SQLITE_OK) {

            std::cout << "SQL error: " << zErrMsg << std::endl;
            sqlite3_free(zErrMsg);

        } else {

            std::cout << "Table deleted successfully ---->> i = " << i << std::endl;
        }
    }

    sqlite3_close(db);

    return 0;
}
