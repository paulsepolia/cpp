
//=============================================================================//
//  This example demonstrates how to "wait" for thread completions by using    //
//  the Pthread join routine.  Threads are explicitly created in a joinable    //
//  state for portability reasons. Use of the pthread_exit status argument is  //
//  also shown.                                                                //
//=============================================================================//

#include <iostream>
#include <pthread.h>
#include <cmath>
#include <iomanip>

using std::endl;
using std::cout;
using std::fixed;
using std::setprecision;
using std::showpoint;

#define NUM_THREADS 8

// a function

void * BusyWork(void *t)
{
    long i;
    long tid;
    double result = 0.0;
    const long I_MAX = static_cast<long>(pow(10.0, 8.0));

    tid = long (t);

    cout << " --> Thread " << tid << " starting..." << endl;

    for (i = 0; i < I_MAX; i++) {
        result = result + sin(static_cast<double>(i)) * cos(static_cast<double>(i));
    }

    cout << " --> Thread " << tid << " is done. Result = " << result << endl;

    pthread_exit((void*) t);
}

// the main function

int main ()
{
    // variables

    pthread_t thread [ NUM_THREADS ];
    pthread_attr_t attr;
    int rc;
    long t;
    void *status;
    int k;
    const int K_MAX = 10000000;

    // output format

    cout << fixed;
    cout << setprecision(5);
    cout << showpoint;

    for (k = 0; k < K_MAX; k++ ) {
        cout << "-------------------------------------------------------------------->> " << k << endl;

        // initialize and set thread detached attribute

        pthread_attr_init(&attr);

        pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

        // create threads

        for (t = 0; t < NUM_THREADS; t++) {
            cout << " --> Main: creating thread: " << t << endl;

            rc = pthread_create(&thread[t], &attr, BusyWork, (void *) t);

            if (rc) {
                cout << " --> ERROR; return code from pthread_create() is " << rc << endl;
                exit(-1);
            }
        }

        // destroy attributes

        pthread_attr_destroy(&attr);

        // join threads

        for(t = 0; t < NUM_THREADS; t++) {
            rc = pthread_join(thread[t], &status);
            if (rc) {
                cout << " --> ERROR; return code from pthread_join() is " << rc << endl;
                exit(-1);
            }

            cout << " --> Main: completed join with thread " << t
                 << " having a status of " << long (status) << endl;
        }

        cout << " --> Main: program completed. Exiting." << endl;
    }

    pthread_exit(NULL);

    return 0;
}

//======//
// FINI //
//======//
