#include <boost/thread.hpp>
#include <boost/asio.hpp>
#include <iostream>
#include <memory>

boost::asio::io_service io_svc{};
int32_t index_local(0);

void worker_thread() {
    std::cout << "------------------>> " << ++index_local << std::endl;
    io_svc.run();
    std::cout << "------------------>> End!" << std::endl;
}

int main() {

    // give work to the io_service here

    std::shared_ptr<boost::asio::io_service::work>
            worker(new boost::asio::io_service::work(io_svc));

    const int32_t I_DO_MAX(100);

    std::cout << "-->> 1" << std::endl;

    boost::thread_group threads{};

    std::cout << "-->> 2" << std::endl;

    // span threads to work with the io_svc

    for (uint32_t i(0); i != I_DO_MAX; i++) {
        std::cout << "-->> 3 --> span thread here --> "
                  << " --> i = " << i << std::endl;
        threads.create_thread(worker_thread);
    }

    std::cout << "-->> 4 --> enter input please here ..." << std::endl;

    std::cin.get();

    std::cout << "-->> 5 --> stop the io_service here" << std::endl;

    // the following line notifies the io_service that all work should be stopped

    io_svc.stop();

    std::cout << "-->> 6 --> join all the threads here" << std::endl;

    threads.join_all();

    std::cout << "-->> 7 --> exiting " << std::endl;

    return 0;
}
