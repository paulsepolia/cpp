#include <vector>
#include <iostream>
#include <atomic>
#include <thread>
#include <iomanip>
#include <chrono>
#include <mutex>
#include <cassert>
#include <boost/thread.hpp>
#include <boost/thread/barrier.hpp>
#include <boost/bind.hpp>
#include <boost/atomic.hpp>

class benchmark_timer {
public:

    explicit benchmark_timer(const double &res) :
            _res(res),
            t1{std::chrono::steady_clock::now()},
            t2{std::chrono::steady_clock::now()} {};

    ~benchmark_timer() {
        t2 = std::chrono::steady_clock::now();
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count()};
        std::cout << " --> res = " << _res << std::endl;
        std::cout << " --> time used (secs) = " << total_time << std::endl;
    }

    auto set_res(const double &res) -> void {
        _res = res;
    }

private:

    double _res{};
    decltype(std::chrono::steady_clock::now()) t1{};
    decltype(std::chrono::steady_clock::now()) t2{};
};

boost::mutex io_mutex;

void thread_fun(boost::barrier &cur_barier, boost::atomic<int> &current) {
    ++current;
    cur_barier.wait();
    boost::lock_guard <boost::mutex> locker(io_mutex);
    std::cout << current << std::endl;
}

int main() {

    boost::barrier bar(3);

    boost::atomic<int> current(0);

    auto thr1{boost::thread(boost::bind(&thread_fun, boost::ref(bar), boost::ref(current)))};

    auto thr2{boost::thread(boost::bind(&thread_fun, boost::ref(bar), boost::ref(current)))};

    auto thr3{boost::thread(boost::bind(&thread_fun, boost::ref(bar), boost::ref(current)))};

    thr1.join();
    thr2.join();
    thr3.join();
}