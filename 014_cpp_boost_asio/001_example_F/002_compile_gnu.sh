#!/bin/bash

  g++       -O3                \
            -Wall              \
	        -pthread           \
            -fopenmp           \
            -std=c++0x         \
            -pedantic          \
            -pedantic-errors   \
            driver_program.cpp \
            -I /opt/Fotech/helios/include \
            /opt/Fotech/helios/lib/*.so  \
            -o x_gnu
