#include <boost/asio.hpp>
#include <iostream>

int main() {

    const int32_t I_DO_MAX(10);

    boost::asio::io_service io_svc{};
    boost::asio::io_service::work worker(io_svc);

    // work has been given to io_svc but the poll is not blocking

    for(int32_t i = 0; i != I_DO_MAX; i++) {
        io_svc.poll();
        std::cout << " --> line --> " << i << std::endl;
    }

    std::cout << " We will see this line" << std::endl;

    return 0;
}