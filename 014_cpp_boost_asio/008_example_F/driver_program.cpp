#include <iostream>
#include <functional>
#include <memory>
#include <boost/asio.hpp>
#include <boost/thread.hpp>

void worker_thread(
        std::shared_ptr<boost::asio::io_service> io_svc,
        uint32_t counter) {

    std::cout << " --> counter --> " << counter << std::endl;
    io_svc->run(); // blocks here until the is_svc->stop() is called
    std::cout << " --> End." << std::endl;
}

int main() {

    const uint32_t I_DO_MAX(10);

    std::shared_ptr<boost::asio::io_service>
            io_svc(new boost::asio::io_service);

    std::shared_ptr<boost::asio::io_service::work>
            worker(new boost::asio::io_service::work(*io_svc));

    std::cout << "--> press ENTER key to exit!" << std::endl;

    boost::thread_group threads{};

    for (uint32_t i = 0; i <= I_DO_MAX; i++) {
        threads.create_thread(std::bind(&worker_thread, io_svc, i));
    }

    std::cin.get();

    io_svc->stop(); // unblocks the io_svc->run();

    threads.join_all();

    return 0;
}
