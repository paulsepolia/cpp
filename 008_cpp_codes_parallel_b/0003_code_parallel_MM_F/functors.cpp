//=====================//
// FUNCTORS DEFINITION //
//=====================//

#include "functors.h"

// functor # 1

// constructor

template<>
Sum<double>::Sum()
{
    sum = 0;
}

// assignment operator

template<>
void Sum<double>::operator()(const double & n)
{
    sum += n;
}

// functor # 2

template<>
double UniqueNumber<double>::operator () ()
{
    return double(123.456);
}

// functor # 3

template<>
double ac_functor<double>::operator()(const double & x, const double & y)
{
    return x+y;
}

// functor # 4

template <>
double product_functor<double>::operator()(const double & x, const double & y)
{
    return x*y;
}

// functor # 5

template <>
bool equal_functor<double>::operator()(const double & i, const double & j)
{
    return (i == j);
}

// end
