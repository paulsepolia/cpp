//=====================//
// FUNCTORS DEFINITION //
//=====================//

#include "functors.h"
#include <cstdlib>

using std::rand;

// functor # 1

// constructor

template<>
Sum_functor<double>::Sum_functor()
{
    sum = 0;
}

// assignment operator

template<>
void Sum_functor<double>::operator()(const double & n)
{
    sum += n;
}

// functor # 2

template<>
double UniqueNumber_functor<double>::operator () ()
{
    return double(123.456);
}

// functor # 3

template<>
double ac_functor<double>::operator()(const double & x, const double & y)
{
    return x+y;
}

// functor # 4

template <>
double product_functor<double>::operator()(const double & x, const double & y)
{
    return x*y;
}

// functor # 5

template <>
bool equal_functor<double>::operator()(const double & i, const double & j) const
{
    return (!(i < j) && !(i > j));
}

// functor # 6

template <>
bool isOdd_functor<double>::operator()(const double & i)
{
    return ((unsigned long int)(i)%2 == 1);
}

// functor # 7

template <>
bool lessThanMinusOne_functor<double>::operator()(const double & i)
{
    return (i < -1);
}

// functor # 8

template <>
bool strComp_functor<char>::operator()(const char & i, const char & j)
{
    return (i < j);
}

// functor # 9

template <>
double functor_pp<double>::operator()(double & i)
{
    return i++;
}

// functor # 10
// random generator functor

template <>
int rand_functor<double>::operator()(const double & i)
{
    return (rand()%static_cast<unsigned long int>(i));
}

// functor # 11

template <>
bool lessThan_functor<double>::operator()(const double & x, const double & y) const
{
    return (x < y);
}

// end
