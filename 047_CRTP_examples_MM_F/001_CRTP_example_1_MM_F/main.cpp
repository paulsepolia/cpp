#include <iostream>

template<typename T>
class A {
public:

    void print_fun() {
        static_cast<T *>(this)->implementation();
        this->implementation();
    }

    void implementation() {
        std::cout << " --> hello from base class --> A" << std::endl;
    }
};

class B : public A<B> {
public:

    static void implementation() {
        std::cout << " --> hello from derived class --> B" << std::endl;
    }

};

class C : public A<C> {
public:

    static void implementation() {
        std::cout << " --> hello from derived class --> C" << std::endl;
    }
};

int main() {

    {
        std::cout << " --> example --> 1" << std::endl;
        A<B> a;
        a.print_fun();
    }

    {
        std::cout << " --> example --> 2" << std::endl;
        A<C> a;
        a.print_fun();
    }

    {
        std::cout << " --> example --> 3" << std::endl;
        B b;
        b.print_fun();
    }

    {
        std::cout << " --> example --> 4" << std::endl;
        C c;
        c.print_fun();
    }
}
