#include <iostream>
#include <vector>
#include <chrono>
#include <cmath>
#include <iomanip>

class BenchmarkTimer
{
public:

    explicit BenchmarkTimer() :
            _res(0.0),
            _t1{std::chrono::steady_clock::now()},
            _t2{std::chrono::steady_clock::now()},
            _t_prev{std::chrono::steady_clock::now()}
    {};

    auto PrintTime() const -> void
    {
        const auto t_loc{std::chrono::steady_clock::now()};
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t_loc - _t_prev).count()};
        _t_prev = t_loc;
        std::cout.precision(10);
        std::cout << std::fixed;
        std::cout << " --> time used up to now (since last reset) (secs) = " << total_time << std::endl;
    }

    auto ResetTimer() -> void
    {
        const auto t_loc{std::chrono::steady_clock::now()};
        _t_prev = t_loc;
        _t1 = t_loc;
        _t2 = t_loc;
    }

    ~BenchmarkTimer()
    {
        _t2 = std::chrono::steady_clock::now();
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(_t2 - _t1).count()};
        std::cout << " --> res = " << _res << std::endl;
        std::cout.precision(10);
        std::cout << std::fixed;
        std::cout << " --> total time used until exit of the scope (since last reset) (secs) = " << total_time
                  << std::endl;
    }

    auto SetRes(const double &res) -> void
    {
        _res = res;
    }

private:

    double _res{};
    decltype(std::chrono::steady_clock::now()) _t1{};
    decltype(std::chrono::steady_clock::now()) _t2{};
    mutable decltype(std::chrono::steady_clock::now()) _t_prev{};
};

template<typename D>
class B
{
protected:

    double _i;
    double _j;

public:

    B() : _i(0.0), _j(0.0)
    {}

    void modify_i(double i)
    {
        static_cast<D *>(this)->modify_i_impl(i);
    }

    void modify_j(double j)
    {
        static_cast<D *>(this)->modify_j_impl(j);
    }

    [[nodiscard]] double get_i() const
    {
        return _i;
    }

    [[nodiscard]] double get_j() const
    {
        return _j;
    }
};

class D : public B<D>
{
public:
    void modify_i_impl(double i)
    {
        for (size_t k = 0; k < i; k++)
        {
            _i += i;
        }
    }

    void modify_j_impl(double j)
    {
        for (size_t k = 0; k < j; k++)
        {
            _j += j;
        }
    }
};

class B2
{
protected:
    double _i;
    double _j;

public:
    B2() : _i(0), _j(0)
    {}

    virtual void modify_i(double i) = 0;

    virtual void modify_j(double i) = 0;

    [[nodiscard]] double get_i() const
    {
        return _i;
    }

    [[nodiscard]] double get_j() const
    {
        return _j;
    }
};

class D2 final : public B2
{
public:

    void modify_i(double i) final
    {
        for (size_t k = 0; k < i; k++)
        {
            _i += i;
        }
    }

    void modify_j(double j) final
    {
        for (size_t k = 0; k < j; k++)
        {
            _j += j;
        }
    }
};

int main()
{
    const auto do_max = (size_t) (std::pow(10.0, 5.0));

    {
        std::cout << std::fixed;
        std::cout.precision(5);

        std::cout << "-->> benchmark -->> 1 -->> CRTP -->> B<D> *pb = new D();" << std::endl;
        BenchmarkTimer ot;
        B<D> *pb = new D();

        for (size_t i = 0; i < do_max; i++)
        {
            pb->modify_i((double) i);
            pb->modify_j((double) i);
        }

        std::cout << "-->> " << pb->get_i() << std::endl;
        std::cout << "-->> " << pb->get_j() << std::endl;
    }

    {
        std::cout << std::fixed;
        std::cout.precision(5);

        std::cout << "-->> benchmark -->> 2 -->> CRTP -->> D d;" << std::endl;
        BenchmarkTimer ot;
        D d;

        for (size_t i = 0; i < do_max; i++)
        {
            d.modify_i((double) i);
            d.modify_j((double) i);
        }

        std::cout << "-->> " << d.get_i() << std::endl;
        std::cout << "-->> " << d.get_j() << std::endl;
    }

    {
        std::cout << std::fixed;
        std::cout.precision(5);

        std::cout << "-->> benchmark -->> 3 -->> Pure virtual function call" << std::endl;
        BenchmarkTimer ot;

        B2 *pb = new D2();

        for (size_t i = 0; i < do_max; i++)
        {
            pb->modify_i((double) i);
            pb->modify_j((double) i);
        }

        std::cout << "-->> " << pb->get_i() << std::endl;
        std::cout << "-->> " << pb->get_j() << std::endl;
    }
}