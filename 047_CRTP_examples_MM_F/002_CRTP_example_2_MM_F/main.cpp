#include <iostream>

template<typename T>
class A {
public:

    void print_fun() {

        T *derived1 = static_cast<T *>(this);

        derived1->implementation();

        T &derived2 = static_cast<T &>(*this);

        derived2.implementation();

        T derived3 = static_cast<T &>(*this);

        derived3.implementation();
    }

private:

    A() = default;

    friend T;
};

class B : public A<B> {
public:

    static void implementation() {
        std::cout << " --> hello from derived class --> B" << std::endl;
    }
};

class C : public A<C> {
public:

    static void implementation() {
        std::cout << " --> hello from derived class --> C" << std::endl;
    }
};

int main() {

    {
        std::cout << " --> example --> 1" << std::endl;
        B b;
        b.print_fun();
    }

    {
        std::cout << " --> example --> 2" << std::endl;
        C c;
        c.print_fun();
    }
}