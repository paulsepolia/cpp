#include <iostream>
#include <vector>
#include <chrono>
#include <cmath>
#include <iomanip>

class BenchmarkTimer
{
public:

    explicit BenchmarkTimer() :
            _res(0.0),
            _t1{std::chrono::steady_clock::now()},
            _t2{std::chrono::steady_clock::now()},
            _t_prev{std::chrono::steady_clock::now()}
    {};

    auto PrintTime() const -> void
    {
        const auto t_loc{std::chrono::steady_clock::now()};
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t_loc - _t_prev).count()};
        _t_prev = t_loc;
        std::cout.precision(10);
        std::cout << std::fixed;
        std::cout << " --> time used up to now (since last reset) (secs) = " << total_time << std::endl;
    }

    auto ResetTimer() -> void
    {
        const auto t_loc{std::chrono::steady_clock::now()};
        _t_prev = t_loc;
        _t1 = t_loc;
        _t2 = t_loc;
    }

    ~BenchmarkTimer()
    {
        _t2 = std::chrono::steady_clock::now();
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(_t2 - _t1).count()};
        std::cout << " --> res = " << _res << std::endl;
        std::cout.precision(10);
        std::cout << std::fixed;
        std::cout << " --> total time used until exit of the scope (since last reset) (secs) = " << total_time
                  << std::endl;
    }

    auto SetRes(const double &res) -> void
    {
        _res = res;
    }

private:

    double _res{};
    decltype(std::chrono::steady_clock::now()) _t1{};
    decltype(std::chrono::steady_clock::now()) _t2{};
    mutable decltype(std::chrono::steady_clock::now()) _t_prev{};
};

template<typename T>
class B
{
protected:

    std::vector<std::vector<double>> _mat;

public:

    B() : _mat(std::vector<std::vector<double>>())
    {}

    void modify_matrix(size_t dim)
    {
        static_cast<T *>(this)->modify_matrix_impl(dim);
    }

    [[nodiscard]] std::vector<std::vector<double>> get_matrix() const
    {
        return _mat;
    }
};

class D1 : public B<D1>
{
public:
    void modify_matrix_impl(size_t dim)
    {
        _mat.resize(dim);
        for (auto &el: _mat)
        {
            el.resize(dim);
        }

        for (size_t i = 0; i < _mat.size(); i++)
        {
            for (size_t j = 0; j < _mat[i].size(); j++)
            {
                _mat[i][j] = (double) (i + j);
            }
        }
    }
};

class D2 : public B<D2>
{
public:
    void modify_matrix_impl(size_t dim)
    {
        _mat.resize(dim);
        for (auto &el: _mat)
        {
            el.resize(dim);
        }

        for (size_t i = 0; i < _mat.size(); i++)
        {
            for (size_t j = 0; j < _mat[i].size(); j++)
            {
                _mat[i][j] = (double) (i) * (double) (j);
            }
        }
    }
};

class BV
{
protected:
    std::vector<std::vector<double>> _mat;

public:
    BV() : _mat(std::vector<std::vector<double>>())
    {}

    virtual void modify_matrix(size_t i) = 0;

    [[nodiscard]] std::vector<std::vector<double>> get_matrix() const
    {
        return _mat;
    }
};

class DV1 final : public BV
{
public:

    void modify_matrix(size_t dim) final
    {
        _mat.resize(dim);
        for (auto &el: _mat)
        {
            el.resize(dim);
        }

        for (size_t i = 0; i < _mat.size(); i++)
        {
            for (size_t j = 0; j < _mat[i].size(); j++)
            {
                _mat[i][j] = (double) (i) + (double) (j);
            }
        }
    }
};

class DV2 final : public BV
{
public:

    void modify_matrix(size_t dim) final
    {
        _mat.resize(dim);
        for (auto &el: _mat)
        {
            el.resize(dim);
        }

        for (size_t i = 0; i < _mat.size(); i++)
        {
            for (size_t j = 0; j < _mat[i].size(); j++)
            {
                _mat[i][j] = (double) (i) * (double) (j);
            }
        }
    }
};

int main()
{
    const auto do_max = (size_t) 2 * (std::pow(10.0, 3.0));

    {
        std::cout << std::fixed;
        std::cout.precision(5);

        std::cout << std::endl;
        std::cout << "-->> benchmark -->> 1 -->> CRTP -->> B<D1> *pb = new D1();" << std::endl;
        BenchmarkTimer ot;
        B<D1> *pb = new D1();

        for (size_t i = 0; i < do_max; i++)
        {
            pb->modify_matrix(i);
        }

        std::cout << "-->> " << pb->get_matrix()[0][0] << std::endl;
        std::cout << "-->> " << pb->get_matrix()[1][1] << std::endl;
        std::cout << "-->> " << pb->get_matrix()[2][2] << std::endl;
    }

    {
        std::cout << std::fixed;
        std::cout.precision(5);

        std::cout << std::endl;
        std::cout << "-->> benchmark -->> 2 -->> CRTP -->> D1 d;" << std::endl;
        BenchmarkTimer ot;
        D1 d;

        for (size_t i = 0; i < do_max; i++)
        {
            d.modify_matrix(i);
        }

        std::cout << "-->> " << d.get_matrix()[0][0] << std::endl;
        std::cout << "-->> " << d.get_matrix()[1][1] << std::endl;
        std::cout << "-->> " << d.get_matrix()[2][2] << std::endl;
    }

    {
        std::cout << std::fixed;
        std::cout.precision(5);

        std::cout << std::endl;
        std::cout << "-->> benchmark -->> 3 -->> Pure virtual function call" << std::endl;
        BenchmarkTimer ot;

        BV *pb = new DV1();

        for (size_t i = 0; i < do_max; i++)
        {
            pb->modify_matrix(i);
        }

        std::cout << "-->> " << pb->get_matrix()[0][0] << std::endl;
        std::cout << "-->> " << pb->get_matrix()[1][1] << std::endl;
        std::cout << "-->> " << pb->get_matrix()[2][2] << std::endl;
    }

    {
        std::cout << std::fixed;
        std::cout.precision(5);

        std::cout << std::endl;
        std::cout << "-->> benchmark -->> 4 -->> CRTP -->> B<D2> *pb = new D2();" << std::endl;
        BenchmarkTimer ot;
        B<D2> *pb = new D2();

        for (size_t i = 0; i < do_max; i++)
        {
            pb->modify_matrix(i);
        }

        std::cout << "-->> " << pb->get_matrix()[0][0] << std::endl;
        std::cout << "-->> " << pb->get_matrix()[1][1] << std::endl;
        std::cout << "-->> " << pb->get_matrix()[2][2] << std::endl;
    }

    {
        std::cout << std::fixed;
        std::cout.precision(5);

        std::cout << std::endl;
        std::cout << "-->> benchmark -->> 5 -->> CRTP -->> D2 d;" << std::endl;
        BenchmarkTimer ot;
        D2 d;

        for (size_t i = 0; i < do_max; i++)
        {
            d.modify_matrix(i);
        }

        std::cout << "-->> " << d.get_matrix()[0][0] << std::endl;
        std::cout << "-->> " << d.get_matrix()[1][1] << std::endl;
        std::cout << "-->> " << d.get_matrix()[2][2] << std::endl;
    }

    {
        std::cout << std::fixed;
        std::cout.precision(5);

        std::cout << std::endl;
        std::cout << "-->> benchmark -->> 6 -->> Pure virtual function call" << std::endl;
        BenchmarkTimer ot;

        BV *pb = new DV2();

        for (size_t i = 0; i < do_max; i++)
        {
            pb->modify_matrix(i);
        }

        std::cout << "-->> " << pb->get_matrix()[0][0] << std::endl;
        std::cout << "-->> " << pb->get_matrix()[1][1] << std::endl;
        std::cout << "-->> " << pb->get_matrix()[2][2] << std::endl;
    }
}