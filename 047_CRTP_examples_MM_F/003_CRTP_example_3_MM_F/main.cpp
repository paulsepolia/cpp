#include <iostream>
#include <vector>
#include <chrono>
#include <cmath>

class BenchmarkTimer
{
public:

    explicit BenchmarkTimer() :
            _res(0.0),
            _t1{std::chrono::steady_clock::now()},
            _t2{std::chrono::steady_clock::now()},
            _t_prev{std::chrono::steady_clock::now()}
    {};

    auto PrintTime() const -> void
    {
        const auto t_loc{std::chrono::steady_clock::now()};
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t_loc - _t_prev).count()};
        _t_prev = t_loc;
        std::cout.precision(10);
        std::cout << std::fixed;
        std::cout << " --> time used up to now (since last reset) (secs) = " << total_time << std::endl;
    }

    auto ResetTimer() -> void
    {
        const auto t_loc{std::chrono::steady_clock::now()};
        _t_prev = t_loc;
        _t1 = t_loc;
        _t2 = t_loc;
    }

    ~BenchmarkTimer()
    {
        _t2 = std::chrono::steady_clock::now();
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(_t2 - _t1).count()};
        std::cout << " --> res = " << _res << std::endl;
        std::cout.precision(10);
        std::cout << std::fixed;
        std::cout << " --> total time used until exit of the scope (since last reset) (secs) = " << total_time
                  << std::endl;
    }

    auto SetRes(const double &res) -> void
    {
        _res = res;
    }

private:

    double _res{};
    decltype(std::chrono::steady_clock::now()) _t1{};
    decltype(std::chrono::steady_clock::now()) _t2{};
    mutable decltype(std::chrono::steady_clock::now()) _t_prev{};
};

template<typename D1>
class B1
{
protected:

    double _i;

public:

    B1() : _i(0)
    {}

    void f(double i)
    {
        static_cast<D1 *>(this)->f_impl(i);
    }

    [[nodiscard]] double get() const
    {
        return _i;
    }
};

class D1 : public B1<D1>
{
public:
    void f_impl(double i)
    {
        _i += i;
    }
};

class B2
{
protected:
    double _i;

public:
    B2() : _i(0)
    {}

    virtual void f(double i) = 0;

    [[nodiscard]] double get() const
    {
        return _i;
    }
};

class D2 final : public B2
{
public:
    void f(double i) final
    {
        _i += i;
    }
};

int main()
{

    const auto do_max = (size_t) (std::pow(10.0, 9.0));

    {
        std::cout << "-->> benchmark -->> 1 -->> CRTP -->> B1<D1> *pb = new D1();" << std::endl;
        BenchmarkTimer ot;
        B1<D1> *pb = new D1();

        for (size_t i = 0; i < do_max; i++)
        {
            pb->f((double) 1);
        }

        std::cout << "-->> " << pb->get() << std::endl;
    }

    {
        std::cout << "-->> benchmark -->> 2 -->> CRTP -->> D2 d;" << std::endl;
        BenchmarkTimer ot;
        D2 d;

        for (size_t i = 0; i < do_max; i++)
        {
            d.f((double) 1);
        }

        std::cout << "-->> " << d.get() << std::endl;
    }

    {
        std::cout << "-->> benchmark -->> 3 -->> Pure virtual function call" << std::endl;
        BenchmarkTimer ot;

        B2 *pb = new D2();

        for (size_t i = 0; i < do_max; i++)
        {
            pb->f((double) 1);
        }

        std::cout << "-->> " << pb->get() << std::endl;
    }
}