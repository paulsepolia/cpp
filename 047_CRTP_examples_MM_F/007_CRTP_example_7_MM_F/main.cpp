#include <iostream>
#include <vector>
#include <chrono>
#include <cmath>
#include <thread>

class BenchmarkTimer
{
public:

    explicit BenchmarkTimer() :
            _res(0.0),
            _t1{std::chrono::steady_clock::now()},
            _t2{std::chrono::steady_clock::now()},
            _t_prev{std::chrono::steady_clock::now()}
    {};

    auto PrintTime() const -> void
    {
        const auto t_loc{std::chrono::steady_clock::now()};
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t_loc - _t_prev).count()};
        _t_prev = t_loc;
        std::cout.precision(10);
        std::cout << std::fixed;
        std::cout << " --> time used up to now (since last reset) (secs) = " << total_time << std::endl;
    }

    auto ResetTimer() -> void
    {
        const auto t_loc{std::chrono::steady_clock::now()};
        _t_prev = t_loc;
        _t1 = t_loc;
        _t2 = t_loc;
    }

    ~BenchmarkTimer()
    {
        _t2 = std::chrono::steady_clock::now();
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(_t2 - _t1).count()};
        std::cout << " --> res = " << _res << std::endl;
        std::cout.precision(10);
        std::cout << std::fixed;
        std::cout << " --> total time used until exit of the scope (since last reset) (secs) = " << total_time
                  << std::endl;
    }

    auto SetRes(const double &res) -> void
    {
        _res = res;
    }

private:

    double _res{};
    decltype(std::chrono::steady_clock::now()) _t1{};
    decltype(std::chrono::steady_clock::now()) _t2{};
    mutable decltype(std::chrono::steady_clock::now()) _t_prev{};
};

template<typename Derived>
class A
{
protected:

    double _i1;
    double _i2;
    double _i3;

public:

    A() : _i1(0), _i2(0), _i3(0)
    {}

    virtual ~A()
    {
        std::cout << "-->> ~A()" << std::endl;
    }

    void set_values(double i1, double i2, double i3)
    {
        static_cast<Derived *>(this)->set_values_impl_v1(i1, i2, i3);
        static_cast<Derived &>(*this).set_values_impl_v2(i1, i2, i3);
    }

    [[nodiscard]] double get_i1() const
    {
        return _i1;
    }

    [[nodiscard]] double get_i2() const
    {
        return _i2;
    }

    [[nodiscard]] double get_i3() const
    {
        return _i3;
    }
};

class B : public A<B>
{
public:

    void set_values_impl_v1(double i1, double i2, double i3)
    {
        _i1 += i1;
        _i2 += i2;
        _i3 += i3;
    }

    void set_values_impl_v2(double i1, double i2, double i3)
    {
        _i1 *= i1;
        _i2 *= i2;
        _i3 *= i3;
    }

public:

    ~B() final
    {
        std::cout << "-->> ~B()" << std::endl;
    }
};

int main()
{
    {
        std::cout << "---------------------------------------------------->> A1" << std::endl;

        A<B> *pa = new B();

        std::cout << "-->> 1" << std::endl;

        pa->set_values(1, 2, 3);

        std::cout << "-->> 2" << std::endl;

        std::this_thread::sleep_for(std::chrono::seconds(2));

        std::cout << "-->> 3" << std::endl;

        std::this_thread::sleep_for(std::chrono::seconds(2));

        std::cout << "-->> 4" << std::endl;

        std::cout << pa->get_i1() << std::endl;
        std::cout << pa->get_i2() << std::endl;
        std::cout << pa->get_i3() << std::endl;

        std::cout << "-->> 5" << std::endl;

        delete pa;
        pa = nullptr;
    }

    {
        std::cout << "---------------------------------------------------->> A2" << std::endl;

        B *pb = new B();

        std::cout << "-->> 1" << std::endl;

        pb->set_values(1, 2, 3);

        std::cout << "-->> 2" << std::endl;

        std::this_thread::sleep_for(std::chrono::seconds(2));

        std::cout << "-->> 3" << std::endl;

        std::this_thread::sleep_for(std::chrono::seconds(2));

        std::cout << "-->> 4" << std::endl;

        std::cout << pb->get_i1() << std::endl;
        std::cout << pb->get_i2() << std::endl;
        std::cout << pb->get_i3() << std::endl;

        std::cout << "-->> 5" << std::endl;

        delete pb;
        pb = nullptr;
    }
}