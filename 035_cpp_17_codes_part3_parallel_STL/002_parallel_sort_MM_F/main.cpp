#include <iostream>
#include <vector>
#include <cmath>
#include <execution>
#include <algorithm>
#include <iomanip>

int main() {

    const auto DIM_MAX = static_cast<uint64_t>(2 * std::pow(10.0, 8.0));
    const auto DO_MAX = static_cast<uint64_t>(std::pow(10.0, 6.0));
    const auto MOD_VAL = static_cast<uint64_t>(100);
    const double VAL1 = 0.99;

    std::cout << std::fixed;
    std::cout << std::setprecision(10);

    std::vector<double> vec(DIM_MAX, VAL1);

    for (uint64_t k = 0; k < DO_MAX; k++) {

        std::sort(std::execution::par, vec.begin(), vec.end());

        const auto res = std::is_sorted(std::execution::par, vec.begin(), vec.end());

        if (k % MOD_VAL == 0) {
            std::cout << "----------------------------------->> k = " << k << std::endl;
            std::cout << res << std::endl;
        }
    }
}