#!/bin/bash

  clang++   -O3            \
            -Wall          \
            -std=c++2a     \
            -stdlib=libc++ \
            -fopenmp       \
            -pthread       \
            -pedantic      \
            main.cpp       \
            -I/opt/tbb/2019/include/  \
            -L/opt/tbb/2019/lib -ltbb \
            -o x_clang -lc++fs

