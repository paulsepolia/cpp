#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <iostream>
#include <vector>
#include <cmath>
#include <chrono>
#include <thread>


void send_something(std::string host,
                    uint32_t port,
                    const std::vector<char> &message) {

    // set up the socket
    boost::asio::io_service ios;
    boost::asio::ip::tcp::endpoint endpoint(boost::asio::ip::address::from_string(host), port);
    boost::asio::ip::tcp::socket socket(ios);

    // connect to the socket
    socket.connect(endpoint);

    boost::system::error_code error;

    // write to the socket
    socket.write_some(boost::asio::buffer(&message[0], message.size()), error);

    // close socket
    socket.close();
}


void read_something(uint32_t port, const uint64_t num_bytes) {

    // set up the socket
    boost::asio::io_service ios;
    boost::asio::ip::tcp::endpoint endpoint(boost::asio::ip::address::from_string("127.0.0.1"), port);
    boost::asio::ip::tcp::socket socket(ios);

    // connect to the socket
    socket.connect(endpoint);

    boost::system::error_code error;

    std::vector<char> message;
    message.resize(num_bytes);

    // read from the socket
    socket.read_some(boost::asio::buffer(&message[0]), error);

    // close socket
    socket.close();
}


int main() {

    const uint64_t NUM_CHARS(4 * std::pow(10.0, 2.0));
    const uint64_t NUM_TIMES(100);
    const std::string host("127.0.0.1");
    const uint32_t port(1990);

    // build a huge message here

    std::vector<char> message;
    message.resize(NUM_CHARS);

    uint64_t new_line(0);
    for (uint64_t i(1); i < NUM_CHARS; i = i + 2) {

        message[i - 1] = 'a';
        message[i] = 'b';
        new_line++;

        if (new_line % 100 == 0) {
            message[i] = '\n';
        }
    }

    for (uint64_t i(0); i != NUM_TIMES; i++) {

        {
            std::cout << "---------------------------->> receiving -->> " << i << std::endl;
            read_something(port, NUM_CHARS);
        }

        {
            std::cout << "---------------------------->> sending -->> " << i << std::endl;
            send_something(host, port, message);
            std::this_thread::sleep_for(std::chrono::seconds(1));
        }
    }

    return 0;
}
