#!/bin/bash

PORT_LOC=1992
OUT_FILE_NAME="out_file"
NUM_TO_RECV=10000
i=0

while [ $i -lt $NUM_TO_RECV ]
do
	echo "------------------------------->> receiving -->> " $i
	netcat -lp $PORT_LOC > "out_file"$i
    rm "out_file"$i
	i=$[$i+1]
done
