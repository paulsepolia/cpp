#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>
#include <chrono>
#include <memory>
#include <cmath>
#include <thread>

int main(int argc, char *argv[]) {

    // local parameters

    const auto NUM_MSG = static_cast<uint64_t>(std::pow(10.0, 9.0));
    const auto DIM_MSG = static_cast<uint64_t>(std::pow(1024.0, 3.0)) * 1;
    const uint64_t MOD_MSG = 20;
    const uint64_t READ_MSG_EVERY_SEC = 0;
    const bool flg_debug = false;

    // local variables

    if (argc < 2) {
        std::cout << "usage: " << argv[0] << " port" << std::endl;
        exit(-1);
    }

    socklen_t clilen;
    std::shared_ptr<char> buffer(new char[DIM_MSG], std::default_delete<char[]>());
    struct sockaddr_in serv_addr{};
    struct sockaddr_in cli_addr{};
    const auto portno = static_cast<uint16_t>(atoi(argv[1]));

    // DESCRIPTION
    // int socket(int domain, int type, int protocol);
    // socket() creates an endpoint for communication and returns a file
    // descriptor that refers to that endpoint.  The file descriptor
    // returned by a successful call will be the lowest-numbered file
    // descriptor not currently open for the process.

    const auto socket_fd = socket(AF_INET, SOCK_STREAM, 0);

    if (socket_fd < 0) {
        std::cout << "ERROR opening socket" << std::endl;
        exit(-1);
    }

    // DESCRIPTION
    // void bzero(void *s, size_t n);
    // The bzero() function erases the data in the n bytes of the memory
    // starting at the location pointed to by s, by writing zeros (bytes
    // containing '\0') to that area.

    bzero((char *) &serv_addr, sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(portno);

    // DESCRIPTION
    // int bind(int sockfd, const struct sockaddr *addr, socklen_t addrlen);
    // When a socket is created with socket(2), it exists in a name space
    // (address family) but has no address assigned to it.  bind() assigns
    // the address specified by addr to the socket referred to by the file
    // descriptor sockfd.  addrlen specifies the size, in bytes, of the
    // address structure pointed to by addr.  Traditionally, this operation
    // is called “assigning a name to a socket”.

    const auto ret1 = bind(socket_fd, (struct sockaddr *) &serv_addr, sizeof(serv_addr));

    if (ret1 < 0) {
        std::cout << "ERROR on binding" << std::endl;
        exit(-1);
    }

    // DESCRIPTION
    // int listen(int sockfd, int backlog);
    //      listen() marks the socket referred to by sockfd as a passive socket,
    //      that is, as a socket that will be used to accept incoming connection
    //      requests using accept(2).
    //
    //      The sockfd argument is a file descriptor that refers to a socket of
    //      type SOCK_STREAM or SOCK_SEQPACKET.

    listen(socket_fd, 5);
    clilen = sizeof(cli_addr);

    // DESCRIPTION
    // int accept(int sockfd, struct sockaddr *addr, socklen_t *addrlen);
    // int accept4(int sockfd, struct sockaddr *addr, socklen_t *addrlen, int flags);
    //      The accept() system call is used with connection-based socket types
    //      (SOCK_STREAM, SOCK_SEQPACKET).  It extracts the first connection
    //      request on the queue of pending connections for the listening socket,
    //      sockfd, creates a new connected socket, and returns a new file
    //      descriptor referring to that socket.  The newly created socket is not
    //      in the listening state.  The original socket sockfd is unaffected by
    //      this call.

    const auto new_socket_fd = accept(socket_fd, (struct sockaddr *) &cli_addr, &clilen);

    if (new_socket_fd < 0) {
        std::cout << "ERROR on accept" << std::endl;
        exit(-1);
    }

    std::cout << " --> server --> reading messages..." << std::endl;

    auto t1 = std::chrono::steady_clock::now();

    for (uint64_t i = 0; i < NUM_MSG; i++) {

        if (READ_MSG_EVERY_SEC != 0) {
            std::this_thread::sleep_for(std::chrono::seconds(READ_MSG_EVERY_SEC));
        }

        bzero(buffer.get(), DIM_MSG);
        uint32_t total_bytes_read = 0;

        while (total_bytes_read < DIM_MSG) {

            const int64_t bytes_read =
                    read(new_socket_fd, buffer.get() + total_bytes_read, DIM_MSG - total_bytes_read);

            if (bytes_read >= 0) {
                total_bytes_read += bytes_read;
            }
        }

        if (i != 0 && i % MOD_MSG == 0) {

            auto t2 = std::chrono::steady_clock::now();

            const auto time_tot =
                    std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);

            std::cout << " --> time for = " << time_tot.count() << std::endl;
            std::cout << " --> data speed (Gbits/sec) = "
                      << (DIM_MSG * MOD_MSG * 8.0) / time_tot.count() / std::pow(1000.0, 3.0) << std::endl;

            t1 = t2;
        }

        if (flg_debug) {

            if (buffer.get()[DIM_MSG - 1] != '1') {

                std::cout << " --> server --> error reading message # " << i << " --> "
                          << buffer.get()[DIM_MSG - 1] << std::endl;
                exit(-1);
            }
        }
    }

    close(new_socket_fd);
    close(socket_fd);

    return 0;
}
