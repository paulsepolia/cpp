#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <iostream>
#include <thread>
#include <cmath>

int main(int argc, char *argv[]) {

    // local parameters

    const auto NUM_MSG = static_cast<uint64_t>(std::pow(10.0, 9.0));
    const auto DIM_MSG = static_cast<uint64_t>(std::pow(1024.0, 3.0)) * 1;

    // local variables

    struct sockaddr_in serv_addr{};
    struct hostent *server;

    if (argc < 3) {
        std::cout << "usage: " << argv[0] << " hostname port" << std::endl;
        exit(-1);
    }

    const auto portno = atoi(argv[2]);

    // DESCRIPTION
    // int socket(int domain, int type, int protocol);
    //      socket() creates an endpoint for communication and returns a file
    //      descriptor that refers to that endpoint.  The file descriptor
    //      returned by a successful call will be the lowest-numbered file
    //      descriptor not currently open for the process.

    const auto sockfd = socket(AF_INET, SOCK_STREAM, 0);

    if (sockfd < 0) {
        std::cout << "ERROR opening socket" << std::endl;
        exit(-1);
    }

    server = gethostbyname(argv[1]);

    if (server == nullptr) {
        std::cout << "ERROR, no such host" << std::endl;
        exit(-1);
    }

    // DESCRIPTION
    // void bzero(void *s, size_t n);
    //      The bzero() function erases the data in the n bytes of the memory
    //      starting at the location pointed to by s, by writing zeros (bytes
    //      containing '\0') to that area.

    bzero((char *) &serv_addr, sizeof(serv_addr));

    // DESCRIPTION
    // AF_INET is an address family that is used to designate the type of addresses
    // that your socket can communicate with (in this case, Internet Protocol v4 addresses).
    // When you create a socket, you have to specify its address family,
    // and then you can only use addresses of that type with the socket.
    // The Linux kernel, for example, supports 29 other address families
    // such as UNIX (AF_UNIX) sockets and IPX (AF_IPX) ...

    serv_addr.sin_family = AF_INET;

    // DESCRIPTION
    // The bcopy() function copies n bytes from src to dest.
    // The result is correct, even when both areas overlap.

    bcopy(server->h_addr, (char *) &serv_addr.sin_addr.s_addr, static_cast<uint32_t>(server->h_length));

    serv_addr.sin_port = htons(static_cast<uint16_t>(portno));

    // DESCRIPTION
    // The connect() system call connects the socket referred to by the file
    // descriptor sockfd to the address specified by addr.  The addrlen
    // argument specifies the size of addr.  The format of the address in
    // addr is determined by the address space of the socket sockfd; see
    // socket(2) for further details.
    //
    // If the socket sockfd is of type SOCK_DGRAM, then addr is the address
    // to which datagrams are sent by default, and the only address from
    // which datagrams are received.  If the socket is of type SOCK_STREAM
    // or SOCK_SEQPACKET, this call attempts to make a connection to the
    // socket that is bound to the address specified by addr.
    //
    // Generally, connection-based protocol sockets may successfully
    // connect() only once; connectionless protocol sockets may use
    // connect() multiple times to change their association.  Connectionless
    // sockets may dissolve the association by connecting to an address with
    // the sa_family member of sockaddr set to AF_UNSPEC (supported on Linux
    // since kernel 2.2).

    const auto ret1 = connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr));

    if (ret1 < 0) {
        std::cout << "ERROR connecting" << std::endl;
    }

    std::shared_ptr<char> buffer(new char[DIM_MSG], std::default_delete<char[]>());

    std::cout << " --> client --> sending messages..." << std::endl;

    for (uint32_t k = 0; k < NUM_MSG; k++) {

        bzero(buffer.get(), DIM_MSG);

        for (size_t i = 0; i < DIM_MSG; i++) {
            buffer.get()[i] = 'a';
        }

        buffer.get()[DIM_MSG - 1] = '1';

        // DESCRIPTION
        //
        // write() writes up to count bytes from the buffer starting at buf to
        // the file referred to by the file descriptor fd.
        //
        // The number of bytes written may be less than count if, for example,
        // there is insufficient space on the underlying physical medium, or the
        // RLIMIT_FSIZE resource limit is encountered (see setrlimit(2)), or the
        // call was interrupted by a signal handler after having written less
        // than count bytes.  (See also pipe(7).)
        //
        // For a seekable file (i.e., one to which lseek(2) may be applied, for
        // example, a regular file) writing takes place at the file offset, and
        // the file offset is incremented by the number of bytes actually
        // written.  If the file was open(2)ed with O_APPEND, the file offset is
        // first set to the end of the file before writing.  The adjustment of
        // the file offset and the write operation are performed as an atomic step.
        //
        // POSIX requires that a read(2) that can be proved to occur after a
        // write() has returned will return the new data.  Note that not all
        // filesystems are POSIX conforming.
        //
        // According to POSIX.1, if count is greater than SSIZE_MAX, the result
        // is implementation-defined; see NOTES for the upper limit on Linux.

        const auto ret2 = write(sockfd, buffer.get(), DIM_MSG);
        const auto error_code = errno;

        if (ret2 < 0) {
            std::cout << "ERROR writing to socket --> " << error_code << std::endl;
        }
    }

    close(sockfd);

    return 0;
}
