#!/bin/bash

  g++-9.3.0 -O3           \
    	    -Wall         \
     	    -std=gnu++2a  \
	        -I/opt/boost/1.70.0/include \
     	    -L/opt/boost/1.70.0/lib/ -lboost_system \
     	    -L/opt/boost/1.70.0/lib/ -lboost_thread \
     	    -lpthread \
      	    receiver_boost_asio.cpp \
      	    -o x_receiver_asio
