#include <boost/asio.hpp>
#include <iostream>
#include <vector>
#include <cmath>
#include <chrono>
#include <thread>

constexpr auto NUM_MSG{(size_t) 1'000'000'000};
constexpr auto DIM_MSG{(size_t) 20*8};

auto main(int argc, char *argv[]) -> int {

    boost::system::error_code ignored_error{};

    if (argc < 3) {
        std::cout << "usage: " << argv[0] << " hostname port" << std::endl;
        exit(-1);
    }

    // get hostname and port number

    const auto host_name{argv[1]};
    const auto port_num{(uint16_t) strtol(argv[2], argv, 10)};

    // set up the socket

    auto ios{boost::asio::io_service{}};
    auto endpoint{boost::asio::ip::tcp::endpoint(boost::asio::ip::address::from_string(host_name), port_num)};
    auto socket{boost::asio::ip::tcp::socket(ios)};

    // connect to the socket

    socket.connect(endpoint);

    std::cout << " --> client --> sending messages..." << std::endl;

    // build the message here

    auto buffer{std::shared_ptr<char>(new char[DIM_MSG], std::default_delete<char[]>())};

    memset(buffer.get(), 0.0, DIM_MSG);

    for (size_t i = 0; i < DIM_MSG; i++) {
        buffer.get()[i] = 'a';
    }

    buffer.get()[DIM_MSG - 1] = '1';

    // write to the socket

    for (size_t i = 0; i < NUM_MSG; i++) {

        auto total{(int64_t) DIM_MSG};

        while (total > 0) {
            total -= socket.write_some(boost::asio::buffer(buffer.get(), DIM_MSG), ignored_error);
        }
    }

    // close socket

    socket.close();
}
