#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <boost/container/vector.hpp>
#include <iostream>
#include <vector>
#include <cmath>
#include <chrono>
#include <thread>
#include <string>

constexpr auto NUM_MSG{(size_t) 1'000'000'000};
constexpr auto DIM_MSG{(size_t) 8};
constexpr auto MOD_MSG{(size_t) 1'000'000};
constexpr auto READ_MSG_EVERY_SEC{(size_t) 0};
constexpr auto flg_debug{false};
constexpr auto G1{1024.0 * 1024.0 * 1024.0};

auto main(int argc, char *argv[]) -> int {

    if (argc < 2) {
        std::cout << "usage: " << argv[0] << " port" << std::endl;
        exit(-1);
    }

    // get port number

    const auto port_num{(uint16_t) strtol(argv[1], argv, 10)};

    // set up the socket

    auto io_context{boost::asio::io_context{}};

    auto acceptor{boost::asio::ip::tcp::acceptor(
            io_context,
            boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port_num))};

    auto socket{boost::asio::ip::tcp::socket(io_context)};

    acceptor.accept(socket);

    auto data{std::shared_ptr<char>(new char[DIM_MSG], std::default_delete<char[]>())};

    auto t1{std::chrono::steady_clock::now()};

    for (size_t i = 0; i < NUM_MSG; i++) {

        if constexpr (READ_MSG_EVERY_SEC != 0) {
            std::this_thread::sleep_for(std::chrono::seconds(READ_MSG_EVERY_SEC));
        }

        auto count{(size_t) 0};

        while (count < DIM_MSG) {
            count += socket.receive(boost::asio::buffer(data.get() + count, DIM_MSG - count));
        }

 //       std::cout << '.';
 //       std::cout.flush();

        if (i != 0 && i % MOD_MSG == 0) {

            const auto t2{std::chrono::steady_clock::now()};

            const auto time_tot{std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1)};

            std::cout << std::endl;
            std::cout << " --> time for = " << time_tot.count() << std::endl;
            std::cout << " --> data speed (GBytes/sec) = "
                      << (DIM_MSG * MOD_MSG * sizeof(char)) / time_tot.count() / G1 << std::endl;

            t1 = t2;
        }

        if constexpr (flg_debug) {

            if (data.get()[DIM_MSG - 1] != '1') {

                std::cout << " --> server --> error reading message # " << i << " --> "
                          << data.get()[DIM_MSG - 1] << std::endl;
                exit(-1);
            }
        }
    }

    // close socket

    socket.close();
}
