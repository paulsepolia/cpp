#include <iostream>
#include <iomanip>
#include <vector>
#include <array>

int main()
{
	std::cout << std::fixed;
	std::cout << std::setprecision(10);
	constexpr uint64_t DIM_MAX_2GB = 268435456;
	const double ONE_GB = 1024.0 * 1024.0 * 1024.0;
	constexpr uint64_t MOD_DBG = 1000;
	std::cout << "Memory allocation application." << std::endl;
	std::vector<double> v;
	v.reserve(DIM_MAX_2GB);

	for (size_t i = 0; i < DIM_MAX_2GB; i++)
	{
		v[i] = (double)(i);

		if ((i + 1) % MOD_DBG == 0)
		{
			std::cout << "Increasing the RAM             ---->> " << i + 1 << std::endl;
			std::cout << "RAM already allocated (MBytes) ---->> " << ((i + 1) * sizeof(double)) / (1024.0 * 1024.0)
					  << std::endl;
		}
	}

	std::cout << v[0] << std::endl;
	const uint64_t bytes_reached = DIM_MAX_2GB * sizeof(double);
	std::cout << "cgroups policies did not apply or failed, " << (bytes_reached / ONE_GB) << std::endl;
}
