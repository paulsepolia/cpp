#!/bin/bash

  g++-9.1  -O3           \
           -Wall         \
           -std=gnu++2a  \
           server.cpp    \
           -o x_server
