#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <iostream>

int main() {

    // local parameters

    const int32_t PORT_NUM(57717);
    const std::string HOST("127.0.0.1");

    // local variables

    int32_t socket_fd(0);
    int32_t n(0);
    struct sockaddr_in serv_addr;
    struct hostent *server;
    char *buffer;

    socket_fd = socket(AF_INET, SOCK_STREAM, 0);

    if (socket_fd < 0) {
        std::cout << "ERROR opening socket" << std::endl;
    }

    server = gethostbyname(HOST.c_str());

    if (!server) {
        std::cout << "ERROR, no such host" << std::endl;
        exit(0);
    }

    bzero((char *) &serv_addr, sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;

    bcopy((char *) server->h_addr, (char *) &serv_addr.sin_addr.s_addr, server->h_length);

    serv_addr.sin_port = htons(PORT_NUM);

    if (connect(socket_fd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
        std::cout << "ERROR connecting" << std::endl;
    }

    // open file to read data

    std::cout << " reading file to send the data from ..." << std::endl;

    FILE *pFile(fopen("data.txt", "r"));

    fseek(pFile, 0, SEEK_END);
    const uint64_t file_size(ftell(pFile));
    rewind(pFile);

    std::cout << " num of chars in the file are: " << file_size << std::endl;

    // allocate memory to contain the whole file

    buffer = new char[file_size];

    // copy the file into the buffer

    uint64_t result = fread(buffer, 1, file_size, pFile);

    if (result != file_size) {
        std::cout << "Reading error from the file" << std::endl;
        exit(1);
    }

    // the whole file is now loaded in the memory buffer

    n = write(socket_fd, buffer, file_size);

    if (n < 0) {
        std::cout << "ERROR writing to socket" << std::endl;
    }

    bzero(buffer, file_size);

    n = read(socket_fd, buffer, file_size - 1);

    if (n < 0) {
        std::cout << "ERROR reading from socket" << std::endl;
    }

    std::cout << buffer << std::endl;

    close(socket_fd);

    delete[] buffer;
}
