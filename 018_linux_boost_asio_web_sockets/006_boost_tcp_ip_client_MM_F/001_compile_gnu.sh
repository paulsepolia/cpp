#!/bin/bash

  g++-9.1  -O3                \
           -Wall              \
           -std=gnu++2a       \
           -lboost_system     \
           -lboost_thread     \
           -lpthread          \
           driver_program.cpp \
           -o x_gnu
