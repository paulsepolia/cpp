#!/bin/bash
	
  g++-9.1  -O3            \
    	   -Wall          \
     	   -std=gnu++2a   \
    	   -lboost_system \
     	   -lboost_thread \
     	   -lpthread \
           ReceiveFdelMessage.cpp \
      	   driver_program.cpp \
      	   -o x_receiver
