#!/bin/bash

# build the serialized info of the struct/class
g++ publish.cpp -std=gnu++11 -g -c
sercoder -i publish.o -o ser_ressource.cpp

# clean-up
rm -rf build_debug

# build
mkdir build_debug
cd build_debug || exit
cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug ..
cmake --build .
