#pragma once

#include "ChannelBase.hpp"
#include <queue>
#include <mutex>
#include <condition_variable>
#include <map>
#include <vector>
#include <string>

// Algorithm about Channel

// step -->> 1: Create channel
// step -->> 2: Start listening for data -->> put data to the data buffer, only up to history
// step -->> 3: Start recording the data -->> put data to the data buffer, use all the buffer
// step -->> 4: Stop recording the incoming data
// step -->> 5: Stop listening for data
// step -->> 6: Destroy channel

template<typename DataType>
class ChannelParameters
{
 public:
	ChannelParameters(
		std::string channel_name,
		uint64_t channel_buffer_size_history,
		uint64_t channel_buffer_size_max,
		uint64_t record_size,
		DataType data_type) : channel_name(std::move(channel_name)),
							  channel_buffer_size_history(channel_buffer_size_history),
							  channel_buffer_size_max(channel_buffer_size_max),
							  record_size(record_size),
							  data_type(data_type)
	{
	}

	ChannelParameters(const ChannelParameters&) = default;
	ChannelParameters(ChannelParameters&&) noexcept = default;
	ChannelParameters& operator=(const ChannelParameters&) = default;
	ChannelParameters& operator=(ChannelParameters&&) noexcept = default;

	std::string channel_name;
	uint64_t channel_buffer_size_history = 0;
	uint64_t channel_buffer_size_max = 0;
	uint64_t record_size = 0;
	DataType data_type{};
};

template<typename DataType>
class ChannelV1 final : public ChannelBase
{
 private:
	bool m_is_listening = false;
	bool m_is_recording = false;
	std::vector<DataType> m_data;
	std::queue<std::vector<DataType>> m_channel_buffer;
	std::queue<DataType*> m_channel_buffer_of_pointers;
	mutable std::mutex m_mtx;
	std::condition_variable m_cv;
	ChannelParameters<DataType> m_channel_parameters{};

 public:
	explicit ChannelV1(ChannelParameters<DataType> channelParameters)
		: m_channel_parameters(std::move(channelParameters))
	{
	}

	ChannelV1() = default;
	~ChannelV1() final = default;
	ChannelV1(const ChannelV1&) = delete;
	ChannelV1(ChannelV1&&) = delete;
	ChannelV1& operator=(const ChannelV1&) = delete;
	ChannelV1& operator=(ChannelV1&&) = delete;

	void put_record(const DataType* data)
	{
		std::unique_lock<std::mutex> lck(m_mtx);

		if (!m_is_listening)
		{
			return;
		}

		m_data.reserve(m_channel_parameters.record_size);
		m_data.resize(m_channel_parameters.record_size);

		for (size_t i = 0; i < m_data.size(); i++)
		{
			m_data[i] = data[i];
		}

		uint64_t channel_buffer_size = 0;

		if (m_is_listening && !m_is_recording)
		{
			channel_buffer_size = m_channel_parameters.channel_buffer_size_history;
		}
		else if (m_is_listening && m_is_recording)
		{
			channel_buffer_size = m_channel_parameters.channel_buffer_size_max;
		}

		if (!m_channel_buffer.empty())
		{
			while (m_channel_buffer.size() >= channel_buffer_size)
			{
				m_channel_buffer.pop();

				if (m_channel_buffer.empty())
				{
					break;
				}
			}
		}

		m_channel_buffer.push(std::move(m_data));
		m_cv.notify_one();
	}

	void put_record(std::vector<DataType>&& data)
	{
		std::unique_lock<std::mutex> lck(m_mtx);

		if (!m_is_listening)
		{
			return;
		}

		m_data = std::move(data);

		uint64_t channel_buffer_size = 0;

		if (m_is_listening && !m_is_recording)
		{
			channel_buffer_size = m_channel_parameters.channel_buffer_size_history;
		}
		else if (m_is_listening && m_is_recording)
		{
			channel_buffer_size = m_channel_parameters.channel_buffer_size_max;
		}

		if (!m_channel_buffer.empty())
		{
			while (m_channel_buffer.size() >= channel_buffer_size)
			{
				m_channel_buffer.pop();

				if (m_channel_buffer.empty())
				{
					break;
				}
			}
		}

		m_channel_buffer.push(std::move(m_data));
		m_cv.notify_one();
	}

	std::vector<DataType> get_record()
	{
		std::unique_lock<std::mutex> lck(m_mtx);
		std::vector<DataType> el;

		if (!m_channel_buffer.empty())
		{
			el = m_channel_buffer.front();
			m_channel_buffer.pop();
			return el;
		}

		while (m_channel_buffer.empty())
		{
			m_cv.wait(lck);
			if (!m_channel_buffer.empty())
			{
				el = m_channel_buffer.front();
				m_channel_buffer.pop();
				break;
			}
		}
		return el;
	}

	void start_listening() final
	{
		std::unique_lock<std::mutex> lck(m_mtx);

		if (m_is_listening)
		{
			return;
		}
		m_is_listening = true;
	}

	void stop_listening() final
	{
		stop_recording();

		std::unique_lock<std::mutex> lck(m_mtx);

		if (!m_is_listening)
		{
			return;
		}

		m_is_listening = false;
		m_channel_buffer = std::queue<std::vector<DataType>>();
	}

	bool is_listening() final
	{
		std::unique_lock<std::mutex> lck(m_mtx);
		return m_is_listening;
	}

	void start_recording() final
	{
		start_listening();

		std::unique_lock<std::mutex> lck(m_mtx);

		if (m_is_recording)
		{
			return;
		}
		m_is_recording = true;
	}

	void stop_recording() final
	{
		std::unique_lock<std::mutex> lck(m_mtx);

		if (!m_is_recording)
		{
			return;
		}
		m_is_recording = false;
	}

	bool is_recording() final
	{
		std::unique_lock<std::mutex> lck(m_mtx);
		return m_is_recording;
	}

	size_t get_channel_buffer_size() const final
	{
		std::unique_lock<std::mutex> lck(m_mtx);
		return m_channel_buffer.size();
	}

	const ChannelParameters<DataType>& get_channel_parameters() const
	{
		return m_channel_parameters;
	}
};
