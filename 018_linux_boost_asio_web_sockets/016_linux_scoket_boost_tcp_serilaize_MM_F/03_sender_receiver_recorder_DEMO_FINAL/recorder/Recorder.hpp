#pragma once

#include <string>

template<typename DataType>
class ChannelParameters;

class ChannelBase;

template<typename RecorderImpl>
class Recorder
{
 private:
	RecorderImpl recorderImpl;

 public:
	Recorder() = default;
	~Recorder() = default;

	Recorder(const Recorder&) = delete;
	Recorder(Recorder&&) = delete;
	Recorder& operator=(const Recorder&) = delete;
	Recorder& operator=(Recorder&&) = delete;

	// start listening all channels
	void activate()
	{
		recorderImpl.activate();
	}

	// stop listening all channels
	void deactivate()
	{
		recorderImpl.deactivate();
	}

	// starts recording specific channel
	template<typename DataType>
	void start(const ChannelParameters<DataType>& channelParameters)
	{
		recorderImpl.start(channelParameters);
	}

	// stops recording specific channel
	template<typename DataType>
	void stop(const ChannelParameters<DataType>& channelParameters)
	{
		recorderImpl.stop(channelParameters);
	}

	// create channel
	template<typename DataType>
	void create_channel(const ChannelParameters<DataType>& channelParameters)
	{
		recorderImpl.create_channel(channelParameters);
	}

	// destroy channel
	template<typename DataType>
	void destroy_channel(const ChannelParameters<DataType>& channelParameters)
	{
		recorderImpl.destroy_channel(channelParameters);
	}

	ChannelBase* get_channel(const std::string& channel_name)
	{
		return recorderImpl.get_channel(channel_name);
	}
};
