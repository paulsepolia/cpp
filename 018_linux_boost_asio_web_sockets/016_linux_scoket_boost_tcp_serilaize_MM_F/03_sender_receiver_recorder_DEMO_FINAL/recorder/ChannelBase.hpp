#pragma once

#include <exception>

class ChannelBase
{
protected:
	ChannelBase() = default;

public:
	virtual ~ChannelBase() = default;
	ChannelBase(const ChannelBase &) = delete;
	ChannelBase(ChannelBase &&) = delete;
	ChannelBase &operator=(const ChannelBase &) = delete;
	ChannelBase &operator=(ChannelBase &&) = delete;

	[[maybe_unused]] virtual void start_listening()
	{
		std::terminate();
	};

	[[maybe_unused]] virtual void stop_listening()
	{
		std::terminate();
	};

	[[maybe_unused]] virtual bool is_listening()
	{
		std::terminate();
	};

	[[maybe_unused]] virtual void start_recording()
	{
		std::terminate();
	};

	[[maybe_unused]] virtual void stop_recording()
	{
		std::terminate();
	};

	[[maybe_unused]] virtual bool is_recording()
	{
		std::terminate();
	};

	[[maybe_unused]] virtual size_t get_channel_buffer_size() const
	{
		std::terminate();
	};
};
