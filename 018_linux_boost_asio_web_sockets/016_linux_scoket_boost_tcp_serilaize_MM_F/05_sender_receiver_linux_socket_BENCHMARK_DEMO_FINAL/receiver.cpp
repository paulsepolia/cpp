#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <cstring>
#include <chrono>
#include <memory>
#include <cmath>
#include <thread>

const size_t NUM_MSGS = 1000000000;
const size_t DIM_MSG = 8 * 1000000;
const size_t MOD_MSG = 2000;
bool READ_MSG_EVERY_SEC = false;
bool FLG_DEBUG = true;

int main(int argc, char* argv[])
{
	if (argc < 2)
	{
		std::cout << "INFO: RECEIVER: USAGE: " << argv[0] << " port" << std::endl;
		return 0;
	}

	socklen_t clilen;
	std::shared_ptr<char> buffer(new char[DIM_MSG], std::default_delete<char[]>());
	struct sockaddr_in serv_addr{};
	struct sockaddr_in cli_addr{};
	const auto portno = static_cast<uint16_t>(std::stoi(argv[1]));

	const auto socket_fd = socket(AF_INET, SOCK_STREAM, 0);

	if (socket_fd < 0)
	{
		std::cout << "ERROR: RECEIVER: ERROR WHILE OPENING SOCKET" << std::endl;
		std::terminate();
	}

	// void bzero(void *s, size_t n);
	// The bzero() function erases the data in the n bytes of the memory
	// starting at the location pointed to by s, by writing zeros (bytes
	// containing '\0') to that area.

	bzero((char*)&serv_addr, sizeof(serv_addr));

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(portno);

	const auto ret1 = bind(socket_fd, (struct sockaddr*)&serv_addr, sizeof(serv_addr));

	if (ret1 < 0)
	{
		std::cout << "ERROR: RECEIVER: ERROR WHILE BINDING TO THE SOCKET" << std::endl;
		std::terminate();
	}

	listen(socket_fd, 5);
	clilen = sizeof(cli_addr);

	const auto new_socket_fd = accept(socket_fd, (struct sockaddr*)&cli_addr, &clilen);

	if (new_socket_fd < 0)
	{
		std::cout << "ERROR: RECEIVER: ERROR ON ACCEPT" << std::endl;
		std::terminate();
	}

	std::cout << "INFO: RECEIVER: READING MESSAGES..." << std::endl;

	auto t1 = std::chrono::steady_clock::now();

	for (size_t i = 0; i < NUM_MSGS; i++)
	{
		if (READ_MSG_EVERY_SEC)
		{
			std::this_thread::sleep_for(std::chrono::seconds(READ_MSG_EVERY_SEC));
		}

		bzero(buffer.get(), DIM_MSG);
		size_t total_bytes_read = 0;

		while (total_bytes_read < DIM_MSG)
		{
			const auto bytes_read =
				read(new_socket_fd, buffer.get() + total_bytes_read, DIM_MSG - total_bytes_read);

			if (bytes_read >= 0)
			{
				total_bytes_read += bytes_read;
			}
		}

		if (i != 0 && i % MOD_MSG == 0)
		{
			auto t2 = std::chrono::steady_clock::now();

			const auto time_tot =
				std::chrono::duration_cast<std::chrono::duration<double >>(t2 - t1);

			std::cout << "INFO: RECEIVER: DATA SPEED (GBytes/sec): "
					  << (DIM_MSG * MOD_MSG * sizeof(char)) / (time_tot.count() * std::pow(1024.0, 3.0)) << std::endl;

			t1 = t2;
		}

		if (FLG_DEBUG)
		{
			if (buffer.get()[DIM_MSG - 1] != '1')
			{
				std::cout << "ERROR: RECEIVER: ERROR READING MESSAGE # " << i << ": "
						  << buffer.get()[DIM_MSG - 1] << std::endl;
				std::terminate();
			}
		}
	}

	close(new_socket_fd);
	close(socket_fd);
}

