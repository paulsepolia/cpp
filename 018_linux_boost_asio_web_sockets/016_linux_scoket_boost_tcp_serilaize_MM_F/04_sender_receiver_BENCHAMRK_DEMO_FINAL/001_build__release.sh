#!/bin/bash

# build the serialized info of the struct/class
g++ publish.cpp -std=gnu++11 -g -c
sercoder -i publish.o -o ser_ressource.cpp

# clean-up
rm -rf build_release

# build
mkdir build_release
cd build_release || exit
cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Release ..
cmake --build .
