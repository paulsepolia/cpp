#include <serialize/net_archive.hpp>
#include <serialize/ser_ostream.hpp>
#include <iostream>
#include <memory>
#include <chrono>
#include <vector>
#include "mystruct.hpp"

const char* RECEIVER_PORT = "12345";
const size_t MOD_DEBUG = 2000;
bool FLG_DEBUG = false;

//===============//
// AUX FUNCTIONS //
//===============//

void print_file_header(const ser::CFileHeader& f_hdr)
{
	switch (f_hdr.getCompressAlgo())
	{
	case ser::CFileHeader::None:
		std::cout << "INFO: PRINT-FILE-HEADER: STREAM IS NOT COMPRESSED" << std::endl;
		break;
	case ser::CFileHeader::LZ4:
		std::cout << "INFO: PRINT-FILE-HEADER: STREAM IS LZ4 COMPRESSED" << std::endl;
		break;
	default:
		std::cout << "INFO: PRINT-FILE-HEADER: UNKNOWN COMPRESSION" << std::endl;
	}
}

void get_data_from_sender(ser::CNetServer::CNetReader& f_clStream)
{
	std::cout << "INFO: RECEIVER: RECEIVING DATA..." << std::endl;

	print_file_header(f_clStream.getHeader());

	try
	{
		size_t ii = 0;
		static auto start_time = std::chrono::system_clock::now();
		static auto end_time = std::chrono::system_clock::now();

		while (!f_clStream.eof())
		{
			if (const ser::CClassInfo* l_classInfo_p = f_clStream.peek())
			{
				ii++;
				ser::CObjGuard l_obj;
				f_clStream >> l_obj;
				//ser::cout << l_obj;

				//================//
				// BENCHMARK HERE //
				//================//
				if (ii % MOD_DEBUG == 0)
				{
					end_time = std::chrono::system_clock::now();
					auto elapsed =
						std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time);
					std::cout << "----------------------------------------------------------------->> " << ii
							  << std::endl;
					std::cout << "-->> INFO: RECEIVER: "
							  << (MOD_DEBUG * double(l_obj.getSize()) / (1024.0 * 1024.0 * 1024.0))
								  / (double(elapsed.count()) / 1000.0) << " GBytes/sec" << std::endl;
					start_time = end_time;
					std::cout << "-->> INFO: RECEIVER: TOTAL ELEMENTS RECEIVED  : " << ii << std::endl;
					std::cout << "-->> INFO: RECEIVER: PEEK GOT: " << l_classInfo_p->getClassName() << std::endl;
				}

				const auto realObj = l_obj.getBuf();

				//===========//
				// TEST HERE //
				//===========//
				if (FLG_DEBUG)
				{
					auto m_data_again = (double*)(realObj);
					for (size_t kk = 0; kk < DATA_DIM; kk++)
					{
						if (m_data_again[kk] != (double(kk) + DECIMALS))
						{
							std::cout << "ERROR: RECEIVER: NOT EXPECTED VALUE: " << m_data_again[kk] << std::endl;
							std::terminate();
						}
					}
				}
			}
			else
			{
				std::cout << "INFO: RECEIVER: PEEK FAILED" << std::endl;
			}
		}
	}
	catch (ser::CSerException& e)
	{
		if (!f_clStream.eof())
		{
			std::cout << "ERROR: RECEIVER: EXCEPTION FROM SERIALIZE LIB: " << e.what() << std::endl;
			std::cout << "ERROR: RECEIVER: " << e.reason() << std::endl;
		}
	}
	catch (...)
	{
		std::cout << "ERROR: RECEIVER: UNKNOWN EXCEPTION" << std::endl;
	}

	std::cout << "INFO: RECEIVER: CONNECTION CLOSED" << std::endl;
}

int main()
{
	ser::CNetServer l_receiver(RECEIVER_PORT);

	std::cerr << "INFO: RECEIVER: LISTENING PORT: " << RECEIVER_PORT << std::endl;

	if (!l_receiver)
	{
		std::cerr << "ERROR: RECEIVER: CANNOT RECEIVE DATA" << std::endl;
		return 1;
	}

	while (true)
	{
		std::unique_ptr<ser::CNetServer::CNetReader> l_archive = l_receiver.waitClient();

		if (!l_archive)
		{
			std::cerr << "ERROR: RECEIVER: WRONG STREAM" << std::endl;
			break;
		}

		get_data_from_sender(*l_archive);
	}
}
