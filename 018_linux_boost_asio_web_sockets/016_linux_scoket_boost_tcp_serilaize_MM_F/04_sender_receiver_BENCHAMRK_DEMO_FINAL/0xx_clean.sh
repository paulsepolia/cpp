#!/bin/bash

rm -rf build_release
rm -rf build_debug
rm -rf publish.o
rm -rf ser_ressource.cpp
rm -rf test*.ar
rm -rf cmake-build-debug/test*.ar
rm -rf cmake-build-release/test*.ar
