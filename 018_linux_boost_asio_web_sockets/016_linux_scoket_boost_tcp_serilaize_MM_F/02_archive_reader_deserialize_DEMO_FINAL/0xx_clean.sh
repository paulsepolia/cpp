#!/bin/bash

rm -rf build_release
rm -rf build_debug
rm -rf arxeio_meta.ar
rm -rf cmake-build-debug/arxeio_meta.ar
rm -rf cmake-build-release/arxeio_meta.ar
