#!/bin/bash

# clean-up
rm -rf build_debug

# build
mkdir build_debug
cd build_debug || exit
cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug ..
cmake --build .
