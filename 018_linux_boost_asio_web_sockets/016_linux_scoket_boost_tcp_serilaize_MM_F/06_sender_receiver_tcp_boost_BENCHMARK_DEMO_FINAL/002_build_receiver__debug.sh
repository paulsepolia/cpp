#!/bin/bash

g++ -O0 -g \
    -Wall \
    -std=gnu++17 \
    receiver.cpp \
    -lboost_system -lboost_thread -lpthread\
    -o x_receiver_debug
