#include <iostream>
#include <deque>
#include <mutex>
#include <condition_variable>
#include <thread>

static const size_t MAX_SIZE_DATA_BUFFER = 1'000'000UL;
static std::deque<double> dataBuffer{};
static double newData = 0;
static std::mutex mtx{};
static std::condition_variable cv{};

void ProduceData()
{
    while (true)
    {
        std::unique_lock<std::mutex> lck(mtx);
        newData++;
        cv.wait(lck,
                [&]()
                { return dataBuffer.size() < MAX_SIZE_DATA_BUFFER; });
        std::cout << "Put data: " << newData << " and size is: " << dataBuffer.size() << std::endl;
        dataBuffer.push_back(newData);
        lck.unlock();
        cv.notify_one();
    }
}

void ConsumeData()
{
    while (true)
    {
        std::unique_lock<std::mutex> lck(mtx);
        cv.wait(lck,
                [&]()
                { return dataBuffer.size() != 0; });
        const auto data = dataBuffer.front();
        dataBuffer.pop_front();
        std::cout << "Got: " << data << " and size is: " << dataBuffer.size() << std::endl;
        lck.unlock();
        cv.notify_one();
    }
}

int main()
{
    std::thread t1(ConsumeData);
    std::thread t2(ProduceData);

    t1.join();
    t2.join();

    return 0;
}
