#include <iostream>
#include <iomanip>

int main()
{
	std::cout << std::fixed;
	std::cout << std::setprecision(10);
	// COUNTER_MAX * sizeof(uint64_t) * DIM = bytes max reached
	constexpr uint64_t COUNTER_MAX = 268435;
	const double ONE_GB = 1024.0 * 1024.0 * 1024.0;
	constexpr uint64_t MOD_DBG = 1;
	const int DIM = 1000;

	std::cout << "Memory allocation application." << std::endl;
	uint64_t counter = 0;

	while (true)
	{
		counter++;
		uint64_t *data = new uint64_t[DIM];

		for (uint64_t i = 0; i < DIM; i++)
		{
			data[i] = i;
		}

		if (counter % MOD_DBG == 0)
		{
			std::cout << "Increasing the RAM             ---->> " << counter << std::endl;
			std::cout << "RAM already allocated (MBytes) ---->> "
					  << (counter * sizeof(uint64_t) * DIM) / (1024.0 * 1024.0) << std::endl;
		}

		if (counter > COUNTER_MAX)
		{
			std::cout << data[1] << std::endl;
			break;
		}
	}

	const uint64_t bytes_reached = COUNTER_MAX * sizeof(uint64_t) * DIM;
	std::cout << "cgroups policies did not apply or failed, " << (bytes_reached / ONE_GB) << std::endl;
}
