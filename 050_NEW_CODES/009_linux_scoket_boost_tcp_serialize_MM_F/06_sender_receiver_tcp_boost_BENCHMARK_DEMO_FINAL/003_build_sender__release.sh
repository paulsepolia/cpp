#!/bin/bash

g++ -O3 \
    -Wall \
    -std=gnu++17 \
    sender.cpp \
    -lboost_system -lboost_thread -lpthread\
    -o x_sender_release
