#!/bin/bash

g++ -O0 -g \
    -Wall \
    -std=gnu++17 \
    sender.cpp \
    -lboost_system -lboost_thread -lpthread\
    -o x_sender_debug
