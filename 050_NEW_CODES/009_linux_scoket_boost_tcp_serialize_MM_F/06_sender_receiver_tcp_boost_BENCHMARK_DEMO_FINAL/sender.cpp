#include <boost/asio.hpp>
#include <iostream>
#include <vector>
#include <chrono>
#include <thread>

const size_t NUM_MSGS = 1000000000;
const size_t DIM_MSG = 8 * 1000000;

int main(int argc, char* argv[])
{
	boost::system::error_code ignored_error{};

	if (argc < 3)
	{
		std::cout << "INFO: SENDER: USAGE: " << argv[0] << " hostname port" << std::endl;
		return 0;
	}

	const auto host_name = argv[1];
	const auto port_num = (uint16_t)std::stoi(argv[2]);
	auto ios{ boost::asio::io_service{}};
	auto endpoint{ boost::asio::ip::tcp::endpoint(boost::asio::ip::address::from_string(host_name), port_num) };
	auto socket{ boost::asio::ip::tcp::socket(ios) };

	socket.connect(endpoint);

	std::cout << "INFO: SENDER: SENDING MESSAGES..." << std::endl;

	//===================//
	// Build the message //
	//===================//

	auto buffer = std::shared_ptr<char>(new char[DIM_MSG], std::default_delete<char[]>());

	memset(buffer.get(), 0.0, DIM_MSG);

	for (size_t i = 0; i < DIM_MSG; i++)
	{
		buffer.get()[i] = 'a';
	}

	buffer.get()[DIM_MSG - 1] = '1';

	//=====================//
	// Write to the socket //
	//=====================//

	for (size_t i = 0; i < NUM_MSGS; i++)
	{
		auto total = DIM_MSG;

		while (total > 0)
		{
			total -= socket.write_some(boost::asio::buffer(buffer.get(), DIM_MSG), ignored_error);
		}
	}

	socket.close();
}
