#!/bin/bash

g++ -O3 \
    -Wall \
    -std=gnu++17 \
    receiver.cpp \
    -lboost_system -lboost_thread -lpthread\
    -o x_receiver_release
