#include <string>
#include <cstring>
#include <stdexcept>

struct MyStruct
{
	static const size_t size = 80;
	char buf[size];

	explicit MyStruct(const std::string& f_msg) : buf{}
	{
		if (f_msg.size() > size)
		{
			throw std::runtime_error(std::string("too long message"));
		}

		std::memcpy(buf, f_msg.c_str(), f_msg.size());

		buf[f_msg.size()] = '\0';
	}
};
