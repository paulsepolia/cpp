#include "mystruct.hpp"
#include <serialize/file_archive.hpp>
#include <serialize/ser_tostring.hpp>
#include <serialize/ser_ostream.hpp>
#include <iostream>

static int serialize(const char* f_filename)
{
	try
	{
		MyStruct l_msg1("Hello");
		MyStruct l_msg2("World");

		ser::CArchiveWriter::COptions l_options(
			ser::CFileHeader::LZ4,
			ser::CFileHeader::DataClassArchive,
			ser::protocol::CABI::makeABI(2148));

		ser::CArchiveWriter ar(f_filename, l_options);

		if (!ar)
		{
			std::cerr << "ERROR: SERIALIZE: CANNOT OPEN FILE: " << f_filename << std::endl;
			return 1;
		}

		ar << l_msg1;
		ar << l_msg2;

		return 0;
	}
	catch (ser::CSerException& e)
	{
		std::cout << e;
		return 1;

	}
	catch (...)
	{
		std::cout << "ERROR: SERIALIZE: UNKNOWN EXCEPTION" << std::endl;
		return 2;
	}
}

static int deserialize(const char* f_filename)
{
	try
	{
		MyStruct l_msg1("any-text");
		MyStruct l_msg2("any-text");

		ser::CArchiveReader ar(f_filename);

		if (!ar)
		{
			std::cerr << "ERROR: DESERIALIZE: CANNOT OPEN FILE: " << f_filename << std::endl;
			return 1;
		}

		ar >> l_msg1;
		ar >> l_msg2;

		ser::cout << l_msg1;
		std::cout << l_msg1.buf << std::endl;
		ser::cout << l_msg2;
		std::cout << l_msg2.buf << std::endl;

		return 0;
	}
	catch (ser::CSerException& e)
	{
		std::cout << e;
		return 1;
	}
	catch (...)
	{
		std::cout << "ERROR: DESERIALIZE: UNKNOWN EXCEPTION" << std::endl;
		return 2;
	}
}

int main(int argc, const char* argv[])
{
	if (argc < 3)
	{
		std::cerr << "usage: example1 <fn> [ser|deser]" << std::endl;
		return 1;
	}

	// The function ser::Load have to be called once to load the classInfos for the serlization library.

	ser::Load();

	if (std::strcmp(argv[2], "ser") == 0)
	{
		return serialize(argv[1]);
	}

	if (std::strcmp(argv[2], "deser") == 0)
	{
		return deserialize(argv[1]);
	}

	std::cerr << "usage:  example1 <fn> [ser|deser]" << std::endl;
}
