#include <serialize/net_archive.hpp>
#include "mystruct.hpp"
#include <iostream>
#include <string>

const size_t ELEMS_TO_SEND = 10000;
const char* SENDER_IP = "127.0.0.1";
const char* SENDER_PORT = "12345";

int main()
{
	ser::Load();

	ser::CNetWriter l_archive;

	std::cout << "INFO: SENDER: CONNECTION TO " << SENDER_IP << ":" << SENDER_PORT << std::endl;

	l_archive.connect(SENDER_IP, SENDER_PORT);

	if (!l_archive)
	{
		std::cerr << "INFO: SENDER: NET-WRITER CONNECTION FAILED" << std::endl;
		return 1;
	}

	try
	{
		for (size_t i = 0; i < ELEMS_TO_SEND; i++)
		{
			MyStruct l_test;
			l_archive << l_test;
			l_archive.flush();
		}
	}
	catch (ser::CSerException& e)
	{
		std::cout << "INFO: SENDER: EXCEPTION: " << e.what() << std::endl;
		std::cout << "INFO: SENDER: REASON: " << e.reason() << std::endl;
	}

	std::cout << "INFO: SENDER: MAIN IS DONE" << std::endl;
	return 0;
}
