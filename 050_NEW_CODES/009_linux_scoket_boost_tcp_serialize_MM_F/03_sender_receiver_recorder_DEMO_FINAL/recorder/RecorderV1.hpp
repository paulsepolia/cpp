#pragma once

#include "Recorder.hpp"
#include "ChannelV1.hpp"
#include <string>
#include <fstream>

class RecorderV1
{
 private:
	std::map<std::string, ChannelBase*> m_channel_names_and_channel_pointers;

	template<typename DataType>
	void do_writing_to_hard_disk(
		const ChannelParameters<DataType>& channelParameters,
		ChannelBase* channel)
	{
		const auto channel_name = channelParameters.channel_name;

		std::ofstream* file_stream_out_ptr;
		std::string file_path_out = "/tmp/";
		std::string file_name_out = "recorder_" + channel_name + ".bin";
		file_stream_out_ptr =
			new std::ofstream(file_path_out + file_name_out,
				std::ios::binary | std::ios::out | std::ofstream::trunc);

		if (channel)
		{
			auto channel_v1 = static_cast<ChannelV1<decltype(channelParameters.data_type)>*>(channel);

			while (channel_v1->is_recording())
			{
				if (channel_v1->get_channel_buffer_size() != 0)
				{
					auto record = channel_v1->get_record();

					if (!record.empty())
					{
						file_stream_out_ptr->write(
							reinterpret_cast<const char*>(&record[0]), channelParameters.record_size);

						file_stream_out_ptr->flush();
					}
				}
			}
		}
	}

 public:
	RecorderV1() = default;

	~RecorderV1()
	{
		for (auto& el : m_channel_names_and_channel_pointers)
		{
			delete el.second;
			el.second = nullptr;
		}
	}

	RecorderV1(const RecorderV1&) = delete;
	RecorderV1(RecorderV1&&) = delete;
	RecorderV1& operator=(const RecorderV1&) = delete;
	RecorderV1& operator=(RecorderV1&&) = delete;

	template<typename DataType>
	void create_channel(const ChannelParameters<DataType>& channelParameters)
	{
		if (m_channel_names_and_channel_pointers.find(channelParameters.channel_name) ==
			m_channel_names_and_channel_pointers.end())
		{
			auto channel = new ChannelV1<DataType>(channelParameters);
			m_channel_names_and_channel_pointers.insert({ channel->get_channel_parameters().channel_name, channel });
		}
	}

	template<typename DataType>
	void destroy_channel(const ChannelParameters<DataType>& channelParameters)
	{
		if (m_channel_names_and_channel_pointers.find(channelParameters.channel_name) !=
			m_channel_names_and_channel_pointers.end())
		{
			delete m_channel_names_and_channel_pointers.at(channelParameters.channel_name);
			m_channel_names_and_channel_pointers[channelParameters.channel_name] = nullptr;
			m_channel_names_and_channel_pointers.erase(channelParameters.channel_name);
		}
	}

	void activate()
	{
		for (auto& el : m_channel_names_and_channel_pointers)
		{
			if (el.second)
			{
				if (!el.second->is_listening())
				{
					el.second->start_listening();
				}
			}
		}
	}

	void deactivate()
	{
		for (auto& el : m_channel_names_and_channel_pointers)
		{
			if (el.second)
			{
				if (el.second->is_listening())
				{
					el.second->stop_listening();
				}
			}
		}
	}

	template<typename DataType>
	void start(const ChannelParameters<DataType>& channelParameters)
	{
		if (m_channel_names_and_channel_pointers.find(channelParameters.channel_name) ==
			m_channel_names_and_channel_pointers.end())
		{
			auto channel = new ChannelV1<DataType>(channelParameters);
			m_channel_names_and_channel_pointers.insert({ channel->get_channel_parameters().channel_name, channel });
		}

		if (!m_channel_names_and_channel_pointers.at(channelParameters.channel_name)->is_recording())
		{
			auto channel = m_channel_names_and_channel_pointers.at(channelParameters.channel_name);
			channel->start_recording();
			do_writing_to_hard_disk(channelParameters, channel);
		}
	}

	template<typename DataType>
	void stop(const ChannelParameters<DataType>& channelDetails)
	{
		if (m_channel_names_and_channel_pointers.at(channelDetails.channel_name))
		{
			if (m_channel_names_and_channel_pointers.at(channelDetails.channel_name)->is_recording())
			{
				m_channel_names_and_channel_pointers.at(channelDetails.channel_name)->stop_recording();
			}
		}
	}

	ChannelBase* get_channel(const std::string& channel_name)
	{
		if (m_channel_names_and_channel_pointers.find(channel_name) ==
			m_channel_names_and_channel_pointers.end())
		{
			return nullptr;
		}

		return m_channel_names_and_channel_pointers.at(channel_name);
	}
};
