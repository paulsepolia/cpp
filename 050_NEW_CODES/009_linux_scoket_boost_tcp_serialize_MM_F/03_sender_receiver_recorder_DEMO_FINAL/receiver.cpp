#include <serialize/net_archive.hpp>
#include <serialize/ser_ostream.hpp>
#include <iostream>
#include <memory>
#include <chrono>
#include <thread>
#include <vector>
#include "mystruct.hpp"
#include "recorder/RecorderV1.hpp"

const char* RECEIVER_PORT = "12345";
const size_t MOD_DEBUG = 500;
bool FLG_DEBUG = true;

//================//
// RECORDER STUFF //
//================//

const std::string CHANNEL_NAME1 = "DESERIALIZED_DATA_1";
const uint64_t CHANNEL_BUFFER_SIZE_MAX1 = 10000;
const uint64_t CHANNEL_BUFFER_SIZE_HISTORY1 = 10000;

const std::string CHANNEL_NAME2 = "DESERIALIZED_DATA_2";
const uint64_t CHANNEL_BUFFER_SIZE_MAX2 = 10000;
const uint64_t CHANNEL_BUFFER_SIZE_HISTORY2 = 10000;
const uint64_t DESERIALIZED_RECORD_SIZE = sizeof(double) * (DATA_DIM);

ChannelParameters<char> channelParametersDeserialized1(
	CHANNEL_NAME1,
	CHANNEL_BUFFER_SIZE_HISTORY1,
	CHANNEL_BUFFER_SIZE_MAX1,
	DESERIALIZED_RECORD_SIZE,
	0);

ChannelParameters<char> channelParametersDeserialized2(
	CHANNEL_NAME2,
	CHANNEL_BUFFER_SIZE_HISTORY2,
	CHANNEL_BUFFER_SIZE_MAX2,
	DESERIALIZED_RECORD_SIZE,
	0);

std::vector<ChannelParameters<char>> channelParametersDeserialized{ channelParametersDeserialized1,
																	channelParametersDeserialized2 };

//===============//
// AUX FUNCTIONS //
//===============//

void configure_recorder_step_initial(
	Recorder<RecorderV1>& recorder,
	std::vector<ChannelParameters<char>>& channelParametersDeser,
	std::vector<ChannelV1<char>*>& channelsDeser,
	std::vector<std::thread>& recording_threads,
	bool& start_recording_threads)
{
	// Create deserialized channels
	for (const auto& el : channelParametersDeser)
	{
		recorder.create_channel(el);
	}

	// Start listening all channels
	recorder.activate();

	// Get back the channels
	for (auto& el : channelParametersDeser)
	{
		auto channel_base = recorder.get_channel(el.channel_name);

		// check
		if (!channel_base)
		{
			std::cout << "ERROR: RECORDER: NOT A VALID CHANNEL" << std::endl;
			std::terminate();
		}

		const auto channel = dynamic_cast<ChannelV1<char>*>(channel_base);

		// check
		if (!channel)
		{
			std::cout << "ERROR: RECORDER: NOT A VALID CHANNEL: MSG2" << std::endl;
			std::terminate();
		}

		channelsDeser.push_back(channel);
	}

	// Start recording deserialized data
	if (start_recording_threads)
	{
		for (auto& el : channelParametersDeser)
		{
			auto rf = [&]()
			{ recorder.start(el); };

			recording_threads.emplace_back(std::thread(rf));
		}
		start_recording_threads = false;
	}
}

void configure_recorder_step_final(
	Recorder<RecorderV1>& recorder,
	std::vector<ChannelParameters<char>>& channelParametersDeser,
	std::vector<ChannelV1<char>*>& channelsDeser,
	std::vector<std::thread>& recording_threads)
{
	// stop channels recording
	for (auto& el : channelParametersDeser)
	{
		recorder.stop(el);
	}

	// join threads
	for (auto& el : recording_threads)
	{
		el.join();
	}

	// destroy channels
	for (auto& el : channelParametersDeser)
	{
		recorder.destroy_channel(el);
	}

	// deactivate recorder
	recorder.deactivate();
}

void get_data_from_sender(ser::CNetServer::CNetReader& f_clStream)
{
	//========================================//
	// Recorder initialization step --> begin //
	//========================================//
	Recorder<RecorderV1> recorder{};
	std::vector<ChannelV1<char>*> channelsDeserialized{};
	std::vector<ChannelV1<double>*> channelsSerialized{};
	std::vector<std::thread> recording_threads{};
	static bool create_and_start_recording_threads = true;

	configure_recorder_step_initial(
		recorder,
		channelParametersDeserialized,
		channelsDeserialized,
		recording_threads,
		create_and_start_recording_threads);
	//======================================//
	// Recorder initialization step --> end //
	//======================================//

	std::cout << "INFO: RECEIVER: RECEIVING DATA..." << std::endl;

	try
	{
		size_t ii = 0;
		static auto start_time = std::chrono::system_clock::now();
		static auto end_time = std::chrono::system_clock::now();

		while (!f_clStream.eof())
		{
			if (const ser::CClassInfo* l_classInfo_p = f_clStream.peek())
			{
				ii++;
				ser::CObjGuard l_obj;
				f_clStream >> l_obj;
				//ser::cout << l_obj;

				//================//
				// BENCHMARK HERE //
				//================//
				if (ii % MOD_DEBUG == 0)
				{
					end_time = std::chrono::system_clock::now();
					auto elapsed =
						std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time);
					std::cout << "----------------------------------------------------------------->> " << ii
							  << std::endl;
					std::cout << "-->> INFO: RECEIVER: "
							  << (MOD_DEBUG * double(l_obj.getSize()) / (1024.0 * 1024.0 * 1024.0))
								  / (double(elapsed.count()) / 1000.0) << " GBytes/sec" << std::endl;
					start_time = end_time;
					std::cout << "-->> INFO: RECEIVER: TOTAL ELEMENTS RECEIVED  : " << ii << std::endl;
					std::cout << "-->> INFO: RECEIVER: PEEK GOT: " << l_classInfo_p->getClassName() << std::endl;
				}

				const auto realObj = l_obj.getBuf();

				// Get a pointer to the deserialized buffer
				// and put deserialized data to the channels
				auto m_data = (char*)(realObj);

				for (auto& el : channelsDeserialized)
				{
					el->put_record(m_data);
				}

				//===========//
				// TEST HERE //
				//===========//
				if (FLG_DEBUG)
				{
					auto m_data_again = (double*)(realObj);
					for (size_t kk = 0; kk < DATA_DIM; kk++)
					{
						if (m_data_again[kk] != (double(kk) + DECIMALS))
						{
							std::cout << "ERROR: RECEIVER: NOT EXPECTED VALUE: " << m_data_again[kk] << std::endl;
							std::terminate();
						}
					}
				}
			}
			else
			{
				std::cout << "INFO: RECEIVER: PEEK FAILED" << std::endl;
			}
		}
	}
	catch (ser::CSerException& e)
	{
		if (!f_clStream.eof())
		{
			std::cout << "ERROR: RECEIVER: EXCEPTION FROM SERIALIZE LIB: " << e.what() << std::endl;
			std::cout << "ERROR: RECEIVER: " << e.reason() << std::endl;
		}
	}
	catch (...)
	{
		std::cout << "ERROR: RECEIVER: UNKNOWN EXCEPTION" << std::endl;
	}

	//===============================//
	// Recorder final step --> begin //
	//===============================//
	std::this_thread::sleep_for(std::chrono::seconds(2));

	configure_recorder_step_final(
		recorder,
		channelParametersDeserialized,
		channelsDeserialized,
		recording_threads);
	//=============================//
	// Recorder final step --> end //
	//=============================//

	std::cout << "INFO: RECEIVER: CONNECTION CLOSED" << std::endl;

	create_and_start_recording_threads = true;
}

int main()
{
	ser::CNetServer l_receiver(RECEIVER_PORT);

	std::cerr << "INFO: RECEIVER: LISTENING PORT: " << RECEIVER_PORT << std::endl;

	if (!l_receiver)
	{
		std::cerr << "ERROR: RECEIVER: CANNOT RECEIVE DATA" << std::endl;
		return 1;
	}

	while (true)
	{
		std::unique_ptr<ser::CNetServer::CNetReader> l_archive = l_receiver.waitClient();

		if (!l_archive)
		{
			std::cerr << "ERROR: RECEIVER: WRONG STREAM" << std::endl;
			break;
		}

		get_data_from_sender(*l_archive);
	}
}
