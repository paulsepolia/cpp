#include <iostream>

const size_t DATA_DIM = 10000;
const double DECIMALS = 0.123456789;

class MyStruct
{
 public:
	MyStruct()
	{
		for (size_t i = 0; i < m_size; i++)
		{
			m_data[i] = double(i) + DECIMALS;
		}
	}

	~MyStruct() = default;

 private:
	static const size_t m_size = DATA_DIM;
	double m_data[m_size]{};
};
