#include <serialize/file_archive.hpp>
#include <serialize/ser_tostring.hpp>
#include <serialize/ser_ostream.hpp>
#include <serialize/ser_objguard.hpp>
#include <iostream>

bool DBG_FLG = true;

int main(int argc, const char* argv[])
{
	if (argc < 2)
	{
		std::cerr << "usage: deser <fn> " << std::endl;
		return 1;
	}

	try
	{
		// Create an archive reader.
		// The archive reader takes a filename as argument.

		std::cout << "INFO: OPENING SERIALIZED ARCHIVE" << std::endl;
		ser::CArchiveReader archive(argv[1]);
		std::cout << "INFO: OPENING SERIALIZED ARCHIVE IS DONE" << std::endl;

		if (!archive)
		{
			std::cerr << "INFO: CAN NOT OPEN SERIALIZED ARCHIVE: " << argv[1] << std::endl;
			return 1;
		}

		ser::CArchiveWriter::COptions l_options(
			ser::CFileHeader::LZ4,
			ser::CFileHeader::DataClassArchive,
			ser::protocol::CABI::makeABI(2148));

		ser::CArchiveWriter arwr;
		bool l_write = false;

		if (argc == 3)
		{
			l_write = true;
			arwr.open(argv[2], l_options);

			if (!arwr)
			{
				std::cerr << "INFO: CAN NOT OPEN ARCHIVE WRITER: " << argv[2] << std::endl;
				return 1;
			}
		}

		// If the archive reader is ok, we read until to the end of file
		ser::CObjGuard l_obj;

		while (!archive.eof())
		{
			if (archive.peek())
			{
				archive >> l_obj;

				if (DBG_FLG)
				{
					ser::cout << l_obj;
				}

				if (l_write)
				{
					arwr << l_obj;
				}
			}
		}
		std::cout << "INFO: EOF" << std::endl;
	}
	catch (ser::CSerException& e)
	{
		std::cout << e << std::endl;
	}
	catch (const char* msg)
	{
		std::cout << "ERROR: MSG EXCEPTION: " << msg << std::endl;
	}
	catch (...)
	{
		std::cout << "ERROR: UNKNOWN EXCEPTION" << std::endl;
	}

	return 0;
}
