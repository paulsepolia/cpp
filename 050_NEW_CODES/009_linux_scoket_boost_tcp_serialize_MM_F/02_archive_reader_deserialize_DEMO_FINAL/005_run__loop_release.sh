#!/bin/bash

loop=1
MAX_LOOPS=10
while [ $loop -le $MAX_LOOPS ]
do
  echo "Loop ---------------------------------------------->>  $loop"
  ./build_release/example arxeio.ar arxeio_meta.ar
  rm arxeio.ar
  mv arxeio_meta.ar arxeio.ar
  loop=$(( loop + 1 ))
done
