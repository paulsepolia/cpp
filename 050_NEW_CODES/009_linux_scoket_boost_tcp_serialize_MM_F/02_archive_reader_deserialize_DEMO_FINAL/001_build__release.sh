#!/bin/bash

# clean-up
rm -rf build_release

# build
mkdir build_release
cd build_release || exit
cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Release ..
cmake --build .
