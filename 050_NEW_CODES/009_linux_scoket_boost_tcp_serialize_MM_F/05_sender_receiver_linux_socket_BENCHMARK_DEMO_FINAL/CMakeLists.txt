cmake_minimum_required(VERSION 3.13)

project(sender_receiver_linux_socket_example)

set(CMAKE_CXX_STANDARD 17)

#========#
# SENDER #
#========#

set(TARGET_sender sender_exe)

add_executable(${TARGET_sender}
        sender.cpp
        )

#==========#
# RECEIVER #
#==========#

set(TARGET_receiver receiver_exe)

add_executable(${TARGET_receiver}
        receiver.cpp
        )

