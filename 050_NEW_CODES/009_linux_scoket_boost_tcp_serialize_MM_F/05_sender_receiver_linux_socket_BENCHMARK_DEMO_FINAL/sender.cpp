#include <unistd.h>
#include <cstring>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <iostream>
#include <thread>

const size_t NUM_MSGS = 1000000000;
const size_t DIM_MSG = 8 * 1000000;

int main(int argc, char* argv[])
{
	struct sockaddr_in serv_addr{};
	struct hostent* server;

	if (argc < 3)
	{
		std::cout << "INFO: SENDER: USAGE: " << argv[0] << " hostname port" << std::endl;
		return 0;
	}

	const auto portno = std::stoi(argv[2]);
	const auto sockfd = socket(AF_INET, SOCK_STREAM, 0);

	if (sockfd < 0)
	{
		std::cout << "ERROR: SENDER: CANNOT OPEN SOCKET" << std::endl;
		std::terminate();
	}

	server = gethostbyname(argv[1]);

	if (server == nullptr)
	{
		std::cout << "ERROR: SENDER: NO SUCH HOST" << std::endl;
		std::terminate();
	}

	// void bzero(void *s, size_t n);
	//      The bzero() function erases the data in the n bytes of the memory
	//      starting at the location pointed to by s, by writing zeros (bytes
	//      containing '\0') to that area.

	bzero((char*)&serv_addr, sizeof(serv_addr));

	serv_addr.sin_family = AF_INET;

	// The bcopy() function copies n bytes from src to dest.
	// The result is correct, even when both areas overlap.

	bcopy(server->h_addr, (char*)&serv_addr.sin_addr.s_addr, static_cast<uint32_t>(server->h_length));

	serv_addr.sin_port = htons(static_cast<uint16_t>(portno));

	const auto ret1 = connect(sockfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr));

	if (ret1 < 0)
	{
		std::cout << "ERROR: SENDER: ERROR WHILE CONNECTING" << std::endl;
		std::terminate();
	}

	std::shared_ptr<char> buffer(new char[DIM_MSG], std::default_delete<char[]>());

	// Build the message only once

	bzero(buffer.get(), DIM_MSG);

	for (size_t ii = 0; ii < DIM_MSG; ii++)
	{
		buffer.get()[ii] = 'a';
	}

	buffer.get()[DIM_MSG - 1] = '1';

	// send many times the same message
	std::cout << "INFO: SENDER: SENDING MESSAGES..." << std::endl;

	for (size_t kk = 0; kk < NUM_MSGS; kk++)
	{
		const auto ret2 = write(sockfd, buffer.get(), DIM_MSG);
		const auto error_code = errno;

		if (ret2 < 0)
		{
			std::cout << "ERROR: SENDER: ERROR WHILE WRITING TO SOCKET: " << error_code << std::endl;
			std::terminate();
		}
	}

	close(sockfd);
}

