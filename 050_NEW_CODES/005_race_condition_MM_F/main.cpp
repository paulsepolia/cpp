#include <iostream>
#include <thread>
#include <vector>

const size_t NUM_THREADS = 100;
size_t x = 0;

void AddNumber()
{
    x++;
}

int main()
{
    std::vector<std::thread> v;
    for(size_t i = 0; i < NUM_THREADS; i++)
    {
        v.emplace_back(std::thread(AddNumber));
    }

    for(auto &el: v)
    {
        el.join();
    }

    std::cout << "x should be = " << NUM_THREADS << " but is: " << x << std::endl;
    
    return 0;
}
