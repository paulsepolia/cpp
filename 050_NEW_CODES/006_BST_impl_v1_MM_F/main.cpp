#include <iostream>
#include <iomanip>
#include <vector>
#include <random>
#include <algorithm>
#include <chrono>
#include <unordered_set>

class BenchmarkTimer
{
public:

    explicit BenchmarkTimer() :
            res(0.0),
            t1{std::chrono::steady_clock::now()},
            t2{std::chrono::steady_clock::now()},
            t_prev{std::chrono::steady_clock::now()}
    {};

    void PrintTime() const
    {
        const auto t_loc{std::chrono::steady_clock::now()};
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t_loc - t_prev).count()};
        t_prev = t_loc;
        std::cout << "--> time used (secs) = " << total_time << std::endl;
    }

    void ResetTimer()
    {
        const auto t_loc{std::chrono::steady_clock::now()};
        t_prev = t_loc;
        t1 = t_loc;
        t2 = t_loc;
    }

    ~BenchmarkTimer()
    {
        t2 = std::chrono::steady_clock::now();
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count()};
        std::cout << "--> res = " << res << std::endl;
        std::cout << "--> total time used (secs) = " << total_time << std::endl;
    }

    void SetRes(const double &res_loc)
    {
        res = res_loc;
    }

private:

    double res = 0.0;
    decltype(std::chrono::steady_clock::now()) t1{};
    decltype(std::chrono::steady_clock::now()) t2{};
    mutable decltype(std::chrono::steady_clock::now()) t_prev{};
};

class BSTNode
{
public:
    double data = 0.0;
    BSTNode *left = nullptr;
    BSTNode *right = nullptr;
};

BSTNode *MakeBSTNode(double data)
{
    auto *res = new BSTNode();
    res->data = data;
    res->left = nullptr;
    res->right = nullptr;
    return res;
}

BSTNode *Insert(BSTNode *root, double data)
{
    if (root == nullptr)
    {
        root = MakeBSTNode(data);
    }
    else if (data <= root->data)
    {
        root->left = Insert(root->left, data);
    }
    else if (data > root->data)
    {
        root->right = Insert(root->right, data);
    }

    return root;
}

bool Search(BSTNode *root, double data)
{
    if (root == nullptr)
    {
        return false;
    }
    else if (data == root->data)
    {
        return true;
    }
    else if (data <= root->data)
    {
        return Search(root->left, data);
    }
    else
    {
        return Search(root->right, data);
    }
}

int main()
{
    std::cout << std::boolalpha;
    const size_t TOT_ELEMS = 100'000UL;
    const size_t ELEMS_TO_FIND = 2UL;
    BSTNode *root = nullptr;

    {
        std::cout << "--> Insert elements in the BST tree --> Unbalanced tree" << std::endl;

        BenchmarkTimer ot;

        for (size_t i = 0; i < TOT_ELEMS; i++)
        {
            root = Insert(root, (double) i);
        }

        ot.PrintTime();
        ot.ResetTimer();

        for (size_t i = TOT_ELEMS - ELEMS_TO_FIND; i < TOT_ELEMS; i++)
        {
            std::cout << "--> Search for " << i << " --> " << Search(root, i) << std::endl;
        }

        ot.PrintTime();
    }

    {
        std::vector<size_t> values;

        for (size_t i = 0; i < TOT_ELEMS; i++)
        {
            values.push_back(i);
        }

        auto rd = std::random_device{};
        auto rng = std::default_random_engine{rd()};
        std::shuffle(std::begin(values), std::end(values), rng);

        std::cout << "--> Insert elements in the BST tree --> Better Balanced tree" << std::endl;

        BenchmarkTimer ot;

        for (const auto &el: values)
        {
            root = Insert(root, (double) el);
        }

        ot.PrintTime();
        ot.ResetTimer();

        for (size_t i = TOT_ELEMS - ELEMS_TO_FIND; i < TOT_ELEMS; i++)
        {
            std::cout << "--> Search for " << i << " --> " << Search(root, i) << std::endl;
        }

        ot.PrintTime();
    }

    {
        std::vector<size_t> values;

        for (size_t i = 0; i < TOT_ELEMS; i++)
        {
            values.push_back(i);
        }

        auto rd = std::random_device{};
        auto rng = std::default_random_engine{rd()};
        std::shuffle(std::begin(values), std::end(values), rng);

        std::cout << "--> Insert elements in the unordered set" << std::endl;

        BenchmarkTimer ot;

        std::unordered_set<double> uns;

        for (const auto &el: values)
        {
            uns.insert((double) el);
        }

        ot.PrintTime();
        ot.ResetTimer();

        for (size_t i = TOT_ELEMS - ELEMS_TO_FIND; i < TOT_ELEMS; i++)
        {
            std::cout << "--> Search for " << i << " --> "
                      << (uns.find((double) i) != uns.end()) << std::endl;
        }

        ot.PrintTime();
    }

    return 0;
}
