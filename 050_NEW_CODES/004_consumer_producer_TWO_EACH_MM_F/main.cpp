#include <iostream>
#include <deque>
#include <mutex>
#include <condition_variable>
#include <thread>
#include <chrono>
#include <iomanip>

const size_t MAX_SIZE_DATA_BUFFER = 1'000'000UL;

class Data
{
public:
    std::deque<size_t> buffer{};
};

Data someData{};

size_t record = 0;
std::mutex mtx{};
std::condition_variable cv{};
auto timePrevious = std::chrono::steady_clock::now();
const double timeRes = 5;

void ProduceData()
{
    while (true)
    {
        std::unique_lock<std::mutex> lck(mtx);
        cv.wait(lck,
                [&]()
                { return someData.buffer.size() < MAX_SIZE_DATA_BUFFER; });
        record++;
        someData.buffer.push_back(record);
        lck.unlock();
        cv.notify_one();
    }
}

void ConsumeData()
{
    while (true)
    {
        std::unique_lock<std::mutex> lck(mtx);
        cv.wait(lck,
                [&]()
                { return someData.buffer.size() > 0; });
        const auto data = someData.buffer.front();
        someData.buffer.pop_front();
        const auto timeNow = std::chrono::steady_clock::now();
        std::chrono::duration<double> timeDiff = timeNow - timePrevious;
        if (timeDiff.count() >= timeRes)
        {
            timePrevious = timeNow;
            std::cout << " Got : " << std::setw(15)
                      << data << " and size is: " << std::setw(15)
                      << someData.buffer.size() << std::endl;
        }
        lck.unlock();
        cv.notify_one();
    }
}

int main()
{
    std::thread t1(ConsumeData);
    std::thread t2(ConsumeData);
    std::thread t3(ProduceData);
    std::thread t4(ProduceData);

    t1.join();
    t2.join();
    t3.join();
    t4.join();

    return 0;
}
