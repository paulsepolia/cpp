#include <iostream>
#include <memory>
#include <mutex>
#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <thread>

std::mutex mtx;
const size_t NUM_THREADS = 10;
const size_t NUM_JOBS = 100;

void worker_thread(const std::shared_ptr<boost::asio::io_service>& io_svc, const uint32_t& counter)
{
	io_svc->run();
}

void print_function(const uint32_t& index)
{
	std::this_thread::sleep_for(std::chrono::seconds(1));
	std::cout << "-->> thread id = " << std::this_thread::get_id() << " -->> " << index << std::endl;
}

int main()
{
	// create a shared pointer to the io_service object
	std::shared_ptr<boost::asio::io_service> io_svc(new boost::asio::io_service);

	// create a shared pointer to the io_service::work object
	std::shared_ptr<boost::asio::io_service::work> worker(new boost::asio::io_service::work(*io_svc));
	boost::asio::io_service::strand strand(*io_svc);

	// lock here and inform the user
	mtx.lock();
	std::cout << "The program will exit once all the work has finished" << std::endl;
	mtx.unlock();

	// create a group of boost threads
	boost::thread_group threads{};

	for (size_t i = 1; i <= NUM_THREADS; i++)
	{
		std::cout << "-->> threads.create_thread -->> " << i << std::endl;
		threads.create_thread([io_svc, i]
		{ return worker_thread(io_svc, i); });
	}

	// sleep for a while
	std::cout << "-->> sleeping here before execution starts..." << std::endl;
	std::this_thread::sleep_for(std::chrono::seconds(10));

	// post jobs here in order AND THEY ARE EXECUTED IN ORDER
	for (size_t i = 1; i <= NUM_JOBS; i++)
	{
		strand.post([i]
		{ return print_function(i); });
	}

	worker.reset();
	threads.join_all();
}
