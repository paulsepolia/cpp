cmake_minimum_required(VERSION 3.23)
project(CRTP_VS_VIRTUAL)
set(CMAKE_CXX_STANDARD 17)

add_executable(x_CRTP_VS_VIRTUAL main.cpp)
