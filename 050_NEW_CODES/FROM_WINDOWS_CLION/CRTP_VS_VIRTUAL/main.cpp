#include <iostream>
#include <thread>
#include <cmath>
#include <iomanip>

const auto DO_MAX = (size_t)(std::pow(10.0, 10.0));
const auto DO_MAX_OUTER = (size_t)(std::pow(10.0, 2.0));

class BenchmarkTimer final
{
 public:

	explicit BenchmarkTimer() :
		m_t_now(std::chrono::steady_clock::now()),
		m_t_prev(std::chrono::steady_clock::now())
	{
	}

	void PrintTime() const
	{
		const auto t_loc = std::chrono::steady_clock::now();
		const auto total_time = std::chrono::duration_cast<std::chrono::duration<double>>(t_loc - m_t_prev).count();
		m_t_prev = t_loc;
		std::cout.precision(10);
		std::cout << std::fixed;
		std::cout << "-->> time used up to now (since last reset) (secs) = " << total_time << std::endl;
	}

	void ResetTimer()
	{
		const auto t_loc = std::chrono::steady_clock::now();
		m_t_prev = t_loc;
		m_t_now = t_loc;
	}

	~BenchmarkTimer()
	{
		m_t_prev = m_t_now;
		m_t_now = std::chrono::steady_clock::now();
		const auto total_time = std::chrono::duration_cast<std::chrono::duration<double>>(m_t_now - m_t_prev).count();
		std::cout.precision(10);
		std::cout << std::fixed;
		std::cout << "-->> total time used until exit of the scope (since last reset) (secs) = " << total_time
				  << std::endl;
	}

 private:

	decltype(std::chrono::steady_clock::now()) m_t_now;
	mutable decltype(std::chrono::steady_clock::now()) m_t_prev;
};

template<typename Derived>
class A
{
 protected:

	size_t m_i1;
	size_t m_i2;
	size_t m_i3;

 public:

	A() : m_i1(0), m_i2(0), m_i3(0)
	{
	}

	~A() = default;

	void set_values_base(size_t i1, size_t i2, size_t i3)
	{
		static_cast<Derived*>(this)->set_values(i1, i2, i3);
	}

	[[nodiscard]] size_t get_i1() const
	{
		return m_i1;
	}

	[[nodiscard]] size_t get_i2() const
	{
		return m_i2;
	}

	[[nodiscard]] size_t get_i3() const
	{
		return m_i3;
	}
};

class B final : public A<B>
{
 public:

	void set_values(size_t i1, size_t i2, size_t i3)
	{
		m_i1 += i1;
		m_i2 += i2;
		m_i3 += i3;
	}

 public:

	~B() = default;
};

class A2
{
 protected:

	size_t m_i1;
	size_t m_i2;
	size_t m_i3;

 public:

	A2() : m_i1(0), m_i2(0), m_i3(0)
	{
	}

	virtual ~A2() = default;

	virtual void set_values(size_t i1, size_t i2, size_t i3)
	{
		m_i1 += i1;
		m_i2 += i2;
		m_i3 += i3;
	}

	void set_values_base(size_t i1, size_t i2, size_t i3)
	{
		set_values(i1, i2, i3);
	}

	[[nodiscard]] size_t get_i1() const
	{
		return m_i1;
	}

	[[nodiscard]] size_t get_i2() const
	{
		return m_i2;
	}

	[[nodiscard]] size_t get_i3() const
	{
		return m_i3;
	}
};

class B2 final : public A2
{
 public:

	void set_values(size_t i1, size_t i2, size_t i3) final
	{
		m_i1 += i1;
		m_i2 += i2;
		m_i3 += i3;
	}

 public:

	~B2() final = default;
};

int main()
{
	std::cout << std::fixed;
	std::cout << std::setprecision(5);

	for (size_t kk = 0; kk <= DO_MAX_OUTER; kk++)
	{
		std::cout << "------------------------------------------------------------>> kk = " << kk << std::endl;

		size_t v_crpt;
		size_t v_direct;
		size_t v_base;
		size_t v_derived;

		{
			std::cout << "-->> Case #1 -->> CRTP" << std::endl;

			BenchmarkTimer ot;

			A<B>* p = new B();

			for (size_t i = 0; i < DO_MAX; i++)
			{
				p->set_values_base(i, i + 1, i + 2);
			}

			// check #1
			v_crpt = p->get_i1();

			// check #2
			const auto DIFF1 = p->get_i2() - p->get_i1();
			if (DIFF1 != DO_MAX)
			{
				std::cout << "ERROR: " << DIFF1 << " != " << DO_MAX << std::endl;
				std::terminate();
			}

			// check #3
			const auto DIFF2 = p->get_i3() - p->get_i2();
			if (DIFF2 != DO_MAX)
			{
				std::cout << "ERROR: " << DIFF2 << " != " << DO_MAX << std::endl;
				std::terminate();
			}
			delete p;
		}

		{
			std::cout << "-->> Case #2 -->> DIRECT" << std::endl;

			BenchmarkTimer ot;

			B* p = new B();

			for (size_t i = 0; i < DO_MAX; i++)
			{
				p->set_values(i, i + 1, i + 2);
			}

			// check #1
			v_direct = p->get_i1();
			if (v_crpt != v_direct)
			{
				std::cout << "ERROR: " << v_crpt << " != " << v_direct << std::endl;
				std::terminate();
			}

			// check #2
			const auto DIFF1 = p->get_i2() - p->get_i1();
			if (DIFF1 != DO_MAX)
			{
				std::cout << "ERROR: " << DIFF1 << " != " << DO_MAX << std::endl;
				std::terminate();
			}

			// check #3
			const auto DIFF2 = p->get_i3() - p->get_i2();
			if (DIFF2 != DO_MAX)
			{
				std::cout << "ERROR: " << DIFF2 << " != " << DO_MAX << std::endl;
				std::terminate();
			}

			delete p;
		}

		{
			std::cout << "-->> Case #3 -->> VIRTUAL BASE" << std::endl;
			BenchmarkTimer ot;
			auto p = new A2();

			for (size_t i = 0; i < DO_MAX; i++)
			{
				p->set_values_base(i, i + 1, i + 2);
			}

			// check #1
			v_base = p->get_i1();

			// check #2
			const auto DIFF1 = p->get_i2() - p->get_i1();
			if (DIFF1 != DO_MAX)
			{
				std::cout << "ERROR: " << DIFF1 << " != " << DO_MAX << std::endl;
				std::terminate();
			}

			// check #3
			const auto DIFF2 = p->get_i3() - p->get_i2();
			if (DIFF2 != DO_MAX)
			{
				std::cout << "ERROR: " << DIFF2 << " != " << DO_MAX << std::endl;
				std::terminate();
			}

			delete p;
		}

		{
			std::cout << "-->> Case #4 -->> VIRTUAL DERIVED" << std::endl;
			BenchmarkTimer ot;
			auto p = new B2();
			for (size_t i = 0; i < DO_MAX; i++)
			{
				p->set_values_base(i, i + 1, i + 2);
			}

			// check #1
			v_derived = p->get_i1();
			if (v_base != v_derived)
			{
				std::cout << "ERROR: " << v_crpt << " != " << v_direct << std::endl;
				std::terminate();
			}

			// check #2
			const auto DIFF1 = p->get_i2() - p->get_i1();
			if (DIFF1 != DO_MAX)
			{
				std::cout << "ERROR: " << DIFF1 << " != " << DO_MAX << std::endl;
				std::terminate();
			}

			// check #3
			const auto DIFF2 = p->get_i3() - p->get_i2();
			if (DIFF2 != DO_MAX)
			{
				std::cout << "ERROR: " << DIFF2 << " != " << DO_MAX << std::endl;
				std::terminate();
			}
			delete p;
		}
	}
}