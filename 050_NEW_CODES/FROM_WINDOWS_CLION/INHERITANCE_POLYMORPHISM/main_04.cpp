#include <iostream>

class Base
{
private:

    int m1 = 0;

public:
    Base() = default;

    ~Base() = default;

    virtual void base_funA()
    {
        m1++;
        std::cout << "-->> Base class -->> base_funA() -->> " << m1 << std::endl;
    }

    virtual void base_funB()
    {
        m1++;
        std::cout << "-->> Base class -->> base_funB() -->> " << m1 << std::endl;
    }
};

class DerivedA : virtual public Base
{
private:
    int m2 = 0;
public:
    void base_funA() override
    {
        m2++;
        std::cout << "-->> DerivedA class -->> DerivedA::base_funA() -->> " << m2 << std::endl;
    }
};

class DerivedB : virtual public Base
{
private:
    int m3 = 0;
public:
    void base_funB() override
    {
        m3++;
        std::cout << "-->> DerivedB class -->> DerivedB::base_funB() -->> " << m3 << std::endl;
    }
};

class FinalA : public DerivedA, public DerivedB
{
private:
    int m_final_A = 0;
public:
    void base_funA() override
    {
        m_final_A++;
        std::cout << "-->> Final class -->> Final::base_funA() -->> " << m_final_A << std::endl;
    }

    void base_funB() override
    {
        m_final_A++;
        std::cout << "-->> Final class -->> Final::base_funB() -->> " << m_final_A << std::endl;
    }
};

class C1 : public Base
{
    int m = 0;
public:
    void base_funA() override
    {
        m++;
        std::cout << "-->> C1 class -->> base_funA() -->> " << m << std::endl;
    }

    void base_funB() override
    {
        m++;
        std::cout << "-->> C1 class -->> base_funB() -->> " << m << std::endl;
    }
};

class C2 : public Base
{
    int m = 0;
public:
    void base_funA() override
    {
        m++;
        std::cout << "-->> C2 class -->> base_funA() -->> " << m << std::endl;
    }

    void base_funB() override
    {
        m++;
        std::cout << "-->> C2 class -->> base_funB() -->> " << m << std::endl;
    }
};


class C3 : public Base
{
    int m = 0;
public:
    void base_funA() override
    {
        m++;
        std::cout << "-->> C3 class -->> base_funA() -->> " << m << std::endl;
    }

    void base_funB() override
    {
        m++;
        std::cout << "-->> C3 class -->> base_funB() -->> " << m << std::endl;
    }
};

int main()
{
    {
        std::cout << "#########################################################" << std::endl;
        std::cout << "Example#1" << std::endl;
        auto *fa = new FinalA();
        {
            std::cout << "----------------------->> 1" << std::endl;
            fa->base_funA();
            fa->base_funB();
        }
        {
            std::cout << "----------------------->> 2" << std::endl;
            auto o = dynamic_cast<Base *>(fa);
            o->base_funA();
            o->base_funB();
        }
        {
            std::cout << "----------------------->> 3" << std::endl;
            auto o = dynamic_cast<DerivedA *>(fa);
            o->base_funA();
            o->base_funB();
        }
        {
            std::cout << "----------------------->> 4" << std::endl;
            auto o = dynamic_cast<DerivedB *>(fa);
            o->base_funA();
            o->base_funB();
        }
    }
    {
        std::cout << "#########################################################" << std::endl;
        std::cout << "Example#2" << std::endl;
        Base *o1 = new C1();
        Base *o2 = new C2();
        Base *o3 = new C3();
        {
            std::cout << "=====================================================>> 1" << std::endl;
            dynamic_cast<C1 *>(o1)->base_funA();
            dynamic_cast<C1 *>(o1)->base_funB();

            if (dynamic_cast<C2 *>(o1) != nullptr)
            {
                std::cout << "=================================================>> 2" << std::endl;
                dynamic_cast<C2 *>(o1)->base_funA();
                dynamic_cast<C2 *>(o1)->base_funB();
            }
            if (dynamic_cast<C3 *>(o1) != nullptr)
            {
                std::cout << "=================================================>> 3" << std::endl;
                dynamic_cast<C3 *>(o1)->base_funA();
                dynamic_cast<C3 *>(o1)->base_funB();
            }
        }
        {
            std::cout << "=====================================================>> 4" << std::endl;
            dynamic_cast<C2 *>(o2)->base_funA();
            dynamic_cast<C2 *>(o2)->base_funB();
            if (dynamic_cast<C1 *>(o2) != nullptr)
            {
                std::cout << "=================================================>> 5" << std::endl;
                dynamic_cast<C1 *>(o2)->base_funA();
                dynamic_cast<C1 *>(o2)->base_funB();
            }
            if (dynamic_cast<C3 *>(o2) != nullptr)
            {
                std::cout << "=================================================>> 6" << std::endl;
                dynamic_cast<C3 *>(o2)->base_funA();
                dynamic_cast<C3 *>(o2)->base_funB();
            }
        }
        {
            std::cout << "=====================================================>> 7" << std::endl;
            dynamic_cast<C3 *>(o3)->base_funA();
            dynamic_cast<C3 *>(o3)->base_funB();
            if (dynamic_cast<C1 *>(o3) != nullptr)
            {
                std::cout << "=================================================>> 8" << std::endl;
                dynamic_cast<C1 *>(o3)->base_funA();
                dynamic_cast<C1 *>(o3)->base_funB();
            }
            if (dynamic_cast<C2 *>(o3) != nullptr)
            {
                std::cout << "=================================================>> 9" << std::endl;
                dynamic_cast<C2 *>(o3)->base_funA();
                dynamic_cast<C2 *>(o3)->base_funB();
            }
        }
    }
}
