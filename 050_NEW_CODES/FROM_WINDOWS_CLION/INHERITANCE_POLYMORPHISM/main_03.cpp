#include <iostream>

class A
{
private:

    size_t address_obj = (size_t) this;

public:

    [[nodiscard]] size_t get_address() const
    {
        return address_obj;
    }

    [[nodiscard]] const A *get_address_ptr() const
    {
        return this;
    }

    void fA1()
    {
        std::cout << "class A -->> this        -->> " << this << std::endl;
        std::cout << "class A -->> address_obj -->> " << std::hex << address_obj << std::endl;
    }

    virtual ~A()
    {
        std::cout << "~A() -->> " << this << std::endl;
    }
};

constexpr size_t DIM1 = 100;
constexpr size_t DIM2 = 200;

class B
{
public:

    [[maybe_unused]] double data1[DIM1] = {};
    [[maybe_unused]] double *data2 = new double[DIM2];
    [[maybe_unused]] double *data3 = nullptr;
};

class C1
{
};

class C2
{
private:
};

class C3
{
public:
};

class C4
{
public:
    virtual ~C4() = default;
};

class C5
{
public:
    explicit C5(int)
    {}

private:
    C5() = default;

public:
    virtual ~C5() = default;
};

class C6 final : public C5
{
public:
    C6() : C5(10)
    {}

    ~C6() final = default;
};

int main()
{
    {
        std::cout << "#########################################################" << std::endl;
        std::cout << "Example#1" << std::endl;
        A a;
        a.fA1();
        std::cout << "&a = " << &a << std::endl;
        if ((size_t) (&a) != a.get_address())
        {
            std::cout << "ERROR: #1" << std::endl;
        }
        if (&a != a.get_address_ptr())
        {
            std::cout << "ERROR: #2" << std::endl;
        }
        if (&a != a.get_address_ptr())
        {
            std::cout << "ERROR: #3" << std::endl;
        }
    }
    {
        std::cout << "#########################################################" << std::endl;
        std::cout << "Example#2" << std::endl;
        std::cout << "pgg -->> sizeof(B) = " << std::dec << sizeof(B) << std::endl;
        B obj_b;
        std::cout << "pgg -->> sizeof(obj_b) = " << std::dec << sizeof(obj_b) << std::endl;
        obj_b.data3 = new double[100];
        std::cout << "pgg -->> sizeof(B) = " << std::dec << sizeof(B) << std::endl;
        std::cout << "pgg -->> sizeof(obj_b) = " << std::dec << sizeof(obj_b) << std::endl;
    }
    {
        std::cout << "#########################################################" << std::endl;
        std::cout << "Example#3" << std::endl;
        [[maybe_unused]] C1 c1;
        std::cout << "-->> sizeof(C1) -->> " << sizeof(C1) << std::endl;
        std::cout << "-->> sizeof(c1) -->> " << sizeof(c1) << std::endl;
        [[maybe_unused]] C2 c2;
        std::cout << "-->> sizeof(C2) -->> " << sizeof(C2) << std::endl;
        std::cout << "-->> sizeof(c2) -->> " << sizeof(c2) << std::endl;
        [[maybe_unused]] C3 c3;
        std::cout << "-->> sizeof(C3) -->> " << sizeof(C3) << std::endl;
        std::cout << "-->> sizeof(c3) -->> " << sizeof(c3) << std::endl;
        [[maybe_unused]] C4 c4;
        std::cout << "-->> sizeof(C4) -->> " << sizeof(C4) << std::endl;
        std::cout << "-->> sizeof(c4) -->> " << sizeof(c4) << std::endl;
    }
    {
        std::cout << "#########################################################" << std::endl;
        std::cout << "Example#4" << std::endl;
        [[maybe_unused]] C1 c1;
        std::cout << "-->> sizeof(C1) -->> " << sizeof(C1) << std::endl;
        std::cout << "-->> sizeof(c1) -->> " << sizeof(c1) << std::endl;
        [[maybe_unused]] C2 c2;
        std::cout << "-->> sizeof(C2) -->> " << sizeof(C2) << std::endl;
        std::cout << "-->> sizeof(c2) -->> " << sizeof(c2) << std::endl;
        [[maybe_unused]] C3 c3;
        std::cout << "-->> sizeof(C3) -->> " << sizeof(C3) << std::endl;
        std::cout << "-->> sizeof(c3) -->> " << sizeof(c3) << std::endl;
        [[maybe_unused]] C4 c4;
        std::cout << "-->> sizeof(C4) -->> " << sizeof(C4) << std::endl;
        std::cout << "-->> sizeof(c4) -->> " << sizeof(c4) << std::endl;
    }
    {
        std::cout << "#########################################################" << std::endl;
        std::cout << "Example#5" << std::endl;
        [[maybe_unused]] C5 *c5;
        C6 c6;
    }
}
