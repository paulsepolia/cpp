#include <iostream>

class A
{
public:
    int public1 = 1;
    int public2 = 2;
    int public3 = 3;
protected:
    int protected1 = 4;
    int protected2 = 5;
    int protected3 = 6;
};

class B : public A
{
public:
    int public1 = A::public1 + A::protected1;
    int public2 = A::public2 + A::protected2;
    int public3 = A::public3 + A::protected3;
};

class C : protected A
{
public:
    int public1 = 20 + A::public1 + A::protected1;
    int public2 = 21 + A::public2 + A::protected2;
    int public3 = 22 + A::public3 + A::protected3;
};

class D : private A
{
public:
    int public1 = 30 + A::public1 + A::protected1;
    int public2 = 31 + A::public2 + A::protected2;
    int public3 = 32 + A::public3 + A::protected3;
};

int main()
{
    {
        std::cout << "#########################################################" << std::endl;
        std::cout << "Example#1" << std::endl;
        A a;
        std::cout << "a.public1 -->> " << a.public1 << std::endl;
        std::cout << "a.public2 -->> " << a.public2 << std::endl;
        std::cout << "a.public3 -->> " << a.public3 << std::endl;
    }
    {
        std::cout << "#########################################################" << std::endl;
        std::cout << "Example#2" << std::endl;
        B b;
        std::cout << "b.public1 -->> " << b.public1 << std::endl;
        std::cout << "b.public2 -->> " << b.public2 << std::endl;
        std::cout << "b.public3 -->> " << b.public3 << std::endl;
    }
    {
        std::cout << "#########################################################" << std::endl;
        std::cout << "Example#3" << std::endl;
        C c;
        std::cout << "c.public1 -->> " << c.public1 << std::endl;
        std::cout << "c.public2 -->> " << c.public2 << std::endl;
        std::cout << "c.public3 -->> " << c.public3 << std::endl;
    }
    {
        std::cout << "#########################################################" << std::endl;
        std::cout << "Example#4" << std::endl;
        D d;
        std::cout << "d.public1 -->> " << d.public1 << std::endl;
        std::cout << "d.public2 -->> " << d.public2 << std::endl;
        std::cout << "d.public3 -->> " << d.public3 << std::endl;
    }
}
