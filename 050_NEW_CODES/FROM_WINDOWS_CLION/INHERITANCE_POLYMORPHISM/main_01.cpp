#include <iostream>

class A
{
public:
    int public1 = 1;
    int public2 = 2;
    int public3 = 3;
protected:
    int protected1 = 1;
    int protected2 = 2;
    int protected3 = 3;
};

class B : public A
{
public:
    int public1 = 10 + protected1;
    int public2 = 11 + protected2;
    int public3 = 12 + protected3;
};

class C : protected A
{
public:
    int public1 = 20 + protected1;
    int public2 = 21 + protected2;
    int public3 = 22 + protected3;
};

class D : private A
{
public:
    int public1 = 30 + protected1;
    int public2 = 31 + protected2;
    int public3 = 32 + protected3;
};

int main()
{
    {
        std::cout << "#########################################################" << std::endl;
        std::cout << "Example#1" << std::endl;
        A a;
        std::cout << "a.public1 -->> " << a.public1 << std::endl;
        std::cout << "a.public2 -->> " << a.public2 << std::endl;
        std::cout << "a.public3 -->> " << a.public3 << std::endl;
    }
    {
        std::cout << "#########################################################" << std::endl;
        std::cout << "Example#2" << std::endl;
        B b;
        std::cout << "b.public1 -->> " << b.public1 << std::endl;
        std::cout << "b.public2 -->> " << b.public2 << std::endl;
        std::cout << "b.public3 -->> " << b.public3 << std::endl;
    }
    {
        std::cout << "#########################################################" << std::endl;
        std::cout << "Example#3" << std::endl;
        C c;
        std::cout << "c.public1 -->> " << c.public1 << std::endl;
        std::cout << "c.public2 -->> " << c.public2 << std::endl;
        std::cout << "c.public3 -->> " << c.public3 << std::endl;
    }
    {
        std::cout << "#########################################################" << std::endl;
        std::cout << "Example#4" << std::endl;
        D d;
        std::cout << "d.public1 -->> " << d.public1 << std::endl;
        std::cout << "d.public2 -->> " << d.public2 << std::endl;
        std::cout << "d.public3 -->> " << d.public3 << std::endl;
    }
}
