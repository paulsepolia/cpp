#include <iostream>
#include <iomanip>
#include <mutex>
#include <vector>

class SingletonClassic
{
private:
    SingletonClassic() = default;

    int m_i = 0;
    double m_d = 0;
    static std::mutex m_mtx;
    static SingletonClassic *m_instance;
public:

    static SingletonClassic *get_instance_ptr()
    {
        std::unique_lock<std::mutex> lck(m_mtx);
        if (m_instance == nullptr)
        {
            m_instance = new SingletonClassic();
        }
        return m_instance;
    }

    void set_i(int m)
    {
        std::unique_lock<std::mutex> lck(m_mtx);
        m_i = m;
    }

    void set_d(double m)
    {
        std::unique_lock<std::mutex> lck(m_mtx);
        m_d = m;
    }

    [[nodiscard]] int get_i() const
    {
        std::unique_lock<std::mutex> lck(m_mtx);
        return m_i;
    }

    [[nodiscard]] double get_d() const
    {
        std::unique_lock<std::mutex> lck(m_mtx);
        return m_d;
    }

public:

    SingletonClassic(const SingletonClassic &other)
    {
        m_d = other.m_d;
        m_i = other.m_i;
    }

    SingletonClassic(SingletonClassic &&other) noexcept
    {
        m_d = other.m_d;
        m_i = other.m_i;
    }

    SingletonClassic &operator=(const SingletonClassic &) = delete;

    SingletonClassic &operator=(SingletonClassic &&) noexcept = delete;
};

std::mutex SingletonClassic::m_mtx{};
SingletonClassic *SingletonClassic::m_instance = nullptr;

class SingletonStatic
{
private:
    SingletonStatic() = default;

    int m_i = 0;
    double m_d = 0;
    mutable std::mutex m_mtx;
public:

    static SingletonStatic *get_instance_ptr()
    {
        static SingletonStatic instance;
        return &instance;
    }

    void set_i(int m)
    {
        std::unique_lock<std::mutex> lck(m_mtx);
        m_i = m;
    }

    void set_d(double m)
    {
        std::unique_lock<std::mutex> lck(m_mtx);
        m_d = m;
    }

    [[nodiscard]] int get_i() const
    {
        std::unique_lock<std::mutex> lck(m_mtx);
        return m_i;
    }

    [[nodiscard]] double get_d() const
    {
        std::unique_lock<std::mutex> lck(m_mtx);
        return m_d;
    }

public:

    SingletonStatic(const SingletonStatic &other)
    {
        m_d = other.m_d;
        m_i = other.m_i;
    }

    SingletonStatic(SingletonStatic &&other) noexcept: m_mtx()
    {
        m_d = other.m_d;
        m_i = other.m_i;
    }

    SingletonStatic &operator=(const SingletonStatic &) = delete;

    SingletonStatic &operator=(SingletonStatic &&) noexcept = delete;
};

class Dualton
{
private:
    Dualton() = default;

    int m_i = 0;
    double m_d = 0;
    mutable std::mutex m_mtx;
public:

    static Dualton *get_instance_ptr()
    {
        static Dualton instance;
        return &instance;
    }

    static Dualton &get_instance_ref()
    {
        static Dualton instance;
        return instance;
    }

    void set_i(int m)
    {
        std::unique_lock<std::mutex> lck(m_mtx);
        m_i = m;
    }

    void set_d(double m)
    {
        std::unique_lock<std::mutex> lck(m_mtx);
        m_d = m;
    }

    [[nodiscard]] int get_i() const
    {
        std::unique_lock<std::mutex> lck(m_mtx);
        return m_i;
    }

    [[nodiscard]] double get_d() const
    {
        std::unique_lock<std::mutex> lck(m_mtx);
        return m_d;
    }

public:

    Dualton(const Dualton &) = delete;

    Dualton(Dualton &&) noexcept = delete;

    Dualton &operator=(const Dualton &) = delete;

    Dualton &operator=(Dualton &&) noexcept = delete;
};

constexpr int v1 = 1234;
constexpr double v2 = 1234.9876543210;
constexpr int v3 = 54321;
constexpr double v4 = 54321.9876543210;
constexpr size_t DIM = 100;

int main()
{
    std::cout << std::setprecision(20);

    {
        std::cout << "#########################################################" << std::endl;
        std::cout << "-->> Example#1" << std::endl;
        auto o_ptr = Dualton::get_instance_ptr();
        std::cout << "-->> o_ptr = " << o_ptr << std::endl;
        o_ptr->set_i(v1);
        std::cout << "-->> o_ptr->get_i() = " << o_ptr->get_i() << std::endl;
        o_ptr->set_d(v2);
        std::cout << "-->> o_ptr->get_i() = " << o_ptr->get_d() << std::endl;
        auto &o_ref = Dualton::get_instance_ref();
        std::cout << "-->> o_ref = " << &o_ref << std::endl;
        o_ref.set_i(v3);
        o_ref.set_d(v4);
        std::cout << "-->> o_ref.get_i() = " << o_ref.get_i() << std::endl;
        std::cout << "-->> o_ref.get_d() = " << o_ref.get_d() << std::endl;
    }
    {
        std::cout << "#########################################################" << std::endl;
        std::cout << "-->> Example#2" << std::endl;
        auto o_ptr = Dualton::get_instance_ptr();
        std::cout << "-->> o_ptr = " << o_ptr << std::endl;
        std::cout << "-->> o_ptr->get_i() = " << o_ptr->get_i() << std::endl;
        std::cout << "-->> o_ptr->get_d() = " << o_ptr->get_d() << std::endl;
        if (o_ptr->get_i() != v1)
        {
            std::cout << "Example#2: ERROR#1" << std::endl;
            std::terminate();
        }
        if (o_ptr->get_d() != v2)
        {
            std::cout << "Example#2: ERROR#2" << std::endl;
            std::terminate();
        }
        auto &o_ref = Dualton::get_instance_ref();
        std::cout << "-->> o_ref = " << &o_ref << std::endl;
        std::cout << "-->> o_ref.get_i() = " << o_ref.get_i() << std::endl;
        std::cout << "-->> o_ref.get_d() = " << o_ref.get_d() << std::endl;
        if (o_ref.get_i() != v3)
        {
            std::cout << "Example#2: ERROR#3" << std::endl;
            std::terminate();
        }
        if (o_ref.get_d() != v4)
        {
            std::cout << "Example#2: ERROR#4" << std::endl;
            std::terminate();
        }
        std::cout << "-->> ALL OKAY!" << std::endl;
    }
    {
        std::cout << "#########################################################" << std::endl;
        std::cout << "-->> Example#3" << std::endl;
        std::vector<Dualton> vec_dual;
        std::vector<Dualton *> vec_dual_ptr1;
        std::vector<Dualton *> vec_dual_ptr2;
        for (size_t i = 0; i < DIM; i++)
        {
            vec_dual_ptr1.push_back(Dualton::get_instance_ptr());
        }
        for (size_t i = 0; i < DIM; i++)
        {
            vec_dual_ptr2.push_back(&Dualton::get_instance_ref());
        }
        // TEST HERE #1
        for (size_t i = 0; i < DIM; i++)
        {
            if (vec_dual_ptr1[i]->get_i() != v1)
            {
                std::cout << "Example#3: ERROR#1 at: " << i << std::endl;
                std::terminate();
            }
            if (vec_dual_ptr1[i]->get_d() != v2)
            {
                std::cout << "Example#3: ERROR#2 at: " << i << std::endl;
                std::terminate();
            }
        }
        // TEST HERE #2
        for (size_t i = 0; i < DIM; i++)
        {
            if (vec_dual_ptr2[i]->get_i() != v3)
            {
                std::cout << "Example#3: ERROR#3 at: " << i << std::endl;
                std::terminate();
            }
            if (vec_dual_ptr2[i]->get_d() != v4)
            {
                std::cout << "Example#3: ERROR#4 at: " << i << std::endl;
                std::terminate();
            }
        }
        std::cout << "-->> ALL OKAY!" << std::endl;
    }
    {
        std::cout << "#########################################################" << std::endl;
        std::cout << "-->> Example#4" << std::endl;
        SingletonStatic::get_instance_ptr()->set_i(v1);
        SingletonStatic::get_instance_ptr()->set_d(v2);
        std::vector<SingletonStatic> vec;
        for (size_t i = 0; i < DIM; i++)
        {
            vec.emplace_back(*SingletonStatic::get_instance_ptr());
        }
        for (size_t i = 0; i < DIM; i++)
        {
            if (vec[i].get_i() != v1)
            {
                std::cout << "Example#4: ERROR#1 at: " << i << std::endl;
                std::terminate();
            }
            if (vec[i].get_d() != v2)
            {
                std::cout << "Example#4: ERROR#2 at: " << i << std::endl;
                std::terminate();
            }
        }
        std::cout << "-->> ALL OKAY!" << std::endl;
    }
    {
        std::cout << "#########################################################" << std::endl;
        std::cout << "-->> Example#5" << std::endl;
        SingletonClassic::get_instance_ptr()->set_i(v1);
        SingletonClassic::get_instance_ptr()->set_d(v2);
        std::vector<SingletonClassic> vec;
        for (size_t i = 0; i < DIM; i++)
        {
            vec.emplace_back(*SingletonClassic::get_instance_ptr());
        }
        for (size_t i = 0; i < DIM; i++)
        {
            if (vec[i].get_i() != v1)
            {
                std::cout << "Example#5: ERROR#1 at: " << i << std::endl;
                std::terminate();
            }
            if (vec[i].get_d() != v2)
            {
                std::cout << "Example#5: ERROR#2 at: " << i << std::endl;
                std::terminate();
            }
        }
        std::cout << "-->> ALL OKAY!" << std::endl;
    }
}
