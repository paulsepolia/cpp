#include <iostream>
#include <functional>
#include <thread>
#include <future>
#include <mutex>

constexpr size_t DO_MAX = 10000000;
constexpr size_t MOD_DBG = 1;
std::mutex mtx;

void set_promise_value(
	std::promise<size_t>& promise_local,
	std::future<size_t>& future_local)
{
	static size_t i = 0;
	while (true)
	{
		i++;
		if (i > DO_MAX)
		{
			break;
		}
		std::promise<size_t> promise_tmp;
		promise_local = std::move(promise_tmp);

		mtx.lock();
		future_local = promise_local.get_future();
		mtx.unlock();

		std::this_thread::sleep_for(std::chrono::milliseconds(2000));
		promise_local.set_value(i);
	}
}

void get_future_value(std::future<size_t>& future_local)
{
	static size_t j = 0;
	while (true)
	{
		j++;
		if (j > DO_MAX)
		{
			break;
		}

		mtx.lock();
		if (future_local.valid())
		{
			const auto i_val = future_local.get();
			if(j % MOD_DBG == 0)
			{
				std::cout << "pgg -->> future_local.get(); -->> " << j << " -->> " << i_val << std::endl;
			}

		}
		mtx.unlock();
	}
}

int main()
{
	std::promise<size_t> promise_local;
	std::future<size_t> future_local;

	std::thread promise_thread(set_promise_value, std::ref(promise_local), std::ref(future_local));
	std::thread future_thread(get_future_value, std::ref(future_local));

	promise_thread.join();
	future_thread.join();

	return 0;
}

