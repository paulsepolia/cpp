#include <iostream>
#include <thread>
#include <vector>

const size_t NUM_THREADS = 10000;
const size_t DO_MAX = 1000;

void AddNumber(size_t& x_local)
{
	x_local++;
}

int main()
{
	for (size_t kk = 0; kk <= DO_MAX; kk++)
	{
		std::cout << "----------------------------------------------->> kk = " << kk << std::endl;
		size_t x = 0;
		std::vector<std::thread> v;

		for (size_t i = 0; i < NUM_THREADS; i++)
		{
			v.emplace_back(std::thread(AddNumber, std::ref(x)));
		}

		for (auto& el : v)
		{
			el.join();
		}

		std::cout << "x should be " << NUM_THREADS << " but is " << x << std::endl;
		if (x != NUM_THREADS)
		{
			std::cout << "ERROR: RACE CONDITION HAPPENED" << std::endl;
			std::terminate();
		}
	}
}