#include <iostream>

class SemanticsNone
{
private:
    double m_d = -1;

    SemanticsNone() = default;

public:

    [[maybe_unused]] static SemanticsNone &get_instance()
    {
        static SemanticsNone instance;
        return instance;
    }

    ~SemanticsNone() = default;

    SemanticsNone(const SemanticsNone &other) = delete;

    SemanticsNone(SemanticsNone &&other) noexcept = delete;

    SemanticsNone &operator=(const SemanticsNone &other) = delete;

    SemanticsNone &operator=(SemanticsNone &&other) noexcept = delete;

    [[maybe_unused]] [[nodiscard]] double get() const
    {
        return m_d;
    }

    [[maybe_unused]] void set(double v)
    {
        m_d = v;
    }
};

int main()
{
    {
        std::cout << "------------------------------------->> Example # 1" << std::endl;
        const auto &instance_ref = SemanticsNone::get_instance();
        std::cout << "-->> instance_ref.get() -->> " << instance_ref.get() << std::endl;
    }
    {
        std::cout << "------------------------------------->> Example # 2" << std::endl;
        auto &instance_ref = SemanticsNone::get_instance();
        std::cout << "-->> instance_ref.get() -->> " << instance_ref.get() << std::endl;
        instance_ref.set(1234);
        std::cout << "-->> instance_ref.get() -->> " << instance_ref.get() << std::endl;
    }
}
