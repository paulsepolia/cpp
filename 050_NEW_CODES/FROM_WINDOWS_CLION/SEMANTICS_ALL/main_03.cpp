#include <iostream>
#include <mutex>

class SemanticsNone
{
private:
    static std::mutex m_mtx;
    double m_d = -1;

    SemanticsNone() = default;

    static SemanticsNone *m_instance;

public:

    [[maybe_unused]] static SemanticsNone *get_instance()
    {
        std::unique_lock<std::mutex> lck(m_mtx);
        if (m_instance == nullptr)
        {
            static SemanticsNone instance;
            m_instance = &instance;
        }
        return m_instance;
    }

    ~SemanticsNone() = default;

    SemanticsNone(const SemanticsNone &other) = delete;

    SemanticsNone(SemanticsNone &&other) noexcept = delete;

    SemanticsNone &operator=(const SemanticsNone &other) = delete;

    SemanticsNone &operator=(SemanticsNone &&other) noexcept = delete;

    [[maybe_unused]] [[nodiscard]] double get() const
    {
        return m_d;
    }

    [[maybe_unused]] void set(double v)
    {
        m_d = v;
    }
};

SemanticsNone *SemanticsNone::m_instance = nullptr;
std::mutex SemanticsNone::m_mtx{};

int main()
{
    {
        std::cout << "------------------------------------->> Example # 1" << std::endl;
        const auto instance_ptr = SemanticsNone::get_instance();
        std::cout << "-->> instance_ptr.get() -->> " << instance_ptr->get() << std::endl;
    }
    {
        std::cout << "------------------------------------->> Example # 2" << std::endl;
        auto instance_ptr = SemanticsNone::get_instance();
        std::cout << "-->> instance_ptr.get() -->> " << instance_ptr->get() << std::endl;
        instance_ptr->set(1234);
        std::cout << "-->> instance_ptr.get() -->> " << instance_ptr->get() << std::endl;
    }
    {
        std::cout << "------------------------------------->> Example # 3" << std::endl;
        auto instance_ptr1 = SemanticsNone::get_instance();
        std::cout << "-->> instance_ref1.get() -->> " << instance_ptr1->get() << std::endl;
        instance_ptr1->set(1234);
        std::cout << "-->> instance_ref1.get() -->> " << instance_ptr1->get() << std::endl;

        auto instance_ptr2 = SemanticsNone::get_instance();
        std::cout << "-->> instance_ptr2.get() -->> " << instance_ptr2->get() << std::endl;
        instance_ptr2->set(54321);
        std::cout << "-->> instance__ptr2.get() -->> " << instance_ptr2->get() << std::endl;

        if (instance_ptr1 != instance_ptr2)
        {
            std::cout << "ERROR::1" << std::endl;
            std::terminate();
        }
    }
}
