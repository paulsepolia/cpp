#include <iostream>

class SemanticsAll
{
private:
    double m_d = -1;
public:

    SemanticsAll()
    {
        std::cout << "-->> DEFAULT CONSTRUCTOR" << std::endl;
    }

    ~SemanticsAll()
    {
        std::cout << "-->> DESTRUCTOR" << std::endl;
    }

    [[maybe_unused]] explicit SemanticsAll(double d) : m_d(d)
    {
        std::cout << "-->> ONE ARGUMENT CONSTRUCTOR" << std::endl;
    }

    SemanticsAll(const SemanticsAll &other)
    {
        std::cout << "-->> COPY CONSTRUCTOR" << std::endl;
        m_d = other.m_d;
    }

    SemanticsAll(SemanticsAll &&other) noexcept
    {
        std::cout << "-->> MOVE CONSTRUCTOR" << std::endl;
        m_d = other.m_d;
    }

    SemanticsAll &operator=(const SemanticsAll &other)
    {
        std::cout << "-->> COPY ASSIGNMENT OPERATOR" << std::endl;
        if (&other != this)
        {
            m_d = other.m_d;
        }
        return *this;
    }

    SemanticsAll &operator=(SemanticsAll &&other) noexcept
    {
        std::cout << "-->> MOVE ASSIGNMENT OPERATOR" << std::endl;
        if (&other != this)
        {
            m_d = other.m_d;
        }
        return *this;
    }

    [[maybe_unused]] [[nodiscard]] double get() const
    {
        return m_d;
    }

    [[maybe_unused]] void set(double v)
    {
        m_d = v;
    }
};

int main()
{
    {
        std::cout << "------------------------------------->> Example # 1" << std::endl;
        SemanticsAll a;
        std::cout << "-->> a.get() -->> " << a.get() << std::endl;
    }
    {
        std::cout << "------------------------------------->> Example # 2" << std::endl;
        SemanticsAll a{};
        std::cout << "-->> a.get() -->> " << a.get() << std::endl;
    }
    {
        std::cout << "------------------------------------->> Example # 3" << std::endl;
        SemanticsAll a{-10};
        std::cout << "-->> a.get() -->> " << a.get() << std::endl;
    }
    {
        std::cout << "------------------------------------->> Example # 4" << std::endl;
        SemanticsAll a(10);
        std::cout << "-->> a.get() -->> " << a.get() << std::endl;
    }
    {
        std::cout << "------------------------------------->> Example # 5" << std::endl;
        SemanticsAll a;
        const auto &b = a;
        std::cout << "-->> a.get() -->> " << a.get() << std::endl;
        std::cout << "-->> b.get() -->> " << b.get() << std::endl;
    }
    {
        std::cout << "------------------------------------->> Example # 6" << std::endl;
        SemanticsAll a;
        auto b = a;
        std::cout << "-->> a.get() -->> " << a.get() << std::endl;
        b.set(1234);
        std::cout << "-->> b.get() -->> " << b.get() << std::endl;
    }
    {
        std::cout << "------------------------------------->> Example # 7" << std::endl;
        SemanticsAll a;
        auto b = a;
        std::cout << "-->> a.get() -->> " << a.get() << std::endl;
        b.set(1234);
        std::cout << "-->> b.get() -->> " << b.get() << std::endl;
        a = b;
        std::cout << "-->> a.get() -->> " << a.get() << std::endl;
    }
    {
        std::cout << "------------------------------------->> Example # 8" << std::endl;
        SemanticsAll a;
        auto b = std::move(a);
        b.set(1234);
        std::cout << "-->> b.get() -->> " << b.get() << std::endl;
        a = std::move(b);
        std::cout << "-->> a.get() -->> " << a.get() << std::endl;
        b = a;
        std::cout << "-->> a.get() -->> " << a.get() << std::endl;
        std::cout << "-->> b.get() -->> " << b.get() << std::endl;
    }
}
