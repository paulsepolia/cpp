#include <iostream>

template<typename T>
class Base
{
public:
    void fun1Base()
    {
        static_cast<T *>(this)->fun1Derived();
        static_cast<T &>(*this).fun1Derived();
    }

    void fun2Base()
    {
        static_cast<T *>(this)->fun2Derived();
        static_cast<T &>(*this).fun2Derived();
    }

    void fun3Base()
    {
        static_cast<T *>(this)->fun3Derived();
        static_cast<T &>(*this).fun3Derived();
    }
};

class DerivedA : public Base<DerivedA>
{
public:
    static void fun1Derived()
    {
        std::cout << "-->> fun1Derived() -->> DerivedA" << std::endl;
    }

    static void fun2Derived()
    {
        std::cout << "-->> fun2Derived() -->> DerivedA" << std::endl;
    }

    static void fun3Derived()
    {
        std::cout << "-->> fun3Derived() -->> DerivedA" << std::endl;
    }
};

class DerivedB : public Base<DerivedB>
{
public:
    static void fun1Derived()
    {
        std::cout << "-->> fun1Derived() -->> DerivedB" << std::endl;
    }

    static void fun2Derived()
    {
        std::cout << "-->> fun2Derived() -->> DerivedB" << std::endl;
    }

    static void fun3Derived()
    {
        std::cout << "-->> fun3Derived() -->> DerivedB" << std::endl;
    }
};

template<typename T>
[[maybe_unused]] void force_to_use_interface_ptr(Base<T> *base_ptr)
{
    std::cout << "-->> pass by value pointer -->> Base<T> *base_ptr" << std::endl;
    base_ptr->fun1Base();
    base_ptr->fun2Base();
    base_ptr->fun3Base();
}

template<typename T>
[[maybe_unused]] void force_to_use_interface_ref(Base<T> &base_ref)
{
    std::cout << "-->> pass by ref value -->> Base<T> &base_ref" << std::endl;
    base_ref.fun1Base();
    base_ref.fun2Base();
    base_ref.fun3Base();
}

template<typename T>
[[maybe_unused]] void force_to_use_interface_ref(Base<T> &&base_ref)
{
    std::cout << "-->> pass by ref Rvalue -->> Base<T> &&base_ref" << std::endl;
    base_ref.fun1Base();
    base_ref.fun2Base();
    base_ref.fun3Base();
}


template<typename T>
[[maybe_unused]] void force_to_use_interface_ptr(Base<T> *&base_ptr)
{
    std::cout << "-->> pass by ref pointer -->> Base<T> *&base_ptr" << std::endl;
    base_ptr->fun1Base();
    base_ptr->fun2Base();
    base_ptr->fun3Base();
}

template<typename T>
[[maybe_unused]] void force_to_use_interface_ref(const Base<T> &base_ref)
{
    std::cout << "-->> pass by const ref value -->> const Base<T> &base_ref" << std::endl;
    base_ref.fun1Base();
    base_ref.fun2Base();
    base_ref.fun3Base();
}

int main()
{
    {
        {
            std::cout << "----------------------------------------------->> Example #1 -->> 1/4" << std::endl;
            Base<DerivedA> a;
            a.fun1Base();
            a.fun2Base();
            a.fun3Base();
            Base<DerivedB> b;
            b.fun1Base();
            b.fun2Base();
            b.fun3Base();
        }
        {
            std::cout << "----------------------------------------------->> Example #1 -->> 2/4" << std::endl;
            DerivedA dA;
            dA.fun1Base();
            dA.fun2Base();
            dA.fun3Base();
            DerivedB dB;
            dB.fun1Base();
            dB.fun2Base();
            dB.fun3Base();
        }
        {
            std::cout << "----------------------------------------------->> Example #1 -->> 3/4" << std::endl;
            DerivedA dA;
            dA.fun1Derived();
            dA.fun2Derived();
            dA.fun3Derived();
            DerivedB dB;
            dB.fun1Derived();
            dB.fun1Derived();
            dB.fun1Derived();
        }
        {
            std::cout << "----------------------------------------------->> Example #1 -->> 4/4" << std::endl;
            DerivedA::fun1Derived();
            DerivedA::fun2Derived();
            DerivedA::fun3Derived();
            DerivedB::fun1Derived();
            DerivedB::fun1Derived();
            DerivedB::fun1Derived();
        }
    }
    {
        std::cout << "----------------------------------------------->> Example #2" << std::endl;
        force_to_use_interface_ptr(new DerivedA());
        force_to_use_interface_ptr(new DerivedB());
    }
    {
        std::cout << "----------------------------------------------->> Example #3" << std::endl;
        auto a = DerivedA();
        auto b = DerivedB();
        force_to_use_interface_ref(a);
        force_to_use_interface_ref(b);
    }
    {
        std::cout << "----------------------------------------------->> Example #4" << std::endl;
        force_to_use_interface_ptr(new DerivedA());
        force_to_use_interface_ptr(new DerivedB());
    }
    {
        std::cout << "----------------------------------------------->> Example #5" << std::endl;
        force_to_use_interface_ref(DerivedA());
        force_to_use_interface_ref(DerivedB());
    }
}