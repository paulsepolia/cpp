#include <iostream>

template<typename T>
class Base
{
public:
    void fun1Base()
    {
        static_cast<T *>(this)->fun1Derived();
        static_cast<T &>(*this).fun1Derived();
    }

    void fun2Base()
    {
        static_cast<T *>(this)->fun2Derived();
        static_cast<T &>(*this).fun2Derived();
    }
};

class DerivedA : public Base<DerivedA>
{
public:
    static void fun1Derived()
    {
        std::cout << "-->> fun1Derived() -->> DerivedA" << std::endl;
    }

    static void fun2Derived()
    {
        std::cout << "-->> fun2Derived() -->> DerivedA" << std::endl;
    }
};

class DerivedB : public Base<DerivedB>
{
public:
    static void fun1Derived()
    {
        std::cout << "-->> fun1Derived() -->> DerivedB" << std::endl;
    }

    static void fun2Derived()
    {
        std::cout << "-->> fun2Derived() -->> DerivedB" << std::endl;
    }
};

template<typename T>
[[maybe_unused]] void force_to_use_interface_ptr(Base<T> *base_ptr)
{
    std::cout << "-->> pass by value pointer -->> Base<T> *base_ptr" << std::endl;
    base_ptr->fun1Base();
    base_ptr->fun2Base();
}

template<typename T>
[[maybe_unused]] void force_to_use_interface_ref(Base<T> &base_ref)
{
    std::cout << "-->> pass by ref value -->> Base<T> &base_ref" << std::endl;
    base_ref.fun1Base();
    base_ref.fun2Base();
}

template<typename T>
[[maybe_unused]] void force_to_use_interface_ref(Base<T> &&base_ref)
{
    std::cout << "-->> pass by ref Rvalue -->> Base<T> &&base_ref" << std::endl;
    base_ref.fun1Base();
    base_ref.fun2Base();
}


template<typename T>
[[maybe_unused]] void force_to_use_interface_ptr(Base<T> *&base_ptr)
{
    std::cout << "-->> pass by ref pointer -->> Base<T> *&base_ptr" << std::endl;
    base_ptr->fun1Base();
    base_ptr->fun2Base();
}

template<typename T>
[[maybe_unused]] void force_to_use_interface_ref(const Base<T> &base_ref)
{
    std::cout << "-->> pass by const ref value -->> const Base<T> &base_ref" << std::endl;
    base_ref.fun1Base();
    base_ref.fun2Base();
}

int main()
{
    {
        std::cout << "----------------------------------------------->> Example #1" << std::endl;
        Base<DerivedA> a;
        a.fun1Base();
        a.fun2Base();
        Base<DerivedB> b;
        b.fun1Base();
        b.fun2Base();
    }

    {
        std::cout << "----------------------------------------------->> Example #2" << std::endl;
        force_to_use_interface_ptr(new DerivedA());
        force_to_use_interface_ptr(new DerivedB());
    }

    {
        std::cout << "----------------------------------------------->> Example #3" << std::endl;
        auto a = DerivedA();
        auto b = DerivedB();
        force_to_use_interface_ref(a);
        force_to_use_interface_ref(b);
    }

    {
        std::cout << "----------------------------------------------->> Example #4" << std::endl;
        force_to_use_interface_ptr(new DerivedA());
        force_to_use_interface_ptr(new DerivedB());
    }

    {
        std::cout << "----------------------------------------------->> Example #5" << std::endl;
        force_to_use_interface_ref(DerivedA());
        force_to_use_interface_ref(DerivedB());
    }
}