#include <iostream>
#include <vector>

template<typename T>
class Base
{
private:
    [[maybe_unused]] static int counter;
public:
    double funBase()
    {
        return static_cast<T *>(this)->funDerived();
    }

    ~Base()
    {
        std::cout << "-->> ~Base() -->> " << ++counter << std::endl;
    }
};

template<typename T>
int Base<T>::counter = 0;

class DerivedA : public Base<DerivedA>
{
private:
    [[maybe_unused]] static int counter;
    double m_d;
public:
    explicit DerivedA(double a) : m_d(a)
    {}

    double funDerived()
    {
        return ++m_d;
    }

    ~DerivedA()
    {
        std::cout << "-->> ~DerivedA() -->> " << ++counter << std::endl;
    }
};

int DerivedA::counter = 0;

class DerivedB : public Base<DerivedB>
{
private:
    [[maybe_unused]] static int counter;
    double m_d = 0;
public:

    explicit DerivedB(double b) : m_d(b)
    {}

    double funDerived()
    {
        return ++m_d;
    }

    ~DerivedB()
    {
        std::cout << "-->> ~DerivedB() -->> " << ++counter << std::endl;
    }
};

int DerivedB::counter = 0;

int main()
{
    {
        std::cout << "----------------------------------------------->> Example # 1" << std::endl;
        Base<DerivedA> a1;
        Base<DerivedB> a2;
        std::cout << "-->> a1.funBase() -->> " << a1.funBase() << std::endl;
        std::cout << "-->> a2.funBase() -->> " << a2.funBase() << std::endl;
        Base<DerivedA> *a3 = new DerivedA(11);
        Base<DerivedB> *a4 = new DerivedB(12);
        std::cout << "-->> a1.funBase() -->> " << a3->funBase() << std::endl;
        std::cout << "-->> a2.funBase() -->> " << a4->funBase() << std::endl;
    }
}