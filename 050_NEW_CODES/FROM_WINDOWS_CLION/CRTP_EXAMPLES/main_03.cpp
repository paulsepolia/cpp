#include <iostream>

class NonTemplatedClassByValue
{
public:

    int m_a1;
    int m_a2;

    NonTemplatedClassByValue(int a1, int a2) : m_a1(a1), m_a2(a2)
    {
    }

    ~NonTemplatedClassByValue() = default;

    // delete copy semantics
    NonTemplatedClassByValue(const NonTemplatedClassByValue &) = delete;

    NonTemplatedClassByValue &operator=(const NonTemplatedClassByValue &) noexcept = delete;

    // delete move semantics
    NonTemplatedClassByValue(NonTemplatedClassByValue &&) = default;

    NonTemplatedClassByValue &operator=(NonTemplatedClassByValue &&) noexcept = delete;
};

class NonTemplatedClassByRValueRef
{
public:
    NonTemplatedClassByValue mA1;
    NonTemplatedClassByValue mA2;

    NonTemplatedClassByRValueRef(NonTemplatedClassByValue &&a1, NonTemplatedClassByValue &&a2) :
            mA1(std::move(a1)), mA2(std::move(a2))
    {}
};

template<typename T>
class TemplatedClassByRValueRef
{
public:
    T mImpl;

public:

    explicit TemplatedClassByRValueRef(T &&impl) : mImpl(std::move(impl))
    {
        std::cout << "-->> &mImpl = " << &mImpl << std::endl;
    }
};

int main()
{
    {
        std::cout << "----------------------------------------------->> Example #1" << std::endl;

        NonTemplatedClassByValue o1(1, 2);
        NonTemplatedClassByValue o2(3, 4);
        std::cout << "o1.mA1 -->> " << o1.m_a1 << std::endl;
        std::cout << "o1.mA2 -->> " << o1.m_a2 << std::endl;
        std::cout << "o2.mA1 -->> " << o2.m_a1 << std::endl;
        std::cout << "o2.mA2 -->> " << o2.m_a2 << std::endl;

        auto b1 = NonTemplatedClassByRValueRef(NonTemplatedClassByValue(1, 2), NonTemplatedClassByValue(3, 4));
        std::cout << "b1.mA1.m_a1 -->> " << b1.mA1.m_a1 << std::endl;
        std::cout << "b1.mA1.m_a2 -->> " << b1.mA1.m_a2 << std::endl;
        std::cout << "b1.mA2.m_a1 -->> " << b1.mA2.m_a1 << std::endl;
        std::cout << "b1.mA2.m_a2 -->> " << b1.mA2.m_a2 << std::endl;
    }

    {
        std::cout << "----------------------------------------------->> Example #2" << std::endl;

        TemplatedClassByRValueRef<NonTemplatedClassByValue> b1(NonTemplatedClassByValue(1, 2));
        TemplatedClassByRValueRef<NonTemplatedClassByValue> b2(NonTemplatedClassByValue(3, 4));
        TemplatedClassByRValueRef<NonTemplatedClassByValue> b3(NonTemplatedClassByValue(5, 6));

        std::cout << "b1.mImpl.mA1 -->> " << b1.mImpl.m_a1 << std::endl;
        std::cout << "b1.mImpl.mA2 -->> " << b1.mImpl.m_a2 << std::endl;

        std::cout << "b2.mImpl.mA1 -->> " << b2.mImpl.m_a1 << std::endl;
        std::cout << "b2.mImpl.mA2 -->> " << b2.mImpl.m_a2 << std::endl;

        std::cout << "b3.mImpl.mA1 -->> " << b3.mImpl.m_a1 << std::endl;
        std::cout << "b3.mImpl.mA2 -->> " << b3.mImpl.m_a2 << std::endl;
    }
}