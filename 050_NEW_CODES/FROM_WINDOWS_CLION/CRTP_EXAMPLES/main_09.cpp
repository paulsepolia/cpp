#include <iostream>
#include <vector>

template<typename T>
class Base
{
private:
    [[maybe_unused]] static int counter;
public:
    double funBase()
    {
        return static_cast<T *>(this)->funDerived();
    }

    ~Base()
    {
        std::cout << "-->> ~Base() -->> " << ++counter << std::endl;
    }
};

template<typename T>
int Base<T>::counter = 0;

class DerivedA : public Base<DerivedA>
{
private:
    [[maybe_unused]] static int counter;
    double m_d = 0;
public:
    double funDerived()
    {
        return ++m_d;
    }

    ~DerivedA()
    {
        std::cout << "-->> ~DerivedA() -->> " << ++counter << std::endl;
    }
};

int DerivedA::counter = 0;

class DerivedB : public Base<DerivedB>
{
private:
    [[maybe_unused]] static int counter;
    double m_d = 0;
public:

    double funDerived()
    {
        return ++m_d;
    }

    ~DerivedB()
    {
        std::cout << "-->> ~DerivedB() -->> " << ++counter << std::endl;
    }
};

int DerivedB::counter = 0;

int main()
{
    {
        const int NUM_ELEM = 3;
        std::cout << "----------------------------------------------->> Example # 1" << std::endl;
        std::vector<Base<DerivedA> *> vA;
        std::vector<Base<DerivedB> *> vB;
        for (int i = 0; i < NUM_ELEM; i++)
        {
            vA.push_back(new DerivedA());
            vB.push_back(new DerivedB());
        }

        double a_res = 0;
        for (const auto &el: vA)
        {
            a_res += el->funBase();
        }

        double b_res = 0;
        for (const auto &el: vB)
        {
            b_res += el->funBase();
        }

        for (const auto &el: vA)
        {
            delete el;
        }
        for (const auto &el: vB)
        {
            delete el;
        }

        std::cout << "a_res = " << a_res << std::endl;
        std::cout << "b_res = " << b_res << std::endl;
    }

    {
        const int NUM_ELEM = 4;
        std::cout << "----------------------------------------------->> Example # 2" << std::endl;
        std::vector<Base<DerivedA> *> vA;
        std::vector<Base<DerivedB> *> vB;
        for (int i = 0; i < NUM_ELEM; i++)
        {
            vA.push_back(new DerivedA());
            vB.push_back(new DerivedB());
        }

        double a_res = 0;
        for (const auto &el: vA)
        {
            a_res += ((DerivedA *) (el))->funDerived();
        }

        double b_res = 0;
        for (const auto &el: vB)
        {
            b_res += ((DerivedB *) (el))->funDerived();
        }

        for (const auto &el: vA)
        {
            delete el;
        }
        for (const auto &el: vB)
        {
            delete el;
        }

        std::cout << "a_res = " << a_res << std::endl;
        std::cout << "b_res = " << b_res << std::endl;
    }

    {
        const int NUM_ELEM = 5;
        std::cout << "----------------------------------------------->> Example # 3" << std::endl;
        std::vector<DerivedA *> vA;
        std::vector<DerivedB *> vB;
        for (int i = 0; i < NUM_ELEM; i++)
        {
            vA.push_back(new DerivedA());
            vB.push_back(new DerivedB());
        }

        double a_res = 0;
        for (const auto &el: vA)
        {
            a_res += el->funDerived();
        }

        double b_res = 0;
        for (const auto &el: vB)
        {
            b_res += el->funDerived();
        }

        for (const auto &el: vA)
        {
            delete el;
        }
        for (const auto &el: vB)
        {
            delete el;
        }

        std::cout << "a_res = " << a_res << std::endl;
        std::cout << "b_res = " << b_res << std::endl;
    }
}