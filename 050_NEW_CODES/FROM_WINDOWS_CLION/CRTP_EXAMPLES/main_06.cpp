#include <iostream>
#include <vector>

template<typename T>
class Base
{
private:
    [[maybe_unused]] static int counter;
public:
    double fun1Base()
    {
        return static_cast<T *>(this)->fun1Derived();
    }

    double fun2Base()
    {
        return static_cast<T *>(this)->fun2Derived();
    }

    ~Base()
    {
        std::cout << "-->> ~Base() -->> " << ++counter << std::endl;
    }
};

template<typename T>
int Base<T>::counter = 0;

class DerivedA : public Base<DerivedA>
{
private:
    double m_d = 0;
public:
    double fun1Derived()
    {
        return ++m_d;
    }

    double fun2Derived()
    {
        return ++m_d;
    }

    ~DerivedA()
    {
        std::cout << "-->> ~DerivedA()" << std::endl;
    }
};

class DerivedB : public Base<DerivedB>
{
private:
    double m_d = 0;
public:

    double fun1Derived()
    {
        return ++m_d;
    }

    double fun2Derived()
    {
        return ++m_d;
    }

    ~DerivedB()
    {
        std::cout << "-->> ~DerivedB()" << std::endl;
    }
};

int main()
{
    {
        const int NUM_ELEM = 10;
        std::cout << "----------------------------------------------->> Example # 1" << std::endl;
        std::vector<Base<DerivedA> *> vA;
        std::vector<Base<DerivedB> *> vB;
        for (int i = 0; i < NUM_ELEM; i++)
        {
            vA.push_back(new DerivedA());
            vB.push_back(new DerivedB());
        }

        double a_res1 = 0;
        double a_res2 = 0;
        for (const auto &el: vA)
        {
            a_res1 += el->fun1Base();
            a_res2 += el->fun2Base();
        }

        double b_res1 = 0;
        double b_res2 = 0;
        for (const auto &el: vB)
        {
            b_res1 += el->fun1Base();
            b_res2 += el->fun2Base();
        }

        for (const auto &el: vA)
        {
            delete el;
        }
        for (const auto &el: vB)
        {
            delete el;
        }

        std::cout << "a_res1 = " << a_res1 << std::endl;
        std::cout << "a_res2 = " << a_res2 << std::endl;
        std::cout << "b_res1 = " << b_res1 << std::endl;
        std::cout << "b_res2 = " << b_res2 << std::endl;
    }

    {
        const int NUM_ELEM = 10;
        std::cout << "----------------------------------------------->> Example # 2" << std::endl;
        std::vector<Base<DerivedA> *> vA;
        std::vector<Base<DerivedB> *> vB;
        for (int i = 0; i < NUM_ELEM; i++)
        {
            vA.push_back(new DerivedA());
            vB.push_back(new DerivedB());
        }

        double a_res1 = 0;
        double a_res2 = 0;
        for (const auto &el: vA)
        {
            a_res1 += ((DerivedA *) (el))->fun1Derived();
            a_res2 += ((DerivedA *) (el))->fun2Derived();
        }

        double b_res1 = 0;
        double b_res2 = 0;
        for (const auto &el: vB)
        {
            b_res1 += ((DerivedB *) (el))->fun1Derived();
            b_res2 += ((DerivedB *) (el))->fun2Derived();
        }

        for (const auto &el: vA)
        {
            delete el;
        }
        for (const auto &el: vB)
        {
            delete el;
        }

        std::cout << "a_res1 = " << a_res1 << std::endl;
        std::cout << "a_res2 = " << a_res2 << std::endl;
        std::cout << "b_res1 = " << b_res1 << std::endl;
        std::cout << "b_res2 = " << b_res2 << std::endl;
    }
}