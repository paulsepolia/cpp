#include <iostream>
#include <memory>

class AbstractClass
{
public:
    virtual void pureVirtualFunction() = 0;

    virtual ~AbstractClass() = default;
};

class DerivedClass final : public AbstractClass
{
public:

    explicit DerivedClass(int k)
    {
        m_k = k;
    };

    void pureVirtualFunction() final
    {
        std::cout << "I am called via an abstract class base pointer -->> " << m_k << std::endl;
        m_k--;
        if (m_k == 0) return;
        pureVirtualFunction();
    }

    ~DerivedClass() final = default;

private:

    int m_k;
};

void you_are_forced_to_use_the_interface(AbstractClass *a)
{
    a->pureVirtualFunction();
}

int main()
{
    auto d = new DerivedClass(1000);
    you_are_forced_to_use_the_interface(d);
}