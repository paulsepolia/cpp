#include <boost/asio.hpp>
#include <iostream>
#include <vector>
#include <chrono>
#include <thread>

const size_t NUM_MSGS = 1000000000;
const size_t DIM_MSG = 8 * 1000000;
const size_t MOD_MSG = 2000;
bool READ_MSG_EVERY_SEC = false;
bool FLG_DEBUG = false;
const double G1 = 1024.0 * 1024.0 * 1024.0;

int main(int argc, char* argv[])
{
	if (argc < 2)
	{
		std::cout << "INFO: RECEIVER: USAGE: " << argv[0] << " port" << std::endl;
		return 0;
	}

	const auto port_num = std::stoi(argv[1]);

	auto io_context{ boost::asio::io_context{}};

	auto acceptor{ boost::asio::ip::tcp::acceptor(
		io_context,
		boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port_num)) };

	auto socket{ boost::asio::ip::tcp::socket(io_context) };

	acceptor.accept(socket);

	auto data = std::shared_ptr<char>(new char[DIM_MSG], std::default_delete<char[]>());

	auto t1 = std::chrono::steady_clock::now();

	for (size_t i = 0; i < NUM_MSGS; i++)
	{
		if (READ_MSG_EVERY_SEC)
		{
			std::this_thread::sleep_for(std::chrono::seconds(READ_MSG_EVERY_SEC));
		}

		size_t count = 0;

		while (count < DIM_MSG)
		{
			count += socket.receive(boost::asio::buffer(data.get() + count, DIM_MSG - count));
		}

		if (i != 0 && i % MOD_MSG == 0)
		{
			const auto t2 = std::chrono::steady_clock::now();
			const auto time_tot = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);

			std::cout << std::endl;
			std::cout << "------------------------------------->>  MESSAGES RECEIVED: " << i << std::endl;
			std::cout << "INFO: DATA SPEED (GBytes/sec) = "
					  << (DIM_MSG * MOD_MSG * sizeof(char)) / time_tot.count() / G1 << std::endl;

			t1 = t2;
		}

		if (FLG_DEBUG)
		{
			if (data.get()[DIM_MSG - 1] != '1')
			{
				std::cout << "ERROR: RECEIVER: ERROR READING MESSAGE # " << i << " : "
						  << data.get()[DIM_MSG - 1] << std::endl;
				std::terminate();
			}
		}
	}

	socket.close();
}
