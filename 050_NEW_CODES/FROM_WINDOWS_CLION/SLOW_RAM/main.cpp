#include <iostream>
#include <chrono>
#include <queue>
#include <thread>
#include <mutex>
#include <string>
#include <unordered_set>

class BenchmarkTimer
{
public:

    explicit BenchmarkTimer() :
            _res(0.0),
            t_prev{std::chrono::steady_clock::now()}
    {
    };

    [[maybe_unused]] void printTime(const std::string &mark_str) const
    {
        const auto t_loc{std::chrono::steady_clock::now()};
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double >>(t_loc - t_prev).count()};
        t_prev = t_loc;
        std::cout << mark_str << " (secs) = " << total_time << std::endl;
    }

    [[maybe_unused]] void resetTimer()
    {
        const auto t_loc{std::chrono::steady_clock::now()};
        t_prev = t_loc;
    }

    ~BenchmarkTimer() = default;

    [[maybe_unused]] auto setRes(const double &res) -> void
    {
        _res = res;
    }

private:

    double _res;
    mutable decltype(std::chrono::steady_clock::now()) t_prev;
};

constexpr auto DO_MAX{(size_t) 1'000};
constexpr auto DIM_MAX{(size_t) 20'000'000};
constexpr auto DIM_MAX_SET{(size_t) 40'000'000};
constexpr auto RECORD_SIZE((size_t) 24);

auto mtx{std::mutex{}};

auto main() -> int
{
    auto lVEC{[]()
              {
                  for (size_t k = 0; k < DO_MAX; ++k)
                  {

                      mtx.lock();
                      std::cout << "--------------------------------->> VECTOR counter = " << k << std::endl;
                      mtx.unlock();

                      BenchmarkTimer ot;

                      auto vec{std::vector<void *>()};

                      for (size_t j = 0; j < DIM_MAX; ++j)
                      {
                          void *ptr = malloc(RECORD_SIZE);
                          vec.push_back(ptr);
                      }

                      mtx.lock();
                      ot.printTime("MARK: BUILD VECTOR");
                      mtx.unlock();

                      std::for_each(vec.begin(), vec.end(), [](void *ptr)
                      { free(ptr); });

                      mtx.lock();
                      ot.printTime("MARK: FREE RAM VECTOR");
                      mtx.unlock();

                      vec.clear();
                      mtx.lock();
                      ot.printTime("MARK: CLEAR VECTOR");
                      mtx.unlock();
                  }
              }};

    auto lSET{[]()
              {
                  for (size_t kk = 0; kk < DO_MAX; kk++)
                  {

                      mtx.lock();
                      std::cout << "--------------------------------->> SET counter = " << kk << std::endl;
                      mtx.unlock();

                      BenchmarkTimer ot;

                      auto cont{std::unordered_set<void *>()};

                      for (size_t jj = 0; jj < DIM_MAX_SET; jj++)
                      {
                          void *ptr = malloc(RECORD_SIZE);
                          cont.emplace(ptr);
                      }

                      mtx.lock();
                      ot.printTime("MARK: BUILD SET");
                      mtx.unlock();

                      std::for_each(cont.begin(), cont.end(), [](void *ptr)
                      { free(ptr); });

                      mtx.lock();
                      ot.printTime("MARK: FREE RAM SET");
                      mtx.unlock();

                      cont.clear();
                      ot.printTime("MARK: SET CLEAN");
                      ot.resetTimer();
                  }
              }};

    auto t1{std::thread(lVEC)};
    //auto t2{std::thread(lVEC)};
    auto t2{std::thread(lSET)};
    //auto t2{std::thread(lSET)};

    t1.join();
    t2.join();
}
