#include <iostream>

constexpr int DIM_SIZE = 1000;
constexpr int MAT_SIZE = 10;

auto f0()
{
    auto local = new int(10);
    return local;
}

auto f1(int *&a)
{
    a = new int(10);
}

auto f2(int *&a)
{
    a = new int[DIM_SIZE];
    for (int i = 0; i < DIM_SIZE; i++)
    {
        a[i] = i;
    }
}

auto f3(int **&a)
{
    a = new int *[DIM_SIZE];
    for (int i = 0; i < DIM_SIZE; i++)
    {
        a[i] = new int[DIM_SIZE];
    }

    for (int i = 0; i < DIM_SIZE; i++)
    {
        for (int j = 0; j < DIM_SIZE; j++)
        {
            a[i][j] = j + (i * j);
        }
    }
}

auto m0()
{
    auto mat = new int *[MAT_SIZE];
    for (int i = 0; i < MAT_SIZE; i++)
    {
        mat[i] = new int[MAT_SIZE];
    }
    for (int i = 0; i < MAT_SIZE; i++)
    {
        for (int j = 0; j < MAT_SIZE; j++)
        {
            mat[i][j] = i + j;
        }
    }
    return mat;
}

auto m1()
{
    auto mat = new int **[MAT_SIZE];
    for (int i = 0; i < MAT_SIZE; i++)
    {
        mat[i] = new int *[MAT_SIZE];
    }
    for (int i = 0; i < MAT_SIZE; i++)
    {
        for (int j = 0; j < MAT_SIZE; j++)
        {
            mat[i][j] = new int[MAT_SIZE];
        }
    }
    for (int i = 0; i < MAT_SIZE; i++)
    {
        for (int j = 0; j < MAT_SIZE; j++)
        {
            for (int k = 0; k < MAT_SIZE; k++)
            {
                mat[i][j][k] = i + j + k;
            }
        }
    }
    return mat;
}


int main()
{
    {
        std::cout << "----------------->> Example # 1" << std::endl;
        int *a;
        f1(a);
        std::cout << "-->> a  = " << a << std::endl;
        std::cout << "-->> *a = " << *a << std::endl;
        delete a;
        a = nullptr;
        if (a)
        {
            std::cout << "-->> a  = " << a << std::endl;
            std::cout << "-->> *a = " << *a << std::endl;
        }
    }
    {
        std::cout << "----------------->> Example # 2" << std::endl;
        int *a;
        f2(a);
        std::cout << "-->> a    = " << a << std::endl;
        std::cout << "-->> *a   = " << *a << std::endl;
        std::cout << "-->> a[0] = " << a[0] << std::endl;
        std::cout << "-->> a[1] = " << a[1] << std::endl;

        std::cout << "-->> *(a++) = " << *(++a) << std::endl;
        std::cout << "-->> a[ 0]  = " << a[0] << std::endl;
        std::cout << "-->> a[-1]  = " << a[-1] << std::endl;

        std::cout << "-->> *(a++) = " << *(++a) << std::endl;
        std::cout << "-->> a[ 0]  = " << a[0] << std::endl;
        std::cout << "-->> a[-1]  = " << a[-1] << std::endl;
        std::cout << "-->> a[-2]  = " << a[-2] << std::endl;

        --a;
        --a;
        delete[] a;
    }
    {
        std::cout << "----------------->> Example # 3" << std::endl;
        int **a;
        f3(a);
        std::cout << "-->> a[0][0] = " << a[0][0] << std::endl;
        std::cout << "-->> a[0][1] = " << a[0][1] << std::endl;
        std::cout << "-->> a[1][0] = " << a[1][0] << std::endl;
        std::cout << "-->> a[1][1] = " << a[1][1] << std::endl;
        std::cout << "-->> a[1][2] = " << a[1][2] << std::endl;

        std::cout << "-->> Testing here..." << std::endl;

        for (int i = 0; i < DIM_SIZE; i++)
        {
            for (int j = 0; j < DIM_SIZE; j++)
            {
                if (a[i][j] != j + (i * j))
                {
                    std::cout << "ERROR!! for " << i << ", " << j << std::endl;
                }
            }
        }

        for (int i = 0; i < DIM_SIZE; i++)
        {
            delete[] a[i];
        }
        delete[] a;
    }
    {
        std::cout << "----------------->> Example # 4" << std::endl;
        const auto p = f0();
        std::cout << " p = " << p << std::endl;
        std::cout << "*p = " << *p << std::endl;
        *p = 11;
        std::cout << " p = " << p << std::endl;
        std::cout << "*p = " << *p << std::endl;
        delete p;
    }
    {
        std::cout << "----------------->> Example # 5" << std::endl;
        const auto p = m0();
        std::cout << " p = " << p[1][1] << std::endl;
        for (int i = 0; i < MAT_SIZE; i++)
        {
            for (int j = 0; j < MAT_SIZE; j++)
            {
                if (p[i][j] != i + j)
                {
                    std::cout << "ERROR: " << i << ", " << j << std::endl;
                }
            }
        }
        for (int i = 0; i < MAT_SIZE; i++)
        {
            delete[] p[i];
        }
        delete[] p;
    }
    {
        std::cout << "----------------->> Example # 6" << std::endl;
        const auto p = m1();
        std::cout << " p = " << p[1][1][1] << std::endl;
        for (int i = 0; i < MAT_SIZE; i++)
        {
            for (int j = 0; j < MAT_SIZE; j++)
            {
                for (int k = 0; k < MAT_SIZE; k++)
                {
                    if (p[i][j][k] != i + j + k)
                    {
                        std::cout << "ERROR: " << i << ", " << j << ", " << k << std::endl;
                    }
                }
            }
        }
        for (int i = 0; i < MAT_SIZE; i++)
        {
            for (int j = 0; j < MAT_SIZE; j++)
            {
                delete[] p[i][j];
            }
        }
        for (int i = 0; i < MAT_SIZE; i++)
        {
            delete[] p[i];
        }
        delete [] p;
    }
}
