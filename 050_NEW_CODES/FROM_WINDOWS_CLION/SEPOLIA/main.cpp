#include "List.h"
#include <iostream>
#include <cassert>

class A{};

class B: public A{};

class C{};

template<class T, typename std::enable_if<std::is_base_of<A, T>{}, bool>::type = true>
	void myFunction()
{
	std::cout << "function A" << std::endl;
}


//template<typename TClass, class = std::enable_if_t<std::is_base_of_v<A, TClass>>>
//        TimePoint GetTimePoint2(const TimeDuration& td) const
//        {
//            return TimePoint(td);
//        }

int main()
{
	myFunction<A>();

	myFunction<B>();

	//myFunction<C>();
}
