#include <iostream>
#include <mutex>

class StaticClassA
{
private:
    static double m_d;
    static std::mutex mtx;
public:
    // delete all kind of constructors and destructors
    StaticClassA() = delete;

    StaticClassA(const StaticClassA &) = delete;

    StaticClassA &operator=(const StaticClassA &) = delete;

    StaticClassA(StaticClassA &&) noexcept = delete;

    StaticClassA &operator=(StaticClassA &&) noexcept = delete;

    ~StaticClassA() = delete;

    static double f1()
    {
        std::unique_lock<std::mutex> lck(mtx);
        return ++m_d;
    }

    static double f2()
    {
        std::unique_lock<std::mutex> lck(mtx);
        return ++m_d;
    }
};

double StaticClassA::m_d = 0;
std::mutex StaticClassA::mtx{};

int main()
{
    {
        std::cout << "------------------------------------->> Example -->> 1" << std::endl;
        std::cout << StaticClassA::f1() << std::endl;
        std::cout << StaticClassA::f2() << std::endl;
    }

    {
        std::cout << "------------------------------------->> Example -->> 2" << std::endl;
        std::cout << StaticClassA::f1() << std::endl;
        std::cout << StaticClassA::f2() << std::endl;
    }
}
