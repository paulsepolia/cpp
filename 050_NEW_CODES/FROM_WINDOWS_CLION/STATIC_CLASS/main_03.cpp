#include <iostream>
#include <mutex>

class SingletonClassA
{
private:
    std::mutex m_mtx;
    double m_d = 0;

    SingletonClassA() = default;

public:

    SingletonClassA(const SingletonClassA &) = delete;

    SingletonClassA(SingletonClassA &&) noexcept = delete;

    SingletonClassA &operator=(const SingletonClassA &) = delete;

    SingletonClassA &operator=(SingletonClassA &&) noexcept = delete;

    ~SingletonClassA() = default;

    static SingletonClassA &get_instance()
    {
        static SingletonClassA instance;
        return instance;
    }

    double fun1()
    {
        std::unique_lock<std::mutex> lck(m_mtx);
        return ++m_d;
    }

    double fun2()
    {
        std::unique_lock<std::mutex> lck(m_mtx);
        return ++m_d;
    }
};

class SingletonClassB
{
private:
    std::mutex m_mtx;
    double m_d = 0;

    SingletonClassB() = default;

public:

    SingletonClassB(const SingletonClassB &) = delete;

    SingletonClassB(SingletonClassB &&) noexcept = delete;

    SingletonClassB &operator=(const SingletonClassB &) = delete;

    SingletonClassB &operator=(SingletonClassB &&) noexcept = delete;

    ~SingletonClassB() = default;

    static SingletonClassB *get_instance()
    {
        static SingletonClassB instance;
        return &instance;
    }

    double fun1()
    {
        std::unique_lock<std::mutex> lck(m_mtx);
        return ++m_d;
    }

    double fun2()
    {
        std::unique_lock<std::mutex> lck(m_mtx);
        return ++m_d;
    }
};

class DualtronClass
{
private:
    std::mutex m_mtx;
    double m_d = 0;

    DualtronClass() = default;

public:

    DualtronClass(const DualtronClass &) = delete;

    DualtronClass(DualtronClass &&) noexcept = delete;

    DualtronClass &operator=(const DualtronClass &) = delete;

    DualtronClass &operator=(DualtronClass &&) noexcept = delete;

    ~DualtronClass() = default;

    static DualtronClass *get_instance_ptr()
    {
        static DualtronClass instance;
        return &instance;
    }

    static DualtronClass &get_instance_ref()
    {
        static DualtronClass instance;
        return instance;
    }

    double fun1()
    {
        std::unique_lock<std::mutex> lck(m_mtx);
        return ++m_d;
    }

    double fun2()
    {
        std::unique_lock<std::mutex> lck(m_mtx);
        return ++m_d;
    }

    double get_m()
    {
        std::unique_lock<std::mutex> lck(m_mtx);
        return m_d;
    }
};


int main()
{
    {
        std::cout << "------------------------------------->> Example -->> 1" << std::endl;

        auto &o1 = SingletonClassA::get_instance();
        auto &o2 = SingletonClassA::get_instance();

        std::cout << "-->> &o1 -->> " << &o1 << std::endl;
        std::cout << "-->> &o2 -->> " << &o2 << std::endl;

        std::cout << "-->> o1.fun1() -->> " << o1.fun1() << std::endl;
        std::cout << "-->> o1.fun2() -->> " << o1.fun2() << std::endl;
        std::cout << "-->> o2.fun1() -->> " << o2.fun1() << std::endl;
        std::cout << "-->> o2.fun2() -->> " << o2.fun2() << std::endl;
    }

    {
        std::cout << "------------------------------------->> Example -->> 2" << std::endl;

        auto ptr1 = SingletonClassB::get_instance();
        auto ptr2 = SingletonClassB::get_instance();

        std::cout << "-->> ptr1 -->> " << ptr1 << std::endl;
        std::cout << "-->> ptr2 -->> " << ptr2 << std::endl;

        std::cout << "-->> ptr1->fun1() -->> " << ptr1->fun1() << std::endl;
        std::cout << "-->> ptr1->fun2() -->> " << ptr1->fun2() << std::endl;
        std::cout << "-->> ptr2->fun1() -->> " << ptr2->fun1() << std::endl;
        std::cout << "-->> ptr2->fun2() -->> " << ptr2->fun2() << std::endl;
    }

    {
        std::cout << "------------------------------------->> Example -->> 3" << std::endl;

        auto ptr1 = DualtronClass::get_instance_ptr();
        auto ptr2 = DualtronClass::get_instance_ptr();
        auto &o1 = DualtronClass::get_instance_ref();
        auto &o2 = DualtronClass::get_instance_ref();

        std::cout << "-->> ptr1 -->> " << ptr1 << std::endl;
        std::cout << "-->> ptr2 -->> " << ptr2 << std::endl;
        std::cout << "-->>  &o1 -->> " << &o1 << std::endl;
        std::cout << "-->>  &o2 -->> " << &o2 << std::endl;

        std::cout << "-->> ptr1->fun1() -->> " << ptr1->fun1() << std::endl;
        std::cout << "-->> ptr1->fun2() -->> " << ptr1->fun2() << std::endl;
        std::cout << "-->> ptr2->fun1() -->> " << ptr2->fun1() << std::endl;
        std::cout << "-->> ptr2->fun2() -->> " << ptr2->fun2() << std::endl;
        std::cout << "-->> ptr2->fun2() -->> " << ptr2->fun2() << std::endl;
        std::cout << "-->> ptr2->fun2() -->> " << ptr2->fun2() << std::endl;

        std::cout << "-->> o1.fun1() -->> " << o1.fun1() << std::endl;
        std::cout << "-->> o1.fun2() -->> " << o1.fun2() << std::endl;
        std::cout << "-->> o2.fun1() -->> " << o2.fun1() << std::endl;
        std::cout << "-->> o2.fun2() -->> " << o2.fun2() << std::endl;

        std::cout << "-->> o1.get_m() -->> " << o1.get_m() << std::endl;
        std::cout << "-->> o2.get_m() -->> " << o2.get_m() << std::endl;
        std::cout << "-->> ptr1->get_m() -->> " << ptr1->get_m() << std::endl;
        std::cout << "-->> ptr2->get_m() -->> " << ptr2->get_m() << std::endl;
    }
}
