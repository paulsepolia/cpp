#include <iostream>
#include <mutex>

class SingletonWithCopyAndMoveSemanticsIsNotASingleton
{
private:
    std::mutex m_mtx;
    double m_d = 0;

    SingletonWithCopyAndMoveSemanticsIsNotASingleton() = default;

public:

    SingletonWithCopyAndMoveSemanticsIsNotASingleton(const SingletonWithCopyAndMoveSemanticsIsNotASingleton &other) : m_mtx()
    {
        std::unique_lock<std::mutex> lck(m_mtx);
        m_d = other.m_d;
    }

    SingletonWithCopyAndMoveSemanticsIsNotASingleton(SingletonWithCopyAndMoveSemanticsIsNotASingleton &&other) noexcept: m_mtx()
    {
        std::unique_lock<std::mutex> lck(m_mtx);
        m_d = other.m_d;
    }

    SingletonWithCopyAndMoveSemanticsIsNotASingleton &operator=(const SingletonWithCopyAndMoveSemanticsIsNotASingleton &other)
    {
        std::unique_lock<std::mutex> lck(m_mtx);
        if (&other != this)
        {
            m_d = other.m_d;
        }
        return *this;
    }

    SingletonWithCopyAndMoveSemanticsIsNotASingleton &operator=(SingletonWithCopyAndMoveSemanticsIsNotASingleton &&other) noexcept
    {
        std::unique_lock<std::mutex> lck(m_mtx);
        if (&other != this)
        {
            m_d = other.m_d;
        }

        return *this;
    }

    ~SingletonWithCopyAndMoveSemanticsIsNotASingleton() = default;

    static SingletonWithCopyAndMoveSemanticsIsNotASingleton *get_instance()
    {
        static SingletonWithCopyAndMoveSemanticsIsNotASingleton instance;
        return &instance;
    }

    double fun()
    {
        std::unique_lock<std::mutex> lck(m_mtx);
        return ++m_d;
    }
};

int main()
{
    {
        std::cout << "------------------------------------->> Example -->> 1" << std::endl;

        auto o1 = SingletonWithCopyAndMoveSemanticsIsNotASingleton::get_instance();
        auto o2 = SingletonWithCopyAndMoveSemanticsIsNotASingleton::get_instance();

        std::cout << "-->> o1->fun() -->> " << o1->fun() << std::endl;
        std::cout << "-->> o2->fun() -->> " << o2->fun() << std::endl;
    }

    {
        std::cout << "------------------------------------->> Example -->> 2" << std::endl;

        auto s1 = SingletonWithCopyAndMoveSemanticsIsNotASingleton(*SingletonWithCopyAndMoveSemanticsIsNotASingleton::get_instance());

        if (&s1 != SingletonWithCopyAndMoveSemanticsIsNotASingleton::get_instance())
        {
            std::cout << &s1 << " -->> " << SingletonWithCopyAndMoveSemanticsIsNotASingleton::get_instance() << std::endl;
            std::cout << "CORRECT COPY-CONSTRUCTOR" << std::endl;
        }

        auto s2 = *SingletonWithCopyAndMoveSemanticsIsNotASingleton::get_instance();

        if (&s2 != SingletonWithCopyAndMoveSemanticsIsNotASingleton::get_instance())
        {
            std::cout << &s2 << " -->> " << SingletonWithCopyAndMoveSemanticsIsNotASingleton::get_instance() << std::endl;
            std::cout << "CORRECT COPY-ASSIGNMENT-OPERATOR" << std::endl;
        }
    }

    {
        std::cout << "------------------------------------->> Example -->> 3" << std::endl;

        auto s1 = SingletonWithCopyAndMoveSemanticsIsNotASingleton(std::move(*SingletonWithCopyAndMoveSemanticsIsNotASingleton::get_instance()));

        if (&s1 != SingletonWithCopyAndMoveSemanticsIsNotASingleton::get_instance())
        {
            std::cout << &s1 << " -->> " << SingletonWithCopyAndMoveSemanticsIsNotASingleton::get_instance() << std::endl;
            std::cout << "CORRECT MOVE-CONSTRUCTOR" << std::endl;
        }

        auto s2 = std::move(*SingletonWithCopyAndMoveSemanticsIsNotASingleton::get_instance());

        if (&s2 != SingletonWithCopyAndMoveSemanticsIsNotASingleton::get_instance())
        {
            std::cout << &s2 << " -->> " << SingletonWithCopyAndMoveSemanticsIsNotASingleton::get_instance() << std::endl;
            std::cout << "CORRECT MOVE-ASSIGNMENT-OPERATOR" << std::endl;
        }
    }
}
