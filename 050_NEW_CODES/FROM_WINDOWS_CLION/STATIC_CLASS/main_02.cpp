#include <iostream>
#include <mutex>
#include <vector>

class StaticClassA
{
private:
    static double m_d;
    static std::mutex mtx;
public:
    // delete all kind of constructors and destructors
    StaticClassA() = default;

    StaticClassA(const StaticClassA &) = delete;

    StaticClassA &operator=(const StaticClassA &) = delete;

    StaticClassA(StaticClassA &&) noexcept = delete;

    StaticClassA &operator=(StaticClassA &&) noexcept = delete;

    ~StaticClassA() = default;

    static double f1()
    {
        std::unique_lock<std::mutex> lck(mtx);
        return ++m_d;
    }

    static double f2()
    {
        std::unique_lock<std::mutex> lck(mtx);
        return ++m_d;
    }

    [[maybe_unused]] static void set(double v)
    {
        std::unique_lock<std::mutex> lck(mtx);
        m_d = v;
    }

    [[maybe_unused]] static double get()
    {
        std::unique_lock<std::mutex> lck(mtx);
        return m_d;
    }
};

double StaticClassA::m_d = 0;
std::mutex StaticClassA::mtx{};

int main()
{
    {
        const int NUM_ELEMS = 100;
        std::cout << "------------------------------------->> Example -->> 1" << std::endl;
        std::vector<StaticClassA *> v;
        for (int i = 0; i < NUM_ELEMS; i++)
        {
            v.emplace_back(new StaticClassA());
        }

        for (const auto &el: v)
        {
            std::cout << el->f1() << std::endl;
            std::cout << el->f2() << std::endl;
        }

        StaticClassA::set(-200);

        for (const auto &el: v)
        {
            std::cout << StaticClassA::f1() << std::endl;
            std::cout << StaticClassA::f2() << std::endl;
        }

        for (auto &el: v)
        {
            delete el;
        }

        {
            StaticClassA::set(1234);
            std::cout << StaticClassA::get() << std::endl;
        }
    }
}
