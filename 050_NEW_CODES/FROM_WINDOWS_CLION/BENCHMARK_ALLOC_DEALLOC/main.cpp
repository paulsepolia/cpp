#include <iostream>
#include <cmath>
#include <ctime>

int main()
{
	std::cout << " ===================================================================== " << std::endl;
	std::cout << "  1 --> The Allocations - Initializations - Deletions Benchmark" << std::endl;
	std::cout << "  2 --> Allocating - Initialising - Deleting 1,2,3,...,100 vectors" << std::endl;
	std::cout << "  3 --> Each vector's length is 1000000" << std::endl;
	std::cout << "  4 --> Please wait..." << std::endl;
	std::cout << " ===================================================================== " << std::endl;

	const int n_LV = 100;
	const int n_LVE = 1000000;

	// local variables

	[[maybe_unused]] auto t1 = std::clock();
	[[maybe_unused]] auto t2 = std::clock();
	[[maybe_unused]] auto t3 = std::clock();
	[[maybe_unused]] auto t4 = std::clock();
	[[maybe_unused]] auto t5 = std::clock();
	[[maybe_unused]] auto t6 = std::clock();
	[[maybe_unused]] auto t7 = std::clock();
	[[maybe_unused]] auto t8 = std::clock();
	double sum1 = 0.0;
	double sum2 = 0.0;
	double sum3 = 0.0;
	long iDEC = 0;
	long iDEL = 0;
	long iINIT = 0;

	t1 = std::clock();

	for (int i = 1; i != n_LV; ++i)
	{
		//============================================================= 1.

		t5 = std::clock();

		auto** LV = new double* [i + 1];  // Declaration of Vectors

		for (int kA = 0; kA != i + 1; ++kA)
		{
			// Creation of  Vectors
			LV[kA] = new double[n_LVE];
			iDEC += 1;
		}

		t6 = std::clock();

		sum2 += (double)(t6 - t5) / static_cast<double>(CLOCKS_PER_SEC);

		//============================================================== 2.

		t3 = std::clock();

		for (int kB = 0; kB != i + 1; ++kB)
		{
			// Initialization of Vectors
			for (int kC = 0; kC != n_LVE; ++kC)
			{
				LV[kB][kC] = 1.0;
				iINIT += 1;
			}
		}

		t4 = std::clock();

		sum1 += (double)(t4 - t3) / (double)(CLOCKS_PER_SEC);

		//================================================================ 3.

		t7 = std::clock();

		for (int kD = 0; kD != i + 1; ++kD)
		{
			delete[] LV[kD];
			iDEL += 1;
		}

		delete[] LV;

		t8 = std::clock();

		sum3 += (double)(t8 - t7) / (double)(CLOCKS_PER_SEC);
	}

	t2 = std::clock();

	std::cout << std::endl;
	std::cout << "  5 --> The Results are the following " << std::endl;
	std::cout << "  6 --> Exact Real Time = " << (double)(t2 - t1) / (double)(CLOCKS_PER_SEC) << std::endl;
	std::cout << "  7 --> Exact Real Time For Initialization = " << sum1 << std::endl;
	std::cout << "  8 --> Exact Real Time For Creation = " << sum2 << std::endl;
	std::cout << "  9 --> Exact Real Time For Deletion = " << sum3 << std::endl;
	std::cout << std::endl;
}
