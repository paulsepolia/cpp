//===========================================//
// INHERITANCE vs NON-INHERITANCE SPEED GAME //
//===========================================//

#include "classesPolymorphic.h"
#include "classesNonPolymorphic.h"
#include <iostream>
#include <iomanip>
#include <chrono>
#include <cmath>

typedef long long int lli;

int main()
{
	// local parameters

	const lli DIM = static_cast<lli>(std::pow(10.0, 10.0));

	// local variables

	double x1;
	[[maybe_unused]] auto t1 = std::chrono::system_clock::now();
	[[maybe_unused]] auto t2 = std::chrono::system_clock::now();
	std::chrono::duration<double> time_span{};
	std::cout << std::fixed;
	std::cout << std::setprecision(5);

	std::cout << " --> POLYMORPHIC CALL SPEED TEST --------------------------------> 1" << std::endl;

	{
		std::cout << " --> POLYM::A * pa1 = new POLYM::A;" << std::endl;
		auto pa1 = new POLYM::A;
		std::cout << " --> POLYM::A * pb1 = new POLYM::B;" << std::endl;
		POLYM::A* pb1 = new POLYM::B;
		std::cout << " --> POLYM::A * pc1 = new POLYM::C;" << std::endl;
		POLYM::A* pc1 = new POLYM::C;
		std::cout << " --> POLYM::A * pd1 = new POLYM::D;" << std::endl;
		POLYM::A* pd1 = new POLYM::D;

		// timing A

		t1 = std::chrono::system_clock::now();

		for (lli i = 0; i != DIM; i++)
		{
			pa1->fun();
			x1 = pa1->get();
		}

		std::cout << " --> A --> x1 = " << x1 << std::endl;
		t2 = std::chrono::system_clock::now();
		time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
		std::cout << " --> A --> time used = " << time_span.count() << std::endl;

		// timing B

		t1 = std::chrono::system_clock::now();

		for (lli i = 0; i != DIM; i++)
		{
			pb1->fun();
			x1 = pb1->get();
		}

		std::cout << " --> B --> x1 = " << x1 << std::endl;
		t2 = std::chrono::system_clock::now();
		time_span = duration_cast<std::chrono::duration<double>>(t2 - t1);
		std::cout << " --> B --> time used = " << time_span.count() << std::endl;

		// timing C

		t1 = std::chrono::system_clock::now();

		for (lli i = 0; i != DIM; i++)
		{
			pc1->fun();
			x1 = pc1->get();
		}

		std::cout << " --> C --> x1 = " << x1 << std::endl;
		t2 = std::chrono::system_clock::now();
		time_span = duration_cast<std::chrono::duration<double>>(t2 - t1);
		std::cout << " --> C --> time used = " << time_span.count() << std::endl;

		// timing D

		t1 = std::chrono::system_clock::now();

		for (lli i = 0; i != DIM; i++)
		{
			pd1->fun();
			x1 = pd1->get();
		}

		std::cout << " --> D --> x1 = " << x1 << std::endl;
		t2 = std::chrono::system_clock::now();
		time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
		std::cout << " --> D --> time used = " << time_span.count() << std::endl;

		// de-allocations

		std::cout << " --> delete pa1;" << std::endl;
		delete pa1;
		std::cout << " --> delete pb1;" << std::endl;
		delete pb1;
		std::cout << " --> delete pc1;" << std::endl;
		delete pc1;
		std::cout << " --> delete pd1;" << std::endl;
		delete pd1;

		std::cout << " --> exit --> 1" << std::endl;
	}

	std::cout << " --> NON-POLYMORPHIC CALL SPEED TEST ----------------------------> 2" << std::endl;

	{
		std::cout << " --> NONPOLYM::A * pa1 = new NONPOLYM::A;" << std::endl;
		auto pa1 = new NONPOLYM::A;
		std::cout << " --> NONPOLYM::A * pb1 = new NONPOLYM::B;" << std::endl;
		NONPOLYM::A* pb1 = new NONPOLYM::B;
		std::cout << " --> NONPOLYM::A * pc1 = new NONPOLYM::C;" << std::endl;
		NONPOLYM::A* pc1 = new NONPOLYM::C;
		std::cout << " --> NONPOLYM::A * pd1 = new NONPOLYM::D;" << std::endl;
		NONPOLYM::A* pd1 = new NONPOLYM::D;

		// timing A

		t1 = std::chrono::system_clock::now();

		for (lli i = 0; i != DIM; i++)
		{
			pa1->fun();
			x1 = pa1->get();
		}

		std::cout << " --> A --> x1 = " << x1 << std::endl;
		t2 = std::chrono::system_clock::now();
		time_span = duration_cast<std::chrono::duration<double>>(t2 - t1);
		std::cout << " --> A --> time used = " << time_span.count() << std::endl;

		// timing B

		t1 = std::chrono::system_clock::now();

		for (lli i = 0; i != DIM; i++)
		{
			pb1->fun();
			x1 = pb1->get();
		}

		std::cout << " --> B --> x1 = " << x1 << std::endl;
		t2 = std::chrono::system_clock::now();
		time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
		std::cout << " --> B --> time used = " << time_span.count() << std::endl;

		// timing C

		t1 = std::chrono::system_clock::now();

		for (lli i = 0; i != DIM; i++)
		{
			pc1->fun();
			x1 = pc1->get();
		}

		std::cout << " --> C --> x1 = " << x1 << std::endl;
		t2 = std::chrono::system_clock::now();
		time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
		std::cout << " --> C --> time used = " << time_span.count() << std::endl;

		// timing D

		t1 = std::chrono::system_clock::now();

		for (lli i = 0; i != DIM; i++)
		{
			pd1->fun();
			x1 = pd1->get();
		}

		std::cout << " --> D --> x1 = " << x1 << std::endl;
		t2 = std::chrono::system_clock::now();
		time_span = duration_cast<std::chrono::duration<double>>(t2 - t1);
		std::cout << " --> D --> time used = " << time_span.count() << std::endl;

		// de-allocations

		std::cout << " --> delete pa1;" << std::endl;
		delete pa1;
		std::cout << " --> delete pb1;" << std::endl;
		delete pb1;
		std::cout << " --> delete pc1;" << std::endl;
		delete pc1;
		std::cout << " --> delete pd1;" << std::endl;
		delete pd1;

		std::cout << " --> exit --> 2" << std::endl;
	}

	std::cout << " --> NON-POLYMORPHIC CALL SPEED TEST ----------------------------> 3" << std::endl;

	{
		std::cout << " --> NONPOLYM::A * pa1 = new NONPOLYM::A;" << std::endl;
		auto pa1 = new NONPOLYM::A;
		std::cout << " --> NONPOLYM::B * pb1 = new NONPOLYM::B;" << std::endl;
		auto pb1 = new NONPOLYM::B;
		std::cout << " --> NONPOLYM::C * pc1 = new NONPOLYM::C;" << std::endl;
		auto pc1 = new NONPOLYM::C;
		std::cout << " --> NONPOLYM::B * pd1 = new NONPOLYM::D;" << std::endl;
		auto pd1 = new NONPOLYM::D;

		// timing A

		t1 = std::chrono::system_clock::now();

		for (lli i = 0; i != DIM; i++)
		{
			pa1->fun();
			x1 = pa1->get();
		}

		std::cout << " --> A --> x1 = " << x1 << std::endl;
		t2 = std::chrono::system_clock::now();
		time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
		std::cout << " --> A --> time used = " << time_span.count() << std::endl;

		// timing B

		t1 = std::chrono::system_clock::now();

		for (lli i = 0; i != DIM; i++)
		{
			pb1->fun();
			x1 = pb1->get();
		}

		std::cout << " --> B --> x1 = " << x1 << std::endl;
		t2 = std::chrono::system_clock::now();
		time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
		std::cout << " --> B --> time used = " << time_span.count() << std::endl;

		// timing C

		t1 = std::chrono::system_clock::now();

		for (lli i = 0; i != DIM; i++)
		{
			pc1->fun();
			x1 = pc1->get();
		}

		std::cout << " --> C --> x1 = " << x1 << std::endl;
		t2 = std::chrono::system_clock::now();
		time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
		std::cout << " --> C --> time used = " << time_span.count() << std::endl;

		// timing D

		t1 = std::chrono::system_clock::now();

		for (lli i = 0; i != DIM; i++)
		{
			pd1->fun();
			x1 = pd1->get();
		}

		std::cout << " --> D --> x1 = " << x1 << std::endl;
		t2 = std::chrono::system_clock::now();
		time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
		std::cout << " --> D --> time used = " << time_span.count() << std::endl;

		// de-allocations

		std::cout << " --> delete pa1;" << std::endl;
		delete pa1;
		std::cout << " --> delete pb1;" << std::endl;
		delete pb1;
		std::cout << " --> delete pc1;" << std::endl;
		delete pc1;
		std::cout << " --> delete pd1;" << std::endl;
		delete pd1;

		std::cout << " --> exit --> 3" << std::endl;
	}
}
