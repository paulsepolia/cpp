#pragma once
#include <iostream>

namespace NONPOLYM
{
	class A
	{
	 public:

		A()
		{
			std::cout << " --> constructor --> A" << std::endl;
		}

		void fun()
		{
			sumA += 1.0;
		}

		double get()
		{
			return sumA;
		}

		~A()
		{
			std::cout << " --> destructor --> ~A" << std::endl;
		}

	 private:

		static double sumA;
	};

	double A::sumA = 0;

	class B : public A
	{
	 public:

		B()
		{
			std::cout << " --> constructor --> B" << std::endl;
		}

		void fun()
		{
			sumB += 1.0;
		}

		double get()
		{
			return sumB;
		}

		~B()
		{
			std::cout << " --> destructor --> ~B" << std::endl;
		}

	 private:

		static double sumB;
	};

	double B::sumB = 0;

	class C : public B
	{
	 public:

		C()
		{
			std::cout << " --> constructor --> C" << std::endl;
		}

		void fun()
		{
			sumC += 1.0;
		}

		double get()
		{
			return sumC;
		}

		~C()
		{
			std::cout << " --> destructor --> ~C" << std::endl;
		}

	 private:

		static double sumC;
	};

	double C::sumC = 0;

	class D : public C
	{
	 public:

		D()
		{
			std::cout << " --> constructor --> D" << std::endl;
		}

		void fun()
		{
			sumD += 1.0;
		}

		double get()
		{
			return sumD;
		}

		~D()
		{
			std::cout << " --> destructor --> ~D" << std::endl;
		}

	 private:

		static double sumD;
	};

	double D::sumD = 0;
}
