#pragma once
#include <iostream>

namespace POLYM
{
	class A
	{
	 public:

		A()
		{
			std::cout << " --> constructor --> A" << std::endl;
		}

		virtual void fun()
		{
			sumA += 1.0;
		}

		virtual double get()
		{
			return sumA;
		}

		virtual ~A()
		{
			std::cout << " --> destructor --> ~A" << std::endl;
		}

	 private:

		static double sumA;
	};

	double A::sumA = 0;

	class B : public A
	{
	 public:

		B()
		{
			std::cout << " --> constructor --> B" << std::endl;
		}

		void fun() override
		{
			sumB += 1.0;
		}

		double get() override
		{
			return sumB;
		}

		~B() override
		{
			std::cout << " --> destructor --> ~B" << std::endl;
		}

	 private:

		static double sumB;
	};

	double B::sumB = 0;

	class C : public B
	{
	 public:

		C()
		{
			std::cout << " --> constructor --> C" << std::endl;
		}

		void fun() override
		{
			sumC += 1.0;
		}

		double get() override
		{
			return sumC;
		}

		~C() override
		{
			std::cout << " --> destructor --> ~C" << std::endl;
		}

	 private:

		static double sumC;
	};

	double C::sumC = 0;

	class D : public C
	{
	 public:

		D()
		{
			std::cout << " --> constructor --> D" << std::endl;
		}

		void fun() override
		{
			sumD += 1.0;
		}

		double get() override
		{
			return sumD;
		}

		~D() override
		{
			std::cout << " --> destructor --> ~D" << std::endl;
		}

	 private:

		static double sumD;
	};

	double D::sumD = 0;
}