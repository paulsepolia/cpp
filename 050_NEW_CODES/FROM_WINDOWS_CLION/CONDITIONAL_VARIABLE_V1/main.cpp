#include <iostream>
#include <thread>
#include <mutex>
#include <condition_variable>

std::mutex mtx;
std::condition_variable cv;
bool ready = false;
const size_t DO_MAX = 1000;

static int i = 0;

void print_id()
{
	while(true)
	{
		std::unique_lock<std::mutex> lck(mtx);

		cv.wait(lck,
			[]
			{ return ready; });

		i++;
		std::cout << "pgg -->> i -->> " << i << std::endl;
		ready = false;

		if(i == DO_MAX) break;
	}
}

void go()
{
	while(true)
	{
		std::this_thread::sleep_for(std::chrono::seconds(1));
		std::unique_lock<std::mutex> lck(mtx);
		ready = true;
		cv.notify_one();

		if(i == DO_MAX) break;
	}
}

int main()
{
	auto th1 = std::thread(print_id);
	auto th2 = std::thread(go);

	go();

	th1.join();
	th2.join();
}
