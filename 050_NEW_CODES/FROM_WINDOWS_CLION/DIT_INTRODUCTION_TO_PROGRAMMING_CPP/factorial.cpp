#include <iostream>
#include <iomanip>

constexpr size_t MAX_FACTORIAL_SIZE_T = 20;
constexpr size_t MAX_FACTORIAL_DBL = 170; // max for double type
constexpr size_t MAX_FACTORIAL_LONG_DBL = 200; // max value 170 for some compilers
constexpr size_t MAX_FACTORIAL_LONG_DBL_GCC = 1760; // max value 170 for some compilers
constexpr size_t SETW1 = 5;
constexpr size_t SETW2 = 30;

template<typename T>
T factorial(T n)
{
    T f = 1;
    auto n_loc = (size_t) (n);
    for (; n_loc > 0; n_loc--)
    {
        f *= n_loc;
    }
    return f;
}

int main()
{
    std::cout << std::setprecision(15);
    {
        std::cout << "-->> EXAMPLE # 1 -->> double factorial(double n) - (double) size_t factorial(size_t n)"
                  << std::endl;
        for (size_t n = 1; n <= MAX_FACTORIAL_SIZE_T; n++)
        {
            const auto diff = factorial((double) n) - (double) factorial((size_t) n);
            if (diff != 0)
            {
                std::cout << std::setw(SETW1) << n << " -->> " << std::setw(SETW2) << diff << std::endl;
            }
        }
    }
    {
        std::cout << "-->> EXAMPLE # 2 -->> (size_t) double factorial(double n) - size_t factorial(size_t n)"
                  << std::endl;
        for (size_t n = 1; n <= MAX_FACTORIAL_SIZE_T; n++)
        {
            const auto diff = (size_t) factorial((double) n) - factorial((size_t) n);
            if (diff != 0)
            {
                std::cout << std::setw(SETW1) << n << " -->> " << std::setw(SETW2) << diff << std::endl;
            }
        }
    }
    {
        std::cout << "-->> EXAMPLE # 3 -->> size_t factorial(size_t n)" << std::endl;
        for (size_t n = 1; n <= MAX_FACTORIAL_SIZE_T; n++)
        {
            std::cout << std::setw(SETW1) << n << " -->> " << std::setw(SETW2) << factorial((size_t) n) << std::endl;
        }
    }
    {
        std::cout << "-->> EXAMPLE # 4 -->> double factorial(double n)" << std::endl;
        for (size_t n = 1; n <= MAX_FACTORIAL_DBL; n++)
        {
            std::cout << std::setw(SETW1) << n << " -->> " << std::setw(SETW2) << factorial((double) n) << std::endl;
        }
    }
    {
        std::cout << "-->> EXAMPLE # 5 -->> long double factorial(long double n)" << std::endl;
        for (size_t n = 1; n <= MAX_FACTORIAL_LONG_DBL; n++)
        {
            std::cout << std::setw(SETW1) << n << " -->> " << std::setw(SETW2) << factorial((long double) n)
                      << std::endl;
        }
    }
    {
        std::cout << "-->> EXAMPLE # 6 -->> long double factorial(long double n)" << std::endl;
        for (size_t n = 1; n <= MAX_FACTORIAL_LONG_DBL_GCC; n++)
        {
            std::cout << std::setw(SETW1) << n << " -->> " << std::setw(SETW2) << factorial((long double) n)
                      << std::endl;
        }
    }
}
