#include <iostream>
#include <iomanip>
#include <chrono>

class BenchmarkTimer final
{
public:

    explicit BenchmarkTimer() :
            m_t_now(std::chrono::steady_clock::now()),
            m_t_prev(std::chrono::steady_clock::now())
    {
    }

    [[maybe_unused]] void PrintTime() const
    {
        const auto t_loc = std::chrono::steady_clock::now();
        const auto total_time = std::chrono::duration_cast<std::chrono::duration<double>>(t_loc - m_t_prev).count();
        m_t_prev = t_loc;
        std::cout.precision(10);
        std::cout << std::fixed;
        std::cout << "-->> time used up to now (since last reset) (secs) = " << total_time << std::endl;
    }

    [[maybe_unused]] void ResetTimer()
    {
        const auto t_loc = std::chrono::steady_clock::now();
        m_t_prev = t_loc;
        m_t_now = t_loc;
    }

    ~BenchmarkTimer()
    {
        m_t_prev = m_t_now;
        m_t_now = std::chrono::steady_clock::now();
        const auto total_time = std::chrono::duration_cast<std::chrono::duration<double>>(m_t_now - m_t_prev).count();
        std::cout.precision(10);
        std::cout << std::fixed;
        std::cout << "-->> total time used until exit of the scope (since last reset) (secs) = " << total_time
                  << std::endl;
    }

private:

    decltype(std::chrono::steady_clock::now()) m_t_now;
    mutable decltype(std::chrono::steady_clock::now()) m_t_prev;
};

// Compute factorials up to MAXN

constexpr size_t MAX_FACT_INT = 12;
constexpr size_t MAX_FACT_SIZE_T = 24; // MAX 21
constexpr size_t MAX_FACT_DBL = 170; // MAX 170
constexpr size_t MAX_FACT_LONG_DBL = 175; // MAX 170 for some compilers
constexpr size_t MAX_FACT_LONG_DBL_GCC = 1754; // max value 1754 for GCC compilers
constexpr size_t SETW1 = 5;
constexpr size_t SETW2 = 30;
constexpr size_t MAX_FACTORS = 10000;
constexpr int FLAG_VALUE = -1;

template<typename T>
T rec_fact(T n)
{
    // If n == 0
    if (!n)
    {
        return 1;
    }
    else
    {
        // 0! = 1
        // n! = n * (n-1)!
        return n * rec_fact(n - 1);
    }
}

template<typename T>
T rec_fact_mem(T n, T *rec_fact_matrix)
{
    // If n == 0
    if (!n)
    {
        return 1;
    }
    else
    {
        // 0! = 1
        // n! = n * (n-1)!
        if (rec_fact_matrix[(size_t) (n - 1)] == -1)
        {
            rec_fact_matrix[(size_t) (n - 1)] = rec_fact(n - 1);
        }
        return n * rec_fact_matrix[(size_t) (n - 1)];
    }
}

int main()
{
    std::cout << std::setprecision(20);
    std::cout << std::scientific;

    {
        std::cout << "-->> Example # 1 -->> int rec_fact(int n)" << std::endl;
        for (int n = 0; n <= MAX_FACT_INT; n++)
        {
            std::cout << std::setw(SETW1) << n << " -->> " << std::setw(SETW2) << rec_fact(n) << std::endl;
        }
    }
    {
        std::cout << "-->> Example # 2 -->> size_t rec_fact(size_t n)" << std::endl;
        for (size_t n = 0; n <= MAX_FACT_SIZE_T; n++)
        {
            std::cout << std::setw(SETW1) << n << " -->> " << std::setw(SETW2) << rec_fact(n) << std::endl;
        }
    }
    {
        std::cout << "-->> Example # 3 -->> double rec_fact(double n)" << std::endl;
        for (size_t n = 0; n <= MAX_FACT_DBL; n++)
        {
            std::cout << std::setw(SETW1) << n << " -->> " << std::setw(SETW2)
                      << rec_fact((double) n) << std::endl;
        }
    }
    {
        std::cout << "-->> Example # 4 -->> long double rec_fact(long double n)" << std::endl;
        for (size_t n = 0; n <= MAX_FACT_LONG_DBL; n++)
        {
            std::cout << std::setw(SETW1) << n << " -->> " << std::setw(SETW2)
                      << rec_fact((long double) n) << std::endl;
        }
    }
    {
        BenchmarkTimer bt;
        std::cout << "-->> Example # 5 -->> long double rec_fact(long double n)" << std::endl;
        for (size_t n = 0; n <= MAX_FACT_LONG_DBL_GCC; n++)
        {
            std::cout << std::setw(SETW1) << n << " -->> " << std::setw(SETW2)
                      << rec_fact((long double) n) << std::endl;
        }
    }
    {
        auto *rec_fact_matrix = new long double[MAX_FACTORS];
        for (size_t i = 0; i < MAX_FACTORS; i++)
        {
            rec_fact_matrix[i] = FLAG_VALUE;
        }

        std::cout << std::setprecision(20);
        std::cout << std::scientific;

        std::cout << "-->> Example # 6 -->> long double rec_fact_mem(long double n)" << std::endl;
        BenchmarkTimer bt;
        for (size_t n = 0; n <= MAX_FACT_LONG_DBL_GCC; n++)
        {
            std::cout << std::setw(SETW1) << n << " -->> " << std::setw(SETW2)
                      << rec_fact_mem((long double) n, rec_fact_matrix) << std::endl;
        }

        delete[] rec_fact_matrix;
    }
    {
        auto *rec_fact_matrix = new long double[MAX_FACTORS];
        for (size_t i = 0; i < MAX_FACTORS; i++)
        {
            rec_fact_matrix[i] = FLAG_VALUE;
        }

        std::cout << std::setprecision(20);
        std::cout << std::scientific;

        std::cout << "-->> Example # 7 -->> long double rec_fact_mem(long double n)" << std::endl;
        BenchmarkTimer bt;
        for (int n = MAX_FACT_LONG_DBL_GCC; n >= 0; n--)
        {
            std::cout << std::setw(SETW1) << n << " -->> " << std::setw(SETW2)
                      << rec_fact_mem((long double) n, rec_fact_matrix) << std::endl;
        }

        delete[] rec_fact_matrix;
    }
    {
        auto *rec_fact_matrix_a = new long double[MAX_FACTORS];
        for (size_t i = 0; i < MAX_FACTORS; i++)
        {
            rec_fact_matrix_a[i] = FLAG_VALUE;
        }

        auto *rec_fact_matrix_b = new long double[MAX_FACTORS];
        for (size_t i = 0; i < MAX_FACTORS; i++)
        {
            rec_fact_matrix_b[i] = FLAG_VALUE - 1;
        }

        std::cout << std::setprecision(20);
        std::cout << std::scientific;

        std::cout << "-->> Example # 8 -->> test" << std::endl;

        // run #1 -->> only last term given as input and the rest are evaluated
        rec_fact_mem((long double) MAX_FACT_LONG_DBL_GCC, rec_fact_matrix_a);

        // run #2 -->> build the result matrix elements from i = 0
        for (size_t n = 0; n <= MAX_FACT_LONG_DBL_GCC; n++)
        {
            rec_fact_mem((long double) n, rec_fact_matrix_b);
        }

        // test here
        for (size_t n = 0; n <= MAX_FACT_LONG_DBL_GCC; n++)
        {
            if (rec_fact_matrix_a[n] == rec_fact_matrix_b[n])
            {
                std::cout << "ERROR with " << n << " -->> "
                          << rec_fact_matrix_a[n] << " <<-->> "
                          << rec_fact_matrix_b[n] << " <<-->> "
                          << std::abs(rec_fact_matrix_a[n] - rec_fact_matrix_b[n]) << std::endl;
                std::terminate();
            }
        }

        delete[] rec_fact_matrix_a;
        delete[] rec_fact_matrix_b;
    }
    return 0;
}
