#include <cstdio>
#include <cstdlib>

typedef struct list_node *Listptr;

struct list_node
{
    int value;
    Listptr next;
};

int empty_fun(Listptr);

int in(Listptr, int);

int n_th(Listptr, int, int *);

void insert_at_start(Listptr *, int);

void insert_at_end(Listptr *, int);

int delete_fun(Listptr *, int);

void print_fun(Listptr);

int main()
{
    Listptr alist;
    int v;
    // List is NULL
    alist = nullptr;
    // Check if list is empty
    printf("List is%s empty\n", empty_fun(alist) ? "" : " not");

    // List is 44 --> NULL
    insert_at_start(&alist, 44);
    printf("List is ");
    print_fun(alist);
    // List is 44 --> 55 --> NULL
    insert_at_end(&alist, 55);
    printf("List is ");
    print_fun(alist);
    // List is 33 --> 44 --> 55 --> NULL
    insert_at_start(&alist, 33);
    printf("List is ");
    print_fun(alist);
    // List is 33 --> 44 --> 55 --> 66 --> NULL
    insert_at_end(&alist, 66);
    printf("List is ");
    print_fun(alist);
    // Check if list is empty
    printf("List is%s empty\n", empty_fun(alist) ? "" : " not");
    // Check membership
    printf("55 is%s in list\n", in(alist, 55) ? "" : " not");
    printf("77 is%s in list\n", in(alist, 77) ? "" : " not");
    if (n_th(alist, 2, &v))
    {
        // Return 2nd element
        printf("Item no 2 is %d\n", v);
    }
    else
    {
        printf("Item no 2 does not exist\n");
    }
    if (n_th(alist, 6, &v))
    {
        // Return 6th element
        printf("Item no 6 is %d\n", v);
    }
    else
    {
        printf("Item no 6 does not exist\n");
    }
    printf("Deleting 55. %s\n", delete_fun(&alist, 55) ? "OK!" : "Failed!");
    // List is 33 --> 44 --> 66 --> NULL
    printf("List is ");
    print_fun(alist);
    printf("Deleting 22. %s\n", delete_fun(&alist, 22) ? "OK!" : "Failed!");
    // List is 33 --> 44 --> 66 --> NULL
    printf("List is ");
    print_fun(alist);
    printf("Deleting 33. %s\n", delete_fun(&alist, 33) ? "OK!" : "Failed!");
    // List is 44 --> 66 --> NULL
    printf("List is ");
    print_fun(alist);
    return 0;
}

// Check if list is empty
int empty_fun(Listptr list)
{
    // Is it really empty?
    if (list == nullptr)
    {
        // Yes, it is
        return 1;
    }
    // No, it isn't
    return 0;
}

// Check if v is member of list
int in(Listptr list, int v)
{
    // Visit list elements up to the end
    while (list != nullptr)
    {
        if (list->value == v) // Did we find what we are looking for?
        {
            return 1; // Yes, we did
        }
        else
        {
            list = list->next;  // No, go to next element
        }
    }
    return 0; // Finally, v is not in list
}

// Return n-th element of list, if it exists, into vaddr
int n_th(Listptr list, int n, int *vaddr)
{
    // Maybe search up to the end of the list
    while (list != nullptr)
    {
        // Did we reach the right element?
        if (n-- == 1)
        {
            *vaddr = list->value; // Yes, return it
            return 1;             // We found it
        }
        else
        {
            list = list->next;  // No, go to next element
        }
    }
    return 0;  // Sorry, list is too short
}

// Insert v as first element of list *ptraddr
void insert_at_start(Listptr *ptraddr, int v)
{
    Listptr templist;
    templist = *ptraddr; // Save current start of list
    *ptraddr = (Listptr) malloc(sizeof(struct list_node)); // Space for new node
    (*ptraddr)->value = v;        // Put value
    (*ptraddr)->next = templist;  // Next element is former first
}

// Insert v as last element of list *ptraddr
void insert_at_end(Listptr *ptraddr, int v)
{
    // Go to end of list
    while (*ptraddr != nullptr)
    {
        ptraddr = &((*ptraddr)->next); // Prepare what we need to change
    }
    *ptraddr = (Listptr) malloc(sizeof(struct list_node));  // Space for new node
    (*ptraddr)->value = v;                                  // Put value
    (*ptraddr)->next = nullptr;                             // There is no next element
}

// Delete element v from list *ptraddr, if it exists
int delete_fun(Listptr *ptraddr, int v)
{
    Listptr templist;

    // Visit list elements up to the end
    while ((*ptraddr) != nullptr)
    {
        if ((*ptraddr)->value == v)
        {   // Did we find what to delete?
            templist = *ptraddr;                // Yes, save address of its node
            *ptraddr = (*ptraddr)->next;        // Bypass deleted element
            free(templist);                     // Free memory for the corresponding node
            return 1;                           // We deleted the element
        }
        else
        {
            // Prepare what we might change
            ptraddr = &((*ptraddr)->next);
        }
    }

    // We didn't find the element we were looking for
    return 0;
}

// Print elements of list
void print_fun(Listptr list)
{
    // Visit list elements up to the end
    while (list != nullptr)
    {
        printf("%d --> ", list->value);   // Print current element
        list = list->next;               // Go to next element
    }
    printf("NULL\n");                    // Print end of list
}
