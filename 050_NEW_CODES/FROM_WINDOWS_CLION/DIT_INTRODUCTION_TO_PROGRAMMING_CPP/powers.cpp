#include <iostream>
#include <iomanip>
#include <cmath>

constexpr size_t MAX_BASE_SIZE_T = 10;
constexpr size_t MAX_EXPONENT_SIZE_T = 15;
constexpr size_t MAX_BASE_DBL = 8;
constexpr size_t MAX_EXPONENT_DBL = 20;
constexpr size_t MAX_BASE_SIZE_TEST1 = 12;
constexpr size_t MAX_EXPONENT_SIZE_TEST1 = 14;
constexpr size_t MAX_BASE_SIZE_TEST2 = 20;
constexpr size_t MAX_EXPONENT_SIZE_TEST2 = 20;
constexpr size_t SETW1 = 4;
constexpr size_t SETW2 = 50;

template<typename T>
T power(T base, T exponent)
{
    T p = 1;
    int exp_loc = exponent;
    for (; exp_loc > 0; exp_loc--)
    {
        p *= base;
    }
    return p;
}

int main()
{
    std::cout << std::setprecision(30);

    {
        std::cout << "-->> Example # 1 -->> size_t = power(size_t, size_t);" << std::endl;
        for (size_t m = 2; m <= MAX_BASE_SIZE_T; m++)
        {
            std::cout << "--------------------------------->> base = " << m << std::endl;
            for (size_t n = 2; n <= MAX_EXPONENT_SIZE_T; n++)
            {
                size_t p = power(m, n);
                std::cout << std::setw(SETW1) << m << " ^ "
                          << std::setw(SETW1) << n << " = " << std::setw(SETW2) << p << std::endl;
            }
        }
    }
    {
        std::cout << "-->> Example # 2 -->> double = power(double, double);" << std::endl;
        for (size_t m = 2; m <= MAX_BASE_DBL; m++)
        {
            std::cout << "--------------------------------->> base = " << m << std::endl;
            for (size_t n = 2; n <= MAX_EXPONENT_DBL; n++)
            {
                auto p = power((double) m, (double) n);
                std::cout << std::setw(SETW1) << m << " ^ "
                          << std::setw(SETW1) << n << " = " << std::setw(SETW2) << p << std::endl;
            }
        }
    }
    {
        std::cout << "-->> Example # 3 -->> power(double, double) - power(size_t, size_t);" << std::endl;
        for (size_t m = 2; m <= MAX_BASE_SIZE_TEST1; m++)
        {
            for (size_t n = 2; n <= MAX_EXPONENT_SIZE_TEST1; n++)
            {
                auto diff = power((double) m, (double) n) - (double) power((size_t) m, (size_t) n);
                if (diff != 0)
                {
                    std::cout << "-->> ERROR for " << m << " -->> " << n << std::endl;
                    std::terminate();
                }
            }
            std::cout << "-->> All okay with base " << m << " and max exponent " << MAX_EXPONENT_SIZE_TEST1
                      << std::endl;
        }
    }
    {
        std::cout << "-->> Example # 4 -->> power(double, double) - std::pow(double, double);" << std::endl;
        for (size_t m = 2; m <= MAX_BASE_SIZE_TEST2; m++)
        {
            for (size_t n = 2; n <= MAX_EXPONENT_SIZE_TEST2; n++)
            {
                const auto res1 = std::pow((long double) m, (long double) n);
                const auto res2 = power((long double) m, (long double) n);
                auto diff = std::abs(res1 - res2);
                if (diff != 0)
                {
                    std::cout << "-->> res1 = " << res1 << std::endl;
                    std::cout << "-->> res2 = " << res2 << std::endl;
                    std::cout << "-->> ERROR, diff is: " << diff << ", for base: " << m << " and exponent: " << n
                              << std::endl;
                }
            }
            std::cout << "-->> All okay with base " << m << " and max exponent " << MAX_EXPONENT_SIZE_TEST2
                      << std::endl;
        }
    }
}
