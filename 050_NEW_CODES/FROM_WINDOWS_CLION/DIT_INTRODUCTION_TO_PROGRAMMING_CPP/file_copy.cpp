#include <cstdio>
#include <iostream>
#include <chrono>
#include <fstream>

class BenchmarkTimer final
{
public:

    explicit BenchmarkTimer() :
            m_t_now(std::chrono::steady_clock::now()),
            m_t_prev(std::chrono::steady_clock::now())
    {
    }

    [[maybe_unused]] void PrintTime() const
    {
        const auto t_loc = std::chrono::steady_clock::now();
        const auto total_time = std::chrono::duration_cast<std::chrono::duration<double>>(t_loc - m_t_prev).count();
        m_t_prev = t_loc;
        std::cout.precision(10);
        std::cout << std::fixed;
        std::cout << "-->> time used up to now (since last reset) (secs) = " << total_time << std::endl;
    }

    [[maybe_unused]] void ResetTimer()
    {
        const auto t_loc = std::chrono::steady_clock::now();
        m_t_prev = t_loc;
        m_t_now = t_loc;
    }

    ~BenchmarkTimer()
    {
        m_t_prev = m_t_now;
        m_t_now = std::chrono::steady_clock::now();
        const auto total_time = std::chrono::duration_cast<std::chrono::duration<double>>(m_t_now - m_t_prev).count();
        std::cout.precision(10);
        std::cout << std::fixed;
        std::cout << "-->> total time used until exit of the scope (since last reset) (secs) = " << total_time
                  << std::endl;
    }

private:

    decltype(std::chrono::steady_clock::now()) m_t_now;
    mutable decltype(std::chrono::steady_clock::now()) m_t_prev;
};

int file_copy_c_style(const char *file_read,
                      const char *file_write,
                      size_t buffer_size)
{
    size_t n;
    char *buf = (char *) malloc(sizeof(char) * buffer_size);

    const auto ifp = fopen(file_read, "rb");
    if (ifp == nullptr)
    {
        perror("fopen source-file");
        return -1;
    }

    const auto ofp = fopen(file_write, "wb");
    if (ofp == nullptr)
    {
        perror("fopen target-file");
        return -2;
    }

    while (!feof(ifp))
    {
        // While we don't reach the end of source
        // Read characters from source file to fill buffer
        n = fread(buf, sizeof(char), sizeof(buf), ifp);

        // Write characters read to target file
        fwrite(buf, sizeof(char), n, ofp);
    }

    fclose(ifp);
    fclose(ofp);
    free(buf);
    return 0;
}

int file_copy_cpp_style(const char *file_read,
                        const char *file_write,
                        size_t buffer_size)
{
    // Open the file for reading
    std::ifstream in;
    in.open(file_read, std::ios::binary | std::ios::in);
    if (!in.is_open())
    {
        return -1;
    }
    // Open file for writing
    std::ofstream out;
    out.open(file_write, std::ios::binary | std::ios::out | std::ios::trunc);
    if (!out.is_open())
    {
        return -2;
    }
    // Allocate the buffer
    auto buffer = new char[buffer_size];

    // 'read' and then 'write' here
    while (true)
    {
        in.read((char *) (buffer), (long) (buffer_size * sizeof(char)));

        if (in.eof())
        {
            buffer_size = in.gcount();
        }
        out.write((char *) (buffer), (long) (buffer_size * sizeof(char)));
        out.flush();
        if (in.eof()) break;
    }

    in.close();
    out.close();
    delete[] buffer;
    return 0;
}

constexpr size_t BUFFER_SIZES[]{128, 256, 512,
                                1 * 1024, 10 * 1024,
                                100 * 1024, 1000 * 1024};

int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        std::cout << "Usage: " << argv[0] << "<source-file> <target-file>" << std::endl;
        return 1;
    }

    {
        std::cout << "----------------->> C-STYLE File Write Benchmark" << std::endl;
        for (const auto &buffer_size: BUFFER_SIZES)
        {
            static int i = 0;
            i++;
            std::cout << "-->> Example using buffer size: " << buffer_size << std::endl;
            std::cout << "-->> File copy: int file_copy_c_style(const char*, const char*, buffer_size)" << std::endl;
            BenchmarkTimer bt;
            std::string file_out = argv[2] + std::string("__c__") + std::to_string(i);
            const auto res = file_copy_c_style(argv[1], file_out.c_str(), buffer_size);
            if (res != 0)
            {
                std::cout << "ERROR while copying the file." << std::endl;
                std::terminate();
            }
        }
    }
    {
        std::cout << "----------------->> C++ File Write Benchmark" << std::endl;
        for (const auto &buffer_size: BUFFER_SIZES)
        {
            static int i = 0;
            i++;
            std::cout << "-->> Example using buffer size: " << buffer_size << std::endl;
            std::cout << "-->> File copy: int file_copy_c_style(const char*, const char*, buffer_size)" << std::endl;
            BenchmarkTimer bt;
            const std::string file_out = argv[2] + std::string("__cpp__") + std::to_string(i);
            const auto res = file_copy_cpp_style(argv[1], file_out.c_str(), buffer_size);
            if (res != 0)
            {
                std::cout << "ERROR while copying the file." << std::endl;
                std::terminate();
            }
        }
    }
}
