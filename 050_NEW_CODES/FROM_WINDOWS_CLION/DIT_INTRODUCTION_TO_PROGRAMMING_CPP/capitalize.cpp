#include <cstdio>

int main()
{
    // Be careful! Declare ch as int because of getchar() and EOF
    int ch;
    // Read first character
    ch = getchar();
    while (ch != EOF) // Go on if we didn't reach end of file
    {
        if (ch >= 'a' && ch <= 'z')
        {
            ch = ch - ('a' - 'A'); // Move 'a' - 'A' positions in the ASCII table
        }
        putchar(ch);       // Print out character
        ch = getchar();            // Read next character
    }
}
