#include <iostream>

constexpr size_t START_YEAR = 2010;
constexpr size_t END_YEAR = 2100;

//=================================================================//
// Gauss method for Easter date computation for Gregorian Calendar //
//=================================================================//

int main()
{
    int year, a, b, c, d, e;

    for (year = START_YEAR; year <= END_YEAR; year++)
    {
        a = year % 19;
        b = year % 4;
        c = year % 7;
        d = (19 * a + 15) % 30;
        e = (2 * b + 4 * c + 6 * d + 6) % 7;

        std::cout << "-->> Easter in the year: " << year;

        if (d + e + 4 > 30)
        {
            // Easter in May.
            std::cout << ", May " << (d + e - 26) << std::endl;
        }
        else
        {
            // Easter in April.
            std::cout << ", April " << (d + e + 4) << std::endl;
        }
    }
}
