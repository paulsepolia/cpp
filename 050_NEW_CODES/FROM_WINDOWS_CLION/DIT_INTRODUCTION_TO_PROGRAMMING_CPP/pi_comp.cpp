#include <iostream>
#include <cmath>
#include <iomanip>

constexpr double THRESHOLDS[]{1.0e-13, 1.0e-14, 1.0e-15,
                              1.0e-16, 1.0e-17, 1.0e-18,
                              1.0e-19, 1.0e-20, 1.0e-21};

double pi_fun(double threshold, size_t &i)
{
    i = 1; // Denominator of current term
    double sum = 0; // So far sum
    double current;
    double pi;
    do
    {
        current = 1.0 / (((double) i) * ((double) i)); // Current term
        sum += current; // Add current term to sum
        i++; // Next term now
    } while (current > threshold);  // Stop if current term is very small

    pi = std::sqrt(6 * sum);        // Compute approximation of pi

    return pi;
}

int main()
{
    std::cout << std::setprecision(25);

    for (const double &threshold: THRESHOLDS)
    {
        std::cout << "-->> Example -->> Pi calc for threshold " << threshold << std::endl;
        size_t i = 0;
        const auto pi = pi_fun(threshold, i);
        std::cout << "-->> Summed " << std::setw(18) << (i - 1) << " terms, pi = " << std::setw(35) << pi << std::endl;
    }
}
