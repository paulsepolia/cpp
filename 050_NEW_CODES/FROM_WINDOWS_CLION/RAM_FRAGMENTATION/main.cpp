#include <iostream>
#include <cmath>
#include <chrono>
#include <queue>
#include <random>
#include <unordered_set>
#include <algorithm>
#include <thread>
#include <mutex>
#include <string>

class benchmark_timer
{
public:

	explicit benchmark_timer() :
			_res(0.0),
			t1{ std::chrono::steady_clock::now() },
			t2{ std::chrono::steady_clock::now() },
			t_prev{ std::chrono::steady_clock::now() }
	{
	};

	auto print_time(const std::string& mark_str) const -> void
	{
		const auto t_loc{ std::chrono::steady_clock::now() };
		const auto total_time{
				std::chrono::duration_cast < std::chrono::duration < double >> (t_loc - t_prev).count() };
		t_prev = t_loc;
		std::cout << " --> time used since last reset (secs) at " << mark_str << " = " << total_time << std::endl;
	}

	auto reset_timer() -> void
	{
		const auto t_loc{ std::chrono::steady_clock::now() };
		t_prev = t_loc;
		t1 = t_loc;
		t2 = t_loc;
	}

	~benchmark_timer()
	{
		t2 = std::chrono::steady_clock::now();
		const auto total_time{ std::chrono::duration_cast < std::chrono::duration < double >> (t2 - t1).count() };
		std::cout << " --> res = " << _res << std::endl;
		std::cout << " --> Time to call the destructor - total time used since last reset (secs) = " << total_time
				  << std::endl;
	}

	auto set_res(const double& res) -> void
	{
		_res = res;
	}

private:

	double _res{};

	decltype (std::chrono::steady_clock::now())

	t1{};

	decltype (std::chrono::steady_clock::now())

	t2{};

	mutable decltype (std::chrono::steady_clock::now())

	t_prev{};
};

const auto DO_MAX{ (size_t)std::pow(10.0, 8.0) };
constexpr auto DIM_MAX{ (size_t)1'000'000 };
constexpr auto RECORD_SIZE((size_t)24);

auto mtx{ std::mutex{}};

auto main() -> int
{
	auto lSET{ []()
			   {
				   for (size_t kk = 0; kk < DO_MAX; kk++)
				   {

					   mtx.lock();
					   std::cout << "--------------------->> SET counter = " << kk << std::endl;
					   mtx.unlock();

					   benchmark_timer ot;

					   auto cont{ std::unordered_set<void*>() };

					   for (size_t jj = 0; jj < DIM_MAX; jj++)
					   {
						   void* ptr = malloc(RECORD_SIZE);
						   cont.emplace(ptr);
					   }

					   mtx.lock();
					   ot.print_time(" MARK SET BUILD ");
					   mtx.unlock();

					   std::for_each(cont.begin(), cont.end(), [](void* ptr)
					   { free(ptr); });

					   mtx.lock();
					   ot.print_time(" MARK SET FREE ");
					   mtx.unlock();

					   cont.clear();
					   ot.print_time(" MARK SET CLEAN ");
					   ot.reset_timer();
				   }
			   }};

	auto lVEC{ []()
			   {
				   for (size_t kk = 0; kk < DO_MAX; kk++)
				   {

					   mtx.lock();
					   std::cout << "--------------------->> VEC counter = " << kk << std::endl;
					   mtx.unlock();

					   benchmark_timer ot;

					   auto vec{ std::vector<void*>() };

					   for (size_t jj = 0; jj < DIM_MAX; jj++)
					   {
						   void* ptr = malloc(RECORD_SIZE);
						   vec.push_back(ptr);
					   }

					   mtx.lock();
					   ot.print_time(" MARK VEC BUILD ");
					   mtx.unlock();

					   std::for_each(vec.begin(), vec.end(), [](void* ptr)
					   { free(ptr); });

					   mtx.lock();
					   ot.print_time(" MARK VEC FREE ");
					   mtx.unlock();

					   vec.clear();
					   ot.print_time(" MARK VEC CLEAN ");
					   ot.reset_timer();
				   }
			   }};

	auto t1{ std::thread(lVEC) };
	auto t2{std::thread(lSET)};
	auto t3{std::thread(lVEC)};

	t1.join();
	t2.join();
	t3.join();
}
