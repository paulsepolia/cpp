#include <iostream>
#include <memory>
#include <mutex>
#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <thread>

std::mutex mtx;
const size_t DO_MAX = 90;
const uint32_t NUM_THREADS = 5;
const uint32_t NUM_JOBS = 100;

//======//
// NOTE //
//======//
// The combinations:
// (1) post, poll
// (2) post, run
// (3) dispatch, poll
// (4) dispatch, run
// Should have the same behaviour under the simple program below

// Function which polls for available job

void worker_thread(const std::shared_ptr<boost::asio::io_service>& io_svc)
{
	// blocks here and executes any given work
	// if the is_svc->stop() is called then any execution stops

	//io_svc->poll();
	io_svc->run();
}

// Function which has the main (the only) work load

void sum_fun(const uint32_t& index)
{
	mtx.lock();
	std::cout << "-->> Thread with index -->> " << index << std::endl;
	mtx.unlock();

	volatile size_t sum = 0;

	for (size_t i1 = 1; i1 <= DO_MAX; i1++)
	{
		for (size_t i2 = 1; i2 <= DO_MAX; i2++)
		{
			for (size_t i3 = 1; i3 <= DO_MAX; i3++)
			{
				for (size_t i4 = 1; i4 <= DO_MAX; i4++)
				{
					for (size_t i5 = 1; i5 <= DO_MAX; i5++)
					{
						sum += (i1 + i2 + i3 + i4 + i5);
						sum -= i5;
					}
					sum -= i4;
				}
				sum -= i3;
			}
			sum -= i2;
		}
	}

	mtx.lock();
	std::cout << "-->> Thread with index -->> " << index << " -->> DONE" << std::endl;
	std::cout << "-->> Sum -->> " << sum << std::endl;
	std::cout << "-->> Index -->> " << index << " <==> std::this_thread::get_id() -->> "
			  << std::this_thread::get_id() << std::endl;
	mtx.unlock();
	std::this_thread::sleep_for(std::chrono::seconds(index % 4));
}

int main()
{
	// Create a pointer to the io_service object

	std::shared_ptr<boost::asio::io_service> io_svc(new boost::asio::io_service);

	// Create a pointer to the io_service::work object

	std::shared_ptr<boost::asio::io_service::work> worker(new boost::asio::io_service::work(*io_svc));

	// Create a group of boost threads
	// Do not create more threads than the actual available cores
	// unless you know what you are doing

	boost::thread_group threads;

	for (uint32_t i = 1; i <= NUM_THREADS; i++)
	{
		threads.create_thread([io_svc]
		{ return worker_thread(io_svc); });
	}

	// Give job here to the io_svc object
	// as soon as any thread is available to get some

	for (uint32_t i = 1; i <= NUM_JOBS; i++)
	{
		std::this_thread::sleep_for(std::chrono::seconds(2));
		std::cout << "-->> POSTED JOB ------------------------------->> i = " << i << " / " << NUM_JOBS << std::endl;
		io_svc->post([i]
		{ return sum_fun(i); });
		//io_svc->dispatch([i]
		//{ return sum_fun(i); });
	}

	// We can stop the execution by entering a number
	int sentinel;
	std::cout << "Enter an integer to stop doing job:";
	std::cin >> sentinel;
	io_svc->stop();
	std::cout << "Enter an integer to join the threads and exit:";
	std::cin >> sentinel;
	threads.join_all();
}
