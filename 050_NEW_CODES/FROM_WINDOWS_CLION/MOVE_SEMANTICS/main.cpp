#include <iostream>
#include <memory>
#include <vector>

struct Node
{

	explicit Node(double v) : value(v)
	{
	}

	double value = -1;
	Node* next = nullptr;

	virtual ~Node()
	{
		std::cout << "-->> ~Node() -->> " << value << std::endl;
	}
};

struct List
{

	//=====================//
	// Default constructor //
	//=====================//

	List() = default;

	//==================//
	// Copy constructor //
	//==================//

	List(const List& other)
	{

		List list;
		Node* tmp;

		while (true)
		{
			if (other.head && list.head == nullptr)
			{
				list.Insert(other.head->value);
				tmp = other.head;
			}
			else if (tmp->next)
			{
				list.Insert(tmp->next->value);
				tmp = tmp->next;
			}
			else
			{
				break;
			}
		}

		head = list.head;
		list.head = nullptr;
	}

	//=============================//
	// Copy assignment constructor //
	//=============================//

	List& operator=(const List& other)
	{

		if (&other != this)
		{

			List list;
			Node* tmp;

			while (true)
			{
				if (other.head && list.head == nullptr)
				{
					list.Insert(other.head->value);
					tmp = other.head;
				}
				else if (tmp->next)
				{
					list.Insert(tmp->next->value);
					tmp = tmp->next;
				}
				else
				{
					break;
				}
			}

			head = list.head;
			list.head = nullptr;
		}

		return *this;
	}

	//==================//
	// Move constructor //
	//==================//

	List(List&& other) noexcept
	{
		if (&other != this)
		{
			head = other.head;
			other.head = nullptr;
		}
	}

	//=============================//
	// Move assignment constructor //
	//=============================//

	List& operator=(List&& other) noexcept
	{
		if (&other != this)
		{
			head = other.head;
			other.head = nullptr;
		}
		return *this;
	}

	//============================//
	// Several constructors below //
	//============================//

	List(const std::initializer_list<double>& l)
	{
		for (const auto& el : l)
		{
			Insert(el);
		}
	}

	explicit List(const std::vector<double>& v)
	{
		for (const auto& el : v)
		{
			Insert(el);
		}
	}

	//===========================================//
	// Append list of nodes via initializer list //
	//===========================================//

	void Append(const std::initializer_list<double>& l)
	{
		for (const auto& el : l)
		{
			Insert(el);
		}
	}

	//=================================//
	// Append list of nodes via vector //
	//=================================//

	void Append(const std::vector<double>& l)
	{
		for (const auto& el : l)
		{
			Insert(el);
		}
	}

	//===============//
	// Insert a node //
	//===============//

	void Insert(double v)
	{

		Node* nextNode = new Node(v);

		if (head == nullptr)
		{
			currentNode = nextNode;
			head = nextNode;
		}
		else
		{
			if(currentNode)
			{
				currentNode->next = nextNode;
				currentNode = nextNode;
			}
		}
	}

	//============//
	// Print list //
	//============//

	void Print() const
	{
		Node* tmp;
		tmp = head;

		while (tmp)
		{
			std::cout << "-->> Print() -->> " << tmp->value << std::endl;
			tmp = tmp->next;
		}
	}

	//==========================================//
	// Erase a node in list with specific value //
	//==========================================//

	int EraseOne(double value)
	{

		Node* tmp;
		tmp = head;

		// check the first element

		if (head && head->value == value)
		{
			tmp = head;
			head = head->next;
			delete tmp;
			return 0;
		}

		// check the rest elements

		while (tmp->next)
		{
			if (tmp->next->value == value)
			{
				tmp->next = tmp->next->next;
				return 0;
			}
			else
			{
				tmp = tmp->next;
			}
		}
		return -1;
	}

	//=============================================//
	// Erase all nodes in list with specific value //
	//=============================================//

	int EraseAll(double value)
	{

		int errorCode = 0;

		while (errorCode == 0)
		{
			errorCode = EraseOne(value);
		}

		return errorCode;
	}

	//============//
	// Destructor //
	//============//

	virtual ~List()
	{

		currentNode = head;
		Node* nextNodeToDelete;

		while (true)
		{
			if (currentNode)
			{
				nextNodeToDelete = currentNode->next;
				delete currentNode;
				currentNode = nextNodeToDelete;
			}
			else
			{
				break;
			}
		}
	}

 private:

	Node* currentNode = nullptr;
	Node* head = nullptr;
};

int main()
{
	{
		std::cout << "-->> example ---------------------------------->> 1" << std::endl;
		auto list = List();

		list.Insert(1);
		list.Insert(2);
		list.Insert(3);
		list.Insert(4);
	}

	{
		std::cout << "-->> example ---------------------------------->> 2" << std::endl;
		auto list = List({ 1, 2 });

		list.Insert(3);
		list.Insert(4);
		list.Append({ 5, 6 });
	}

	{
		std::cout << "-->> example ---------------------------------->> 3" << std::endl;

		auto list = List({ 1, 2 });

		list.Append({ 3, 4 });
		list.Insert(5);
		list.Insert(6);
	}

	{
		std::cout << "-->> example ---------------------------------->> 4" << std::endl;

		std::vector<double> values{ 1, 2 };

		auto list = List(values);

		list.Append({ 3, 4 });
		list.Insert(5);
		list.Insert(6);
	}

	{
		std::cout << "-->> example ---------------------------------->> 5" << std::endl;

		std::vector<double> values{ 1, 2 };

		auto list = List(values);

		std::vector<double> values2{ 3, 4 };

		list.Append(values2);
		list.Insert(5);
		list.Insert(6);
	}

	{
		std::cout << "-->> example ---------------------------------->> 6" << std::endl;

		std::vector<double> values{ 1, 2, 3, 4, 5, 6 };

		const auto list1 = List(values);
		const auto list2 = List(list1);
	}

	{
		std::cout << "-->> example ---------------------------------->> 7" << std::endl;

		std::vector<double> values{ 1, 2, 3, 4, 5, 6 };

		const auto list1 = List(values);
		const auto list2 = list1;

		std::cout << &list1 << std::endl;
		std::cout << &list2 << std::endl;
	}

	{
		std::cout << "-->> example ---------------------------------->> 8" << std::endl;

		std::vector<double> values{ 1, 2, 3, 4, 5, 6 };

		auto list1 = List(values);
		const auto list2 = List(std::move(list1));
	}

	{
		std::cout << "-->> example ---------------------------------->> 9" << std::endl;

		std::vector<double> values{ 1, 2, 3, 4, 5, 6 };

		auto list1 = List(values);
		const auto list2 = std::move(list1);

		std::cout << &list2 << std::endl;
	}

	{
		std::cout << "-->> example ---------------------------------->> 10" << std::endl;

		std::vector<double> values{ 1, 2, 3, 4, 5, 6 };

		auto list = List(values);

		int v = 1;
		std::cout << "pgg -->> Erasing element: " << v << std::endl;
		list.EraseOne(v);
		list.Print();

		v = 3;
		std::cout << "pgg -->> Erasing element: " << v << std::endl;
		list.EraseOne(v);
		list.Print();

		v = 6;
		std::cout << "pgg -->> Erasing element: " << v << std::endl;
		list.EraseOne(v);
		list.Print();
	}

	{
		std::cout << "-->> example ---------------------------------->> 11" << std::endl;

		std::vector<double> values{ 1, 2, 5, 1, 2, 4, 5 };

		auto list = List(values);

		int v = 1;
		std::cout << "pgg -->> Erasing all elements: " << v << std::endl;
		list.EraseAll(v);
		list.Print();

		v = 2;
		std::cout << "pgg -->> Erasing all elements: " << v << std::endl;
		list.EraseAll(v);
		list.Print();

		v = 5;
		std::cout << "pgg -->> Erasing all elements: " << v << std::endl;
		list.EraseAll(v);
		list.Print();

		v = 6;
		std::cout << "pgg -->> Erasing all elements: " << v << std::endl;
		list.EraseAll(v);
		list.Print();

		v = 4;
		std::cout << "pgg -->> Erasing all elements: " << v << std::endl;
		list.EraseAll(v);
		list.Print();
	}
}