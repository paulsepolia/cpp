#include <iostream>

template<typename T>
using fp_T_ptr_ref_to_void = void (*)(T *&, size_t);

template<typename T>
using fp_T_ptr_to_T_ptr = T *(*)(T *, size_t);

template<typename T>
using fp_T_2ptr_ref_to_void = void (*)(T **&, size_t, size_t);

template<typename T>
using fp_T_2ptr_to_T_2ptr = T **(*)(T **, size_t, size_t);

template<typename T>
void transform_vector(T *&data, size_t dim, fp_T_ptr_ref_to_void<T> tv)
{
    tv(data, dim);
}

template<typename T>
T *transform_vector(T *data, size_t dim, fp_T_ptr_to_T_ptr<T> tv)
{
    return tv(data, dim);
}

template<typename T>
void transform_matrix(T **&data, size_t dim1, size_t dim2, fp_T_2ptr_ref_to_void<T> tm)
{
    tm(data, dim1, dim2);
}

template<typename T>
T **transform_matrix(T **data, size_t dim1, size_t dim2, fp_T_2ptr_to_T_2ptr<T> tm)
{
    return tm(data, dim1, dim2);
}

template<typename T>
void vector_times_two(T *&data, size_t dim)
{
    for (size_t i = 0; i < dim; i++)
    {
        data[i] = 2 * data[i];
    }
}

template<typename T>
void vector_times_four(T *&data, size_t dim)
{
    for (size_t i = 0; i < dim; i++)
    {
        data[i] = 4 * data[i];
    }
}

template<typename T>
void matrix_times_two(T **&data, size_t dim1, size_t dim2)
{
    for (size_t i = 0; i < dim1; i++)
    {
        for (size_t j = 0; j < dim2; j++)
        {
            data[i][j] = 2 * data[i][j];
        }
    }
}

template<typename T>
void matrix_times_four(T **&data, size_t dim1, size_t dim2)
{
    for (size_t i = 0; i < dim1; i++)
    {
        for (size_t j = 0; j < dim2; j++)
        {
            data[i][j] = 4 * data[i][j];
        }
    }
}

template<typename T>
T *vector_times_two2(T *data, size_t dim)
{
    for (size_t i = 0; i < dim; i++)
    {
        data[i] = 2 * data[i];
    }
    return data;
}

template<typename T>
T *vector_times_four2(T *data, size_t dim)
{
    for (size_t i = 0; i < dim; i++)
    {
        data[i] = 4 * data[i];
    }
    return data;
}

template<typename T>
T **matrix_times_two2(T **data, size_t dim1, size_t dim2)
{
    for (size_t i = 0; i < dim1; i++)
    {
        for (size_t j = 0; j < dim2; j++)
        {
            data[i][j] = 2 * data[i][j];
        }
    }
    return data;
}

template<typename T>
T **matrix_times_four2(T **data, size_t dim1, size_t dim2)
{
    for (size_t i = 0; i < dim1; i++)
    {
        for (size_t j = 0; j < dim2; j++)
        {
            data[i][j] = 4 * data[i][j];
        }
    }
    return data;
}

int main()
{
    constexpr int DIM = 10000;
    {
        std::cout << "----------------->> Example # 1" << std::endl;
        auto data = new double[DIM];
        for (size_t i = 0; i < DIM; i++)
        {
            data[i] = (double)i;
        }

        transform_vector(data, DIM, vector_times_two);

        for (int i = 0; i < DIM; i++)
        {
            if (data[i] != (double)(i * 2))
            {
                std::cout << "ERROR at: " << i << std::endl;
                std::terminate();
            }
        }
        delete[] data;
        std::cout << "-->> All okay" << std::endl;
    }
    {
        std::cout << "----------------->> Example # 2" << std::endl;
        auto data = new double[DIM];
        for (size_t i = 0; i < DIM; i++)
        {
            data[i] = (double)i;
        }
        transform_vector(data, DIM, vector_times_four);
        for (size_t i = 0; i < DIM; i++)
        {
            if (data[i] != (double)(i * 4))
            {
                std::cout << "ERROR at: " << i << std::endl;
                std::terminate();
            }
        }
        delete[] data;
        std::cout << "-->> All okay" << std::endl;
    }
    {
        std::cout << "----------------->> Example # 3" << std::endl;
        auto data = new double[DIM];
        for (size_t i = 0; i < DIM; i++)
        {
            data[i] = (double)i;
        }
        auto res = transform_vector(data, DIM, vector_times_two2);
        for (size_t i = 0; i < DIM; i++)
        {
            if (res[i] != (double)(i * 2))
            {
                std::cout << "ERROR at: " << i << std::endl;
                std::terminate();
            }
        }
        delete[] data;
        std::cout << "-->> All okay" << std::endl;
    }
    {
        std::cout << "----------------->> Example # 4" << std::endl;
        auto data = new double[DIM];
        for (size_t i = 0; i < DIM; i++)
        {
            data[i] = (double)i;
        }
        auto res = transform_vector(data, DIM, vector_times_four2);
        for (int i = 0; i < DIM; i++)
        {
            if (res[i] != (double)(i * 4))
            {
                std::cout << "ERROR at: " << i << std::endl;
                std::terminate();
            }
        }
        delete[] data;
        std::cout << "-->> All okay" << std::endl;
    }
    {
        std::cout << "----------------->> Example # 5" << std::endl;
        auto data = new double *[DIM];
        for (size_t i = 0; i < DIM; i++)
        {
            data[i] = new double[DIM];
        }
        for (size_t i = 0; i < DIM; i++)
        {
            for (size_t j = 0; j < DIM; j++)
            {
                data[i][j] = (double)(i + j);
            }
        }

        transform_matrix(data, DIM, DIM, matrix_times_two);

        for (size_t i = 0; i < DIM; i++)
        {
            for (size_t j = 0; j < DIM; j++)
            {
                if (data[i][j] != (double)((i + j) * 2))
                {
                    std::cout << "ERROR at: " << i << ", " << j << std::endl;
                    std::terminate();
                }
            }
        }
        for (size_t i = 0; i < DIM; i++)
        {
            delete[] data[i];
        }
        delete[] data;
        std::cout << "-->> All okay" << std::endl;
    }
    {
        std::cout << "----------------->> Example # 6" << std::endl;
        auto data = new double *[DIM];
        for (size_t i = 0; i < DIM; i++)
        {
            data[i] = new double[DIM];
        }
        for (size_t i = 0; i < DIM; i++)
        {
            for (size_t j = 0; j < DIM; j++)
            {
                data[i][j] = (double)(i + j);
            }
        }

        transform_matrix(data, DIM, DIM, matrix_times_four);

        for (size_t i = 0; i < DIM; i++)
        {
            for (size_t j = 0; j < DIM; j++)
            {
                if (data[i][j] != (double)((i + j) * 4))
                {
                    std::cout << "ERROR at: " << i << ", " << j << std::endl;
                    std::terminate();
                }
            }
        }
        for (size_t i = 0; i < DIM; i++)
        {
            delete[] data[i];
        }
        delete[] data;
        std::cout << "-->> All okay" << std::endl;
    }
    {
        std::cout << "----------------->> Example # 7" << std::endl;
        auto data = new double *[DIM];
        for (size_t i = 0; i < DIM; i++)
        {
            data[i] = new double[DIM];
        }
        for (size_t i = 0; i < DIM; i++)
        {
            for (size_t j = 0; j < DIM; j++)
            {
                data[i][j] = (double)(i + j);
            }
        }

        data = transform_matrix(data, DIM, DIM, matrix_times_two2);

        for (size_t i = 0; i < DIM; i++)
        {
            for (size_t j = 0; j < DIM; j++)
            {
                if (data[i][j] != (double)((i + j) * 2))
                {
                    std::cout << "ERROR at: " << i << ", " << j << std::endl;
                    std::terminate();
                }
            }
        }
        for (size_t i = 0; i < DIM; i++)
        {
            delete[] data[i];
        }
        delete[] data;
        std::cout << "-->> All okay" << std::endl;
    }
    {
        std::cout << "----------------->> Example # 8" << std::endl;
        auto data = new double *[DIM];
        for (size_t i = 0; i < DIM; i++)
        {
            data[i] = new double[DIM];
        }
        for (size_t i = 0; i < DIM; i++)
        {
            for (size_t j = 0; j < DIM; j++)
            {
                data[i][j] = (double)(i + j);
            }
        }

        data = transform_matrix(data, DIM, DIM, matrix_times_four2);

        for (size_t i = 0; i < DIM; i++)
        {
            for (size_t j = 0; j < DIM; j++)
            {
                if (data[i][j] != (double)((i + j) * 4))
                {
                    std::cout << "ERROR at: " << i << ", " << j << std::endl;
                    std::terminate();
                }
            }
        }
        for (size_t i = 0; i < DIM; i++)
        {
            delete[] data[i];
        }
        delete[] data;
        std::cout << "-->> All okay" << std::endl;
    }
}
