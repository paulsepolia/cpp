#include <iostream>

using fp_int_ptr_ref_to_void = void (*)(int *&, int);
using fp_int_ptr_to_int_ptr = int *(*)(int *, int);
using fp_int2_ptr_ref_to_void = void (*)(int **&, int, int);
using fp_int2_ptr_to_int2_ptr = int **(*)(int **, int, int);

void transform_vector(int *&data, int dim, fp_int_ptr_ref_to_void tv)
{
    tv(data, dim);
}

int *transform_vector(int *data, int dim, fp_int_ptr_to_int_ptr tv)
{
    return tv(data, dim);
}

void transform_matrix(int **&data, int dim1, int dim2, fp_int2_ptr_ref_to_void tm)
{
    tm(data, dim1, dim2);
}

int **transform_matrix(int **data, int dim1, int dim2, fp_int2_ptr_to_int2_ptr tm)
{
    return tm(data, dim1, dim2);
}

void vector_times_two(int *&data, int dim)
{
    for (int i = 0; i < dim; i++)
    {
        data[i] = 2 * data[i];
    }
}

void vector_times_four(int *&data, int dim)
{
    for (int i = 0; i < dim; i++)
    {
        data[i] = 4 * data[i];
    }
}

void matrix_times_two(int **&data, int dim1, int dim2)
{
    for (int i = 0; i < dim1; i++)
    {
        for (int j = 0; j < dim2; j++)
        {
            data[i][j] = 2 * data[i][j];
        }
    }
}

void matrix_times_four(int **&data, int dim1, int dim2)
{
    for (int i = 0; i < dim1; i++)
    {
        for (int j = 0; j < dim2; j++)
        {
            data[i][j] = 4 * data[i][j];
        }
    }
}

int *vector_times_two2(int *data, int dim)
{
    for (int i = 0; i < dim; i++)
    {
        data[i] = 2 * data[i];
    }
    return data;
}

int *vector_times_four2(int *data, int dim)
{
    for (int i = 0; i < dim; i++)
    {
        data[i] = 4 * data[i];
    }
    return data;
}

int **matrix_times_two2(int **data, int dim1, int dim2)
{
    for (int i = 0; i < dim1; i++)
    {
        for (int j = 0; j < dim2; j++)
        {
            data[i][j] = 2 * data[i][j];
        }
    }
    return data;
}

int **matrix_times_four2(int **data, int dim1, int dim2)
{
    for (int i = 0; i < dim1; i++)
    {
        for (int j = 0; j < dim2; j++)
        {
            data[i][j] = 4 * data[i][j];
        }
    }
    return data;
}

int main()
{
    constexpr int DIM = 100;
    {
        std::cout << "----------------->> Example # 1" << std::endl;
        auto data = new int[DIM];
        for (int i = 0; i < DIM; i++)
        {
            data[i] = i;
        }

        transform_vector(data, DIM, vector_times_two);

        for (int i = 0; i < DIM; i++)
        {
            if (data[i] != i * 2)
            {
                std::cout << "ERROR at: " << i << std::endl;
                std::terminate();
            }
        }
        delete[] data;
        std::cout << "-->> All okay" << std::endl;
    }
    {
        std::cout << "----------------->> Example # 2" << std::endl;
        auto data = new int[DIM];
        for (int i = 0; i < DIM; i++)
        {
            data[i] = i;
        }
        transform_vector(data, DIM, vector_times_four);
        for (int i = 0; i < DIM; i++)
        {
            if (data[i] != i * 4)
            {
                std::cout << "ERROR at: " << i << std::endl;
                std::terminate();
            }
        }
        delete[] data;
        std::cout << "-->> All okay" << std::endl;
    }
    {
        std::cout << "----------------->> Example # 3" << std::endl;
        auto data = new int[DIM];
        for (int i = 0; i < DIM; i++)
        {
            data[i] = i;
        }
        auto res = transform_vector(data, DIM, vector_times_two2);
        for (int i = 0; i < DIM; i++)
        {
            if (res[i] != i * 2)
            {
                std::cout << "ERROR at: " << i << std::endl;
                std::terminate();
            }
        }
        delete[] data;
        std::cout << "-->> All okay" << std::endl;
    }
    {
        std::cout << "----------------->> Example # 4" << std::endl;
        auto data = new int[DIM];
        for (int i = 0; i < DIM; i++)
        {
            data[i] = i;
        }
        auto res = transform_vector(data, DIM, vector_times_four2);
        for (int i = 0; i < DIM; i++)
        {
            if (res[i] != i * 4)
            {
                std::cout << "ERROR at: " << i << std::endl;
                std::terminate();
            }
        }
        delete[] data;
        std::cout << "-->> All okay" << std::endl;
    }
    {
        std::cout << "----------------->> Example # 5" << std::endl;
        auto data = new int *[DIM];
        for (int i = 0; i < DIM; i++)
        {
            data[i] = new int[DIM];
        }
        for (int i = 0; i < DIM; i++)
        {
            for (int j = 0; j < DIM; j++)
            {
                data[i][j] = (i + j);
            }
        }

        transform_matrix(data, DIM, DIM, matrix_times_two);

        for (int i = 0; i < DIM; i++)
        {
            for (int j = 0; j < DIM; j++)
            {
                if (data[i][j] != (i + j) * 2)
                {
                    std::cout << "ERROR at: " << i << ", " << j << std::endl;
                    std::terminate();
                }
            }
        }
        for (int i = 0; i < DIM; i++)
        {
            delete[] data[i];
        }
        delete[] data;
        std::cout << "-->> All okay" << std::endl;
    }
    {
        std::cout << "----------------->> Example # 6" << std::endl;
        auto data = new int *[DIM];
        for (int i = 0; i < DIM; i++)
        {
            data[i] = new int[DIM];
        }
        for (int i = 0; i < DIM; i++)
        {
            for (int j = 0; j < DIM; j++)
            {
                data[i][j] = (i + j);
            }
        }

        transform_matrix(data, DIM, DIM, matrix_times_four);

        for (int i = 0; i < DIM; i++)
        {
            for (int j = 0; j < DIM; j++)
            {
                if (data[i][j] != (i + j) * 4)
                {
                    std::cout << "ERROR at: " << i << ", " << j << std::endl;
                    std::terminate();
                }
            }
        }
        for (int i = 0; i < DIM; i++)
        {
            delete[] data[i];
        }
        delete[] data;
        std::cout << "-->> All okay" << std::endl;
    }
    {
        std::cout << "----------------->> Example # 7" << std::endl;
        auto data = new int *[DIM];
        for (int i = 0; i < DIM; i++)
        {
            data[i] = new int[DIM];
        }
        for (int i = 0; i < DIM; i++)
        {
            for (int j = 0; j < DIM; j++)
            {
                data[i][j] = (i + j);
            }
        }

        data = transform_matrix(data, DIM, DIM, matrix_times_two2);

        for (int i = 0; i < DIM; i++)
        {
            for (int j = 0; j < DIM; j++)
            {
                if (data[i][j] != (i + j) * 2)
                {
                    std::cout << "ERROR at: " << i << ", " << j << std::endl;
                    std::terminate();
                }
            }
        }
        for (int i = 0; i < DIM; i++)
        {
            delete[] data[i];
        }
        delete[] data;
        std::cout << "-->> All okay" << std::endl;
    }
    {
        std::cout << "----------------->> Example # 8" << std::endl;
        auto data = new int *[DIM];
        for (int i = 0; i < DIM; i++)
        {
            data[i] = new int[DIM];
        }
        for (int i = 0; i < DIM; i++)
        {
            for (int j = 0; j < DIM; j++)
            {
                data[i][j] = (i + j);
            }
        }

        data = transform_matrix(data, DIM, DIM, matrix_times_four2);

        for (int i = 0; i < DIM; i++)
        {
            for (int j = 0; j < DIM; j++)
            {
                if (data[i][j] != (i + j) * 4)
                {
                    std::cout << "ERROR at: " << i << ", " << j << std::endl;
                    std::terminate();
                }
            }
        }
        for (int i = 0; i < DIM; i++)
        {
            delete[] data[i];
        }
        delete[] data;
        std::cout << "-->> All okay" << std::endl;
    }
}
