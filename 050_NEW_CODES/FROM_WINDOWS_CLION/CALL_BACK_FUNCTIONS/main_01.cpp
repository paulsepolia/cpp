#include <iostream>
#include <vector>

using fp_void_int_ref = void (*)(int &);

int *f1(int *&data, int dim, fp_void_int_ref call_back)
{
    for (int i = 0; i < dim; i++)
    {
        call_back(data[i]);
    }
    return data;
}

void double_int(int &x)
{
    x = 2 * x;
}

void square_int(int &x)
{
    x = x * x;
}

int main()
{
    {
        std::cout << "----------------->> Example # 1" << std::endl;
        constexpr int DIM = 10;
        int *data = new int[DIM];
        for (int i = 0; i < DIM; i++)
        {
            data[i] = i;
        }
        auto data_new = f1(data, DIM, square_int);
        for (int i = 0; i < DIM; i++)
        {
            if (data_new[i] != i * i)
            {
                std::cout << "ERROR: at " << i << std::endl;
                std::terminate();
            }
        }
        std::cout << "-->> All okay" << std::endl;
    }
    {
        std::cout << "----------------->> Example # 2" << std::endl;
        constexpr int DIM = 10;
        int *data = new int[DIM];
        for (int i = 0; i < DIM; i++)
        {
            data[i] = i;
        }
        auto data_new = f1(data, DIM, double_int);
        for (int i = 0; i < DIM; i++)
        {
            if (data_new[i] != 2 * i)
            {
                std::cout << "ERROR: at " << i << std::endl;
                std::terminate();
            }
        }
        std::cout << "-->> All okay" << std::endl;
    }
    {
        std::cout << "----------------->> Example # 3" << std::endl;
        constexpr int DIM = 10;
        std::vector<fp_void_int_ref> v_pf;
        v_pf.push_back(double_int);
        v_pf.push_back(square_int);
        {
            // case # 1
            int *data = new int[DIM];
            for (int i = 0; i < DIM; i++)
            {
                data[i] = i;
            }
            auto data_new = f1(data, DIM, v_pf[0]);
            for (int i = 0; i < DIM; i++)
            {
                if (data_new[i] != 2 * i)
                {
                    std::cout << "ERROR: at " << i << std::endl;
                    std::terminate();
                }
            }
            std::cout << "-->> All okay" << std::endl;
        }
        {
            // case # 2
            int *data = new int[DIM];
            for (int i = 0; i < DIM; i++)
            {
                data[i] = i;
            }
            auto data_new = f1(data, DIM, v_pf[1]);
            for (int i = 0; i < DIM; i++)
            {
                if (data_new[i] != i * i)
                {
                    std::cout << "ERROR: at " << i << std::endl;
                    std::terminate();
                }
            }
            std::cout << "-->> All okay" << std::endl;
        }
    }
}
