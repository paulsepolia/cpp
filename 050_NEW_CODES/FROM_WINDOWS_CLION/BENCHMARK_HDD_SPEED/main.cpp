#include <iostream>
#include <fstream>
#include <chrono>

const size_t N = 1e7; // size of the array
const size_t M = 200; // number of times to write and read the data
const size_t BYTES_PER_DOUBLE = sizeof(double);
const size_t FILE_SIZE = N * BYTES_PER_DOUBLE;

int main()
{
    // Create an array of random doubles
    auto data = new double[N];
    for (size_t i = 0; i < N; i++)
    {
        data[i] = static_cast<double>(10.0);
    }

    // Open a file for writing
    std::ofstream file("data.bin", std::ios::binary);
    if (!file)
    {
        std::cerr << "Error: Unable to open file for writing." << std::endl;
        return 1;
    }

    // Write the data to the file M times and measure the time taken
    std::chrono::duration<double> write_time{};
    for (size_t i = 0; i < M; i++)
    {
        auto start = std::chrono::high_resolution_clock::now();
        file.write(reinterpret_cast<char *>(data), FILE_SIZE);
        file.flush();
        auto end = std::chrono::high_resolution_clock::now();
        write_time += (end - start);
    }

    // Close the file
    file.close();

    // Open the file for reading
    std::ifstream file_in("data.bin", std::ios::binary);
    if (!file_in)
    {
        std::cerr << "Error: Unable to open file for reading." << std::endl;
        return 1;
    }

    // Read the data from the file M times and measure the time taken
    std::chrono::duration<double> read_time{};
    for (size_t i = 0; i < M; i++)
    {
        auto start = std::chrono::high_resolution_clock::now();
        file_in.read(reinterpret_cast<char *>(data), FILE_SIZE);
        auto end = std::chrono::high_resolution_clock::now();
        read_time += (end - start);
    }

    // Close the file
    file_in.close();

    // Print the time taken to write and read the data
    std::cout << "Time taken to write the data: " << write_time.count() << " seconds" << std::endl;
    std::cout << "Time taken to read the data: " << read_time.count() << " seconds" << std::endl;
    std::cout << "Write speed is: " << (FILE_SIZE * M) / (1024.0 * 1024.0) / write_time.count() << " MBytes/sec"
              << std::endl;
    std::cout << "Read speed is: " << (FILE_SIZE * M) / (1024.0 * 1024.0) / read_time.count() << " MBytes/sec"
              << std::endl;
    delete[] data;
}
