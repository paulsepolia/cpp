#include <iostream>
#include <vector>
#include <string>

class Person
{
public:

    explicit Person(std::string name) : m_name{new std::string(std::move(name))}
    {
    }

    [[maybe_unused]] void print_name()
    {
        std::cout << (*m_name) << std::endl;
    }

    [[maybe_unused]] void delete_person()
    {
        delete m_name;
        m_name = nullptr;
        delete this;
    }

private:

    ~Person()
    {
        std::cout << "DOES ANYONE CALLED ME???" << std::endl;
    }

    const std::string *m_name;
};

int main()
{
    {
        std::cout << "------------------>> 1" << std::endl;
        auto *pa = new Person(std::string("a1"));
        pa->print_name();
        pa->delete_person(); // CALL THE DESTRUCTOR HERE
        pa->print_name(); // BREAKS HERE, ERROR
        pa->delete_person(); // BREAKS HERE, ERROR
    }

    return 0;
}
