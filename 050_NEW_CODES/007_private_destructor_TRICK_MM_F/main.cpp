#include <iostream>
#include <vector>
#include <string>

class Person
{
public:

    explicit Person(std::string name)
    {
        pName = new std::string(std::move(name));
    }

    void printName()
    {
        std::cout << *pName << std::endl;
    }

    void deletePerson()
    {
        delete pName;
        delete this;
    }

private:

    ~Person() = default;

private:

    std::string *pName;
};

int main()
{
    {
        std::cout << "------------------>> 1" << std::endl;
        auto *pa = new Person(std::string("a1"));
        pa->printName();
        pa->deletePerson();
    }

    return 0;
}
