
//==========================//
// random_device            //
// uniform_int_distribution //
//==========================//

#include <iostream>
#include <string>
#include <map>
#include <random>
#include <iomanip>
#include <cmath>
#include <ctime>

using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::map;
using std::random_device;
using std::uniform_int_distribution;
using std::uniform_real_distribution;
using std::fixed;
using std::setprecision;
using std::pow;
using std::clock;

//===================//
// the main function //
//===================//

int main()
{
    const unsigned long int DIM = static_cast<unsigned long int>(pow(10.0, 7.0));

    random_device rd;
    uniform_int_distribution<int> distA(0, 10);
    uniform_real_distribution<double> distB(0, 10);
    time_t t1;
    time_t t2;

    srand(static_cast<unsigned int>(time(0)));

    cout << fixed;
    cout << setprecision(10);

    double val_sum1 = 0.0;
    double val_sum2 = 0.0;
    double val_sum3 = 0.0;
    double val1 = 0;
    double val2 = 0;
    double val3 = 0;

    cout << "--> 1 --> generating random numbers --> uniform_real_distribution" << endl;

    t1 = clock();

    for(unsigned long int i = 0; i != DIM; i++) {
        val1 = distB(rd);
        val_sum1 = val_sum1 + val1;
    }

    t2 = clock();

    cout << " --> value average (real) = " << val_sum1/DIM << endl;
    cout << " --> time used            = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

    cout << "--> 2 --> generating random numbers --> uniform_int_distribution" << endl;

    t1 = clock();

    for(unsigned long int i = 0; i != DIM; i++) {
        val2 = distA(rd);
        val_sum2 = val_sum2 + val2;
    }

    t2 = clock();

    cout << " --> value average (int)  = " << val_sum2/DIM << endl;
    cout << " --> time used            = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

    cout << "--> 3 --> generating random numbers --> rand()" << endl;

    t1 = clock();

    for(unsigned long int i = 0; i != DIM; i++) {
        val3 = (static_cast<unsigned long int>(rand())+1UL)%11;
        val_sum3 = val_sum3 + val3;
    }

    t2 = clock();

    cout << " --> value average (real) = " << val_sum3/DIM << endl;
    cout << " --> time used            = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

    return 0;
}

//=====//
// END //
//=====//
