#include <iostream>
#include <vector>
#include <chrono>

class BenchmarkTimer {
public:

    explicit BenchmarkTimer() :
            _res(0.0),
            _t1{std::chrono::steady_clock::now()},
            _t2{std::chrono::steady_clock::now()},
            _t_prev{std::chrono::steady_clock::now()} {};

    auto PrintTime() const -> void {
        const auto t_loc{std::chrono::steady_clock::now()};
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t_loc - _t_prev).count()};
        _t_prev = t_loc;
        std::cout.precision(10);
        std::cout << std::fixed;
        std::cout << " --> time used up to now (since last reset) (secs) = " << total_time << std::endl;
    }

    auto ResetTimer() -> void {
        const auto t_loc{std::chrono::steady_clock::now()};
        _t_prev = t_loc;
        _t1 = t_loc;
        _t2 = t_loc;
    }

    ~BenchmarkTimer() {
        _t2 = std::chrono::steady_clock::now();
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(_t2 - _t1).count()};
        std::cout << " --> res = " << _res << std::endl;
        std::cout.precision(10);
        std::cout << std::fixed;
        std::cout << " --> total time used until exit of the scope (since last reset) (secs) = " << total_time
                  << std::endl;
    }

    auto SetRes(const double &res) -> void {
        _res = res;
    }

private:

    double _res{};
    decltype(std::chrono::steady_clock::now()) _t1{};
    decltype(std::chrono::steady_clock::now()) _t2{};
    mutable decltype(std::chrono::steady_clock::now()) _t_prev{};
};

// Forward declare AbstractDispatcher
class AbstractDispatcher;

// Parent class for the elements
// e.g: ArchivedFile, SplitFile and ExtractedFile

class File {

public:
    // This function accepts an object of any class derived from
    // AbstractDispatcher and must be implemented in all derived classes
    virtual void Accept(AbstractDispatcher &dispatcher) = 0;
};

// Forward declare specific elements (files) to be dispatched
class ArchivedFile;

class SplitFile;

class ExtractedFile;

class AbstractDispatcher {  // Declares the interface for the dispatcher
public:
    // Declare overloads for each kind of a file to dispatch
    virtual void Dispatch(ArchivedFile &file) = 0;

    virtual void Dispatch(SplitFile &file) = 0;

    virtual void Dispatch(ExtractedFile &file) = 0;
};

class ArchivedFile : public File {  // Specific element class #1
public:
    // Resolved at runtime, it calls the dispatcher's overloaded function,
    // corresponding to ArchivedFile.
    void Accept(AbstractDispatcher &dispatcher) override {
        dispatcher.Dispatch(*this);
    }
};

class SplitFile : public File {  // Specific element class #2
public:
    // Resolved at runtime, it calls the dispatcher's overloaded function,
    // corresponding to SplitFile.
    void Accept(AbstractDispatcher &dispatcher) override {
        dispatcher.Dispatch(*this);
    }
};

class ExtractedFile : public File {  // Specific element class #3
public:
    // Resolved at runtime, it calls the dispatcher's overloaded function,
    // corresponding to ExtractedFile.
    void Accept(AbstractDispatcher &dispatcher) override {
        dispatcher.Dispatch(*this);
    }
};

class Dispatcher : public AbstractDispatcher {  // Implements dispatching of all
    // kind of elements (files)
public:
    void Dispatch(ArchivedFile &) override {
        std::cout << "dispatching ArchivedFile" << std::endl;
    }

    void Dispatch(SplitFile &) override {
        std::cout << "dispatching SplitFile" << std::endl;
    }

    void Dispatch(ExtractedFile &) override {
        std::cout << "dispatching ExtractedFile" << std::endl;
    }
};

int main() {

    ArchivedFile archived_file;
    SplitFile split_file;
    ExtractedFile extracted_file;

    std::vector<File *> files = {
            &archived_file,
            &split_file,
            &extracted_file,
    };

    Dispatcher dispatcher;

    for (File *file : files) {
        file->Accept(dispatcher);
    }
}