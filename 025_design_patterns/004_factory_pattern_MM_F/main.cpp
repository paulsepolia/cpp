#include <vector>
#include <iostream>

class Stooge
{
public:
    virtual void slap_stick() = 0;

    static Stooge *make_stooge(int choice);

    virtual ~Stooge() = default;
};

class Larry : public Stooge
{
public:
    void slap_stick() final
    {
        std::cout << "Larry: poke eyes" << std::endl;
    }
};

class Moe : public Stooge
{
public:
    void slap_stick() final
    {
        std::cout << "Moe: slap head" << std::endl;
    }
};

class Curly : public Stooge
{
public:
    void slap_stick() final
    {
        std::cout << "Curly: suffer abuse" << std::endl;
    }
};

Stooge *Stooge::make_stooge(int choice)
{
    if (choice == 1)
    {
        return new Larry;
    }
    else if (choice == 2)
    {
        return new Moe;
    }
    else
    {
        return new Curly;
    }
}


int main()
{
    std::vector<Stooge *> roles;
    int choice;

    while (true)
    {
        std::cout << "Larry(1) Moe(2) Curly(3) Go(0): ";
        std::cin >> choice;

        if (choice == 0)
        {
            break;
        }
        roles.push_back(Stooge::make_stooge(choice));
    }

    for (const auto &el: roles)
    {
        el->slap_stick();
    }

    for (auto &el: roles)
    {
        delete el;
    }
}

