#include <vector>
#include <iostream>

class Singleton1
{
public:

    static Singleton1 &getInstance()
    {
        static Singleton1 theOnlyInstance;
        return theOnlyInstance;
    }

    Singleton1(const Singleton1 &) = delete;

    Singleton1(Singleton1 &&) = delete;

    Singleton1 &operator=(Singleton1 &) = delete;

    Singleton1 operator=(Singleton1 &&) = delete;

    double x1 = 0;

private:
    Singleton1() = default;
};

class Singleton2
{
public:

    static Singleton2 *getInstance()
    {
        static auto *theOnlyInstance = new Singleton2();
        return theOnlyInstance;
    }

    Singleton2(const Singleton2 &) = delete;

    Singleton2(Singleton2 &&) = delete;

    Singleton2 &operator=(Singleton2 &) = delete;

    Singleton2 &operator=(Singleton2 &&) = delete;

    double x1 = 0;

private:
    Singleton2() = default;
};

int main()
{
    {
        std::cout << "----------------------------->> 1" << std::endl;

        auto &p1 = Singleton1::getInstance();
        auto &p2 = Singleton1::getInstance();

        std::cout << &p1 << std::endl;
        std::cout << &p2 << std::endl;

        std::cout << &(p1.x1) << std::endl;
        std::cout << &(p2.x1) << std::endl;

        p1.x1 = 10;

        std::cout << p1.x1 << std::endl;
        std::cout << p2.x1 << std::endl;

        p1.x1 = 11;

        std::cout << p1.x1 << std::endl;
        std::cout << p2.x1 << std::endl;
    }

    {
        std::cout << "----------------------------->> 2" << std::endl;

        auto *p1 = Singleton2::getInstance();
        auto *p2 = Singleton2::getInstance();

        std::cout << p1 << std::endl;
        std::cout << p2 << std::endl;

        std::cout << &(p1->x1) << std::endl;
        std::cout << &(p2->x1) << std::endl;

        p1->x1 = 10;

        std::cout << p1->x1 << std::endl;
        std::cout << p2->x1 << std::endl;

        p1->x1 = 11;

        std::cout << p1->x1 << std::endl;
        std::cout << p2->x1 << std::endl;
    }
}

