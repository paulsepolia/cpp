#!/bin/bash

  # 1. compile

  g++.7.1.0 -O3                \
            -Wall              \
	        -pthread           \
            -fopenmp           \
            -std=gnu++14       \
            driver_program.cpp \
            -o x_gnu
