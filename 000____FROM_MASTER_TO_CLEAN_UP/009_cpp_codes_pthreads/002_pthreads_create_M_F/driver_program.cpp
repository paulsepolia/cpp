//====================================//
// C++ Code for pthread_create() test //
//====================================//

#include <iostream>
#include <pthread.h>

using  std::cout;
using  std::endl;

#define NTHREADS 50000

// a function

void *do_nothing(void *null)
{
    int i;
    i = 0;
    pthread_exit(NULL);
}

// the main function

int main()
{
    int rc;
    int i;
    int j;
    int detachstate;
    pthread_t tid;
    pthread_attr_t attr;

    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

    for (j = 0; j < NTHREADS; j++) {
        rc = pthread_create(&tid, &attr, do_nothing, NULL);
        if (rc) {
            cout << "ERROR; return code from pthread_create() is " << rc << endl;
            exit(-1);
        }

        // Wait for the thread

        rc = pthread_join(tid, NULL);
        if (rc) {
            cout << "ERROR; return code from pthread_join() is " << rc << endl;
            exit(-1);
        }
    }

    pthread_attr_destroy(&attr);
    pthread_exit(NULL);

    return 0;
}

//======//
// FINI //
//======//
