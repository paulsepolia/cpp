#!/bin/bash

  # 1. compile

  icpc -O3                 \
	   -ipo                \
       -Wall               \
	   -wd1202             \
       -std=gnu++17        \
       -pthread            \
       driver_program.cpp  \
       -o x_intel
