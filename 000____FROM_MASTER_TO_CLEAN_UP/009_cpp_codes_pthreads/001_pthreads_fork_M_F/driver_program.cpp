//===================================//
// C++ Code for fork() creation test //
//===================================//

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#define NFORKS 50000

using namespace std;

// a function

void do_nothing()
{
    int i(0);
}

// the main function

int main()
{
    int pid;
    int j;
    int status;

    for (j = 0; j < NFORKS; j++) {
        // error handling

        if ((pid = fork()) < 0 ) {
            cout << "--> fork failed with error code = " << pid << endl;
            exit(0);
        } else if (pid == 0) { // this is the child of the fork
            do_nothing();
            exit(0);
        } else { // this is the parent of the fork
            waitpid(pid, &status, 0);
        }
    }

    return 0;
}

//======//
// FINI //
//======//
