//==================================//
// A "hello world" Pthreads program //
// Demonstrates thread creation     //
//==================================//

#include <iostream>
#include <pthread.h>

using std::cout;
using std::endl;

#define NUM_THREADS 1000

// a function

void *PrintHello(void *threadid)
{
    long tid;
    tid = long (threadid);
    cout << "Hello World! It's me, thread # " << tid << endl;
    pthread_exit(NULL);
}

// the main function

int main()
{
    pthread_t threads[NUM_THREADS];
    int rc;
    long t;

    for(t = 0; t < NUM_THREADS; t++) {
        cout << "In main: creating thread " <<  t << endl;

        rc = pthread_create(&threads[t], NULL, PrintHello, (void *) t);

        if (rc) {
            cout << " ERROR; return code from pthread_create() is " <<  rc << endl;
            exit(-1);
        }
    }

    // Last thing that main() should do

    pthread_exit(NULL);

    return 0;
}

//======//
// FINI //
//======//
