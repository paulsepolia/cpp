
//======================================================================//
// A "hello world" Pthreads program which demonstrates another safe way //
// to pass arguments to threads during thread creation.  In this case,  //
// a structure is used to pass multiple arguments.                      //
//======================================================================//

#include <iostream>
#include <pthread.h>

using std::cout;
using std::endl;

#define NUM_THREADS 8

// an array

char * messages [ NUM_THREADS ];

// a struct

struct thread_data {
    int thread_id;
    int sum;
    char *message;
};

struct thread_data thread_data_array [ NUM_THREADS ];

// a function

void * PrintHello(void * threadarg)
{
    int taskid, sum;
    char * hello_msg;
    struct thread_data * my_data;
    struct timespec req;
    struct timespec rem;

    int msec = 2000;

    req.tv_sec = msec / 1000;
    req.tv_nsec = (msec % 1000) * 1000000;

    nanosleep(&req, &rem);

    my_data = (struct thread_data *) threadarg;

    taskid = my_data -> thread_id;
    sum = my_data -> sum;
    hello_msg = my_data -> message;

    cout << "Thread " << taskid << " : " << hello_msg << " Sum = " << sum << endl;

    pthread_exit(NULL);

    return 0;
}

// the main function

int main()
{
    pthread_t threads[NUM_THREADS];
    int *taskids[NUM_THREADS];
    int rc, t, sum;

    sum = 0;
    messages[0] = "English: Hello World!";
    messages[1] = "French: Bonjour, le monde!";
    messages[2] = "Spanish: Hola al mundo";
    messages[3] = "Klingon: Nuq neH!";
    messages[4] = "German: Guten Tag, Welt!";
    messages[5] = "Russian: Zdravstvytye, mir!";
    messages[6] = "Japan: Sekai e konnichiwa!";
    messages[7] = "Latin: Orbis, te saluto!";

    for( t = 0; t < NUM_THREADS; t++) {
        sum = sum + t;
        thread_data_array[t].thread_id = t;
        thread_data_array[t].sum = sum;
        thread_data_array[t].message = messages[t];

        cout << " Creating thread " << t << endl;

        rc = pthread_create(&threads[t], NULL, PrintHello, (void *) &thread_data_array[t]);

        if (rc) {
            cout << " ERROR; return code from pthread_create() is " << rc << endl;
            exit(-1);
        }
    }

    pthread_exit(NULL);

    return 0;
}

//======//
// FINI //
//======//
