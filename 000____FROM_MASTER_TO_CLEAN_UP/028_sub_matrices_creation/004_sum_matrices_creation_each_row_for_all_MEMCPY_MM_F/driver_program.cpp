#include <iostream>
#include <chrono>
#include <vector>
#include <cmath>
#include <random>
#include <algorithm>
#include <cstring>

typedef short ELEM_TYPE;

std::vector<ELEM_TYPE> create_matrix(const uint64_t &rows,
                                     const uint64_t &columns) {

    std::vector<ELEM_TYPE> vd;
    vd.resize(rows * columns);

    for (uint64_t i = 0; i < vd.size(); i++) {
        vd[i] = static_cast<ELEM_TYPE>(i);
    }

    return vd;
}

std::vector<std::vector<ELEM_TYPE>>
create_row_by_row_submatrices(const std::vector<ELEM_TYPE> &matrix_in,
                              const uint64_t &rows,
                              const uint64_t &columns,
                              const std::vector<uint64_t> &columns_sub,
                              const std::vector<uint64_t> &offset) {

    // allocate space for all the sub-matrices

    std::vector<std::vector<ELEM_TYPE>> sub_matrices_out;

    sub_matrices_out.resize(columns_sub.size());

    for (uint64_t i = 0; i < sub_matrices_out.size(); i++) {
        sub_matrices_out[i].resize(rows * columns_sub[i]);
    }

    // create the sub-matrices in one go

    for (uint64_t i = 0; i < sub_matrices_out.size(); i++) {

        const auto length = sizeof(ELEM_TYPE) * columns_sub[i];

        // traverse the matrix_in by row and pick up
        // the sub-rows and put them in the sub-matrices

        for (uint64_t j = 0; j < rows; j++) {

            memcpy(&sub_matrices_out[i][j * columns_sub[i]], &matrix_in[j * columns + offset[i]], length);
        }
    }

    return sub_matrices_out;
}

void shuffle_matrix(std::vector<ELEM_TYPE> &matrix) {

    auto seed = std::chrono::system_clock::now().time_since_epoch().count();

    std::shuffle(matrix.begin(), matrix.end(), std::default_random_engine(seed));
}

int main() {

    const uint64_t K_MAX = 1000;
    double total_time = 0;
    double average_time = 0;

    for (uint64_t k = 0; k < K_MAX; k++) {

        std::cout << "---------------------------------------------->> k = " << k << std::endl;

        // parameters

        const auto rows = static_cast<uint64_t>(64);
        const auto columns = static_cast<uint64_t>(60000);

        const uint64_t SUBS_NUM = 100;

        std::vector<uint64_t> columns_sub;
        columns_sub.resize(SUBS_NUM);

        std::vector<uint64_t> off_sets;
        off_sets.resize(SUBS_NUM);

        std::random_device rd; // obtain a random number from hardware
        std::mt19937 eng(rd()); // seed the generator

        std::uniform_int_distribution<> distr_columns(100, static_cast<int32_t>(columns) - 100);
        std::uniform_int_distribution<> distr_offsets(0, 100);

        for (uint64_t i = 0; i < SUBS_NUM; i++) {
            columns_sub[i] = static_cast<uint64_t>(distr_columns(eng));
        }

        for (uint64_t i = 0; i < SUBS_NUM; i++) {
            off_sets[i] = static_cast<uint64_t>(distr_offsets(eng));
        }

        std::cout << std::boolalpha;

        // create matrix

        auto t1 = std::chrono::high_resolution_clock::now();

        std::vector<ELEM_TYPE> matrix_in = create_matrix(rows, columns);

        auto t2 = std::chrono::high_resolution_clock::now();

        const auto time_to_create_matrix =
                std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);

        std::cout << " --> time to create matrix = " << time_to_create_matrix.count() << std::endl;

        // many sub-matrices

        std::vector<std::vector<ELEM_TYPE>> sub_matrices;

        // shuffle the input matrix

        shuffle_matrix(matrix_in);

        // create many sub-matrices

        t1 = std::chrono::high_resolution_clock::now();

        sub_matrices = create_row_by_row_submatrices(matrix_in,
                                                     rows,
                                                     columns,
                                                     columns_sub,
                                                     off_sets);

        t2 = std::chrono::high_resolution_clock::now();

        const auto time_to_create_sub_matrices =
                std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);

        total_time = total_time + time_to_create_sub_matrices.count();
        average_time = total_time / (k + 1);

        std::cout << " --> time to create sub-matrices = " << time_to_create_sub_matrices.count() << std::endl;
        std::cout << " --> total time = " << total_time << std::endl;
        std::cout << " --> average time = " << average_time << std::endl;
        std::cout << " --> sub-matrices creation per sec = " << total_time / (k + 1) / SUBS_NUM << std::endl;
    }
}
