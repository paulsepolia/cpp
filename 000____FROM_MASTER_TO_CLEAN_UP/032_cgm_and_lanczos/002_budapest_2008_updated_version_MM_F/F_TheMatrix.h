//  Title: The Real Symmetric Positive Definite matrix function
//
//  Interface: void Matrix_F(double** matrix , int dim)
//
//  Input: (1) int dim -> The dimension of the matrix
//
//  Output: (1) double** matrix -> The resultant matrix
//
//  Purpose: Gives back a very specific Real Symmetric Positive Definite matrix,
//		     matrix[0...dim-1][0...dim-1]

void Matrix_F(double **matrix, int dim) {

    if (matrixFlag == 1) {

        // Matrix 1

        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++) {
                if (i == j) {
                    matrix[i][j] = (i + 1.);
                } else {
                    matrix[i][j] = 0.0;
                }
            }
        }
    } else if (matrixFlag == 2) {

        // Matrix 2

        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++) {
                if (i == j) {
                    matrix[i][j] = 2.0;
                } else if (i - j == 1) {
                    matrix[i][j] = -1.0;
                } else if (-i + j == 1) {
                    matrix[i][j] = -1.0;
                } else {
                    matrix[i][j] = 0.0;
                }
            }
        }
    } else if (matrixFlag == 3) {

        // Matrix 3

        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++) {
                if (i == j) {
                    matrix[i][j] = (i + 1);
                } else if (i - j == 1) {
                    matrix[i][j] = -1.0;
                } else if (-i + j == 1) {
                    matrix[i][j] = -1.0;
                } else {
                    matrix[i][j] = 0.0;
                }
            }
        }
    } else if (matrixFlag == 4) {

        // Matrix 4

        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++) {
                if (i == j) {
                    matrix[i][j] = 2.5 * (i + 1.0) * (j + 1.0);
                } else {
                    matrix[i][j] = (i + 1.0) * (j + 1.0) / 3.0;
                }
            }
        }
    }
}
