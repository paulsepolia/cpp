//  This file contains C++ source codes from book:
//  Introduction to Programming with C++,
//  Y. Daniel Liang, 2007, Pearson Education.

//  Title: The partition array function
//
//  Interface: int partition (double* list, int first, int last)
//
//  Purpose:

int partition(double *list, int first, int last) {

    double pivot = list[first]; // Choose the first element as the pivot
    int low = first + 1;        // Index for forward search
    int high = last;            // Index for backward search

    while (high > low) {

        // Search forward from left
        while (low <= high && list[low] <= pivot) {
            low++;
        }

        // Search backward from right
        while (low <= high && list[high] > pivot) {
            high--;
        }

        // Swap two elements in the list
        if (high > low) {
            double temp = list[high];
            list[high] = list[low];
            list[low] = temp;
        }
    }

    while (high > first && list[high] >= pivot) {
        high--;
    }

    // Swap pivot with list[high]

    if (pivot > list[high]) {
        list[first] = list[high];
        list[high] = pivot;
        return high;
    } else {
        return first;
    }
}

//  Title: The Quick Sort function
//
//  Interface A: void Quick_Sort(double* list, int arraySize)
//
//  Interface B: void Quick_Sort(double* list ,int first ,int last)
//
//  Purpose: Take a list[0...arraySize-1] and gives it back sorted as list[0...arraySize-1]

// The helper function

void Quick_Sort(double *list, int first, int last) {

    if (last > first) {
        int pivotIndex = partition(list, first, last);
        Quick_Sort(list, first, pivotIndex - 1);
        Quick_Sort(list, pivotIndex + 1, last);
    }
}

// The main sorting function

void Quick_Sort(double *list, int arraySize) {
    Quick_Sort(list, 0, arraySize - 1);
}

