//  Title: The Conjugate Gradient Function - T.F. version
//
//  Interface: void CG_F_FT(double** a,
//	    			        int n,
//		    		        double* b,
//			    	        double* x)
//
//  Input:  (1) double** a -> The matrix variable
//	        (2) double* b  -> The vector variable
//	        (3) int n      -> The dimension of the vector, matrix
//
//
//  Output: (1) double* x -> The resultant unknown vector
//
//  Purpose: Takes as input a[0...n-1][0...n-1] and b[0...n-1]
//           and solves using the Conjugate Gradient algorithm the equation:
//           a[0...dim-1][0...dim-1] * x[0...dim-1] == b[0...dim-1],
//		     and gives back the vector x[0...dim-1]

void CG_F_FT(double *x, double **a, const double *b, int n) {

    auto *d = new double[n];   // [0..n-1] auxiliary vector d
    auto *r = new double[n];   // [0..n-1] auxiliary vector r
    auto *ti = new double[n];  // [0..n-1] auxiliary vector ti

    double q = 0.0;
    double beta, eta, alpa, alpa1, stl;
    double rm = 0.0;
    double tol = 1.0E-8;
    int j, k, l;

    for (j = 0; j < n; j++) {
        x[j] = ti[j] = 0.00;
        r[j] = b[j];
        d[j] = b[j];
    }

    for (j = 0; j < 2 * n; j++) {

        // OpenMP code 1. starts here.

# pragma omp parallel for\
                                       default( none )\
                                       if( ompA )\
                                       shared( a, d, n, ti )\
                                       private( k, l )\
                                       reduction( + : q )\
                                       num_threads( NT )\
                                       schedule( static )

        // OpenMP code 1. ends here.

        for (k = 0; k < n; k++) {

            q = 0.0;

            for (l = 0; l < n; l++) {
                q += a[k][l] * d[l];
            }

            ti[k] = q;
        }

        beta = eta = alpa = 0.00;

        for (k = 0; k < n; k++) {
            beta += d[k] * r[k];
            alpa += r[k] * r[k];
            eta += d[k] * ti[k];
        }

        if (eta == 0.00) {
            std::cout << " ERROR IN CGM : DIVIDED BY ZERO " << std::endl;
            system("pause");
            return;
        }

        stl = beta / eta;
        alpa1 = 0.00;

        for (k = 0; k < n; k++) {
            x[k] += stl * d[k];
            r[k] -= stl * ti[k];
            alpa1 += r[k] * r[k];
        }

        for (k = 0; k < n; k++) {
            d[k] = r[k] + (alpa1 / alpa) * d[k];
        }

        for (k = 0; k < n; k++) {

            if (k == 0) {
                rm = std::abs(r[k]);
            } else if (std::abs(r[k]) >= rm) {
                rm = std::abs(r[k]);
            }
        }

        if (std::abs(stl) < tol || rm < tol) {
            return;
        }
    }

    delete[] d; // Deleting temporary help space
    delete[] r; // Deleting temporary help space
    delete[] ti; // Deleting temporary help space
}

//  Title: The Conjugate Gradient Function. PGG version
//
//  Interface: inline void CG_F(double* RESTA,
//				                double** matrix,
//				                double* vector,
//				                int& dim)
//
//  Input: (1) double** matrix -> The matrix variable
//	       (2) double* vector -> The vector variable
//	       (3) int& dim -> The dimension of the vector, matrix
//
//
//  Output: (1) double* RESTA -> The resultant unknown vector
//
//  Purpose: Takes as input the matrix[0...dim-1][0...dim-1] and vector[0...dim-1]
//           and solves using the Conjugate Gradient algorithm the equation :
//           matrix[0...dim-1][0...dim-1] * RESTA[0...dim-1] == vector[0...dim-1],
//		     and gives back the vector RESTA[0...dim-1]

inline void CG_F_PGG(double *RESTA,
                     double **matrix,
                     const double *vector,
                     int &dim) {

    // Declarations of the local variables
    // Help RAM space creation

    auto *xMinus = new double[dim];    // Help 1
    auto *xN = new double[dim];        // Help 2
    auto *pMinus = new double[dim];    // Help 3
    auto *pN = new double[dim];        // Help 4
    auto **rN = new double *[dim + 2]; // Help 5

    for (int i = 0; i < dim + 2; i++) {
        rN[i] = new double[dim];
    }

    auto *RR0 = new double[dim];        // Help 6
    auto *dotP = new double[dim];       // Help 7

    // Initialization Conditions

    for (int i = 0; i < dim; i++) {
        xMinus[i] = 0.;
    }

    for (int i = 0; i < dim; i++) {
        rN[0][i] = vector[i];
    }

    for (int i = 0; i < dim; i++) {
        pMinus[i] = rN[0][i];
    }

    double alpha_n;
    double beta_n;

    // The main body of the algorithm

    for (int i = 1; i < dim + 2; i++) {

        // Step -> 1

        Dot_F(RR0, matrix, pMinus, dim);

        alpha_n = Dot_F(rN[i - 1], rN[i - 1], dim) / Dot_F(pMinus, RR0, dim);

        // Step -> 2

        ScaVe_F(RR0, alpha_n, pMinus, dim);

        AddVe_F(xN, xMinus, RR0, dim);

        // Step -> 3

        Dot_F(RR0, matrix, pMinus, dim);

        ScaVe_F(RR0, alpha_n, RR0, dim);

        SubVe_F(rN[i], rN[i - 1], RR0, dim);

        // Step -> 4
        // Re-Orthogonalisation here

        dotP[i - 1] = Dot_F(rN[i - 1], rN[i - 1], dim);

        Ortho_F(rN[i], rN[i], rN, dotP, i, dim);

        // Step -> 5

        beta_n = Dot_F(rN[i], rN[i], dim) / Dot_F(rN[i - 1], rN[i - 1], dim);

        // Step -> 6

        ScaVe_F(RR0, beta_n, pMinus, dim);

        AddVe_F(pN, rN[i], RR0, dim);

        // Step -> 7
        // Update conditions

        for (int i = 0; i < dim; i++) {
            pMinus[i] = pN[i];
            xMinus[i] = xN[i];
        }
    } // End of main body of the algorithm

    // The Results

    for (int i = 0; i < dim; i++) { RESTA[i] = xMinus[i]; }

    // Free up temporary help RAM space

    delete[] xMinus;       // Delete Help 1
    delete[] xN;           // Delete Help 2
    delete[] pMinus;       // Delete Help 3
    delete[] pN;           // Delete Help 4

    for (int i = 0; i < dim + 2; i++) {
        delete[] rN[i];
    }

    delete[] rN;           // Delete Help 5
    delete[] RR0;          // Delete Help 6
    delete[] dotP;         // Delete Help 7
}
