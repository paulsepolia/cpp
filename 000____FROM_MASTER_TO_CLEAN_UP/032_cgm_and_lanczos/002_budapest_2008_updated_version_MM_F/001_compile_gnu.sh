#!/bin/bash

  g++-9.2.0   -O3                \
              -Wall              \
              -std=gnu++2a       \
              -fopenmp           \
              -pthread           \
              driver_program.cpp \
              -o x_gnu
