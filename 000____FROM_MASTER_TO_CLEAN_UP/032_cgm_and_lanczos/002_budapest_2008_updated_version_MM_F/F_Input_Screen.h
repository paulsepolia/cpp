#include "F_LanczosAlgorithm.h"

void Input_Screen_F() {

    // all the inputs from the keyboard

    std::cout << " =========================================================== " << std::endl;
    std::cout << "  " << std::endl;
    std::cout << " The Lanczos' Iterative Diagonaliser. " << std::endl;
    std::cout << "  " << std::endl;
    std::cout << " C++ && OpenMP, non-sparse syntax use version. " << std::endl;
    std::cout << "  " << std::endl;
    std::cout << " =========================================================== " << std::endl;
    std::cout << "  " << std::endl;

    std::cout << "  1. Give the number of elements of each Lanczos' vector    ----->> ";
    int n_LVE;
    std::cin >> n_LVE; // The number of elements of each Lanczos' vector

    std::cout << "  2. Give how many eigenvalues do you want to take back     ----->> ";
    int n_EigVa; // The number of eigenvalues to take back.
    std::cin >> n_EigVa;

    std::cout << "  3. Give the module number                                 ----->> ";
    int modNu; // The starting dimension of the tri-diagonal matrix
    std::cin >> modNu; // and current step in each iteration.

    std::cout << "  4. Give the 'error' convergence constant                  ----->> ";
    double the_error;  // The maximum difference for convergence
    std::cin >> the_error;

    std::cout << "  5. To use the OpenMP directives 'ompA'       (yes/1,no/0) ----->> ";
    std::cin >> ompA; // 'ompA' Global OpenMP variable

    std::cout << "  6. To use the OpenMP directives 'ompB'       (yes/1,no/0) ----->> ";
    std::cin >> ompB; // 'ompB' Global OpenMP variable

    std::cout << "  7. Give the number of OpenMP threads                      ----->> ";
    // 'NT' Global OpenMP variable.
    std::cin >> NT; // Number of threads for parallel execution

    std::cout << "  8. Choose the matrix                            (1/2/3/4) ----->> ";
    // 'matrixFlag' Global variable
    std::cin >> matrixFlag;

    std::cout << "  9. To use the Conjugate Gradient Method      (yes/1,no/0) ----->> ";
    // 'conjuFlag' Global  variable.
    std::cin >> conjuFlag;

    std::cout << " 10. To use the 'Quick' or the 'Slow' CGM         (Q/1,S/0) ----->> ";
    // 'conjuTibor' Global  variable.
    std::cin >> conjuTibor;

    std::cout << "  " << std::endl;
    std::cout << " Please Wait ................. " << std::endl;
    std::cout << "  " << std::endl;

    // declaration of the problem's matrix

    auto **MatrixMP = new double *[n_LVE];
    for (int i = 0; i < n_LVE; i++) { MatrixMP[i] = new double[n_LVE]; }

    Matrix_F(MatrixMP, n_LVE);

    // declaration of the first 2 Lanczos' vectors

    auto **LaVe = new double *[n_LVE + 1];

    for (int i = 0; i < 2; i++)      // Only 2 Lanczos vectors !
    { LaVe[i] = new double[n_LVE]; } // The rest are built dynamically.

    Make_LV_F(LaVe, n_LVE);

    // declaration the ram space for eigenvalues

    auto *alphaMP = new double[n_LVE];

    // initializing and starting the Lanczos' Algorithm

    time_t t1;
    t1 = clock();

    Lanczos_Algorithm(MatrixMP,        // The system's matrix
                      LaVe,            // The Lanczos' vectors
                      alphaMP,         // The eigenvalues
                      n_LVE,           // The dimension of the matrix
                      n_EigVa,         // Number of eigenvalues to take back
                      modNu,           // the step
                      the_error);      // the error

    // timing

    time_t t2;
    t2 = clock();

    std::cout << " ================================================= " << std::endl;
    std::cout << " The Cpu time used is -->> " << (t2 - t1) / CLOCKS_PER_SEC << std::endl;
    std::cout << " ================================================= " << std::endl;
}
