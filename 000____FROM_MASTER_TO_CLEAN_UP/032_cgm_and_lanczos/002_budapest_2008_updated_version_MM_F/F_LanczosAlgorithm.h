# include <iostream>
# include <cmath>
# include <omp.h>
# include "F_CommonVariables.h"
# include "F_NumericalRecipes.h"
# include "F_QuickSort.h"
# include "F_LinearAlgebra.h"
# include "F_TheMatrix.h"
# include "F_Orthogonalizer.h"
# include "F_ConjugateGradient.h"

//  Title: Making ram space for Lanczos' vectors
//
//  Interface: double** Make_LV_F double** LV , int& dim)
//
//  Input: (1) int& dim -> The dimension of each Lanczos' vector
//
//  Output: (1) LV[0] -> The zero order Lanczos' vector
//	        (2) LV[1] -> The first order Lanczos' vector
//
//  Purpose: Initialization of the first 2 elements
//           LV[0][0...dim-1] and LV[1][0...dim-1]
//		     of the already declared ram space LV*[dim]

void Make_LV_F(double **LV, int &dim) {

    // Definition of LV[0]

    for (int i = 0; i < dim; i++) {
        LV[0][i] = static_cast<double>(0.0);
    }

    // Definition of LV[1]

    for (long i = 0; i < dim; i++) {
        LV[1][i] = 1.0 / std::sqrt(static_cast<double>(dim));
    }

}

//  Title: The Lanczos' Algorithm
//
//  Interface: void(double** matrix,
//				    double** LaVe,
//				    double* alphaMP,
//				    int dim,
//				    int n_EigVa,
//				    int modNu,
//				    double the_error)
//
//  Input: (1) double** matrix  -> The problem's matrix
//		   (2) double** LaVe -> The first 2 Lanczos' vectors
//		   (3) int dim -> The common dimension of Lanczos' vectors and matrix
//		   (4) int n_EigVa -> The number of eigenvalues to take back
//	       (5) int mod_Nu -> the step
//		   (6) double the_error -> the accuracy of the results
//
//  Output: (1) double** LaVe -> The produced Lanczos' vectors
//		    (2) double* alphaMP -> The produced eigenvalues
//
//  Purpose: Takes as input the matrix[0...dim-1] and applies the
//           Lanczos' Iterative Diagonalisation algorithm
//		     to give back the Lanczos' vectors LaVe[0...n_EigVa][0...dim-1],
//		     the alphaMP[1...n_EigVa] eigenvalues

void Lanczos_Algorithm(double **matrix,     // The system's matrix
                       double **LaVe,       // The Lanczos' vectors
                       double *alphaMP,     // The eigenvalues
                       int dim,             // The dimension of the matrix
                       int n_EigVa,         // Number of eigenvalues
                       int modNu,           // The step
                       double the_error) {  // The accuracy of the results

    // Creating the wCGM vector[dim]
    // Help 1

    double *wCGM;

    // Creating ram space for the wMP matrix[n_LVE+1][dim]

    auto **wMP = new double *[dim];

    // Help 2
    for (int i = 0; i <= dim; i++) {
        wMP[i] = new double[dim];
    }

    // Other help RAM space

    auto *RR0 = new double[dim];    // Help 3
    auto *RR1 = new double[dim];    // Help 4
    auto *RR2 = new double[dim];    // Help 5
    auto *RR3 = new double[dim];    // Help 6
    auto *diff = new double[dim];   // Help 7
    auto *betaMP = new double[dim]; // Help 8
    auto *dotP = new double[dim];   // Help 9

    // Initializing the first two Lanczos' vectors

    Make_LV_F(LaVe, dim);

    // Set the loop iteration counter to zero

    int j = 0;

    // Main body of the Lanczos' algorithm

    for (;;) {

        j++;

        time_t t1 = clock();

        std::cout << " ================================================================= " << std::endl;
        std::cout << " 1. Inside Lanczos' && Iteration is           ---->>>>  " << j << std::endl;

        // 1
        std::cout << " 2. Inside Conjugate Gradient && Iteration is ---->>>>  " << j << std::endl;

        time_t t3 = clock();

        // 1.a
        if (conjuFlag && conjuTibor) {
            CG_F_FT(RR0, matrix, LaVe[j], dim);
        }

        // 1.b
        if (conjuFlag && !conjuTibor) {
            CG_F_PGG(RR0, matrix, LaVe[j], dim);
        }

        time_t t4 = clock();

        std::cout << " 3. Cpu time used for Conjugate Gradient is   ---->>>>  "
                  << (t4 - t3) / 10000. << std::endl;

        if (!conjuFlag) { Dot_F(RR0, matrix, LaVe[j], dim); }   // 1.b

        wCGM = RR0;                                             // 1.c

        // 2

        ScaVe_F(RR1, betaMP[j - 1], LaVe[j - 1], dim);          // 2.a

        SubVe_F(RR1, wCGM, RR1, dim);                           // 2.b

        wMP[j] = RR1;                                           // 2.c

        // 3

        alphaMP[j] = Dot_F(wMP[j], LaVe[j], dim);   // 3.a

        // 4

        ScaVe_F(RR2, alphaMP[j], LaVe[j], dim);     // 4.a

        SubVe_F(RR2, wMP[j], RR2, dim);             // 4.b

        wMP[j] = RR2;                               // 4.c

        // 5

        betaMP[j] = pow(Dot_F(wMP[j], wMP[j], dim), 0.5);   // 5.a

        // 6

        ScaVe_F(RR3, 1. / betaMP[j], wMP[j], dim);          // 6.a

        LaVe[j + 1] = RR3;                                  // 6.b

        // 7. Re-orthogonalising here

        // Dynamic creation ram space for the next Lanczos' vector

        auto *RR4 = new double[dim];                            // 7.a

        dotP[j] = Dot_F(LaVe[j], LaVe[j], dim);                 // 7.b

        Ortho_F(RR4, LaVe[j + 1], LaVe + 1, dotP + 1, j, dim);  // 7.c

        LaVe[j + 1] = RR4;                                      // 7.d

        time_t t2 = clock();

        std::cout << " 4. Cpu time used for Lanczos' is             ---->>>>  "
                  << (t2 - t1) / 1000.0 << std::endl;

        // 8. Checking for convergence

        std::cout.precision(16);

        if ((j > 1 * n_EigVa && j % modNu == 0) || j == dim + 1) {

            // Set A
            for (int i = 0; i < j; i++) {
                RR0[i] = alphaMP[i];
                RR1[i] = betaMP[i];
            }

            // Set B
            for (int i = 0; i < j - 1; i++) {
                RR2[i] = alphaMP[i];
                RR3[i] = betaMP[i];
            }

            std::cout << " ---------->>>> testing for convergence " << j << std::endl;

            tqli_NR(RR0, RR1 - 1, j - 1); // Set A. Dot_Matrix method
            tqli_NR(RR2, RR3 - 1, j - 2); // Set B. Dot_Matrix

            if (conjuFlag) {
                for (int i = 1; i < j; i++)    // Set A. Conjugate Gradient method
                { RR0[i] = 1. / RR0[i]; }

                for (int i = 1; i < j - 1; i++)  // Set B. Conjugate Gradient method
                { RR2[i] = 1. / RR2[i]; }
            }

            Quick_Sort(RR0, j);     // Set A
            Quick_Sort(RR2, j - 1); // Set B

            for (int i = 1; i < n_EigVa - 1; i++) { diff[i] = fabs(RR0[i] - RR2[i]); }

            Quick_Sort(diff, n_EigVa - 1);

            if (diff[n_EigVa - 2] < the_error) {
                for (int i = 1; i <= n_EigVa; i++) {
                    std::cout << " the eigenvalues are " << i
                              << " ------------------------>>>>>  "
                              << RR0[i] << std::endl;
                }
                break;
            } else if (j == dim + 1) {
                std::cout << " ---->>> Inside the Final Trial " << std::endl;
                for (int i = 1; i <= n_EigVa; i++) { std::cout << i << " -->> " << RR0[i] << std::endl; }
                break;
            }
        } // Convergence loop ends here
    } // Infinite loop is finished

    // Free up temporary help RAM space

    delete[] RR0;                          // Deletion of Help 3
    delete[] RR1;                          // Deletion of Help 4
    delete[] RR2;                          // Deletion of Help 5
    delete[] wMP;                          // Deletion of Help 2
    delete[] RR3;                          // Deletion of Help 6
    delete[] diff;                         // Deletion of Help 7
    delete[] betaMP;                       // Deletion of Help 8
    delete[] dotP;                         // Deletion of Help 9
}
