// The Projection Function

inline void proj_F(double *RESTA, double *u, double *v, double &dotP, int &dim) {

    double RR0, RR1, RR2;

    RR0 = Dot_F(u, v, dim);       // 1
    RR1 = dotP;                   // 2
    RR2 = RR0 / RR1;              // 3
    ScaVe_F(RESTA, RR2, u, dim);  // 4
}

//  Title: The Orthogonalisation function.
//
//  Interface: inline void Ortho_F(double* RESTA,
//                                 double* vecOld,
//	            		           double** vecSet,
//                                 int& nSet,
//					               int& dim)
//
//  Input: (1) double* vecOld -> The old vector to be orthogonalised
//		   (2) double** vecSet -> The vector base set
//		   (4) int& nSet -> The number of base vectors
//		   (5) int& dim -> The dimension of vectors
//
//  Output: (1) double* RESTA -> The orthogonalised resultant vector
//
//  Purpose: Gives back the vector RESTA[0...dim-1], which is the orthogonalized
//           version of vecOld[0...dim-1], according to the orthogonal base
//		     vecSet[0...nSet-1][0...dim-1]

inline void Ortho_F(double *RESTA,
                    double *vecOld,
                    double **vecSet,
                    double *dotP,
                    int nSet,
                    int dim) {

    // Declaring RAM space for the projections

    auto **RR0 = new double *[nSet];
    for (int i = 0; i < nSet; i++) {
        RR0[i] = new double[dim];
    }

    // Finding all the projections

    for (int i = 0; i < nSet; i++) {
        proj_F(RR0[i], vecSet[i], vecOld, dotP[i], dim);
    }

    SubVe_F(RESTA, vecOld, RR0[0], dim);

    for (int i = 1; i < nSet; i++) {
        SubVe_F(RESTA, RESTA, RR0[i], dim);
    }

    // Free-Up help RAM space

    for (int i = 0; i < nSet; i++) {
        delete[] RR0[i];
    }
    delete[] RR0;
}
