#!/bin/bash

  clang++   -O3         \
            -Wall       \
            -std=c++1z  \
            -pthread    \
            -fopenmp    \
            driver_program.cpp \
            -o x_clang
