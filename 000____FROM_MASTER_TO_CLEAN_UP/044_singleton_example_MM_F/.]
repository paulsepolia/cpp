#include <iostream>
#include <chrono>
#include <thread>

class ASingleton {

public:

	static auto getInstance() -> ASingleton&
	{
		static auto anInstance = ASingleton();
		return anInstance;
	}

	ASingleton(const ASingleton&) = delete;

	ASingleton& operator=(const ASingleton&) = delete;

	ASingleton(ASingleton&&) = delete;

	ASingleton& operator=(const ASingleton&&) = delete;

	auto PrintTimeUsed() const -> void {

		const auto t_loc { std::chrono::steady_clock::now() };

		const auto total_time { std::chrono::duration_cast<
				std::chrono::duration<double>>(t_loc - _t_prev).count() };

		_t_prev = t_loc;

		std::cout.precision(5);
		std::cout << std::fixed;
		std::cout << " --> time used (seconds) = " << total_time
				<< std::endl;
	}

	auto PrintTotalTimeUsed() const -> void {

		const auto t_loc { std::chrono::steady_clock::now() };

		const auto total_time { std::chrono::duration_cast<
				std::chrono::duration<double>>(t_loc - _t1).count() };

		std::cout.precision(5);
		std::cout << std::fixed;
		std::cout << " --> total time used up to now (seconds) = " << total_time
				<< std::endl;
	}

	~ASingleton() {
		_t2 = std::chrono::steady_clock::now();

		const auto total_time { std::chrono::duration_cast<
				std::chrono::duration<double>>(_t2 - _t1).count() };

		std::cout.precision(5);
		std::cout << std::fixed;
		std::cout << " --> total time used until exit of the scope (seconds) = "
				<< total_time << std::endl;
	}

private:

	ASingleton() :
			_t1 { std::chrono::steady_clock::now() }, _t2 {
					std::chrono::steady_clock::now() }, _t_prev {
					std::chrono::steady_clock::now() } {
	}
	;

	decltype(std::chrono::steady_clock::now()) _t1 { };
	decltype(std::chrono::steady_clock::now()) _t2 { };
	mutable decltype(std::chrono::steady_clock::now()) _t_prev { };
};

auto main() -> int {

	{
		std::cout << "----------------------------------->> 1" << std::endl;

		ASingleton::getInstance().PrintTimeUsed();
		ASingleton::getInstance().PrintTotalTimeUsed();

		std::this_thread::sleep_for(std::chrono::seconds(2));

		ASingleton::getInstance().PrintTimeUsed();
		ASingleton::getInstance().PrintTotalTimeUsed();

		std::this_thread::sleep_for(std::chrono::seconds(3));

		ASingleton::getInstance().PrintTimeUsed();
		ASingleton::getInstance().PrintTotalTimeUsed();

		std::this_thread::sleep_for(std::chrono::seconds(4));

		ASingleton::getInstance().PrintTimeUsed();
		ASingleton::getInstance().PrintTotalTimeUsed();

		std::this_thread::sleep_for(std::chrono::seconds(5));
	}

	{
		std::cout << "----------------------------------->> 2" << std::endl;

		ASingleton::getInstance().PrintTimeUsed();
		ASingleton::getInstance().PrintTotalTimeUsed();

		std::this_thread::sleep_for(std::chrono::seconds(2));

		ASingleton::getInstance().PrintTimeUsed();
		ASingleton::getInstance().PrintTotalTimeUsed();

		std::this_thread::sleep_for(std::chrono::seconds(3));

		ASingleton::getInstance().PrintTimeUsed();
		ASingleton::getInstance().PrintTotalTimeUsed();

		std::this_thread::sleep_for(std::chrono::seconds(4));

		ASingleton::getInstance().PrintTimeUsed();
		ASingleton::getInstance().PrintTotalTimeUsed();

		std::this_thread::sleep_for(std::chrono::seconds(5));
	}

	{
			std::cout << "----------------------------------->> 2" << std::endl;

			ASingleton::getInstance().PrintTimeUsed();
			ASingleton::getInstance().PrintTotalTimeUsed();

			std::this_thread::sleep_for(std::chrono::seconds(2));

			ASingleton::getInstance().PrintTimeUsed();
			ASingleton::getInstance().PrintTotalTimeUsed();

			std::this_thread::sleep_for(std::chrono::seconds(3));

			ASingleton::getInstance().PrintTimeUsed();
			ASingleton::getInstance().PrintTotalTimeUsed();

			std::this_thread::sleep_for(std::chrono::seconds(4));

			ASingleton::getInstance().PrintTimeUsed();
			ASingleton::getInstance().PrintTotalTimeUsed();
		}
}
