#pragma once

#include <chrono>

class BSingleton {

public:

	static auto getInstance() -> BSingleton*;

	auto PrintTimeUsed() const -> void;

	auto PrintTotalTimeUsed() const -> void;

	~BSingleton();

	BSingleton(const BSingleton&) = delete;

	BSingleton& operator=(const BSingleton&) = delete;

	BSingleton(BSingleton&&) = delete;

	BSingleton& operator=(const BSingleton&&) = delete;

public:

	static BSingleton* _anInstance;

	BSingleton();

	decltype(std::chrono::steady_clock::now()) _t1 { };
	decltype(std::chrono::steady_clock::now()) _t2 { };
	mutable decltype(std::chrono::steady_clock::now()) _t_prev { };
};
