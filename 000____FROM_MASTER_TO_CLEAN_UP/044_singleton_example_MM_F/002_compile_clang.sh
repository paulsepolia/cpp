#!/bin/bash

  clang++   -O3           \
            -Wall         \
            -std=c++17    \
            -pthread      \
            -pedantic     \
            ASingleton.cpp\
            BSingleton.cpp\
            main.cpp      \
            -o x_clang

