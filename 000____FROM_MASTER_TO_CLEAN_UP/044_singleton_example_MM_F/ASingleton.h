#pragma once

#include <chrono>

class ASingleton {

public:

	static auto getInstance() -> ASingleton&;

	auto PrintTimeUsed() const -> void;

	auto PrintTotalTimeUsed() const -> void;

	~ASingleton();

	ASingleton(const ASingleton&) = delete;

	ASingleton& operator=(const ASingleton&) = delete;

	ASingleton(ASingleton&&) = delete;

	ASingleton& operator=(const ASingleton&&) = delete;

public:

	ASingleton();

	decltype(std::chrono::steady_clock::now()) _t1 { };
	decltype(std::chrono::steady_clock::now()) _t2 { };
	mutable decltype(std::chrono::steady_clock::now()) _t_prev { };
};
