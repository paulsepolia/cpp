#!/bin/bash

  g++-9.2.0 -O3           \
            -Wall         \
            -std=gnu++2a  \
            -pthread      \
            -fopenmp      \
            -pedantic     \
            ASingleton.cpp\
            BSingleton.cpp\
            main.cpp      \
            -o x_gnu

