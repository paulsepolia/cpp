#include <thread>
#include <algorithm>
#include <iostream>
#include <functional>
#include <cmath>

const double SUM_MAX = std::pow(10.0, 9.0);
const auto K_MAX = static_cast<size_t>(2 * std::pow(10.0, 2.0));

void fun() {

    double sum_loc = 0;
    while (true) {
        sum_loc += 1;
        if (sum_loc >= SUM_MAX) break;
    }
}

int main() {

    const auto THREADS_MAX = static_cast<size_t>(std::pow(10.0, 2.0));
    std::vector<std::thread> vec;

    size_t k = 0;
    while (k < K_MAX) {

        vec.emplace_back(std::thread(fun));

        if (vec.size() % THREADS_MAX == 0) {
            std::for_each(vec.begin(), vec.end(), std::mem_fn(&std::thread::join));
            vec.clear();
            vec.shrink_to_fit();
        }
        k++;
    }

    std::for_each(vec.begin(), vec.end(), std::mem_fn(&std::thread::join));

    return 0;
}