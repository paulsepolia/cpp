#include <iostream>
#include <vector>

class A {
public:

    A(uint32_t x_arg) : x(x_arg) {
        std::cout << "A (x_arg) " << std::endl;
    }

    A() {
        x = 0;
        std::cout << "A ()" << std::endl;
    }

    A(const A &rhs) noexcept {
        x = rhs.x;
        std::cout << "A (A &)" << std::endl;
    }

    A(A &&rhs) noexcept {
        x = rhs.x;
        std::cout << "A (A &&)" << std::endl;
    }

private:
    uint32_t x;
};

int main() {

    {
        std::vector<A> a;
        std::cout << "call emplace_back:" << std::endl;
        a.emplace_back(0);
    }

    {
        std::vector<A> a;
        std::cout << "call push_back:" << std::endl;
        a.push_back(1);
    }

    {
        std::vector<A> a;
        std::cout << "call emplace_back with object:" << std::endl;
        const uint32_t k1 = 100;
        a.emplace_back(k1);
    }

    {
        std::vector<A> a;
        std::cout << "call push_back with object:" << std::endl;
        const uint32_t k1 = 100;
        a.push_back(k1);
    }

    return 0;
}