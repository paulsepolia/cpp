#include <iostream>
#include <vector>

class A {

public :

    A() : dim1(0), d1(nullptr) {}

    explicit A(const uint64_t &dim) : dim1(dim), d1(new double[dim]) {

        for (uint64_t i = 0; i < dim1; i++) {
            d1[i] = i;
        }
    }

    A(const A &a) : dim1(0), d1(nullptr) {
        dim1 = a.dim1;
        delete[] d1;
        d1 = new double[a.dim1];
        for (uint64_t i = 0; i < dim1; i++) {
            d1[i] = a.d1[i];
        }
    }

    A &operator=(const A &a) {
        if (this != &a) {
            dim1 = a.dim1;
            delete[] d1;
            d1 = new double[a.dim1];
            for (uint64_t i = 0; i < a.dim1; i++) {
                d1[i] = a.d1[i];
            }
        }
        return *this;
    }

    A(A &&a) noexcept : dim1(0), d1(nullptr) {
        dim1 = a.dim1;
        delete[] d1;
        d1 = a.d1;
        a.dim1 = 0;
        a.d1 = nullptr;
    }

    A &operator=(A &&a) noexcept {
        if (this != &a) {
            dim1 = a.dim1;
            delete[] d1;
            d1 = a.d1;
            a.dim1 = 0;
            a.d1 = nullptr;
        }
        return *this;
    }

    virtual ~A() {
        delete[] d1;
    }

    virtual double get_data1(const uint64_t &i) const {
        if (d1 == nullptr) return 0;
        return d1[i];
    }

    virtual double get_data2(const uint64_t &i) const {
        if (d1 == nullptr) return 0;
        return d1[i];
    }

private:

    uint64_t dim1;
    double *d1;
};

class B : public A {

public :

    B() : A(), dim2(0), d2(nullptr) {}

    explicit B(const uint64_t &dim) : A(dim), dim2(dim), d2(new double[dim]) {

        for (uint64_t i = 0; i < dim2; i++) {
            d2[i] = 100 + i;
        }
    }

    B(const B &b) : A(b), dim2(0), d2(nullptr) {
        dim2 = b.dim2;
        d2 = new double[dim2];
        for (uint64_t i = 0; i < dim2; i++) {
            d2[i] = b.d2[i];
        }
    }

    B &operator=(const B &b) {
        if (this != &b) {
            A::operator=(b);
            dim2 = b.dim2;
            delete[] d2;
            d2 = new double[dim2];
            for (uint64_t i = 0; i < dim2; i++) {
                d2[i] = b.d2[i];
            }
        }
        return *this;
    }

    B(B &&b) noexcept : A(static_cast<A &&>(b)), dim2(0), d2(nullptr) {
        dim2 = b.dim2;
        d2 = b.d2;
        b.dim2 = 0;
        b.d2 = nullptr;
    }

    B &operator=(B &&b) noexcept {
        if (this != &b) {
            dim2 = b.dim2;
            delete[] d2;
            d2 = b.d2;
            A::operator=(std::move(b));
            b.dim2 = 0;
            b.d2 = nullptr;
        }
        return *this;
    }

    ~B() final {
        delete[] d2;
    }

    double get_data2(const uint64_t &i) const final {
        if (d2 == nullptr) return 0;
        return d2[i];
    }

private:

    uint64_t dim2;
    double *d2;
};

int main() {

    const uint64_t DIM_MAX = 100;

    {
        for (uint64_t k = 0; k <= DIM_MAX; k++) {

            A a1(DIM_MAX);

            std::cout << a1.get_data1(12) << std::endl;

            A a2;
            a2 = a1;

            std::cout << a2.get_data1(11) << std::endl;
            std::cout << a2.get_data1(12) << std::endl;

            a2 = std::move(a1);

            std::cout << a2.get_data1(13) << std::endl;
            std::cout << a2.get_data1(14) << std::endl;
        }
    }

    {
        for (uint64_t k = 0; k <= DIM_MAX; k++) {

            B b1(DIM_MAX);
            B b2(b1);
            b2 = b1;
            std::cout << b1.get_data1(21) << std::endl;
            std::cout << b1.get_data1(22) << std::endl;
            std::cout << b2.get_data1(23) << std::endl;
            std::cout << b2.get_data1(24) << std::endl;
            std::cout << b2.get_data2(23) << std::endl;
            std::cout << b2.get_data2(24) << std::endl;
        }
    }

    {
        for (uint64_t k = 0; k <= DIM_MAX; k++) {

            B b1(DIM_MAX);
            B b2(std::move(b1));
            std::cout << b2.get_data1(23) << std::endl;
            std::cout << b2.get_data1(24) << std::endl;
            std::cout << b2.get_data2(23) << std::endl;
            std::cout << b2.get_data2(24) << std::endl;
        }
    }

    return 0;
}