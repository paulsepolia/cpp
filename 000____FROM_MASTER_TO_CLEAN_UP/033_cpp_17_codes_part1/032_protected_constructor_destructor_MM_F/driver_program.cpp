#include <iostream>
#include <iomanip>
#include <cmath>
#include <chrono>
#include <vector>


class A {

protected:

    A() {
        std::cout << "con --> A" << std::endl;
    };

    virtual ~A() {
        std::cout << "de --> ~A" << std::endl;
    }

public:

    void funB() const {
        std::cout << "A : here I am" << std::endl;
    }

    void funA(const A &a) const {

        a.funB();
    }
};

class B : public A {

public:

    B(){
        std::cout << "con --> B" << std::endl;
    };

    void funA(const B &b) const {

        std::cout << "B : here I am" << std::endl;
        b.funB();
    }

     ~B() final {
        std::cout << "de --> ~B" << std::endl;
    }
};

int main() {

    A *pa;

    pa->funA(*pa);
    pa->funB();

    B b;

    b.funA(b);
    b.funB();
}