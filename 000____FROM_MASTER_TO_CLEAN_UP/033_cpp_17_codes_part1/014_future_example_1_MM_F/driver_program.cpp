// future example

#include <iostream>
#include <future>
#include <chrono>

// a non-optimized way of checking for prime numbers

const uint64_t PRIME_NUM = 444444443;

static uint64_t index = 0;

bool is_prime(uint64_t x) {
    bool flg = true;

    for (int i2 = 0; i2 < 10; i2++) {
        index = 0;
        std::cout << std::endl;
        std::cout << "---------------------->> " << i2 << std::endl << std::flush;
        flg = true;
        for (uint64_t i = 2; i < x; ++i) {
            if (x % i == 0) flg = false;
        }
    }

    return flg;
}

int main() {

    // call function asynchronously
    std::future<bool> fut = std::async(is_prime, PRIME_NUM);

    // do something while waiting for function to set future:
    std::cout << "checking, please wait";
    std::chrono::milliseconds span(100);

    while (fut.wait_for(span) == std::future_status::timeout) {
        index++;
        std::cout << index << " " << std::flush;
    }

    bool x = fut.get();     // retrieve return value

    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << std::endl << PRIME_NUM << (x ? " is prime." : " is not prime.") << std::endl;

    return 0;
}
