#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <cmath>
#include <unordered_set>
#include <unordered_map>
#include <random>

class A {
public:

    explicit A(const double &val) : _val(val) {}

    virtual ~A() = default;

    friend bool operator<(const A &v1, const A &v2) {
        return v1._val < v2._val;
    }

    bool operator==(const A &v) const {
        return _val == v._val;
    }

    double get_val() const {
        return _val;
    }

private:

    double _val;
};

namespace std {

    template<>
    struct hash<A> {
        size_t operator()(const A &obj) const {
            return std::hash<double>()(obj.get_val());
        }
    };
}

int main() {

    const auto dim = static_cast<size_t>(std::pow(10.0, 1.0));
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_real_distribution<double> dist(1.0, static_cast<double>(dim));

    {
        std::cout << " --> set and class A" << std::endl;

        std::set<A> s;

        for (size_t i = 0; i < dim; i++) {
            s.insert(A(dist(mt)));
        }

        std::cout << s.size() << std::endl;
        for (const auto &el: s) {
            std::cout << el.get_val() << std::endl;
        }
    }

    {
        std::cout << " --> map and class A" << std::endl;

        std::map<A, size_t> m;

        for (size_t i = 0; i < dim; i++) {
            m.insert(std::pair<A, size_t>(A(dist(mt)), i));
        }

        std::cout << m.size() << std::endl;
        for (const auto &el: m) {
            std::cout << el.first.get_val() << " --> " << el.second << std::endl;
        }
    }

    {
        std::cout << " --> unordered set and class A" << std::endl;

        std::unordered_set<A> un_s;

        for (size_t i = 0; i < dim; i++) {
            un_s.insert(A(dist(mt)));
        }

        std::cout << un_s.size() << std::endl;
        for (const auto &el: un_s) {
            std::cout << el.get_val() << std::endl;
        }
    }

    {
        std::cout << " --> unordered map and class A" << std::endl;

        std::unordered_map<A, size_t> un_m;

        for (size_t i = 0; i < dim; i++) {
            un_m.insert(std::pair<A, size_t>(A(dist(mt)), i));
        }

        std::cout << un_m.size() << std::endl;
        for (const auto &el: un_m) {
            std::cout << el.first.get_val() << " --> " << el.second << std::endl;
        }
    }

    return 0;
}