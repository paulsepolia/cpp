#include <iostream>
#include <cmath>
#include <chrono>
#include <iomanip>
#include <vector>
#include <memory>
#include <thread>
#include <mutex>

const auto DIM_DATA = static_cast<uint64_t>(std::pow(10.0, 8.0));
const auto DO_MAX = static_cast<uint64_t>(std::pow(10.0, 8.0));
const double ERROR_DIFF = 2.0;

std::mutex mtx;
std::vector<double> data_vec;

void write_data(double & time_write)
{
	std::unique_lock<std::mutex> lck(mtx, std::defer_lock);
	lck.lock();

	const auto t1 = std::chrono::steady_clock::now();

	std::cout << "--> writing data..." << std::endl;

	for (uint64_t i = 0; i < DIM_DATA; i++)
	{
		data_vec.push_back(1.0 / DIM_DATA);
	}

	const auto t2 = std::chrono::steady_clock::now();
	time_write = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count();

	lck.unlock();
}

void delete_data()
{
	std::cout << "--> deleting data..." << std::endl;
	data_vec.clear();
	data_vec.shrink_to_fit();
}

void read_delete_data(double& sum_total, double & time_read_del)
{
	std::unique_lock<std::mutex> lck(mtx, std::defer_lock);
	lck.lock();

	const auto t1 = std::chrono::steady_clock::now();

	std::cout << "--> reading data..." << std::endl;

	for (const auto& el : data_vec)
	{
		sum_total += el;
	}

	delete_data();

	const auto t2 = std::chrono::steady_clock::now();
	time_read_del = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count();

	lck.unlock();
}

int main() {

	std::cout << std::fixed;
	std::cout << std::setprecision(8);

	double sum_global = 0;

	for (uint64_t i = 1; i <= DO_MAX; i++)
	{
		double sum_loc = 0;
		double time_read_del = 0;
		double time_write = 0;
		std::thread t_w_data(write_data, std::ref(time_write));
		std::thread t_rd_data(read_delete_data, std::ref(sum_loc), std::ref(time_read_del));

		t_w_data.join();
		t_rd_data.join();

		std::cout << "--> local sum is = " << sum_loc << std::endl;
		std::cout << "-------------------------------->> run  = " << i << std::endl;
		sum_global += sum_loc;
		std::cout << "-------------------------------->> s_gl = " << sum_global << std::endl;
		std::cout << "-------------------------------->> t_rd = " << time_read_del << std::endl;
		std::cout << "-------------------------------->> t_w  = " << time_write << std::endl;

		if (sum_global - static_cast<double>(i) > ERROR_DIFF)
		{
			std::cout << "ERROR!" << std::endl;
			exit(-1);
		}
	}

	int sentinel;
	std::cout << "Enter integer to exit local scope: ";
	std::cin >> sentinel;
	std::cout << "pgg --> after the exit" << std::endl;
}
