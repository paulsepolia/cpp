#include <iostream>
#include <thread>
#include <cmath>
#include <vector>
#include <mutex>

std::mutex mtx;

void add_number(const uint64_t & i) {

    const auto MAX_LIMIT = static_cast<uint64_t>(std::pow(10.0, 10.0));

    mtx.lock();
    std::cout << "label = " << i << std::endl;
    mtx.unlock();

    double_t sum = 0;

    while (true) {
        sum += 1.0;
        if (sum > static_cast<double_t>(MAX_LIMIT)) break;
    }

}

int main() {

    const uint64_t NUM_THREADS = 10;
    std::vector<std::thread> threads;

    // span threads

    for (uint64_t i = 0; i != NUM_THREADS; i++) {

        std::thread th(add_number, i);
        threads.push_back(std::move(th));
    }

    // join threads

    for (auto &th: threads) th.join();

    return 0;
}