#include <iostream>
#include <thread>

void threadCallback(size_t *px) {
    (*px)++;
    std::cout << "Inside Thread x = " << (*px) << std::endl;
}

int main() {

    size_t x = 9;
    size_t * px = &x;
    std::cout << "In Main Thread : Before Thread Start x = " << x << std::endl;
    std::thread threadObj(threadCallback, px);
    threadObj.join();
    std::cout << "In Main Thread : After Thread Joins x = " << x << std::endl;
    return 0;
}