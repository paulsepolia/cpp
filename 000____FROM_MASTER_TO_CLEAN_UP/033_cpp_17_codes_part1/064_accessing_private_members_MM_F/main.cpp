#include <iostream>
#include <memory>

const uint32_t DIM_MAX = 100;

class A {

public:

    A() {
        for (int32_t i = 1; i <= DIM_MAX; i++) {
            x1[i] = i;
        }
    }

    virtual ~A() = default;

private:

    int32_t x1[DIM_MAX];
};

int main() {

    A a;
    auto *pi = reinterpret_cast<int32_t *>(&a);

    for (int32_t i = 1; i <= DIM_MAX; i++) {
        std::cout << " line --> " << i << " --> " << *(pi++) << std::endl;
    }

    return 0;
}