#include <iostream>
#include <thread>
#include <functional>
#include <mutex>
#include <condition_variable>

class Application {

    std::mutex m_mutex;
    std::condition_variable m_cond_var;
    bool m_b_data_loaded;

public:

    Application() {
        m_b_data_loaded = false;
    }

    void load_data() {

        // Make This Thread sleep for 1 Second

        std::cout << " --> Loading data from XML, please wait..." << std::endl;

        // Lock The Data structure

        std::lock_guard<std::mutex> guard(m_mutex);

        std::this_thread::sleep_for(std::chrono::seconds(5));

        // Set the flag to true, means data is loaded

        m_b_data_loaded = true;

        // Notify the condition variable

        m_cond_var.notify_one();
    }

    bool is_data_loaded() {
        return m_b_data_loaded;
    }

    void main_task() {

        std::cout << " --> Do some handshaking" << std::endl;

        // Acquire the lock

        std::unique_lock<std::mutex> mlock(m_mutex);

        // Start waiting for the Condition Variable to get signaled
        // Wait() will internally release the lock and make the thread to block.
        // As soon as condition variable get signaled, resume the thread and
        // again acquire the lock. Then check if condition is met or not
        // If condition is met then continue else again go in wait.

        m_cond_var.wait(mlock, std::bind(&Application::is_data_loaded, this));

        std::cout << " --> Do processing on loaded data" << std::endl;
    }
};

int main() {

    Application app;

    std::thread thread_1(&Application::main_task, &app);
    std::thread thread_2(&Application::load_data, &app);

    thread_2.join();
    thread_1.join();

    return 0;
}