#include <vector>
#include <string>
#include <iostream>

struct President {

    std::string name;
    std::string country;
    int year;

    President(std::string p_name, std::string p_country, int p_year)
            : name(std::move(p_name)),
              country(std::move(p_country)),
              year(p_year) {
        std::cout << "I am being constructed..." << std::endl;
    }

    President(President &&other) noexcept(true)
            : name(std::move(other.name)),
              country(std::move(other.country)),
              year(other.year) {
        std::cout << "I am being moved..." << std::endl;
    }

    President &operator=(const President &other) = default;
};

int main() {

    {
        std::vector<President> elections;
        std::cout << "emplace_back:" << std::endl;
        elections.emplace_back("Nelson Mandela", "South Africa", 1994);

        for (President const &president: elections) {
            std::cout << president.name
                      << " was elected president of "
                      << president.country
                      << " in "
                      << president.year << std::endl;
        }
    }

    {
        std::vector<President> reElections;
        std::cout << "push_back: " << std::endl;

        reElections.push_back(President("Franklin Delano Roosevelt", "the USA", 1936));

        std::cout << "Contents:" << std::endl;

        for (President const &president: reElections) {
            std::cout << president.name
                      << " was re-elected president of "
                      << president.country
                      << " in "
                      << president.year << std::endl;
        }
    }
}