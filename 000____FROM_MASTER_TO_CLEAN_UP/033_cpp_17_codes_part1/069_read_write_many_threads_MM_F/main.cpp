#include <iostream>
#include <cmath>
#include <chrono>
#include <iomanip>
#include <vector>
#include <memory>
#include <thread>
#include <mutex>
#include <array>
#include <numeric>

const auto DIM_DATA = 10 * static_cast<uint64_t>(std::pow(10.0, 7.0));
const auto DO_MAX = static_cast<uint64_t>(std::pow(10.0, 8.0));
const uint64_t NUM_WT = 2;
const uint64_t NUM_RT = 1;
const double ERROR_DIFF = 2.0;
const uint32_t PRECISION_LOC = 8;
const uint32_t SET_W1 = 20;
const uint32_t SET_W2 = 20;

std::mutex mtx;
std::vector<double> data_vec;

void write_data(double &time_write) {
    std::unique_lock<std::mutex> lck(mtx, std::defer_lock);
    lck.lock();

    const auto t1 = std::chrono::steady_clock::now();

    std::cout << "-->> writing data..." << std::endl;

    for (uint64_t i = 0; i < DIM_DATA; i++) {
        data_vec.push_back(1.0 / DIM_DATA);
    }

    const auto t2 = std::chrono::steady_clock::now();
    time_write = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count();

    lck.unlock();
}

void delete_data() {
    std::cout << "-->> deleting data..." << std::endl;
    data_vec.clear();
    data_vec.shrink_to_fit();
}

void read_delete_data(double &sum_total, double &time_read_del) {
    std::unique_lock<std::mutex> lck(mtx, std::defer_lock);
    lck.lock();

    const auto t1 = std::chrono::steady_clock::now();

    std::cout << "-->> reading data..." << std::endl;

    for (const auto &el : data_vec) {
        sum_total += el;
    }

    delete_data();

    const auto t2 = std::chrono::steady_clock::now();
    time_read_del = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count();

    lck.unlock();
}

int main() {

    std::cout << std::fixed;
    std::cout << std::setprecision(PRECISION_LOC);

    double sum_global = 0;

    const auto time_start = std::chrono::steady_clock::now();

    for (uint64_t i = 1; i <= DO_MAX; i++) {

        const auto time_now = std::chrono::steady_clock::now();
        const auto tot_t = std::chrono::duration_cast<std::chrono::duration<double>>(time_now - time_start).count();

        std::cout << "-------------------------------->> run = "
                  << std::setw(SET_W2) << std::right << i << std::endl;
        if (i > 1) {
            std::cout << "-------------------------------->> t/r = "
                      << std::setw(SET_W2) << std::right << tot_t / (i - 1) << std::endl;
        }

        double sum_loc = 0;
        double time_read_del = 0;
        double time_write = 0;

        std::array<std::thread, NUM_WT> th_w{};
        std::array<double, NUM_WT> time_wv{};
        std::array<std::thread, NUM_RT> th_r{};
        std::array<double, NUM_RT> time_rv{};
        std::array<double, NUM_RT> sum_v{};

        for (uint64_t kw = 0; kw < NUM_WT; kw++) {
            th_w[kw] = std::thread(write_data, std::ref(time_wv[kw]));
        }

        for (uint64_t kr = 0; kr < NUM_RT; kr++) {
            th_r[kr] = std::thread(read_delete_data, std::ref(sum_v[kr]), std::ref(time_rv[kr]));
        }

        for (auto &el : th_w) {
            el.join();
        }

        for (auto &el : th_r) {
            el.join();
        }

        time_write = std::accumulate(time_wv.begin(), time_wv.end(), 0.0);
        sum_loc = std::accumulate(sum_v.begin(), sum_v.end(), 0.0);
        time_read_del = std::accumulate(time_rv.begin(), time_rv.end(), 0.0);

        std::cout << "-->> s_lo = " << std::setw(SET_W1) << std::right << sum_loc << std::endl;
        sum_global += sum_loc;
        std::cout << "-->> s_gl = " << std::setw(SET_W1) << std::right << sum_global << std::endl;
        std::cout << "-->> t_wd = " << std::setw(SET_W1) << std::right << time_write << std::endl;
        std::cout << "-->> t_rd = " << std::setw(SET_W1) << std::right << time_read_del << std::endl;

        if (sum_global - static_cast<double>(NUM_WT * i) > ERROR_DIFF) {
            std::cout << "ERROR!" << std::endl;
            exit(-1);
        }
    }

    int sentinel = 0;
    std::cout << "Enter integer to exit local scope: ";
    std::cin >> sentinel;
    std::cout << "pgg --> after the exit" << std::endl;
}
