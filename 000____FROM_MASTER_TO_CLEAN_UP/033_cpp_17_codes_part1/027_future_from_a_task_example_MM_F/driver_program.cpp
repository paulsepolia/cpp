#include <iostream>
#include <future>
#include <thread>

uint64_t fib(uint64_t n) {
    if (n < 3) {
        return 1;
    } else {
        return fib(n - 1) + fib(n - 2);
    }
}

int main() {

    // future from a packaged_task
    std::packaged_task<uint64_t()> task([] {

        const auto res = fib(50);
        return static_cast<uint64_t>(res);
    }); // wrap the function

    std::future<uint64_t> f1 = task.get_future();  // get a future

    std::thread t(std::move(task)); // launch on a thread

    std::cout << "Waiting..." << std::flush;
    f1.wait();
    std::cout << "Results are: " << f1.get() << std::endl;

    t.join();
}