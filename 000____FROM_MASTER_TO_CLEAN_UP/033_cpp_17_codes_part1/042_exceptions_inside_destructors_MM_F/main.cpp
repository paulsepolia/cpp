#include <iostream>

class Bad1 {
public:

    ~Bad1() noexcept(false) {
        throw 1;
    }
};

class Bad2 {
public:

    ~Bad2() noexcept(true) {
        throw 2;
    }
};

class Bad3 {
public:

    ~Bad3() {
        throw 3;
    }
};


int main() {

    try {
        std::cout << " pgg --> 1" << std::endl;
        Bad1 bad1;
        std::cout << " pgg --> 2" << std::endl;
    }
    catch (...) {
        std::cout << " pgg --> 3" << std::endl;
    }

    try {
        std::cout << " pgg --> 4" << std::endl;
        Bad2 bad2;
        std::cout << " pgg --> 5" << std::endl;
    }
    catch (...) {
        std::cout << " pgg --> 6" << std::endl;
    }

    try {
        std::cout << " pgg --> 7" << std::endl;
        Bad3 bad3;
        std::cout << " pgg --> 8" << std::endl;
    }
    catch (...) {
        std::cout << " pgg --> 9" << std::endl;
    }

    return 0;
}