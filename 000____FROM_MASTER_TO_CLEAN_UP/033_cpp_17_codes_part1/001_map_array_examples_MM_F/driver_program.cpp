#include <iostream>
#include <map>

int main() {

    {
        double my_array[3] = {1.0, 2.0, 3.0};
        auto[a, b, c] = my_array;

        std::cout << "my_array[0] --> " << my_array[0] << std::endl;
        std::cout << "my_array[1] --> " << my_array[1] << std::endl;
        std::cout << "my_array[2] --> " << my_array[2] << std::endl;
        std::cout << "a = " << a << std::endl;
        std::cout << "b = " << b << std::endl;
        std::cout << "c = " << c << std::endl;
    }

    {
        std::pair<uint32_t, uint32_t> my_pair(10, 11);
        auto[a, b] = my_pair;
        std::cout << "a = " << a << std::endl;
        std::cout << "b = " << b << std::endl;
    }


    {
        std::map<uint32_t, uint32_t> my_map;

        for(uint32_t i = 0; i != 3; i++) {
            my_map.insert(std::pair<uint32_t, uint32_t>(i, i+1));
        }

        for (const auto &[k, v] : my_map) {
            std::cout << "key = " << k << std::endl;
            std::cout << "value = " << v << std::endl;
        }
    }
}

