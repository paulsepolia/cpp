#include <thread>
#include <vector>
#include <algorithm>
#include <functional>

void do_work(int id) {}

void f() {

	const int I_MAX = 2000000;
	std::vector<std::thread> threads;

	for (int i = 0; i < I_MAX; ++i) {
		threads.push_back(std::thread(do_work, i));
	}

	std::for_each(threads.begin(), threads.end(), std::mem_fn(&std::thread::join));
}

int main() {

	f();
}
