#include <iostream>
#include <future>
#include <thread>
#include <vector>

uint64_t fib(uint64_t n) {
    if (n < 3) {
        return 1;
    } else {
        return fib(n - 1) + fib(n - 2);
    }
}

int main() {

    std::cout << "--------------------------------------------->> 1" << std::endl;

    {
        // future from an async()

        std::future<uint64_t> fut = std::async(std::launch::async, [] {

            std::cout << "--> step 1 --> sleeping for few milliseconds" << std::endl << std::flush;
            std::this_thread::sleep_for(std::chrono::milliseconds(1000));
            std::cout << "--> step 2 --> sleeping for few milliseconds" << std::endl << std::flush;
            std::this_thread::sleep_for(std::chrono::milliseconds(1000));
            std::cout << "--> step 3 --> returning" << std::endl << std::flush;

            return static_cast<uint64_t>(8);
        });

        std::cout << "Waiting for the results to become available..." << std::endl << std::flush;
        fut.wait();
        std::cout << "The results are available and their are: " << fut.get() << std::endl << std::flush;
    }

    std::cout << "--------------------------------------------->> 2" << std::endl;

    {
        // future from an async()

        std::future<std::vector<uint64_t>> fut = std::async(std::launch::async, [] {

            std::vector<uint64_t> res_vec{};
            const uint64_t res1 = fib(10);
            res_vec.emplace_back(res1);
            std::cout << "--> res1 --> " << res1 << std::endl << std::flush;

            const uint64_t res2 = fib(20);
            std::cout << "--> res2 --> " << res2 << std::endl << std::flush;
            res_vec.emplace_back(res2);

            const uint64_t res3 = fib(48);
            std::cout << "--> res3 --> " << res3 << std::endl << std::flush;
            res_vec.emplace_back(res3);

            return res_vec;

        });

        std::cout << "Waiting for the results to become available..." << std::endl << std::flush;
        fut.wait();
        std::cout << "The results are available and their are: " << std::endl;
        for (auto &el: fut.get()) {
            std::cout << "el = " << el << std::endl;
        }
    }
}
