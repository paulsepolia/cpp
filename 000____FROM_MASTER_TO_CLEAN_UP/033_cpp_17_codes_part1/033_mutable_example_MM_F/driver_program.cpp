#include <iostream>

class A {
public:
    A() : x1(0), x2(0) {
        x1 = 1;
        x2 = 2;
    };

    void f1() const {
        std::cout << " --> x1 = " << x1 << std::endl;
        std::cout << " --> x2 = " << x2 << std::endl;
    }

    void f2() const {

        x2 = 3;
        std::cout << " --> x1 = " << x1 << std::endl;
        std::cout << " --> x2 = " << x2 << std::endl;
    }

private:
    int x1;
    mutable int x2;
};


int main() {

    A a;

    a.f1();
    a.f2();

    return 0;
}