#include <iostream>
#include <csignal>
#include <thread>

void signal_handler(int32_t signum) {

    std::cout << "pgg --> Interrupt signal (" << signum << ") received." << std::endl;
    std::cout << "pgg --> next step is to sleep for few seconds..." << std::endl;

    while(true) {
        static uint64_t i = 0;
        std::cout << " ... sleeping again --> " << i++ << std::endl;
        std::this_thread::sleep_for(std::chrono::seconds(2));
    }

    exit(signum);
}

int main() {

    // register all available signals

    signal(SIGHUP, signal_handler);     // #1 Hangup. Usually means that the controlling terminal has been disconnected.
    signal(SIGINT, signal_handler);     // #2 Interrupt. The user can generate this signal by pressing Ctrl+C or Delete.
    signal(SIGQUIT, signal_handler);    // #3 Quits the process and produces a core dump.
    signal(SIGILL, signal_handler);     // #4 Illegal instruction.
    signal(SIGTRAP, signal_handler);    // #5 Trace or breakpoint trap.
    signal(SIGABRT, signal_handler);    // #6 Abort.

    signal(SIGFPE, signal_handler);     // #8 Arithmetic exception. Informs a process of a floating-point error.
    signal(SIGKILL, signal_handler);    // #9 Killed. Forces the process to terminate. This is a sure kill
    signal(SIGBUS, signal_handler);     // #10 Bus error.
    signal(SIGSEGV, signal_handler);    // #11 Segmentation fault.
    signal(SIGSYS, signal_handler);     // #12 Bad system call.
    signal(SIGPIPE, signal_handler);    // #13 Broken pipe.
    signal(SIGALRM, signal_handler);    // #14 Alarm clock.
    signal(SIGTERM, signal_handler);    // #15 Terminated. A gentle kill that gives processes a chance to clean up.
    signal(SIGUSR1, signal_handler);    // #16 User signal 1.
    signal(SIGUSR2, signal_handler);    // #17 User signal 2.
    signal(SIGCHLD, signal_handler);    // #18 Child status changed.
    signal(SIGPWR, signal_handler);     // #19 Power fail or restart.
    signal(SIGWINCH, signal_handler);   // #20 Window size change.
    signal(SIGURG, signal_handler);     // #21 Urgent socket condition.
    signal(SIGPOLL, signal_handler);    // #22 Poll-able event.
    signal(SIGSTOP, signal_handler);    // #23 Stopped (signal). Pauses a process.
    signal(SIGTSTP, signal_handler);    // #24 Stopped (user).
    signal(SIGCONT, signal_handler);    // #25 Continued.
    signal(SIGTTIN, signal_handler);    // #26 Stopped (tty input).
    signal(SIGTTOU, signal_handler);    // #27 Stopped (tty output).
    signal(SIGVTALRM, signal_handler);  // #28 Virtual timer expired.
    signal(SIGPROF, signal_handler);    // #29 Profiling timer expired.
    signal(SIGXCPU, signal_handler);    // #30 CPU time limit exceeded.
    signal(SIGXFSZ, signal_handler);    // #31 File size limit exceeded.

    while (true) {
        static uint64_t i = 0;
        std::cout << " ... sleeping --> " << i++ << std::endl;
        std::this_thread::sleep_for(std::chrono::seconds(2));
    }
}
