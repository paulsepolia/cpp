// lock_guard example
#include <iostream>
#include <thread>
#include <mutex>
#include <stdexcept>
#include <vector>
#include <cmath>

std::mutex mtx;

void print_even(int32_t x) {

    if (x % 2 == 0) {
        std::cout << x << " is even" << std::endl;
    } else {
        throw (std::logic_error("not even"));
    }
}

void print_thread_id(int32_t id) {

    try {
        // using a local lock_guard to lock mtx
        // guarantees unlocking on destruction / exception:
        std::lock_guard<std::mutex> lck(mtx);
        print_even(id);
    }
    catch (const std::logic_error &e) {
        std::cout << "[exception caught]" << std::endl;
        std::cout << " e.what() = " << e.what() << std::endl;
    }
}

int main() {

    const auto NUM_THREADS = static_cast<u_int64_t>((std::pow(10.0, 4.0)));

    std::vector<std::thread> threads;
    threads.resize(NUM_THREADS);

    // spawn the threads
    for (uint64_t i = 0; i < NUM_THREADS; ++i) {
        threads[i] = std::thread(print_thread_id, i + 1);
    }

    // join the threads
    for (auto &th : threads) th.join();

    return 0;
}