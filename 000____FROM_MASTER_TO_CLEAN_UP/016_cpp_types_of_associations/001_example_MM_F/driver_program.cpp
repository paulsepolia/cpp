#include <iostream>
#include <memory>

class A {

public:

    void set_value(const uint64_t &val) {

        _value = val;
    }

    uint64_t get_value() const {

        return _value;
    }

private:

    uint64_t _value;
};

class B {

public:

    B() : _ptrA(std::make_shared<A>()) {}

    std::shared_ptr<A> get_pointer_to_A() const {

        return _ptrA;
    }

    void set_value(const uint64_t &val) {

        return _ptrA->set_value(val);
    }

    uint64_t get_value() const {

        return _ptrA->get_value();
    }

private:

    std::shared_ptr<A> _ptrA;
};


int main() {

    A objA;
    B objB;

    const uint64_t val(100);

    objB.set_value(val);

    objA = *objB.get_pointer_to_A().get();

    std::cout << " --> A --> " << objA.get_value() << std::endl;
    std::cout << " --> B --> " << objB.get_value() << std::endl;

    return 0;
}
