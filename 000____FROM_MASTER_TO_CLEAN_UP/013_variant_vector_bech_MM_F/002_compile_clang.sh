#!/bin/bash

  clang++   -O3            \
            -Wall          \
            -std=c++2a     \
            -stdlib=libc++ \
            -fopenmp       \
            -pthread       \
            -pedantic      \
            main.cpp       \
            -o x_clang -lc++fs

