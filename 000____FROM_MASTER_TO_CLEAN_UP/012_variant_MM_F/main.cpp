#include <memory>
#include <iostream>
#include <malloc.h>
#include <variant>
#include <cmath>

void *operator new(std::size_t count) {
    std::cout << "allocating " << count << " bytes" << std::endl;
    return malloc(count);
}

struct SampleVisitor {

    void operator()(int i) const { std::cout << "int: " << i << '\n'; }

    void operator()(float f) const { std::cout << "float: " << f << '\n'; }

    void operator()(const std::string &s) const { std::cout << "string: " << s << '\n'; }
};

int main() {

    std::variant<int, float, std::string> intFloatString;
    static_assert(std::variant_size_v<decltype(intFloatString)> == 3);

    // default initialized to the first alternative, should be 0
    std::visit(SampleVisitor{}, intFloatString);

    // index will show the currently used 'type'
    std::cout << "index = " << intFloatString.index() << std::endl;
    intFloatString = 100.0f;
    std::cout << "index = " << intFloatString.index() << std::endl;
    intFloatString = "hello super world";
    std::cout << "index = " << intFloatString.index() << std::endl;

    // try with get_if:
    if (const auto intPtr(std::get_if<int>(&intFloatString)); intPtr) {
        std::cout << "int: " << *intPtr << std::endl;
    }
    else if (const auto floatPtr(std::get_if<float>(&intFloatString)); floatPtr) {
        std::cout << "float: " << *floatPtr << std::endl;
    }

    if (std::holds_alternative<int>(intFloatString)) {
        std::cout << "the variant holds an int!" << std::endl;
    }
    else if (std::holds_alternative<float>(intFloatString)) {
        std::cout << "the variant holds a float" << std::endl;
    }
    else if (std::holds_alternative<std::string>(intFloatString)) {
        std::cout << "the variant holds a string" << std::endl;
    }

    // try/catch and bad_variant_access
    try {
        auto f = std::get<float>(intFloatString);
        std::cout << "float! " << f << std::endl;
    }
    catch (std::bad_variant_access &) {
        std::cout << "our variant doesn't hold float at this moment..." << std::endl;
    }

    // visit:
    std::visit(SampleVisitor{}, intFloatString);
    intFloatString = 10;
    std::visit(SampleVisitor{}, intFloatString);
    intFloatString = 10.0f;
    std::visit(SampleVisitor{}, intFloatString);
}

//• Line 18, 22: If you don’t initialise a variant with a value, then the variant is initialised with the
//        first type. In that case, the first alternative type must have a default constructor. Line 22 will
//        print the value 0.
//• Line: 25, 27, 29, 31, 33, 35: You can check what the currently used type is via index() or
//holds_alternative.
//• Line 39, 41, 47: You can access the value by using get_if or get (might throw bad_variant_-
//access exception).
//• Type Safety - the variant doesn’t allow you to get a value of the type that’s not active.
//• No extra heap allocation occurs.
//• Line 9, 22: You can use a visitor to invoke an action on a currently active type. The example
//uses PrintVisitor to print the currently active value. It’s a simple structure with overloads
//for operator(). The visitor is then passed to std::visit which performs the visitation.
//• The variant class calls destructors and constructors of non-trivial types, so in the example, the
//        string object is cleaned up before we switch to new variants.