#include <iostream>
#include <vector>
#include <chrono>
#include <set>
#include <unordered_set>
#include <algorithm>
#include <thread>

class benchmark_timer {
public:

    explicit benchmark_timer(const double &res) :
            _res(res),
            t1{std::chrono::steady_clock::now()},
            t2{std::chrono::steady_clock::now()},
            t_prev{std::chrono::steady_clock::now()} {};

    auto print_time() const -> void {
        const auto t_loc{std::chrono::steady_clock::now()};
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t_loc - t_prev).count()};
        t_prev = t_loc;
        std::cout << " --> time used since last reset (secs) = " << total_time << std::endl;
    }

    auto reset_timer() -> void {
        const auto t_loc{std::chrono::steady_clock::now()};
        t_prev = t_loc;
        t1 = t_loc;
        t2 = t_loc;
    }

    ~benchmark_timer() {
        t2 = std::chrono::steady_clock::now();
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count()};
        std::cout << " --> res = " << _res << std::endl;
        std::cout << " --> total time used since last reset (secs) = " << total_time << std::endl;
    }

    auto set_res(const double &res) -> void {
        _res = res;
    }

private:

    double _res{};
    decltype(std::chrono::steady_clock::now()) t1{};
    decltype(std::chrono::steady_clock::now()) t2{};
    mutable decltype(std::chrono::steady_clock::now()) t_prev{};
};

const auto DIM{(size_t) 120'000'000};
const auto REC{(size_t) 8};

int main() {

    {
        std::cout << "------------------------------>> 1 -->> unordered_set" << std::endl;

        auto ot{benchmark_timer(0.0)};
        auto cont1{std::unordered_set<void *>()};
        auto cont2{std::unordered_set<void *>()};

        auto la1 = [&]() {
            for (size_t i = 0; i < DIM; i++) {
                void *ptr{malloc(REC)};
                cont1.emplace(ptr);
            }
        };

        auto la2 = [&]() {
            for (size_t i = 0; i < DIM; i++) {
                void *ptr{malloc(REC)};
                cont2.emplace(ptr);
            }
        };

        auto th1{std::thread(la1)};
        auto th2{std::thread(la2)};

        th1.join();
        th2.join();

        void *ptr1{malloc(REC)};
        const auto it1{cont1.find(ptr1)};

        void *ptr2{malloc(REC)};
        const auto it2{cont1.find(ptr2)};

        std::cout << " --> out1 --> " << &it1 << std::endl;
        std::cout << " --> out2 --> " << &it2 << std::endl;

        ot.print_time();
        ot.reset_timer();
    }
}