//=================//
// Object function //
//=================//

#include <iostream>
#include <vector>
#include <deque>
#include <list>
#include <thread>
#include <algorithm>

using std::cout;
using std::cin;
using std::endl;
using std::vector;
using std::deque;
using std::list;
using std::thread;

//  an object function

class LessThan {
public:
    int operator() (int x, int y)
    {
        return x < y;
    }

    int operator() (int x, int y, int z)
    {
        return (x + y + z);
    }

    void operator() ()
    {
        const long int I_MAX = static_cast<long int>(pow(10.0, 7.2));
        long int i;

        vector<double>* vecA = new vector<double> [1];
        deque<double>* deqA  = new deque<double>  [1];
        list<double>* listA  = new list<double>   [1];

        // build vector

        cout << "--> building the vector..." << endl;
        for (i = 0; i < I_MAX; i++) {
            (*vecA).push_back(cos(static_cast<double>(i)));
        }

        // build deque

        cout << "--> building the deque..." << endl;

        (*deqA).assign((*vecA).begin(), (*vecA).end());

        // build list

        cout << "--> building the list..." << endl;

        (*listA).assign((*vecA).begin(), (*vecA).end());

        // sorting the vector

        cout << "--> sorting the vector..." << endl;

        sort((*vecA).begin(), (*vecA).end());

        // sorting the deque

        cout << "--> sorting the deque..." << endl;

        sort((*deqA).begin(), (*deqA).end());

        // clear the vector

        cout << "--> clearing the vector..." << endl;
        (*vecA).clear();

        // clear the deque

        cout << "--> clearing the deque..." << endl;
        (*deqA).clear();

        // clear the list

        cout << "--> clearing the list..." << endl;
        (*listA).clear();

        // delete the vector

        cout << "--> deleting the vector..." << endl;
        delete [] vecA;

        // delete the deque

        cout << "--> deleting the deque..." << endl;
        delete [] deqA;

        // delete the list

        cout << "--> deleting the list..." << endl;
        delete [] listA;
    }
};

// a fun to call the heavey duty

void FUN()
{
    LessThan T;
    T();
}

// the main

int main()
{
    LessThan b;

    cout << "--> b(2,3)          = " << b(2,3) << endl;
    cout << "--> LessThan()(2,3) = " << LessThan()(2,3) << endl;

    cout << "--> b(1,2,3)          = " << b(1,2,3) << endl;
    cout << "--> LessThan()(1,2,3) = " << LessThan()(1,2,3) << endl;

    const int K_MAX = 1000;
    int k;

    for (k = 0; k < K_MAX; k++) {
        cout << "--------------------------------------------------------->> " << k << endl;
        cout << "--> calling object function via FUN and thread th1" << endl;
        thread th1(FUN);
        cout << "--> calling object function via FUN and thread th3" << endl;
        thread th2(FUN);
        cout << "--> calling object function via FUN and thread th3" << endl;
        thread th3(FUN);
        cout << "--> calling object function via FUN and thread th4" << endl;
        thread th4(FUN);

        cout << "--> Joining the threads... please wait!" << endl;

        th1.join();
        th2.join();
        th3.join();
        th4.join();

        cout << "--> done!" << endl;
    }

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
