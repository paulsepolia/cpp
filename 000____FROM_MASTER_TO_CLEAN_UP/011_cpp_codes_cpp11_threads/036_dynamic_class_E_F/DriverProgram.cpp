//=======================================//
// test drive program for PFArrayD class //
//=======================================//

#include <iostream>
#include <cmath>
#include <thread>
#include <vector>
#include "External.h"

using std::cin;
using std::cout;
using std::endl;
using std::thread;
using std::vector;

int main()
{
    const int I_MAX = 100000;
    const long DIMEN_MAX = static_cast<long>(pow(10.0, 7.5));
    const int NUM_THREADS = 8;

    vector<thread> vecThreads;

    for (int i = 0 ; i < I_MAX; i++) {
        cout << "------------------------------------------->> " << i << endl;

        // create threads

        for (int j = 0; j < NUM_THREADS; j++) {
            vecThreads.push_back(thread(&benchFun<long, double>, DIMEN_MAX));
        }

        // join threads

        for (auto & t : vecThreads) {
            t.join();
        }

        // clear vector

        vecThreads.clear();
    }

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
