
//=============//
// permutation //
//=============//

#include <iostream>
#include <vector>
#include <thread>
#include <time.h>
#include "functions.h"


using std::cin;
using std::endl;
using std::vector;
using std::thread;

// the main function

int main()
{
    const int NUM_ELEMS = 15;
    const int NUM_THREADS = 2;
    vector<thread> thVec;
    time_t t1;
    time_t t2;
    double tall;

    t1 = clock();

    // create threads

    for (int i = 0; i < NUM_THREADS; i++) {
        thVec.push_back(thread(benchFunHeap, NUM_ELEMS));
    }

    // join threads

    for (int i = 0; i < NUM_THREADS; i++) {
        thVec[i].join();
    }

    // clear vector

    thVec.clear();

    // end timer

    t2 = clock();

    // report time

    tall = (t2-t1)/CLOCKS_PER_SEC;

    cout << "-------------------->> " << tall << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
