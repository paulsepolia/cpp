//===================//
// std::lock example //
//===================//

#include <iostream>       // std::cout
#include <thread>         // std::thread
#include <mutex>          // std::mutex, std::lock
#include <cmath>

// gobal definition

std::mutex foo, bar;

// task_a

void task_a()
{
    //foo.lock();        // does not work always, produces dead locks
    //bar.lock();        // does not work always, produces dead locks
    std::lock(foo, bar); // always work, does not produce dead locks
    std::cout << " --> task a" << std::endl;
    foo.unlock();
    bar.unlock();
}

// task_b

void task_b()
{
    //bar.lock();        // does not work always, produces dead locks
    //foo.lock();        // does not work always, produces dead locks
    std::lock(bar, foo); // always work, does not produce dead locks
    std::cout << " --> task b" << std::endl;
    bar.unlock();
    foo.unlock();
}

// the main function

int main()
{
    int i;
    const long int I_MAX = static_cast<long int>(pow(10.0, 7.0));

    for (i = 0; i < I_MAX; ++i) {
        std::cout << "-------------------------------->> " << i << std::endl;

        std::thread th1(task_a);
        std::thread th2(task_b);
        std::thread th3(task_a);
        std::thread th4(task_b);

        th1.join();
        th2.join();
        th3.join();
        th4.join();
    }

    int sentinel;
    std::cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
