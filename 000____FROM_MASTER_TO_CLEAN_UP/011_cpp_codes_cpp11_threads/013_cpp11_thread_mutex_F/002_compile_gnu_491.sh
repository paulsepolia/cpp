#!/bin/bash

  # 1. compile

  g++-4.9.1 -O3                \
            -Wall              \
	    -pthread           \
            -fopenmp           \
            -std=gnu++14       \
	    -static            \
            driver_program.cpp \
            -o x_gnu_491
