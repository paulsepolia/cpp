//===============//
// mutex example //
//===============//

#include <iostream>       // std::cout
#include <thread>         // std::thread
#include <mutex>          // std::mutex

std::mutex mtx;           // mutex for critical section

// a function

void print_block(int n, char c)
{
    // critical section (exclusive access to std::cout signaled by locking mtx):
    mtx.lock();
    for (int i = 0; i < n; ++i) {
        std::cout << c;
    }
    std::cout << std::endl;;
    mtx.unlock();
}

//the main function

int main()
{
    int k;
    const int K_MAX = 100000L;

    for (k = 0; k < K_MAX; k++) {
        std::cout << std::endl;
        std::cout << "----------------------------------------------------->> " << k << std::endl;
        std::cout << std::endl;

        std::thread th1(print_block, 50, '1');
        std::thread th2(print_block, 50, '2');
        std::thread th3(print_block, 50, '3');
        std::thread th4(print_block, 50, '4');
        std::thread th5(print_block, 50, '5');
        std::thread th6(print_block, 50, '6');
        std::thread th7(print_block, 50, '7');
        std::thread th8(print_block, 50, '8');

        th1.join();
        th2.join();
        th3.join();
        th4.join();
        th5.join();
        th6.join();
        th7.join();
        th8.join();
    }

    int sentinel;
    std::cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
