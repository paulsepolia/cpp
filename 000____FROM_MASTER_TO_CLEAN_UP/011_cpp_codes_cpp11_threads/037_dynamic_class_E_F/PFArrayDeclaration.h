//===========//
// PFArray.h //
//===========//

// objects of this class are partially filled arrays of doubles

#ifndef PFARRAYD_H
#define PFARRAYD_H

namespace pgg {
template <typename T, typename P>
class PFArrayD {
public:

    PFArrayD<T,P>(); // default constructor
    explicit PFArrayD<T,P>(T); // a non-default constructor
    PFArrayD<T,P>(const PFArrayD<T,P>&); // copy constructor

    void addElement(P);
    bool full() const;
    T getCapacity() const;
    T getNumberUsed() const;
    void emptyArray()
    {
        used = 0;
    };

    P& operator [] (T); // overload [] operator
    PFArrayD<T,P>& operator = (const PFArrayD<T,P>&); // overload assignment

    ~PFArrayD<T,P>(); // destructor

private:

    P *a;
    T capacity;
    T used;
};
} // pgg

#endif // PFARRAYD_H

//======//
// FINI //
//======//
