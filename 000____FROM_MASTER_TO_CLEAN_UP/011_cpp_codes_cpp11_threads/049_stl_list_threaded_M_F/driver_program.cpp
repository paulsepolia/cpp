
//===========//
// Sort list //
//===========//

#include <iostream>
#include <list>
#include <cmath>
#include <functional>
#include <vector>
#include <thread>

using std::list;
using std::endl;
using std::cin;
using std::cout;
using std::pow;
using std::greater;
using std::vector;
using std::thread;

// the benchmark function

void benchFun(const long int DIMEN_MAX)
{
    // local variables and parameters

    const long int K_MAX = static_cast<long int>(pow(10.0, 6.0));

    for (int k = 0; k < K_MAX; k++) {
        cout << "---------------------------------------->> " << k << endl;

        // declare the list pointers

        list<double> * listA = new list<double>;
        list<double> * listB = new list<double>;

        // build the lists

        cout << "  1 --> build the lists" << endl;

        for (long i = 0; i < DIMEN_MAX; i++) {
            listA->push_back(static_cast<double>(i));
            listA->push_back(static_cast<double>(DIMEN_MAX-i));
        }

        // sort the lists

        cout << "  2 --> sort the lists" << endl;

        listA->sort();
        listB->sort();

        // sort the lists

        cout << "  3 --> sort the lists again" << endl;

        listA->sort(greater<double>());
        listB->sort(greater<double>());

        // delete

        cout << "  4 --> delete the lists here" << endl;

        delete listA;
        delete listB;
    }
}

// the main function

int main()
{
    const long int DIMEN_MAX = static_cast<long int>(pow(10.0, 7.0));
    const int NUM_THREADS = 4;
    vector<thread> vecThreads;

    // span the threads

    for (int j = 0; j < NUM_THREADS; j++) {
        vecThreads.push_back(thread(benchFun, DIMEN_MAX));
    }

    // join the threads

    for (auto& t : vecThreads) {
        t.join();
    }

    // clear the threads vector

    vecThreads.clear();

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
