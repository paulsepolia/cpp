//==============================//
// C++11, spawn several threads //
//==============================//

#include <iostream>
#include <cmath>
#include <iomanip>
#include <thread>
#include <vector>

// a function

const long int I_MAX = 20000000;

void do_work(int id)
{
    std::cout << "------------->> " << id << std::endl;
};

// a function which forks and joins threads

void f()
{
    // local variables and parameters

    long int i;
    std::vector<std::thread> threadsVec;

    // create threads, each do work and joined

    for(i = 0; i < I_MAX; ++i) {
        threadsVec.push_back(std::thread(do_work, i));
        threadsVec[i].join();
    }

}

// the main function

int main()
{
    // infos

    std::cout << "-----------------------------------------" << std::endl;
    std::cout << "--> I create and join consequtive " << I_MAX << " C++11 threads" << std::endl;

    // call the function

    f();

    // the sentinel

    int sentinel;
    std::cin >> sentinel;

    // exit

    return 0;
}

//======//
// FINI //
//======//
