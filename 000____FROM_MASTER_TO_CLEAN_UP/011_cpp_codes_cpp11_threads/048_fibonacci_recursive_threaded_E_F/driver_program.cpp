
//==============================//
// fibonacci recursive threaded //
///=============================//

#include <iostream>
#include <thread>
#include <vector>
#include <cmath>
#include <ctime>
#include <iomanip>
#include "functions.h"

using std::endl;
using std::cin;
using std::cout;
using std::vector;
using std::thread;
using std::pow;
using std::time;
using std::setprecision;
using std::fixed;
using std::showpos;
using std::showpoint;

// type definition

typedef long long int intLL;

// the main function

int main()
{
    const intLL I_MAX = static_cast<intLL>(pow(10.0, 7.4));
    int NUM_THREADS;
    vector<thread> thVec;
    intLL firstFibNum;
    intLL secondFibNum;
    intLL nth;
    time_t t1;
    time_t t2;
    double tall;

    // set the output look

    cout << setprecision(5);
    cout << fixed;
    cout << showpos;
    cout << showpoint;

    // interface

    cout << "Enter the number of threads to use: ";
    cin >> NUM_THREADS;

    cout << "Enter the first Fibonacci number: ";
    cin >> firstFibNum;

    cout << "Enter the second Fibonacci number: ";
    cin >> secondFibNum;

    cout << "Enter the position of the desired Fibonacci number: ";
    cin >> nth;

    for (intLL i = 0; i < I_MAX; i++) {

        // counter

        cout << "-------------------------------------------------->> " << i << endl;
        cout << endl;

        // timer starts

        t1 = clock();

        // create threads

        for (int i = 0; i < NUM_THREADS; i++) {
            thVec.push_back(thread(benchFun, firstFibNum, secondFibNum, nth));
        }

        // join threads

        for (int i = 0; i < NUM_THREADS; i++) {
            thVec[i].join();
        }

        // clear vector

        thVec.clear();

        // timer ends

        t2 = clock();
        tall = (t2-t1)/static_cast<double>(CLOCKS_PER_SEC);

        // report

        cout << endl;
        cout << " ----------------------->> time used = " << tall << endl;
        cout << endl;
    }

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
