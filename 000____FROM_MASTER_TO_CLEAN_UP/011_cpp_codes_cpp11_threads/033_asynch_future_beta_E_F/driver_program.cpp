//==================//
// future and async //
//==================//

#include <iostream>
#include <future>
#include <thread>
#include <iomanip>

using std::endl;
using std::cout;
using std::cin;
using std::future;
using std::async;
using std::launch;
using std::setprecision;
using std::fixed;
using std::showpos;
using std::showpoint;

// function definition

double fib(int n)
{
    if (n < 3) {
        return 1.0;
    } else {
        return fib(n-1) + fib(n-2);
    }
}

// the main function

int main()
{
    // local variables and parameters

    int val = 50;
    const int I_MAX = 10000;
    const int NUM_THREADS = 10;

    // output style

    cout << setprecision(10);
    cout << fixed;
    cout << showpos;
    cout << showpoint;

    // main loop

    for (int i = 0; i < I_MAX; i++) {
        cout << "-------------------------------------------------->> " << i << endl;

        future<double> * f = new future<double> [NUM_THREADS];

        for (int i = 0; i < NUM_THREADS; i++) {
            f[i] = async(launch::async, [val] () {
                return fib(val);
            });
        }

        cout << " --> waiting ..." << endl;

        for (int i = 0; i < NUM_THREADS; i++) {
            f[i].wait();
        }

        for (int i = 0; i < NUM_THREADS; i++) {
            cout << " --> f[" << i << "] --> " << f[i].get() << endl;
        }
    }

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
