#!/bin/bash

  g++-9.2.0  -O3                \
             -Wall              \
             -std=gnu++17       \
	         -pthread           \
	         -fopenmp           \
	         -static            \
             driver_program.cpp \
             -o x_gnu
