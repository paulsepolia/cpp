#!/bin/bash

  clang++    -O3                \
             -Wall              \
             -std=c++1z         \
	         -pthread           \
	         -static            \
             driver_program.cpp \
             -o x_clang
