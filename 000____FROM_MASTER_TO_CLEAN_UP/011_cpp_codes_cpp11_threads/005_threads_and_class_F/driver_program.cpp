//===================//
// C++11 and threads //
//===================//

// includes

#include <iostream>
#include <iomanip>
#include <ctime>
#include <cmath>
#include <thread>

// a global constant

const long int J_MAX = static_cast<long int>(pow(10.0, 8.0));

// a function definition

void do_something(int i)
{
    std::cout << " --> " << i << std::endl;
}

// a class definition

struct func {
    long int j;
    int& i;

    func(int& i_):i(i_) {}

    void operator()()
    {
        for(j = 0; j < J_MAX; ++j) {
            do_something(++i);
        }
    }
};

// a function definition

void oops()
{
    int some_local_stateA = -100000;
    int some_local_stateB = 000000;
    int some_local_stateC = 100000;
    int some_local_stateD = 1000000;

    func my_funcA(some_local_stateA);
    func my_funcB(some_local_stateB);
    func my_funcC(some_local_stateC);
    func my_funcD(some_local_stateD);

    std::thread my_threadA(my_funcA);
    std::thread my_threadB(my_funcB);
    std::thread my_threadC(my_funcC);
    std::thread my_threadD(my_funcD);

    my_threadA.join();
    my_threadB.join();
    my_threadC.join();
    my_threadD.join();
}

// the main function

int main()
{
    std::cout << std::fixed;
    std::cout << std::showpoint;
    std::cout << std::setprecision(5);

    oops();

    int sentinel;
    std::cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
