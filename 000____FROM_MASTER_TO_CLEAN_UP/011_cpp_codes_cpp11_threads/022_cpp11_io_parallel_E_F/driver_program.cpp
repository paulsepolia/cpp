//============================//
// Read and Write binary file //
// using buffers              //
//============================//

#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <thread>
#include <ctime>
#include <iomanip>

// using

using std::string;
using std::ofstream;
using std::ifstream;
using std::cout;
using std::endl;
using std::ios;
using std::flush;
using std::thread;
using std::this_thread::get_id;
using std::fixed;
using std::setprecision;
using std::showpoint;

// functions prototypes

void writeFun(const string &, const long &, double *);
void readFun(const string &, const long &);

// the main function

int main()
{
    // 1. variables and parameters

    const long ARRAY_DIM = static_cast<long>(pow(10.0, 7.5));
    double* arrayLocal = new double [ARRAY_DIM];
    const long I_MAX = 1000000;
    long i;

    const string fileName1 = "my_file_1.bin";
    const string fileName2 = "my_file_2.bin";
    const string fileName3 = "my_file_3.bin";
    const string fileName4 = "my_file_4.bin";
    const string fileName5 = "my_file_5.bin";
    const string fileName6 = "my_file_6.bin";
    const string fileName7 = "my_file_7.bin";
    const string fileName8 = "my_file_8.bin";

    // 2. adjust the output style

    cout << fixed;
    cout << showpoint;
    cout << setprecision(5);

    // 3. build the array

    for (i = 0; i < ARRAY_DIM; i++) {
        arrayLocal[i] = cos(static_cast<double>(i));
    }

    // 4. main do-loop

    for (i = 0; i < I_MAX; i++) {
        cout << "------------------------------------------------------>> " << i << endl;

        thread th01(writeFun, fileName1, ARRAY_DIM, arrayLocal);
        thread th02(writeFun, fileName2, ARRAY_DIM, arrayLocal);
        thread th03(writeFun, fileName3, ARRAY_DIM, arrayLocal);
        thread th04(writeFun, fileName4, ARRAY_DIM, arrayLocal);
        thread th05(writeFun, fileName5, ARRAY_DIM, arrayLocal);
        thread th06(writeFun, fileName6, ARRAY_DIM, arrayLocal);
        thread th07(writeFun, fileName7, ARRAY_DIM, arrayLocal);
        thread th08(writeFun, fileName8, ARRAY_DIM, arrayLocal);

        th01.join();
        th02.join();
        th03.join();
        th04.join();
        th05.join();
        th06.join();
        th07.join();
        th08.join();

        cout << endl;

        thread th09(readFun, fileName1, ARRAY_DIM);
        thread th10(readFun, fileName2, ARRAY_DIM);
        thread th11(readFun, fileName3, ARRAY_DIM);
        thread th12(readFun, fileName4, ARRAY_DIM);
        thread th13(readFun, fileName5, ARRAY_DIM);
        thread th14(readFun, fileName6, ARRAY_DIM);
        thread th15(readFun, fileName7, ARRAY_DIM);
        thread th16(readFun, fileName8, ARRAY_DIM);

        th09.join();
        th10.join();
        th11.join();
        th12.join();
        th13.join();
        th14.join();
        th15.join();
        th16.join();
    }

    // exit

    return 0;
}

//==================//
// writing function //
//==================//

void writeFun(const string & fileName, const long & dim, double * arrayLoc)
{
    // local variables and parameters

    ofstream fileOut;
    clock_t t1;
    clock_t t2;

    // start time

    t1 = clock();

    // open the file

    fileOut.open(fileName.c_str(), ios::out | ios::binary | ios::trunc);

    // check if the file opened with success

    if (!fileOut.is_open()) {
        cout << "Error! The file stream is not opened. Abort." << endl;
    }

    // go to the beggining of the file

    fileOut.seekp(0);

    // write to the file

    fileOut.write(reinterpret_cast<char*>(&arrayLoc[0]), dim * sizeof(double));

    // flush the buffer to ensure the data has been written

    fileOut << flush;

    // close the file

    fileOut.close();

    // end time

    t2 = clock();

    // report

    cout << " --> writing done --> thread id = "
         << get_id()
         << " --> time used = "
         << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;
}

//==================//
// reading function //
//==================//

void readFun(const string & fileName, const long & dim)
{
    // local variables and parameters

    ifstream fileIn;
    clock_t t1;
    clock_t t2;

    double * arrayLoc = new double [dim];

    // start time

    t1 = clock();

    // open the file

    fileIn.open( fileName.c_str(), ios::in  | ios::binary);

    // rewind the file

    fileIn.seekg(0, fileIn.beg);

    // read the file

    fileIn.read(reinterpret_cast<char*>(&arrayLoc[0]), dim * sizeof(double));

    // close the file

    fileIn.close();

    // delete the container

    delete [] arrayLoc;

    // end time

    t2 = clock();

    // report

    cout << " --> reading done --> thread id = "
         << get_id()
         << " --> time used = "
         << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;
}

//======//
// FINI //
//======//
