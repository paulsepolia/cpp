//===============//
// C++11, Thread //
//===============//

#include <iostream>
#include <iomanip>
#include <ctime>
#include <cmath>
#include <thread>

// definition of "void do_something()" function

void do_something()
{
    double sumLoc = 0.0;
    double iLoc = 0.0;
    const double MY_BOUND_MAX = 20.0;
    clock_t t1;
    clock_t t2;
    double tall;

    t1 = clock();

    while(sumLoc <= MY_BOUND_MAX) {
        iLoc = iLoc + 1.0;
        sumLoc = sumLoc + 1.0/iLoc;
    }

    t2 = clock();
    tall = (t2-t1)/static_cast<double>(CLOCKS_PER_SEC);

    std::cout << " --> " << MY_BOUND_MAX << " --> reached in --> " << tall << std::endl;
    std::cout << " --> " << sumLoc << " <--> " << iLoc << std::endl;
}

// definition of "void do_something()_else" function

void do_something_else()
{
    double sumLoc = 0.0;
    double iLoc = 0.0;
    const double MY_BOUND_MAX = 21.0;
    clock_t t1;
    clock_t t2;
    double tall;

    t1 = clock();

    while(sumLoc <= MY_BOUND_MAX) {
        iLoc = iLoc + 1.0;
        sumLoc = sumLoc + 1.0/iLoc;
    }

    t2 = clock();
    tall = (t2-t1)/static_cast<double>(CLOCKS_PER_SEC);

    std::cout << " --> " << MY_BOUND_MAX << " --> else reached in --> " << tall << std::endl;
    std::cout << " --> " << sumLoc << " <--> " << iLoc << std::endl;
}

// a class with a function to be threaded

class background_task {
public:
    void operator()() const
    {
        do_something();
        do_something_else();
    }
};

// main program

int main()
{
    // 1. local variables and parameters

    const long int I_MAX = 10; //static_cast<long int>(pow(10.0, 5.0));
    background_task f;
    long int i;

    // 2. set the output format

    std::cout << std::fixed;
    std::cout << std::setprecision(5);
    std::cout << std::showpoint;

    // 3. main do-loop

    for (i = 0; i < I_MAX; i++) {
        std::cout << std::endl;
        std::cout << "------------------------------------------------>> " << i << std::endl;
        std::cout << std::endl;

        // 2. start the 4 threads

        std::thread my_thread1(f);
        std::thread my_thread2(f);

        // 3. join the launched threads

        my_thread1.detach();
        my_thread2.detach();

        // 4. do extra work here

        std::cout << " ---------------> After the threads have been detached (next loop starts)" << std::endl;
    }

    // 4. sentinel

    int sentinel;
    std::cout << "YOU CAN ENTER AN INTEGER TO EXIT ANYTIME" << std::endl;
    std::cout << "ALL THREADS ARE DETACHED BUT STILL RUNNING..." << std::endl;
    std::cout << "THE THREADED FUNCTION IS A BLACK BOX" << std::endl;
    std::cout << "Anyway, if you want to exit enter an integer here: " << std::endl;
    std::cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
