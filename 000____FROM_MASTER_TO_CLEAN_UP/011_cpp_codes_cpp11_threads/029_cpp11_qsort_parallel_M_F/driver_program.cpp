//===============================//
// qsort example                 //
// WARNING: QSORT IS THREAD SAFE //
//===============================//

#include <cstdlib>
#include <iostream>
#include <cmath>
#include <iomanip>
#include <thread>
#include <vector>
#include <omp.h>
#include <ctime>


using std::cin;
using std::cout;
using std::endl;
using std::fixed;
using std::setprecision;
using std::showpoint;
using std::showpos;
using std::vector;
using std::thread;
using std::clock;


// the comparing function

static int compareFun (const void * a, const void * b)
{
    if ((*(double*)a  > *(double*)b)) {
        return  1;
    };
    if ((*(double*)a  < *(double*)b)) {
        return -1;
    };

    return 0; // to avoid warning massages
}

// the main function

int main ()
{
    // variables and parameters

    const long DIM_MAX = static_cast<long>(pow(10.0, 7.8));
    const long DO_MAX = static_cast<long>(pow(10.0, 6.5));
    const long NUM_THREADS = 4;
    vector<thread> vecThreads;
    long i;
    long k;
    long j;
    clock_t t1;
    clock_t t2;
    double tall;

    // output format

    cout << fixed;
    cout << setprecision(5);
    cout << showpoint;
    cout << showpos;

    // main do-loop

    for (k = 0; k < DO_MAX; k++) {
        // the counter

        cout << "-------------------------------------------------->> " << k << endl;

        // timing

        t1 = clock();

        // the arrays to be sorted

        cout << "  1 --> declare the arrays ..." << endl;

        double ** p = new double * [NUM_THREADS];

        for (i = 0; i < NUM_THREADS; i++) {
            p[i] = new double [DIM_MAX];
        }

        // build the arrays

        cout << "  2 --> building the arrays in parallel ..." << endl;

        #pragma omp parallel for  \
        default(none) \
        private(j, i) \
        shared(p)     \
        num_threads(NUM_THREADS)

        for (j = 0; j < NUM_THREADS; j++) {
            for (i = 0; i < DIM_MAX; i++) {
                p[j][i] = cos(static_cast<double>(i));
            }
        }

        // sort the arrays

        cout << "  3 --> spawning " << NUM_THREADS << " threads ..." << endl;
        cout << "  4 --> sorting ..." << endl;

        for (i = 0; i < NUM_THREADS; i++) {
            vecThreads.push_back(thread(&qsort, &p[i][0], DIM_MAX, sizeof(double), compareFun));
        }

        // join threads

        for (auto & t : vecThreads) {
            t.join();
        }

        // clear vector

        cout << "  5 --> clearing the threads vector ..." << endl;

        vecThreads.clear();

        // delete the arrays

        cout << "  6 --> free up RAM..." << endl;

        for (j = 0; j < NUM_THREADS; j++) {
            delete [] p[j];
        }

        delete [] p;

        // timing

        t2 = clock();
        tall = (t2-t1)/static_cast<double>(CLOCKS_PER_SEC);

        // report

        cout << "  7 --> time used -->> " << tall << endl;
    }

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
