#include <iostream>

class A1 {
public:
    virtual auto f1A1() const -> void {
        fc();
    }

private:
    virtual auto fc() const -> void {
        std::cout << "-------------->> fA2 from A1" << std::endl;
    }
};

class A2 : public A1 {
public:
    auto f1A2() const -> void {
        f1A1();
    }

private:
    auto fc() const -> void final {
        std::cout << "-------------->> fA2 from A2" << std::endl;
    }
};

auto main() -> int {

    {
        std::cout << "--->> 1" << std::endl;
        A2 a;
        a.f1A2();
    }

    {
        std::cout << "--->> 2" << std::endl;
        A1 a;
        a.f1A1();
    }

    {
        std::cout << "--->> 3" << std::endl;
        A1 *a{new A2()};
        dynamic_cast<A2 *>(a)->f1A2();
    }

    {
        std::cout << "--->> 4" << std::endl;
        A1 *a{new A2()};
        a->f1A1();
    }
}
