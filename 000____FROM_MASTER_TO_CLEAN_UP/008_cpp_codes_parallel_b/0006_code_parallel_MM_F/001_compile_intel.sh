#!/bin/bash

  # 1. compile

  icpc -O1                 \
       -Wall               \
	  -wd1202             \
       -std=c++11          \
	  -openmp             \
	  -D_GLIBCXX_PARALLEL \
	  functors.cpp        \
	  functions.cpp       \
       driver_program.cpp  \
       -o x_intel
