//==============//
// parallel all //
//==============//

#include "functors.h"
#include "functions.h"

#include <iostream>
#include <vector>
#include <iterator>
#include <cmath>
#include <ctime>
#include <iomanip>
#include <chrono>
#include <functional>

#include <parallel/algorithm>
#include <parallel/numeric>
#include <parallel/settings.h>

using std::cout;
using std::endl;
using std::vector;
using std::pow;
using std::fixed;
using std::setprecision;
using namespace std::chrono;
using std::plus;
using std::boolalpha;
using std::to_string;

// type definition

typedef unsigned long int uli;

// the main function

int main()
{
    cout << fixed;
    cout << setprecision(5);
    cout << boolalpha;

    //===================//
    // parallel settings //
    //===================//

    //==========================================================//
    // FORCE ALL POSSIBLE ALGORITHMS TO BE EXECUTED IN PARALLEL //
    //==========================================================//

    __gnu_parallel::_Settings s;
    s.algorithm_strategy = __gnu_parallel::force_parallel;
    __gnu_parallel::_Settings::set(s);

    //==========================//
    // end of parallel settings //
    //==========================//

    const uli DIM1 = 1 * uli(pow(10.0, 8.0));
    const uli TRIALS1 = 2 * uli(pow(10.0, 0.0)); // HEAVY
    const uli TRIALS2 = 4 * uli(pow(10.0, 0.0)); // LESS HEAVY

    const double ELEM1 = 1.0;
    const double INIT = 0.0;

    double a1;
    bool res_bool;

    vector<double> v1;
    vector<double> v3;
    const vector<double> v2(2, -1);

    v1.resize(DIM1);
    v3.resize(DIM1);

    auto t1 = system_clock::now();
    auto t2 = system_clock::now();
    duration<double> time_span;

    // --> A-1
    // vector constructor

    cout << " --> vector constructor --> non-parallel ---------------> A-1" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        vector<double> v1(DIM1, ELEM1);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // --> A-2
    // iota

    cout << " --> iota --> non-parallel -----------------------------> A-2" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        iota(v1.begin(), v1.end(), 0.0);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // --> 1-1
    // accumulate # 1

    cout << " --> accumulate # 1 --> default ------------------------> 1-1" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        accumulate(v1.begin(), v1.end(), INIT);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // --> 1-2
    // accumulate # 2

    cout << " --> accumulate # 2 --> functor ------------------------> 1-2" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        accumulate(v1.begin(), v1.end(), INIT, ac_functor<double>());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // --> 1-3
    // accumulate # 3

    cout << " --> accumulate # 3 --> function -----------------------> 1-3" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        accumulate(v1.begin(), v1.end(), INIT, &ac_fun<double>);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // --> 2-1
    // adjacent_difference # 1

    cout << " --> adjacent_difference # 1 --> default ---------------> 2-1" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        adjacent_difference(v1.begin(), v1.end(), v3.begin());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // --> 2-2
    // adjacent_difference # 2

    cout << " --> adjacent_difference # 2 --> function --------------> 2-2" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        adjacent_difference(v1.begin(), v1.end(), v3.begin(), &ac_fun<double>);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // --> 2-3
    // adjacent_difference # 3

    cout << " --> adjacent_difference # 3 --> functor ---------------> 2-3" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        adjacent_difference(v1.begin(), v1.end(), v3.begin(), ac_functor<double>());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // --> 3-1
    // inner_product # 1

    cout << " --> inner_product # 1 --> function --------------------> 3-1" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        a1 = inner_product(v1.begin(), v1.end(), v3.begin(),
                           INIT,
                           &ac_fun<double>,
                           &product_fun<double>);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> inner_product result = " << a1 << endl;

    // --> 3-2
    // inner_product # 2

    cout << " --> inner_product # 2 --> functor ---------------------> 3-2" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        a1 = inner_product(v1.begin(), v1.end(), v3.begin(),
                           INIT,
                           ac_functor<double>(),
                           product_functor<double>());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> inner_product result = " << a1 << endl;

    // --> 4-1
    // partial_sum # 1

    cout << " --> partial_sum # 1 --> default -----------------------> 4-1" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        partial_sum(v1.begin(), v1.end(), v3.begin());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // --> 4-2
    // partial_sum # 2

    cout << " --> partial_sum # 2 --> function ----------------------> 4-2" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        partial_sum(v1.begin(), v1.end(), v3.begin(), &ac_fun<double>);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // --> 4-3
    // partial_sum # 3

    cout << " --> partial_sum # 3 --> functor -----------------------> 4-3" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        partial_sum(v1.begin(), v1.end(), v3.begin(), ac_functor<double>());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // --> 5-1
    // adjacent_find # 1

    cout << " --> adjacent_find # 1 --> default ---------------------> 5-1" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        adjacent_find(v1.begin(), v1.end());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // --> 5-2
    // adjacent_find # 2

    cout << " --> adjacent_find # 2 --> function --------------------> 5-2" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        adjacent_find(v1.begin(), v1.end(), &equal_fun<double>);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // --> 5-3
    // adjacent_find # 3

    cout << " --> adjacent_find # 3 --> functor ---------------------> 5-3" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        adjacent_find(v1.begin(), v1.end(), equal_functor<double>());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // --> 6
    // count

    cout << " --> count ---------------------------------------------> 6" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        a1 = count(v1.begin(), v1.end(), -1.0);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> a1 = " << a1 << endl;

    // --> 7-1
    // count_if # 1

    cout << " --> count_if # 1 --> function -------------------------> 7-1" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        a1 = count_if(v1.begin(), v1.end(), &isOdd_fun<double>);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> a1 = " << a1 << endl;

    // --> 7-2
    // count_if # 2

    cout << " --> count_if # 2 --> functor --------------------------> 7-2" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        a1 = count_if(v1.begin(), v1.end(), isOdd_functor<double>());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> a1 = " << a1 << endl;

    // --> 8-1
    // equal # 1

    cout << " --> equal # 1 --> default -----------------------------> 8-1" << endl;

    iota(v1.begin(), v1.end(), 0.0);
    iota(v3.begin(), v3.end(), 0.0);

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        res_bool = equal(v1.begin(), v1.end(), v3.begin());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> equal result = " << res_bool << endl;

    // --> 8-2
    // equal # 2

    cout << " --> equal # 2 --> function ----------------------------> 8-2" << endl;

    iota(v1.begin(), v1.end(), 0.0);
    iota(v3.begin(), v3.end(), 0.0);

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        res_bool = equal(v1.begin(), v1.end(), v3.begin(), &equal_fun<double>);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> equal result = " << res_bool << endl;

    // --> 8-3
    // equal # 3

    cout << " --> equal # 3 --> functor -----------------------------> 8-3" << endl;

    iota(v1.begin(), v1.end(), 0.0);
    iota(v3.begin(), v3.end(), 0.0);

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        res_bool = equal(v1.begin(), v1.end(), v3.begin(), equal_functor<double>());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> equal result = " << res_bool << endl;

    // --> 9
    // find

    cout << " --> find ----------------------------------------------> 9" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        find(v1.begin(), v1.end(), -1.0);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // --> 10-1
    // find_if # 1

    cout << " --> find_if # 1 --> lamda function --------------------> 10-1" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        find_if(v1.begin(), v1.end(), [](const double & i) {
            return (i < -1.0);
        });
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // --> 10-2
    // find_if # 2

    cout << " --> find_if # 2 --> function --------------------------> 10-2" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        find_if(v1.begin(), v1.end(), &lessThanMinusOne_fun<double>);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // --> 10-3
    // find_if # 3

    cout << " --> find_if # 3 --> functor ---------------------------> 10-3" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        find_if(v1.begin(), v1.end(), lessThanMinusOne_functor<double>());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // --> 11
    // find_first_of

    // --> 12-1
    // for_each # 1

    cout << " --> for_each # 1 --> functor --------------------------> 12-1" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        for_each(v1.begin(), v1.end(), Sum_functor<double>());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // --> 12-2
    // for_each # 2

    cout << " --> for_each # 2 --> function -------------------------> 12-2" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        for_each(v1.begin(), v1.end(), &double_fun<double>);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // --> 12-3
    // for_each # 3

    cout << " --> for_each # 3 --> lamda function -------------------> 12-3" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        for_each(v1.begin(), v1.end(), [](const double & i) {
            return 2*i;
        });
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // --> 13-1
    // generate # 1

    cout << " --> generate # 1 --> functor --------------------------> 13-1" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        generate(v1.begin(), v1.end(), UniqueNumber_functor<double>());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // --> 13-2
    // generate # 2

    cout << " --> generate # 2 --> function -------------------------> 13-2" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        generate(v1.begin(), v1.end(), &UniqueNumber_fun<double>);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // --> 13-3
    // generate # 3

    cout << " --> generate # 3 --> lamda function -------------------> 13-3" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        generate(v1.begin(), v1.end(), []() {
            return 123.4;
        });
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // --> 14-1
    // generate_n # 1

    cout << " --> generate_n # 1 --> functor ------------------------> 14-1" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        generate_n(v1.begin(), DIM1, UniqueNumber_functor<double>());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // --> 14-2
    // generate_n # 2

    cout << " --> generate_n # 2 --> function -----------------------> 14-2" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        generate_n(v1.begin(), DIM1, &UniqueNumber_fun<double>);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // --> 14-3
    // generate_n # 3

    cout << " --> generate_n # 3 --> lamda function -----------------> 14-3" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        generate_n(v1.begin(), DIM1, []() {
            return 123.4;
        });
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // --> 15-1
    // lexicographical_compare # 1

    vector<char> * pvA = new vector<char>(DIM1, 'a');
    vector<char> * pvB = new vector<char>(DIM1, 'a');

    cout << " --> lexicographical_compare # 1 --> lamda function ----> 15-1" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        res_bool = lexicographical_compare(pvA->begin(), pvA->end(),
                                           pvB->begin(), pvB->end(),
        [](const char & x, const char & y) {
            return (x < y);
        });
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> res_bool = " << res_bool << endl;

    // --> 15-2
    // lexicographical_compare # 2

    cout << " --> lexicographical_compare # 2 --> function ----------> 15-2" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        res_bool = lexicographical_compare(pvA->begin(), pvA->end(),
                                           pvB->begin(), pvB->end(),
                                           &strComp_fun<char>);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> res_bool = " << res_bool << endl;

    // --> 15-3
    // lexicographical_compare # 3

    cout << " --> lexicographical_compare # 3 --> functor -----------> 15-3" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        res_bool = lexicographical_compare(pvA->begin(), pvA->end(),
                                           pvB->begin(), pvB->end(),
                                           strComp_functor<char>());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> res_bool = " << res_bool << endl;

    // --> 15-4
    // lexicographical_compare # 4

    cout << " --> lexicographical_compare # 4 --> default -----------> 15-4" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        res_bool = lexicographical_compare(pvA->begin(), pvA->end(),
                                           pvB->begin(), pvB->end());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> res_bool = " << res_bool << endl;

    // delete the local char vector pointer

    delete pvA;
    delete pvB;

    // --> 17
    // search

    cout << " --> search --------------------------------------------> 17" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        search(v1.begin(), v1.end(), v2.begin(), v2.end());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // --> 18
    // search_n

    cout << " --> search_n ------------------------------------------> 18" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        search_n(v1.begin(), v1.end(), 2, -11.22);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // --> 19
    // transform

    cout << " --> transform -----------------------------------------> 19" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        transform(v1.begin(), v1.end(), v1.begin(), [](double & i) {
            return i++;
        });
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // --> 28
    // random_shuffle

    cout << " --> random_shuffle ------------------------------------> 28" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS1; i++) {
        random_shuffle(v1.begin(), v1.end());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // --> 33
    // sort

    cout << " --> sort ----------------------------------------------> 33" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS1; i++) {
        sort(v1.begin(), v1.end());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // --> 34
    // stable_sort

    cout << " --> stable_sort ---------------------------------------> 34" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS1; i++) {
        stable_sort(v1.begin(), v1.end());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // --> 35
    // unique_copy

    cout << " --> unique_copy ---------------------------------------> 35" << endl;

    iota(v1.begin(), v1.end(), 0.0);
    random_shuffle(v1.begin(), v1.end());

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        unique_copy(v1.begin(), v1.end(), v3.begin());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    //===========//
    // LAST THIS //
    //===========//

    // nth_element

    cout << " --> nth_element ---------------------------------------> LAST" << endl;

    iota(v1.begin(), v1.end(), 0.0);
    random_shuffle(v1.begin(), v1.end());

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        nth_element(v1.begin(), v1.begin()+i, v1.end());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    return 0;
}

// end
