#!/bin/bash

  # 1. compile

  g++  -O3                       \
       -Wall                     \
       -std=c++0x                \
	  -fopenmp                  \
	  -march=native             \
	  -fwhole-program           \
	  -D_GLIBCXX_PARALLEL       \
       driver_program.cpp        \
       -o x_gnu_ubu
