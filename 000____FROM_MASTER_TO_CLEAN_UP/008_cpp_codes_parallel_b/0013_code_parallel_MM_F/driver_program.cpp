#include <iostream>
#include <vector>
#include <iterator>
#include <numeric>
#include <cmath>
#include <chrono>
#include <parallel/algorithm>
#include <parallel/settings.h>


int main()
{
    const auto DIM1 = static_cast<uint64_t>(pow(10.0, 8.0));
    const auto TRIALS1 = static_cast<uint64_t>(pow(10.0, 1.0));

    {
        __gnu_parallel::_Settings s;
        s.algorithm_strategy = __gnu_parallel::force_parallel;
        __gnu_parallel::_Settings::set(s);

        std::vector<double> v1(DIM1);

        std::cout << " --> building v1..." << std::endl;

        std::iota(v1.begin(), v1.end(), 0.0);

        std::cout << " --> sorting v1 in a parallel..." << std::endl;

        auto t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS1; i++) {
            std::sort(v1.begin(), v1.end());
        }

        auto t2 = std::chrono::system_clock::now();
        auto time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2-t1);

        std::cout << " --> time used (parallel) = " << time_span.count() << std::endl;
        std::cout << " --> some results :" << std::endl;

        std::cout << " --> v1[0] = " << v1[0] << std::endl;
        std::cout << " --> v1[1] = " << v1[1] << std::endl;
        std::cout << " --> v1[2] = " << v1[2] << std::endl;
        std::cout << " --> v1[3] = " << v1[3] << std::endl;
    }

    {
        __gnu_parallel::_Settings s;
        s.algorithm_strategy = __gnu_parallel::force_sequential;
        __gnu_parallel::_Settings::set(s);

        std::vector<double> v1(DIM1);

        std::cout << " --> building v1..." << std::endl;

        std::iota(v1.begin(), v1.end(), 0.0);

        std::cout << " --> sorting v1 in serial..." << std::endl;

        auto t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS1; i++) {
            std::sort(v1.begin(), v1.end());
        }

        auto t2 = std::chrono::system_clock::now();
        auto time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2-t1);

        std::cout << " --> time used (serial) = " << time_span.count() << std::endl;
        std::cout << " --> some results :" << std::endl;

        std::cout << " --> v1[0] = " << v1[0] << std::endl;
        std::cout << " --> v1[1] = " << v1[1] << std::endl;
        std::cout << " --> v1[2] = " << v1[2] << std::endl;
        std::cout << " --> v1[3] = " << v1[3] << std::endl;
    }

    return 0;
}