#include <iostream>
#include <fstream>
#include <chrono>

int main()
{
    std::ofstream myfile;
    std::string file_base_name("x_example.txt");
    const uint64_t FILES_NUM(4);
    const uint64_t FILES_LEN(200000);
    const uint64_t K_MAX(100);

    for (uint64_t k = 1; k != K_MAX; k++) {

        std::cout << "============================================================================" << std::endl;
        std::cout << std::endl;
        std::cout << "--> write in parallel ------------------------------------------------> k = " << k << std::endl;
        std::cout << std::endl;
        auto time_now = std::chrono::system_clock::now();
        auto time_start = std::chrono::system_clock::to_time_t(time_now);
        std::cout << "--> write time --> starts --> " << std::ctime(&time_start); // << std::endl;

        #pragma omp parallel for private(myfile)

        for (uint64_t i = 0; i < FILES_NUM; i++) {
            myfile.open(file_base_name + std::to_string(i));

            for (uint64_t j = 0; j <= FILES_LEN; j++) {
                myfile << "Writing this to a file" << std::endl;
            }

            myfile.flush();
            myfile.close();
        }

        time_now = std::chrono::system_clock::now();
        auto time_end = std::chrono::system_clock::to_time_t(time_now);
        std::cout << "--> write time --> ends --> " << std::ctime(&time_end) << std::endl;

        std::cout << "--> delete in parallel -----------------------------------------------> k = " << k << std::endl;
        std::cout << std::endl;

        time_now = std::chrono::system_clock::now();
        time_start = std::chrono::system_clock::to_time_t(time_now);
        std::cout << "--> delete time --> starts --> " << std::ctime(&time_start); //<< std::endl;

        #pragma omp parallel for
        for (uint64_t i = 0; i < FILES_NUM; i++) {
            const char * tmp = ("example.txt" + std::to_string(i)).c_str();
            remove(tmp);
        }

        time_now = std::chrono::system_clock::now();
        time_end = std::chrono::system_clock::to_time_t(time_now);
        std::cout << "--> delete time --> ends --> " << std::ctime(&time_end) << std::endl;
    }

    return 0;
}
