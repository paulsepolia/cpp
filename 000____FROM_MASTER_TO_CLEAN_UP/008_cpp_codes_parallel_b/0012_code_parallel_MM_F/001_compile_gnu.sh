#!/bin/bash

  # 1. compile

  g++  -O3                       \
       -Wall                     \
       -std=c++0x                \
	   -fopenmp                  \
	   -march=native             \
	   -fwhole-program           \
       driver_program.cpp        \
       -o x_gnu
