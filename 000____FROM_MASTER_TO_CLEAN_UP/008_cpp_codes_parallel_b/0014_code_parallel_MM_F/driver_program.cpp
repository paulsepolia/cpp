#include <iostream>
#include <vector>
#include <iterator>
#include <cmath>
#include <ctime>
#include <iomanip>
#include <chrono>
#include <functional>
#include <random>
#include <parallel/algorithm>
#include <parallel/numeric>
#include <parallel/settings.h>

// functor # 1

template<typename T>
struct Sum {
public:

    // constructor

    Sum() : sum(0) {}

    // functor

    void operator()(const T &n) {
        sum += n;
    }

private:

    // local variable

    T sum;
};

// functor # 2

template<typename T>
struct UniqueNumber {
    double operator()() {
        return T(123.456);
    }
};

// functor # 3

template<typename T>
struct ac_functor {
    T operator()(const T &x, const T &y) {
        return x + y;
    }
};

// functor # 4

template<typename T>
struct product_functor {
    T operator()(const T &x, const T &y) {
        return x * y;
    }
};

// functor # 5

template<typename T>
struct equal_functor {
    bool operator()(const T &i, const T &j) {
        return (i == j);
    }
};

// function # 1

template<typename T>
T ac_fun(const T &x, const T &y) {
    return x + y;
}

// function # 2

template<typename T>
T product_fun(const T &x, const T &y) {
    return x * y;
}

// function # 3

template<typename T>
bool equal_fun(const T &i, const T &j) {
    return (i == j);
}

int main() {
    std::cout << std::fixed;
    std::cout << std::setprecision(5);
    std::cout << std::boolalpha;

    const uint64_t DIM1 = 1 * uint64_t(std::pow(10.0, 8.0));
    const uint64_t TRIALS1 = 5 * uint64_t(std::pow(10.0, 0.0));
    const uint64_t TRIALS2 = 5 * uint64_t(std::pow(10.0, 1.0));
    const double ELEM1 = 1.0;
    const double INIT = 0.0;

    {
        //===================//
        // parallel settings //
        //===================//

        //==========================================================//
        // FORCE ALL POSSIBLE ALGORITHMS TO BE EXECUTED IN PARALLEL //
        //==========================================================//

        __gnu_parallel::_Settings s;
        s.algorithm_strategy = __gnu_parallel::force_parallel;
        __gnu_parallel::_Settings::set(s);

        //==========================//
        // end of parallel settings //
        //==========================//

        double a1 = 0;
        bool res_bool = true;

        std::vector<double> v1;
        std::vector<double> v3;
        v1.resize(DIM1);
        v3.resize(DIM1);

        // std::vector constructor

        std::cout << " --> std::vector constructor --> non-parallel" << std::endl;

        auto t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS2; i++) {
            std::vector<double> v1(DIM1, ELEM1);
        }

        auto t2 = std::chrono::system_clock::now();
        auto time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;

        // iota

        std::cout << " --> iota --> non-parallel" << std::endl;

        t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS2; i++) {
            iota(v1.begin(), v1.end(), 0.0);
        }

        t2 = std::chrono::system_clock::now();
        time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;

        // shuffle

        std::cout << " --> shuffle --> parallel" << std::endl;

        t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS1; i++) {

            auto seed = std::chrono::system_clock::now().time_since_epoch().count();

            std::shuffle(v1.begin(), v1.end(), std::default_random_engine(seed));
        }

        t2 = std::chrono::system_clock::now();
        time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;

        // sort

        std::cout << " --> sort --> parallel" << std::endl;

        t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS1; i++) {
            sort(v1.begin(), v1.end());
        }

        t2 = std::chrono::system_clock::now();
        time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;

        // stable_sort

        std::cout << " --> stable_sort --> parallel" << std::endl;

        t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS1; i++) {
            stable_sort(v1.begin(), v1.end());
        }

        t2 = std::chrono::system_clock::now();
        time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;

        // find

        std::cout << " --> find --> parallel" << std::endl;

        t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS2; i++) {
            find(v1.begin(), v1.end(), -1.0);
        }

        t2 = std::chrono::system_clock::now();
        time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;

        // find_if

        std::cout << " --> find_if --> parallel" << std::endl;

        t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS2; i++) {
            find_if(v1.begin(), v1.end(), [](const double &i) {
                return (i < -1.0);
            });
        }

        t2 = std::chrono::system_clock::now();
        time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;

        // transform

        std::cout << " --> transform --> parallel" << std::endl;

        t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS2; i++) {
            std::transform(v1.begin(), v1.end(), v1.begin(), [](double &i) {
                return i++;
            });
        }

        t2 = std::chrono::system_clock::now();
        time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;

        // search

        std::cout << " --> search --> parallel" << std::endl;

        std::vector<double> v2(2, -1);

        t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS2; i++) {
            std::search(v1.begin(), v1.end(), v2.begin(), v2.end());
        }

        t2 = std::chrono::system_clock::now();
        time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;

        // search_n

        std::cout << " --> search_n --> parallel" << std::endl;

        t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS2; i++) {
            search_n(v1.begin(), v1.end(), 2, -11.22);
        }

        t2 = std::chrono::system_clock::now();
        time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;

        // for_each

        std::cout << " --> for_each --> parallel" << std::endl;

        t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS2; i++) {
            for_each(v1.begin(), v1.end(), Sum<double>());
        }

        t2 = std::chrono::system_clock::now();
        time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;

        // generate

        std::cout << " --> generate --> parallel" << std::endl;

        t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS2; i++) {
            generate(v1.begin(), v1.end(), UniqueNumber<double>());
        }

        t2 = std::chrono::system_clock::now();
        time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;

        // generate_n

        std::cout << " --> generate_n --> parallel" << std::endl;

        t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS2; i++) {
            generate_n(v1.begin(), DIM1, UniqueNumber<double>());
        }

        t2 = std::chrono::system_clock::now();
        time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;

        // unique_copy

        std::cout << " --> unique_copy --> parallel" << std::endl;

        iota(v1.begin(), v1.end(), 0.0);

        auto seed = std::chrono::system_clock::now().time_since_epoch().count();

        std::shuffle(v1.begin(), v1.end(), std::default_random_engine(seed));

        t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS2; i++) {
            unique_copy(v1.begin(), v1.end(), v3.begin());
        }

        t2 = std::chrono::system_clock::now();
        time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;

        // accumulate # 1

        std::cout << " --> accumulate # 1 --> default --> parallel" << std::endl;

        t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS2; i++) {
            accumulate(v1.begin(), v1.end(), INIT);
        }

        t2 = std::chrono::system_clock::now();
        time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;

        // accumulate # 2

        std::cout << " --> accumulate # 2 --> functor --> parallel" << std::endl;

        t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS2; i++) {
            std::accumulate(v1.begin(), v1.end(), INIT, ac_functor<double>());
        }

        t2 = std::chrono::system_clock::now();
        time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;

        // accumulate # 3

        std::cout << " --> accumulate # 3 --> function --> parallel" << std::endl;

        t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS2; i++) {
            accumulate(v1.begin(), v1.end(), INIT, &ac_fun<double>);
        }

        t2 = std::chrono::system_clock::now();
        time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;

        // inner_product # 1

        std::cout << " --> inner_product # 1 --> functions --> parallel" << std::endl;

        t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS2; i++) {

            a1 = std::inner_product(v1.begin(), v1.end(), v3.begin(),
                                    INIT,
                                    &ac_fun<double>,
                                    &product_fun<double>);
        }

        t2 = std::chrono::system_clock::now();
        time_span = std::chrono::duration_cast<std::chrono::duration<double >>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;
        std::cout << " --> inner_product result = " << a1 << std::endl;

        // inner_product # 2

        std::cout << " --> inner_product # 2 --> functors --> parallel" << std::endl;

        t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS2; i++) {

            a1 = std::inner_product(v1.begin(), v1.end(), v3.begin(),
                                    INIT,
                                    ac_functor<double>(),
                                    product_functor<double>());
        }

        t2 = std::chrono::system_clock::now();
        time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;
        std::cout << " --> inner_product result = " << a1 << std::endl;

        // equal # 1

        std::cout << " --> equal # 1 --> default --> parallel" << std::endl;

        iota(v1.begin(), v1.end(), 0.0);
        iota(v3.begin(), v3.end(), 0.0);

        t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS2; i++) {
            res_bool = equal(v1.begin(), v1.end(), v3.begin());
        }

        t2 = std::chrono::system_clock::now();
        time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;
        std::cout << " --> equal result = " << res_bool << std::endl;

        // equal # 2

        std::cout << " --> equal # 2 --> functions --> parallel" << std::endl;

        iota(v1.begin(), v1.end(), 0.0);
        iota(v3.begin(), v3.end(), 0.0);

        t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS2; i++) {
            res_bool = equal(v1.begin(), v1.end(), v3.begin(), &equal_fun<double>);
        }

        t2 = std::chrono::system_clock::now();
        time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;
        std::cout << " --> equal result = " << res_bool << std::endl;

        // equal # 3

        std::cout << " --> equal # 3 --> functors --> parallel" << std::endl;

        iota(v1.begin(), v1.end(), 0.0);
        iota(v3.begin(), v3.end(), 0.0);

        t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS2; i++) {
            res_bool = equal(v1.begin(), v1.end(), v3.begin(), equal_functor<double>());
        }

        t2 = std::chrono::system_clock::now();
        time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;
        std::cout << " --> equal result = " << res_bool << std::endl;

        //===========//
        // LAST THIS //
        //===========//

        // nth_element

        std::cout << " --> nth_element --> parallel " << std::endl;

        iota(v1.begin(), v1.end(), 0.0);

        seed = std::chrono::system_clock::now().time_since_epoch().count();

        std::shuffle(v1.begin(), v1.end(), std::default_random_engine(seed));

        t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS2; i++) {
            std::nth_element(v1.begin(), v1.begin() + i, v1.end());
        }

        t2 = std::chrono::system_clock::now();
        time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;

        // results

        std::cout << " --> some results :" << std::endl;

        std::cout << " --> v1[0] = " << v1[0] << std::endl;
        std::cout << " --> v1[1] = " << v1[1] << std::endl;
        std::cout << " --> v1[2] = " << v1[2] << std::endl;
        std::cout << " --> v1[3] = " << v1[3] << std::endl;
        std::cout << " --> v1[TRIALS2-1] = " << v1[TRIALS2 - 1]
                  << " --> TRAILS2-1 = " << TRIALS2 - 1 << std::endl;

    }

    {
        //=================//
        // serial settings //
        //=================//

        //=========================================================//
        // FORCE ALL POSSIBLE ALGORITHMS TO BE EXECUTED SEQUENTIAL //
        //=========================================================//

        __gnu_parallel::_Settings s;
        s.algorithm_strategy = __gnu_parallel::force_sequential;
        __gnu_parallel::_Settings::set(s);

        //========================//
        // end of serial settings //
        //========================//

        double a1 = 0;
        bool res_bool = true;

        std::vector<double> v1;
        std::vector<double> v3;
        v1.resize(DIM1);
        v3.resize(DIM1);

        // std::vector constructor

        std::cout << " --> std::vector constructor --> non-parallel" << std::endl;

        auto t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS2; i++) {
            std::vector<double> v1(DIM1, ELEM1);
        }

        auto t2 = std::chrono::system_clock::now();
        auto time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;

        // iota

        std::cout << " --> iota --> non-parallel" << std::endl;

        t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS2; i++) {
            iota(v1.begin(), v1.end(), 0.0);
        }

        t2 = std::chrono::system_clock::now();
        time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;

        // shuffle

        std::cout << " --> shuffle --> serial" << std::endl;

        t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS1; i++) {

            auto seed = std::chrono::system_clock::now().time_since_epoch().count();

            std::shuffle(v1.begin(), v1.end(), std::default_random_engine(seed));
        }

        t2 = std::chrono::system_clock::now();
        time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;

        // sort

        std::cout << " --> sort --> serial" << std::endl;

        t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS1; i++) {
            sort(v1.begin(), v1.end());
        }

        t2 = std::chrono::system_clock::now();
        time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;

        // stable_sort

        std::cout << " --> stable_sort --> serial" << std::endl;

        t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS1; i++) {
            stable_sort(v1.begin(), v1.end());
        }

        t2 = std::chrono::system_clock::now();
        time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;

        // find

        std::cout << " --> find --> serial" << std::endl;

        t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS2; i++) {
            find(v1.begin(), v1.end(), -1.0);
        }

        t2 = std::chrono::system_clock::now();
        time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;

        // find_if

        std::cout << " --> find_if --> serial" << std::endl;

        t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS2; i++) {
            find_if(v1.begin(), v1.end(), [](const double &i) {
                return (i < -1.0);
            });
        }

        t2 = std::chrono::system_clock::now();
        time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;

        // transform

        std::cout << " --> transform --> serial" << std::endl;

        t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS2; i++) {
            std::transform(v1.begin(), v1.end(), v1.begin(), [](double &i) {
                return i++;
            });
        }

        t2 = std::chrono::system_clock::now();
        time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;

        // search

        std::cout << " --> search --> serial" << std::endl;

        std::vector<double> v2(2, -1);

        t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS2; i++) {
            std::search(v1.begin(), v1.end(), v2.begin(), v2.end());
        }

        t2 = std::chrono::system_clock::now();
        time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;

        // search_n

        std::cout << " --> search_n --> serial" << std::endl;

        t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS2; i++) {
            search_n(v1.begin(), v1.end(), 2, -11.22);
        }

        t2 = std::chrono::system_clock::now();
        time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;

        // for_each

        std::cout << " --> for_each --> serial" << std::endl;

        t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS2; i++) {
            for_each(v1.begin(), v1.end(), Sum<double>());
        }

        t2 = std::chrono::system_clock::now();
        time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;

        // generate

        std::cout << " --> generate --> serial" << std::endl;

        t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS2; i++) {
            generate(v1.begin(), v1.end(), UniqueNumber<double>());
        }

        t2 = std::chrono::system_clock::now();
        time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;

        // generate_n

        std::cout << " --> generate_n --> serial" << std::endl;

        t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS2; i++) {
            generate_n(v1.begin(), DIM1, UniqueNumber<double>());
        }

        t2 = std::chrono::system_clock::now();
        time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;

        // unique_copy

        std::cout << " --> unique_copy --> serial" << std::endl;

        iota(v1.begin(), v1.end(), 0.0);

        auto seed = std::chrono::system_clock::now().time_since_epoch().count();

        std::shuffle(v1.begin(), v1.end(), std::default_random_engine(seed));

        t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS2; i++) {
            unique_copy(v1.begin(), v1.end(), v3.begin());
        }

        t2 = std::chrono::system_clock::now();
        time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;

        // accumulate # 1

        std::cout << " --> accumulate # 1 --> default --> serial" << std::endl;

        t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS2; i++) {
            accumulate(v1.begin(), v1.end(), INIT);
        }

        t2 = std::chrono::system_clock::now();
        time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;

        // accumulate # 2

        std::cout << " --> accumulate # 2 --> functor --> serial" << std::endl;

        t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS2; i++) {
            std::accumulate(v1.begin(), v1.end(), INIT, ac_functor<double>());
        }

        t2 = std::chrono::system_clock::now();
        time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;

        // accumulate # 3

        std::cout << " --> accumulate # 3 --> function --> serial" << std::endl;

        t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS2; i++) {
            accumulate(v1.begin(), v1.end(), INIT, &ac_fun<double>);
        }

        t2 = std::chrono::system_clock::now();
        time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;

        // inner_product # 1

        std::cout << " --> inner_product # 1 --> functions --> serial" << std::endl;

        t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS2; i++) {

            a1 = std::inner_product(v1.begin(), v1.end(), v3.begin(),
                                    INIT,
                                    &ac_fun<double>,
                                    &product_fun<double>);
        }

        t2 = std::chrono::system_clock::now();
        time_span = std::chrono::duration_cast<std::chrono::duration<double >>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;
        std::cout << " --> inner_product result = " << a1 << std::endl;

        // inner_product # 2

        std::cout << " --> inner_product # 2 --> functors --> serial" << std::endl;

        t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS2; i++) {

            a1 = std::inner_product(v1.begin(), v1.end(), v3.begin(),
                                    INIT,
                                    ac_functor<double>(),
                                    product_functor<double>());
        }

        t2 = std::chrono::system_clock::now();
        time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;
        std::cout << " --> inner_product result = " << a1 << std::endl;

        // equal # 1

        std::cout << " --> equal # 1 --> default --> serial" << std::endl;

        iota(v1.begin(), v1.end(), 0.0);
        iota(v3.begin(), v3.end(), 0.0);

        t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS2; i++) {
            res_bool = equal(v1.begin(), v1.end(), v3.begin());
        }

        t2 = std::chrono::system_clock::now();
        time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;
        std::cout << " --> equal result = " << res_bool << std::endl;

        // equal # 2

        std::cout << " --> equal # 2 --> functions --> serial" << std::endl;

        iota(v1.begin(), v1.end(), 0.0);
        iota(v3.begin(), v3.end(), 0.0);

        t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS2; i++) {
            res_bool = equal(v1.begin(), v1.end(), v3.begin(), &equal_fun<double>);
        }

        t2 = std::chrono::system_clock::now();
        time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;
        std::cout << " --> equal result = " << res_bool << std::endl;

        // equal # 3

        std::cout << " --> equal # 3 --> functors --> serial" << std::endl;

        iota(v1.begin(), v1.end(), 0.0);
        iota(v3.begin(), v3.end(), 0.0);

        t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS2; i++) {
            res_bool = equal(v1.begin(), v1.end(), v3.begin(), equal_functor<double>());
        }

        t2 = std::chrono::system_clock::now();
        time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;
        std::cout << " --> equal result = " << res_bool << std::endl;

        //===========//
        // LAST THIS //
        //===========//

        // nth_element

        std::cout << " --> nth_element --> serial" << std::endl;

        iota(v1.begin(), v1.end(), 0.0);

        seed = std::chrono::system_clock::now().time_since_epoch().count();

        std::shuffle(v1.begin(), v1.end(), std::default_random_engine(seed));

        t1 = std::chrono::system_clock::now();

        for (uint64_t i = 0; i != TRIALS2; i++) {
            std::nth_element(v1.begin(), v1.begin() + i, v1.end());
        }

        t2 = std::chrono::system_clock::now();
        time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
        std::cout << " --> time used = " << time_span.count() << std::endl;

        // results

        std::cout << " --> some results :" << std::endl;

        std::cout << " --> v1[0] = " << v1[0] << std::endl;
        std::cout << " --> v1[1] = " << v1[1] << std::endl;
        std::cout << " --> v1[2] = " << v1[2] << std::endl;
        std::cout << " --> v1[3] = " << v1[3] << std::endl;
        std::cout << " --> v1[TRIALS2-1] = " << v1[TRIALS2 - 1]
                  << " --> TRAILS2-1 = " << TRIALS2 - 1 << std::endl;

    }

    return 0;
}
