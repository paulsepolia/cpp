//======================//
// FUNCTIONS DEFINITION //
//======================//

#include <cstdlib>
#include "functions.h"

using std::rand;

// function # 1

template<> inline
double ac_fun<double>(const double & x, const double & y)
{
    return x+y;
}

// function # 2

template <> inline
double product_fun<double>(const double & x, const double & y)
{
    return x*y;
}

// function # 3

template <> inline
bool equal_fun<double>(const double & i, const double & j)
{
    return (!(i < j) && !(i > j));
}

// function # 4

template <> inline
bool isOdd_fun<double>(const double & i)
{
    return ((unsigned long int)(i)%2 == 1);
}

// function # 5

template <> inline
bool lessThanMinusOne_fun<double>(const double & i)
{
    return (i < -1);
}

// function # 6

template <> inline
double double_fun<double>(const double & x)
{
    return 2*x;
}

// function # 7

template <> inline
double UniqueNumber_fun<double>()
{
    return double(123.456);
}

// function # 8

template <> inline
bool strComp_fun<char>(const char & x, const char & y)
{
    return (x < y);
}

// function # 9

template <> inline
double fun_pp(double & x)
{
    return x++;
}

// function # 10
// random generator function

template <> inline
int rand_fun<double>(const double & i)
{
    return (rand()%static_cast<unsigned long int>(i));
}

// function # 11

template <> inline
bool lessThan_fun<double>(const double & x, const double & y)
{
    return (x < y);
}

// end
