//======================//
// FUNCTIONS DEFINITION //
//======================//

#include "functions.h"

// function # 1

template<>
double ac_fun(const double & x, const double & y)
{
    return x+y;
}

// function # 2

template <>
double product_fun(const double & x, const double & y)
{
    return x*y;
}

// function # 3

template <>
bool equal_fun(const double & i, const double & j)
{
    return (i == j);
}

// function # 4

template <>
bool isOdd_fun(const double & i)
{
    return ((unsigned long int)(i)%2 == 1);
}

// end
