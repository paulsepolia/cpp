#include <iostream>
#include <cstdint>
#include <memory>

int main() {

    {
        std::cout << " --> example --> 1 -------------------> start" << std::endl;

        double d = 111.11;
        auto *pd = new double(d);
        std::shared_ptr<double> spd1(pd);
        std::shared_ptr<double> spd2(pd);

        std::cout << " &d    = " << &d << std::endl;
        std::cout << " spd1  = " << spd1 << std::endl;
        std::cout << " spd2  = " << spd2 << std::endl;
        std::cout << " pd    = " << pd << std::endl;
        std::cout << " d     = " << d << std::endl;
        std::cout << " *spd1 = " << *spd1 << std::endl;
        std::cout << " *spd2 = " << *spd2 << std::endl;
        std::cout << " *pd   = " << *pd << std::endl;

        spd1.reset();

        std::cout << " &d    = " << &d << std::endl;
        std::cout << " spd1  = " << spd1 << std::endl;
        std::cout << " spd2  = " << spd2 << std::endl;
        std::cout << " pd    = " << pd << std::endl;
        std::cout << " d     = " << d << std::endl;
        if (spd1) std::cout << " *spd1 = " << *spd1 << std::endl;
        if (spd2) std::cout << " *spd2 = " << *spd2 << std::endl;
        std::cout << " *pd   = " << *pd << std::endl;

        std::cout << " --> example --> 1 -------------------> end" << std::endl;
    }
}