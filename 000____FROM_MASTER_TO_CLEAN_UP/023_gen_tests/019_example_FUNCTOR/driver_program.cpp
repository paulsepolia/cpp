#include <iostream>
#include <algorithm>
#include <vector>

class add {
public:
    add(int x) : x(x) {}

    int operator()(int y) const { return x + y; }

private:
    int x;
};

class mult {
public:
    mult(int x) : x(x) {}

    int operator()(int y) const { return x * y; }

private:
    int x;
};


int main() {

    std::vector<int> v1{1, 2, 3};
    std::vector<int> v2(v1.size());

    std::cout << " --> 1 --> original vector" << std::endl;

    for (auto &e: v1) {
        std::cout << e << std::endl;
    }

    std::transform(v1.begin(), v1.end(), v2.begin(), add(10));

    std::cout << " --> 2 --> new vector with an input value added" << std::endl;

    for (auto &e: v2) {
        std::cout << e << std::endl;
    }

    std::transform(v1.begin(), v1.end(), v2.begin(), mult(10));

    std::cout << " --> 3 --> new vector with an input value multiplied" << std::endl;

    for (auto &e: v2) {
        std::cout << e << std::endl;
    }

    std::cout << " --> 4 --> " << add(11)(12) << std::endl;
    std::cout << " --> 5 --> " << mult(11)(12) << std::endl;
}