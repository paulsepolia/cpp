#include<iostream>

class Base {
public:
    virtual void show() { std::cout << " --> In Base " << std::endl; }
};

class Derived : public Base {
public:
    void show() { std::cout << " --> In Derived " << std::endl; }
};

int main() {

    std::cout << " --> 1" << std::endl;

    Base *bp0(new Base);
    bp0->show();

    std::cout << " --> 2" << std::endl;

    Base *bp1(new Derived);
    bp1->show();  // RUN-TIME POLYMORPHISM

    std::cout << " --> 3" << std::endl;

    Derived *dp0(new Derived);
    dp0->show();

    Base &br1(*bp1);
    br1.show();

    return 0;
}