#include <iostream>

class Base {
protected:
    int i;
public:
    Base(int a) { i = a; }

    virtual void display() { std::cout << "I am Base class object, i = " << i << std::endl; }
};

class Derived : public Base {
    int j;
public:
    Derived(int a, int b) : Base(a) { j = b; }

    virtual void display() {
        std::cout << "I am Derived class object, i = "
             << i << ", j = " << j << std::endl;
    }
};


void some_func (Base *p_obj)
{
    p_obj->display();
}

int main()
{
    Base *bp = new Base(33) ;
    Derived *dp = new Derived(45, 54);
    some_func(bp);
    some_func(dp);  // No Object Slicing
    return 0;
}