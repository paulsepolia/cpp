#include<iostream>

class Base {
public:
    virtual void show() = 0;
};

class Derived : public Base {
public:
    virtual void show() {
        std::cout << " --> Derived" << std::endl;
    }
};


int main() {
    //Base b; // COMPILER ERROR
    //Base *bp0(new Base); // COMPILER ERROR
    Base *bp1(new Derived);
    Derived *dp0(new Derived);

    //bp0->show();
    bp1->show();
    dp0->show();

    return 0;
}