#include <iostream>

class Weapon {
public:
    virtual void features() { std::cout << " --> Loading weapon features" << std::endl; }
};

class Bomb : public Weapon {
public:
    void features() { std::cout << " --> Loading bomb features" << std::endl; }
};

class Gun : public Weapon {
public:
    void features() { std::cout << " --> Loading gun features" << std::endl; }
};

class Loader {
public:
    void loadFeatures(Weapon *weapon) {
        weapon->features();
    }
};

int main() {
    Loader *l(new Loader);
    Weapon *pw;

    Weapon w;
    Bomb b;
    Gun g;

    std::cout << " --> 1" << std::endl;

    pw = &w;
    l->loadFeatures(pw);

    std::cout << " --> 2" << std::endl;

    pw = &b;
    l->loadFeatures(pw);

    std::cout << " --> 3" << std::endl;

    pw = &g;
    l->loadFeatures(pw);

    return 0;
}