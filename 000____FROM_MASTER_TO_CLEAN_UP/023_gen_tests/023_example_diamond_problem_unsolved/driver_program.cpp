#include <iostream>

// DOES NOT COMPILE

class A {

public:

    void print_fun() {
        std::cout << " -->> A" << std::endl;
    }
};

class B : public virtual A {

public:

    void print_fun() {
        std::cout << " -->> B" << std::endl;
    }
};

class C : public virtual A {

public:

    void print_fun() {
        std::cout << " -->> C" << std::endl;
    }
};

class D : public virtual B, public virtual C {
};


int main() {

    D d;
    d.print_fun();

    A a;
    a.print_fun();

    B b;
    b.print_fun();

    C c;
    c.print_fun();

    return 0;
}
