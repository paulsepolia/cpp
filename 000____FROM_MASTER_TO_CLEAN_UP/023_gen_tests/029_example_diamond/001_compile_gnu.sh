#!/bin/bash

  # 1. compile

  g++-7.2.0   -O3                \
              -Wall              \
              -std=gnu++14       \
              driver_program.cpp \
              -o x_gnu
