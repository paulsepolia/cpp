#include <iostream>
#include <boost/bind.hpp>
#include <boost/function.hpp>

class print_number {
public:
    void operator()(int i) {
        std::cout << i << std::endl;
    }
};

void print_number_fun(int i) {
    std::cout << i << std::endl;
}

int main() {

    // use of functor to make function

    std::cout << " --> use of functors to make function" << std::endl;

    std::function<void()> f1 = std::bind(print_number(), 2);
    std::function<void()> f2 = std::bind(print_number(), 3);

    f1();
    f2();

    // use of function to make function

    std::cout << " --> use of functions to make function" << std::endl;

    std::function<void()> f3 = std::bind(print_number_fun, 4);
    std::function<void()> f4 = std::bind(print_number_fun, 5);

    f3();
    f4();

    return 0;
}

