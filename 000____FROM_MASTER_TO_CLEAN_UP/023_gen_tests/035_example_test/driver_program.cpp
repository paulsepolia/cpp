#include <iostream>
#include <cmath>
#include <exception>
#include <memory>


int main() {

    const uint64_t DIM1(std::pow(10.0, 9.0));
    const uint64_t I_DO_MAX(std::pow(10.0, 6.0));

    for (uint64_t i = 0; i <= I_DO_MAX; i++) {

        std::shared_ptr<double> pv;

        try {

            std::cout << "----------------->> i = " << i << std::endl;

            pv.reset(new double[DIM1]);

            for (uint64_t j = 0; j < DIM1; j++) {
                pv.get()[j] = j;
            }
        }
        catch (std::exception &e) {
        }

        if (i == I_DO_MAX) {
            std::cout << "pv.get()[10] = " << pv.get()[10] << std::endl;
        }
    }


    std::cout << "give an input to exit: ";
    int32_t sentinel;
    std::cin >> sentinel;
}