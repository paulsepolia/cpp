// Not good code as destructor is not virtual

#include<iostream>

class Base {
public:
    Base() { std::cout << "Constructor: Base" << std::endl; }

    // ~Base() { std::cout << "Destructor : Base" << std::endl; }
    virtual ~Base() { std::cout << "Destructor : Base" << std::endl; }
};

class Derived : public Base {
public:
    Derived() { std::cout << "Constructor: Derived" << std::endl; }

    ~Derived() { std::cout << "Destructor : Derived" << std::endl; }
};

int main() {

    Base *Var(new Derived());
    delete Var;

    return 0;
}
