#include <iostream>

class Base {
protected:
    int i;
public:
    Base(int a) { i = a; }

    virtual void display() { std::cout << "I am Base class object, i = " << i << std::endl; }
};

class Derived : public Base {
    int j;
public:
    Derived(int a, int b) : Base(a) { j = b; }

    virtual void display() {
        std::cout << "I am Derived class object, i = "
             << i << ", j = " << j << std::endl;
    }
};

// Global method, Base class object is passed by value
void some_func(Base obj) {
    obj.display();
}

int main() {
    Base b(33);
    Derived d(45, 54);
    some_func(b);
    some_func(d);  // Object Slicing, the member j of d is sliced off
    return 0;
}