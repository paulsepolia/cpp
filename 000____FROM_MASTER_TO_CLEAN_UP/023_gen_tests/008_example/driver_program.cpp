#include<iostream>

class Base {
public:
    virtual void show() = 0;
};

class Derived : public Base {
public:
    void show() { std::cout << "In Derived " << std::endl; }
};

int main() {

    Derived d;
    Base &br(d);
    br.show();

    Base * bp(&d);
    bp->show();
    
    return 0;
}