#include <iostream>

class A {
public:

    void print_fun() {
        std::cout << " -->> A" << std::endl;
    }
};

class B: public virtual A {};

class C: public virtual A {};

class D: public B, public C {};


int main() {

    D d;
    d.print_fun();

    A a;
    a.print_fun();

    B b;
    b.print_fun();

    C c;
    c.print_fun();

    return 0;
}
