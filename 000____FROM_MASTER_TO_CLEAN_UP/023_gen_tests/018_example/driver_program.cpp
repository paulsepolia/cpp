#include <iostream>

class A
{
public:

    int k;

    static int i;

    static void print_fun() {
        std::cout << " --> static function here --> " << i++ << std::endl;
    }

    virtual void print_pure() = 0;

};

int A::i = 0;

class B: public A
{
public:

    void print_pure(){
        k = i;
        std::cout << " --> print pure function --> " << k << std::endl;
    }
};


int main() {

    std::cout << " --> 1" << std::endl;

    A::print_fun();

    B b;

    std::cout << " --> 2" << std::endl;

    b.print_fun();

    std::cout << " --> 3" << std::endl;

    b.print_pure();

    A * pA;

    pA = &b;

    std::cout << " --> 4" << std::endl;

    pA->print_fun();

    std::cout << " --> 5" << std::endl;

    pA->print_pure();

    return 0;
}