#!/bin/bash

  clang++   -O3            \
            -Wall          \
            -std=c++17     \
            -pthread       \
            -pedantic      \
            main.cpp       \
            -o x_clang

