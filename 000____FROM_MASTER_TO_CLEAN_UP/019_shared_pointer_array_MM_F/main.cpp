#include <iostream>
#include <memory>
#include <cstdint>

int main() {

    const uint64_t DIM_MAX = 1000;

    std::shared_ptr<double[]> sp_a(new double[DIM_MAX]);

    for (uint64_t i = 0; i < DIM_MAX; i++) {
        sp_a[i] = static_cast<double>(i);
    }

    for (uint64_t i = 0; i < DIM_MAX; i++) {
        std::cout << sp_a[i] << std::endl;
    }

}