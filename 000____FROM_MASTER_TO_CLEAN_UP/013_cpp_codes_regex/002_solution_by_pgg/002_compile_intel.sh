#!/bin/bash

  # compile

  icpc  -c -O3 driver_program.cpp

  # link

  icpc  driver_program.o                    \
        /usr/lib/x86_64-linux-gnu/libpcre.a \
        -static                             \
        -o x_intel

  # clean

  rm *.o
