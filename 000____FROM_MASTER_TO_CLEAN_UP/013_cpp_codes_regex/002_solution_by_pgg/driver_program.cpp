
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstdio>
#include <pcre.h>
#include <cstring>

using std::endl;
using std::cout;
using std::cin;
using std::ios;
using std::ifstream;
using std::exit;
using std::fopen;
using std::fclose;
using std::strlen;

// the main function

int main(int argc, char ** argv)
{
    // local variables and parameters

    int sentinel;
    FILE * inputFile = NULL; // initialize the pointer to the NULL value
    char * line = NULL;
    size_t lengthOfLine;
    size_t read;
    pcre * re;
    pcre_extra * pcreExtra;
    int pcreExecRet;
    const char * error;
    int erroffset;
    char ** aLineToMatch;
    int index;
    int rc;
    int subStrVec[30];
    const char *psubStrMatchStr;

    // if the number of input arguments are equal to two
    // then the inputFile stream is the stdin
    // else the inputFile stream is the argv[1]

    inputFile = stdin;

    // test if the inputFile stream is opened with success
    // if not then exit the program

    if (inputFile == NULL) {
        cout << "ERROR. The file did not open. Exit." << endl;
        exit(-1);
    }

    // compile the regular expression

    re = pcre_compile(argv[1],           // the pattern
                      0,                 // default options
                      &error,            // for error message
                      &erroffset,        // for error offset
                      NULL);             // use default character tables

    // test if the compilation is okay

    if (re == NULL) {
        cout << "ERROR. PCRE compilation failed at offset "
             << erroffset << " : " <<  error << endl;
        cout << "Exit." << endl;
        exit(-1);
    }

    // optimize the regex

    pcreExtra = pcre_study(re, 0, &error);

    // read each line of the input file

    while (getline(&line, &lengthOfLine, inputFile) != -1) {

        // set the new line

        aLineToMatch = &line;

        // try to find the regex in aLineToMatch, and report results

        pcreExecRet = pcre_exec(re,
                                pcreExtra,
                                *aLineToMatch,
                                strlen(*aLineToMatch),  // length of string
                                0,                      // start looking at this point
                                0,                      // options
                                subStrVec,
                                30);                    // length of subStrVec

        if(pcreExecRet < 0) {
            // something bad happened...

            switch(pcreExecRet) {
            case PCRE_ERROR_NOMATCH :
                cout << "String did not match the pattern" << endl;
                break;
            case PCRE_ERROR_NULL :
                cout << "Something was null" << endl;
                break;
            case PCRE_ERROR_BADOPTION :
                cout << "A bad option was passed" << endl;
                break;
            case PCRE_ERROR_BADMAGIC :
                cout << "Magic number bad (compiled re corrupt?" << endl;
                break;
            case PCRE_ERROR_UNKNOWN_NODE :
                cout << "Something kooky in the compiled re" << endl;
                break;
            case PCRE_ERROR_NOMEMORY :
                cout << "Ran out of memory" << endl;
                break;
            default :
                cout << "Unknown error" << endl;
                break;
            } // end switch
        } else {
            // a match case

            cout << "Result: We have a match!" << endl;

            // at this point, rc contains the number of substring matches found...

            if(pcreExecRet == 0) {
                cout << "But too many substrings were found to fit in subStrVec!" << endl;

                // set rc to the max number of substring matches possible

                pcreExecRet = 30 / 3;
            }

            // print the results

            for(int j = 0; j < pcreExecRet; j++) {
                pcre_get_substring(*aLineToMatch,
                                   subStrVec,
                                   pcreExecRet,
                                   j,
                                   &(psubStrMatchStr));

                cout << "Match("
                     << j
                     << "/"
                     << pcreExecRet-1
                     << "): ("
                     << subStrVec[j*2]
                     << ","
                     << subStrVec[j*2+1]
                     << "): '"
                     << psubStrMatchStr
                     << "'" << endl;

            }

            // free up the substring

            pcre_free_substring(psubStrMatchStr);
        }
    }


    // free up the regular expression

    pcre_free(re);

    // close the input stream

    fclose(inputFile);

    // test which line contains the regular expression

    return 0;
}

//======//
// FINI //
//======//

