#!/bin/bash

  # compile

  gcc -c driver_program.c

  # produce executable

  gcc  driver_program.o                     \
       /usr/lib/x86_64-linux-gnu/libpcre.so \
       -o x_gnu

  # clean

  rm driver_program.o

