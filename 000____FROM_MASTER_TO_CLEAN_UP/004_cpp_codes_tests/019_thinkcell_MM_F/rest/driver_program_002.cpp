
#include <iostream>
#include <string>
#include <iomanip>
#include <vector>
#include <algorithm>
#include <tuple>
#include <array>
#include <map>
#include <typeinfo>

using namespace std;

struct Base {}; // non-polymorphic
struct Derived : Base {};

struct Base2 {
    virtual void foo() {}
}; // polymorphic
struct Derived2 : Base2 {};

int main()
{
    int k1 = 50;
    int * k2 = &k1;

    long int lk1 = 50;
    long int * lk2 = &lk1;

    short int sk1 = 50;
    short int * sk2 = &sk1;

    double d1 = 50.0;
    double * d2 = &d1;

    long double ld1 = 50.0;
    long double * ld2 = &ld1;

    float fd1 = 50.0;
    float * fd2 = &fd1;

    Base base1;
    Base *base2;

    Derived dev1;
    Derived *dev2;

    Base2 base21;
    Base2 *base22;

    Derived2 dev21;
    Derived2 *dev22;

    cout << " --> typeid(k1).name()  = " << typeid(k1).name() << endl;
    cout << " --> typeid(k2).name()  = " << typeid(k2).name() << endl;

    cout << " --> typeid(lk1).name() = " << typeid(lk1).name() << endl;
    cout << " --> typeid(lk2).name() = " << typeid(lk2).name() << endl;

    cout << " --> typeid(sk1).name() = " << typeid(sk1).name() << endl;
    cout << " --> typeid(sk2).name() = " << typeid(sk2).name() << endl;

    cout << " --> typeid(d1).name()  = " << typeid(d1).name() << endl;
    cout << " --> typeid(d2).name()  = " << typeid(d2).name() << endl;

    cout << " --> typeid(ld1).name() = " << typeid(ld1).name() << endl;
    cout << " --> typeid(ld2).name() = " << typeid(ld2).name() << endl;

    cout << " --> typeid(fd1).name() = " << typeid(fd1).name() << endl;
    cout << " --> typeid(fd2).name() = " << typeid(fd2).name() << endl;

    cout << " --> typeid(base1).name() = " << typeid(base1).name() << endl;
    cout << " --> typeid(base2).name() = " << typeid(base2).name() << endl;

    cout << " --> typeid(dev1).name() = " << typeid(dev1).name() << endl;
    cout << " --> typeid(dev2).name() = " << typeid(dev2).name() << endl;

    cout << " --> typeid(base21).name() = " << typeid(base21).name() << endl;
    cout << " --> typeid(base22).name() = " << typeid(base22).name() << endl;

    cout << " --> typeid(dev21).name() = " << typeid(dev21).name() << endl;
    cout << " --> typeid(dev22).name() = " << typeid(dev22).name() << endl;

    //cout << " --> typeid(*dev21).name() = " << typeid(*dev21).name() << endl;
    //cout << " --> typeid(*dev22).name() = " << typeid(*dev22).name() << endl;

    cout << " --> typeid(&dev21).name() = " << typeid(&dev21).name() << endl;
    cout << " --> typeid(&dev22).name() = " << typeid(&dev22).name() << endl;

    //cout << " --> typeid(&&dev21).name() = " << typeid(&&dev21).name() << endl;
    //cout << " --> typeid(&&dev22).name() = " << typeid(&&dev22).name() << endl;

    cout << " --> typeid(Base).name()     = " << typeid(Base).name() << endl;
    cout << " --> typeid(Derived).name()  = " << typeid(Derived).name() << endl;
    cout << " --> typeid(Base2).name()    = " << typeid(Base2).name() << endl;
    cout << " --> typeid(Derived2).name() = " << typeid(Derived2).name() << endl;

    cout << " --> typdeid(cout).name() = " << typeid(cout).name() << endl;
    cout << " --> typdeid(&cout).name() = " << typeid(&cout).name() << endl;
    cout << " --> typdeid(int).name() = " << typeid(int).name() << endl;
    cout << " --> typdeid(double).name() = " << typeid(double).name() << endl;
    cout << " --> typdeid(const double).name() = " << typeid(const double).name() << endl;

    cout << " --> typdeid(int*).name() = " << typeid(int*).name() << endl;
    cout << " --> typdeid(double*).name() = " << typeid(double*).name() << endl;
    cout << " --> typdeid(const double*).name() = " << typeid(const double*).name() << endl;
    cout << " --> typdeid(double* const).name() = " << typeid(double* const).name() << endl;

    cout << " --> typdeid(string).name() = " << typeid(string).name() << endl;
    cout << " --> typdeid(string*).name() = " << typeid(string*).name() << endl;

    cout << " --> typdeid(vector<double>).name() = "
         << typeid(vector<double>).name() << endl;
    cout << " --> typdeid(map<int, int>).name() = "
         << typeid(map<int, int>).name() << endl;

    cout << " --> typdeid(vector<double>*).name() = "
         << typeid(vector<double>*).name() << endl;
    cout << " --> typdeid(map<int, int>*).name() = "
         << typeid(map<int, int>*).name() << endl;



    return 0;
}
