
#include <iostream>
#include <string>
#include <iomanip>
#include <vector>
#include <algorithm>
#include <tuple>
#include <array>
#include <map>
#include <typeinfo>

using namespace std;

struct A {
    double x;
};

const A* a = new A {0};

decltype( a->x ) x3;       // type of x3 is double (declared type)
decltype((a->x)) x4 = x3;  // type of x4 is const double& (lvalue expression)

template <class T, class U>
auto add(T t, U u) -> decltype(t + u); // return type depends on template parameters

template<class T, class U>
auto add(T t, U u) -> decltype(t+u)
{
    return static_cast<double>(t*u+1.1);
}

auto fL = [](double, double) -> double {
    return 10.0;
};

template <typename T>
T fA(T & a)
{
    return static_cast<T>(a);
}

template <>
double fA<double>(double & a)
{
    return -1.0;
}

int main()
{
    int i = 33;
    decltype(i) j = i*2;

    cout << "i = " << i << ", "
         << "j = " << j << endl;

    auto f = [](int a, int b) -> int {
        return a*b;
    };

    decltype(f) f2 = f; // the type of a lambda function is unique and unnamed

    cout << " --> typeid(f).name() = " << typeid(f).name() << endl;
    cout << " --> typeid(add<int,int>).name() = " << typeid(add<int,int>).name() << endl;
    cout << " --> typeid(add<int,double>).name() = " << typeid(add<int,double>).name() << endl;
    cout << " --> typeid(add<double,double>).name() = " << typeid(add<double,double>).name() << endl;
    cout << " --> typeid(fL).name() = " << typeid(fL).name() << endl;
    cout << " --> typeid((fL)).name() = " << typeid((fL)).name() << endl;

    i = f(2, 2);

    j = f2(3, 3);

    cout << "i = " << i << ", "
         << "j = " << j << endl;

    vector<int> v;
    decltype(v)::value_type i2 = 0; // int i = 0;

    cout << add<int, double>(1,11) << endl;

    double KK = 10;
    cout << fA(KK) << endl;

}

