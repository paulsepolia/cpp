#!/bin/bash

  # 1. compile

  icpc  -c              \
	   -O3             \
        -Wall           \
        -std=c++11      \
        driver_program.cpp

  # 2. link

  icpc  driver_program.o  \
        -static           \
        -o x_intel

  # 3. clean objects

  rm  driver_program.o

  # 4. exit

