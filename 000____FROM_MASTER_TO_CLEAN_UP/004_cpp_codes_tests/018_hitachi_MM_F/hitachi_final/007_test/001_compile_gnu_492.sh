#!/bin/bash

  # 1. compile

  g++-4.9.2 -c                  \
		  -O3                 \
            -Wall               \
            -std=gnu++14        \
            driver_program.cpp

  # 2. link

  g++-4.9.2  driver_program.o                  \
		   -static                           \
             -o x_gnu_492

  # 3. clean objects

  rm  driver_program.o

  # 4. exit

