
#include <iostream>    // cout
#include <string>
#include <iomanip>
#include <vector>
#include <algorithm>
#include <climits>

using namespace std;

int f(int n, int l, int r)
{
    //return (n<<l)>>r;
    return n*pow(2.0,l-r);
}

// Purpose:

// The purpose of the code is to return the result of
// "n * power(2,l-r)"
// so multiplies "n" by the power of "2" to the integer "(l-r)".

// Problems:

// If and of the "n", "l", "r" is negative then
// the result is wrong

// Note:
// No book used. I run the code many times and I observed the results
// Time taken: 1 hour

int main()
{
    cout << boolalpha;

    for (int n = 0; n != 10; ++n) {
        for (int l = 0; l != 4; ++l) {
            for (int r = -2; r != 8; ++r) {
                cout << " n = " << n << endl;
                cout << " l = " << l << endl;
                cout << " r = " << r << endl;
                cout << " " << f(n,l,r) << endl;
            }
        }

    }

    return 0;
}

// FINI
