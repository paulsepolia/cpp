
#include <iostream>    // cout
#include <string>
#include <iomanip>
#include <vector>
#include <algorithm>
#include <climits>

using namespace std;

void fn(long * p1, long *p2)
{
    register int x = *p1;
    register int y = *p2;
    x ^= y;
    y ^= x;
    x ^= y;
    *p1 = x;
    *p2 = y;
}

// Purpose:

// The addresses pointed by "p1" and "p2" hold some integer values.
// The purpose of the program is to swap the above integer values.
// So, e.g. if "*p1" equals "10" and "*p2" equals "11",
// after the execution of the function "fn"
// the values are the following:
// "*p1" equals "11" and "*p2" equals "10".
// "p1" address and "p2" "address" remains the same.

// Problems:

// There is a suggestion by the programmer to the compiler,
// the "x" and "y" variables to be stored in register and not in RAM ("register" keyword).
// Maybe the compiler decides to put the "x" and "y"
// in the register for fast access maybe not.
// The compiler will decide.

// Notes:

// No books used. I run the code several times.
// Time taken: 10 minutes

int main()
{
    cout << boolalpha;

    /*
    	for (int x = 0; x != 5; x++)
    	{
    		for (int y = 0; y != 8; y++)
    		{
    			int k = x^y;
    			cout << " x = " << x << endl;
    			cout << " y = " << y << endl;
    			cout << k << endl;
    		}
    	}
    */

    long * p1 = new long ;
    long * p2 = new long;

    for (long x = 0; x != 5; x++) {
        for (long y = 0; y != 8; y++) {
            *p1 = x;
            *p2 = y;
            cout << " x = " << x << endl;
            cout << " y = " << y << endl;
            fn(p1, p2);
            cout << " *p1 = " << *p1 << endl;
            cout << " *p2 = " << *p2 << endl;
        }
    }


    *p1 = 12;
    *p2 = -1;
    cout << " *p1 = " << *p1 << endl;
    cout << " *p2 = " << *p2 << endl;
    cout << "  p1 = " <<  p1 << endl;
    cout << "  p2 = " <<  p2 << endl;

    fn(p1, p2);

    cout << " *p1 = " << *p1 << endl;
    cout << " *p2 = " << *p2 << endl;
    cout << "  p1 = " <<  p1 << endl;
    cout << "  p2 = " <<  p2 << endl;

    p1 = p2;

    cout << " *p1 = " << *p1 << endl;
    cout << " *p2 = " << *p2 << endl;

    cout << " p1 = " << p1 << endl;
    cout << " p2 = " << p2 << endl;

    return 0;
}

// FINI
