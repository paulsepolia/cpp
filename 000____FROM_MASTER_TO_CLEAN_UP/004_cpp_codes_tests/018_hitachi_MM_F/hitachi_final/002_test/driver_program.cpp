
#include <iostream>    // cout
#include <string>
#include <iomanip>
#include <vector>
#include <algorithm>

using namespace std;

// Purpose:

// The purpose of the program is to build in stack,
// the zero-indexed array "v", so as
// "v[0]" equals "0", "v[1]" equals "1", "v[2]" equals "2", ... "v[9]" equals "9".
// It uses a while loop to do that.
// When the "i" index reaches the value "10"
// the next loop execution never occurs, since the "(i<10)" is "false".

// Problems:

// The index is being updated in the expression "v[i] = i++;"
// The behavior of the above expression is undefined.
// Explenation: the final value of "i" is ambiguous,
// depending on the order of expression evaluation,
// the increment "i++" may occur before,
// after, or interleaved with the assignment.
// Further:
// If the increment "i++" occures before the assignment
// and the current value of "i" is "0",
// then the "v[0]" is not initialized.
// Also, if the increment "i++" occurs before the assignment,
// and the current value of "i" is "9",
// then the "v[10]" is being set equal to "10",
// but the zero-indexed array "v" is declared so as its index to
// run from 0,1,2,..9 so setting the "v[10]",
// access RAM space that is not reserved by the system for the array "v".

// Notes:

// I did not use any book.
// Time taken: 20 minutes

int main()
{
    cout << boolalpha;

    unsigned int v[10];
    unsigned int i = 0;
    int j=0;
    int k;

    while (i < 10) {
//		k = i++;
        v[i] = i++;
        i;
//		j++;
    }

    k = i++;
    cout << v[0] << endl;
    cout << v[1] << endl;
    cout << v[2] << endl;
    cout << v[3] << endl;
    cout << v[4] << endl;
    cout << v[5] << endl;
    cout << v[6] << endl;
    cout << v[7] << endl;
    cout << v[8] << endl;
    cout << v[9] << endl;
    printf ("floats: %4.2f %+.0e %E \n", 3.1416, 3.1416, 3.1416);
    return 0;
}

// FINI
