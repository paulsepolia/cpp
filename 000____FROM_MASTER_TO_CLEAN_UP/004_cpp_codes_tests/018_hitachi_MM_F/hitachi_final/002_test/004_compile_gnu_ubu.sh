#!/bin/bash

  # 1. compile

  g++ -c                \
      -O3               \
      -Wall             \
      -std=c++0x        \
      driver_program.cpp

  # 2. link

  g++  driver_program.o   \
       -static            \
       -o x_gnu_ubu

  # 3. clean objects

  rm  driver_program.o

  # 4. exit

