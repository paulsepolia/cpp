
#include<iostream>

using std::endl;
using std::cout;

//===========//
// Fibonacci //
//===========//

// recursive version

constexpr long long int Fibonacci_R(long long int n)
{
    return n <= 1L ? 1L : Fibonacci_R(n-1) + Fibonacci_R(n-2);
}

// iterative version

long long int Fibonacci_I(long long int n)
{
    long long int fib[] = {0,1,1};
    for(long long int i=2; i<=n; i++) {
        fib[i%3] = fib[(i-1)%3] + fib[(i-2)%3];
        cout << "fib(" << i << ") = " << fib[i%3] << endl;
    }

    return fib[n%3];
}

// the main function

int main()
{
    constexpr long long int res = Fibonacci_R(33);
    const int a = 44;

    cout << " --> res = " << res << endl;

    // calculate the fib(i) from scratch for each i <= a using your recursive function

    cout << endl << "From recursive function" << endl;
    for(int i = 1; i <= a; ++i) {
        cout << "fib(" << i << ") = " << Fibonacci_R(i) << endl;
    }
    cout << endl;

    // or calculate fib(a) once and output the intermediate results from the looping version

    cout << "From iterative function" << endl;

    const int a1 = 66;
    Fibonacci_I(a1);

    cout << endl;

    return 0;
}

// END
