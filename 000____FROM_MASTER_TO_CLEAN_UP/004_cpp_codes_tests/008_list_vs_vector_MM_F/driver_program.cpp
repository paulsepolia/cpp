
#include <iostream>
#include <vector>
#include <list>
#include <ctime>
#include <cmath>
#include <algorithm>
#include <iomanip>

using std::cout;
using std::endl;
using std::cin;
using std::vector;
using std::list;
using std::clock;
using std::cos;
using std::sort;
using std::setprecision;
using std::fixed;

int main()
{
    const int DIM = 100000000;
    const int K_MAX = 100000;

    for (int k = 0; k != K_MAX; ++k) {

        cout << "------------------------------------------------>> " << K_MAX << endl;

        vector<double> * vec = new vector<double>[1];
        list<double> * ls = new list<double>[1];
        clock_t t1;
        clock_t t2;

        cout << fixed;
        cout << setprecision(10);

        // build vector

        cout << " --> build the vector" << endl;

        for (int i = 0; i != DIM; ++i) {
            vec[0].push_back(cos(static_cast<double>(i)));
        }

        // iterate over the vector

        cout << " --> iterate over the vector" << endl;

        t1 = clock();

        for (int i = 0; i != vec->size(); ++i) {
            double xxx = vec[0][i];
        }

        t2 = clock();

        cout << " --> time used to iterate over the vector = "
             << (t2-static_cast<double>(t1))/CLOCKS_PER_SEC << endl;

        // sort vector

        cout << " --> sort the vector" << endl;

        t1 = clock();

        sort(vec[0].begin(), vec[0].end());

        t2 = clock();

        cout << " --> time used to sort the vector = "
             << (t2-static_cast<double>(t1))/CLOCKS_PER_SEC << endl;

        // delete vector

        cout << " --> delete vector" << endl;

        delete [] vec;

        // build_list

        cout << " --> build the list" << endl;

        for (int i = 0; i != DIM; ++i) {
            ls[0].push_back(cos(static_cast<double>(i)));
        }

        // sort list

        cout << " --> sort the list" << endl;

        t1 = clock();

        ls[0].sort();

        t2 = clock();

        cout << " --> time used to sort the list = "
             << (t2-static_cast<double>(t1))/CLOCKS_PER_SEC << endl;

        // iterate over the list

        cout << " --> iterate over the list" << endl;

        t1 = clock();

        list<double>::iterator it;

        for (it = ls[0].begin(); it != ls[0].end(); ++it) {
            double xxx = *it;
        }

        t2 = clock();

        cout << " --> time used to iterate over the list = "
             << (t2-static_cast<double>(t1))/CLOCKS_PER_SEC << endl;

        // delete list

        cout << " --> delete list" << endl;

        delete [] ls;
    }

    // return

    int sentinel;
    cin >> sentinel;

    return 0;
}
