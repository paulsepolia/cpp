
#include <iostream>
#include <vector>
#include <list>
#include <ctime>
#include <cmath>
#include <algorithm>
#include <iomanip>

using std::cout;
using std::endl;
using std::cin;
using std::vector;
using std::list;
using std::clock;
using std::cos;
using std::sort;
using std::setprecision;
using std::fixed;
using std::copy;

class BIG_DATA {

public:
    // constructor # 1

    BIG_DATA() : dim(0), dat(0)
    {
        cout << " --> inside --> DEFAULT CONSTRUCTOR" << endl;
    }

    // constructor # 2

    explicit BIG_DATA(int len) : dim(len)
    {

        cout << " --> inside --> CONSTRUCTOR WITH ONE ARGUMENT" << endl;
        dat = new double [dim];
        for (int i = 0; i != dim; ++i) {
            dat[i] = static_cast<double>(i);
        }
    }

    // destructor

    ~BIG_DATA()
    {
        cout << " --> inside --> DESTRUCTOR --> 1" << endl;
        if (dat != 0) {
            cout << " --> inside --> DESTRUCTOR --> 2" << endl;
            delete [] dat;
        }
    }

    // copy constructor

    BIG_DATA(const BIG_DATA & other)
        : dim(other.dim)
        , dat(new double[other.dim])
    {

        cout << " --> inside --> COPY CONSTRUCTOR --> 1" << endl;
        copy(other.dat, other.dat + dim, this->dat);
        cout << " --> inside --> COPY CONSTRUCTOR --> 2" << endl;
    }

    // operator <

    bool operator <(const BIG_DATA & elem) const
    {
        cout << " --> inside --> operator <" << endl;
        return (this->dim < elem.dim);
    }

    // operator =

    BIG_DATA & operator =(const BIG_DATA & elem)
    {

        cout << " --> inside --> operator = --> 1" << endl;

        for (int i = 0; i < this->dim; ++i) {

            this->dat[i] = elem.dat[i];
        }

        cout << " --> inside --> operator = --> 2" << endl;

        return *this;
    }

    // deallocate

    void deallocate()
    {

        cout << " --> inside --> destructor --> 1" << endl;

        if (this->dat != 0) {
            cout << " --> inside --> destructor --> 2" << endl;
            delete [] this->dat;
            cout << " --> inside --> destructor --> 3" << endl;
        }
        dat = 0;


    }

    // move constructor

    BIG_DATA(BIG_DATA && other)
        : dim(0),
          dat(0)
    {

        cout << " --> inside --> move constructor" << endl;

        // copy values

        dat = other.dat;
        dim = other.dim;

        // release the data pointer from the source object
        // so that the destructor does not free the memory multiple times

        other.dat = 0;
        other.dim = 0;
    }

    // move assignment operator

    BIG_DATA& operator=(BIG_DATA && other)
    {

        cout << " --> inside move operator = --> 1" << endl;

        if (this != &other) {
            // free the existing resource

            cout << " --> inside move operator = --> 2" << endl;
            delete[] dat;
            cout << " --> inside move operator = --> 3" << endl;

            // Copy the data pointer and its length from the
            // source object

            this->dat = other.dat;
            this->dim = other.dim;

            // Release the data pointer from the source object so that
            // the destructor does not free the memory multiple times

            other.dat = 0;
            other.dim = 0;
        }
        return *this;
    }


private:

    int dim;
    double * dat;

};

int main()
{
    const int DIM = 10;
    const int K_MAX = 2;

    for (int k = 0; k != K_MAX; ++k) {

        cout << "--------------------------------------------------------->> " << K_MAX << endl;

        vector<BIG_DATA> * vec = new vector<BIG_DATA>[1];
        list<BIG_DATA> * ls = new list<BIG_DATA>[1];
        clock_t t1;
        clock_t t2;

        cout << fixed;
        cout << setprecision(10);

        // build vector

        cout << " ------------------------------------------------>> build the vector" << endl;

        for (int i = 0; i != DIM; ++i) {
            vec[0].push_back(BIG_DATA(i+1));
        }

        // iterate over the vector

        cout << " ------------------------------------------------>> iterate over the vector" << endl;

        t1 = clock();

        for (unsigned int i = 0; i != vec->size(); ++i) {
            BIG_DATA xxx = vec[0][i];
        }

        t2 = clock();

        cout << " --> time used to iterate over the vector = "
             << (t2-static_cast<double>(t1))/CLOCKS_PER_SEC << endl;

        // sort vector

        cout << " ------------------------------------------------>> sort the vector" << endl;

        t1 = clock();

        sort(vec[0].begin(), vec[0].end());

        t2 = clock();

        cout << " --> time used to sort the vector = "
             << (t2-static_cast<double>(t1))/CLOCKS_PER_SEC << endl;

        // delete vector

        cout << " ------------------------------------------------>> delete vector" << endl;

        delete [] vec;

        // build_list

        cout << " ------------------------------------------------>> build the list" << endl;

        for (int i = 0; i != DIM; ++i) {
            ls[0].push_back(BIG_DATA(i+1));
        }

        // iterate over the list

        cout << " ------------------------------------------------>> iterate over the list" << endl;

        t1 = clock();

        list<BIG_DATA>::iterator it;

        for (it = ls[0].begin(); it != ls[0].end(); ++it) {
            BIG_DATA xxx = *it;
        }

        t2 = clock();

        cout << " --> time used to iterate over the list = "
             << (t2-static_cast<double>(t1))/CLOCKS_PER_SEC << endl;

        // sort list

        cout << " ------------------------------------------------>> sort the list" << endl;

        t1 = clock();

        ls[0].sort();

        t2 = clock();

        cout << " --> time used to sort the list = "
             << (t2-static_cast<double>(t1))/CLOCKS_PER_SEC << endl;

        // delete list

        cout << " ------------------------------------------------>> delete list" << endl;

        delete [] ls;
    }

    // return

//     int sentinel;
//     cin >> sentinel;

    return 0;
}
