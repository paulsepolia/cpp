//===================================//
// driver program to queueThreadSafe //
//===================================//

#include "queueThreadSafe.h"
#include <iostream>
#include <thread>
#include <iomanip>

using std::endl;
using std::cout;
using std::thread;
using std::boolalpha;

// global variable

queueThreadSafe<int> q;

// function doWork

void doWork(const int & DIM)
{
    // push elements

    cout << " --> 1 --> push elements" << endl;

    for (int i = 0; i != DIM; i++) {
        q.push(i);
    }

    cout << " --> 2 --> size of queue = " << q.size() << endl;

    // emplace elements

    cout << " --> 3 --> emplace elements" << endl;

    for (int i = 0; i != DIM; i++) {
        q.emplace(i);
    }

    cout << " --> 4 --> size of queue = " << q.size() << endl;

    // back

    cout << " --> 5 --> q.back() = " << q.back() << endl;

    // front

    cout << " --> 6 --> q.front() = " << q.front() << endl;

    // pop elements

    cout << " --> 7 --> pop elements" << endl;

    unsigned int SIZE_LOC = q.size();

    for (unsigned int i = 0; i != SIZE_LOC; i++) {
        q.pop(); // edw perimenei an einai adeio mexri na parei kainourio
    }

    // push elements again

    cout << " --> 8 --> push elements" << endl;

    for (int i = 0; i != DIM; i++) {
        q.push(i);
    }

    cout << " --> 9 --> size of queue = " << q.size() << endl;

}

// the main function

int main()
{
    // local parameters

    const int DIM = 4;
    const int NT = 10;

    // adjust the output field

    cout << boolalpha;

    // threads array

    thread * th = new thread [NT];

    // spawn threads

    for (int i = 0; i != NT; i++) {
        th[i]= thread(doWork, DIM);
    }

    // join threads

    for (int i = 0; i != NT; i++) {
        th[i].join();
    }

    // q1

    queueThreadSafe<int> q1;

    for (int i = 0; i != DIM; i++) {
        q1.push(i);
    }

    // q2

    queueThreadSafe<int> q2(q1);

    cout << " --> q1.size() = " << q1.size() << endl;
    cout << " --> q2.size() = " << q2.size() << endl;

    // set q2 equal to q1

    q2 = q1;

    cout << " --> q1.size() = " << q1.size() << endl;
    cout << " --> q2.size() = " << q2.size() << endl;

    // test if they are equal;

    cout << " --> (q1 == q2) = " << (q1 == q2) << endl;

    // delete threads array

    delete [] th;

    return 0;
}

// end
