//============================//
// driver program to queue_tf //
//============================//

#include "queue_ts.h"
#include <iostream>
#include <thread>

using std::endl;
using std::cout;
using std::thread;

// do_work function

void do_work(const int & DIM, queue_ts<int> & q)
{
    for (int i = 0; i != DIM; i++) {
        q.push(i);
        cout << " --> LL --> " << q.size() << endl;
    }
}

// the main function

int main()
{
    // local parameters

    const int DIM = 4;

    // local variables

    queue_ts<int> q1;

    thread th1(do_work, DIM, q1);
    thread th2(do_work, DIM, q1);

    th1.join();
    th2.join();

    return 0;
}

// end
