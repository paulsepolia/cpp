//===========================//
// queue_ts class definition //
//===========================//

#ifndef QUEUE_TS_DEF_H
#define QUEUE_TS_DEF_H

#include "queue_ts_dec.h"

#include <queue>
using std::queue;

// push --> 1

template <class T>
void queue_ts<T>::push(const T & val)
{
    mtx.lock();

    this->queue<T>::push(val);

    mtx.unlock();
}

// push --> 2

template <class T>
void queue_ts<T>::push(T & val)
{
    mtx.lock();

    this->queue<T>::push(val);

    mtx.unlock();
}

#endif // QUEUE_TS_DEF_H

// end

