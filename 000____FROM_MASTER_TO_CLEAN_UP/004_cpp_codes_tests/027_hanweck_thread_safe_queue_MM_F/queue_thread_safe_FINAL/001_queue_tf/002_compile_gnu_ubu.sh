#!/bin/bash

  # 1. compile

  g++   -O3                \
        -Wall              \
        -std=c++0x         \
	   -pthread           \
        driver_program.cpp \
        -pthread           \
        -o x_gnu_ubu
