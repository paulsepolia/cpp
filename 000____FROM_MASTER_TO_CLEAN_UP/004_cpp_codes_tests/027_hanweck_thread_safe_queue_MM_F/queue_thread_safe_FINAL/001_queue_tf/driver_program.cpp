//============================//
// driver program to queue_tf //
//============================//

#include "queue_ts.h"
#include <iostream>

using std::endl;
using std::cout;

// the main function

int main()
{
    // local parameters

    const int DIM = 100;

    // local variables

    queue_ts<double> q1;

    for (int i = 0; i != DIM; i++) {
        q1.push(i);
    }

    cout << " --> q1.front() = " << q1.front()  << endl;
    cout << " --> q1.back() = " << q1.back()  << endl;

    for (int i = 0; i != DIM; i++) {
        q1.pop();
    }

    cout << " --> q1.size() = " << q1.size() << endl;

    return 0;
}

// end
