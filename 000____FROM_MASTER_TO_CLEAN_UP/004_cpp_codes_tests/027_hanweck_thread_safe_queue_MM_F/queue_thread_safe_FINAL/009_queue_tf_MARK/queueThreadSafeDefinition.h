//============================//
// queueThreadSafe definition //
//============================//

#ifndef QUEUE_THREAD_SAFE_DEF_H
#define QUEUE_THREAD_SAFE_DEF_H

#include "queueThreadSafe.h"

#include <iostream>
#include <queue>
#include <mutex>

using std::queue;
using std::mutex;
using std::cout;
using std::endl;
using std::unique_lock;

// 1 --> constructor

template <class T>
queueThreadSafe<T>::queueThreadSafe() {};

// 2 --> push

template <class T>
void queueThreadSafe<T>::push(const T & val)
{
    unique_lock<mutex> lck(this->mtx);
    this->queue<T>::push(val);
    lck.unlock();
    cv.notify_one();
}

// 3 -> copy constructor

template <class T>
queueThreadSafe<T>::queueThreadSafe(const queueThreadSafe & q)
{
    for (unsigned int i = 0; i != this->size(); i++) {
        this->pop();
    }

    queue<T> qTmp(q);

    for (unsigned int i = 0; i != q.size(); i++) {
        this->push(qTmp.front());
        qTmp.pop();
    }
}

// 4 --> pop

template <class T>
void queueThreadSafe<T>::pop()
{
    unique_lock<mutex> lck(this->mtx);

    while(this->queue<T>::empty()) {
        this->cv.wait(lck);
    }

    this->queue<T>::pop();
}

// 5 --> back

template <class T>
T  queueThreadSafe<T>::back()
{
    unique_lock<mutex> lck(this->mtx);
    T val = this->queue<T>::back();
    return val;
}

// 6 --> front

template <class T>
T  queueThreadSafe<T>::front()
{
    unique_lock<mutex> lck(this->mtx);

    while(this->queue<T>::empty()) {
        this->cv.wait(lck);
    }

    T val = this->queue<T>::front();
    return val;
}

// 7 --> emplace

template <class T>
template <class... Args>
void queueThreadSafe<T>::emplace(Args && ... args)
{
    unique_lock<mutex> lck(this->mtx);
    this->queue<T>::emplace(args ...);
}

// 8 --> empty

template <class T>
bool queueThreadSafe<T>::empty() const
{
    unique_lock<mutex> lck(this->mtx);
    bool val = this->queue<T>::empty();
    return val;
}

// 9 --> size

template <class T>
unsigned int queueThreadSafe<T>::size() const
{
    unique_lock<mutex> lck(this->mtx);
    unsigned int val = this->queue<T>::size();
    return val;
}

// 10 --> swap

template <class T>
void queueThreadSafe<T>::swap(queueThreadSafe & other) noexcept
{
    unique_lock<mutex> lck(this->mtx);
    this->queue<T>::swap(other);
}

#endif // QUEUE_THREAD_SAFE_DEF_H

// end
