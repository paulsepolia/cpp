//============================//
// queue_ts class declaration //
//============================//

#ifndef QUEUE_TS_DEC_H
#define QUEUE_TS_DEC_H

#include <queue>

using std::queue;

// class queue_ts

template <class T>
class queue_ts : public queue<T> {

public:

    // construnctor

    queue_ts();

    // copy construnctor

    queue_ts(const queue<T> &);

    // member functions

    void push(const T &);
    void push(T &);
    void pop();
    T back();
    T front();
    bool empty() const;
    unsigned int size() const;
    void swap(queue_ts &) noexcept;

    template <class ... Args>
    void emplace (Args && ... args);


};

#endif // QUEUE_TS_DEC_H

// end
