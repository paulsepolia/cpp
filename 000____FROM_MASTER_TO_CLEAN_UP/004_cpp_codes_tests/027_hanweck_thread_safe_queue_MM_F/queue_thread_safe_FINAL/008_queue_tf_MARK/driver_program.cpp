//===================================//
// driver program to queueThreadSafe //
//===================================//

#include "queueThreadSafe.h"
#include <iostream>
#include <thread>

using std::endl;
using std::cout;
using std::thread;

// doWorkA function

void doWorkA(const int & DIM, queueThreadSafe<int> q)
{
    // push elements

    for (int i = 0; i != DIM; i++) {
        q.push(i);
    }

    // emplace elements

    for (int i = 0; i != DIM; i++) {
        q.emplace(i);
    }

    // back

    cout << " --> W2 --> q.back() = " << q.back() << endl;

    // front

    cout << " --> W3 --> q.front() = " << q.front() << endl;

    // pop elements

    for (unsigned int i = 0; i != q.size(); i++) {
        q.pop();
        cout << " --> W4 --> popping ... --> q.size() = " << q.size() << endl;
    }

    // swap

    queueThreadSafe<double> q1;
    queueThreadSafe<double> q2;

    for (int i = 0; i != DIM; i++) {
        q1.push(i);
        q2.push(DIM-i);
    }

    cout << " --> BEFORE" << endl;

    cout << " --> q1.front() = " << q1.front() << endl;
    cout << " --> q2.front() = " << q2.front() << endl;
    cout << " --> q1.back() = " << q1.back() << endl;
    cout << " --> q2.back() = " << q2.back() << endl;

    q1.swap(q2);

    cout << " --> AFTER" << endl;

    cout << " --> q1.front() = " << q1.front() << endl;
    cout << " --> q2.front() = " << q2.front() << endl;
    cout << " --> q1.back() = " << q1.back() << endl;
    cout << " --> q2.back() = " << q2.back() << endl;
}

// the main function

int main()
{
    // local parameters

    const int DIM = 4;

    // local variables

    queueThreadSafe<int> q1;

    // spawn threads

    thread th1(doWorkA, DIM, q1);
    thread th2(doWorkA, DIM, q1);
    thread th3(doWorkA, DIM, q1);
    thread th4(doWorkA, DIM, q1);

    // join threads

    th1.join();
    th2.join();
    th3.join();
    th4.join();

    return 0;
}

// end
