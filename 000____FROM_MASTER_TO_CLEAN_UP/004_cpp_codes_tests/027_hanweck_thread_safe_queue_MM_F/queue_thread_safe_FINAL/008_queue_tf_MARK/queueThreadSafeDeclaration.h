//=============================//
// queueThreadSafe declaration //
//=============================//

#ifndef QUEUE_THREAD_SAFE_DECL_H
#define QUEUE_THREAD_SAFE_DECL_H

#include <queue>
#include <mutex>
#include <condition_variable>

using std::queue;
using std::mutex;
using std::condition_variable;

// class queueThreadSafe

template <class T>
class queueThreadSafe : private queue<T> {

public:

    // construnctor

    queueThreadSafe();

    // copy construnctor

    queueThreadSafe(const queueThreadSafe &);

    // member functions

    void push(const T &);
    void push(T &);
    void pop();
    T back();
    T front();
    bool empty() const;
    unsigned int size() const;
    void swap(queueThreadSafe &) noexcept;

    template <class ... Args>
    void emplace(Args && ... args);

private:

    mutable mutex mtx;
    condition_variable cv;
};

#endif // QUEUE_THREAD_SAFE_DECL_H

// end
