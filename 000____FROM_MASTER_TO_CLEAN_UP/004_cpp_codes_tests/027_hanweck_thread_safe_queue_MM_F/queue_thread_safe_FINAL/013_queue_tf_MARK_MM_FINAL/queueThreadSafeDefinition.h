//============================//
// queueThreadSafe definition //
//============================//

#ifndef QUEUE_THREAD_SAFE_DEF_H
#define QUEUE_THREAD_SAFE_DEF_H

#include "queueThreadSafeDeclaration.h"

#include <iostream>
#include <queue>
#include <mutex>

using std::queue;
using std::mutex;
using std::cout;
using std::endl;
using std::unique_lock;

// 1 --> constructor

template <class T>
queueThreadSafe<T>::queueThreadSafe() {};

// 2 --> push

template <class T>
void queueThreadSafe<T>::push(const T & val)
{
    unique_lock<mutex> lck(m_mtx);
    m_queue.push(val);
    lck.unlock();       // GIATI AUTO KAI TI KANEI?
    m_cv.notify_one();
}

// 3 --> copy constructor

template <class T>
queueThreadSafe<T>::queueThreadSafe(const queueThreadSafe & q)
{
    // NA GINETAI EXECUTION MONON APO TON MASTER THREAD

    unsigned int d_loc = m_queue.size();

    for (unsigned int i = 0; i != d_loc; i++) {
        m_queue.pop();
    }

    queue<T> qTmp(q); // PROBLEM HERE?

    d_loc = q.size();

    for (unsigned int i = 0; i != d_loc; i++) {
        m_queue.push(qTmp.front());
        qTmp.pop();
    }
}

// 4 --> pop

template <class T>
void queueThreadSafe<T>::pop()
{
    unique_lock<mutex> lck(m_mtx);

    while(m_queue.empty()) {
        m_cv.wait(lck);
    }

    m_queue.pop();
    lck.unlock(); // TI KANEI AYTO KAI GIATI?
    m_cv.notify_one();
}

// 5 --> back

template <class T>
T  queueThreadSafe<T>::back() // EINAI H' OXI THREAD SAFE?
{
    unique_lock<mutex> lck(m_mtx);
    T val = m_queue.back();

    return val;
}

// 6 --> front

template <class T>
T  queueThreadSafe<T>::front()
{
    unique_lock<mutex> lck(m_mtx);

    while(m_queue.empty()) {
        m_cv.wait(lck);
    }

    T val = m_queue.front();
    lck.unlock();
    m_cv.notify_one(); // TI KANEI AUTO?

    return val;
}

// 7 --> emplace

template <class T>
template <class... Args>
void queueThreadSafe<T>::emplace(Args && ... args)
{
    unique_lock<mutex> lck(m_mtx);
    m_queue.emplace(args ...);
    lck.unlock();
    m_cv.notify_one(); // GIATI AYTO?
}

// 8 --> empty

template <class T>
bool queueThreadSafe<T>::empty() const // EINAI H' OXI THREAD SAFE?
{
    unique_lock<mutex> lck(m_mtx);
    bool val = m_queue.empty();

    return val;
}

// 9 --> size

template <class T>
unsigned int queueThreadSafe<T>::size() const // EINAI H' OXI THREAD SAFE?
{
    unique_lock<mutex> lck(m_mtx);
    unsigned int val = m_queue.size();

    return val;
}

// 10 --> swap

template <class T>
void queueThreadSafe<T>::swap(queueThreadSafe & other) noexcept
{
    unique_lock<mutex> lck(m_mtx);
    m_queue.swap(other); // PROBLEM HERE?
}

// 11 --> operator =

template <class T>
queueThreadSafe<T> & queueThreadSafe<T>::operator = (const queueThreadSafe<T> & other)
{
    this->m_queue = other.m_queue;

    return *this;
}

// 12 --> operator ==

template<typename T>
bool queueThreadSafe<T>::operator == (const queueThreadSafe<T> & other) const
{
    bool bval = (this->m_queue == other.m_queue);

    return bval;
}

#endif // QUEUE_THREAD_SAFE_DEF_H

// end
