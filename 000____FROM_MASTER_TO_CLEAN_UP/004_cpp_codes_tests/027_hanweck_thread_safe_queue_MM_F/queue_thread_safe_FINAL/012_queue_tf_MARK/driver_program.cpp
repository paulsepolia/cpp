//===================================//
// driver program to queueThreadSafe //
//===================================//

#include "queueThreadSafe.h"
#include <iostream>
#include <thread>

using std::endl;
using std::cout;
using std::thread;

// function: doWork

void doWork(const int & DIM, queueThreadSafe<int> q)
{
    // push elements

    cout << " --> 1 --> push elements" << endl;

    for (int i = 0; i != DIM; i++) {
        q.push(i);
    }

    cout << " --> 2 --> size of queue = " << q.size() << endl;

    // emplace elements

    cout << " --> 3 --> emplace elements" << endl;

    for (int i = 0; i != DIM; i++) {
        q.emplace(i);
    }

    cout << " --> 4 --> size of queue = " << q.size() << endl;

    // back

    cout << " --> 5 --> q.back() = " << q.back() << endl;

    // front

    cout << " --> 6 --> q.front() = " << q.front() << endl;

    // pop elements

    cout << " --> 7 --> pop elements" << endl;

    unsigned int SIZE_LOC = q.size();

    for (unsigned int i = 0; i != SIZE_LOC; i++) {
        q.pop();
    }

    cout << " --> 8 --> size of queue = " << q.size() << endl;
}

// the main function

int main()
{
    // local parameters

    const int DIM = 4;
    const int NT = 10;

    // local variables

    queueThreadSafe<int> q1;

    // threads array

    thread * th = new thread [NT];

    // spawn threads

    for (int i = 0; i != NT; i++) {
        th[i]= thread(doWork, DIM, q1);
    }

    // join threads

    for (int i = 0; i != NT; i++) {
        th[i].join();
    }


    queueThreadSafe<int> q2;
    queueThreadSafe<int> q3(q2);



    // delete threads array

    delete [] th;

    return 0;
}

// end
