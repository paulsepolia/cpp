//===================================//
// driver program to queueThreadSafe //
//===================================//

#include "queueThreadSafe.h"
#include <iostream>
#include <thread>
#include <mutex>

using std::endl;
using std::cout;
using std::thread;
using std::mutex;

// global variable

queueThreadSafe<int> q;

// function doWork

void doWork(const int & DIM)
{
    // push elements

    cout << " --> 1 --> push elements" << endl;

    for (int i = 0; i != DIM; i++) {
        q.push(i);
    }

    cout << " --> 2 --> size of queue = " << q.size() << endl;

    // emplace elements

    cout << " --> 3 --> emplace elements" << endl;

    for (int i = 0; i != DIM; i++) {
        q.emplace(i);
    }

    cout << " --> 4 --> size of queue = " << q.size() << endl;

    // pop elements

    cout << " --> 5 --> pop elements" << endl;

    for (int i = 0; i != 2*DIM; i++) {
        int val = q.pop();
        if (i == 0) {
            cout << " --> 6 --> val = " << val << endl;
        }
    }

    // push elements again

    cout << " --> 7 --> push elements again" << endl;

    for (int i = 0; i != DIM; i++) {
        q.push(i);
    }

    cout << " --> 8 --> size of queue = " << q.size() << endl;
}

// the main function

int main()
{
    // local parameters

    const int DIM = 400;
    const int NT = 4;

    // threads array

    thread * th = new thread [NT];

    // spawn threads

    for (int i = 0; i != NT; i++) {
        th[i]= thread(doWork, DIM);
    }

    // join threads

    for (int i = 0; i != NT; i++) {
        th[i].join();
    }

    // delete threads array

    delete [] th;

    return 0;
}

// end
