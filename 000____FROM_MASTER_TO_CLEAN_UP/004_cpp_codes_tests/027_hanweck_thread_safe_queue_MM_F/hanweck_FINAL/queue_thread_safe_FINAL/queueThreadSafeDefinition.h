//============================//
// queueThreadSafe definition //
//============================//

#ifndef QUEUE_THREAD_SAFE_DEF_H
#define QUEUE_THREAD_SAFE_DEF_H

#include "queueThreadSafeDeclaration.h"

#include <queue>
#include <mutex>

using std::queue;
using std::mutex;
using std::unique_lock;

// 1 --> push # 1

template <typename T>
void queueThreadSafe<T>::push(const T & val)
{
    unique_lock<mutex> lck(m_mtx); // lock the scope
    m_queue.push(val); // push element into the queue
    lck.unlock(); // unlock the scope ASAP to reduce lock contention
    m_cv.notify_one(); // wake up one waiting thread
}

// 2 --> push # 2

template <typename T>
void queueThreadSafe<T>::push(T&& val)
{
    unique_lock<mutex> lck(m_mtx); // lock the scope
    m_queue.push(std::move(val)); // move the element to the queue
    lck.unlock(); // unlock the scope ASAP to reduce lock contention
    m_cv.notify_one(); // wake up one waiting thread
}

// 3 --> pop

template <typename T>
T queueThreadSafe<T>::pop()
{
    unique_lock<mutex> lck(m_mtx); // lock the scope

    while(m_queue.empty()) { // if queue is empty then wait
        m_cv.wait(lck);
    }

    T val = m_queue.front(); // get the front element
    m_queue.pop(); // pop the front element

    return val; // return by value the front element
}

// 4 --> emplace

template <typename T>
template <typename ... Args>
void queueThreadSafe<T>::emplace(Args && ... args)
{
    unique_lock<mutex> lck(m_mtx); // lock the scope
    m_queue.emplace(args ...); // build and push the element
    lck.unlock(); // unlock the scope asap to reduce lock contention
    m_cv.notify_one(); // notify one waiting thread
}

// 5 --> empty

template <typename T>
bool queueThreadSafe<T>::empty() const
{
    unique_lock<mutex> lck(m_mtx); // lock the scope
    bool val = m_queue.empty(); // get the boolean value

    return val; // return by value the above boolean value
}

// 6 --> size

template <typename T>
unsigned int queueThreadSafe<T>::size() const
{
    unique_lock<mutex> lck(m_mtx); // lock the scope
    unsigned int val = m_queue.size(); // get the size

    return val; // return by value the above size
}

#endif // QUEUE_THREAD_SAFE_DEF_H
