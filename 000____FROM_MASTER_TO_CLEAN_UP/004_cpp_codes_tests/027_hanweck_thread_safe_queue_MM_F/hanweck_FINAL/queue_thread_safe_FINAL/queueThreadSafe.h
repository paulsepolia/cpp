//========================//
// queueThreadSafe header //
//========================//

#ifndef QUEUE_THREAD_SAFE_H
#define QUEUE_THREAD_SAFE_H

#include "queueThreadSafeDeclaration.h"
#include "queueThreadSafeDefinition.h"

#endif // QUEUE_THREAD_SAFE_H
