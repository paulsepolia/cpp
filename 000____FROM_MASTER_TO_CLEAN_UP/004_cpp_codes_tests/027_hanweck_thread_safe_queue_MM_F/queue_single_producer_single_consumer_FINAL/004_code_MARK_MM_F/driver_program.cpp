
//=============================//
// driver program to queueSPSC //
//=============================//

#include "queue_spsc.h"
#include <iostream>
#include <thread>

using std::cout;
using std::endl;
using std::thread;

// function doWork

void doWork(const int & DIM)
{
    queueSPSC<int> q1;
    int val = 0;

    for (int i = 0; i != DIM; i++) {
        q1.push(i);
        q1.pop(val);
    }

    cout << " --> val = " << val << endl;

    q1.pop(val);
}

// the main function

int main()
{
    const int DIM = 40000;
    const int NT = 2;

    // threads array

    thread * th = new thread [NT];

    // spawn threads

    for (int i = 0; i != NT; i++) {
        th[i]= thread(doWork, DIM);
    }

    // join threads

    for (int i = 0; i != NT; i++) {
        th[i].join();
    }

    // delete threads array

    delete [] th;
}

// end
