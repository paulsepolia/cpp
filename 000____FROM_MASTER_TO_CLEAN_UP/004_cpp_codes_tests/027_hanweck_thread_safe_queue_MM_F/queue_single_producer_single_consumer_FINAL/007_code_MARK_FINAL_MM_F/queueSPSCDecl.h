#ifndef QUEUE_SPSC_DEF_H
#define QUEUE_SPSC_DEF_H

#include <list>
using std::list;

// class queueSCSP

template <typename T>
class queueSPSC {
public:

    // constructor

    queueSPSC<T>();

    // member functions

    void push(const T &); // pushes elements into the queue at the end and removes
    // that poped by pop
    bool pop(T &); // retrieves the first element
    unsigned int size() const;

private:

    list<T> m_list;
    typename list<T>::iterator m_first;
    typename list<T>::iterator m_last;
};

#endif // QUEUE_SPSC_DEF_H
