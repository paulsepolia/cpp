
#ifndef QUEUE_SPSC_DECL_H
#define QUEUE_SPSC_DECL_H

#include "queue_spsc_decl.h"
#include <list>

using std::list;

// constructor

template <class T>
queueSPSC<T>::queueSPSC()
{
    m_list.push_back(T());
    m_head = m_list.begin();
    m_tail = m_list.end();
}

// push

template <class T>
void queueSPSC<T>::push(const T & val)
{
    m_list.push_back(val);
    m_tail = m_list.end();
    m_list.erase(m_list.begin(), m_head);
}

// pop

template <class T>
bool queueSPSC<T>::pop(T & val)
{
    typename list<T>::iterator next_loc = m_head;

    ++next_loc;

    if (next_loc != m_tail) {
        m_head = next_loc;
        val = * m_head;
        return true;
    }

    return false;
}

// size

template <class T>
unsigned int queueSPSC<T>::size() const
{
    return m_list.size();
}

#endif // QUEUE_SCSP_DEF_H
