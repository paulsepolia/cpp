
//=============================//
// driver program to queueSPSC //
//=============================//

#include "queue_spsc.h"
#include <iostream>
#include <thread>

using std::cout;
using std::endl;
using std::thread;

// function doWork

void doWork(const int & DIM)
{
    queueSPSC<int> q1;
    int val = 0;

    cout << " 1 --> q1.size() = " << q1.size() << endl;

    // push elements

    for (int i = 0; i != DIM; i++) {
        cout << " 2 --> q1.size() = " << q1.size() << endl;
        q1.push(i);
    }

    // pop elements

    for (int i = 0; i != DIM; i++) {
        cout << " 3 --> q1.size() = " << q1.size() << endl;
        q1.pop(val);
        cout << " 4 --> val = " << val << endl;
    }

    q1.push(10);
    cout << " 5 --> q1.size() = " << q1.size() << endl;
}

// the main function

int main()
{
    const int DIM = 4;
    const int NT = 2;

    // threads array

    thread * th = new thread [NT];

    // spawn threads

    for (int i = 0; i != NT; i++) {
        th[i]= thread(doWork, DIM);
    }

    // join threads

    for (int i = 0; i != NT; i++) {
        th[i].join();
    }

    // delete threads array

    delete [] th;
}

// end
