//================//
// HEAVY INTEGERS //
//================//

#include <iostream>
#include <string>

using std::endl;
using std::cout;
using std::string;
using std::to_string;

// the solution

int solution(int A, int B)
{
    // local parameter

    const double SEVEN = 7.00;

    // local variables

    string s;
    int DIM;
    double sum = 0.0;
    int k = 0;

    // main loop

    for (int i = A; i != B+1; i++) {
        s = to_string(i);
        DIM = s.length();
        sum = 0.0;

        for (int j = 0; j != DIM; j++) {
            sum = sum + s[j]-'0';
        }

        sum = sum/DIM;

        if (sum > SEVEN) {
            k++;
        }
    }

    return k;
}

// the main function

int main()
{
    int A = 1;
    int B = 10000000;

    int sol = solution(A,B);

    cout << " --> number of heavy integers = " << sol << endl;

    return 0;
}

// END
