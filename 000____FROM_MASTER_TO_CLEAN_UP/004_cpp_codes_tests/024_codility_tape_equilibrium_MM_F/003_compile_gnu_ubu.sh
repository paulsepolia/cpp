#!/bin/bash

  # 1. compile

  g++  -O3                \
       -Wall              \
       -std=gnu++0x       \
       driver_program.cpp \
       -o x_gnu_ubu
