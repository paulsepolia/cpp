#!/bin/bash

  # 1. compile

  g++  -O3                \
       -Wall              \
       -std=gnu++0x       \
       -pthread           \
	  -fopenmp           \
       driver_program.cpp \
       -o x_gnu_ubu
