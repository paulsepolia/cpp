#!/bin/bash

  # 1. compile

  icpc  -O3                \
        -Wall              \
        -std=c++11         \
        -pthread           \
	   -openmp            \
	   sum_fun.cpp        \
        driver_program.cpp \
        -o x_intel
