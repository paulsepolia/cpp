
#include <iostream>
#include <string>
#include <cstdio>
#include <ctime>

using std::string;
using std::cout;
using std::endl;

// Get current date/time, format is YYYY-MM-DD.HH:mm:ss

const string currentDateTime()
{
    time_t now = time(0);
    struct tm tstruct;
    char buf[80];

    tstruct = *localtime(&now);

    strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);

    return buf;
}

// the main function

int main()
{
    cout << " currentDateTime() = " << currentDateTime() << endl;

    return 0;
}

// end
