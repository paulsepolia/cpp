//===========================//
// parallel dot using OpenMP //
//===========================//

#include <iostream>
#include <fstream>
#include <vector>
#include <iomanip>

using std::cout;
using std::endl;
using std::ifstream;
using std::ios;
using std::vector;
using std::fixed;
using std::setprecision;

// the main function

int main()
{
    // local parameters

    const int THREADS_NUM = 4; // number of OpenMP threads
    const int DIM = 10; // dimension of the vectors

    // local variables

    double * vA = new double [DIM];
    double * vB = new double [DIM];
    double * vC = new double [DIM];
    double sum_loc = 0.0;
    int i;

    // puts some numbers in the vectors
    // put zeros into the vector vC

    for (i = 0; i != DIM; i++) {
        vA[i] = i;
        vB[i] = i+1;
        vC[i] = 0.0;
    }

    // execute the product in parallel using OpenMP

    #pragma omp parallel default(none)\
    num_threads(THREADS_NUM)\
    private(i)\
    shared(vA)\
    shared(vB)\
    shared(vC)
    {
        for (i = 0; i < DIM; ++i) {
            vC[i] = vA[i] * vB[i];
        }
    }

    // evaluate the sum

    for (i = 0; i != DIM; ++i) {
        sum_loc = sum_loc + vC[i];
    }

    // output

    cout << fixed;
    cout << setprecision(5);

    cout << " dot product = " << sum_loc << endl;

    // free up the heap

    delete [] vA;
    delete [] vB;
    delete [] vC;

    return 0;
}

// end
