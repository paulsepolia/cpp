
#include <cmath>
#include <iostream>

// class: my_base_class

class my_base_class {

public:

    float compute_value(float in)
    {
        return std::sqrt(in + m_stored_value);
    }

    my_base_class(float stored_value) :
        m_stored_value(stored_value) { }

private:

    float m_stored_value;
};

// class :: my_class
// note: I added the "public" keywork to change the inheritance
// from "private" (the default) to "public" so as to be able to
// access the function "compute_value" from the inherited class

class my_class : public my_base_class {

public:

    my_class(float stored_value) :
        my_base_class(stored_value) { }
};

// the main function

int main(int argc, char* argv[])
{
    my_class my_obj(8.0f);
    std::cout << "\nThe value computed by my_class is: " << my_obj.compute_value(1.0f);
    std::cout << std::endl;
}

// end
