#!/bin/bash

  # 1. compile

  g++-5.2.0  -O3                  \
             -Wall                \
             -std=gnu++14         \
             driver_program_b.cpp \
             -o x_gnu_b
