
#include "parameters.h"
#include "functions.h"

#include <iostream>
#include <vector>
#include <cstdlib>
#include <string>
#include <ctime>
#include <iomanip>

using std::cout;
using std::cin;
using std::endl;
using std::vector;
using std::srand;
using std::string;
using std::time;
using std::setw;
using std::left;

#include <algorithm>
using std::count;

//==================//
// the main program //
//==================//

int main()
{
    //==================//
    // local parameters //
    //==================//

    const int I_DO_MAX = 10000;

    //=================//
    // local variables //
    //=================//

    // reels declaration and definition as vectors of strings

    vector<string> * reel = new vector<string> [NUM_REELS];

    // build reels --> step --> alpha --> NO RANDOM

    for (int k = 0; k != NUM_REELS; k++) {
        for (int i = 0; i != NUM_SYMBOLS; i++) {
            for (int j = 0; j != data1[i]; j++) {
                reel[k].push_back(symbols[i]);
            }
        }
    }

    // start game

    int win2 = 0; 		// amount of £ for two-same consecutive elements for each spin
    int win3 = 0; 		// amount of £ for three-same consecutive elements for each spin
    int win4 = 0; 		// amount of £ for four-same consecutive elements for each spin
    int win5 = 0; 		// amount of £ for five-same consecutive elements for each spin
    int total_win = 0;  // amount of total gain money from total spins

    // play game here

    for (int iL = 0; iL != I_DO_MAX; iL++) {

        cout << endl;
        cout << "============================================================>> Spin: " << iL <<  endl;

        // initialize random generator

        srand(static_cast<unsigned int>(time(0)));

        // build reels --> step --> beta (no same consecutive element) --> RANDOM

        for (int i = 0; i != NUM_REELS; i++) {
            build_reel(reel[i]);
        }

        // reels random rotation

        for (int i = 0; i != NUM_REELS; i++) {
            random_rotate_reel(reel[i]);
        }

        // display reels output

        display_reels_layout(reel);

        // create reels_all

        vector<vector<string>> * reels_all = new vector<vector<string>> [1];

        create_reels_all(reel, reels_all);

        // create paylines

        vector<string> * vec_paylines = new vector<string> [NUM_PAYLINES];

        for (int i = 0; i != NUM_PAYLINES; i++) {
            create_payline(payline[i], vec_paylines[i], reels_all[0]);
        }

        // display all paylines

        display_all_paylines(vec_paylines);

        // find possible winning paylines

        vector<int> * possible_line_symbol = new vector<int> [1];

        possible_win_paylines(vec_paylines, symbols, possible_line_symbol[0]);

        // check if the vector is empty

        vector<string> * win_symbol = new vector<string> [4];

        bool iflag3 = false;
        bool iflag4 = false;

        cout << "--------------------->> winning lines and combinations" << endl;
        cout << endl;

        if (possible_line_symbol[0].empty()) {
            cout << " --> NO POSSIBLE WINNING LINES" << endl;
        } else {

            // check each possible winning payline
            // for consecutive elements

            int k = 0;
            int line_num_current = 0;
            int line_num_old = -1;

            for (unsigned int i = 0; i != possible_line_symbol[0].size()/3; i++) {

                int line_num = possible_line_symbol[0][k];
                int counter = possible_line_symbol[0][k+2];

                // counter equal 2
                // check only for two consecutive same elements

                line_num_current = line_num;

                if (line_num_old != line_num_current) {

                    if (counter == 2) {
                        for (unsigned int j = 0; j != vec_paylines[line_num].size()-1; j++) {
                            if (vec_paylines[line_num][j] == vec_paylines[line_num][j+1]) {
                                win_symbol[0].push_back(vec_paylines[line_num][j+1]);

                                cout << " --> 2 same-consecutives for line " << line_num
                                     << ", winning symbol is " << vec_paylines[line_num][j] << endl;
                            }
                        }
                    }
                }

                // counter equal 3
                // check first for three consecutive same elements
                // and if not then check for two same consecutive same elements

                if (line_num_old != line_num_current) {

                    if (counter == 3) {
                        // check for 3 same
                        for (unsigned int j = 0; j != vec_paylines[line_num].size()-2; j++) {
                            if ((vec_paylines[line_num][j] == vec_paylines[line_num][j+1])&&
                                    (vec_paylines[line_num][j+1] == vec_paylines[line_num][j+2])) {
                                win_symbol[1].push_back(vec_paylines[line_num][j+2]);
                                iflag3 = true; // flag that I found 3 consecutive same elements

                                cout << " --> 3 same-consecutives for line " << line_num
                                     << ", winning symbol is " << vec_paylines[line_num][j] << endl;
                            }
                        }

                        if (!iflag3) {
                            // check for 2 same
                            for (unsigned int j = 0; j != vec_paylines[line_num].size()-1; j++) {
                                if (vec_paylines[line_num][j] == vec_paylines[line_num][j+1]) {
                                    win_symbol[0].push_back(vec_paylines[line_num][j+1]);

                                    cout << " --> 2 same-consecutives for line " << line_num
                                         << ", winning symbol is " << vec_paylines[line_num][j] << endl;
                                }
                            }
                        }
                    }
                }

                // counter equals 4
                // check first for four same consequtive elements
                // then for three consequtive elements
                // then for two consequtive elements

                // reset flags

                if (line_num_old != line_num_current) {

                    iflag3 = false;
                    iflag4 = false;

                    if (counter == 4) {
                        // check for 4 same
                        for (unsigned int j = 0; j != vec_paylines[line_num].size()-3; j++) {
                            if ((vec_paylines[line_num][j] == vec_paylines[line_num][j+1])&&
                                    (vec_paylines[line_num][j+1] == vec_paylines[line_num][j+2])&&
                                    (vec_paylines[line_num][j+2] == vec_paylines[line_num][j+3])) {
                                win_symbol[2].push_back(vec_paylines[line_num][j+3]);
                                iflag4 = true; // flag that I found 4 consecutive same elements

                                cout << " --> 4 same-consecutives for line " << line_num
                                     << ", winning symbol is " << vec_paylines[line_num][j] << endl;
                            }
                        }

                        if (!iflag4) {
                            // check for 3 same
                            for (unsigned int j = 0; j != vec_paylines[line_num].size()-2; j++) {
                                if ((vec_paylines[line_num][j] == vec_paylines[line_num][j+1])&&
                                        (vec_paylines[line_num][j+1] == vec_paylines[line_num][j+2])) {
                                    win_symbol[1].push_back(vec_paylines[line_num][j+2]);
                                    iflag3 = true; // flag that I found 3 consecutive same elements

                                    cout << " --> 3 same-consecutives for line " << line_num
                                         << ", winning symbol is " << vec_paylines[line_num][j] << endl;
                                }
                            }
                        }

                        if (!iflag4 && !iflag3) {
                            // check for 2 same
                            for (unsigned int j = 0; j != vec_paylines[line_num].size()-1; j++) {
                                if (vec_paylines[line_num][j] == vec_paylines[line_num][j+1]) {
                                    win_symbol[0].push_back(vec_paylines[line_num][j+1]);

                                    cout << " --> 2 same-consecutives for line " << line_num
                                         << ", winning symbol is " << vec_paylines[line_num][j] << endl;
                                }
                            }
                        }
                    }
                }

                // counter equals 5

                if (counter == 5) {

                    win_symbol[3].push_back(symbols[possible_line_symbol[0][k+1]]);

                    cout << " --> 5 same-consecutives for line " << line_num
                         << ", winning symbol is " << win_symbol[3][0] << endl;
                }

                // update counters

                line_num_old = line_num_current;
                k=k+3;
            }

            // calculate and display results

            const int spin_index = iL + 1;

            cout << endl;

            calculate_and_display_results(win_symbol,
                                          spin_index,
                                          total_win,
                                          win2,
                                          win3,
                                          win4,
                                          win5);
        }

        // delete win_symbol

        delete [] win_symbol;

        // delete possible_line_symbol

        delete [] possible_line_symbol;

        // delete reels_all

        delete [] reels_all;

        // delete vec_paylines

        delete [] vec_paylines;

        // spin again?

        cout << endl;
        cout << " --> Do you want to play again?(y/n) : ";
        char sentinel;
        cin >> sentinel;
        if (sentinel == 'n') break;
    }

    // delete reel

    delete [] reel;

    return 0;
}

// end
