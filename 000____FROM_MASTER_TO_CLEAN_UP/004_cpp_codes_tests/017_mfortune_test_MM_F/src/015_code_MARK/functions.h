
#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include "parameters.h"

#include <vector>
#include <string>

using std::vector;
using std::string;

// 1 --> build_reel

void build_reel(vector<string> &);

// 2 --> rotate_reel

void random_rotate_reel(vector<string> &);

// 3 --> create payline

void create_payline(const int *, vector<string> &, const vector<vector<string>> &);

// 4 --> find possible winning paylines

void possible_win_paylines(const vector<string> *,
                           const string *,
                           vector<int> &);

// 5 --> display reels layout

void display_reels_layout(const vector<string> *);

// # 6 --> put all reels in a vector of vector<string> records

void create_reels_all(const vector<string> *, vector<vector<string>> *);

// # 7 --> display all paylines

void display_all_paylines(const vector<string> *);

// # 8 --> calculate and display results

void calculate_and_display_results(const vector<string> *,
                                   const int &,
                                   int &,
                                   int &,
                                   int &,
                                   int &,
                                   int &);






#endif // FUNCTIONS_H

// end
