
#include "parameters.h"
#include "functions.h"

#include <iostream>
#include <vector>
#include <cstdlib>
#include <string>
#include <ctime>
#include <iomanip>

using std::cout;
using std::cin;
using std::endl;
using std::vector;
using std::srand;
using std::string;
using std::time;
using std::setw;
using std::left;

#include <algorithm>
using std::count;

//==================//
// the main program //
//==================//

int main()
{
    //==================//
    // local parameters //
    //==================//

    const int I_DO_MAX = 10000;

    //=================//
    // local variables //
    //=================//

    // reels declaration and definition as vectors of strings

    vector<string> * reel = new vector<string> [NUM_REELS];

    // build reels --> step --> alpha

    for (int k = 0; k != NUM_REELS; k++) {
        for (int i = 0; i != NUM_SYMBOLS; i++) {
            for (int j = 0; j != data1[i]; j++) {
                reel[k].push_back(symbols[i]);
            }
        }
    }

    //

    int total2 = 0;
    int total3 = 0;
    int total4 = 0;
    int total5 = 0;
    int total_win = 0;

    // play game here

    for (int iL = 0; iL != I_DO_MAX; iL++) {

        cout << endl;
        cout << "============================================================>> Spin: " << iL <<  endl;

        // initialize random generator

        srand(static_cast<unsigned int>(time(0)));

        // build reels --> step --> beta (no same consecutive element)

        for (int i = 0; i != NUM_REELS; i++) {
            build_reel(reel[i]);
        }

        // reels random rotation

        for (int i = 0; i != NUM_REELS; i++) {
            random_rotate_reel(reel[i]);
        }

        // reels output

        display_reels_layout(reel);

        // create reels_all

        vector<vector<string>> * reels_all = new vector<vector<string>> [1];

        create_reels_all(reel, reels_all);

        // create paylines

        vector<string> * vec_paylines = new vector<string> [NUM_PAYLINES];

        for (int i = 0; i != NUM_PAYLINES; i++) {
            create_payline(payline[i], vec_paylines[i], reels_all[0]);
        }

        // display all paylines

        cout << endl;
        cout << "-------------------->> paylines" << endl;
        cout << endl;

        for (int i = 0; i != NUM_PAYLINES; i++) {
            cout << setw(DELIM_LOC_A) << left << i;
            for (int j = 0; j != DIM_HORIZONTAL; j++) {
                cout << setw(DELIM_LOC_A) << left << vec_paylines[i][j] << " ";
            }
            cout << endl;
        }

        cout << endl;

        // find possible winning paylines

        vector<int> * possible_line_symbol = new vector<int> [1];

        possible_win_paylines(vec_paylines, symbols, possible_line_symbol[0]);

        // check if the vector is empty

        vector<string> * win_symbol = new vector<string> [4];

        bool iflag3 = false;
        bool iflag4 = false;

        if (possible_line_symbol[0].empty()) {
            cout << " --> NO POSSIBLE WINNING LINES" << endl;
        } else {

            // check each possible winning payline
            // for consecutive elements

            int k = 0;
            int line_num_current = 0;
            int line_num_old = -1;

            for (unsigned int i = 0; i != possible_line_symbol[0].size()/3; i++) {

                int line_num = possible_line_symbol[0][k];
                int counter = possible_line_symbol[0][k+2];

                // check only for two consecutive same elements

                line_num_current = line_num;

                if (line_num_old != line_num_current) {

                    if (counter == 2) {
                        for (unsigned int j = 0; j != vec_paylines[line_num].size()-1; j++) {
                            if (vec_paylines[line_num][j] == vec_paylines[line_num][j+1]) {
                                win_symbol[0].push_back(vec_paylines[line_num][j+1]);
                            }
                        }
                    }
                }

                // counter equal 3
                // check first for three consecutive same elements
                // and if not then check for two same consecutive same elements

                if (line_num_old != line_num_current) {

                    if (counter == 3) {
                        // check for 3 same
                        for (unsigned int j = 0; j != vec_paylines[line_num].size()-2; j++) {
                            if ((vec_paylines[line_num][j] == vec_paylines[line_num][j+1])&&
                                    (vec_paylines[line_num][j+1] == vec_paylines[line_num][j+2])) {
                                win_symbol[1].push_back(vec_paylines[line_num][j+2]);
                                iflag3 = true;
                            }
                        }

                        if (!iflag3) {
                            // check for 2 same
                            for (unsigned int j = 0; j != vec_paylines[line_num].size()-1; j++) {
                                if (vec_paylines[line_num][j] == vec_paylines[line_num][j+1]) {
                                    win_symbol[0].push_back(vec_paylines[line_num][j+1]);
                                }
                            }
                        }
                    }
                }

                // counter equals 4
                // check first for four same consequtive elements

                // reset flags

                if (line_num_old != line_num_current) {

                    iflag3 = false;
                    iflag4 = false;

                    if (counter == 4) {
                        // check for 4 same
                        for (unsigned int j = 0; j != vec_paylines[line_num].size()-3; j++) {
                            if ((vec_paylines[line_num][j] == vec_paylines[line_num][j+1])&&
                                    (vec_paylines[line_num][j+1] == vec_paylines[line_num][j+2])&&
                                    (vec_paylines[line_num][j+2] == vec_paylines[line_num][j+3])) {
                                win_symbol[2].push_back(vec_paylines[line_num][j+3]);
                                iflag4 = true;
                            }
                        }

                        if (!iflag4) {
                            // check for 3 same
                            for (unsigned int j = 0; j != vec_paylines[line_num].size()-2; j++) {
                                if ((vec_paylines[line_num][j] == vec_paylines[line_num][j+1])&&
                                        (vec_paylines[line_num][j+1] == vec_paylines[line_num][j+2])) {
                                    win_symbol[1].push_back(vec_paylines[line_num][j+2]);
                                    iflag3 = true;
                                }
                            }
                        }

                        if (!iflag4 && !iflag3) {
                            // check for 2 same
                            for (unsigned int j = 0; j != vec_paylines[line_num].size()-1; j++) {
                                if (vec_paylines[line_num][j] == vec_paylines[line_num][j+1]) {
                                    win_symbol[0].push_back(vec_paylines[line_num][j+1]);
                                }
                            }
                        }
                    }

                }
                // counter equals 5

                if (counter == 5) {

                    win_symbol[3].push_back(symbols[possible_line_symbol[0][k+1]]);
                }

                // update counters

                line_num_old = line_num_current;
                k=k+3;
            }

            // results

            cout << "-------------------->> results"  << endl;
            cout << endl;

            // two consecutive same elements

            int counter2 = 0;

            if (!win_symbol[0].empty()) {

                for (unsigned int i = 0; i != win_symbol[0].size(); i++) {
                    if ((win_symbol[0][i] == "Jackpot")  ||
                            (win_symbol[0][i] == "Pumpkin")  ||
                            (win_symbol[0][i] == "Eyes")) {
                        counter2++;
                    }
                }

                if (counter2 != 0) {

                    int c_jackpot =
                        count(win_symbol[0].begin(), win_symbol[0].end(), "Jackpot");

                    int c_pumpkin =
                        count(win_symbol[0].begin(), win_symbol[0].end(), "Pumpkin");

                    int c_eyes =
                        count(win_symbol[0].begin(), win_symbol[0].end(), "Eyes");

                    total2 = total2 + c_jackpot*10 + c_pumpkin*5 + c_eyes*8;

                    cout << " --> total pounds for same-two consecutive = "
                         << (c_jackpot*10 + c_pumpkin*5 + c_eyes*8) << endl;
                }
            }

            if ((win_symbol[0].empty()) || (counter2 == 0)) {
                cout << " --> NO SAME-TWO WINNING CONSECUTIVE ELEMENTS" << endl;

            }

            // three consecutive same elements

            if (!win_symbol[1].empty()) {

                int c_Wild =
                    count(win_symbol[1].begin(), win_symbol[1].end(), "Wild");

                int c_9 =
                    count(win_symbol[1].begin(), win_symbol[1].end(), "9");

                int c_10 =
                    count(win_symbol[1].begin(), win_symbol[1].end(), "10");

                int c_J =
                    count(win_symbol[1].begin(), win_symbol[1].end(), "J");

                int c_Q =
                    count(win_symbol[1].begin(), win_symbol[1].end(), "Q");

                int c_K =
                    count(win_symbol[1].begin(), win_symbol[1].end(), "K");

                int c_A =
                    count(win_symbol[1].begin(), win_symbol[1].end(), "A");

                int c_Pumpkin =
                    count(win_symbol[1].begin(), win_symbol[1].end(), "Pumpkin");

                int c_Eyes =
                    count(win_symbol[1].begin(), win_symbol[1].end(), "Eyes");

                int c_Jackpot =
                    count(win_symbol[1].begin(), win_symbol[1].end(), "Jackpot");

                int c_FreeSpins =
                    count(win_symbol[1].begin(), win_symbol[1].end(), "FreeSpins");

                int c_WW =
                    count(win_symbol[1].begin(), win_symbol[1].end(), "WW");

                int total3_tmp = c_Wild * 120 + c_9 * 10 + c_10 * 10 +
                                 c_J    * 20  + c_Q * 35 + c_K  * 15 +
                                 c_A    * 20  + c_Pumpkin * 60 + c_Eyes * 20  +
                                 c_Jackpot * 20 + c_FreeSpins * 5 + c_WW * 0 ;

                total3 = total3 + total3_tmp;

                cout << " --> total pounds for same-three consecutive = "
                     << total3_tmp << endl;
            }

            if (win_symbol[1].empty()) {
                cout << " --> NO SAME-THREE WINNING CONSECUTIVE ELEMENTS" << endl;

            }

            // four consecutive same elements

            if (!win_symbol[2].empty()) {

                int c_Wild =
                    count(win_symbol[2].begin(), win_symbol[2].end(), "Wild");

                int c_9 =
                    count(win_symbol[2].begin(), win_symbol[2].end(), "9");

                int c_10 =
                    count(win_symbol[2].begin(), win_symbol[2].end(), "10");

                int c_J =
                    count(win_symbol[2].begin(), win_symbol[2].end(), "J");

                int c_Q =
                    count(win_symbol[2].begin(), win_symbol[2].end(), "Q");

                int c_K =
                    count(win_symbol[2].begin(), win_symbol[2].end(), "K");

                int c_A =
                    count(win_symbol[2].begin(), win_symbol[2].end(), "A");

                int c_Pumpkin =
                    count(win_symbol[2].begin(), win_symbol[2].end(), "Pumpkin");

                int c_Eyes =
                    count(win_symbol[2].begin(), win_symbol[2].end(), "Eyes");

                int c_Jackpot =
                    count(win_symbol[2].begin(), win_symbol[2].end(), "Jackpot");

                int c_FreeSpins =
                    count(win_symbol[2].begin(), win_symbol[2].end(), "FreeSpins");

                int c_WW =
                    count(win_symbol[2].begin(), win_symbol[2].end(), "WW");

                int total4_tmp = c_Wild * 550 + c_9 * 15 + c_10 * 25 +
                                 c_J    * 65  + c_Q * 85 + c_K  * 55 +
                                 c_A    * 130  + c_Pumpkin * 140 + c_Eyes * 95  +
                                 c_Jackpot * 140 + c_FreeSpins * 10 + c_WW * 0 ;

                total4 = total4 + total4_tmp;

                cout << " --> total pounds for same-four consecutive = "
                     << total4_tmp << endl;
            }

            if (win_symbol[2].empty()) {
                cout << " --> NO SAME-FOUR WINNING CONSECUTIVE ELEMENTS" << endl;

            }

            // five consecutive same elements

            if (!win_symbol[3].empty()) {

                int c_Wild =
                    count(win_symbol[3].begin(), win_symbol[3].end(), "Wild");

                int c_9 =
                    count(win_symbol[3].begin(), win_symbol[3].end(), "9");

                int c_10 =
                    count(win_symbol[3].begin(), win_symbol[3].end(), "10");

                int c_J =
                    count(win_symbol[3].begin(), win_symbol[3].end(), "J");

                int c_Q =
                    count(win_symbol[3].begin(), win_symbol[3].end(), "Q");

                int c_K =
                    count(win_symbol[3].begin(), win_symbol[3].end(), "K");

                int c_A =
                    count(win_symbol[3].begin(), win_symbol[3].end(), "A");

                int c_Pumpkin =
                    count(win_symbol[3].begin(), win_symbol[3].end(), "Pumpkin");

                int c_Eyes =
                    count(win_symbol[3].begin(), win_symbol[3].end(), "Eyes");

                int c_Jackpot =
                    count(win_symbol[3].begin(), win_symbol[3].end(), "Jackpot");

                int c_FreeSpins =
                    count(win_symbol[3].begin(), win_symbol[3].end(), "FreeSpins");

                int c_WW =
                    count(win_symbol[3].begin(), win_symbol[3].end(), "WW");

                int total5_tmp = c_Wild * 1500 + c_9 * 40 + c_10 * 70 +
                                 c_J    * 90  + c_Q * 160 + c_K  * 135 +
                                 c_A    * 180  + c_Pumpkin * 220 + c_Eyes * 155  +
                                 c_Jackpot * 0 + c_FreeSpins * 70 + c_WW * 0 ;

                total5 = total5 + total5_tmp;

                cout << " --> total pounds for same-five consecutive = "
                     << total5_tmp << endl;
            }

            if (win_symbol[3].empty()) {
                cout << " --> NO SAME-FIVE WINNING CONSECUTIVE ELEMENTS" << endl;

            }
            // totals

            cout << " Total amount played = £ " << iL + 1 << endl;

            total_win = total2 + total3 + total4 + total5;

            cout << " Total amount won    = £ " << total_win << endl;

        }

        // delete win_symbol

        delete [] win_symbol;

        // delete possible_line_symbol

        delete [] possible_line_symbol;

        // delete reels_all

        delete [] reels_all;

        // delete vec_paylines

        delete [] vec_paylines;
    }

    return 0;
}

// end
