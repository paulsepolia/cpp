
#include "parameters.h"
#include "functions.h"

#include <iostream>
#include <vector>
#include <cstdlib>
#include <string>
#include <ctime>
#include <iomanip>

using std::cout;
using std::cin;
using std::endl;
using std::vector;
using std::srand;
using std::string;
using std::time;
using std::setw;
using std::left;

//==================//
// the main program //
//==================//

int main()
{
    //==================//
    // local parameters //
    //==================//

    const int I_DO_MAX = 1000;
    const int DELIM_LOC = 10;

    //=================//
    // local variables //
    //=================//

    // reels declaration and definition as vectors of strings

    vector<string> * reel = new vector<string> [NUM_REELS];

    // build reels --> step --> alpha

    for (int k = 0; k != NUM_REELS; k++) {
        for (int i = 0; i != NUM_SYMBOLS; i++) {
            for (int j = 0; j != data1[i]; j++) {
                reel[k].push_back(symbols[i]);
            }
        }
    }

    // play game here

    for (int iL = 0; iL != I_DO_MAX; iL++) {

        cout << "---------------------------------------------------------->> " << iL <<  endl;

        // initialize random generator

        srand(static_cast<unsigned int>(time(0)));

        // build reels --> step --> beta (no same consecutive element)

        for (int i = 0; i != NUM_REELS; i++) {
            build_reel(reel[i]);
        }

        // reels random rotation

        for (int i = 0; i != NUM_REELS; i++) {
            random_rotate_reel(reel[i]);
        }

        // reels output

        cout << endl;
        cout << "-------------------->> reels layout" << endl;
        cout << endl;

        for (int i = 0; i != DIM_VERTICAL; i++) {
            cout << setw(DELIM_LOC) << left << reel[0][i] << " "
                 << setw(DELIM_LOC) << left << reel[1][i] << " "
                 << setw(DELIM_LOC) << left << reel[2][i] << " "
                 << setw(DELIM_LOC) << left << reel[3][i] << " "
                 << setw(DELIM_LOC) << left << reel[4][i] << endl;
        }

        // create reels_all

        vector<vector<string>> * reels_all = new vector<vector<string>> [1];

        //vector<vector<string>> reels_all;

        for (int i = 0; i != NUM_REELS; i++) {
            reels_all[0].push_back(reel[i]);
        }

        // create paylines

        vector<string> * vec_paylines = new vector<string> [NUM_PAYLINES];

        for (int i = 0; i != NUM_PAYLINES; i++) {
            create_payline(payline[i],  vec_paylines[i],  reels_all[0]);
        }

        // check paylines

        cout << endl;
        cout << "-------------------->> paylines" << endl;
        cout << endl;

        for (int i = 0; i != NUM_PAYLINES; i++) {
            for (int j = 0; j != DIM_HORIZONTAL; j++) {
                cout << setw(DELIM_LOC) << left << vec_paylines[i][j] << " ";
            }
            cout << endl;
        }

        // clear reels_all

        delete [] reels_all;

        // delete vec_paylines

        delete [] vec_paylines;
    }

    return 0;
}

// end
