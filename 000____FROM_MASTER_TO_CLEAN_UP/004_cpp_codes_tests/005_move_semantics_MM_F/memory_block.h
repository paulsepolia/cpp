
#pragma once

#include <iostream>
#include <algorithm>

using std::cout;
using std::endl;
using std::copy;

class MemoryBlock {
public:

    // simple constructor that initializes the resource

    explicit MemoryBlock(unsigned long int length)
        : _length(length)
        , _data(new int[length])
    {
        cout << "In MemoryBlock(unsigned long int). length = "
             << _length << endl;
    }

    // destructor

    ~MemoryBlock()
    {
        cout << "In ~MemoryBlock(). length = "
             << _length;

        if (_data != NULL) {
            cout << " Deleting resource";
            // delete the resource
            delete[] _data;
        }

        cout << endl;
    }

    // copy constructor

    MemoryBlock(const MemoryBlock& other)
        : _length(other._length)
        , _data(new int[other._length])
    {
        cout << "In MemoryBlock(const MemoryBlock&). length = "
             << other._length << ". Copying resource" << endl;

        copy(other._data, other._data + _length, _data);
    }

    // copy assignment operator

    MemoryBlock& operator=(const MemoryBlock& other)
    {
        cout << "In operator=(const MemoryBlock&). length = "
             << other._length << ". Copying resource" << endl;

        if (this != &other) {
            // Free the existing resource
            delete[] _data;

            _length = other._length;
            _data = new int[_length];
            copy(other._data, other._data + _length, _data);
        }
        return *this;
    }

    // Retrieves the length of the data resource

    unsigned long int Length() const
    {
        return _length;
    }

    // Move constructor

    MemoryBlock(MemoryBlock&& other)
        : _length(0),
          _data(NULL)
    {
        cout << "In MemoryBlock(MemoryBlock&&). length = "
             << other._length << ". Moving resource" << endl;

        // Copy the data pointer and its length from the
        // source object

        _data = other._data;
        _length = other._length;

        // Release the data pointer from the source object so that
        // the destructor does not free the memory multiple times.

        other._data = NULL;
        other._length = 0;
    }

    // Move assignment operator

    MemoryBlock& operator=(MemoryBlock&& other)
    {
        cout << "In operator=(MemoryBlock&&). length = "
             << other._length << endl;

        if (this != &other) {
            // Free the existing resource

            delete[] _data;

            // Copy the data pointer and its length from the
            // source object

            _data = other._data;
            _length = other._length;

            // Release the data pointer from the source object so that
            // the destructor does not free the memory multiple times

            other._data = NULL;
            other._length = 0;
        }
        return *this;
    }

private:
    unsigned long int _length; // the length of the resource
    int * _data; // the resource
};

