#include <iostream>
#include <iomanip>
#include <memory>
#include <cmath>

constexpr size_t factorial(size_t n) {

    size_t res = 0;

    if (n == 0) {
        return 1;
    } else {
        res = n * factorial(n - 1);
    }

    return res;
}

constexpr double factorial_d(size_t n) {

    double res = 0;

    if (n == 0) {
        return 1;
    } else {
        res = n * factorial_d(n - 1);
    }

    return res;
}

int main() {

    const size_t MAX_FAC = 21;

    std::cout << std::setprecision(0);

    {
        std::cout << " --> example --> 1 --> start" << std::endl;

        for (size_t i = 0; i < MAX_FAC; i++) {
            std::cout << std::fixed << std::setw(30) << std::right << factorial(i) << std::endl;
        }

        std::cout << " --> example --> 1 --> end" << std::endl;
    }

    {
        std::cout << " --> example --> 2 --> start" << std::endl;

        for (size_t i = 0; i < MAX_FAC; i++) {
            std::cout << std::fixed << std::setw(30) << std::right << factorial_d(i) << std::endl;
        }

        std::cout << " --> example --> 2 --> end" << std::endl;
    }

    {
        std::cout << " --> example --> 3 --> start" << std::endl;

        for (size_t i = 0; i < MAX_FAC; i++) {
            std::cout << std::fixed << std::setw(30) << std::right << factorial(i) - factorial_d(i) << std::endl;
        }

        std::cout << " --> example --> 3 --> end" << std::endl;
    }

}
