#include <iostream>
#include <vector>
#include <memory>
#include <functional>
#include <chrono>
#include <iomanip>
#include <cmath>

class benchmark_timer {
public:

    explicit benchmark_timer(const double &res) :
            _res(res),
            t1{std::chrono::steady_clock::now()},
            t2{std::chrono::steady_clock::now()} {};

    ~benchmark_timer() {
        t2 = std::chrono::steady_clock::now();
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count()};
        std::cout << " --> res = " << _res << std::endl;
        std::cout << " --> time used (secs) = " << total_time << std::endl;
    }

    auto set_res(const double &res) -> void {
        _res = res;
    }

private:

    double _res{};
    decltype(std::chrono::steady_clock::now()) t1{};
    decltype(std::chrono::steady_clock::now()) t2{};
};

class A {
public:
    A() = default;

    explicit A(std::string name) : _name(std::move(name)) {}

    [[nodiscard]] auto get_name() const -> std::string {
        return _name;
    }

    auto set_name(std::string name) -> void {
        _name = std::move(name);
    }

    virtual ~A() = default;

    auto operator new(size_t size) -> void * {
        void *p{std::malloc(size)};
        if (DEBUG_LOG) {
            std::cout << "new: allocated " << size << " bytes" << std::endl;
        }
        return p;
    }

    auto operator delete(void *p) noexcept -> void {
        if (DEBUG_LOG) {
            std::cout << "delete: deleted memory" << std::endl;
        }
        return std::free(p);
    }

    auto operator new[](size_t size) -> void * {
        void *p{std::malloc(size)};
        if (DEBUG_LOG) {
            std::cout << "new: allocated " << size << " bytes" << std::endl;
        }
        return p;
    }

    auto operator delete[](void *p) noexcept -> void {
        if (DEBUG_LOG) {
            std::cout << "delete: deleted memory" << std::endl;
        }
        return std::free(p);
    }

private:
    std::string _name;
public:
    static bool DEBUG_LOG;
};

bool A::DEBUG_LOG = false;

int main() {

    std::cout << std::setprecision(5);
    std::cout << std::scientific;
    const auto TRY_MAX{size_t(std::pow(10.0, 1.0))};
    const auto DO_MAX{size_t(std::pow(10.0, 6.0))};
    const auto NUM_ELEM{size_t(std::pow(10.0, 2.0))};
    const auto NAME1{std::string("33333333333333333333333333333333333444444444444")};

    for (size_t ii = 0; ii < TRY_MAX; ii++) {

        std::cout << "-------------------------------------------->> # " << ii << std::endl;

        {
            std::cout << "-->> 1 --> inplace only --> proper way" << std::endl;
            auto sum_tot{0.0};
            auto ot{benchmark_timer(0.0)};

            auto memory{std::malloc(NUM_ELEM * sizeof(A))};

            for (size_t kk = 0; kk < DO_MAX; kk++) {
                auto p{reinterpret_cast<A *>(memory)};
                std::uninitialized_fill_n(p, NUM_ELEM, A(NAME1));
                sum_tot += (double) p[0].get_name()[0];
                sum_tot += (double) p[NUM_ELEM - 1].get_name()[0];

                for (size_t kk1 = 0; kk1 != NUM_ELEM; kk1++) {
                    std::destroy_at(&p[kk1]);
                }
            }
            ot.set_res(sum_tot);
            std::free(memory);
        }

        {
            std::cout << "-->> 2 --> malloc + inplace + free --> proper way" << std::endl;

            auto sum_tot{0.0};
            auto ot{benchmark_timer(0.0)};

            for (size_t kk = 0; kk < DO_MAX; kk++) {

                auto memory{std::malloc(NUM_ELEM * sizeof(A))};
                auto p{reinterpret_cast<A *>(memory)};
                std::uninitialized_fill_n(p, NUM_ELEM, A(NAME1));
                sum_tot += (double) p[0].get_name()[0];
                sum_tot += (double) p[NUM_ELEM - 1].get_name()[0];

                for (size_t kk1 = 0; kk1 != NUM_ELEM; kk1++) {
                    std::destroy_at(&p[kk1]);
                }
                std::free(memory);
            }
            ot.set_res(sum_tot);
        }

        {
            std::cout << "-->> 3 --> new + delete" << std::endl;

            auto sum_tot{0.0};
            auto ot{benchmark_timer(0.0)};

            for (size_t kk = 0; kk < DO_MAX; kk++) {
                auto p{new A[NUM_ELEM]};
                for (size_t kk1 = 0; kk1 < NUM_ELEM; kk1++) {
                    p[kk1] = A(NAME1);
                }
                sum_tot += (double) p[0].get_name()[0];
                sum_tot += (double) p[NUM_ELEM - 1].get_name()[0];
                delete[] p;
            }
            ot.set_res(sum_tot);
        }
    }
}
