#include <iostream>
#include <vector>
#include <deque>

template<template<typename> class OutContainer,
        template<typename> class InContainer,
        typename T>
OutContainer<T> recon(const InContainer<T> &in) {

    OutContainer<T> out;

    for (const auto &el: in) {
        out.push_back(el);
    }

    return out;
}

auto main() -> int {

    {
        std::cout << "---------------------------------->> 1" << std::endl;
        std::cout << "--> vector to deque" << std::endl;

        std::vector<double> v1;
        v1.push_back(10.0);
        v1.push_back(11.0);

        auto out{recon<std::deque, std::vector, double>(v1)};

        for (const auto &el: out) {
            std::cout << el << std::endl;
        }
    }

    {
        std::cout << "---------------------------------->> 2" << std::endl;
        std::cout << "--> deque to vector" << std::endl;

        std::deque<double> d1;
        d1.push_back(10.0);
        d1.push_back(11.0);

        auto out{recon<std::vector, std::deque, double>(d1)};

        for (const auto &el: out) {
            std::cout << el << std::endl;
        }
    }
}

