#include <string>
#include <iostream>

class UserEntry {
public:
    void Load() { }
    std::string GetName() const { return name; }
    unsigned GetAge() const { return age; }
private:
    std::string name {"pavlos"};
    unsigned age { 0 };
    size_t cacheEntry { 0 }; // not exposed
};

int main(){

    UserEntry ua;

    std::cout << ua.GetName() << std::endl;
}