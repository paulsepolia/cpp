
//=======================================//
// implementation of the class queue_pgg //
//=======================================//

#include "queue_pgg.h"

// default constructor

template <typename T>
queue_pgg<T>::queue_pgg() {}

// destructor

template <typename T>
queue_pgg<T>::~queue_pgg() {}

// assignment operator

template <typename T>
queue_pgg<T> & queue_pgg<T>::operator = (const queue_pgg<T> & s1)
{
    this->elems = s1.elems;

    return *this;
}

// copy constructor

template <typename T>
queue_pgg<T>::queue_pgg(const queue_pgg<T> & s1)
{
    *this = s1;
}

// empty --> public member function

template <typename T>
bool queue_pgg<T>::empty() const
{
    return elems.empty();
}

// size --> public member function

template <typename T>
size_t queue_pgg<T>::size() const
{
    return elems.size();
}

// front --> public member function

template <typename T>
T & queue_pgg<T>::front()
{
    return elems.front();
}

// back --> public member function

template <typename T>
T & queue_pgg<T>::back()
{
    return elems.back();
}

// push --> public member function

template <typename T>
void queue_pgg<T>::push(const T & arg)
{
    elems.push_back(arg);
}

// pop --> public member function

template <typename T>
void queue_pgg<T>::pop()
{
    elems.pop_front();
}

// swap --> public member function

template <typename T>
void queue_pgg<T>::swap(queue_pgg<T> & s1)
{
    queue_pgg<T> stmp;

    stmp = s1;
    s1 = *this;
    *this = stmp;
}

// emplace --> public member function

template <typename T>
void queue_pgg<T>::emplace(const T & arg)
{
    elems.emplace_back(arg);
}

//======//
// FINI //
//======//
