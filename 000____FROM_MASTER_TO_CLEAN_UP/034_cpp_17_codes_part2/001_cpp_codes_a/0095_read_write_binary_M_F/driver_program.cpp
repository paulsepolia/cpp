//=======================================//
// Writing a binary file and then        //
// Reading the entire binary file in RAM //
//=======================================//

#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>
#include <cmath>

using namespace std;

int main ()
{
    // 1. local variables and parameters

    const long int I_MAX = pow(10, 8);
    const string fileName = "my_file.bin";
    const double testDiff = 10E-6;
    ifstream fileIn;
    ofstream fileOut;
    streampos fileStart;
    streampos fileEnd;
    streampos fileSize;
    double* myArray;
    double tmpA;
    double tmpB;
    long int i;

    // 2. adjust the output format

    cout << fixed;
    cout << setprecision(10);
    cout << showpoint;
    cout << showpos;

    // 3. open the stream to write the data

    fileOut.open(fileName.c_str(), ios::out | ios::binary);

    // 4. test if the file stream is openend

    if(!fileOut.is_open()) {
        cout << " Error! The file to write to is not opened. Exit." << endl;
        return -1;
    }

    // 5. write to the file

    cout << " 1 --> Write data to the file" << endl;

    for (i = 0; i < I_MAX; i++) {
        tmpA = static_cast<double>(i+1);
        fileOut.write(reinterpret_cast<char*>(&tmpA), sizeof(tmpA));
    }

    // 6. close the file

    fileOut.close();

    // 7. open the file stream to read

    fileIn.open(fileName.c_str(), ios::in | ios::binary);

    // 8. test if the file stream is opened

    if (!fileIn.is_open()) {
        cout << " Error! The file to read from is not opened. Exit." << endl;
    }

    // 9. get the size in bytes of the file

    fileIn.seekg(0, ios::beg);
    fileStart = fileIn.tellg();

    fileIn.seekg(0, ios::end);
    fileEnd = fileIn.tellg();

    fileSize = fileEnd - fileStart;

    cout << " 2 --> The size of the file in bytes is --> " << fileSize << endl;

    // 10. read the entire file and store it ot RAM

    cout << " 3 --> Read the entire file and store it to RAM" << endl;

    myArray = new double [I_MAX];

    fileIn.seekg(0, ios::beg);

    fileIn.read(reinterpret_cast<char*>(&myArray[0]), fileSize);

    // 11. a hard test

    for (i = 0; i < I_MAX; i++) {
        tmpA = static_cast<double>(i+1);
        tmpB = myArray[i] - tmpA ;
        if (abs(tmpB) > testDiff) {
            cout << " Error in --> " << i << endl;
            cout << " Original value is --> " << tmpA << endl;
            cout << " The value I read is --> " << myArray[i] << endl;
            cout << " The difference is --> " << tmpB << endl;
            return -1;
        }
    }

    // 12. some output to test

    cout << " 5 --> Some output " << endl;

    for (i = 90; i < 93; i++) {
        cout << myArray[i] << endl;
    }

    // 13. free up the RAM

    cout << " 6 --> Free up the RAM" << endl;
    delete [] myArray;

    return 0;
}

//======//
// FINI //
//======//

