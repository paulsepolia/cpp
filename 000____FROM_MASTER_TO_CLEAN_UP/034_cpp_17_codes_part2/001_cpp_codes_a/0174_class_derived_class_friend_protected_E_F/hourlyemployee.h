//==================//
// hourlyemployee.h //
//==================//

//====================================================//
// This is the header file hourlyemployee.h           //
// This is the interface for the class HourlyEmployee //
//====================================================//

#ifndef HOURLYEMPLOYEE_H
#define HOURLYEMPLOYEE_H

#include <string>
#include "employee.h"

using std::string;

namespace pgg {
class HourlyEmployee : public Employee {
public:
    HourlyEmployee(); // default constructor
    HourlyEmployee(string, string, double, double); // non-default constructor
    void setRate(double);
    double getRate() const;
    void setHours(double);
    double getHours() const;
    void printCheck(); // change the definition of the base function
private:
    double wageRate;
    double hours;
};
} // pgg

#endif // HOURLYEMPLOYEE_H