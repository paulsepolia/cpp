
//=================//
// Hand-made queue //
//=================//

//===============//
// code --> 0325 //
//===============//

// keywords: list, deque, queue, handmade, hand made, container

#include <iostream>
#include <cmath>
#include <iomanip>
#include <deque>
#include <list>
#include "queue_pgg.h"
#include "queue_pgg.cpp"

using std::cout;
using std::cin;
using std::endl;
using std::pow;
using std::fixed;
using std::showpoint;
using std::showpos;
using std::setprecision;
using std::boolalpha;
using std::deque;
using std::list;

// the main function

int main()
{
    // local variables and parameters

    const long long I_MAX = static_cast<long long>(pow(10.0, 7.5));
    const long long K_MAX = static_cast<long long>(pow(10.0, 5.0));
    queue_pgg<double, deque<double> > * q1;
    queue_pgg<double, list<double> > * q2;

    // adjust the output format

    cout << fixed;
    cout << showpos;
    cout << showpoint;
    cout << setprecision(15);
    cout << boolalpha;

    // the benchmark loop

    for (long long k = 0; k != K_MAX; k++) {
        // the counter

        cout << "-------------------------------------------------------------->> " << k << endl;

        // q1 --> the hand made queue container

        q1 = new queue_pgg<double, deque<double> >;

        // q1 --> build the queue

        cout << " --> build the hand-made queue, *q1" << endl;

        for (long i = 0; i != I_MAX; i++) {
            q1->push(static_cast<double>(i));
        }

        // q1 = q2 --> use the = operator

        cout << " --> *q2 = *q1 --> use the operator = ()" << endl;

        q2 = new queue_pgg<double, list<double> >;

        *q2 = *q1;

        // q1 --> some values

        cout << " --> q1->empty() = " << q1->empty() << endl;
        cout << " --> q1->size()  = " << q1->size() << endl;
        cout << " --> q1->front() = " << q1->front() << endl;
        cout << " --> q1->back()  = " << q1->back() << endl;
        q1->pop();
        cout << " --> q1->front() = " << q1->front() << endl;
        cout << " --> q1->back()  = " << q1->back() << endl;

        // q1 --> pop the queue

        cout << " --> *q1 --> pop element-by-element the hand-made queue" << endl;

        while(!q1->empty()) {
            q1->pop();
        }

        // q1 --> some values

        cout << " --> q1->empty() = " << q1->empty() << endl;
        cout << " --> q1->size()  = " << q1->size() << endl;

        // q1 --> delete the queue

        delete q1;

        // q2 --> some values

        cout << " --> q2->empty() = " << q2->empty() << endl;
        cout << " --> q2->size()  = " << q2->size() << endl;
        cout << " --> q2->front() = " << q2->front() << endl;
        cout << " --> q2->back()  = " << q2->back() << endl;
        q2->pop();
        cout << " --> q2->front() = " << q2->front() << endl;
        cout << " --> q2->back()  = " << q2->back() << endl;

        // q2 --> pop the queue

        cout << " --> *q2 --> pop element-by-element the hand-made queue" << endl;

        while(!q2->empty()) {
            q2->pop();
        }

        // q2 --> some values

        cout << " --> q2->empty() = " << q2->empty() << endl;
        cout << " --> q2->size()  = " << q2->size() << endl;

        // q2 --> delete the container

        cout << " --> *q2 --> delete the hand-made queue" << endl;

        delete q2;

        // q1 --> build the container again

        cout << " --> build the two containers again, *q1 and *q2" << endl;

        q1 = new queue_pgg<double, deque<double> >;

        for (long long i = 0; i != I_MAX; i++) {
            q1->push(static_cast<double>(i));
        }

        // q2 --> build the container again

        q2 = new queue_pgg<double, list<double> >;

        for (long long i = 0; i != I_MAX; i++) {
            q2->push(static_cast<double>(-i));
        }

        // get some elements

        cout << " --> q1->front() = " << q1->front() << endl;
        cout << " --> q2->front() = " << q2->front() << endl;
        cout << " --> q1->back()  = " << q1->back()  << endl;
        cout << " --> q2->back()  = " << q2->back()  << endl;

        // swap the two containers

        cout << " --> swap the two containers, *q1 and *q2" << endl;

        q2->swap(*q1);

        // get some elements

        cout << " --> q1->front() = " << q1->front() << endl;
        cout << " --> q2->front() = " << q2->front() << endl;
        cout << " --> q1->back()  = " << q1->back()  << endl;
        cout << " --> q2->back()  = " << q2->back()  << endl;

        // use the copy constructor

        cout << " --> use of the copy constructor to initialize q3(*q1) and q4(*q2)" << endl;

        queue_pgg<double, list<double> > q3(*q1);
        queue_pgg<double, deque<double> > q4(*q2);

        // delete the containers

        cout << " --> delete the containers *q1, *q2" << endl;

        delete q1;
        delete q2;

        // get some elements

        cout << " --> q3->front() = " << q3.front() << endl;
        cout << " --> q4->front() = " << q4.front() << endl;
        cout << " --> q3->back()  = " << q3.back()  << endl;
        cout << " --> q4->back()  = " << q4.back()  << endl;

        // pop q3 and q4

        cout << " --> pop q3" << endl;

        while(!q3.empty()) {
            q3.pop();
        }

        cout << " --> pop q4" << endl;

        while(!q4.empty()) {
            q4.pop();
        }

        // use the emplace member function to build q3 and q4

        cout << " --> emplace the q3" << endl;

        for (long long i = 0; i != I_MAX; i++) {
            q3.push(static_cast<double>(i));
        }

        cout << " --> emplace the q4" << endl;

        for (long long i = 0; i != I_MAX; i++) {
            q4.push(static_cast<double>(i+1));
        }

        // get some elements

        cout << " --> q3->front() = " << q3.front() << endl;
        cout << " --> q4->front() = " << q4.front() << endl;
        cout << " --> q3->back()  = " << q3.back()  << endl;
        cout << " --> q4->back()  = " << q4.back()  << endl;

        // pop q3 and q4

        cout << " --> pop q3" << endl;

        while(!q3.empty()) {
            q3.pop();
        }

        cout << " --> pop q4" << endl;

        while(!q4.empty()) {
            q4.pop();
        }
    }

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

