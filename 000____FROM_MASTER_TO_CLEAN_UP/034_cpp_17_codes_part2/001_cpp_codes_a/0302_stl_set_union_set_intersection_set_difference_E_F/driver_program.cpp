
//===================//
// STL, includes,    //
// set_union,        //
// set_difference    //
// set_intersection  //
//===================//

#include <iostream>
#include <cassert>
#include <algorithm>
#include <vector>
#include <iterator>
#include <cmath>

using std::cout;
using std::cin;
using std::endl;
using std::includes;
using std::set_union;
using std::set_intersection;
using std::vector;
using std::back_inserter;
using std::pow;

// a function

template <class T>
bool compA(const T& x, const T& y)
{
    return (x == y);
}

// a function object

template <class T>
class compA_CL {
public:
    bool operator()(const T& x, const T& y)
    {
        return (x == y);
    }
};

// the main function

int main()
{

    cout << "Illustrating the generic set operations." << endl;
    vector<double> v1;
    vector<double> v2;
    vector<double> v3;
    const long long DIM_MAX = static_cast<long long>(pow(10.0, 7.0));

    // build v1

    for (long long i = 0; i < DIM_MAX; i++) {
        v1.push_back(cos(static_cast<double>(i+1)));
    }

    // build v2

    for (long long i = 0; i < DIM_MAX; i++) {
        v2.push_back(cos(static_cast<double>(i+2)));
    }

    // set_union v3 = union(v1,v2);

    set_union(v1.begin(), v1.end(), v2.begin(), v2.end(), back_inserter(v3));

    cout << " --> v3.size() = " << v3.size() << endl;

    // set_intersection v3 = set_intersection(v1,v2);

    v3.clear();
    v3.resize(0);

    set_intersection(v1.begin(), v1.end(), v2.begin(), v2.end(), back_inserter(v3));

    cout << " --> v3.size() = " << v3.size() << endl;

    // set_difference v3 = set_difference(v1,v2);

    v3.clear();
    v3.resize(0);

    set_difference(v1.begin(), v1.end(), v2.begin(), v2.end(), back_inserter(v3));

    cout << " --> v3.size() = " << v3.size() << endl;

    //
    // set_union v3 = union(v1,v2);
    //

    set_union(v1.begin(), v1.end(), v2.begin(), v2.end(), back_inserter(v3), compA<double>);

    cout << " --> v3.size() = " << v3.size() << endl;

    // set_intersection v3 = set_intersection(v1,v2);

    v3.clear();
    v3.resize(0);

    set_intersection(v1.begin(), v1.end(), v2.begin(), v2.end(), back_inserter(v3), compA<double>);

    cout << " --> v3.size() = " << v3.size() << endl;

    // set_difference v3 = set_difference(v1,v2);

    v3.clear();
    v3.resize(0);

    set_difference(v1.begin(), v1.end(), v2.begin(), v2.end(), back_inserter(v3), compA<double>);

    cout << " --> v3.size() = " << v3.size() << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

