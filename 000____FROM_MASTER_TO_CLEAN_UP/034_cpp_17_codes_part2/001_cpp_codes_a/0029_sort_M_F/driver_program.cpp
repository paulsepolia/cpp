//=========================================//
// Author: Pavlos G. Galiatsatos           //
// Date: 2013/10/13                        //
// Functions: Bubble, Selection, Insertion //
//=========================================//

#include <iostream>
#include <cmath>
#include <iomanip>
#include <ctime>

using namespace std;

// A. function declaration

// A-1.

void bubbleSort(long double* list, long int length);

// A-2.

void selectionSort(long double* list, long int length);

// A-3.

void insertionSort(long double* list, long int length);

// B. the main function

int main()
{
    // 1. variables and parameters

    const long int DIM_ARR = 200000;
    const int K_MAX = 3;
    long double* arrayA = new long double [DIM_ARR];
    long int i;
    int k;
    clock_t t;

    // 2. Adjust the output format

    cout << fixed;
    cout << showpoint;
    cout << setprecision(15);
    cout << showpos;

    // 3. Bubble Sort

    for(i = 0; i < DIM_ARR; i++) {
        arrayA[i] = cos(static_cast<double>(i));
    }

    for (k = 1; k <= K_MAX; k++) {
        cout << " --------------------------------------------> " << k << endl;
        cout << " Bubble Sort " << endl;

        t = clock();

        bubbleSort(arrayA, DIM_ARR);

        t = clock() - t;

        cout << " It took (seconds) = " << static_cast<double>(t)/CLOCKS_PER_SEC << endl;
        cout << " array[0]          = " << arrayA[0] << endl;
        cout << " array[1]          = " << arrayA[1] << endl;
        cout << " array[2]          = " << arrayA[2] << endl;
        cout << " array[DIM_ARR-3]  = " << arrayA[DIM_ARR-3] << endl;
        cout << " array[DIM_ARR-2]  = " << arrayA[DIM_ARR-2] << endl;
        cout << " array[DIM_ARR-1]  = " << arrayA[DIM_ARR-1] << endl;
    }

    // 4. Selection sort

    for(i = 0; i < DIM_ARR; i++) {
        arrayA[i] = cos(static_cast<double>(i));
    }

    for (k = 1; k <= K_MAX; k++) {
        cout << " --------------------------------------------> " << k << endl;
        cout << " Selection Sort " << endl;

        t = clock();

        selectionSort(arrayA, DIM_ARR);

        t = clock() - t;

        cout << " It took (seconds) = " << static_cast<double>(t)/CLOCKS_PER_SEC << endl;
        cout << " array[0]          = " << arrayA[0] << endl;
        cout << " array[1]          = " << arrayA[1] << endl;
        cout << " array[2]          = " << arrayA[2] << endl;
        cout << " array[DIM_ARR-3]  = " << arrayA[DIM_ARR-3] << endl;
        cout << " array[DIM_ARR-2]  = " << arrayA[DIM_ARR-2] << endl;
        cout << " array[DIM_ARR-1]  = " << arrayA[DIM_ARR-1] << endl;
    }

    // 5. Insertion Sort

    for(i = 0; i < DIM_ARR; i++) {
        arrayA[i] = cos(static_cast<double>(i));
    }

    for (k = 1; k <= K_MAX; k++) {
        cout << " --------------------------------------------> " << k << endl;
        cout << " Insertion Sort " << endl;

        t = clock();

        insertionSort(arrayA, DIM_ARR);

        t = clock() - t;

        cout << " It took (seconds) = " << static_cast<double>(t)/CLOCKS_PER_SEC << endl;
        cout << " array[0]          = " << arrayA[0] << endl;
        cout << " array[1]          = " << arrayA[1] << endl;
        cout << " array[2]          = " << arrayA[2] << endl;
        cout << " array[DIM_ARR-3]  = " << arrayA[DIM_ARR-3] << endl;
        cout << " array[DIM_ARR-2]  = " << arrayA[DIM_ARR-2] << endl;
        cout << " array[DIM_ARR-1]  = " << arrayA[DIM_ARR-1] << endl;
    }

    return 0;
}

// C. function declaration

// C-1.

void bubbleSort(long double* list, long int length)
{
    long double temp;
    long int iteration;
    long int index;

    for (iteration = 1; iteration < length; iteration++) {
        for (index = 0; index < length - iteration; index++) {
            if (list[index] > list[index+1]) {
                temp = list[index];
                list[index] = list[index + 1];
                list[index + 1] = temp;
            }
        }
    }
}

// C-2.

void selectionSort(long double* list, long int length)
{
    long int index;
    long int smallestIndex;
    long int location;
    long double temp;

    for (index = 0; index < length - 1; index++) {
        // step a

        smallestIndex = index;

        for (location = index + 1; location < length; location++) {
            if (list[location] < list[smallestIndex]) {
                smallestIndex = location;
            }
        }

        // step b

        temp = list[smallestIndex];
        list[smallestIndex] = list[index];
        list[index] = temp;
    }
}

// C-3.

void insertionSort(long double* list, long int length)
{
    long int firstOutOfOrder;
    long int location;
    long double temp;

    for (firstOutOfOrder = 1; firstOutOfOrder < length; firstOutOfOrder++) {
        if (list[firstOutOfOrder] < list[firstOutOfOrder - 1]) {
            temp = list[firstOutOfOrder];
            location = firstOutOfOrder;

            do {
                list[location] = list[location - 1];
                location--;
            } while (location > 0 && list[location-1] > temp);

            list[location] = temp;
        }
    }
}

//======//
// FINI //
//======//
