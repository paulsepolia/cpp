
//==================//
// insert iterators //
//==================//

#include <iostream>
#include <iomanip>
#include <list>
#include <vector>
#include <deque>
#include <iterator>
#include <cmath>

using std::endl;
using std::cin;
using std::cout;
using std::fixed;
using std::showpos;
using std::showpoint;
using std::setprecision;
using std::list;
using std::vector;
using std::deque;
using std::front_inserter;
using std::back_inserter;
using std::back_insert_iterator;
using std::front_insert_iterator;
using std::pow;

// the main function

int main()
{
    // local variables and parameters

    const long DIM_MAX = static_cast<long>(pow(10.0, 7.9));
    const long K_MAX = static_cast<long>(pow(10.0, 6.0));

    // adjust the output format

    cout << fixed;
    cout << showpos;
    cout << showpoint;
    cout << setprecision(20);

    // main bench loop

    for (long k = 0; k < K_MAX; k++) {
        // the containers

        vector<double> * v1 = new vector<double>;
        list<double>  * LA1 = new list<double>;
        deque<double> * dq1 = new deque<double>;

        cout << "---------------------------------------------------->> " << k << endl;

        // build the vector

        cout << " 1 --> build the vector" << endl;

        for (long i = 0; i < DIM_MAX; i++) {
            v1->push_back(cos(static_cast<double>(i)));
        }

        // copy the vector to the deque

        cout << " 2 --> vector --> deque" << endl;

        copy(v1->begin(), v1->end(), back_inserter(*dq1));

        cout << " 3 --> deque --> list" << endl;

        copy(dq1->begin(), dq1->end(), back_inserter(*LA1));

        // results

        cout << endl << endl;
        cout << (*v1)[0] << endl;
        cout << (*dq1)[0] << endl;
        cout << *(LA1->begin()) << endl;
        cout << (*v1)[1] << endl;
        cout << (*dq1)[1] << endl;
        cout << *(++LA1->begin()) << endl;
        cout << (*v1)[2] << endl;
        cout << (*dq1)[2] << endl;
        cout << *(++(++LA1->begin())) << endl;
        cout << endl << endl;

        // delete the containers

        cout << " 4 --> delete the vector" << endl;

        delete v1;

        cout << " 5 --> delete the list" << endl;

        delete LA1;

        cout << " 6 --> delete the deque" << endl;

        delete dq1;
    }

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
