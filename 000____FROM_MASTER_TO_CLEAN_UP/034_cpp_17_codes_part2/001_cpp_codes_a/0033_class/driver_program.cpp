//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/10/17              //
// Functions: Class              //
//===============================//

#include <iostream>
#include "illustrateHeader.h"

using namespace std;

int main()
{
    illustrate illusOb1(3);
    illustrate illusOb2(5);

    return 0;
}

//======//
// FINI //
//======//
