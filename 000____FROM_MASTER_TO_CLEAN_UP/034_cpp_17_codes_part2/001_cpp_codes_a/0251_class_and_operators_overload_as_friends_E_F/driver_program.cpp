
//==========================================//
// driver program to template class Pair<T> //
//==========================================//

#include <iostream>
#include <iomanip>
#include "pair_declaration.h"
#include "pair_definition.h"

using std::cout;
using std::cin;
using std::endl;
using std::fixed;
using std::setprecision;
using std::showpos;
using std::showpoint;
using std::boolalpha;
using std::noboolalpha;

// the main function

int main()
{
    // set the output format

    cout << fixed;
    cout << setprecision(5);
    cout << showpos;
    cout << showpoint;
    cout << boolalpha;

    // local variables and parameters

    // testing the default constructor and the member function -> set

    Pair<double> pA;

    pA.print();

    double val1 = 10.0;
    double val2 = 11.1;

    pA.set(val1, val2);

    pA.print();

    cout << " --> pA.getFirst()  = " << pA.getFirst() << endl;
    cout << " --> pA.getSecond() = " << pA.getSecond() << endl;

    pA.set(22.22, 33.33);

    pA.print();

    cout << " --> pA.getFirst()  = " << pA.getFirst() << endl;
    cout << " --> pA.getSecond() = " << pA.getSecond() << endl;

    pA.set(22.22);

    pA.print();

    cout << " --> pA.getFirst()  = " << pA.getFirst() << endl;
    cout << " --> pA.getSecond() = " << pA.getSecond() << endl;

    // testing the one argument constructor

    Pair<double> pB(23.45);

    pB.print();

    cout << " --> pB.getFirst()  = " << pB.getFirst() << endl;
    cout << " --> pB.getSecond() = " << pB.getSecond() << endl;

    // testing the two argument constructor

    Pair<double> pC(23.45, 12.34);

    pC.print();

    cout << " --> pC.getFirst()  = " << pC.getFirst() << endl;
    cout << " --> pC.getSecond() = " << pC.getSecond() << endl;

    // testing the friend function operator +

    Pair<double> pA1(1.0, 3.0);
    Pair<double> pA2(2.0, 4.0);
    Pair<double> pA3;

    pA3 = pA1 + pA2;

    cout << " --> pA3.getFirst()  = " << pA3.getFirst() << endl;
    cout << " --> pA3.getSecond() = " << pA3.getSecond() << endl;

    // testing the friend function operator -

    pA3 = pA1 - pA2;

    cout << " --> pA3.getFirst()  = " << pA3.getFirst() << endl;
    cout << " --> pA3.getSecond() = " << pA3.getSecond() << endl;

    // testing the friend function operator *

    pA3 = pA1 * pA2;

    cout << " --> pA3.getFirst()  = " << pA3.getFirst() << endl;
    cout << " --> pA3.getSecond() = " << pA3.getSecond() << endl;

    // testing the friend function operator /

    pA3 = pA1 / pA2;

    cout << " --> pA3.getFirst()  = " << pA3.getFirst() << endl;
    cout << " --> pA3.getSecond() = " << pA3.getSecond() << endl;

    pA1++;
    ++pA1;
    pA1--;
    --pA1;

    cout << pA1 << endl;
    cin >> pA1;
    cout << pA1 << endl;

    pA1 += pA2;
    cout << pA1 << endl;

    pA1 -= pA2;
    cout << pA1 << endl;

    pA1 *= pA2;
    cout << pA1 << endl;

    pA1 /= pA2;
    cout << pA1 << endl;

    // test the bool operators

    cout << "------------------------->> " << endl;

    cout << (pA1 == pA1 ) << endl;
    cout << (pA1 <= pA1 ) << endl;
    cout << (pA1 <  pA1 ) << endl;
    cout << (pA1 >= pA1 ) << endl;
    cout << (pA1 >  pA1 ) << endl;

    cout << "------------------------->> " << endl;

    cout << (pA1 == pA2 ) << endl;
    cout << (pA1 <= pA2 ) << endl;
    cout << (pA1 <  pA2 ) << endl;
    cout << (pA1 >= pA2 ) << endl;
    cout << (pA1 >  pA2 ) << endl;

    cout << "------------------------>> []" << endl;

    cout << " pA1[1] = " << pA1[1] << endl;
    cout << " pA1[2] = " << pA1[1] << endl;
    cout << " pA2[1] = " << pA2[1] << endl;
    cout << " pA2[2] = " << pA2[2] << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
