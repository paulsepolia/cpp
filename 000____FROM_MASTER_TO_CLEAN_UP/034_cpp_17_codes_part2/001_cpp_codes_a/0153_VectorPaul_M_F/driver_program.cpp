//================//
// driver program //
//================//

#include "IncludeLibs.h"
#include "VectorPaul.h"
#include "VectorPaulFunctions.h"
#include "ExternalFunctions.h"

// the main function

int main()
{
    // local parameters and variables

    const long MAX_DO = static_cast<long>(pow(10.0, 7.0));
    const long DIM = static_cast<long>(pow(10.0, 7.5));
    const int NUM_THREADS = 8;
    vector<thread> vecThreads;
    long i;
    long j;

    // set the output format

    cout << fixed;
    cout << setprecision(15);
    cout << showpoint;
    cout << showpos;

    // main do-loop

    for (i = 0; i < MAX_DO; i++) {
        // counter

        cout << "------------------------------------------------------------------>> " << i << " / " << MAX_DO << endl;

        // vector of objetcs

        VectorPaul<double> * objMain = new VectorPaul<double> [NUM_THREADS];

        // spawn threads

        for (j = 0; j < NUM_THREADS; j++) {
            vecThreads.push_back(thread( &benchFun<double>, objMain[j], DIM));
        }

        // join the threads

        for (auto & t: vecThreads) {
            t.join();
        }

        // clear the threads vector

        vecThreads.clear();

        // delete the objects

        delete [] objMain;
    }

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
