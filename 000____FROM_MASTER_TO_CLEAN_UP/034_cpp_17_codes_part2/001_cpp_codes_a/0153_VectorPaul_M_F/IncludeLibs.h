//================//
// common headers //
//================//

#include <iostream>
#include <cmath>
#include <iomanip>
#include <algorithm>
#include <vector>
#include <thread>

using std::cin;
using std::cout;
using std::endl;
using std::showpoint;
using std::setprecision;
using std::showpos;
using std::fixed;
using std::sort;
using std::random_shuffle;
using std::max_element;
using std::min_element;
using std::reverse;
using std::is_sorted;
using std::vector;
using std::thread;

//======//
// FINI //
//======//