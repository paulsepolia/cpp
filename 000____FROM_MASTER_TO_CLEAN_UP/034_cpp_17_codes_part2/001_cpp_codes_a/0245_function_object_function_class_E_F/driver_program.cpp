
//===========================================//
// operator() function as a binary predicate //
//===========================================//

#include <iostream>

using std::endl;
using std::cout;
using std::cin;

// template function object LessThan

template <class T>
class LessThan {
public:

    bool operator () (const T & x, const T & y) const
    {
        return (x < y);
    }
};

// template function object CompareLastDigits

template <class T>
class CompareLastDigits {
public:
    bool operator()(T x, T y) const
    {
        return (x%10 < y%10);
    }
};

// a template class

template <class T, class Compare>
class PairSelect {
public:

    // parameterized constructor

    PairSelect(const T &x, const T &y) : a(x), b(y) {}

    // member function

    void PrintSmaller() const
    {
        cout << (Compare() (a,b) ? a : b) << endl;
    }

private:
    T a;
    T b;

};

// the main function

int main()
{
    // test case where LessThan<double> is a function template class

    PairSelect<double, LessThan<double>> P(123.4, 98.7);

    P.PrintSmaller();

    // test case where CompareLastDigits<int> is a function template class

    PairSelect<int, CompareLastDigits<int>> Q(123, 98);

    Q.PrintSmaller();

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

