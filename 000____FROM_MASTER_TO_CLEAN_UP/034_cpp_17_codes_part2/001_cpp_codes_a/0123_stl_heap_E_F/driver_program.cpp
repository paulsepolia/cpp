//================//
// STL algorithms //
//================//

#include <iostream>
#include <algorithm>
#include <vector>
#include <deque>
#include <cmath>
#include <ctime>
#include <iomanip>

int main()
{
    // local variables and parameters

    long int i;
    long int k;
    const long int I_MAX = static_cast<long int>(pow(10.0, 7.9));
    const long int K_MAX = static_cast<long int>(pow(10.0, 7.0));

    // set the output format

    std::cout << std::fixed;
    std::cout << std::setprecision(5);
    std::cout << std::showpoint;
    std::cout << std::showpos;

    for (k = 0; k < K_MAX; k++) {
        std::cout << "---------------------------------------------------->> " << k << std::endl;

        //========//
        // vector //
        //========//

        // declaring the vector

        std::vector<double>* vecA = new std::vector<double>[1];

        // build the vector

        std::cout << " --> push elements into the vector ..." << std::endl;
        for (i = 0; i < I_MAX; i++) {
            (*vecA).push_back(cos(static_cast<double>(i)));
        }

        // print

        //for (double v : (*vecA))
        //{ std::cout << v << std::endl; }

        // increments the value in the vector

        std::cout << " --> increments the vector ..." << std::endl;
        for (double &v : (*vecA)) {
            v++;
        }

        // stabel_sort the vector

        std::cout << " --> stable_sorting the vector ..." << std::endl;
        std::stable_sort((*vecA).begin(), (*vecA).end());

        // fill the vector with a double

        std::cout << " --> filling the vector ..." << std::endl;
        std::fill((*vecA).begin(), (*vecA).end(), static_cast<double>(2.0));

        // clear the vector

        std::cout << " --> clearing the vector ..." << std::endl;
        (*vecA).clear();

        // delete the vector

        std::cout << " --> deleting the vector ..." << std::endl;
        delete [] vecA;

        //=======//
        // deque //
        //=======//

        // declaring the deque

        std::deque<double>* deqA = new std::deque<double> [1];

        // build the deque

        std::cout << " --> push elements into the deque ..." << std::endl;
        for (i = 0; i < I_MAX; i++) {
            (*deqA).push_back(cos(static_cast<double>(i)));
        }

        // print the deque

        //for (double v : (*deqA))
        //{ std::cout << v << std::endl; }

        // increments the value in the deque

        std::cout << " --> increments the deque ..." << std::endl;
        for (double &v : (*deqA)) {
            v++;
        }

        // stable_sort the deque

        std::cout << " --> stable_sorting the deque ..." << std::endl;
        std::stable_sort((*deqA).begin(), (*deqA).end());

        // fill the deque with a double

        std::cout << " --> filling the deque ..." << std::endl;
        std::fill((*deqA).begin(), (*deqA).end(), static_cast<double>(2.0));

        // clear the deque

        std::cout << " --> clearing the deque ..." << std::endl;
        (*deqA).clear();

        // delete the deque

        std::cout << " --> deleting the deque ..." << std::endl;
        delete[] deqA;

        //=======//
        // list //
        //=======//

        // declaring the list

        std::deque<double>* listA = new std::deque<double>[1];

        // build the list

        std::cout << " --> push elements into the list ..." << std::endl;
        for (i = 0; i < I_MAX; i++) {
            (*listA).push_back(cos(static_cast<double>(i)));
        }

        // print the list

        //for (double v : (*listA))
        //{ std::cout << v << std::endl; }

        // increments the value in the list

        std::cout << " --> increments the list ..." << std::endl;
        for (double &v : (*listA)) {
            v++;
        }

        // stable_sort the list

        std::cout << " --> stable_sorting the list ..." << std::endl;
        std::stable_sort((*listA).begin(), (*listA).end());

        // fill the deque with a double

        std::cout << " --> filling the list ..." << std::endl;
        std::fill((*listA).begin(), (*listA).end(), static_cast<double>(2.0));

        // clear the list

        std::cout << " --> clearing the list ..." << std::endl;
        (*listA).clear();

        // delete the list

        std::cout << " --> deleting the list ..." << std::endl;
        delete[] listA;
    }

    // sentinel

    int sentinel;
    std::cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
