
//=============================//
// creating a custom algorithm //
//=============================//

#include <iostream>
#include <vector>
#include <list>
#include <algorithm>

using std::cout;
using std::cin;
using std::endl;
using std::vector;
using std::list;

// an hand-made algorithm that doubles
// the values in specified range

template<class T>
void times2(T start, T end)
{
    while(start != end) {
        *start = (*start) * 2;
        start++;
    }
}

// an algorithm that
// triples the values in specified range

template<class T>
void times3(T start, T end)
{
    while(start != end) {
        *start = (*start) * 3;
        start++;
    }
}

// the main function

int main()
{
    // local variables and parameters

    vector<double> vA;

    // build the vector

    for (int i = 0; i < 10; i++) {
        vA.push_back(i);
    }

    // original vector

    for (int i = 0; i < 10; i++) {
        cout << vA[i] << " ";
    }

    // apply the hand-made algorithm

    times2(vA.begin(), vA.end());

    // output the new vector

    cout << endl;

    for (unsigned int i = 0; i < vA.size(); i++) {
        cout << vA[i] << " ";
    }

    // apply the hand-made algorithm

    times3(vA.begin(), vA.end());

    // output the new vector

    cout << endl;

    for (unsigned int i = 0; i < vA.size(); i++) {
        cout << vA[i] << " ";
    }

    // sentineling

    cout << endl;
    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
