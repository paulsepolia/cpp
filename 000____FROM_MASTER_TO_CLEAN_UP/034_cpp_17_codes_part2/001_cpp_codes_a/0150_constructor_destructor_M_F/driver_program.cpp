
// Destructors fulfill the opposite functionality of constructors:
// They are responsible for the necessary cleanup needed by a class when its lifetime ends.
// The classes we have defined in previous chapters did not allocate any resource
// and thus did not really require any clean up.

// But now, let's imagine that the class
// allocates dynamic memory to store the string it has as data member;
// in this case, it would be very useful to have a function
// called automatically at the end of the object's life in charge of releasing this memory.
// To do this, we use a destructor.
// A destructor is a member function very similar to a default constructor:
// it takes no arguments and returns nothing, not even void.
// It also uses the class name as its own name, but preceded with a tilde sign (~):


// destructors

#include <iostream>
#include <string>

using std::string;
using std::endl;
using std::cout;
using std::cin;

// a class

class Example4 {
private:

    string* ptr;
    int*    a1 = new int    [1000000000];
    double* a2 = new double [1000000000];

public:

    Example4 () : ptr (new string)
    {
        cout << " --> constructor --> 1 " << endl;     // constructor
    }

    Example4 (const string& str) : ptr (new string (str))
    {
        cout << " --> constructor --> 2 " << endl;     // constructor
    }

    ~Example4 ()
    {
        cout << " ------------------------> destructor --> 3 " << endl;

        delete ptr;
        delete [] a1;
        delete [] a2;
    } // destructor

    const string& content() const   // public member function
    {
        return *ptr;
    }
};

// the main function

int main ()
{
    Example4 foo;
    Example4 bar ("Example");

    long i;
    const long I_MAX = 10000000L;

    for (i = 0; i < I_MAX; i++) {
        Example4 foo2;
        Example4 foo3("1");
    }

    cout << "bar's content: " << bar.content() << endl;
    cout << "foo's content: " << foo.content() << endl;

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
