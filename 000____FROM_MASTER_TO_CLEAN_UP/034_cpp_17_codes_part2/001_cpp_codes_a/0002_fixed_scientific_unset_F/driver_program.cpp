//=================================//
// manipulators: scientific, fixed //
//=================================//

//===============//
// code --> 0002 //
//===============//

// keywords: manipulators, fixed, scientific, unset, ios

#include <iostream>

using std::cin;
using std::cout;
using std::endl;
using std::ios;
using std::fixed;
using std::scientific;

// the main function

int main()
{

    double hours = 35.45;
    double rate = 15.00;
    double tolerance = 0.01000;

    cout << " --> 1" << endl;

    cout << " --> default settings:" << endl;

    cout << " --> 2" << endl;

    cout << " --> hours = " << hours << endl;
    cout << " --> rate = " << rate << endl;
    cout << " --> pay = " << hours * rate << endl;
    cout << " --> tolerance = " << tolerance << endl;

    cout << " --> 3" << endl;

    cout << scientific;

    cout << " --> scientific notation:" << endl;

    cout << " --> 4" << endl;

    cout << " --> hours = " << hours << endl;
    cout << " --> rate = " << rate << endl;
    cout << " --> pay = " << hours * rate << endl;
    cout << " --> tolerance = " << tolerance << endl;

    cout << " --> 5" << endl;

    cout.unsetf(ios::scientific);

    cout << fixed;

    cout << " --> fixed decimal notation:" << endl;

    cout << " --> 6" << endl;

    cout << " --> hours = " << hours << endl;
    cout << " --> rate = " << rate << endl;
    cout << " --> pay = " << hours * rate << endl;
    cout << " --> tolerance = " << tolerance << endl;

    cout << " --> 7" << endl;

    cout.unsetf(ios::fixed);

    cout << " --> again default settings:" << endl;

    cout << " --> 8" << endl;

    cout << " --> hours = " << hours << endl;
    cout << " --> rate = " << rate << endl;
    cout << " --> pay = " << hours * rate << endl;
    cout << " --> tolerance = " << tolerance << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;


    return 0;
}

//======//
// FINI //
//======//

