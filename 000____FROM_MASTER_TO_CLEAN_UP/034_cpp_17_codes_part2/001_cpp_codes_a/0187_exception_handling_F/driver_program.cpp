
//====================//
// Exception handling //
//====================//

#include <iostream>
#include <string>

using std::string;
using std::cout;
using std::endl;
using std::cin;


int main()
{
    int dividend;
    int divisor = 1;
    int quotient;

    string inpStr = "The input stream is in the fail state.";

    try {
        cout << "Line 4: Enter the dividend: ";
        cin >> dividend;
        cout << endl;

        cout << "Line 7: Enter the divisor: ";
        cin >> divisor;
        cout << endl;

        if (divisor == 0) {
            throw divisor;
        } else if (divisor < 0) {
            throw string ("Negative divisor.");
        } else if (!cin) {
            throw inpStr;
        }

        quotient = dividend / divisor;

        cout << "Line 17: Quotient = " << quotient << endl;
    } catch (int x) {
        cout << "Line 19: Division be " << x << endl;
    } catch (string s) {
        cout << "Line 21: " << s << endl;
    }

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
