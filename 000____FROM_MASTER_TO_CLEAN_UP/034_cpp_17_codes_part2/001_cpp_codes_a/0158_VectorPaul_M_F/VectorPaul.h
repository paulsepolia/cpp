//==================//
// VectorPaul class //
//==================//

#include "IncludeLibs.h"

// a class

template <typename T>
class VectorPaul {
private:

    long dim;
    T * p1;
    static int RandomNumber();

    struct c_unique {
        int current;
        c_unique() : current (0) { }
        int operator()()
        {
            return ++current;
        }
    } UniqueNumber;

public:

    // member functions here

    void allocateVector();
    void deleteVector();
    void fillVector(T&);
    void generateVectorRandom();
    void generateVectorUnique();
    T getVector(long);
    long getDim();
    bool isSortedVector();
    T maxElementVector();
    T minElementVector();
    void randomShuffleVector();
    void reverseVector();
    void setDim(long);
    void setVector(long, T);
    void sortVector();

    // friend functions here
};

//======//
// FINI //
//======//
