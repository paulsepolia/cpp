//======================//
// VectorPaul Functions //
//======================//

#include "IncludeLibs.h"

//===============================//
// private member functions here //
//===============================//

// private member function

template <typename T>
int VectorPaul<T>::RandomNumber()
{
    return (rand()%100);
}

//==============================//
// public member functions here //
//==============================//

// public member function

template <typename T>
void VectorPaul<T>::allocateVector()
{
    p1 = new T [dim];
}

// public member function

template <typename T>
void VectorPaul<T>::deleteVector()
{
    delete [] p1;
}

// public member function

template <typename T>
void VectorPaul<T>::fillVector(T & elem)
{
    long i;
    for (i = 0; i < dim; i++) {
        p1[i] = elem;
    }
}

// public member function

template <typename T>
void VectorPaul<T>::generateVectorRandom()
{
    return generate(p1, p1+dim, RandomNumber);
}

// public member function

template <typename T>
void VectorPaul<T>::generateVectorUnique()
{
    return generate(p1, p1+dim, UniqueNumber);
}

// public member function

template <typename T>
long VectorPaul<T>::getDim()
{
    return dim;
}

// public member function

template <typename T>
T VectorPaul<T>::getVector(long i)
{
    return p1[i];
}

// public member function

template <typename T>
bool VectorPaul<T>::isSortedVector()
{
    return is_sorted(p1, p1+dim);
}

// public member function

template <typename T>
T VectorPaul<T>::maxElementVector()
{
    return *max_element(p1, p1+dim);
}

// public member function

template <typename T>
T VectorPaul<T>::minElementVector()
{
    return *min_element(p1, p1+dim);
}

// public member function

template <typename T>
void VectorPaul<T>::randomShuffleVector()
{
    random_shuffle(p1, p1+dim);
}

// public member function

template <typename T>
void VectorPaul<T>::reverseVector()
{
    reverse(p1, p1+dim);
}

// public member function

template <typename T>
void VectorPaul<T>::setDim(long i)
{
    dim = i;
}

// public member function

template <typename T>
void VectorPaul<T>::setVector(long i, T val)
{
    p1[i] = val;
}

// public member function

template <typename T>
void VectorPaul<T>::sortVector()
{
    sort(p1, p1+dim);
}

//======//
// FINI //
//======//