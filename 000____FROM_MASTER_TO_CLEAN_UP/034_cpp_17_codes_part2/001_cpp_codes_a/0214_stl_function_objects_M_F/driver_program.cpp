
//===========================================//
// use the logical_not unary function object //
//===========================================//

#include <iostream>
#include <vector>
#include <functional>
#include <algorithm>

using std::cout;
using std::cin;
using std::endl;
using std::vector;
using std::boolalpha;
using std::transform;
using std::logical_not;

// the main function

int main()
{
    // local variables and parameters

    vector<bool> vA;
    logical_not<bool> funObjA; // function object

    // put values into vA

    for (int i = 0; i < 10; i++) {
        vA.push_back(static_cast<bool>(i%2));
    }

    // turn on boolalpha I/O flag

    cout << boolalpha;

    // display the contents of the vector

    cout << " --> original contents of vA --> " << endl;

    for (unsigned int i = 0; i < vA.size(); i++) {
        cout << vA[i] << " <--> ";
    }

    cout << endl;

    // use the logical_not function object

    transform(vA.begin(), vA.end(), vA.begin(), funObjA); // use function object

    // display the inverted contents of vA

    cout << " --> inverted contents of vA --> " << endl;

    for (unsigned int i = 0; i < vA.size(); i++) {
        cout << vA[i] << " <--> ";
    }

    // use the logical_not function object

    // use the defualt constructor of the "logical_not" class
    // to create the function object
    // the function object is destroyed at the return of the transform function

    transform(vA.begin(), vA.end(), vA.begin(), logical_not<bool>());

    // display the re-inverted contents of vA

    cout << endl;

    cout << " --> re-inverted contents of vA --> " << endl;

    for (unsigned int i = 0; i < vA.size(); i++) {
        cout << vA[i] << " <--> ";
    }

    cout << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

