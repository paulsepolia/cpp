
///===============//
// driver program //
///===============//

#include <iostream>
#include <iomanip>
#include <string>
#include "BaseHeader.h"
#include "BaseDefinition.h"
#include "DerivedHeader.h"
#include "DerivedDefinition.h"

using std::cin;
using std::cout;
using std::endl;
using std::fixed;
using std::setprecision;
using std::showpoint;
using std::showpos;
using std::string;

using pgg::base;
using pgg::derived;

int main()
{
    cout << fixed;
    cout << showpos;
    cout << showpoint;
    cout << setprecision(2);

    // base

    base ticketA;

    cout << " -->  1 --> " << ticketA.getTicketNumber() << endl;
    cout << " -->  2 --> " << ticketA.getTicketPrice() << endl;

    ticketA.setTicketNumber(10);
    ticketA.setTicketPrice(1234.567);

    cout << " -->  3 --> " << ticketA.getTicketNumber() << endl;
    cout << " -->  4 --> " << ticketA.getTicketPrice() << endl;

    ticketA.resetTicket();

    cout << " -->  5 --> " << ticketA.getTicketNumber() << endl;
    cout << " -->  6 --> " << ticketA.getTicketPrice() << endl;

    // derived

    derived ticketB;

    cout << " -->  7 --> " << ticketB.getTicketNumber() << endl;
    cout << " -->  8 --> " << ticketB.getTicketPrice() << endl;
    cout << " -->  9 --> " << ticketB.getTicketSeason() << endl;

    ticketB.setTicketNumber(10);
    ticketB.setTicketPrice(1234.567);
    ticketB.setTicketSeason("Summer");

    cout << " --> 10 --> " << ticketB.getTicketNumber() << endl;
    cout << " --> 11 --> " << ticketB.getTicketPrice() << endl;
    cout << " --> 12 --> " << ticketB.getTicketSeason() << endl;

    ticketB.resetTicket();

    cout << " --> 13 --> " << ticketB.getTicketNumber() << endl;
    cout << " --> 14 --> " << ticketB.getTicketPrice() << endl;
    cout << " --> 15 --> " << ticketB.getTicketSeason() << endl;

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
