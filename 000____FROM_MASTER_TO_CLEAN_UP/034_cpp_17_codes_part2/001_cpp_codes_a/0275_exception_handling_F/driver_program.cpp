
//====================//
// exception handling //
//====================//

#include <iostream>

using std::cout;
using std::cin;
using std::endl;

// the throw type class

class NoMilk {
public:

    NoMilk() {};
    NoMilk(int howMany) : count(howMany) {};
    int getCount() const
    {
        return count;
    }

private:

    int count;
};

// the main function

int main()
{

    int donuts;
    int milk;
    double dpg;

    try {
        cout << "Enter number of donuts: ";
        cin >> donuts;
        cout << endl;
        cout << "Enter number of glasses of milk: ";
        cin >> milk;
        cout << endl;

        if (milk <= 0) {
            throw NoMilk(donuts);
        }

        dpg = donuts / static_cast<double>(milk);
        cout << donuts << " donuts." << endl;
        cout << milk << " glasses of milk." << endl;
        cout << " You have " << dpg << " donuts for each glass of milk." << endl;
    } catch(NoMilk e) {
        cout << e.getCount() << " donuts, and NO MILK!" << endl;
        cout << " GO BUY MILK!" << endl;
    }

    cout << "End of program!" << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

