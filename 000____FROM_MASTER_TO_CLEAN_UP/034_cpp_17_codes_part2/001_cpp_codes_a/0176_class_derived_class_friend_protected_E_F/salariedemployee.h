//====================//
// salariedemployee.h //
//====================//

//======================================================//
// This is the header file salariedemployee.h           //
// This is the interface for the class SalariedEmployee //
//======================================================//

#ifndef SALARIEDEMPLOYEE_H
#define SALARIEDEMPLOYEE_H

#include <string>
#include "employee.h"

using std::string;

namespace pgg {
class SalariedEmployee : public Employee {
public:
    SalariedEmployee(); // default constructor
    SalariedEmployee(string, string, double); // non-default constructor
    double getSalary() const;
    void setSalary(double);
    void printCheck(); // change the definition of the base function
private:
    double salary;
};
} // pgg

#endif // SALARIEDEMPLOYEE_H