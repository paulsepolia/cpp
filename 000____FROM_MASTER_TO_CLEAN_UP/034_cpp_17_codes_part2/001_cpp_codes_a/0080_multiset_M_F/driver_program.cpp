//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/10/30              //
// Functions: set                //
// Algorithms: erase             //
//===============================//

#include <iostream>
#include <set>

using namespace std;

// main

int main ()
{
    // variables declaration

    long int I_MAX = 2 * static_cast<long int>(10000000);
    multiset<double, less<double> > S1;
    multiset<double, less<double> > S2;
    long int i;
    long int j;
    const long int J_MAX = 100;

    for (j = 0; j < J_MAX; j++) {
        cout << "----------------------------------------------------------> " << j + 1 << endl;

        // build the 1st set

        cout << " 1 --> Build the S1 set" << endl;

        for (i = 0; i < I_MAX; i++) {
            S1.insert(static_cast<double>(i));
        }

        // build the 2nd set

        cout << " 2 --> Build the S2 set" << endl;

        for (i = 0; i < I_MAX; i++) {
            S2.insert(static_cast<double>(I_MAX-1-i));
        }

        // get the size

        cout << " 3 --> Get the size. Should be equal to : " << I_MAX << endl;

        cout << " ---------> " << S1.size() << endl;
        cout << " ---------> " << S2.size() << endl;

        // compare the sets

        cout << " 4 --> Compare the sets, should be equal: ";

        if (S1 == S2) {
            cout << " Equal sets" << endl;
        }

        // erase

        cout << " 5 --> Erase element by element the set S1" << endl;

        for (i = 0; i < I_MAX; i++) {
            S1.erase(static_cast<double>(i));
        }

        cout << " 6 --> Erase element by element the set S2" << endl;

        for (i = 0; i < I_MAX; i++) {
            S2.erase(static_cast<double>(i));
        }

        // compare the sets again

        cout << " 7 --> Compare the sets, should be equal: ";

        if (S1 == S2) {
            cout << " Equal sets" << endl;
        }

        // get the size

        cout << " 8 --> Get the size after the manual erase" << endl;

        cout << " ---------> " << S1.size() << endl;
        cout << " ---------> " << S2.size() << endl;

        // free RAM

        cout << " 9 --> Free up the RAM" << endl;

        S1.clear();
        S2.clear();
    }

    return 0;
}

// end
