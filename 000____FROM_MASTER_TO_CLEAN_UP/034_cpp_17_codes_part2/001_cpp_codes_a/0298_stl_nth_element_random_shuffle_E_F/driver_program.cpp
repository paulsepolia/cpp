//==========================//
// STL, nth_element example //
//==========================//

#include <iostream>
#include <algorithm>
#include <vector>
#include <deque>
#include <iomanip>
#include <cmath>
#include <ctime>

using std::cout;
using std::cin;
using std::endl;

using std::random_shuffle;
using std::nth_element;

using std::vector;
using std::deque;

using std::fixed;
using std::showpoint;
using std::showpos;
using std::setprecision;

using std::pow;

// a function

template <class T>
bool compA(const T& i, const T& j)
{
    return (i < j);
}

// a function object

template <class T>
class compA_CL {
public:
    bool operator()(const T& i, const T& j)
    {
        return (i < j);
    }
};

// the main function

int main()
{
    // local variables and parameters

    const long long DIM_MAX = static_cast<long long>(pow(10.0, 8.0));
    const long long K_MAX = static_cast<long long>(pow(10.0, 5.0));
    const long long N_ELEM = 10000LL;
    clock_t t1;
    clock_t t2;

    // adjust the output

    cout << fixed;
    cout << setprecision(20);
    cout << showpos;
    cout << showpoint;

    // for loop

    for (long long k = 0; k != K_MAX; k++) {
        // the counter

        cout << "------------------------------------------------->> " << k << endl;

        vector<double> * vA = new vector<double>;
        deque<double> * dA = new deque<double>;
        double * aA = new double [DIM_MAX];

        //
        //
        // build the vector

        cout << " --> build the vector" << endl;

        t1 = clock();

        for (long long i = 0; i != DIM_MAX; i++) {
            vA->push_back(cos(static_cast<double>(i)));
        }

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // shuffle the vector

        cout << " --> suffle the vector" << endl;

        t1 = clock();

        random_shuffle(vA->begin(), vA->end());

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // using default comparison (operator <)

        cout << " --> nth_element using default comparison (operator <)" << endl;

        t1 = clock();

        nth_element(vA->begin(), vA->begin()+N_ELEM, vA->end());

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // using function as comp

        cout << " --> nth_element using compA<double>" << endl;

        t1 = clock();

        nth_element(vA->begin(), vA->begin()+N_ELEM, vA->end(), compA<double>);

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // using function object as comp

        t1 = clock();

        cout << " --> nth_element using compA_CL<double>()" << endl;

        nth_element(vA->begin(), vA->begin()+N_ELEM, vA->end(), compA_CL<double>());

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // print out some contents

        cout << " --> (*vA)[0]        = " << (*vA)[0] << endl;
        cout << " --> (*vA)[1]        = " << (*vA)[1] << endl;
        cout << " --> (*vA)[2]        = " << (*vA)[2] << endl;
        cout << " --> (*vA)[3]        = " << (*vA)[3] << endl;
        cout << " --> (*vA)[4]        = " << (*vA)[4] << endl;
        cout << " --> (*vA)[N_ELEM-1] = " << (*vA)[N_ELEM-1] << endl;

        //
        //
        // build the deque

        cout << " --> build the deque" << endl;

        t1 = clock();

        for (long long i = 0; i != DIM_MAX; i++) {
            dA->push_back(cos(static_cast<double>(i)));
        }

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // shuffle the deque

        cout << " --> shuffle the deque" << endl;

        t1 = clock();

        random_shuffle(dA->begin(), dA->end());

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // using default comparison (operator <)

        cout << " --> nth_element using default comparison (operator <)" << endl;

        t1 = clock();

        nth_element(dA->begin(), dA->begin()+N_ELEM, dA->end());

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // using function as comp

        cout << " --> nth_element using compA<double>" << endl;

        t1 = clock();

        nth_element(dA->begin(), dA->begin()+N_ELEM, dA->end(), compA<double>);

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // using function object as comp

        cout << " --> nth_element using compA_CL<double>()" << endl;

        t1 = clock();

        nth_element(dA->begin(), dA->begin()+N_ELEM, dA->end(), compA_CL<double>());

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // print out some contents

        cout << " --> (*dA)[0]        = " << (*dA)[0] << endl;
        cout << " --> (*dA)[1]        = " << (*dA)[1] << endl;
        cout << " --> (*dA)[2]        = " << (*dA)[2] << endl;
        cout << " --> (*dA)[3]        = " << (*dA)[3] << endl;
        cout << " --> (*dA)[4]        = " << (*dA)[4] << endl;
        cout << " --> (*dA)[N_ELEM-1] = " << (*dA)[N_ELEM-1] << endl;

        //
        //
        // build the array

        cout << " --> build the array" << endl;

        t1 = clock();

        for (long long i = 0; i != DIM_MAX; i++) {
            aA[i] = cos(static_cast<double>(i));
        }

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // shuffle the array

        cout << " --> shuffle the array" << endl;

        t1 = clock();

        random_shuffle(&aA[0], &aA[DIM_MAX]);

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // using default comparison (operator <)

        cout << " --> nth_element using default comparison (operator <)" << endl;

        t1 = clock();

        nth_element(&aA[0], &aA[0]+N_ELEM, &aA[DIM_MAX]);

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // using function as comp

        cout << " --> nth_element using compA<double>" << endl;

        t1 = clock();

        nth_element(&aA[0], &aA[0]+N_ELEM, &aA[DIM_MAX], compA<double>);

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // using function object as comp

        cout << " --> nth_element using compA_CL<double>()" << endl;

        t1 = clock();

        nth_element(&aA[0], &aA[0]+N_ELEM, &aA[DIM_MAX], compA_CL<double>());

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // print out some contents

        cout << " --> aA[0]        = " << aA[0] << endl;
        cout << " --> aA[1]        = " << aA[1] << endl;
        cout << " --> aA[2]        = " << aA[2] << endl;
        cout << " --> aA[3]        = " << aA[3] << endl;
        cout << " --> aA[4]        = " << aA[4] << endl;
        cout << " --> aA[N_ELEM-1] = " << aA[N_ELEM-1] << endl;

        // delete the containers

        cout << " --> delete the array" << endl;

        t1 = clock();

        delete [] aA;

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        cout << " --> delete the vector" << endl;

        t1 = clock();

        delete vA;

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        cout << " --> delete the deque" << endl;

        t1 = clock();

        delete dA;

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;
    }

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

