
//================//
// driver program //
//================//

#include <iostream>
#include <cmath>
#include <iomanip>
#include <cstdlib>
#include "base.h"
#include "derivedA.h"

using std::cout;
using std::endl;
using std::cin;
using std::pow;
using std::setprecision;
using std::fixed;
using std::showpos;
using std::showpoint;
using std::boolalpha;
using std::exit;

using pgg::Base;
using pgg::DerivedA;

// main function

int main()
{
    // set the output format

    cout << fixed;
    cout << setprecision(18);
    cout << showpos;
    cout << showpoint;
    cout << boolalpha;

    // local parameters and variables

    const long K_MAX = static_cast<long>(pow(10.0, 7.0));
    const long DIM_MAX = static_cast<long>(pow(10.0, 7.5));

    for (long k = 0; k < K_MAX; k++) {

        cout << "---------------------------------------------------------------->> " << k << endl;

        // A1 object

        Base A1;

        // allocate A1

        cout << "  1 --> allocate the vector: A1.allocate(DIM_MAX);" << endl << endl;

        A1.allocate(DIM_MAX);

        // get the dimension of A1

        cout << "  2 --> get the dimension of the vector: A1.getDim() = " << A1.getDim() << endl << endl;

        // build the A1

        cout << "  3 --> build the vector element by element" << endl << endl;

        for (long i = 0; i < DIM_MAX; i++) {
            A1.setElement(cos(static_cast<double>(i)), i);
        }

        // test the build process

        cout << "  4 --> get few elements of the vector A1" << endl << endl;

        cout << "        A1.getElement(0) = " << A1.getElement(0) << endl << endl;
        cout << "        cos(0)           = " << cos(static_cast<double>(0)) << endl << endl;
        cout << "        A1.getElement(1) = " << A1.getElement(1) << endl << endl;
        cout << "        cos(1)           = " << cos(static_cast<double>(1)) << endl << endl;
        cout << "        A1.getElement(2) = " << A1.getElement(2) << endl << endl;
        cout << "        cos(2)           = " << cos(static_cast<double>(2)) << endl << endl;

        // set A2 equal to A1 using the copy constructor

        cout << "  5 --> use the copy constructor: Base A2(A1);" << endl << endl;

        Base A2(A1);

        // get the dimension of A2

        cout << "  6 --> get the dimension of the vector: A2.getDim() = " << A2.getDim() << endl << endl;

        // test the build process

        cout << "  7 --> get few elements of the vector A2" << endl << endl;

        cout << "        A1.getElement(0) = " << A1.getElement(0) << endl << endl;
        cout << "        A2.getElement(0) = " << A2.getElement(0) << endl << endl;
        cout << "        A1.getElement(1) = " << A1.getElement(1) << endl << endl;
        cout << "        A2.getElement(1) = " << A2.getElement(1) << endl << endl;
        cout << "        A1.getElement(2) = " << A1.getElement(2) << endl << endl;
        cout << "        A2.getElement(2) = " << A2.getElement(2) << endl << endl;

        // test for equality via isEqual member function

        cout << "  8 --> test for equality via isEqual member function. Should give --> true"
             << endl << endl;

        cout << "        A2.isEqual(A1) = " << A2.isEqual(A1) << endl << endl;

        // test for equality via == member function

        cout << "  9 --> test for equality via == member function. Should give --> true" << endl << endl;

        cout << "        (A2 == A1) = " << (A2 == A1) << endl << endl;

        // test for less-than via < member function

        cout << " 10 --> test for equality via < member function. Should give --> false" << endl << endl;

        cout << "        (A2 < A1) = " << (A2 < A1) << endl << endl;

        // test for less-than-or-equal via <= member function

        cout << " 11 --> test for equality via <= member function. Should give --> true" << endl << endl;

        cout << "        (A2 <= A1) = " << (A2 <= A1) << endl << endl;

        // test for greater-than via > member function

        cout << " 12 --> test for equality via > member function. Should give --> false" << endl << endl;

        cout << "        (A2 > A1) = " << (A2 > A1) << endl << endl;

        // test for greater-than-or-equal via >= member function

        cout << " 13 --> test for equality via >= member function. Should give --> true" << endl << endl;

        cout << "        (A2 >= A1) = " << (A2 >= A1) << endl << endl;

        // test for inequality via != member function

        cout << " 14 --> test for equality via == member function. Should give --> false"
             << endl << endl;

        cout << "        (A2 != A1) = " << (A2 != A1) << endl << endl;

        // A3 object

        Base A3(10.0, DIM_MAX);

        // add A1 and A2 via add member function

        cout << " 15 --> add A1 and A2 via add member function: A3.add(A1, A2);" << endl << endl;

        A3.add(A1, A2);

        // get the dimension of A3

        cout << " 16 --> get the dimension of the vector: A3.getDim() = "
             << A3.getDim() << endl << endl;

        // test the addition

        for (long i = 0; i != DIM_MAX; i++) {
            if (A3.getElement(i) != (A1.getElement(i) + A2.getElement(i))) {
                cout << "Error! Enter an integer to exit: ";
                int sentinel;
                cin >> sentinel;
                exit(-1);
            }
        }

        cout << "    --> add member functions works fine!" << endl << endl;

        // multiply A1 and A2 via add member function

        cout << " 17 --> multiply A1 and A2 via multiply member function: A3.multiply(A1, A2);"
             << endl << endl;

        A3.multiply(A1, A2);

        // test the multiplication

        for (long i = 0; i != DIM_MAX; i++) {
            if (A3.getElement(i) != (A1.getElement(i) * A2.getElement(i))) {
                cout << "Error! Enter an integer to exit: ";
                int sentinel;
                cin >> sentinel;
                exit(-1);
            }
        }

        cout << "    --> multiply member functions works fine!" << endl << endl;

        // subtract A1 and A2 via subtract member function

        cout << " 18 --> subtract A2 from A1 via subtract member function: A3.subtract(A1, A2);"
             << endl << endl;

        A3.subtract(A1, A2);

        // test the subtraction

        for (long i = 0; i != DIM_MAX; i++) {
            if (A3.getElement(i) != (A1.getElement(i) - A2.getElement(i))) {
                cout << "Error! Enter an integer to exit: ";
                int sentinel;
                cin >> sentinel;
                exit(-1);
            }
        }

        cout << "    --> subtract member functions works fine!" << endl << endl;

        // divide A1 and A2 via divide member function

        cout << " 19 --> divide A1 with A2 via divide member function: A3.divide(A1, A2);"
             << endl << endl;

        A3.divide(A1, A2);

        // test the divide member function

        for (long i = 0; i != DIM_MAX; i++) {
            if (A3.getElement(i) != (A1.getElement(i) / A2.getElement(i))) {
                cout << "Error! Enter an integer to exit: ";
                int sentinel;
                cin >> sentinel;
                exit(-1);
            }
        }

        cout << "    --> divide member functions works fine!" << endl << endl;

        Base A4;

        A3.destroy();
        A4.destroy();

        A3 = (A1 + A2);
        A4.add(A1, A2);

        cout << " 20 --> add == + ==> (A3 == A4) = " << (A3 == A4) << endl << endl;

        A3.destroy();
        A4.destroy();

        A3 = A1 - A2;
        A4.subtract(A1, A2);

        cout << " 21 --> subtract == - ==> (A3 == A4) = " << (A3 == A4) << endl << endl;

        A3.destroy();
        A4.destroy();

        A3 = A1 * A2;
        A4.multiply(A1, A2);

        cout << " 22 --> multiply == * ==> (A3 == A4) = " << (A3 == A4) << endl << endl;

        cout << "    --> A1[1] = " << A1[1] << endl;
        cout << "    --> A2[1] = " << A2[1] << endl;
        cout << "    --> A1[2] = " << A1[2] << endl;
        cout << "    --> A2[2] = " << A2[2] << endl;
        cout << "    --> A3[1] = " << A3[1] << endl;
        cout << "    --> A4[1] = " << A4[1] << endl;
        cout << "    --> A3[2] = " << A3[2] << endl;
        cout << "    --> A4[2] = " << A4[2] << endl << endl;

        A3.destroy();
        A4.destroy();

        A3 = A1 / A2;
        A4.divide(A1, A2);

        cout << " 23 --> divide == / ==> (A3 == A4) = " << (A3 == A4) << endl << endl;

        cout << "    ==> (++A3   == ++A4)   = " << (++A3 == ++A4) << endl << endl;
        cout << "    ==> (--A3   == --A4)   = " << (--A3 == --A4) << endl << endl;
        cout << "    ==> ((A3++) == (A4++)) = " << ((A3++) == (A4++)) << endl << endl;
        cout << "    ==> ((A3--) == (A4--)) = " << ((A3--) == (A4--)) << endl << endl;

        cout << "    --> A1[1] = " << A1[1] << endl;
        cout << "    --> A2[1] = " << A2[1] << endl;
        cout << "    --> A1[2] = " << A1[2] << endl;
        cout << "    --> A2[2] = " << A2[2] << endl;
        cout << "    --> A3[1] = " << A3[1] << endl;
        cout << "    --> A4[1] = " << A4[1] << endl;
        cout << "    --> A3[2] = " << A3[2] << endl;
        cout << "    --> A4[2] = " << A4[2] << endl << endl;

        A1.destroy();
        A2.destroy();
        A3.destroy();
        A4.destroy();

        A1 = static_cast<long>(DIM_MAX);
        A1 = 100.0;

        cout << "    --> A1.getElement(1) = " << A1.getElement(1) << endl << endl;

        cout << "    --> A1.getElement(DIM_MAX-1) = " << A1.getElement(DIM_MAX-1) << endl << endl;

        A2 = static_cast<long>(DIM_MAX+1);
        A2 = 111.1;

        cout << "    --> A2.getElement(1) = " << A2.getElement(1) << endl << endl;

        cout << "    --> A2.getElement(DIM_MAX) = " << A2.getElement(DIM_MAX) << endl << endl;

        A3 = static_cast<long>(DIM_MAX+1);
        A3 = 222.2;

        cout << "    --> A3.getElement(1) = " << A3.getElement(1) << endl << endl;

        cout << "    --> A3.getElement(DIM_MAX) = " << A3.getElement(DIM_MAX) << endl << endl;

        cout << " 24 --> play with derived class here" << endl << endl;

        DerivedA D1;

        cout << " 25 --> DerivedA D1; --> D1.getDim() = " << D1.getDim() << endl << endl;

        D1.setDim(DIM_MAX);

        cout << " 26 --> DerivedA D1; --> D1.setDim(DIM_MAX); D1.getDim() = "
             << D1.getDim() << endl << endl;

        DerivedA D2(10.0, DIM_MAX);

        cout << " 27 --> DerivedA D2(10.0, DIM_MAX); --> D2.getDim() = "
             << D2.getDim() << endl << endl;

        cout << " 28 --> DerivedA D2(10.0, DIM_MAX); --> D2.getElement(1) = "
             << D2.getElement(1) << endl << endl;

        cout << " 29 --> DerivedA D2(10.0, DIM_MAX); --> D2.getElement(2) = "
             << D2.getElement(2) << endl << endl;

        DerivedA D3(D2);

        cout << " 30 --> DerivedA D3(D2); --> D3.getDim() = " << D3.getDim() << endl << endl;

        cout << " 31 --> DerivedA D3(D2); --> D3.getElement(1) = " << D3.getElement(1) << endl << endl;

        cout << " 32 --> DerivedA D3(D2); --> D3.getElement(2) = " << D3.getElement(2) << endl << endl;

        cout << " 33 --> D1.destroy();" << endl << endl;

        D1.destroy();
        D1.destroy();

        cout << " 34 --> D2.destroy();" << endl << endl;

        D2.destroy();

        cout << " 35 --> D3.destroy();" << endl << endl;

        D3.destroy();
        D3.destroy();

        cout << " 35 --> D3.destroy();" << endl << endl;

        //

        cout << " 36 --> D1.allocate(DIM_MAX);" << endl << endl;

        D1.allocate(DIM_MAX);

        cout << " 37 --> D2.allocate(DIM_MAX);" << endl << endl;

        D2.allocate(DIM_MAX);

        cout << " 38 --> D3.allocate(DIM_MAX);" << endl << endl;

        D3.allocate(DIM_MAX);

        //

        cout << " 39 --> build D1 using D1.setElement(value)" << endl << endl;

        for (int i = 0; i < DIM_MAX; i++) {
            D1.setElement(cos(static_cast<double>(i)), i);
        }

        cout << " 39 --> build D2 ==> D2 = D1;" << endl << endl;

        D2 = D1;

        cout << " 40 --> (D2 == D1) = " << (D2 == D1) << endl << endl;

        cout << " 41 --> build D3 ==> D3 = D1 + D2;" << endl << endl;

        D3 = D1 + D2;

        cout << " 42 --> (D3 == D1 + D2) = " << (D3 == D1 + D2) << endl << endl;

        D3 = D1 * D2;

        cout << " 43 --> (D3 == D1 * D2) = " << (D3 == D1 * D2) << endl << endl;

        D3 = D1 - D2;

        cout << " 44 --> (D3 == D1 - D2) = " << (D3 == D1 - D2) << endl << endl;

        D3 = D1 / D2;

        cout << " 45 --> (D3 == D1 / D2) = " << (D3 == D1 / D2) << endl << endl;

        cout << "        getElement.D1(1) = " << D1.getElement(1) << endl << endl;

        cout << "                   D1[1] = " << D1[1] << endl << endl;

        D1++;

        cout << "        getElement.D1(1) = " << D1.getElement(1) << endl << endl;

        cout << "                   D1[1] = " << D1[1] << endl << endl;

        D1 = -D1;

        cout << "        getElement.D1(1) = " << D1.getElement(1) << endl << endl;

        cout << "                   D1[1] = " << D1[1] << endl << endl;

        //

        cout << " 46 --> D1.createBackup() = " << endl << endl;

        D1.createBackup();

        cout << "    --> D1.getElement(10) = " << D1.getElement(10) << endl << endl;

        D2 = D1;

        cout << " 47 --> D2.createBackup() = " << endl << endl;

        D2.createBackup();

        cout << "    --> D2.getElement(10) = " << D2.getElement(10) << endl << endl;

        D3 = D2;

        cout << " 48 --> D3.createBackup() = " << endl << endl;

        D3.createBackup();

        cout << "    --> D3.getElement(10) = " << D3.getElement(10) << endl << endl;

        //

        cout << "    --> D1++; D1++;" << endl;

        D1++;
        D1++;

        D1.getBackup();

        cout << " 49 --> D1.getBackup(); = " << endl << endl;

        cout << "    --> D1.getElement(10) = " << D1.getElement(10) << endl << endl;

        cout << "    --> D2++; D2++;" << endl;

        D2++;
        D2++;

        D2.getBackup();

        cout << " 50 --> D2.getBackup(); = " << endl << endl;

        cout << "    --> D2.getElement(10) = " << D2.getElement(10) << endl << endl;

        cout << "    --> D3++; D3++;" << endl;

        D3++;
        D3++;

        D3.getBackup();

        cout << " 51 --> D3.getBackup(); = " << endl << endl;

        cout << "    --> D3.getElement(10) = " << D3.getElement(10) << endl << endl;

        cout << " 52 --> D1.destroyBackup();" << endl << endl;

        D1.destroyBackup();

        cout << "    --> D1.getElement(10) = " << D1.getElement(10) << endl << endl;

        cout << " 53 --> D2.destroyBackup();" << endl << endl;

        D2.destroyBackup();

        cout << "    --> D2.getElement(10) = " << D2.getElement(10) << endl << endl;

        cout << " 54 --> D3.destroyBackup();" << endl << endl;

        D3.destroyBackup();

        cout << "    --> D3.getElement(10) = " << D3.getElement(10) << endl << endl;

        cout << " 55 --> D1.destroy();" << endl << endl;

        D1.destroy();
        D1.destroy();
        D1.destroy();

        cout << " 56 --> D2.destroy();" << endl << endl;

        D2.destroy();
        D2.destroy();

        cout << " 57 --> D3.destroy();" << endl << endl;

        D3.destroy();
        D3.destroy();
        D3.destroy();

        D3 = static_cast<long>(DIM_MAX);
        D3 = 333.3;

        cout << "  D3.getElement(10) = " << D3.getElement(10) << endl;

        // final

        cout << " XX --> FINAL statement" << endl<< endl;
    }

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
