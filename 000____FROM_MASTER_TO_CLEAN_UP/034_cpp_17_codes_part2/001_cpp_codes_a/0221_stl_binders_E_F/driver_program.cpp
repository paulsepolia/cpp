
//=================//
// bind1st example //
//=================//

#include <iostream>
#include <functional>
#include <algorithm>
#include <vector>
#include <cmath>
#include <iomanip>

using std::endl;
using std::cout;
using std::cin;
using std::count_if;
using std::equal_to;
using std::greater;
using std::less;
using std::vector;
using std::pow;
using std::fixed;
using std::setprecision;
using std::showpos;
using std::showpoint;
using std::remove_if;

// the main function

int main ()
{
    // local variables and parameters

    const long DIMEN_MAX = 2 * static_cast<long>(pow(10.0, 1.0));
    vector<double> vA;
    long cx;
    vector<double>::iterator endp;
    vector<double>::iterator it;
    long i;

    // set the output format

    cout << fixed;
    cout << setprecision(20);
    cout << showpos;
    cout << showpoint;

    // build the vector

    for (long i = 0; i < DIMEN_MAX; i++) {
        vA.push_back(cos(static_cast<double>(i)));
    }

    // apply the bind1st binder function

    cx = 0;
    cx = count_if( vA.begin(), vA.end(), bind1st(equal_to<double>(), cos(10.0)) );

    // output the results

    cout << "There are --> " << cx
         << " --> elements that are equal to cos(10.0) = "
         << cos(10.0) << endl;

    // apply the bind2nd binder function

    endp = remove_if( vA.begin(), vA.end(), bind2nd(greater<double>(), 0.001) );

    // output the new vector

    it = vA.begin();
    i = 0;
    while(it != endp) {
        i++;
        cout << i << " --> " << *it << endl;
        it++;
    }

    // shrink it to its actual size

    vA.shrink_to_fit();

    cout << vA.size() << endl;

    // apply the bind2nd binder function

    endp = remove_if( vA.begin(), vA.end(), bind2nd(less<double>(), -0.001) );

    // display the new vector

    it = vA.begin();
    i = 0;
    while(it != endp) {
        i++;
        cout << i << " --> " << *it << endl;
        it++;
    }

    // shrink it to its actual size

    vA.shrink_to_fit();

    cout << vA.size() << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

