//=============================//
// Header File.                //
// MatrixCL Class Declaration. //
//=============================//

template <typename T, typename P>
class MatrixCL {
public:

    MatrixCL();                                        // constructor
    ~MatrixCL();                                       // destructor

    //========================//
    // Member functions here. //
    //========================//

    void CreateF(P, P);                                         // create manually the vector
    void DeleteF();                                             // delete manually the matrix
    void SetElementF(P, P, const T&);                           // set the vector element by element
    T GetElementF(P, P) const;                                  // get the ith element of the vector
    void SetEqualF(const MatrixCL<T,P>&);                       // assignment operator
    void SetEqualZeroF();                                       // set to zero
    void SetEqualNumberF(const T&);                             // set to a specific number
    void AddF(const MatrixCL<T,P>&, const MatrixCL<T,P>&);      // add matrices
    void SubtractF(const MatrixCL<T,P>&, const MatrixCL<T,P>&); // subtract matrices
    void TimesF(const MatrixCL<T,P>&, const MatrixCL<T,P>&);    // multiply matrices element-by-element
    void DotF(const MatrixCL<T,P>&, const MatrixCL<T,P>&);      // dot matrices
    T TotalF();                                                 // TotalF

    //========================//
    // Friend functions here. //
    //========================//

    // EMPTY

    //============================//
    // Overloaded operators here. //
    //============================//

    //  1. '=' operator.
    MatrixCL<T,P>& operator=(const MatrixCL<T,P>&);

    //  2. '==' operator.
    bool operator==(const MatrixCL<T,P>&);

private:

    T** m_Mat;  // pointer to the [0][0] element of the matrix
    P m_DimA;   // help variable
    P m_DimB;   // help variable
};

//==============//
// End of code. //
//==============//
