//====================//
// hourlyemployee.cpp //
//====================//

///========================================================//
// This is the file hourlyemployee.cpp                     //
// This is the implementation for the class HourlyEmployee //
//=========================================================//

#include <string>
#include <iostream>
#include "hourlyemployee.h"

using std::string;
using std::cout;
using std::endl;

namespace pgg {
HourlyEmployee::HourlyEmployee() : Employee(), wageRate(0.0), hours(0) {}

HourlyEmployee::HourlyEmployee(string theName, string theNumber,
                               double theWageRate, double theHours) : Employee(theName, theNumber),
    wageRate(theWageRate), hours(theHours) {}

void HourlyEmployee::setRate(double newWageRate)
{
    wageRate = newWageRate;
}

double HourlyEmployee::getRate() const
{
    return wageRate;
}

void HourlyEmployee::setHours(double hoursWorked)
{
    hours = hoursWorked;
}

double HourlyEmployee::getHours() const
{
    return hours;
}

void HourlyEmployee::printCheck()
{
    setNetPay(hours * wageRate);

    cout << "\n---------------------------------------------------\n";
    cout << "Pay to the order of " << getName() << endl;
    cout << "The sum of " << getNetPay() << " Dollars\n";
    cout << "-----------------------------------------------------\n";
    cout << "Check stub: NOT NEGOTIABLE\n";
    cout << "Employee Number: " << getSsn() << endl;
    cout << "Hourly Employee. \nHours worked: " << hours
         << " Rate: " << wageRate << " Pay: " << getNetPay() << endl;
    cout << "-----------------------------------------------------\n";
}
} // pgg

//======//
// FINI //
//======//