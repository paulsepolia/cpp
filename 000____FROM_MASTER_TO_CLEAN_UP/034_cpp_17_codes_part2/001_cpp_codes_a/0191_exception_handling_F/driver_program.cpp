
// using a user-defined exception class

#include <iostream>

using std::cout;
using std::cin;
using std::endl;

// the class

class divByZero {
};

// main function

int main()
{
    int dividend;
    int divisor;
    int quotient;

    divByZero div1;

    try {
        cout << "Line 3: Enter the dividend: ";
        cin >> dividend;
        cout << endl;

        cout << "Line 6: Enter the divisor: ";
        cin >> divisor;
        cout << endl;

        if (divisor == 0) {
            throw div1;
        }

        // rest of code
    } catch (divByZero) {
        cout << "Line 14: Division by zero!" << endl;
    }

    try {
        cout << "Line 3: Enter the dividend: ";
        cin >> dividend;
        cout << endl;

        cout << "Line 6: Enter the divisor: ";
        cin >> divisor;
        cout << endl;

        if (divisor == 0) {
            throw divByZero();
        }

        // rest of code
    } catch (divByZero) {
        cout << "Line 14: Division by zero!" << endl;
    }

    int sentinel;
    cin >> sentinel;

    return 0;

}

//======//
// FINI //
//======//