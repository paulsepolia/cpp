
//=========================================//
// declaration of the class TitledEmployee //
//=========================================//

#ifndef TITLEDEMPLOYEE_H
#define TITLEDEMPLOYEE_H

#include <string>
#include "salariedemployee.h"

using std::string;

namespace pgg {
class TitledEmployee : public SalariedEmployee {
public:

    // default constructor

    TitledEmployee();

    // constructor with three arguments

    TitledEmployee(string, string, string, double);

    // member functions

    string getTitle() const;
    void setTitle(string);
    void setName(string); // redefinition of the base class function

private:

    string title;
};


} // pgg

#endif // TITLEDEMPLOYEE_H

//======//
// FINI //
//======//
