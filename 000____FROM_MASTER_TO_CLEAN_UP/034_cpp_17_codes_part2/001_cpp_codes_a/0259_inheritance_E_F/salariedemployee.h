
//===========================================//
// declaration of the class SalariedEmployee //
//===========================================//

#ifndef SALARIEDEMPLOYEE_H
#define SALARIEDEMPLOYEE_H

#include <string>
#include "employee.h"

using std::string;

namespace pgg {
class SalariedEmployee : public Employee {
public:

    // default constructor

    SalariedEmployee();

    // constructor with three arguments

    SalariedEmployee(string, string, double);

    // member functions

    double getSalary() const;
    void setSalary(double);
    void printCheck(); // redefinition of the base class function

private:

    double salary;
};


} // pgg

#endif // SALARIEDEMPLOYEE_H

//======//
// FINI //
//======//
