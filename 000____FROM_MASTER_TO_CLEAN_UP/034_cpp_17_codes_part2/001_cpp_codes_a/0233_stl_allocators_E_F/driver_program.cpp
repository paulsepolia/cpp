
//=============================================//
// demonstrate allocator's max_size() function //
//=============================================//

#include <iostream>
#include <vector>
#include <list>
#include <deque>
#include <iomanip>

using std::endl;
using std::cout;
using std::cin;
using std::vector;
using std::list;
using std::deque;
using std::fixed;
using std::setprecision;
using std::showpos;
using std::showpoint;

// the main function

int main()
{
    // local variables and parameters

    // vector related allocator type max_size

    vector<short int>::allocator_type VEC_si_a;
    vector<int>::allocator_type VEC_i_a;
    vector<long int>::allocator_type VEC_li_a;
    vector<long long int>::allocator_type VEC_lli_a;
    vector<float>::allocator_type VEC_f_a;
    vector<double>::allocator_type VEC_d_a;
    vector<long double>::allocator_type VEC_ld_a;

    // list related allocator type max_size

    list<short int>::allocator_type LIST_si_a;
    list<int>::allocator_type LIST_i_a;
    list<long int>::allocator_type LIST_li_a;
    list<long long int>::allocator_type LIST_lli_a;
    list<float>::allocator_type LIST_f_a;
    list<double>::allocator_type LIST_d_a;
    list<long double>::allocator_type LIST_ld_a;

    // deque related allocator type max_size

    deque<short int>::allocator_type DEQUE_si_a;
    deque<int>::allocator_type DEQUE_i_a;
    deque<long int>::allocator_type DEQUE_li_a;
    deque<long long int>::allocator_type DEQUE_lli_a;
    deque<float>::allocator_type DEQUE_f_a;
    deque<double>::allocator_type DEQUE_d_a;
    deque<long double>::allocator_type DEQUE_ld_a;

    // set the output format

    cout << fixed;
    cout << setprecision(20);
    cout << showpos;
    cout << showpoint;

    // display output --> vector

    cout << endl;
    cout << " --> Here are the number of the objects in a VECTOR that can be allocated:" << endl;
    cout << endl;
    cout << " --> short integers     --> " << VEC_si_a.max_size() << endl;
    cout << " --> integers           --> " <<  VEC_i_a.max_size() << endl;
    cout << " --> long integers      --> " << VEC_li_a.max_size() << endl;
    cout << " --> long long integers --> " << VEC_lli_a.max_size() << endl;
    cout << " --> float              --> " << VEC_f_a.max_size() << endl;
    cout << " --> double             --> " << VEC_d_a.max_size() << endl;
    cout << " --> long double        --> " << VEC_ld_a.max_size() << endl;
    cout << endl;

    // display output --> list

    cout << endl;
    cout << " --> Here are the number of the objects in a LIST that can be allocated:" << endl;
    cout << endl;
    cout << " --> short integers     --> " << LIST_si_a.max_size() << endl;
    cout << " --> integers           --> " <<  LIST_i_a.max_size() << endl;
    cout << " --> long integers      --> " << LIST_li_a.max_size() << endl;
    cout << " --> long long integers --> " << LIST_lli_a.max_size() << endl;
    cout << " --> float              --> " << LIST_f_a.max_size() << endl;
    cout << " --> double             --> " << LIST_d_a.max_size() << endl;
    cout << " --> long double        --> " << LIST_ld_a.max_size() << endl;
    cout << endl;

    // display output --> deque

    cout << endl;
    cout << " --> Here are the number of the objects in a DEQUE that can be allocated:" << endl;
    cout << endl;
    cout << " --> short integers     --> " << DEQUE_si_a.max_size() << endl;
    cout << " --> integers           --> " << DEQUE_i_a.max_size() << endl;
    cout << " --> long integers      --> " << DEQUE_li_a.max_size() << endl;
    cout << " --> long long integers --> " << DEQUE_lli_a.max_size() << endl;
    cout << " --> float              --> " << DEQUE_f_a.max_size() << endl;
    cout << " --> double             --> " << DEQUE_d_a.max_size() << endl;
    cout << " --> long double        --> " << DEQUE_ld_a.max_size() << endl;
    cout << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

