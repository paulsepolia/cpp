
//======================================//
// STL, sort, stable_sort, partial_sort //
//======================================//

#include <vector>
#include <algorithm>
#include <iostream>
#include <iterator>

using std::endl;
using std::cout;
using std::cin;

using std::sort;
using std::partial_sort;
using std::stable_sort;
using std::reverse;

using std::vector;
using std::ostream_iterator;

// function object

template <class T>
class comp_last {
public:

    bool operator()(T x, T y)
    {
        return (x%10 < y%10);
    }
};

// the main function

int main()
{
    cout << "Illustrating the generic sort, stable_sort,"
         << " and partial_sort algorithm." << endl;

    const int N = 20;

    vector<int> V0;

    for (int i = 0; i != N; i++) {
        V0.push_back(i);
    }

    vector<int> V1 = V0;

    ostream_iterator<int> out(cout, " ");

    cout << "Before sorting: " << endl;
    copy(V1.begin(), V1.end(), out);
    cout << endl;

    sort(V1.begin(), V1.end(), comp_last<int>());

    cout << "After sorting by last digits with sort: " << endl;
    copy(V1.begin(), V1.end(), out);
    cout << endl << endl;

    V1 = V0;

    cout << "Before sorting: " << endl;
    copy(V1.begin(), V1.end(), out);
    cout << endl << endl;

    stable_sort(V1.begin(), V1.end(), comp_last<int>());

    cout << "After sorting by last digits with stable_sort: " << endl;
    copy(V1.begin(), V1.end(), out);
    cout << endl << endl;

    V1 = V0;

    reverse(V1.begin(), V1.end());

    cout << "Before sorting: " << endl;
    copy(V1.begin(), V1.end(), out);
    cout << endl << endl;

    partial_sort(V1.begin(), V1.begin()+5, V1.end(), comp_last<int>());

    cout << "After sorting with parial_sort to get 5 values: " << endl;
    copy(V1.begin(), V1.end(), out);
    cout << endl << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

