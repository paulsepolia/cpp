
#ifndef PACKET_FUNCTIONS_H
#define PACKET_FUNCTIONS_H

//====================//
// packet_functions.h //
//====================//

#include <string>
#include "packet.h"

using std::string;

// default constructor

packet::packet() : pnum (-1), info ("") {}

// constructor

packet::packet(int n, string i) : pnum (n), info(i) {}

// member function

int packet::getpnum() const
{
    return pnum;
}

// member function

string packet::getinfo() const
{
    return info;
}

// external function
// defines operator < () for the class packet
// compare two packets for less than

bool operator < (const packet & a, const packet & b)
{
    return (a.getpnum() < b.getpnum());
}

// external function
// defines operator == () for the class packet
// compare two packets for equality

bool operator == (const packet & a, const packet & b)
{
    return (a.getpnum() == b.getpnum());
}

// external function
// obtain next packet
// returns true if packet available
// and false otherwise

bool getpacket(packet & pkt)
{
    packet message[] = {
        packet(2, "is"),
        packet(4, "Programming"),
        packet(0, "The"),
        packet(3, "Power"),
        packet(1, "STL"),
        packet(-1, "") // end of transmission mark
    };

    static int i = 0;

    if (message[i] == packet(-1, "")) {
        return false;
    }

    pkt = message[i];

    i++;

    return true;
}

//======//
// FINI //
//======//

#endif // PACKET_FUNCTIONS_H

