
//==========================//
// STL, transform algorithm //
//==========================//

#include <iostream>
#include <algorithm>
#include <vector>
#include <functional>
#include <cmath>
#include <iomanip>

using std::cout;
using std::cin;
using std::endl;
using std::transform;
using std::vector;
using std::plus;
using std::multiplies;
using std::pow;
using std::fixed;
using std::setprecision;
using std::showpos;
using std::showpoint;

// function --> 0

template <class T>
T sin_pgg(T i)
{
    return sin(i);
}

// function --> 1

template <class T>
T increase_pgg(T i)
{
    return ++i;
}

// function --> 2

template <class T>
T add_pgg(const T i, const T j)
{
    return i+j;
}

// function --> 3

template <class T>
T subtract_pgg(const T i, const T j)
{
    return i-j;
}

// function object --> 0

template <class T>
class sin_pgg_CL {
public:
    T operator()(T i)
    {
        return sin(i);
    }
};

// function object --> 1

template <class T>
class increase_pgg_CL {
public:
    T operator()(T i)
    {
        return ++i;
    }
};

// function object --> 2

template <class T>
class add_pgg_CL {
public:
    T operator()(const T i, const T j)
    {
        return (i+j);
    }
};

// function object --> 3

template <class T>
class subtract_pgg_CL {
public:
    T operator()(const T i, const T j)
    {
        return (i-j);
    }
};

// the main function

int main ()
{
    // local variables and parameters

    const long long DIM_MAX = static_cast<long long>(pow(10.0, 8.0));
    const long long K_MAX = static_cast<long long>(pow(10.0, 6.0));
    long long i;

    // set the output format

    cout << fixed;
    cout << showpos;
    cout << showpoint;
    cout << setprecision(20);

    // main for loop

    for(long long k = 0; k != K_MAX; k++) {
        // the counter

        cout << "------------------------------------------------>> " << k << endl;

        // local variables

        vector<double> * VA = new vector<double>;
        vector<double> * VB = new vector<double>;

        // build *VA

        for (i = 0; i != DIM_MAX; i++) {
            VA->push_back(cos(static_cast<double>(i)));
        }

        // allocate space for *VB

        VB->resize(VA->size());

        // apply transform

        transform(VA->begin(), VA->end(), VB->begin(), increase_pgg<double>);

        cout << " --> (*VA)[0] = " << (*VA)[0] << endl;
        cout << " --> (*VB)[0] = " << (*VB)[0] << endl;
        cout << " --> (*VA)[1] = " << (*VA)[1] << endl;
        cout << " --> (*VB)[1] = " << (*VB)[1] << endl;
        cout << " --> (*VA)[2] = " << (*VA)[2] << endl;
        cout << " --> (*VB)[2] = " << (*VB)[2] << endl;

        // apply transform

        cout << endl;

        transform(VA->begin(), VA->end(), VB->begin(), increase_pgg_CL<double>());

        cout << " --> (*VA)[0] = " << (*VA)[0] << endl;
        cout << " --> (*VB)[0] = " << (*VB)[0] << endl;
        cout << " --> (*VA)[1] = " << (*VA)[1] << endl;
        cout << " --> (*VB)[1] = " << (*VB)[1] << endl;
        cout << " --> (*VA)[2] = " << (*VA)[2] << endl;
        cout << " --> (*VB)[2] = " << (*VB)[2] << endl;

        // apply copy

        cout << endl;

        copy(VA->begin(), VA->end(), VB->begin());

        cout << " --> (*VA)[0] = " << (*VA)[0] << endl;
        cout << " --> (*VB)[0] = " << (*VB)[0] << endl;
        cout << " --> (*VA)[1] = " << (*VA)[1] << endl;
        cout << " --> (*VB)[1] = " << (*VB)[1] << endl;
        cout << " --> (*VA)[2] = " << (*VA)[2] << endl;
        cout << " --> (*VB)[2] = " << (*VB)[2] << endl;

        // apply transform

        cout << endl;

        transform(VA->begin(), VA->end(), VB->begin(), VA->begin(), subtract_pgg<double>);

        cout << " --> (*VA)[0] = " << (*VA)[0] << endl;
        cout << " --> (*VA)[1] = " << (*VA)[1] << endl;
        cout << " --> (*VA)[2] = " << (*VA)[2] << endl;

        // apply transform

        cout << endl;

        transform(VA->begin(), VA->end(), VB->begin(), VA->begin(), subtract_pgg_CL<double>());

        cout << " --> (*VA)[0] = " << (*VA)[0] << endl;
        cout << " --> (*VA)[1] = " << (*VA)[1] << endl;
        cout << " --> (*VA)[2] = " << (*VA)[2] << endl;

        // apply transform

        cout << endl;

        transform(VA->begin(), VA->end(), VA->begin(), sin_pgg<double>);

        cout << " --> (*VA)[0] = " << (*VA)[0] << endl;
        cout << " --> (*VA)[1] = " << (*VA)[1] << endl;
        cout << " --> (*VA)[2] = " << (*VA)[2] << endl;

        // apply transform

        cout << endl;

        transform(VA->begin(), VA->end(), VA->begin(), sin_pgg_CL<double>());

        cout << " --> (*VA)[0] = " << (*VA)[0] << endl;
        cout << " --> (*VA)[1] = " << (*VA)[1] << endl;
        cout << " --> (*VA)[2] = " << (*VA)[2] << endl;

        // apply transform

        cout << endl;

        transform(VA->begin(), VA->end(), VA->begin(), sin_pgg<double>);

        cout << " --> (*VA)[0] = " << (*VA)[0] << endl;
        cout << " --> (*VA)[1] = " << (*VA)[1] << endl;
        cout << " --> (*VA)[2] = " << (*VA)[2] << endl;

        // apply transform

        cout << endl;

        transform(VA->begin(), VA->end(), VA->begin(), sin_pgg_CL<double>());

        cout << " --> (*VA)[0] = " << (*VA)[0] << endl;
        cout << " --> (*VA)[1] = " << (*VA)[1] << endl;
        cout << " --> (*VA)[2] = " << (*VA)[2] << endl;

        // apply transform

        cout << endl;

        transform(VA->begin(), VA->end(), VA->begin(), sin_pgg<double>);

        cout << " --> (*VA)[0] = " << (*VA)[0] << endl;
        cout << " --> (*VA)[1] = " << (*VA)[1] << endl;
        cout << " --> (*VA)[2] = " << (*VA)[2] << endl;

        // apply transform

        cout << endl;

        transform(VA->begin(), VA->end(), VA->begin(), sin_pgg_CL<double>());

        cout << " --> (*VA)[0] = " << (*VA)[0] << endl;
        cout << " --> (*VA)[1] = " << (*VA)[1] << endl;
        cout << " --> (*VA)[2] = " << (*VA)[2] << endl;

        // apply transform

        cout << endl;

        transform(VA->begin(), VA->end(), VA->begin(), sin_pgg<double>);

        cout << " --> (*VA)[0] = " << (*VA)[0] << endl;
        cout << " --> (*VA)[1] = " << (*VA)[1] << endl;
        cout << " --> (*VA)[2] = " << (*VA)[2] << endl;

        // apply transform

        cout << endl;

        transform(VA->begin(), VA->end(), VA->begin(), sin_pgg_CL<double>());

        cout << " --> (*VA)[0] = " << (*VA)[0] << endl;
        cout << " --> (*VA)[1] = " << (*VA)[1] << endl;
        cout << " --> (*VA)[2] = " << (*VA)[2] << endl;

        // apply transform

        for (int j = 0; j != 100; j++) {
            cout << endl;

            transform(VA->begin(), VA->end(), VA->begin(), sin_pgg_CL<double>());

            cout << " --> (*VA)[0] = " << (*VA)[0] << endl;
            cout << " --> (*VA)[1] = " << (*VA)[1] << endl;
            cout << " --> (*VA)[2] = " << (*VA)[2] << endl;
        }

        //

        cout << endl;

        // delete the vectors

        delete VA;
        delete VB;
    }

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

