//============//
// accumulate //
//============//

#include <iostream>
#include <functional>
#include <numeric>
#include <iomanip>

// using

using std::accumulate;
using std::cin;
using std::cout;
using std::endl;
using std::minus;
using std::multiplies;
using std::fixed;
using std::setprecision;
using std::noshowpos;
using std::showpoint;

// a function

int myMinus (int x, int y)
{
    return x-y;
}

// a function

int myfunction (int x, int y)
{
    return x+2*y;
}

// a class

class myclass {
public:
    int operator()(int x, int y)
    {
        return x+3*y;
    }
} myobject;

// the main function

int main ()
{
    // data

    int init = 100;
    int numbers[] = {10, 20, 30};

    cout << fixed;
    cout << showpoint;
    cout << setprecision(5);

    // default accumulate

    cout << "using default accumulate: ";
    cout << accumulate(numbers, numbers+3, init);
    cout << endl;

    // accumulate and functional's minus

    cout << "using functional's minus: ";
    cout << accumulate (numbers, numbers+3, init, minus<int>());
    cout << endl;

    // accumulate and myMinus

    cout << "using myMinus: ";
    cout << accumulate (numbers, numbers+3, init, myMinus);
    cout << endl;


    // accumulate and myfunction

    cout << "using custom function: ";
    cout << accumulate (numbers, numbers+3, init, myfunction);
    cout << endl;

    // accumulate and myobject

    cout << "using custom object: ";
    cout << accumulate (numbers, numbers+3, init, myobject);
    cout << endl;

    // accumulate and multiplies

    cout << "using custom object: ";
    cout << accumulate (numbers, numbers+3, init, multiplies<int>());
    cout << endl;

    // the sentinel

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
