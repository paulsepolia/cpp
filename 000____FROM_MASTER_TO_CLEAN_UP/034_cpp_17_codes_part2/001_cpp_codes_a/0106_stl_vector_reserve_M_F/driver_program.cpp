
#include <iostream>
#include <cmath>
#include <vector>

using namespace std;

int main()
{
    vector<double> vecA;
    long int i;
    const long int I_MAX_DO = static_cast<long int>(pow(10.0, 8.0));
    double tmpA;
    const long int K_MAX_DO = 1000000L;
    long int k;

    // --> 1 --> building

    for (k = 0; k < K_MAX_DO; k++) {
        cout << "---------------------------------------------------------->> " << k << endl;
        cout << "  1 -->> building and resizing at the same time --> SLOW PROCESS" << endl;

        vecA.clear();
        vecA.shrink_to_fit();

        for (i = 1; i <= I_MAX_DO; i++) {
            tmpA = static_cast<double>(i);
            vecA.push_back(tmpA);
        }

        cout << "  vecA[1] = " << vecA[1] << endl;

        // --> 2 --> clean the vector container

        cout << "  2 -->> clean the vector container --> FAST" << endl;

        vecA.clear();
        vecA.shrink_to_fit();

        // --> 3 --> reserve space

        cout << "  3 -->> building having reserved first the needed space --> FAST PROCESS" << endl;

        vecA.reserve(I_MAX_DO);

        for (i = 1; i <= I_MAX_DO; i++) {
            tmpA = static_cast<double>(i);
            vecA.push_back(tmpA);
        }

        cout << "  vecA[1] = " << vecA[1] << endl;

        // --> 4 --> clean the vector container

        cout << "  4 -->> clean the vector container --> FAST" << endl;

        vecA.clear();
        vecA.shrink_to_fit();

    }

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
