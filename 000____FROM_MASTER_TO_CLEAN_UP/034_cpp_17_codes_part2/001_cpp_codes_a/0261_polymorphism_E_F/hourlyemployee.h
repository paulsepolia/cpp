
//=========================================//
// declaration of the class HourlyEmployee //
//=========================================//

#ifndef HOURLYEMPLOYEE_H
#define HOURLYEMPLOYEE_H

#include <string>
#include "employee.h"

using std::string;

namespace pgg {
class HourlyEmployee : public Employee {
public:

    // default constructor

    HourlyEmployee();

    // constructor with four arguments

    HourlyEmployee(string, string, double, double);

    // member functions

    void setRate(double);
    double getRate() const;
    void setHours(double);
    double getHours() const;
    void printCheck(); // redefinition of the base class function

private:

    double wageRate;
    double hours;
};


} // pgg

#endif // HOURLYEMPLOYEE_H

//======//
// FINI //
//======//

