
//==========================================//
// definition of the class SalariedEmployee //
//==========================================//

#include <string>
#include <iostream>
#include "salariedemployee.h"

using std::string;
using std::cout;
using std::endl;

namespace pgg {
// default constructor

SalariedEmployee::SalariedEmployee() : Employee(), salary(0) {}

// constructor with three arguments

SalariedEmployee::SalariedEmployee(string theName, string theNumber, double theWeeklyPay)
    : Employee(theName, theNumber), salary(theWeeklyPay) {}

// member function

double SalariedEmployee::getSalary() const
{
    return salary;
}

// member function

void SalariedEmployee::setSalary(double newSalary)
{
    salary = newSalary;
}

// member function

void SalariedEmployee::printCheck() // redefinition and drop the const qualifier
{
    setNetPay(salary);

    cout << "______________________________________________________________" << endl;
    cout << "Pay to the order of " << getName() << endl;
    cout << "The sum of " << getNetPay() << " Dollars" << endl;
    cout << "______________________________________________________________" << endl;
    cout << "Check Stub: NOT NEGOTIABLE" << endl;
    cout << "Employee Number: " << getSsn() << endl;
    cout << "Salaried Employee. Regular Pay: " << salary << endl;
    cout << "______________________________________________________________" << endl;
}

} // pgg

