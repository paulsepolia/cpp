//==========//
// STL: set //
//==========//

#include <iostream>
#include <set>
#include <iomanip>
#include <ctime>
#include <cmath>
#include <ctime>

int main ()
{
    const long int DIM_A = static_cast<long int>(pow(10.0, 7.5));
    const long int K_MAX = static_cast<long int>(pow(10.0, 5.0));
    const long int I_MAX = DIM_A;
    long int i;
    long int k;
    clock_t t1;
    clock_t t2;
    long int counterLoc = 0;

    // set the outpot format

    std::cout << std::fixed;
    std::cout << std::setprecision(5);
    std::cout << std::showpoint;

    for (k = 0 ; k < K_MAX; k++) {
        // counter

        std::cout << "-------------------------------------------------->> " << k << std::endl;

        //=================//
        // LONG INTS --> 1 //
        //=================//

        // build the array of long ints

        std::cout << " -->  1 --> build the dynamic array (long ints) --> arrayAL1" << std::endl;
        t1 = clock();
        long int* arrayAL1 = new long int [DIM_A];
        for (i = 0; i < DIM_A; i++) {
            arrayAL1[i] = DIM_A - i;
        }
        t2 = clock();
        std::cout << " --> done --> " << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;

        // declare the set

        std::set<long int> *LSetA1 = new std::set<long int> [1];

        // fill the set with the elements of the array

        t1 = clock();
        std::cout
                << " -->  2 --> build the set (long ints) --> (*LSetA1).insert(arrayAL1, arrayAL1+DIM_A);"
                << std::endl;
        (*LSetA1).insert(arrayAL1, arrayAL1+DIM_A);
        t2 = clock();
        std::cout << " --> done --> " << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
        std::cout << " --> size of *LSetA1 = (*LSetA1).size() = " << (*LSetA1).size() << std::endl;

        // delete the set

        t1 = clock();
        std::cout << " -->  3 --> delete the set (long ints)--> delete [] LSetA1;" << std::endl;
        delete [] LSetA1;
        t2 = clock();
        std::cout << " --> done --> " << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;

        // delete the array

        t1 = clock();
        std::cout << " -->  4 --> delete the array (long ints) --> delete [] arrayAL1;" << std::endl;
        delete [] arrayAL1;
        t2 = clock();
        std::cout << " --> done --> " << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;

        //===============//
        // DOUBLES --> 1 //
        //===============//

        // build the array of doubles

        std::cout << " -->  5 --> build the dynamic array (doubles) --> arrayAD1" << std::endl;
        t1 = clock();
        double* arrayAD1 = new double [DIM_A];
        for (i = 0; i < DIM_A; i++) {
            arrayAD1[i] = static_cast<double>(DIM_A - i);
        }
        t2 = clock();
        std::cout << " --> done --> " << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;

        // declare the set

        std::set<double> *DSetA1 = new std::set<double> [1];

        // fill the set with the elements of the array

        t1 = clock();
        std::cout
                << " -->  6 --> build the set (doubles) --> (*DSetA1).insert(arrayAD1, arrayAD+DIM_A);"
                << std::endl;
        (*DSetA1).insert(arrayAD1, arrayAD1+DIM_A);
        t2 = clock();
        std::cout << " --> done --> " << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
        std::cout << " --> size of *DSetA1 = (*DSetA1).size() = " << (*DSetA1).size() << std::endl;

        // delete the set

        t1 = clock();
        std::cout << " -->  7 --> delete the set (doubles) --> delete [] DSetA1;" << std::endl;
        delete [] DSetA1;
        t2 = clock();
        std::cout << " --> done --> " << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;

        // delete the array

        t1 = clock();
        std::cout << " -->  8 --> delete the array (doubles) --> delete [] arrayAD1;" << std::endl;
        delete [] arrayAD1;
        t2 = clock();
        std::cout << " --> done --> " << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;

        //===============//
        // DOUBLES --> 2 //
        //===============//

        // build the array of doubles

        std::cout << " -->  9 --> build the dynamic array (doubles) --> arrayAD2" << std::endl;
        t1 = clock();
        double* arrayAD2 = new double [DIM_A];
        for (i = 0; i < DIM_A; i++) {
            arrayAD2[i] = cos(static_cast<double>(i));
        }
        t2 = clock();
        std::cout << " --> done --> " << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;

        // declare the set

        std::set<double> *DSetA2 = new std::set<double> [1];

        // fill the set with the elements of the array

        t1 = clock();
        std::cout
                << " --> 10 --> build the set (doubles) --> (*DSetA2).insert(arrayAD2, arrayAD2+DIM_A);"
                << std::endl;
        (*DSetA2).insert(arrayAD2, arrayAD2+DIM_A);
        t2 = clock();
        std::cout << " --> done --> " << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
        std::cout << " --> size of *DSetA2 = (*DSetA2).size() = " << (*DSetA2).size() << std::endl;

        // delete the set

        t1 = clock();
        std::cout << " --> 11 --> delete the set (doubles) --> delete [] DSetA2;" << std::endl;
        delete [] DSetA2;
        t2 = clock();
        std::cout << " --> done --> " << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;

        // delete the array

        t1 = clock();
        std::cout << " --> 12 --> delete the array (doubles) --> delete [] arrayAD2;" << std::endl;
        delete [] arrayAD2;
        t2 = clock();
        std::cout << " --> done --> " << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;

        //===============//
        // DOUBLES --> 3 //
        //===============//

        // declare the set

        std::set<double> *DSetA3 = new std::set<double> [1];

        // fill the set with some elements using insert

        t1 = clock();
        std::cout << " --> 13 --> build the set (doubles) --> (*DSetA3).insert(double);" << std::endl;
        for(i = 0; i < I_MAX; i++) {
            (*DSetA3).insert(cos(static_cast<double>(i)));
        }
        t2 = clock();
        std::cout << " --> done --> " << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
        std::cout << " --> size of *DSetA3 = (*DSetA3).size() = " << (*DSetA3).size() << std::endl;

        // erasing the set element by element

        t1 = clock();
        std::cout << " --> 14 --> erasing the set element by element (doubles) using:" << std::endl;
        std::cout << " --> (*DSetA3).erase((*DSetA3).find(cos(static_cast<double>(i))));" << std::endl;
        for(i = 0; i < I_MAX; i++) {
            (*DSetA3).erase((*DSetA3).find(cos(static_cast<double>(i))));
        }
        t2 = clock();
        std::cout << " --> done --> " << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
        std::cout << " --> size of *DSetA3 = (*DSetA3).size() = " << (*DSetA3).size() << std::endl;

        // delete the set

        t1 = clock();
        std::cout << " --> 15 --> delete the set (doubles) --> delete [] DSetA3;" << std::endl;
        delete [] DSetA3;
        t2 = clock();
        std::cout << " --> done --> " << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;

        //===============//
        // DOUBLES --> 4 //
        //===============//

        // declare the set

        std::set<double> *DSetA4 = new std::set<double> [1];

        // fill the set with some elements using emplace

        t1 = clock();
        std::cout << " --> 16 --> build the set (doubles) --> (*DSetA4).emplace(double);" << std::endl;
        for(i = 0; i < I_MAX; i++)
            //{ (*DSetA4).emplace(cos(static_cast<double>(i))); }
        {
            (*DSetA4).insert(cos(static_cast<double>(i)));
        }
        t2 = clock();
        std::cout << " --> done --> " << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
        std::cout << " --> size of *DSetA4 = (*DSetA4).size() = " << (*DSetA4).size() << std::endl;

        // erasing the set element by element

        t1 = clock();
        std::cout << " --> 17 --> erasing the set element by element (doubles) using:" << std::endl;
        std::cout << " --> (*DSetA4).erase((*DSetA4).find(cos(static_cast<double>(i))));" << std::endl;
        for(i = 0; i < I_MAX; i++) {
            (*DSetA4).erase((*DSetA4).find(cos(static_cast<double>(i))));
        }
        t2 = clock();
        std::cout << " --> done --> " << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
        std::cout << " --> size of *DSetA4 = (*DSetA4).size() = " << (*DSetA4).size() << std::endl;

        // delete the set

        t1 = clock();
        std::cout << " --> 18 --> delete the set (doubles) --> delete [] DSetA4;" << std::endl;
        delete [] DSetA4;
        t2 = clock();
        std::cout << " --> done --> " << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;

        //===============//
        // DOUBLES --> 5 //
        //===============//

        // declare the set

        std::set<double> *DSetA5 = new std::set<double> [1];

        // fill the set with some elements using emplace

        t1 = clock();
        std::cout << " --> 19 --> build the set (doubles) --> (*DSetA5).emplace(double);" << std::endl;
        for(i = 0; i < I_MAX; i++)
            //{ (*DSetA5).emplace(cos(static_cast<double>(i))); }
        {
            (*DSetA5).insert(cos(static_cast<double>(i)));
        }
        t2 = clock();
        std::cout << " --> done --> " << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
        std::cout << " --> size of *DSetA5 = (*DSetA5).size() = " << (*DSetA5).size() << std::endl;

        // count how many elements are there (if all are there)

        t1 = clock();
        std::cout << " --> 20 --> count how many elements are in the set;" << std::endl;
        for(i = 0; i < I_MAX; i++) {
            if( (*DSetA5).count(cos(static_cast<double>(i))) != 0) {
                counterLoc = counterLoc + 1L;
            } else {
                std::cout << " --> WARNING: The compiler is doing non-safe optimizations :(" << std::endl;
            }
        }
        t2 = clock();

        std::cout << " --> done --> " << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
        std::cout << " --> I found --> " << counterLoc << " --> elements!" << std::endl;

        // erasing the set

        t1 = clock();
        std::cout << " --> 21 --> erasing the set using clear()" << std::endl;
        (*DSetA5).clear();
        t2 = clock();
        std::cout << " --> done --> " << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
        std::cout << " --> size of *DSetA5 = (*DSetA5).size() = " << (*DSetA5).size() << std::endl;

        // delete the set

        t1 = clock();
        std::cout << " --> 22 --> delete the set (doubles) --> delete [] DSetA5;" << std::endl;
        delete [] DSetA4;
        t2 = clock();
        std::cout << " --> done --> " << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
    }

    return 0;
}

//======//
// FINI //
//======//
