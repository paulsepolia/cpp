
//=========================//
// insert_iterator example //
//=========================//

#include <iostream>
#include <iterator>
#include <list>
#include <algorithm>

using std::endl;
using std::cout;
using std::cin;
using std::insert_iterator;
using std::list;
using std::copy;

// the main function

int main()
{
    list<int> foo;
    list<int> bar;
    list<int>::iterator it;

    // build foo

    for (int i = 1; i <= 5; i++) {
        foo.push_back(i);
    }

    // build bar

    for (int i = 1; i <= 5; i++) {
        bar.push_back(i*10);
    }

    // get the beggining of the foo list

    it = foo.begin();

    // advance the iterator 3 positions

    advance(it, 3);

    // define insert_iterator

    insert_iterator<list<int> > insert_it(foo, it);

    // copy bar list to 3rd position and beyond of foo list

    copy(bar.begin(), bar.end(), insert_it);

    //

    cout << " --> foo: ";

    for (it = foo.begin(); it!= foo.end(); it++) {
        cout << " " << *it;
    }

    cout << endl;

    cout << " --> bar: ";

    for (it = bar.begin(); it != bar.end(); it++) {
        cout << " " << *it;
    }

    cout << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

