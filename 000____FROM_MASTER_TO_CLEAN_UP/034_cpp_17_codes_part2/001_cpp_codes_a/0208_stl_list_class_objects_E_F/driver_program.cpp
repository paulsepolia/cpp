
//=====================================//
// using a list ot store class objects //
//=====================================//

#include <iostream>
#include <list>
#include <string>

using std::cin;
using std::cout;
using std::endl;
using std::list;
using std::string;

// the class

class mailList {
public:

    mailList() : name (""), street (""), city (""), state(""), zip("") {}

    mailList(string n, string s, string c, string st, string z) : name (n), street (s), city (c),
        state(st), zip(z)  {}

    string getname()
    {
        return name;
    }
    string getcity()
    {
        return city;
    }
    string getstreet()
    {
        return street;
    }
    string getstate()
    {
        return state;
    }
    string getzip()
    {
        return zip;
    }

private:

    string name;
    string street;
    string city;
    string state;
    string zip;
};

// < operator for class objects mailList

bool operator < (mailList &a, mailList &b)
{
    return a.getname() < b.getname();
}

// == operator for class objects mailList

bool operator == (mailList &a, mailList &b)
{
    return a.getname() == b.getname();
}


// a function to display list on screen

void display(list<mailList> &lst)
{
    list<mailList>::iterator p;

    for (p = lst.begin(); p != lst.end(); p++) {
        cout << p -> getname() << ": ";
        cout << p -> getstreet() << ", ";
        cout << p -> getcity() << ", ";
        cout << p -> getstate() << " ";
        cout << p -> getzip() << endl;
    }
}

// main function

int main()
{
    list<mailList> mlstA;
    list<mailList> mlstB;

    // the lists

    mlstA.push_back(mailList("A5", "1111", "M", "TX", "11111"));
    mlstA.push_back(mailList("A4", "1111", "M", "TX", "11112"));
    mlstA.push_back(mailList("A3", "1111", "M", "TX", "11113"));
    mlstA.push_back(mailList("A2", "1111", "M", "TX", "11114"));
    mlstA.push_back(mailList("A1", "1111", "M", "TX", "11115"));

    mlstB.push_back(mailList("A5", "1111", "M", "TX", "11111"));
    mlstB.push_back(mailList("B3", "1111", "M", "TX", "11112"));
    mlstB.push_back(mailList("B2", "1111", "M", "TX", "11113"));
    mlstB.push_back(mailList("A2", "1111", "M", "TX", "11114"));
    mlstB.push_back(mailList("A1", "1111", "M", "TX", "11115"));

    // display lists

    cout << "----------------------------> mlstA" << endl;

    display(mlstA);

    cout << "----------------------------> mlstB" << endl;

    display(mlstB);

    // sort the lists

    mlstA.sort();
    mlstB.sort();

    // display lists

    cout << "----------------------------> mlstA sorted" << endl;

    display(mlstA);

    cout << "----------------------------> mlstB sortred" << endl;

    display(mlstB);

    // merge mailing lists

    mlstA.merge(mlstB);

    cout << "----------------------------> mlstA sorted and merged" << endl;

    display(mlstA);

    cout << "----------------------------> mlstB sorted and merged" << endl;

    display(mlstB); // should be empty

    // remove duplicates

    mlstA.unique();

    cout << "----------------------------> mlstA sorted, merged and removing duplicates" << endl;

    display(mlstA);

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
