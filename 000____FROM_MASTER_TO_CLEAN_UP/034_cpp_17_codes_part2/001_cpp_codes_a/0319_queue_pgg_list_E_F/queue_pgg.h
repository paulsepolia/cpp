
//====================================//
// declaration of the class queue_pgg //
//====================================//

#ifndef QUEUE_PGG_H
#define QUEUE_PGG_H

#include <list>
#include <cstdlib>

using std::list;
using std::size_t;

template <typename T>
class queue_pgg {
public:

    queue_pgg<T>(); // default constructor
    ~queue_pgg<T>(); // destructor
    queue_pgg<T>(const queue_pgg<T> &); // copy constructor

    // member functions

    bool empty() const;
    size_t size() const;
    T & front();
    T & back();
    void push(const T &);
    void pop();
    void swap(queue_pgg<T> &);
    void emplace(const T &);

    // operators overload

    queue_pgg<T> & operator = (const queue_pgg<T> &); // assignment operator

private:

    list<T> elems;
};

#endif // deque_PGG_H

//======//
// FINI //
//======//

