
//=============================//
// superbase class declaration //
//=============================//

// a pure abstact class works as an interface

#ifndef SUPERBASE_H
#define SUPERBASE_H

namespace pgg {
class Superbase {
public:

    // member functions

    virtual void allocate(long) = 0;
    virtual long getDim() const = 0;
    virtual double getElement(long) const = 0;
    virtual void setDim(long) = 0;
    virtual void setElement(double, long) = 0;
    virtual void setEqual(const Superbase&) = 0;
    virtual bool isEqual(const Superbase&) const = 0;
    virtual void add(const Superbase&, const Superbase&) = 0;
    virtual void subtract(const Superbase&, const Superbase&) = 0;
    virtual void multiply(const Superbase&, const Superbase&) = 0;
    virtual void divide(const Superbase&, const Superbase&) = 0;
    virtual double magnitude() const = 0;
    virtual void destroy() = 0;
};

} // pgg

#endif // SUPERBASE_H

//======//
// FINI //
//======//
