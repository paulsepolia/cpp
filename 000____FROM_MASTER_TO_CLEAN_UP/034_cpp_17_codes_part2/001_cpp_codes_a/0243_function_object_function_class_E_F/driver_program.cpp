
//=================//
// function object //
//=================//

#include <iostream>

using std::endl;
using std::cout;
using std::cin;

// the class to define the function object sq()

// details:

// sq is a function class
// sq() is a function object
// sq()(5) is a function call

template <class T>
class sq {
public:
    T operator () (T x) const
    {
        return x * x;
    }
};

// the main function

int main()
{
    cout << " 5   * 5   = " << sq<int>()(5) << endl;
    cout << " 6.0 * 6.0 = " << sq<double>()(6.0) << endl;

    sq<int> funObInt;
    sq<double> funObDouble;

    cout << " 7   * 7   = " << funObInt(7) << endl;
    cout << " 8.0 * 8.0 = " << funObDouble(8.0) << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;
}

//======//
// FINI //
//======//
