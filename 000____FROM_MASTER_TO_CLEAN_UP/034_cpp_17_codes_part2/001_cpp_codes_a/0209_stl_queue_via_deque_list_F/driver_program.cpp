
//===============//
// queue adaptor //
//===============//

#include <iostream>
#include <queue>
#include <deque>
#include <list>
#include <cmath>

using std::endl;
using std::cout;
using std::cin;
using std::list;
using std::deque;
using std::queue;
using std::pow;

// the main function

int main()
{
    const long DIMEN_MAX = static_cast<long>(pow(10.0, 7.8));
    const long K_MAX = static_cast<long>(pow(10.0, 5.0));

    for(long k = 0; k < K_MAX; k++) {
        // counter

        cout << "------------------------------------------------>> " << k << endl;

        // declare the adaptors

        queue<double> * qA = new queue<double>;
        queue<double, deque<double>> * qB = new queue<double, deque<double>>;
        queue<double, list<double>> * qC = new queue<double, list<double>>;

        // build the containers

        cout << " --> build --> default queue" << endl;

        for (long i = 0; i < DIMEN_MAX; i++) {
            qA->push(static_cast<double>(i));
        }

        cout << " --> build --> queue via deque" << endl;

        for (long i = 0; i < DIMEN_MAX; i++) {
            qB->push(static_cast<double>(i));
        }

        cout << " --> build --> queue via list" << endl;

        for (long i = 0; i < DIMEN_MAX; i++) {
            qC->push(static_cast<double>(i));
        }

        // pop the containers

        cout << " --> pop --> default queue" << endl;

        for (long i = 0; i < DIMEN_MAX; i++) {
            qA->pop();
        }

        cout << " --> pop --> queue via deque" << endl;

        for (long i = 0; i < DIMEN_MAX; i++) {
            qB->pop();
        }

        cout << " --> pop --> queue via list" << endl;

        for (long i = 0; i < DIMEN_MAX; i++) {
            qC->pop();
        }

        // free RAM

        cout << " --> delete --> default queue" << endl;

        delete qA;

        cout << " --> delete --> queue via deque" << endl;

        delete qB;

        cout << " --> delete --> queue via list" << endl;

        delete qC;
    }

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

