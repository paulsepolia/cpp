//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/10/13              //
// Functions: Vector             //
//===============================//

#include <iostream>
#include <vector>
#include <cmath>
#include <iomanip>

using namespace std;

int main()
{
    // 1. variables declaration

    const long int VEC_DIM = 10 * static_cast<long int>(10000000);
    const double ONE = 1.0;
    const long int K_MAX = 1000;
    long int i;
    long int k;

    vector<double> vecA;
    vector<double> vecB(VEC_DIM);
    vector<double> vecC(VEC_DIM,ONE);

    // 2. set the output style

    cout << showpoint;
    cout << showpos;
    cout << fixed;
    cout << setprecision(15);

    // 3. build the vecA

    for (i = 0; i < VEC_DIM; i++) {
        vecA.push_back(ONE);
    }

    // 4. some outputs

    cout << "vecA[0] = " << vecA[0] << endl;
    cout << "vecB[0] = " << vecB[0] << endl;
    cout << "vecC[0] = " << vecC[0] << endl;

    cout << "vecA[1] = " << vecA[1] << endl;
    cout << "vecB[1] = " << vecB[1] << endl;
    cout << "vecC[1] = " << vecC[1] << endl;

    // 5. delete all the element form the object

    vecA.clear();
    vecB.clear();
    vecC.clear();

    // 6. rebuild the vectors

    for (i = 0; i < VEC_DIM; i++) {
        vecA.push_back(cos(static_cast<double>(i)));
        vecB.push_back(cos(static_cast<double>(i)));
        vecC.push_back(cos(static_cast<double>(i)));
    }

    // 7. some outputs

    cout << "vecA[0] = " << vecA[0] << endl;
    cout << "vecB[0] = " << vecB[0] << endl;
    cout << "vecC[0] = " << vecC[0] << endl;

    cout << "vecA[1] = " << vecA[1] << endl;
    cout << "vecB[1] = " << vecB[1] << endl;
    cout << "vecC[1] = " << vecC[1] << endl;

    // 8. play with the vector

    for (k = 0; k < K_MAX; k++) {
        cout << " k ------------------------------------------> " << k << endl;
        cout << " 1 --> clear the vectors " << endl;

        vecA.clear();
        vecB.clear();
        vecC.clear();

        cout << " 2 --> build again the vectors" << endl;

        for (i = 0; i < VEC_DIM; i++) {
            vecA.push_back(cos(static_cast<double>(i)));
            vecB.push_back(cos(static_cast<double>(i)));
            vecC.push_back(cos(static_cast<double>(i)));
        }

        cout << " 3 --> size when full = " << vecA.size() << endl;
        cout << " 4 --> max size       = " << vecA.max_size() << endl;
        cout << " 5 --> size when full = " << vecB.size() << endl;
        cout << " 6 --> max size       = " << vecB.max_size() << endl;
        cout << " 7 --> size when full = " << vecC.size() << endl;
        cout << " 8 --> max size       = " << vecC.max_size() << endl;
        cout << " 9 --> clear element by element the vectors" << endl;

        for (i = 0; i < VEC_DIM; i++) {
            vecA.pop_back();
            vecB.pop_back();
            vecC.pop_back();
        }

        cout << "10 --> size when empty = " << vecA.size() << endl;
        cout << "11 --> max size       = " << vecA.max_size() << endl;
        cout << "12 --> size when empty = " << vecB.size() << endl;
        cout << "13 --> max size       = " << vecB.max_size() << endl;
        cout << "14 --> size when empty = " << vecC.size() << endl;
        cout << "15 --> max size       = " << vecC.max_size() << endl;
        cout << endl;
    }

    return 0;
}

//======//
// FINI //
//======//
