
//====================//
// exception handling //
//====================//

#include <iostream>
#include <string>

using std::cin;
using std::cout;
using std::endl;
using std::string;

// a class

class NegativeNumber {
public:
    NegativeNumber() {}
    NegativeNumber(string theMessage) : message(theMessage) {}
    string getMessage() const
    {
        return message;
    }

private:

    string message;
};

// a class

class DivideByZero {
};

// the main function

int main()
{
    int pencils;
    int erasers;
    double ppe;

    try {
        //

        cout << "How many pencils do you have?" << endl;
        cin >> pencils;

        if (pencils < 0) {
            throw NegativeNumber("pencils");
        }

        //

        cout << "How many erasers do you have?" << endl;
        cin >> erasers;

        if (erasers < 0) {
            throw NegativeNumber("erasers");
        }

        //

        if (erasers != 0) {
            ppe = pencils / static_cast<double>(erasers);
        } else {
            throw DivideByZero();
        }

        //

        cout << "Each eraser must last through " << ppe << " pencils." << endl;
    } catch(NegativeNumber e) {
        cout << "Cannot have a negative number of " << e.getMessage() << endl;
    } catch(DivideByZero e) {
        cout << "Do not make any mistakes!" << endl;
    } catch(...) {
        cout << "Unexplained exception!" << endl;
    }

    //
    cout << "End of program!" << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
