//================================//
// Author: Pavlos G. Galiatsatos  //
// Date: 2013/10/23               //
// Functions: vector, deque, list //
//================================//

#include <iostream>
#include <vector>
#include <deque>
#include <list>

using namespace std;

// main function
int main()
{
    vector<double> aVector;
    deque<double> aDeque;
    list<double> aList;
    long int i;
    long int j;
    const long int I_MAX = 3 * 40000000;
    const long int J_MAX = 1000;
    double tmpA;

    for (j = 0; j < J_MAX ; j++) {
        // vector: add at the end

        cout << endl;
        cout << "---------------------------------------------->>> " << j << endl;
        cout << " 1 --> Insert at the end" << endl;
        cout << " 2 --> For vector" << endl;

        tmpA = 0.0;

        for (i = 0; i < I_MAX; i++) {
            tmpA = tmpA + 1.0;
            aVector.push_back(tmpA);
        }

        cout << " 3 --> Delete at the end" << endl;
        cout << " 4 --> For vector" << endl;

        // vector: delete at the end

        for (i = 0; i < I_MAX; i++) {
            aVector.pop_back();
        }

        cout << " 5 --> Insert at the end" << endl;
        cout << " 6 --> For deque" << endl;

        // deque: add at the end

        tmpA = 0.0;

        for (i = 0; i < I_MAX; i++) {
            tmpA = tmpA + 1.0;
            aDeque.push_back(tmpA);
        }

        cout << " 7 --> Delete at the end" << endl;
        cout << " 8 --> For deque" << endl;

        // deque: delete at the end

        for (i = 0; i < I_MAX; i++) {
            aDeque.pop_back();
        }

        // list: add at the end

        cout << " 9 --> Insert at the end" << endl;
        cout << "10 --> For list" << endl;

        tmpA = 0.0;

        for (i = 0; i < I_MAX; i++) {
            tmpA = tmpA + 1.0;
            aList.push_back(tmpA);
        }

        cout << "11 --> Delete at the end" << endl;
        cout << "12 --> For list" << endl;

        // list: delete at the end

        for (i = 0; i < I_MAX; i++) {
            aList.pop_back();
        }
    }


    return 0;
}
// end
