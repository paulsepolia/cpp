//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/10/13              //
// Functions: string             //
//===============================//

#include <iostream>
#include <string>

using namespace std;

int main()
{
    string str1;
    string str2;
    string str3;
    long int i;
    const long int I_DO_MAX = 100;
    const long int I_DO_MAX2 = 100000;
    long int tmp1;
    const long int LEN_MAX = 3000000000;

    str1 = "Pavlos G. Galiatsatos";
    str2 = "75 Hamilton Road, OX27QA";

    cout << "str1 = " << str1 << endl;
    cout << "str2 = " << str2 << endl;
    cout << "str1.length() = " << str1.length() << endl;
    cout << "str2.length() = " << str2.length() << endl;

    str3 = str1 + ", " + str2;

    cout << "str3 = " << str3 << endl;
    cout << "str3.length() = " << str3.length() << endl;

    cout << "str1.size() = " << str1.size() << endl;
    cout << "str2.size() = " << str2.size() << endl;
    cout << "str3.size() = " << str3.size() << endl;

    for (i = 0; i < I_DO_MAX; i++) {
        str3.append(str1);
    }

    cout << "str3 = " << str3 << endl;
    cout << "str3.size() = " << str3.size() << endl;

    for (i = 0; i < I_DO_MAX2; i++) {
        str3.append(str3);
        tmp1 = str3.size();
        cout << i << " --> " << " str3.size() = " << tmp1 << endl;
        if (tmp1 > LEN_MAX) {
            str3 = " 1 ";
        }
    }

    return 0;
}

//======//
// FINI //
//======//
