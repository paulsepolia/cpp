
#include <iostream>
#include <iomanip>
#include "classa.h"
#include "classb.h"

using std::cout;
using std::cin;
using std::endl;
using std::setprecision;
using std::fixed;
using std::showpoint;
using std::showpos;
using std::boolalpha;
using pgg::ClassA;
using pgg::ClassB;

int main()
{
    // adjust the output

    cout << fixed;
    cout << setprecision(10);
    cout << showpos;
    cout << showpoint;
    cout << boolalpha;

    //

    ClassA A1;
    ClassB B1;

    A1.print();
    cout << A1.add() << endl;
    cout << A1.multiply() << endl;
    A1.locA();

    B1.print();
    cout << B1.add() << endl;
    cout << B1.multiply() << endl;
    B1.locB();

    cout << " ---------------->> A1 <  B1 = " << (A1 <  B1) << endl;
    cout << " ---------------->> A1 >  B1 = " << (A1 >  B1) << endl;
    cout << " ---------------->> A1 <= B1 = " << (A1 <= B1) << endl;
    cout << " ---------------->> A1 >= B1 = " << (A1 >= B1) << endl;
    cout << " ---------------->> A1 == B1 = " << (A1 == B1) << endl;
    cout << " ---------------->> A1 == A1 = " << (A1 == A1) << endl;
    cout << " ---------------->> (A1 + A1).value() = " << (A1 + A1).value() << endl;
    cout << " ---------------->> (B1 - B1).value() = " << (B1 - B1).value() << endl;
    cout << " ---------------->> (A1 * A1).value() = " << (A1 * A1).value() << endl;
    cout << " ---------------->> (B1 / B1).value() = " << (B1 / B1).value() << endl;

    //

    cout << " --> A1.value() = " << A1.value() << endl;

    A1++;
    cout << "A1++; A1.value() = " << A1.value() << endl;

    ++A1;
    cout << "++A1; A1.value() = " << A1.value() << endl;

    --A1;
    cout << "--A1; A1.value() = " << A1.value() << endl;

    A1--;
    cout << "A1--; A1.value() = " << A1.value() << endl;


    //

    cout << " --> B1.value() = " << B1.value() << endl;

    B1++;
    cout << "B1++; B1.value() = " << B1.value() << endl;

    ++B1;
    cout << "++B1; B1.value() = " << B1.value() << endl;

    --B1;
    cout << "--B1; B1.value() = " << B1.value() << endl;

    B1--;
    cout << "B1--; B1.value() = " << B1.value() << endl;

    //

    ClassA A2(10.0, 20.0);
    ClassB B2(3.0, 4.0);

    A2.print();
    cout << A2.add() << endl;
    cout << A2.multiply() << endl;
    A2.locA();

    B2.print();
    cout << B2.add() << endl;
    cout << B2.multiply() << endl;
    B2.locB();

    cout << " ---------------->> A2 <  B2 = " << (A2 <  B2) << endl;
    cout << " ---------------->> A2 >  B2 = " << (A2 >  B2) << endl;
    cout << " ---------------->> A2 <= B2 = " << (A2 <= B2) << endl;
    cout << " ---------------->> A2 >= B2 = " << (A2 >= B2) << endl;
    cout << " ---------------->> A2 == B2 = " << (A2 == B2) << endl;
    cout << " ---------------->> A2 <  A2 = " << (A2 <  A2) << endl;
    cout << " ---------------->> A2 >  A2 = " << (A2 >  A2) << endl;
    cout << " ---------------->> A2 <= A2 = " << (A2 <= A2) << endl;
    cout << " ---------------->> A2 >= A2 = " << (A2 >= A2) << endl;
    cout << " ---------------->> A2 == A2 = " << (A2 == A2) << endl;

    cout << " ---------------->> (A2 + A2).value() = " << (A2 + A2).value() << endl;
    cout << " ---------------->> (B2 - B2).value() = " << (B2 - B2).value() << endl;
    cout << " ---------------->> (A2 * A2).value() = " << (A2 * A2).value() << endl;
    cout << " ---------------->> (B2 / B2).value() = " << (B2 / B2).value() << endl;

    //

    cout << " --> A2.value() = " << A2.value() << endl;

    A2++;
    cout << "A2++; A2.value() = " << A2.value() << endl;

    ++A2;
    cout << "++A2; A2.value() = " << A2.value() << endl;

    --A2;
    cout << "--A2; A2.value() = " << A2.value() << endl;

    A2--;
    cout << "A2--; A2.value() = " << A2.value() << endl;

    //

    cout << " --> B2.value() = " << B2.value() << endl;

    B2++;
    cout << "B2++; B2.value() = " << B2.value() << endl;

    ++B2;
    cout << "++B2; B2.value() = " << B2.value() << endl;

    --B2;
    cout << "--B2; B2.value() = " << B2.value() << endl;

    B2--;
    cout << "B2--; B2.value() = " << B2.value() << endl;

    //

    ClassA A3(30.0, 2.0, 3.0);
    ClassB B3(4.0, 5.0, 6.0);

    A3.print();
    cout << A3.add() << endl;
    cout << A3.multiply() << endl;
    A3.locA();

    B3.print();
    cout << B3.add() << endl;
    cout << B3.multiply() << endl;
    B3.locB();

    cout << " ---------------->> A3 <  B3 = " << (A3 <  B3) << endl;
    cout << " ---------------->> A3 >  B3 = " << (A3 >  B3) << endl;
    cout << " ---------------->> A3 <= B3 = " << (A3 <= B3) << endl;
    cout << " ---------------->> A3 >= B3 = " << (A3 >= B3) << endl;
    cout << " ---------------->> A3 == B3 = " << (A3 == B3) << endl;
    cout << " ---------------->> (A3 + A3).value() = " << (A3 + A3).value() << endl;
    cout << " ---------------->> (B3 - B3).value() = " << (B3 - B3).value() << endl;
    cout << " ---------------->> (A3 * A3).value() = " << (A3 * A3).value() << endl;
    cout << " ---------------->> (B3 / B3).value() = " << (B3 / B3).value() << endl;

    //

    cout << " --> A3.value() = " << A3.value() << endl;

    A3++;
    cout << "A3++; A3.value() = " << A3.value() << endl;

    ++A3;
    cout << "++A3; A3.value() = " << A3.value() << endl;

    --A3;
    cout << "--A3; A3.value() = " << A3.value() << endl;

    A3--;
    cout << "A3--; A3.value() = " << A3.value() << endl;

    //

    cout << " --> B3.value() = " << B3.value() << endl;

    B3++;
    cout << "B3++; B3.value() = " << B3.value() << endl;

    ++B3;
    cout << "++B3; B3.value() = " << B3.value() << endl;

    --B3;
    cout << "--B3; B3.value() = " << B3.value() << endl;

    B3--;
    cout << "B3--; B3.value() = " << B3.value() << endl;

    A3 = A1;
    cout << " A3 == A1 ==>" << (A3 == A1) << endl;
    A3 = A2;
    cout << " A3 == A2 ==>" << (A3 == A2) << endl;
    B1 = B3;
    cout << " B1 == B3 ==>" << (B1 == B3) << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
