
//====================//
// ClassA declaration //
//====================//

#ifndef CLASS_A
#define CLASS_A

#include "ainterface.h"

namespace pgg {
class ClassA : public InterfaceA {
public:

    ClassA();
    ClassA(double, double);
    ClassA(double, double, double);
    ClassA(const ClassA&);
    virtual ~ClassA();
    virtual void print() const;
    virtual double add() const;
    virtual double multiply() const;
    void locA() const;
    virtual double value() const;

    // <, <=, >, >=, ==
    // operator overload

    virtual bool operator <  (const InterfaceA&) const;
    virtual bool operator <= (const InterfaceA&) const;
    virtual bool operator >  (const InterfaceA&) const;
    virtual bool operator >= (const InterfaceA&) const;
    virtual bool operator == (const ClassA&) const;
    virtual bool operator == (const InterfaceA&) const;

    // operator overload
    // +, -, *, /

    virtual ClassA operator +  (const ClassA&) const;
    virtual ClassA operator -  (const ClassA&) const;
    virtual ClassA operator *  (const ClassA&) const;
    virtual ClassA operator /  (const ClassA&) const;

    // operator overload
    // ++, --, -

    virtual const ClassA operator ++ ();
    virtual const ClassA operator ++ (int);
    virtual const ClassA operator -- ();
    virtual const ClassA operator -- (int);
    virtual const ClassA operator -  () const;

    // operator overload
    // +=, -=, *=, /=

    virtual const ClassA operator += (const ClassA&);
    virtual const ClassA operator -= (const ClassA&);
    virtual const ClassA operator *= (const ClassA&);
    virtual const ClassA operator /= (const ClassA&);

    // operator overload
    // =

    virtual const ClassA operator = (const ClassA&);

private:

    double a;
    double b;
    double *c;
};
}

#endif // CLASS_A

//======//
// FINI //
//======//
