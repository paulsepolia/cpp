
//=============================//
// linked list                 //
// create and delete           //
// CREATE AND DELETE BACKWARDS //
//=============================//

#include <iostream>
#include <cmath>
#include <iomanip>
#include <vector>
#include <thread>

using std::cout;
using std::cin;
using std::endl;
using std::pow;
using std::setprecision;
using std::showpos;
using std::showpoint;
using std::fixed;
using std::vector;
using std::thread;

// the node type

struct nodeType {
    long info;
    nodeType *link;
};

// the benchmark function

void benchFun ()
{
    const long I_MAX = static_cast<long>(pow(10.0, 7.1));
    const long J_MAX = static_cast<long>(pow(10.0, 8.0));

    cout << setprecision(10);
    cout << fixed;
    cout << showpos;
    cout << showpoint;

    nodeType *first;
    nodeType *newNode;

    long num = 10; // get an integer

    for (long j = 0; j < J_MAX; j++) {
        cout << "---------------------------------------->> " << j << endl;

        first = NULL; // initialize

        // build backwards

//                cout << " --> build" << endl;

        for (long i = 0; i < I_MAX; i++) {
            newNode = new nodeType;
            newNode->info = num;
            newNode->link = first;
            first = newNode;
            num = i;

        } // end for

        // delete forwards
        // only possible deletion

//              cout << " --> delete" << endl;

        nodeType *q;
        while (first->link != NULL) {
            q = first->link;
            first->link = q->link;
            delete q;
        }

    }
}

// the main function

int main()
{
    const int NUM_THREADS = 8;
    vector<thread> threadVec;

    // open threads

    for (int i = 0; i < NUM_THREADS; i++) {
        threadVec.push_back(thread(benchFun));
    }

    // join threads

    for (auto & t : threadVec) {
        t.join();
    }

    // clear the vector

    threadVec.clear();

    // sentineling

    int sentinel;
    cin >> sentinel;
}

//======//
// FINI //
//======//
