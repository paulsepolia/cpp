//======================//
// pointer to functions //
//======================//

// the code illustrates how you can pass
// an function as an agumnet using pointers
// so a pointer points to a function!

#include <iostream>

// a function

int addition(int a, int b)
{
    return (a+b);
}

// a function

int subtraction(int a, int b)
{
    return (a-b);
}

// a function and a pointer to function
// both definitions are valid

int operation(int x, int y, int functionCall(int, int))
//int operation(int x, int y, int (*functionCall)(int, int))
{
    int g;
    g = functionCall(x, y);
//  g = (*functionCall)(x,y);
    return (g);
}

// the main function

int main()
{
    int m;
    int n;

    m = operation(7, 5, addition);
    n = operation(7, 5, subtraction);

    std::cout << " m = " << m << std::endl;
    std::cout << " n = " << n << std::endl;

    int (*minus)(int, int) = &subtraction;

    m = operation(7, 5, subtraction);
    n = operation(7, 5, minus);

    std::cout << " m = " << m << std::endl;
    std::cout << " n = " << n << std::endl;

    int sentinel;
    std::cin >> sentinel;
}

//======//
// FINI //
//======//

