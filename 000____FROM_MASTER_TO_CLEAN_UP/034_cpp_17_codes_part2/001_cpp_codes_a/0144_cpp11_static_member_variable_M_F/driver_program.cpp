//===========================//
// static members in classes //
//===========================//

#include <iostream>

using std::cout;
using std::endl;
using std::cin;

// a class

class Dummy {
public:

    static int n;

    Dummy ()   // default constructor
    {
        n++;
        cout << " --> default constructor --> n = " << n << endl;
    };

    ~Dummy ()   // default destructor
    {
        n--;
        cout << " --> default destructor --> n = " << n << endl;
    };
};

// initialization ONLY ONCE AND OUTISE the class
// static member variable

int Dummy::n = 0;

// the main variable

int main ()
{
    Dummy a;


    std::cout << "  1 --> a.n = " << a.n << std::endl;
    std::cout << "  2 --> a.n = " << a.n << std::endl;

    Dummy b[500];

    std::cout << "  3 --> b[0].n = " << b[0].n << std::endl;
    std::cout << "  4 --> b[1].n = " << b[1].n << std::endl;
    std::cout << "  5 --> b[2].n = " << b[2].n << std::endl;
    std::cout << "  6 --> b[3].n = " << b[3].n << std::endl;
    std::cout << "  7 --> b[4].n = " << b[4].n << std::endl;

    Dummy * c = new Dummy;

    std::cout << "  8 --> (*c).n = " << (*c).n << std::endl;

    std::cout << "  9 --> a.n = " << a.n << std::endl;

    std::cout << " deleting a class object " << std::endl;

    delete c;

    std::cout << " 10 --> Dummy::n = " << Dummy::n << std::endl;

    // sentineling

    int sentinel;
    std::cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
