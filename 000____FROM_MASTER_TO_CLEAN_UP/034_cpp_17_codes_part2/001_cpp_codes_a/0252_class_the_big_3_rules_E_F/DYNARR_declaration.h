
#ifndef DYNARR_DECL_H
#define DYNARR_DECL_H

//==========================//
// class DYNARR declaration //
//==========================//

class DYNARR {
public:

    // constructors

    DYNARR();
    DYNARR(int);
    DYNARR(const DYNARR &);

    // member functions

    void addElement(double);
    bool full() const;
    int getCapacity() const;
    int getNumberUsed() const;
    void emptyArray();

    // [] operator overload as member function

    double & operator [] (int);

    // = operator overload as member function

    DYNARR & operator = (const DYNARR &);

    // destructor

    ~DYNARR();

private:

    double *a;    // for an array of double
    int capacity; // for the size of the array
    int used;     // for the number of array positions currently in use
};

//======//
// FINI //
//======//

#endif // DYNARR_DECL_H

