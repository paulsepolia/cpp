
//=====================================//
// function object vs function pointer //
//=====================================//

#include <iostream>
#include <iomanip>
#include <vector>
#include <cmath>
#include <ctime>
#include <algorithm>

using std::cout;
using std::cin;
using std::endl;
using std::fixed;
using std::setprecision;
using std::showpos;
using std::showpoint;
using std::vector;
using std::pow;
using std::clock;
using std::clock_t;
using std::sort;
using std::random_shuffle;

// object function

template <class T>
class less_with_count {
public:

    // default constructor

    less_with_count() : counter(0LL), progenitor(0) {}

    // copy constructor

    less_with_count(less_with_count<T>& x) : counter(0LL),
        progenitor(0 < x.progenitor ? x.progenitor : &x) {}

    // overload operator()

    bool operator() (const T& x, const T& y)
    {
        ++counter;
        return (x < y);
    }

    // member function

    long long report() const
    {
        return counter;
    }

    // destructor

    ~less_with_count()
    {
        if (progenitor != NULL) {
            progenitor->counter += counter;
        }
    }

private:
    long long counter;
    less_with_count<T> * progenitor;
};

// the main function

int main()
{
    // local variables and parameters

    const long long DIM_MAX = static_cast<long long>(pow(10.0, 8.0));
    const long long K_MAX = static_cast<long long>(pow(10.0, 5.0));
    clock_t t1;
    clock_t t2;
    less_with_count<double> comp_count_obj;

    // output format

    cout << fixed;
    cout << showpos;
    cout << showpoint;
    cout << setprecision(20);

    // for loop

    for (long long k = 0; k < K_MAX; k++) {
        // the counter

        cout << "-------------------------------------------------------->> " << k << endl;

        // local variables

        vector<double> *VA = new vector<double>;
        double * AA = new double [DIM_MAX];

        //
        // build the vector
        //

        cout << " --> build the vector" << endl;

        for (long long i = 0; i != DIM_MAX; i++) {
            VA->push_back(cos(static_cast<double>(i)));
        }

        // random shuffle

        cout << " --> random shuffle --> the vector" << endl;

        t1 = clock();

        random_shuffle(VA->begin(), VA->end());

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // sort with a counter

        cout << endl;
        cout << " --> sort with counter --> the vector" << endl;

        t1 = clock();

        sort(VA->begin(), VA->end(), comp_count_obj);

        t2 = clock();

        long long state1 = comp_count_obj.report();

        cout << " --> counter = " << state1 << endl;

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // delete the vector

        delete VA;

        //
        // build the array
        //

        cout << endl;
        cout << " --> build the array" << endl;

        for (long long i = 0; i != DIM_MAX; i++) {
            AA[i]=cos(static_cast<double>(i));
        }

        // random shuffle

        cout << " --> random shuffle --> the array" << endl;

        t1 = clock();

        random_shuffle(&AA[0], &AA[DIM_MAX]);

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // sort with a counter

        cout << endl;
        cout << " --> sort with counter --> the array" << endl;

        t1 = clock();

        sort(&AA[0], &AA[DIM_MAX], comp_count_obj);

        t2 = clock();

        long long state2 = comp_count_obj.report();

        cout << " --> counter = " << state2-state1 << endl;

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // delete the array

        delete [] AA;
    }

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

