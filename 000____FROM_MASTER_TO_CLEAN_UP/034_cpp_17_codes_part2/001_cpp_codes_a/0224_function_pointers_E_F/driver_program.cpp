
//===========================//
// function pointers example //
//===========================//

#include <iostream>
#include <cmath>
#include <cstdlib>
#include <ctime>

using std::endl;
using std::cout;
using std::cin;
using std::pow;
using std::rand;
using std::qsort;
using std::time;

// function --> 1

// we use void * arguments so as to be able to use any type of agruments
// that is the C-way to pass any type of arguments
// C++ uses templates, but also the above syntax,
// so it is perfectly legal to use that in a C++ pure code

int int_sorter(const void * first_arg, const void * second_arg)
{
    int first = * (int*) first_arg;
    int second = * (int*) second_arg;

    // sort here

    if (first < second) {
        return -1;
    } else if (first == second) {
        return 0;
    } else {
        return 1;
    }
}

// the main function

int main()
{
    // local variables and parameters

    const int DIMEN_MAX = static_cast<int>(pow(10.0, 7.0));
    int * arrayInt = new int [DIMEN_MAX];
    int i;

    // build the array

    srand(time(NULL));

    for (i = 0; i < DIMEN_MAX; i++) {
        arrayInt[i] = rand();
    }

    // sort the array

    qsort(arrayInt, DIMEN_MAX, sizeof(int), int_sorter);

    // display some results

    for ( i = 0; i < 1000; i++ ) {
        cout << " --> " << i << " --> " << arrayInt[i] << endl;
    }

    // delete the array

    delete [] arrayInt;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

