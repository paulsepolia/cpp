
//=========================//
// stl, generate algorithm //
//=========================//

#include <iostream>
#include <iomanip>
#include <cassert>
#include <vector>
#include <deque>
#include <list>
#include <algorithm>
#include <cmath>

using std::cout;
using std::cin;
using std::endl;
using std::fixed;
using std::setprecision;
using std::showpoint;
using std::showpos;
using std::vector;
using std::deque;
using std::list;
using std::generate;
using std::pow;

// object function

template <class T, class P>
class calc_cos {
public:

    // default constructor

    calc_cos() : i(-1) {}

    // () operator overload with no arguments

    P operator()()
    {
        ++i;
        return cos(static_cast<double>(i));
    }

private:
    T i;
};

// the main function

int main()
{
    // local variables and parameters

    const long long DIM_MAX = static_cast<long long>(pow(10.0, 7.0));
    const long long K_MAX = static_cast<long long>(pow(10.0, 5.0));
    long long i;

    // main for loop

    for(long long k = 0; k != K_MAX; k++) {
        // counter

        cout << "-------------------------------------------------->> " << k << endl;

        // local containers

        vector<double> * VA = new vector<double>;
        list<double> * LA = new list<double>;
        deque<double> * DA = new deque<double>;
        list<double>::iterator itl;
        deque<double>::iterator itd;

        // allocate space

        cout << " --> build the vector container" << endl;

        for (i = 0; i < DIM_MAX; i++) {
            VA->push_back(0.0);
        }

        // fill vector with some numbers --> cos(i)

        cout << " --> fill up vector using generate(....)" << endl;

        generate(VA->begin(), VA->end(), calc_cos<long long, double>());

        // test

        cout << " --> test vector" << endl;

        for (i = 0; i != DIM_MAX; i++) {
            assert((*VA)[i] == cos(static_cast<double>(i)));
        }

        // report vector

        cout << " --> ok with vector" << endl;

        // allocate space for list

        cout << " --> build the list container" << endl;

        for (i = 0; i < DIM_MAX; i++) {
            LA->push_back(0.0);
        }

        // fill list with some numbers --> cos(i)

        cout << " --> fill up list using generate(....)" << endl;

        generate(LA->begin(), LA->end(), calc_cos<long long, double>());

        // test

        cout << " --> test list" << endl;

        for (itl = LA->begin(), i = 0; itl != LA->end(); itl++, i++) {
            assert(*itl == cos(static_cast<double>(i)));
        }

        // report list

        cout << " --> ok with list" << endl;

        // allocate space for deque

        cout << " --> build the deque container" << endl;

        for (i = 0; i < DIM_MAX; i++) {
            DA->push_back(0.0);
        }

        // fill deque with some numbers --> cos(i)

        cout << " --> fill up deque using generate(....)" << endl;

        generate(DA->begin(), DA->end(), calc_cos<long long, double>());

        // test

        cout << " --> test deque" << endl;

        for (itd = DA->begin(), i = 0; itd != DA->end(); itd++, i++) {
            assert(*itd == cos(static_cast<double>(i)));
        }

        // report deque

        cout << " --> ok with deque" << endl;

        // delete the containers

        cout << " --> delete --> vector" << endl;

        delete VA;

        cout << " --> delete --> list" << endl;

        delete LA;

        cout << " --> delete --> deque" << endl;

        delete DA;
    }

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

