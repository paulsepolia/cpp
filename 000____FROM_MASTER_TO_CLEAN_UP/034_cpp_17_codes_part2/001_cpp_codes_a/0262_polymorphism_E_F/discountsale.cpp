
//======================================//
// definition of the class DiscountSale //
//======================================//

#include "discountsale.h"

namespace pgg {
// default constructor

DiscountSale::DiscountSale() : Sale(), discount(0) {}

// constructor with two arguments

DiscountSale::DiscountSale(double thePrice, double theDiscount)
    : Sale(thePrice), discount(theDiscount) {}

// member function

double DiscountSale::getDiscount() const
{
    return discount;
}

// member function

void DiscountSale::setDiscount(double newDiscount)
{
    discount = newDiscount;
}

// virtual member function

double DiscountSale::bill() const
{
    double fraction = discount / 100;

    return (1-fraction) * getPrice();
}

} // pgg

//======//
// FINI //
//======//
