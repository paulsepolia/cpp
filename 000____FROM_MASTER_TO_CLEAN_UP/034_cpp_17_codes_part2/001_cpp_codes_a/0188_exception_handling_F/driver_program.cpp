
//===============================//
// handling bad_alloc exceptions //
//===============================//

#include <iostream>
#include <stdexcept>

using std::endl;
using std::cout;
using std::cin;
using std::bad_alloc;

int main()
{

    double *list[100];
    const long DIM_MAX = 1800000000;

    try {
        for (long i = 0; i < 100; i++) {
            list[i] = new double [DIM_MAX];
            cout << "Line 4: Created list[" << i <<"] of " << DIM_MAX << " components." << endl;
        }
    } catch (bad_alloc be) {
        cout << "Line 7: In bad_alloc catch block: " << be.what() << "." << endl;
    }

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
