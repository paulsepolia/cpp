
//==========================//
// istream iterator example //
//==========================//

#include <iostream>
#include <iterator>

using std::endl;
using std::cout;
using std::cin;
using std::istream_iterator;

// the main function

int main()
{
    // local variables and parameters

    double val1;
    double val2;

    // insert values

    cout << "please, insert two values: ";

    istream_iterator<double> eosA;
    istream_iterator<double> it(cin); // stdin iterator

    if (it != eosA) val1 = *it;

    it++;

    if (it != eosA) val2 = *it;

    cout << " val1 = " << val1 << endl;
    cout << " val2 = " << val2 << endl;
    cout << " val1 * val2 = " << val1 * val2 << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

