//=========//
// complex //
//=========//

#include <iostream>
#include <complex>

using std::cout;
using std::endl;
using std::complex;

// the main function

int main ()
{
    complex<double> comA (10.0, 20.0);

    cout << "Real part: " << comA.real() << endl;
    cout << "Imaginary part: " << comA.imag() << endl;

    complex<double> comB;
    complex<double> comC;

    comB = {30.00, 40.00};

    cout << "Real part: " << comB.real() << endl;
    cout << "Imaginary part: " << comB.imag() << endl;

    // +

    comC = comA + comB;

    cout << "Real part: " << comC.real() << endl;
    cout << "Imaginary part: " << comC.imag() << endl;

    // *

    comC = comA * comB;

    cout << "Real part: " << comC.real() << endl;
    cout << "Imaginary part: " << comC.imag() << endl;

    // -

    comC = comA - comB;

    cout << "Real part: " << comC.real() << endl;
    cout << "Imaginary part: " << comC.imag() << endl;

    // /

    comC = comA / comB;

    cout << "Real part: " << comC.real() << endl;
    cout << "Imaginary part: " << comC.imag() << endl;

    return 0;
}

// END
