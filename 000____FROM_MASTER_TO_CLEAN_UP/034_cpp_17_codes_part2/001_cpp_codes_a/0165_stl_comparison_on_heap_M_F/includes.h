//==========//
// includes //
//==========//

#include <iostream>
#include <algorithm>
#include <vector>
#include <list>
#include <deque>
#include <cmath>
#include <iomanip>
#include <ctime>

using std::cout;
using std::cin;
using std::endl;
using std::list;
using std::vector;
using std::deque;
using std::fixed;
using std::setprecision;
using std::showpoint;
using std::sort;

//======//
// FINI //
//======//
