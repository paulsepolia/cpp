//============================================================================//
// The keyword 'this' represents a pointer to the object                      //
// whose member function is being executed.                                   //
// It is used within a class's member function to refer to the object itself. //
//============================================================================//

// One of its uses can be to check if a parameter passed
// to a member function is the object itself.

// For example:

#include <iostream>

// a class

class Dummy {
public:
    bool isitme (Dummy& param);
};

// member function definition

bool Dummy::isitme (Dummy& param)
{
    if (&param == this) {
        return true;
    } else {
        return false;
    }
}

// the main function

int main()
{
    Dummy a;
    Dummy* b = &a;

    if ( b -> isitme(a) ) {
        std::cout << "yes, &a is b" << std::endl;
    }

    if ( (*b).isitme(a) ) {
        std::cout << "yes, &a is b" << std::endl;
    }

    if ( a.isitme(a) ) {
        std::cout << "yes, &a is &a" << std::endl;
    }

    // sentineling

    int sentinel;
    std::cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
