
//========================================//
// definition of the class HourlyEmployee //
//========================================//

#include <string>
#include <iostream>
#include "hourlyemployee.h"

using std::string;
using std::cout;
using std::endl;

namespace pgg {
// default constructor

HourlyEmployee::HourlyEmployee() : Employee(), wageRate(0), hours(0) {}

// constructor with four arguments

HourlyEmployee::HourlyEmployee(string theName, string theNumber, double theWageRate, double theHours)
    : Employee(theName, theNumber), wageRate(theWageRate), hours(theHours) {}

// member function

void HourlyEmployee::setRate(double newWageRate)
{
    wageRate = newWageRate;
}

// member function

double HourlyEmployee::getRate() const
{
    return wageRate;
}

// member function

void HourlyEmployee::setHours(double hoursWorked)
{
    hours = hoursWorked;
}

// member function

double HourlyEmployee::getHours() const
{
    return hours;
}

// member function

void HourlyEmployee::printCheck() // redefinition and drop the const qualifier
{
    setNetPay(hours * wageRate);

    cout << "______________________________________________________________" << endl;
    cout << "Pay to the order of " << getName() << endl;
    cout << "The sum of " << getNetPay() << " Dollars" << endl;
    cout << "______________________________________________________________" << endl;
    cout << "Check Stub: NOT NEGOTIABLE" << endl;
    cout << "Employee Number: " << getSsn() << endl;
    cout << "Hourly Employee." << endl;
    cout << "Hours worked: " << hours
         << " Rate: " << wageRate << " Pay: " << getNetPay() << endl;
    cout << "______________________________________________________________" << endl;
}

} // pgg

