#include <iostream>
#include <string>
#include <pthread.h>
#include <iomanip>
#include <ctime>

using namespace std;

// function declaration

void *aFun(void *);

// main function

int main()
{
    // variables declaration

    const int numThreads = 8;
    pthread_t *threadArray = new pthread_t [numThreads];
    string *strArray = new string [numThreads];
    int i;
    clock_t t1;
    clock_t t2;

    // give values to the strings

    for (i = 0; i < numThreads; i++) {
        strArray[i] = to_string(i);
    }

    // set the output format

    cout << setprecision(20);
    cout << showpoint;
    cout << showpos;

    // Create independent threads each of which
    // will execute the function and call the function

    t1 = clock(); // timing

    for (i = 0; i < numThreads; i++) {
        pthread_create(&threadArray[i], NULL, aFun, (void*) (&strArray[i]));
    }

    // join

    for (i = 0; i < numThreads; i++) {
        pthread_join(threadArray[i], NULL);
    }

    t2 = clock(); // timing

    // timing

    cout << " Total real time is --> " << static_cast<double>(t2-t1)/(CLOCKS_PER_SEC*numThreads) << endl;

    return 0;
}

// function definition

void *aFun(void *argument)
{
    // variables declaration

    string sA;
    double sum;
    const long int I_MAX = 100 * static_cast<long int>(1000000000);
    long i;

    // give value

    sA = *((string *) argument);

    // a message

    cout << " Hello from thread --> " << sA << endl;

    // local waste of CPU time
    // to notice the parallel execution

    sum = 0.0;

    for(i = 0; i < I_MAX; i++) {
        sum = sum + static_cast<double>(i);
    }

    cout << " Sum is --> " << sum << " and thread is " << sA << endl;

    return NULL;
}

// end
