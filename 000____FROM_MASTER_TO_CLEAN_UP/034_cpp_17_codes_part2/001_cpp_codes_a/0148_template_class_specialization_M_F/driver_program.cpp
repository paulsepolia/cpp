
// It is possible to define a different implementation
// for a template when a specific type is passed as template argument.
// This is called a template specialization.

// For example, let's suppose that we have a very simple class
// called mycontainer that can store one element of any type
// and that has just one member function called increase,
// which increases its value. But we find that when
// it stores an element of type char it would be more convenient
// to have a completely different implementation with a function member uppercase,
// so we decide to declare a class template specialization for that type:

//==========================//
// template specialization  //
//==========================//

#include <iostream>

// a class template

template <class T>
class mycontainer {
private:

    T element;

public:

    mycontainer (T arg)
    {
        element = arg;     // a constructor
    }

    T increase ()   // public member function
    {
        return ++element;
    }
};

// class template specialization:

template <>
class mycontainer <char> {
private:

    char element;

public:

    mycontainer (char arg)
    {
        element = arg;     // a constructor
    }

    char uppercase ()   // member function
    {
        if ( (element >= 'a') && (element <= 'z') )
            element += 'A' - 'a';
        return element;
    }
};

// the main function

int main ()
{
    mycontainer<int> myint (7);
    mycontainer<char> mychar ('j');
    std::cout << myint.increase() << std::endl;
    std::cout << mychar.uppercase() << std::endl;

    int sentinel;
    std::cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
