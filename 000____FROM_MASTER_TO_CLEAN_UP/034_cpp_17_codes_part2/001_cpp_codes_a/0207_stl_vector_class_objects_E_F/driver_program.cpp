
//==================================//
// store a class object in a vector //
//==================================//

#include <iostream>
#include <vector>
#include <algorithm>
#include <cstdlib>

using namespace std;

// class definition

class DailyOutput {
public:

    // default constructor

    DailyOutput() : units (0) {}

    // constructor

    explicit DailyOutput(int x) : units (x) {}

    // member functions

    double get_units()
    {
        return units;
    }

private:

    int units;
};

// definition of operator < ()
// for the objects of the class DailyOutput

bool operator < (DailyOutput & a, DailyOutput & b)
{
    return (a.get_units() < b.get_units());
}

// definition of operator == ()
// for the objects of the class DailyOutput

bool operator == (DailyOutput & a, DailyOutput & b)
{
    return (a.get_units() == b.get_units());
}

// the main function

int main()
{
    vector<DailyOutput> v;
    vector<DailyOutput>::iterator p;

    // 1.

    for (int i = 0; i < 140; i++) {
        v.push_back(DailyOutput(60 + rand()%10));
    }

    // 2.

    cout << "Daily production:" << endl;

    for (unsigned int i = 0; i < v.size(); i++) {
        cout << v[i].get_units() << " ";
    }

    cout << endl << endl;

    // 3.

    p = min_element(v.begin(), v.end());

    cout << "Minimum units: " << p -> get_units();

    cout << endl;

    // 4.

    p = max_element(v.begin(), v.end());

    cout << "Maximum units: " << p -> get_units();

    cout << endl;

    // 5.

    // look for consecutive days with the same production

    p = v.begin();

    do {
        p = adjacent_find(p, v.end());

        if (p != v.end()) {
            cout << "Two consecutive days with ";
            cout << p -> get_units() << " units." << endl;
            p++; // move on the next element
        }
    } while (p != v.end());

    // xx.

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
