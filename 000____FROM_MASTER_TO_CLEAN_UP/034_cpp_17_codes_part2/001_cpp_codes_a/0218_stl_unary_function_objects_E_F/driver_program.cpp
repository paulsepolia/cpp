
//=====================================//
// create a reciprocal function object //
//=====================================//

#include <iostream>
#include <list>
#include <algorithm>
#include <iomanip>
#include <cmath>

using std::endl;
using std::cout;
using std::cin;
using std::setprecision;
using std::showpos;
using std::showpoint;
using std::fixed;
using std::list;
using std::transform;
using std::pow;

// a simple function object

template <class T1, class T2>
class reciprocalA {
public:
    T1 operator () (T2 i)
    {
        return static_cast<T1>(1.0/i); // return reciprocal
    }
};

// a simple function object

class reciprocalB {
public:
    double operator () (double i)
    {
        return static_cast<double>(1.0/i); // return reciprocal
    }
};

// the main function

int main()
{
    // local variables and parameters

    const int DIMEN_MAX = static_cast<int>(pow(10.0, 1.0));
    list<double> vals;
    list<double>::iterator it;

    // output format

    cout << fixed;
    cout << setprecision(20);
    cout << showpos;
    cout << showpoint;

    // build the list

    for (int i = 1; i <= DIMEN_MAX; i++) {
        vals.push_back(static_cast<double>(i));
    }

    // display the original values

    cout << "original contents of list: " << endl;
    cout << endl;

    for (it = vals.begin(); it != vals.end(); it++) {
        cout << *it << " " << endl;
    }

    cout << endl;

    // use the function object

    transform(vals.begin(), vals.end(), vals.begin(), reciprocalA<double,double>());

    // above i call the function object

    // display the transformed contents of vals

    cout << "Transformed contents of list: " << endl;
    cout << endl;

    for (it = vals.begin(); it != vals.end(); it++) {
        cout << *it << " " << endl;
    }

    cout << endl;

    // use the function object

    transform(vals.begin(), vals.end(), vals.begin(), reciprocalB());

    // above i call the function object

    // display the transformed contents of vals

    cout << "re-transformed contents of list: " << endl;
    cout << endl;

    for (it = vals.begin(); it != vals.end(); it++) {
        cout << *it << " " << endl;
    }

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
