
#include <iostream>
#include <vector>

using std::vector;
using std::endl;
using std::cout;

int main()
{
    vector<int> container;

    for (int i = 1; i <= 11; i++) {
        container.push_back(i);
    }

    cout << "Here is what is in the container: " << endl;
    vector<int>::iterator p;

    for (p = container.begin(); p != container.end(); p++) {
        cout << *p << endl;
    }

    cout << endl;

    cout << "Setting entries to 0:" << endl;

    for (p = container.begin(); p != container.end(); p++) {
        *p = 0;
    }

    cout << "Container now contains: " << endl;

    for (p = container.begin(); p != container.end(); p++) {
        cout << *p << endl;
    }

    return 0;
}

// fini

