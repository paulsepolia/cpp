
//=========================//
// insert_iterator example //
//=========================//

#include <iostream>
#include <iterator>
#include <vector>
#include <algorithm>
#include <string>

using std::endl;
using std::cout;
using std::cin;
using std::vector;
using std::copy;
using std::string;
using std::inserter;

// the main function

int main()
{
    // local variables and parameters

    vector<string> vA;
    vector<string> vB;
    vector<string>::iterator it;

    // build vA

    vA.push_back("The");
    vA.push_back("STL");
    vA.push_back("are");
    vA.push_back("powerful.");

    // build vB

    vB.push_back("and");
    vB.push_back("insert");
    vB.push_back("iterators");

    // get the size of the vectors

    cout << " --> size of vA = " << vA.size() << endl;
    cout << " --> size of vB = " << vB.size() << endl;

    // outputs the contents of the vectors

    cout << "----------------------------------------------" << endl;

    for (it = vA.begin(); it != vA.end(); it++) {
        cout << *it << " " << endl;
    }

    cout << "----------------------------------------------" << endl;

    // outputs the contents of the vectors

    for (it = vB.begin(); it != vB.end(); it++) {
        cout << *it << " " << endl;
    }

    // insert vB to vA

    copy(vB.begin(), vB.end(), inserter(vA, vA.begin()+2));

    // outputs the contents of the vectors

    cout << "----------------------------------------------" << endl;

    for (it = vA.begin(); it != vA.end(); it++) {
        cout << *it << " " << endl;
    }

    cout << "----------------------------------------------" << endl;

    // get the size of the vectors

    cout << " --> size of vA = " << vA.size() << endl;
    cout << " --> size of vB = " << vB.size() << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

