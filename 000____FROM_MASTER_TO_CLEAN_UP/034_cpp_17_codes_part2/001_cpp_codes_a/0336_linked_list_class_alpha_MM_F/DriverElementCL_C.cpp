//========================================//
// Driver program to the ElementCL class. //
//========================================//

#include <iostream>

#include "ElementCL.h"
#include "ElementCLMemberFunctions.h"

#include "ListCL.h"
#include "ListCLMemberFunctions.h"
#include "ListCLFriendFunctions.h"

#include "TypeDefinitions.h"

using namespace std;

int main()
{

    // local constants

    const TB DIMLIST = 1E2;
    const TB DIMLISTB = 1E3;
    const TB DIMLISTC = 1E6;

    // local variables

    TB i, len, pos;
    TA va, vb;
    TA elema, elemb, elemc;

    // local objects

    ListCL<TA,TB> oListA;
    ListCL<TA,TB> oListB;
    ListCL<TA,TB> oListC;

    // code rest

    // test 1.

    len = oListA.LengthList();
    cout << "---->  1. Must be zero." << endl;
    cout << "---->  2. Length of list is " << len << endl;

    // test 2.

    for (i = 0; i < DIMLIST; i++) {
        va = 1.0 + i;
        oListA.AddElement(va);
    }

    len = oListA.LengthList();
    cout << "---->  3. Must be " << DIMLIST << endl;
    cout << "---->  4. Length of list is " << len << endl;

    // test 3.

    pos = DIMLIST+1;
    elema = oListA.GetListElement(pos);
    cout << "---->  5. Must be -1." << endl;
    cout << "---->  6. Element at position " << pos << " is " << elema << endl;

    pos = 10;
    elema = oListA.GetListElement(pos);
    cout << "---->  7. Must be 10." << endl;
    cout << "---->  8. Element at position " << pos << " is " << elema << endl;

    // test 4.

    for (i = 0; i < 2*DIMLIST; i++) {
        va = 1.0 + i;
        oListA.AddElement(va);
    }

    len = oListA.LengthList();
    cout << "---->  9. Must be " << 3*DIMLIST << endl;
    cout << "----> 10. Length of list is " << len << endl;

    // test 5.

    pos = 10;
    elema = 111;
    oListA.SetListElement(elema, pos);
    elemb = oListA.GetListElement(pos);
    cout << "----> 11. Must be " << elema << endl;
    cout << "----> 12. Element at position " << pos << " is " << elemb << endl;
    elemb = oListA.GetListElement(pos-1);
    cout << "----> 13. Must be " << pos-1 << endl;
    cout << "----> 14. Element at position " << pos-1 << " is " << elemb << endl;

    // test 6.

    oListA.ClearList();
    len = oListA.LengthList();
    cout << "----> 15. Must be zero. " << endl;
    cout << "----> 16. Length of list is " << len << endl;

    // test 7.

    for (i = 0; i < DIMLISTB; i++) {
        va = 1.0 + i;
        oListA.AddElement(va);
    }

    len = oListA.LengthList();
    cout << "----> 17. Must be " << DIMLISTB << endl;
    cout << "----> 18. Length of list is " << len << endl;

    for (i = 0; i < 2; i++) {
        elema = i + 1 + 10.0;
        pos = i + 1;
        oListA.SetListElement(elema, pos);
        elemb = oListA.GetListElement(pos);
        cout << "----> 19. Must be " << elema << endl;
        cout << "----> 20. Element is " << elemb << endl;
    }

    // test 8.

    CreateList(oListB, oListA.LengthList());
    SetEqual(oListB, oListA);
    pos = 101;
    elemb = oListB.GetListElement(pos);
    len = oListB.LengthList();

    cout << "----> 21. Must be " << DIMLISTB << endl;
    cout << "----> 22. Length of list is " << len << endl;
    cout << "----> 23. Must be " << pos << endl;
    cout << "----> 24. Element is " << elemb << endl;

    // test 9.

    oListA.ClearList();
    oListB.ClearList();
    oListC.ClearList();

    CreateList(oListA, DIMLISTB);
    CreateList(oListB, DIMLISTB);
    CreateList(oListC, DIMLISTB);

    for (i = 0; i < DIMLISTB; i++) {
        pos = i + 1;
        va = 11.1 + i;
        oListA.SetListElement(va, pos);
        vb = 22.2 + i;
        oListB.SetListElement(vb, pos);
    }

    AddList(oListC, oListA, oListB);

    elema = oListA.GetListElement(10);
    elemb = oListB.GetListElement(10);
    elemc = oListC.GetListElement(10);

    cout << "----> 25. elema is " << elema << endl;
    cout << "----> 26. elemb is " << elemb << endl;
    cout << "----> 27. elemc is " << elemc << endl;

    SubtractList(oListC, oListA, oListB);

    elema = oListA.GetListElement(10);
    elemb = oListB.GetListElement(10);
    elemc = oListC.GetListElement(10);

    cout << "----> 28. elema is " << elema << endl;
    cout << "----> 29. elemb is " << elemb << endl;
    cout << "----> 30. elemc is " << elemc << endl;

    MultiplyList(oListC, oListA, oListB);

    elema = oListA.GetListElement(10);
    elemb = oListB.GetListElement(10);
    elemc = oListC.GetListElement(10);

    cout << "----> 31. elema is " << elema << endl;
    cout << "----> 32. elemb is " << elemb << endl;
    cout << "----> 33. elemc is " << elemc << endl;

    DivideList(oListC, oListA, oListB);

    elema = oListA.GetListElement(10);
    elemb = oListB.GetListElement(10);
    elemc = oListC.GetListElement(10);

    cout << "----> 34. elema is " << elema << endl;
    cout << "----> 35. elemb is " << elemb << endl;
    cout << "----> 36. elemc is " << elemc << endl;

    // test 10.

    for (i = 0; i < DIMLISTC; i++) {
        va = 1.0 + i;
        oListA.AddElement(va, i+1);
    }

    pos = 100;
    elema = oListA.GetListElement(pos);

    cout << "----> 37. Must be " << pos << endl;
    cout << "----> 38. elema is " << elema << endl;

    return 0;
}

//==============//
// End of code. //
//==============//
