
//=======================//
// base class definition //
//=======================//

#include <iostream>
#include <cstdlib>
#include <cmath>
#include "superbase.h"
#include "base.h"

using std::cout;
using std::cin;
using std::endl;
using std::exit;
using std::sqrt;

namespace pgg {
// default constructor

Base::Base() : dim(0), vec(nullptr) {}

// constructor with two arguments

Base::Base(double val, long theDim) : dim(theDim), vec(nullptr)
{
    vec = new double [theDim];

    for (long i = 0; i < dim; i++) {
        vec[i] = val;
    }
}

// copy constructor

Base::Base(const Base& obj) : dim(obj.dim), vec(nullptr)
{
    vec = new double [dim];

    for (long i = 0; i != dim; i++) {
        vec[i] = obj.vec[i];
    }
}

// destructor

Base::~Base()
{
    delete [] vec;
    vec = nullptr;
}

// member function

void Base::allocate(long theDim)
{
    delete [] this->vec;
    this->vec = nullptr;

    this->dim = theDim;
    this->vec = new double [theDim];
}

// member function

long Base::getDim() const
{
    return this->dim;
}

// member function

double Base::getElement(long index) const
{
    return this->vec[index];
}

// member function

void Base::setDim(long theDim)
{
    delete [] this->vec;
    this->vec = nullptr;
    this->dim = theDim;
}

// member function

void Base::setElement(double elem, long index)
{
    this->vec[index] = elem;
}

// member function

void Base::setEqual(const Superbase& obj)
{
    this->allocate(obj.getDim());

    for (long i = 0; i < obj.getDim(); i++) {
        this->vec[i] = obj.getElement(i);
    }
}

// member function

bool Base::isEqual(const Superbase& obj) const
{
    if (this->getDim() != obj.getDim()) {
        return false;
    }

    for (long i = 0; i < obj.getDim(); i++) {
        if (this->vec[i] != obj.getElement(i)) {
            return false;
        }
    }

    return true;
}

// member function

void Base::add(const Superbase& objA, const Superbase& objB)
{
    if (objA.getDim() != objB.getDim()) {
        cout << "Objects of unequal length! Enter an integer to exit: ";
        int sentinel;
        cin >> sentinel;
        exit(-1);
    }

    this->allocate(objA.getDim());

    for (long i = 0; i < objA.getDim(); i++) {
        this->vec[i] = objA.getElement(i) + objB.getElement(i);
    }
}

// member function

void Base::subtract(const Superbase& objA, const Superbase& objB)
{
    if (objA.getDim() != objB.getDim()) {
        cout << "Objects of unequal length! Enter an integer to exit: ";
        int sentinel;
        cin >> sentinel;
        exit(-1);
    }

    this->allocate(objA.getDim());

    for (long i = 0; i < objA.getDim(); i++) {
        this->vec[i] = objA.getElement(i) - objB.getElement(i);
    }
}

// member function

void Base::multiply(const Superbase& objA, const Superbase& objB)
{
    if (objA.getDim() != objB.getDim()) {
        cout << "Objects of unequal length! Enter an integer to exit: ";
        int sentinel;
        cin >> sentinel;
        exit(-1);
    }

    this->allocate(objA.getDim());

    for (long i = 0; i < objA.getDim(); i++) {
        this->vec[i] = objA.getElement(i) * objB.getElement(i);
    }
}

// member function

void Base::divide(const Superbase& objA, const Superbase& objB)
{
    if (objA.getDim() != objB.getDim()) {
        cout << "Objects of unequal length! Enter an integer to exit: ";
        int sentinel;
        cin >> sentinel;
        exit(-1);
    }

    this->allocate(objA.getDim());

    for (long i = 0; i < objA.getDim(); i++) {
        this->vec[i] = objA.getElement(i) / objB.getElement(i);
    }
}

// member function

double Base::magnitude() const
{
    double tmp = 0;

    for (long i = 0; i < dim; i++) {
        tmp = tmp + ((this->vec[i]) * (this->vec[i]));
    }

    return sqrt(tmp);
}

// member function

void Base::destroy()
{
    delete [] vec;
    this->vec = nullptr;
    this->dim = 0;
}

// == operator overload

bool Base::operator == (const Base& obj) const
{
    if (this->dim != obj.dim) {
        cout << "Objects of unequal length! Enter an integer to exit: ";
        int sentinel;
        cin >> sentinel;
        exit(-1);
    }

    for (long i = 0; i < obj.dim; i++) {
        if (this->vec[i] != obj.vec[i]) {
            return false;
        }
    }

    return true;
}

// != operator overload

bool Base::operator != (const Base& obj) const
{
    if (this->dim != obj.dim) {
        cout << "Objects of unequal length! Enter an integer to exit: ";
        int sentinel;
        cin >> sentinel;
        exit(-1);
    }

    return !(*this == obj);
}

// < operator overload

bool Base::operator < (const Base& obj) const
{
    if (this->dim != obj.dim) {
        cout << "Objects of unequal length! Enter an integer to exit: ";
        int sentinel;
        cin >> sentinel;
        exit(-1);
    }

    return (this->magnitude() < obj.magnitude());
}

// <= operator overload

bool Base::operator <= (const Base& obj) const
{
    if (this->dim != obj.dim) {
        cout << "Objects of unequal length! Enter an integer to exit: ";
        int sentinel;
        cin >> sentinel;
        exit(-1);
    }

    return (this->magnitude() <= obj.magnitude());
}

// > operator overload

bool Base::operator > (const Base& obj) const
{
    if (this->dim != obj.dim) {
        cout << "Objects of unequal length! Enter an integer to exit: ";
        int sentinel;
        cin >> sentinel;
        exit(-1);
    }

    return (this->magnitude() > obj.magnitude());
}

// >= operator overload

bool Base::operator >= (const Base& obj) const
{
    if (this->dim != obj.dim) {
        cout << "Objects of unequal length! Enter an integer to exit: ";
        int sentinel;
        cin >> sentinel;
        exit(-1);
    }

    return (this->magnitude() >= obj.magnitude());
}

// + operator overload

Base Base::operator + (const Base& obj) const
{
    if (this->dim != obj.dim) {
        cout << "Objects of unequal length! Enter an integer to exit: ";
        int sentinel;
        cin >> sentinel;
        exit(-1);
    }

    Base tmp;

    tmp.allocate(obj.dim);

    tmp.add(*this, obj);

    return Base(tmp);
}

// + operator overload ==> add a double

Base Base::operator + (double val) const
{
    Base tmp;

    tmp.dim = this->dim;

    tmp.allocate(tmp.dim);

    for (long i = 0; i < this->dim; i++) {
        tmp.vec[i] = this->vec[i] + val;
    }

    return Base(tmp);
}

// - operator overload

Base Base::operator - (const Base& obj) const
{
    if (this->dim != obj.dim) {
        cout << "Objects of unequal length! Enter an integer to exit: ";
        int sentinel;
        cin >> sentinel;
        exit(-1);
    }

    Base tmp;

    tmp.allocate(obj.dim);

    tmp.subtract(*this, obj);

    return Base(tmp);
}

// - operator overload ==> subtract a double

Base Base::operator - (double val) const
{
    Base tmp;

    tmp.dim = this->dim;

    tmp.allocate(tmp.dim);

    for (long i = 0; i < this->dim; i++) {
        tmp.vec[i] = this->vec[i] - val;
    }

    return Base(tmp);
}

// * operator overload

Base Base::operator * (const Base& obj) const
{
    if (this->dim != obj.dim) {
        cout << "Objects of unequal length! Enter an integer to exit: ";
        int sentinel;
        cin >> sentinel;
        exit(-1);
    }

    Base tmp;

    tmp.allocate(obj.dim);

    tmp.multiply(*this, obj);

    return Base(tmp);
}

// * operator overload ==> multiply by a double

Base Base::operator * (double val) const
{
    Base tmp;

    tmp.dim = this->dim;

    tmp.allocate(tmp.dim);

    for (long i = 0; i < this->dim; i++) {
        tmp.vec[i] = this->vec[i] * val;
    }

    return Base(tmp);
}

// / operator overload

Base Base::operator / (const Base& obj) const
{
    if (this->dim != obj.dim) {
        cout << "Objects of unequal length! Enter an integer to exit: ";
        int sentinel;
        cin >> sentinel;
        exit(-1);
    }

    Base tmp;

    tmp.allocate(obj.dim);

    tmp.divide(*this, obj);

    return Base(tmp);
}

// / operator overload ==> divide by a double

Base Base::operator / (double val) const
{
    Base tmp;

    tmp.dim = this->dim;

    tmp.allocate(tmp.dim);

    for (long i = 0; i < this->dim; i++) {
        tmp.vec[i] = this->vec[i] / val;
    }

    return Base(tmp);
}

// ++ operator overload (prefix)

const Base Base::operator ++ ()
{
    for (long i = 0; i < this->dim; i++) {
        ++(this->vec[i]);
    }

    return *this;
}

// ++ operator overload (postfix)

const Base Base::operator ++ (int)
{
    for (long i = 0; i < this->dim; i++) {
        ++(this->vec[i]);
    }

    return *this;
}

// -- operator overload (prefix)

const Base Base::operator -- ()
{
    for (long i = 0; i < this->dim; i++) {
        --(this->vec[i]);
    }

    return *this;
}

// -- operator overload (postfix)

const Base Base::operator -- (int)
{
    for (long i = 0; i < this->dim; i++) {
        --(this->vec[i]);
    }

    return *this;
}

// - unary operator overload

const Base Base::operator - ()
{
    for (long i = 0; i < this->dim; i++) {
        this->vec[i] = -(this->vec[i]);
    }

    return *this;
}

// += operator overload

const Base Base::operator += (const Base& obj)
{
    if (this->dim != obj.dim) {
        cout << "Objects of unequal length! Enter an integer to exit: ";
        int sentinel;
        cin >> sentinel;
        exit(-1);
    }

    for (long i = 0; i < obj.dim; i++) {
        this->vec[i] = this->vec[i] + obj.vec[i];
    }

    return *this;
}

// += operator overload ==> add a double

const Base Base::operator += (double val)
{
    for (long i = 0; i < this->dim; i++) {
        this->vec[i] = this->vec[i] + val;
    }

    return *this;
}

// -= operator overload

const Base Base::operator -= (const Base& obj)
{
    if (this->dim != obj.dim) {
        cout << "Objects of unequal length! Enter an integer to exit: ";
        int sentinel;
        cin >> sentinel;
        exit(-1);
    }

    for (long i = 0; i < obj.dim; i++) {
        this->vec[i] = this->vec[i] - obj.vec[i];
    }

    return *this;
}

// -= operator overload ==> subtract a double

const Base Base::operator -= (double val)
{
    for (long i = 0; i < this->dim; i++) {
        this->vec[i] = this->vec[i] - val;
    }

    return *this;
}

// *= operator overload

const Base Base::operator *= (const Base& obj)
{
    if (this->dim != obj.dim) {
        cout << "Objects of unequal length! Enter an integer to exit: ";
        int sentinel;
        cin >> sentinel;
        exit(-1);
    }

    for (long i = 0; i < obj.dim; i++) {
        this->vec[i] = this->vec[i] * obj.vec[i];
    }

    return *this;
}

// *= operator overload ==> multiply by a double

const Base Base::operator *= (double val)
{
    for (long i = 0; i < this->dim; i++) {
        this->vec[i] = this->vec[i] * val;
    }

    return *this;
}

// /= operator overload

const Base Base::operator /= (const Base& obj)
{
    if (this->dim != obj.dim) {
        cout << "Objects of unequal length! Enter an integer to exit: ";
        int sentinel;
        cin >> sentinel;
        exit(-1);
    }

    for (long i = 0; i < obj.dim; i++) {
        this->vec[i] = this->vec[i] / obj.vec[i];
    }

    return *this;
}

// /= operator overload ==> divide by a double

const Base Base::operator /= (double val)
{
    for (long i = 0; i < this->dim; i++) {
        this->vec[i] = this->vec[i] / val;
    }

    return *this;
}

// = operator overload

const Base Base::operator = (const Base& obj)
{
    this->destroy();
    this->allocate(obj.dim);

    for (long i = 0; i < obj.dim; i++) {
        this->vec[i] = obj.vec[i];
    }

    return *this;
}

// = operator overload

const Base Base::operator = (long theDim)
{
    this->destroy();
    this->allocate(theDim);

    for (long i = 0; i < this->dim; i++) {
        this->vec[i] = 0;
    }

    return *this;
}

// = operator overload

const Base Base::operator = (double val)
{
    for (long i = 0; i < this->dim; i++) {
        this->vec[i] = val;
    }

    return *this;
}

// [] operator overload

double Base::operator [] (long index)
{
    if ( (index < 0) || (index >= this->dim)) {
        cout << "Illegal index! Enter an integer to exit: ";
        int sentinel;
        cin >> sentinel;
        exit(-1);
    }

    return this->vec[index];
}

} //pgg

//======//
// FINI //
//======//
