
//========================//
// base class declaration //
//========================//

#ifndef BASE_H
#define BASE_H

#include "superbase.h"

namespace pgg {
class Base : public Superbase {
public:

    // constructors

    Base();
    Base(double, long);
    Base(const Base&);

    // destructor

    virtual ~Base();

    // member functions

    void allocate(long);
    long getDim() const;
    double getElement(long) const;
    void setDim(long);
    void setElement(double, long);
    void setEqual(const Superbase&);
    bool isEqual(const Superbase&) const;
    void add(const Superbase&, const Superbase&);
    void subtract(const Superbase&, const Superbase&);
    void multiply(const Superbase&, const Superbase&);
    void divide(const Superbase&, const Superbase&);
    double magnitude() const;
    void destroy();

    // operators overload as member functions

    // ==, <, <=, >, >=, !=

    bool operator == (const Base&) const;
    bool operator != (const Base&) const;
    bool operator <  (const Base&) const;
    bool operator <= (const Base&) const;
    bool operator >  (const Base&) const;
    bool operator >= (const Base&) const;

    // +, -, *, /

    Base operator + (const Base&) const;
    Base operator - (const Base&) const;
    Base operator * (const Base&) const;
    Base operator / (const Base&) const;

    Base operator + (double) const;
    Base operator - (double) const;
    Base operator * (double) const;
    Base operator / (double) const;

    // ++, --, -

    const Base operator ++ ();
    const Base operator ++ (int);
    const Base operator -- ();
    const Base operator -- (int);
    const Base operator -  ();

    // +=, -=, *=, /=

    const Base operator += (const Base&);
    const Base operator -= (const Base&);
    const Base operator *= (const Base&);
    const Base operator /= (const Base&);

    const Base operator += (double);
    const Base operator -= (double);
    const Base operator *= (double);
    const Base operator /= (double);

    // =

    const Base operator = (const Base&);
    const Base operator = (long); // wokrs like the allocate function
    const Base operator = (double); // set all vector equal to double

    // []

    double operator [] (long);

protected:

    long dim;
    double *vec = nullptr;
};

} // pgg

#endif // BASE_H

//======//
// FINI //
//======//
