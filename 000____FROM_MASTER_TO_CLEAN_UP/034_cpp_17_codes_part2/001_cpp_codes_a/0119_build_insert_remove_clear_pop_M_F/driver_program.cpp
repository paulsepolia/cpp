//=====================================//
// STL Build, Push, Pop, Remove, Clear //
// Vector, Deque, List                 //
//=====================================//

#include <iostream>
#include <algorithm>
#include <vector>
#include <list>
#include <deque>
#include <cmath>
#include <iomanip>
#include <ctime>

// A. the main program

int main()
{
    // 1. variables and parameters

    const long int I_MAX = static_cast<long int>(pow(10.0, 7.7));
    const long int K_MAX = static_cast<long int>(pow(10.0, 6.0));
    long int i;
    long int k;
    std::list<double> listA;
    std::vector<double> vecA;
    std::deque<double> deqA;
    clock_t t1;
    clock_t t2;

    std::cout << std::fixed;
    std::cout << std::setprecision(5);
    std::cout << std::showpoint;

    // 2. main bench loop

    for (k = 0; k < K_MAX; k++) {
        std::cout << std::endl;
        std::cout << "------------------------------------------------>>> " << k << std::endl;
        std::cout << std::endl;

        //=======================//
        // BUILD using push_back //
        //=======================//

        std::cout<< std::endl;
        std::cout << " BUILD using push_back()" << std::endl;
        std::cout<< std::endl;

        #pragma omp parallel sections default(none)\
        shared(vecA, deqA, listA, std::cout)\
        private(t1, t2, i)
        {
            #pragma omp section
            {
                // 3. building the vector

                t1 = clock();
                std::cout << "  1 --> building the vector --> vecA" << std::endl;
                for (i = 0; i < I_MAX; i++)
                {
                    vecA.push_back(static_cast<double>(cos(i)));
                }
                t2 = clock();
                std::cout << "    --> done with vector --> vecA --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 4. building the list

                t1 = clock();
                std::cout << "  2 --> building the list --> listA" << std::endl;
                for (i = 0; i < I_MAX; i++)
                {
                    listA.push_back(static_cast<double>(cos(i)));
                }
                t2 = clock();
                std::cout << "    --> done with list --> listA --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 5. building the deque

                t1 = clock();
                std::cout << "  3 --> building the deque --> dequeA" << std::endl;
                for (i = 0; i < I_MAX; i++)
                {
                    deqA.push_back(static_cast<double>(cos(i)));
                }
                t2 = clock();
                std::cout << "    --> done with deque --> dequeA --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
        }

        //======================//
        // CLEAR using pop_back //
        //======================//

        std::cout<< std::endl;
        std::cout << " CLEAR using pop_back()" << std::endl;
        std::cout<< std::endl;

        #pragma omp parallel sections default(none)\
        shared(vecA, deqA, listA, std::cout)\
        private(t1, t2, i)
        {
            #pragma omp section
            {
                // 6. pop_back the vector

                t1 = clock();
                std::cout << "  4 --> pop_back the vector --> vecA" << std::endl;
                for (i = 0; i < I_MAX; i++)
                {
                    vecA.pop_back();
                }
                t2 = clock();
                std::cout << "    --> done with vector --> vecA --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 7. pop_back the list

                t1 = clock();
                std::cout << "  5 --> pop_back the list --> listA" << std::endl;
                for (i = 0; i < I_MAX; i++)
                {
                    listA.pop_back();
                }
                t2 = clock();
                std::cout << "    --> done with list --> listA --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 8. pop_back the deque

                t1 = clock();
                std::cout << "  6 --> pop_back the deque --> dequeA" << std::endl;
                for (i = 0; i < I_MAX; i++)
                {
                    deqA.pop_back();
                }
                t2 = clock();
                std::cout << "    --> done with deque --> dequeA --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
        }

        // 9. get the sizes of the containers

        std::cout << std::endl;
        std::cout << " --> vecA.size()   --> " << vecA.size()   << std::endl;
        std::cout << " --> listA.size()  --> " << listA.size()  << std::endl;
        std::cout << " --> dequeA.size() --> " << deqA.size() << std::endl;
        std::cout << std::endl;

        //========================//
        // BUILD using push_front //
        //========================//

        std::cout<< std::endl;
        std::cout << " BUILD using push_front()" << std::endl;
        std::cout<< std::endl;

        #pragma omp parallel sections default(none)\
        shared(vecA, deqA, listA, std::cout)\
        private(t1, t2, i)
        {
            #pragma omp section
            {
                // 10. building the vector

                t1 = clock();
                std::cout << "  7 --> building the vector --> vecA" << std::endl;
                for (i = 0; i < I_MAX; i++)
                {
                    vecA.push_back(static_cast<double>(cos(i)));
                }
                t2 = clock();
                std::cout << "    --> done with vector --> vecA --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 11. building the list

                t1 = clock();
                std::cout << "  8 --> building the list --> push_front --> listA" << std::endl;
                for (i = 0; i < I_MAX; i++)
                {
                    listA.push_front(static_cast<double>(cos(i)));
                }
                t2 = clock();
                std::cout << "    --> done with list --> listA --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 12. building the deque

                t1 = clock();
                std::cout << "  9 --> building the deque --> push_front --> dequeA" << std::endl;
                for (i = 0; i < I_MAX; i++)
                {
                    deqA.push_front(static_cast<double>(cos(i)));
                }
                t2 = clock();
                std::cout << "    --> done with deque --> dequeA --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
        }

        //=======================//
        // CLEAR using pop_front //
        //=======================//

        std::cout<< std::endl;
        std::cout << " CLEAR using pop_front()" << std::endl;
        std::cout<< std::endl;

        #pragma omp parallel sections default(none)\
        shared(vecA, deqA, listA, std::cout)\
        private(t1, t2, i)
        {
            #pragma omp section
            {
                // 13. pop_back the vector

                t1 = clock();
                std::cout << " 10 --> pop_back the vector --> vecA" << std::endl;
                for (i = 0; i < I_MAX; i++)
                {
                    vecA.pop_back();
                }
                t2 = clock();
                std::cout << "    --> done with vector --> vecA --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 14. pop_front the list

                t1 = clock();
                std::cout << " 11 --> pop_front the list --> listA" << std::endl;
                for (i = 0; i < I_MAX; i++)
                {
                    listA.pop_front();
                }
                t2 = clock();
                std::cout << "    --> done with list --> listA --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 15. pop_front the deque

                t1 = clock();
                std::cout << " 12 --> pop_front the deque --> dequeA" << std::endl;
                for (i = 0; i < I_MAX; i++)
                {
                    deqA.pop_front();
                }
                t2 = clock();
                std::cout << "    --> done with deque --> dequeA --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
        }

        // 16. get the sizes of the containers

        std::cout << std::endl;
        std::cout << " --> vecA.size()   --> " << vecA.size() << std::endl;
        std::cout << " --> listA.size()  --> " << listA.size() << std::endl;
        std::cout << " --> dequeA.size() --> " << deqA.size() << std::endl;
        std::cout << std::endl;

        //=======================//
        // BUILD using push_back //
        //=======================//

        std::cout<< std::endl;
        std::cout << " BUILD using push_back()" << std::endl;
        std::cout<< std::endl;

        #pragma omp parallel sections default(none)\
        shared(vecA, deqA, listA, std::cout)\
        private(t1, t2, i)
        {
            #pragma omp section
            {
                // 17. building the vector

                t1 = clock();
                std::cout << " 13 --> building the vector --> vecA" << std::endl;
                for (i = 0; i < I_MAX; i++)
                {
                    vecA.push_back(static_cast<double>(cos(i)));
                }
                t2 = clock();
                std::cout << "    --> done with vector --> vecA --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 18. building the list

                t1 = clock();
                std::cout << " 14 --> building the list --> listA" << std::endl;
                for (i = 0; i < I_MAX; i++)
                {
                    listA.push_back(static_cast<double>(cos(i)));
                }
                t2 = clock();
                std::cout << "    --> done with list --> listA --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 19. building the deque

                t1 = clock();
                std::cout << " 15 --> building the deque --> dequeA" << std::endl;
                for (i = 0; i < I_MAX; i++)
                {
                    deqA.push_back(static_cast<double>(cos(i)));
                }
                t2 = clock();
                std::cout << "    --> done with deque --> dequeA --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
        }

        //===================//
        // CLEAR using clear //
        //===================//

        std::cout<< std::endl;
        std::cout << " CLEAR using clear()" << std::endl;
        std::cout<< std::endl;

        #pragma omp parallel sections default(none)\
        shared(vecA, deqA, listA, std::cout)\
        private(t1, t2)
        {
            #pragma omp section
            {
                // 20. clear the vector

                t1 = clock();
                std::cout << " 16 --> clear the vector --> vecA" << std::endl;
                vecA.clear();
                t2 = clock();
                std::cout << "    --> done with vector --> vecA --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 21. clear the list

                t1 = clock();
                std::cout << " 17 --> clear the list --> listA" << std::endl;
                listA.clear();
                t2 = clock();
                std::cout << "    --> done with list --> listA --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 22. clear the deque

                t1 = clock();
                std::cout << " 18 --> clear the deque --> dequeA" << std::endl;
                deqA.clear();
                t2 = clock();
                std::cout << "    --> done with deque --> dequeA --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
        }

        // 23. get the sizes of the containers

        std::cout << std::endl;
        std::cout << " --> vecA.size()   --> " << vecA.size()   << std::endl;
        std::cout << " --> listA.size()  --> " << listA.size()  << std::endl;
        std::cout << " --> dequeA.size() --> " << deqA.size() << std::endl;
        std::cout << std::endl;

        //===============//
        // shrink_to_fit //
        //===============//

        std::cout<< std::endl;
        std::cout << " SHRINK TO FIT using shrink_to_fit()" << std::endl;
        std::cout<< std::endl;

        #pragma omp parallel sections default(none)\
        shared(vecA, deqA, listA, std::cout)\
        private(t1, t2)
        {
            #pragma omp section
            {
                // 24. shrink_to_fit the vector

                t1 = clock();
                std::cout << " 19 --> shrink_to_fit the vector --> vecA" << std::endl;
                vecA.shrink_to_fit();
                t2 = clock();
                std::cout << "    --> done with vector --> vecA --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 25. clear the list

                t1 = clock();
                std::cout << " 20 --> clear the list --> listA" << std::endl;
                listA.clear();
                t2 = clock();
                std::cout << "    --> done with list --> listA --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 26. shrink_to_fit the deque

                t1 = clock();
                std::cout << " 21 --> shrink_to_fit the deque --> dequeA" << std::endl;
                deqA.shrink_to_fit();
                t2 = clock();
                std::cout << "    --> done with deque --> dequeA --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
        }

        //=======================//
        // BUILD using push_back //
        //=======================//

        std::cout<< std::endl;
        std::cout << " BUILD using push_back()" << std::endl;
        std::cout<< std::endl;

        #pragma omp parallel sections default(none)\
        shared(vecA, deqA, listA, std::cout)\
        private(t1, t2, i)
        {
            #pragma omp section
            {
                // 27. building the vector

                t1 = clock();
                std::cout << " 22 --> building the vector --> vecA" << std::endl;
                for (i = 0; i < I_MAX; i++)
                {
                    vecA.push_back(static_cast<double>(cos(i)));
                }
                t2 = clock();
                std::cout << "    --> done with vector --> vecA --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 28. building the list

                t1 = clock();
                std::cout << " 23 --> building the list --> listA" << std::endl;
                for (i = 0; i < I_MAX; i++)
                {
                    listA.push_back(static_cast<double>(cos(i)));
                }
                t2 = clock();
                std::cout << "    --> done with list --> listA --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 29. building the deque

                t1 = clock();
                std::cout << " 24 --> building the deque --> dequeA" << std::endl;
                for (i = 0; i < I_MAX; i++)
                {
                    deqA.push_back(static_cast<double>(cos(i)));
                }
                t2 = clock();
                std::cout << "    --> done with deque --> dequeA --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
        }

        //===================//
        // CLEAR using erase //
        //===================//

        std::cout<< std::endl;
        std::cout << " CLEAR using erase()" << std::endl;
        std::cout<< std::endl;

        #pragma omp parallel sections default(none)\
        shared(vecA, deqA, listA, std::cout)\
        private(t1, t2)
        {
            #pragma omp section
            {
                // 30. erasing the vector

                t1 = clock();
                std::cout << " 25 --> erasing the vector --> vecA" << std::endl;
                vecA.erase(vecA.begin(), vecA.end());
                t2 = clock();
                std::cout << "    --> done with vector --> vecA --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 31. erasing the list

                t1 = clock();
                std::cout << " 26 --> erasing the list --> listA" << std::endl;
                listA.erase(listA.begin(), listA.end());
                t2 = clock();
                std::cout << "    --> done with list --> listA --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 32. erasing the deque

                t1 = clock();
                std::cout << " 27 --> erasing the deque --> dequeA" << std::endl;
                deqA.erase(deqA.begin(), deqA.end());
                t2 = clock();
                std::cout << "    --> done with deque --> dequeA --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
        }

        //===============//
        // shrink_to_fit //
        //===============//

        std::cout<< std::endl;
        std::cout << " SHRINK TO FIT using shrink_to_fit" << std::endl;
        std::cout<< std::endl;

        #pragma omp parallel sections default(none)\
        shared(vecA, deqA, listA, std::cout)\
        private(t1, t2)
        {
            #pragma omp section
            {
                // 33. shrink_to_fit the vector

                t1 = clock();
                std::cout << " 28 --> shrink_to_fit the vector --> vecA" << std::endl;
                vecA.shrink_to_fit();
                t2 = clock();
                std::cout << "    --> done with vector --> vecA --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 34. clear the list

                t1 = clock();
                std::cout << " 29 --> clear the list --> listA" << std::endl;
                listA.clear();
                t2 = clock();
                std::cout << "    --> done with list --> listA --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 35. shrink_to_fit the deque

                t1 = clock();
                std::cout << " 30 --> shrink_to_fit the deque --> dequeA" << std::endl;
                deqA.shrink_to_fit();
                t2 = clock();
                std::cout << "    --> done with deque --> dequeA --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
        }
    }

    int sentinel;
    std::cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
