
//=====================//
// DerivedDefinition2.h //
//=====================//

#ifndef DERIVEDDEFINITION2_H
#define DERIVEDDEFINITION2_H

#include <string>
#include "BaseHeader.h"
#include "BaseDefinition.h"
#include "DerivedHeader.h"
#include "DerivedDefinition.h"
#include "DerivedHeader2.h"

using std::string;
using pgg::base;
using pgg::derived;
using pgg::derived2;

namespace pgg {
// default constructor

derived2::derived2() : derived(), ticketType ("INVALID") {}

// non-default constructor

derived2::derived2(int num, double price, string season, string type)
    : derived(num, price, season), ticketType (type) {}

// member function

string derived2::getTicketType() const
{
    return ticketType;
}

// member function

void derived2::setTicketType(string type)
{
    ticketType = type;
}

// member function

void derived2::resetTicket()
{
    derived::resetTicket();
    ticketType = "INVALID";
}
} // pgg

#endif // DERIVEDDEFINITION2_H

//======//
// FINI //
//======//
