//======================//
// DiscountSaleHeader.h //
//======================//

#ifndef DISCOUNTSALE_H
#define DISCOUNTSALE_H

#include "SaleHeader.h"

namespace pgg {
class DiscountSale : public Sale {
public:
    DiscountSale(); //default constructor
    DiscountSale(double, double);
    double getDiscount() const;
    void setDiscount(double);
    virtual double bill() const; // it is by default virtual
private:
    double discount;
};
} // pgg

#endif // DISCOUNTSALE_H

//======//
// FINI //
//======//
