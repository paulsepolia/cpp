//===================//
// DiscountSaleDef.h //
//===================//

#include "DiscountSaleHeader.h"

namespace pgg {
// default constructor

DiscountSale::DiscountSale() : Sale(), discount(0) {}

// non-default constructor

DiscountSale::DiscountSale(double thePrice, double theDiscount)
    : Sale(thePrice), discount(theDiscount) {}

// member function

double DiscountSale::getDiscount() const
{
    return discount;
}

// member function

void DiscountSale::setDiscount(double newDiscount)
{
    discount = newDiscount;
}

// member function

double DiscountSale::bill() const
{
    double fraction = discount / 100.0;
    return (1-fraction) * getPrice();
}
}

//======//
// FINI //
//======//
