//===================//
// DriverProgram.cpp //
//===================//

#include <iostream>
#include "SaleHeader.h"
#include "SaleDef.h"
#include "DiscountSaleHeader.h"
#include "DiscountSaleDef.h"

using std::cout;
using std::endl;
using std::ios;
using namespace pgg;

int main()
{
    Sale simple(10.00); // one item at $10.00
    DiscountSale discount(11.00, 10); // one item at $11.00 with a 10% discount

    cout.setf(ios::fixed);
    cout.setf(ios::showpoint);
    cout.precision(2);

    if (discount < simple) {
        cout << "Discount item is cheaper." << endl;
        cout << "Savings is $" << simple.savings(discount) << endl;
    } else {
        cout << "Discounted item is not cheaper." << endl;
    }

    return 0;
}

//======//
// FINI //
//======//
