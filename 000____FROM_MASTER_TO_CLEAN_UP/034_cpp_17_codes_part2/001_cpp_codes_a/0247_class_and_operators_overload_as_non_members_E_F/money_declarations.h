
//=============//
// class Money //
//=============//

#include <iostream>

using std::cout;
using std::endl;

// declaration

class Money {
public:

    Money();
    Money(double); // that constructor supports automatic type conversion

    void input(const double &);
    void output() const;
    double get() const;

private:

    double valLoc;
};

//======//
// FINI //
//======//
