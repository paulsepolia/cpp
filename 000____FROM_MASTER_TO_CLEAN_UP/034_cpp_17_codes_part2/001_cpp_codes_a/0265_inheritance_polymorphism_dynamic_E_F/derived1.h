
//============================//
// Derived1 class declaration //
//============================//

#ifndef DERIVED_1_H
#define DERIVED_1_H

#include "base.h"

namespace pgg {
class Derived1 : public Base {
public:

    Derived1();
    virtual ~Derived1();
    virtual void setDim(long);
    virtual long getDim() const;
    virtual void reserve();
    virtual void build(double);
    virtual void buildRandom(int);
    virtual void destroy();
    virtual void setEqual(const Derived1&);
    virtual double getElementAlpha(long) const;
    virtual double getElementBeta(long) const;
    virtual double getElementAlpha1(long) const;
    virtual double getElementBeta1(long) const;
    virtual void setElement(double, long);
    virtual bool isEqual(const Derived1&) const;
    virtual Derived1 & operator = (const Derived1&); // = operator overload
    Derived1(const Derived1&); // copy constructor

    // friend functions

    friend bool operator == (const Derived1&, const Derived1&);

    // +, -, *, /

    friend const Derived1 operator + (const Derived1&, const Derived1&);
    friend const Derived1 operator - (const Derived1&, const Derived1&);
    friend const Derived1 operator * (const Derived1&, const Derived1&);
    friend const Derived1 operator / (const Derived1&, const Derived1&);

    // +=, -=, *=, /=

    friend const Derived1& operator += (Derived1&, const Derived1&);
    friend const Derived1& operator -= (Derived1&, const Derived1&);
    friend const Derived1& operator *= (Derived1&, const Derived1&);
    friend const Derived1& operator /= (Derived1&, const Derived1&);

    // -, ++, --

    friend const Derived1 operator -  (const Derived1&);
    friend const Derived1 operator ++ (Derived1&);
    friend const Derived1 operator ++ (Derived1&, int);
    friend const Derived1 operator -- (Derived1&);
    friend const Derived1 operator -- (Derived1&, int);

private:

    long dim1;
    double * a1;
    double * b1;
};

} // pgg

#endif // DERIVED_1_H

//======//
// FINI //
//======//
