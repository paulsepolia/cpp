
//=======================//
// base class definition //
//=======================//

#include <cstdlib>
#include <cmath>
#include "base.h"

using std::rand;
using std::srand;

namespace pgg {
// default constructor

Base::Base() : dim(0), a(NULL), b(NULL) {}

// virtual destructor

Base::~Base()
{
    delete [] a;
    delete [] b;

    a = NULL;
    b = NULL;
}

// member function

void Base::setDim(long theDim)
{
    dim = theDim;
}

// member function

long Base::getDim() const
{
    return dim;
}

// member function

void Base::reserve()
{
    a = new double [dim];
    b = new double [dim];
}

// member function

void Base::build(double theElem)
{
    for (long i = 0; i < dim; i++) {
        a[i] = theElem;
        b[i] = theElem;
    }
}

// member function

void Base::buildRandom(int theSeed)
{
    srand(theSeed);

    double theElem = static_cast<double>(rand());

    for (long i = 0; i < dim; i++) {
        a[i] = theElem;
        b[i] = theElem;
    }
}

// member function

void Base::destroy()
{
    delete [] a;
    delete [] b;

    a = NULL;
    b = NULL;

    dim = 0;
}

// member function

void Base::setEqual(const Base& objA)
{
    this->destroy();
    this->setDim(objA.dim);
    this->reserve();

    for (long i = 0; i < objA.dim; i++) {
        this->a[i] = objA.a[i];
        this->b[i] = objA.b[i];
    }
}

// member function

double Base::getElementAlpha(long index) const
{
    return a[index];
}

// member function

double Base::getElementBeta(long index) const
{
    return b[index];
}

// member function

void Base::setElement(double theElem, long index)
{
    a[index] = theElem;
    b[index] = -theElem;
}

// member function

bool Base::isEqual(const Base& objA) const
{
    if (objA.dim != this->dim) {
        return false;
    }

    for (long i = 0; i != objA.dim; i++) {
        if (objA.a[i] != this->a[i]) {
            return false;
        }

        if (objA.b[i] != this->b[i]) {
            return false;
        }
    }

    return true;
}

// member function
// = operator overload

Base & Base::operator = (const Base & objA)
{
    this->setEqual(objA);

    return *this;
}


// copy constructor

Base::Base(const Base& objA) : dim(objA.dim), a(NULL), b(NULL)
{
    a = new double [dim];
    b = new double [dim];

    for (long i = 0; i < dim; i++) {
        a[i] = objA.a[i];
        b[i] = objA.b[i];
    }
}

// friend function
// operator overload ==

bool operator == (const Base& objA1, const Base& objA2)
{
    return (objA1.isEqual(objA2));
}

// friend function
// operator overload +

const Base operator + (const Base& objA1, const Base& objA2)
{
    Base objA3;

    objA3.setDim(objA1.dim);

    objA3.reserve();

    for (long i = 0; i < objA1.dim; i++) {
        objA3.setElement(objA1.a[i] + objA2.a[i], i);
    }

    return Base(objA3);
}

// friend function
// operator overload -

const Base operator - (const Base& objA1, const Base& objA2)
{
    Base objA3;

    objA3.setDim(objA1.dim);

    objA3.reserve();

    for (long i = 0; i < objA1.dim; i++) {
        objA3.setElement(objA1.a[i] - objA2.a[i], i);
    }

    return Base(objA3);
}

// friend function
// operator overload *

const Base operator * (const Base& objA1, const Base& objA2)
{
    Base objA3;

    objA3.setDim(objA1.dim);

    objA3.reserve();

    for (long i = 0; i < objA1.dim; i++) {
        objA3.setElement(objA1.a[i] * objA2.a[i], i);
    }

    return Base(objA3);
}

// friend function
// operator overload /

const Base operator / (const Base& objA1, const Base& objA2)
{
    Base objA3;

    objA3.setDim(objA1.dim);

    objA3.reserve();

    for (long i = 0; i < objA1.dim; i++) {
        objA3.setElement(objA1.a[i] / objA2.a[i], i);
    }

    return Base(objA3);
}

// friend function
// operator overload +=

const Base& operator += (Base& objA1, const Base& objA2)
{
    objA1 = objA1 + objA2;

    return objA1;
}

// friend function
// operator overload -=

const Base& operator -= (Base& objA1, const Base& objA2)
{
    objA1 = objA1 - objA2;

    return objA1;
}

// friend function
// operator overload *=

const Base& operator *= (Base& objA1, const Base& objA2)
{
    objA1 = objA1 * objA2;

    return objA1;
}

// friend function
// operator overload /=

const Base& operator /= (Base& objA1, const Base& objA2)
{
    objA1 = objA1 / objA2;

    return objA1;
}

// friend function
// unary operator overload -

const Base operator - (const Base& objA1)
{
    Base objA3;

    objA3.setDim(objA1.dim);

    objA3.reserve();

    for (long i = 0; i < objA1.dim; i++) {
        objA3.setElement(-objA1.a[i], i);
    }

    return Base(objA3);
}

// friend function
// operator overload ++ (prefix)

const Base operator ++ (Base& objA1)
{
    Base objA3;

    objA3.setDim(objA1.dim);

    objA3.reserve();

    for (long i = 0; i < objA1.dim; i++) {
        objA3.setElement(++objA1.a[i], i);
    }

    return Base(objA3);
}

// friend function
// operator overload ++ (postfix)

const Base operator ++ (Base& objA1, int)
{
    Base objA3;

    objA3.setDim(objA1.dim);

    objA3.reserve();

    for (long i = 0; i < objA1.dim; i++) {
        objA3.setElement(++objA1.a[i], i);
    }

    return Base(objA3);
}

// friend function
// operator overload -- (prefix)

const Base operator -- (Base& objA1)
{
    Base objA3;

    objA3.setDim(objA1.dim);

    objA3.reserve();

    for (long i = 0; i < objA1.dim; i++) {
        objA3.setElement(--objA1.a[i], i);
    }

    return Base(objA3);
}

// friend function
// operator overload -- (postfix)

const Base operator -- (Base& objA1, int)
{
    Base objA3;

    objA3.setDim(objA1.dim);

    objA3.reserve();

    for (long i = 0; i < objA1.dim; i++) {
        objA3.setElement(--objA1.a[i], i);
    }

    return Base(objA3);
}

} // pgg

//======//
// FINI //
//======//

