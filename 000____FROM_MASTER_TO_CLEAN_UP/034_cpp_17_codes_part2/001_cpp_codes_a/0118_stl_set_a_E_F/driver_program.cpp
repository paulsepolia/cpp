//==========//
// STL: set //
//==========//

#include <iostream>
#include <set>
#include <iomanip>
#include <ctime>
#include <cmath>
#include <ctime>

int main ()
{
    const long int DIM_A = static_cast<long int>(pow(10.0, 7.5));
    const long int K_MAX = static_cast<long int>(pow(10.0, 5.0));
    long int i;
    long int k;
    clock_t t1;
    clock_t t2;

    // set the outpot format

    std::cout << std::fixed;
    std::cout << std::setprecision(5);
    std::cout << std::showpoint;

    for (k = 0 ; k < K_MAX; k++) {
        // counter

        std::cout << "-------------------------------------------------->> " << k << std::endl;

        // build the array of doubles

        std::cout << " --> 1 --> build the dynamic array --> arrayA" << std::endl;
        t1 = clock();
        double* arrayA = new double [DIM_A];
        for (i = 0; i < DIM_A; i++) {
            arrayA[i] = cos(static_cast<double>(cos(i)));
        }
        t2 = clock();
        std::cout << " --> done --> " << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;

        // declare the set

        std::set<double> *dSetA = new std::set<double> [1];

        // fill the set with the elements of the array

        t1 = clock();
        std::cout << " --> 2 --> build the set --> (*dSetA).insert(arrayA, arrayA+DIM_A);" << std::endl;
        (*dSetA).insert(arrayA, arrayA+DIM_A);
        t2 = clock();
        std::cout << " --> done --> " << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;

        // delete the set

        t1 = clock();
        std::cout << " --> 3 --> delete the set --> delete [] dSetA;" << std::endl;
        delete [] dSetA;
        t2 = clock();
        std::cout << " --> done --> " << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;

        // delete the array

        t1 = clock();
        std::cout << " --> 4 --> delete the array --> delete [] arrayA;" << std::endl;
        delete [] arrayA;
        t2 = clock();
        std::cout << " --> done --> " << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
    }

    return 0;
}

//======//
// FINI //
//======//
