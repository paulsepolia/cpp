//==================//
// VectorPaul class //
//==================//

#include "IncludeLibs.h"

// a class

template <typename T>
class VectorPaul {
private:

    // 1.

    long dim;
    T * p1;
    static int RandomNumber();

    // 2.

    struct c_unique {
        int current;
        c_unique() : current (0) { }
        int operator()()
        {
            return ++current;
        }
    } UniqueNumber;

    // 3.

    static int compareFun(const void * a, const void * b);

public:

    // constructors

    VectorPaul<T>() : dim (0), p1 (NULL), UniqueNumber() { };

    // member functions here

    void addVector(const VectorPaul<T>&, const VectorPaul<T>&);
    void allocateVector();
    void deleteVector();
    void fillVector(T&);
    void generateVectorRandom();
    void generateVectorUnique();
    T getVector(long) const;
    long getDim();
    bool isSortedVector();
    T maxElementVector();
    T minElementVector();
    void randomShuffleVector();
    void reverseVector();
    void setDim(long);
    void setEqualTo(const VectorPaul<T>&);
    void setVector(long, T);
    void sortVector();
    void sortVectorQSort();
    void subtractVector(const VectorPaul<T>&, const VectorPaul<T>&);

    // overloaded operators here

    VectorPaul<T> & operator = (const VectorPaul<T>&);
    VectorPaul<T>   operator + (const VectorPaul<T>&) const;
    VectorPaul<T>   operator - (const VectorPaul<T>&) const; // THI IS WRONG!
};

//======//
// FINI //
//======//
