//================//
// common headers //
//================//

#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <iomanip>
#include <iostream>
#include <thread>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::fixed;
using std::generate;
using std::is_sorted;
using std::max_element;
using std::min_element;
using std::qsort;
using std::rand;
using std::random_shuffle;
using std::reverse;
using std::setprecision;
using std::showpoint;
using std::showpos;
using std::sort;
using std::srand;
using std::thread;
using std::time;
using std::vector;

//======//
// FINI //
//======//
