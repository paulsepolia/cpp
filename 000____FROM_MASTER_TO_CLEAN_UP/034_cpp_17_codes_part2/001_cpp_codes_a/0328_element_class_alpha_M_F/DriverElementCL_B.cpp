//========================================//
// Driver program to the ElementCL class. //
//========================================//

#include <iostream>
#include <iomanip>
#include <cmath>
#include "ElementCL.h"
#include "ElementCLMemberFunctions.h"
#include "TypeDefinitions.h"

using namespace std;

int main()
{
    //  1. local constants

    const TB DIM  = 1E4;
    const TB MAXT = 1E3;
    const TA ZERO = 00.00000000000000000000;
    const TA AA   = 11.11111111111111111111;
    const TA BB   = 22.22222222222222222222;

    //  2. local variables

    TB i;
    TB j;
    TA atmp;
    TA btmp;
    TA ctmp;
    TA dtmp;

    //  3. local objects

    ElementCL<TA>* oArrayA;
    oArrayA = new ElementCL<TA> [DIM];

    ElementCL<TA>* oArrayB;
    oArrayB = new ElementCL<TA> [DIM];

    ElementCL<TA> oA(AA);
    ElementCL<TA> oB(BB);

    //  4. setting for the output format

    cout << setprecision(20) << fixed << endl;

    //  5. tests

    //  6. "=" test

    for (i = 0; i < DIM; i++) {
        oArrayA[i] = oA;
        oArrayB[i] = oB;
    }

    cout << "  1 -->  Must be " << AA << endl;
    cout << "  2 -->       Is " << oArrayA[0].GetElementF() << endl;
    cout << "  3 -->  Must be " << BB << endl;
    cout << "  4 -->       Is " << oArrayB[0].GetElementF() << endl;

    //  7. "+=" test
    //     Some round-off errors occur

    oA.SetElementF(ZERO);
    oB.SetElementF(ZERO);

    for (j = 0; j < MAXT; j++) {
        for (i = 0; i < DIM; i++) {
            oA += oArrayA[i];
            oB += oArrayB[i];
        }
    }

    atmp = oA.GetElementF();
    btmp = oB.GetElementF();

    ctmp = MAXT * DIM * oArrayA[0].GetElementF();
    dtmp = MAXT * DIM * oArrayB[0].GetElementF();

    cout << "  5 -->  Must be " << ctmp << endl;
    cout << "  6 -->       Is " << atmp << endl;
    cout << "  7 -->  Must be " << dtmp << endl;
    cout << "  8 -->       Is " << btmp << endl;
    cout << "  9 --> Error is " << abs(atmp - ctmp) << endl;
    cout << " 10 --> Error is " << abs(btmp - dtmp) << endl;

    //  8. "-=" test
    //     Some round-off errors occur

    oA.SetElementF(ZERO);
    oB.SetElementF(ZERO);

    for (j = 0; j < MAXT; j++) {
        for (i = 0; i < DIM; i++) {
            oA -= oArrayA[i];
            oB -= oArrayB[i];
        }
    }

    atmp = oA.GetElementF();
    btmp = oB.GetElementF();

    ctmp = MAXT * DIM * oArrayA[0].GetElementF();
    dtmp = MAXT * DIM * oArrayB[0].GetElementF();

    cout << " 11 -->  Must be " << abs(ctmp) << endl;
    cout << " 12 -->       Is " << abs(atmp) << endl;
    cout << " 13 -->  Must be " << abs(dtmp) << endl;
    cout << " 14 -->       Is " << abs(btmp) << endl;
    cout << " 15 --> Error is " << abs(atmp + ctmp) << endl;
    cout << " 16 --> Error is " << abs(btmp + dtmp) << endl;

    // 4. "+=" and "-=" test
    //    Some round-off errors occur

    oA.SetElementF(ZERO);
    oB.SetElementF(ZERO);

    for (j = 0; j < MAXT; j++) {
        for (i = 0; i < DIM; i++) {
            oA += oArrayA[i];
            oB += oArrayB[i];
        }
    }

    for (j = 0; j < MAXT; j++) {
        for (i = 0; i < DIM; i++) {
            oA -= oArrayA[i];
            oB -= oArrayB[i];
        }
    }

    atmp = oA.GetElementF();
    btmp = oB.GetElementF();

    cout << " 17 -->  Must be " << ZERO << endl;
    cout << " 18 -->       Is " << atmp << endl;
    cout << " 19 -->  Must be " << ZERO << endl;
    cout << " 20 -->       Is " << btmp << endl;
    cout << " 21 --> Error is " << abs(atmp) << endl;
    cout << " 22 --> Error is " << abs(btmp) << endl;

    // XX. Exiting

    cout << endl;
    return 0;
}

//======//
// FINI //
//======//
