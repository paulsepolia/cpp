//=================//
// class templates //
//=================//

#include <iostream>

// a class template

template <class T>
class mypair {
public:

    mypair ()
    {
        a = 0;     // constructor
        b = 0;
    }

    mypair (T first, T second)
    {
        a = first;     // constructor
        b = second;
    }

    T getmax (); // member function declaration

private:

    T a; // private variable
    T b; // private variable
};

// member function definition

template <class T>
T mypair<T>::getmax ()
{
    T retval;
    retval = a > b ? a : b;

    return retval;
}

// the main function

int main ()
{
    mypair <int> myobjectA (100, 75);
    mypair <int> myobjectB (10, 10);
    mypair <double> myobjectC (11.1, 11.2);
    mypair <double> myobjectD;

    std::cout << myobjectA.getmax() << std::endl;
    std::cout << myobjectB.getmax() << std::endl;
    std::cout << myobjectC.getmax() << std::endl;
    std::cout << myobjectD.getmax() << std::endl;

    int sentinel;
    std::cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

