//====================//
// benchmark function //
//====================//

#include "includes.h"

// the void benchmark function

void bench_fun(const long I_MAX)
{
    // variables and parameters

    long int i;
    clock_t t1;
    clock_t t2;
    double tall;
    double tAll1;
    double tAll2;
    double tAll3;
    double tTotal;

    // set the output

    cout << fixed;
    cout << setprecision(5);
    cout << showpoint;

    // main bench loop

    list<double>   * listA  = new list<double>;
    vector<double> * vecA   = new vector<double>;
    deque<double>  * deqA   = new deque<double>;

    // build the vector

    t1 = clock();

    cout << "  1 --> build the vector ..." << endl;

    for (i = 0; i < I_MAX; i++) {
        vecA->push_back(static_cast<double>(cos(i)));
    }

    t2 = clock();

    tall = (t2-t1)/static_cast<double>(CLOCKS_PER_SEC);

    cout << "  2 --> done with the vector       --> " << tall << endl;

    // build the list

    t1 = clock();

    cout << "  3 --> build the list ..." << endl;

    for (i = 0; i < I_MAX; i++) {
        listA->push_back(static_cast<double>(cos(i)));
    }

    t2 = clock();

    tall = (t2-t1)/static_cast<double>(CLOCKS_PER_SEC);

    cout << "  4 --> done with the list         --> " << tall << endl;

    // build the deque

    t1 = clock();

    cout << "  5 --> build the deque ..." << endl;

    for (i = 0; i < I_MAX; i++) {
        deqA->push_back(static_cast<double>(cos(i)));
    }

    t2 = clock();

    tall = (t2-t1)/static_cast<double>(CLOCKS_PER_SEC);

    cout << "  6 --> done with the deque        --> " << tall << endl;

    // sort the vector

    t1 = clock();

    cout << "  7 --> sort the vector ..." << endl;

    sort(vecA->begin(), vecA->end());

    t2 = clock();

    tall = (t2-t1)/static_cast<double>(CLOCKS_PER_SEC);

    cout << "  8 --> done with the vector       --> " << tall << endl;

    sort(vecA->begin(), vecA->end());

    // sort the list

    t1 = clock();

    cout << "  9 --> sort the list ..." << endl;

    listA->sort();

    t2 = clock();

    tall = (t2-t1)/static_cast<double>(CLOCKS_PER_SEC);

    cout << " 10 --> done with the list         --> " << tall << endl;

    // sort the deque

    t1 = clock();

    cout << " 11 --> sort the deque ..." << endl;

    sort(deqA->begin(), deqA->end());

    t2 = clock();

    tall = (t2-t1)/static_cast<double>(CLOCKS_PER_SEC);

    cout << " 12 --> done with the deque        --> " << tall << endl;

    // delete the vector

    t1 = clock();

    cout << " 13 --> delete the vector ..." << endl;

    delete  vecA;

    t2 = clock();

    tAll1 = (t2-t1)/static_cast<double>(CLOCKS_PER_SEC);

    cout << " 14 --> done with the vector       --> " << tAll1 << endl;

    // delete the list

    t1 = clock();

    cout << " 15 --> delete the list ..." << endl;

    delete  listA;

    t2 = clock();

    tAll2 = (t2-t1)/static_cast<double>(CLOCKS_PER_SEC);

    cout << " 16 --> done with the list         --> " << tAll2 << endl;

    t1 = clock();

    // delete the deque

    cout << " 17 --> delete the deque ..." << endl;

    delete  deqA;

    t2 = clock();

    tAll3 = (t2-t1)/static_cast<double>(CLOCKS_PER_SEC);

    cout << " 18 --> done with the deque        --> " << tAll3 << endl;

    tTotal = tAll1 + tAll2 + tAll3;

    cout << " 19 --> total deallocation of heap --> " << tTotal << endl;
}

//======//
// FINI //
//======//
