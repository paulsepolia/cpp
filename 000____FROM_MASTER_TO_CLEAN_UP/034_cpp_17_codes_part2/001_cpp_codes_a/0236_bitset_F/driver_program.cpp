
//================//
// bitset example //
//================//

#include <iostream>
#include <bitset>
#include <string>

using std::cout;
using std::cin;
using std::endl;
using std::boolalpha;
using std::noboolalpha;
using std::bitset;
using std::string;

// the main function

int main ()
{
    bitset<32> foo ( string("11111111111111111111111111111111"));

    cout << boolalpha;

    cout << " --> foo.all()  = " << foo.all()  << endl;
    cout << " --> foo.any()  = " << foo.any()  << endl;
    cout << " --> foo.none() = " << foo.none() << endl;

    cout << noboolalpha;

    cout << " --> foo.all()  = " << foo.all()  << endl;
    cout << " --> foo.any()  = " << foo.any()  << endl;
    cout << " --> foo.none() = " << foo.none() << endl;


    cout << " --> foo.set()  = " << foo.set() << endl;

    cout << " --> foo.set(0) = " << foo.set(0,0) << endl;
    cout << " --> foo.set(1) = " << foo.set(1,0) << endl;
    cout << " --> foo.set(2) = " << foo.set(2,0) << endl;
    cout << " --> foo.set(3) = " << foo.set(3,0) << endl;
    cout << " --> foo.set(4) = " << foo.set(4,0) << endl;

    cout << noboolalpha;

    cout << " --> foo.all()  = " << foo.all()  << endl;
    cout << " --> foo.any()  = " << foo.any()  << endl;
    cout << " --> foo.none() = " << foo.none() << endl;

    cout << " --> foo.flip(1) = " << foo.flip(1) << endl;
    cout << " --> foo.flip(2) = " << foo.flip(2) << endl;
    cout << " --> foo.flip(3) = " << foo.flip(3) << endl;
    cout << " --> foo.flip(4) = " << foo.flip(4) << endl;

    bitset<40> mybits;          // mybits: 0000
    mybits.set();              // mybits: 1111

    string mystring = mybits.to_string<char, string::traits_type,string::allocator_type>();

    std::cout << "mystring: " << mystring << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
