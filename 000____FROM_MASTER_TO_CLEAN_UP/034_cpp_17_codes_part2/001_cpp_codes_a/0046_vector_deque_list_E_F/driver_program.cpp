//================================//
// Author: Pavlos G. Galiatsatos  //
// Date: 2013/10/23               //
// Functions: vector, deque, list //
//================================//

#include <iostream>
#include <vector>
#include <deque>
#include <list>

using namespace std;

// main function
int main()
{
    // variable declaration

    vector<double> aVector;
    deque<double> aDeque;
    list<double> aList;
    const long int I_MAX = 100000;
    const long int I_MAX_TWO_V =   2 * static_cast<long int>(100000);
    const long int I_MAX_TWO_D = 500 * static_cast<long int>(100000);
    const long int I_MAX_TWO_L = 500 * static_cast<long int>(100000);
    long int i;
    const double tmpA = 200.0;
    vector<double>::iterator itV;
    deque<double>::iterator itD;
    list<double>::iterator itL;

    // build the vector

    cout << " 1 --> Build the vector" << endl;

    for (i = 0; i < I_MAX; i++) {
        aVector.push_back(static_cast<double>(i));
    }

    // build the deque

    cout << " 2 --> Build the deque" << endl;

    for (i = 0; i < I_MAX; i++) {
        aDeque.push_back(static_cast<double>(i));
    }

    // build the list

    cout << " 3 --> Build the list" << endl;

    for (i = 0; i < I_MAX; i++) {
        aList.push_back(static_cast<double>(i));
    }

    // insert element into a vector at the beginning

    cout << " 4 --> Insert into the vector at the beginning - slow" << endl;

    for (i = 0; i < I_MAX_TWO_V; i++) {
        itV = aVector.begin();
        aVector.insert(itV, tmpA);
    }

    // insert elements into a deque at the beginning

    cout << " 5 --> Insert into the deque at the beginning - fast" << endl;

    for (i = 0; i < I_MAX_TWO_D; i++) {
        itD = aDeque.begin();
        aDeque.insert(itD, tmpA);
    }

    // insert elements into a list at the beginning

    cout << " 6 --> Insert into the list at the beginning - fast" << endl;

    for (i = 0; i < I_MAX_TWO_L; i++) {
        itL = aList.begin();
        aList.insert(itL, tmpA);
    }

    // insert element into a vector at the end

    cout << " 7 --> Insert into the vector at the end - fast" << endl;

    for (i = 0; i < I_MAX_TWO_V; i++) {
        itV = aVector.end();
        aVector.insert(itV, tmpA);
    }

    // insert elements into a deque at the end

    cout << " 8 --> Insert into the deque at the end - fast" << endl;

    for (i = 0; i < I_MAX_TWO_D; i++) {
        itD = aDeque.end();
        aDeque.insert(itD, tmpA);
    }

    // insert elements into a list at the end

    cout << " 9 --> Insert into the list at the end - fast" << endl;

    for (i = 0; i < I_MAX_TWO_L; i++) {
        itL = aList.end();
        aList.insert(itL, tmpA);
    }

    // erase element into a vector at the end

    cout << "10 --> Erase all the vector at once" << endl;

    aVector.erase(aVector.begin(), aVector.end());

    // erase elements into a deque at the end

    cout << "11 --> Erase all the deque at once" << endl;

    aDeque.erase(aDeque.begin(), aDeque.end());

    // erase elements into a list at the end - does not apply

    cout << "12 --> Erase all the list at once" << endl;

    aList.erase(aList.begin(), aList.end());

    return 0;
}
// end
