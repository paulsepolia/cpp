
//====================================//
// declaration of the class queue_pgg //
//====================================//

#ifndef QUEUE_PGG_H
#define QUEUE_PGG_H

#include <cstdlib>
#include <deque>

using std::size_t;
using std::deque;

template <typename T, typename CONT = deque<T> >
class queue_pgg {
public:

    queue_pgg<T, CONT>(); // default constructor
    ~queue_pgg<T, CONT>(); // destructor
    queue_pgg<T, CONT>(const queue_pgg<T, CONT> &); // copy constructor

    // member functions

    bool empty() const;
    size_t size() const;
    T & front();
    T & back();
    void push(const T &);
    void pop();
    void swap(queue_pgg<T, CONT> &);
    void emplace(const T &);

    // operators overload

    queue_pgg<T, CONT> & operator = (const queue_pgg<T, CONT> &); // assignment operator

private:

    CONT elems;
};

#endif // deque_PGG_H

//======//
// FINI //
//======//

