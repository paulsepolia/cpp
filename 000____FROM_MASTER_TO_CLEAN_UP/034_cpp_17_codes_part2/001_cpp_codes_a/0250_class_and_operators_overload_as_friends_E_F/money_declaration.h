
//=========================//
// class Money declaration //
//=========================//

#include <iostream>

using std::cout;
using std::endl;
using std::istream;
using std::ostream;

// declaration

class Money {
public:

    // constructors

    Money(); // default constructor
    Money(double); // supports automatic type conversion

    // member functions

    void input(const double &);
    void output() const;
    double get() const;

    // overloading operators as friend functions

    friend const Money & operator += (Money &, const Money &);
    friend const Money & operator -= (Money &, const Money &);
    friend const Money & operator *= (Money &, const Money &);
    friend const Money & operator /= (Money &, const Money &);

    friend const Money operator + (const Money &, const Money &);
    friend const Money operator - (const Money &, const Money &);
    friend const Money operator * (const Money &, const Money &);
    friend const Money operator / (const Money &, const Money &);

    friend bool operator == (const Money &, const Money &);
    friend bool operator <  (const Money &, const Money &);
    friend bool operator >  (const Money &, const Money &);
    friend bool operator <= (const Money &, const Money &);
    friend bool operator >= (const Money &, const Money &);

    friend const Money operator -  (const Money &);
    friend const Money operator ++ (Money &);
    friend const Money operator ++ (Money &, int);
    friend const Money operator -- (Money &);
    friend const Money operator -- (Money &, int);

    friend ostream & operator << (ostream &, const Money &);
    friend istream & operator >> (istream &, Money &);

private:

    double valLoc;
};

//======//
// FINI //
//======//

