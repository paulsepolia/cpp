// steady_clock example
#include <iostream>
#include <ctime>
#include <ratio>
#include <chrono>

int main ()
{
    // timing start

    std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();

    // do-loop

    std::cout << " --> printing out 10 milion stars...\n";
    for (long int i = 0; i < 10000000L; ++i) std::cout << "*";
    std::cout << std::endl;

    // timing ends

    std::chrono::steady_clock::time_point t2 = std::chrono::steady_clock::now();

    // get the time

    std::chrono::duration<double> time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);

    // report

    std::cout << "It took me " << time_span.count() << " seconds.";
    std::cout << std::endl;

    int sentinel;
    std::cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//