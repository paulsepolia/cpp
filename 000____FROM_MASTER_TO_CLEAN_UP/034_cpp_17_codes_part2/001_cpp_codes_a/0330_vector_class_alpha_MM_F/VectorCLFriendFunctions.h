//==================================//
// Header file.                     //
// VectorCL class friend functions. //
//==================================//

//  1. DotF.

template <typename T, typename P>
T  DotF(const VectorCL<T,P>& vecA, const VectorCL<T,P>& vecB)
{
    P i;
    T atmp = 0;
    T sum = 0;

    for (i = 0; i < vecA.m_Dim; i++) {
        atmp =  vecA.GetElementF(i) * vecB.GetElementF(i);
        sum = sum + atmp;
    }

    return sum;
}

//  2. TotalF.

template <typename T, typename P>
T  TotalF(const VectorCL<T,P>& vec)
{
    P i;
    T sum = 0;

    for (i = 0; i < vec.m_Dim; i++) {
        sum = sum + vec.GetElementF(i);
    }

    return sum;
}

//  3. MaxF.

template <typename T, typename P>
T  MaxF(const VectorCL<T,P>& vec)
{
    P i;
    T maxTmp;

    maxTmp = vec.GetElementF(0);

    for (i = 1; i < vec.m_Dim; i++) {
        if (maxTmp < vec.GetElementF(i) ) {
            maxTmp = vec.GetElementF(i);
        }
    }

    return maxTmp;
}

//  4. Min.

template <typename T, typename P>
T  MinF(const VectorCL<T,P>& vec)
{
    P i;
    T minTmp;

    minTmp = vec.GetElementF(0);

    for (i = 1; i < vec.m_Dim; i++) {
        if (minTmp > vec.GetElementF(i) ) {
            minTmp = vec.GetElementF(i);
        }
    }

    return minTmp;
}

//  5. MaxAbs.

template <typename T, typename P>
T  MaxAbsF(const VectorCL<T,P>& vec)
{
    P i;
    T maxAbsTmp;

    maxAbsTmp = fabs(vec.GetElementF(0));

    for (i = 1; i < vec.m_Dim; i++) {
        if (maxAbsTmp < fabs(vec.GetElementF(i)) ) {
            maxAbsTmp = fabs(vec.GetElementF(i));
        }
    }

    return maxAbsTmp;
}

//  6. MinAbs.

template <typename T, typename P>
T  MinAbsF(const VectorCL<T,P>& vec)
{
    P i;
    T minAbsTmp;

    minAbsTmp = fabs(vec.GetElementF(0));

    for (i = 1; i < vec.m_Dim; i++) {
        if (minAbsTmp > fabs(vec.GetElementF(i)) ) {
            minAbsTmp = fabs(vec.GetElementF(i));
        }
    }

    return minAbsTmp;
}

//==============//
// End of code. //
//==============//
