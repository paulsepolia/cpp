//=======================//
// Header file.          //
// Functions File Alpha. //
//=======================//

//  1. random_number function.

template <typename T>
inline  T random_number(T low, T high)
{
    T random_num;
    T range;

    range = high-low;
    random_num = low + (static_cast<T>(range) * rand()) / RAND_MAX;

    return random_num;
}

//==============//
// End of code. //
//==============//
