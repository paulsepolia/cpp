
//========================//
// numeric_limits example //
//========================//

#include <iostream>
#include <limits>

using std::cout;
using std::endl;
using std::cin;
using std::numeric_limits;
using std::boolalpha;

int main ()
{
    cout << boolalpha << endl;

    // int

    cout << " --> Minimum value for int --> " << numeric_limits<int>::min() << endl;
    cout << " --> Maximum value for int --> " << numeric_limits<int>::max() << endl;
    cout << " --> int is signed         --> " << numeric_limits<int>::is_signed << endl;
    cout << " --> Non-sign bits in int  --> " << numeric_limits<int>::digits << endl;
    cout << " --> int has infinity      --> " << numeric_limits<int>::has_infinity << endl;

    // long int

    cout << " --> Minimum value for long --> " << numeric_limits<long int>::min() << endl;
    cout << " --> Maximum value for long --> " << numeric_limits<long int>::max() << endl;
    cout << " --> long is signed         --> " << numeric_limits<long int>::is_signed << endl;
    cout << " --> Non-sign bits in long  --> " << numeric_limits<long int>::digits << endl;
    cout << " --> long has infinity      --> " << numeric_limits<long int>::has_infinity << endl;

    // long long int

    cout << " --> Minimum value for long long int --> " << numeric_limits<long long int>::min() << endl;
    cout << " --> Maximum value for long long int --> " << numeric_limits<long long int>::max() << endl;
    cout << " --> long long int is signed         --> "
         << numeric_limits<long long int>::is_signed << endl;
    cout << " --> Non-sign bits in long long int  --> "
         << numeric_limits<long long int>::digits << endl;
    cout << " --> long long int has infinity      --> "
         << numeric_limits<long long int>::has_infinity << endl;

    // double

    cout << " --> Minimum value for double --> " << numeric_limits<double>::min() << endl;
    cout << " --> Maximum value for double --> " << numeric_limits<double>::max() << endl;
    cout << " --> double is signed         --> " << numeric_limits<double>::is_signed << endl;
    cout << " --> Non-sign bits in double  --> " << numeric_limits<double>::digits << endl;
    cout << " --> double has infinity      --> " << numeric_limits<double>::has_infinity << endl;

    // long double

    cout << " --> Minimum value for long double --> " << numeric_limits<long double>::min() << endl;
    cout << " --> Maximum value for long double --> " << numeric_limits<long double>::max() << endl;
    cout << " --> long double is signed         --> " << numeric_limits<long double>::is_signed << endl;
    cout << " --> Non-sign bits in long double  --> " << numeric_limits<long double>::digits << endl;
    cout << " --> long double has infinity      --> " << numeric_limits<long double>::has_infinity << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
