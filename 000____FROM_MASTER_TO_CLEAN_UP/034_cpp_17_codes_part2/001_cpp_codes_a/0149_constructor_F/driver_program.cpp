//===================================//
// classes and default constructors  //
//===================================//

// you need to provide the default constructor if
// there is an agrument based constructor

#include <iostream>
#include <string>

using std::string;
using std::cout;
using std::endl;
using std::cin;

// a class

class ExampleA {
private:
    string data;
public:
    ExampleA (const string& str) : data(str) {} // a constructor
    ExampleA() {  } // default constructor
    const string& content() const
    {
        return data;     // public member function
    }
};

// the main function

int main ()
{
    ExampleA foo;
    ExampleA bar ("Example");

    cout << "bar's content: " << bar.content() << endl;
    cout << "foo's content: " << foo.content() << endl;

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

