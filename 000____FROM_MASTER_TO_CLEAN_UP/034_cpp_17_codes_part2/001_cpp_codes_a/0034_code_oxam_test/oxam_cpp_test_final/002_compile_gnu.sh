#!/bin/bash

  # 1. compile

  g++ -O3            \
      -Wc++0x-compat \
      -Wall          \
      -static        \
      oxam_test.cpp  \
      -o x_gnu
