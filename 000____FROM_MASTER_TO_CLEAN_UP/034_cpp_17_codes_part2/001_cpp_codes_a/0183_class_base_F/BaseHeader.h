
//==============//
// BaseHeader.h //
//==============//

#ifndef BASEHEADER_H
#define BASEHEADER_H

namespace pgg {
class base {
public:

    base();  // default constructor
    base(int, double); // non-default constructor

    double getTicketPrice() const;
    void setTicketPrice(double);
    int getTicketNumber() const;
    void setTicketNumber(int);
    void resetTicket();

private:

    double ticketPrice;
    int ticketNum;
};
} // pgg

#endif // BASEHEADER_H

//======//
// FINI //
//======//
