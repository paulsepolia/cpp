
//====================//
// constant iterators //
//====================//

#include <iostream>
#include <iomanip>
#include <vector>
#include <iterator>

using std::endl;
using std::cin;
using std::cout;
using std::fixed;
using std::showpos;
using std::showpoint;
using std::setprecision;
using std::vector;

// a template function

template <class T >
void printLA(const T & v)
{
    typename T::const_iterator itc;

    for (itc = v.begin(); itc != v.end(); itc++) {
        cout << *itc << endl;
    }
}

// a template function

template <class T>
void printLB(T & v)
{
    typename T::iterator it;

    for (it = v.begin(); it != v.end(); it++) {
        cout << *it << endl;
    }
}

// the main function

int main()
{
    // local variables and parameters

    const int DIM_MAX = 100;
    vector<double> vA;

    // adjust the output format

    cout << fixed;
    cout << setprecision(15);
    cout << showpos;
    cout << showpoint;

    // build the vector

    for (int i = 0; i < DIM_MAX; i++) {
        vA.push_back(static_cast<double>(i));
    }

    // print the vector

    printLA< vector<double> >(vA);

    printLB< vector<double> >(vA);

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

