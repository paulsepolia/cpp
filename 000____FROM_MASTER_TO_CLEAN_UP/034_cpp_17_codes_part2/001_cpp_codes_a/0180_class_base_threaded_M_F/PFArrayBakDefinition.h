//========================//
// PFArrayBakDefinition.h //
//========================//

#ifndef PFARRAYDBAK_DEF
#define PFARRAYDBAK_DEF

#include "PFArrayDeclaration.h"
#include "PFArrayBakDeclaration.h"
#include <iostream>

using std::cout;

namespace pgg {
// default constructor

template <typename T, typename P>
PFArrayDBak<T,P>::PFArrayDBak() : PFArrayD<T,P>(), b(NULL), usedB(T(0))
{
    b = new P [ PFArrayD<T,P>::getCapacity() ];
}

// non default expicit constructor

template <typename T, typename P>
PFArrayDBak<T,P>::PFArrayDBak(T capacityValue) :
    PFArrayD<T,P>(capacityValue), b(NULL), usedB(T(0))
{
    b = new P [capacityValue];
}

// copy constructor

template <typename T, typename P>
PFArrayDBak<T,P>::PFArrayDBak(const PFArrayDBak& oldObject) :
    PFArrayD<T,P>(oldObject), b(NULL), usedB(T(0))
{
    b = new P[ PFArrayD<T,P>::getCapacity() ];
    usedB = oldObject.usedB;
    for (T i = 0; i < usedB; i++) {
        b[i] = oldObject.b[i];
    }
}

// backup public member function

template <typename T, typename P>
void PFArrayDBak<T,P>::backup()
{
    usedB = PFArrayD<T,P>::getNumberUsed();
    for (T i = 0; i < usedB; i++) {
        b[i] = PFArrayD<T,P>::operator[](i);
    }
}

// restore public member function

template <typename T, typename P>
void PFArrayDBak<T,P>::restore()
{
    PFArrayD<T,P>::emptyArray();
    for (T i = 0; i < usedB; i++) {
        PFArrayD<T,P>::addElement(b[i]);
    }
}

// = operator

template <typename T, typename P>
PFArrayDBak<T,P>& PFArrayDBak<T,P>::operator =(const PFArrayDBak& rightSide)
{
    T oldCapacity = PFArrayD<T,P>::getCapacity();

    PFArrayD<T,P>::operator = (rightSide); // you use a call to the base class assignment operator
    // in order to assign to the base class member variables

    if (oldCapacity != rightSide.getCapacity() ) {
        delete [] b;
        b = new P [rightSide.getCapacity()];
    }

    usedB = rightSide.usedB;

    for (T i = 0; i < usedB; i++) {
        b[i] = rightSide.b[i];
    }

    return *this;
}

template <typename T, typename P>
PFArrayDBak<T,P>::~PFArrayDBak()
{
    delete [] b;     // the destructor for the base class ~PFArrayD is called automatically
}
// and it performs delete [] a;

} // pgg

#endif // PFARRAYDBAK_DEF

//======//
// FINI //
//======//
