//==================//
// VectorPaul class //
//==================//

#include "IncludeLibs.h"

// a class

template <typename T>
class VectorPaul {
private:

    long dim;
    T * p1;

public:

    void setDim(long);
    void allocateVector();
    void setVector(long, T);
    T getVector(long);
    void sortVector();
    void randomShuffleVector();
    T maxElementVector();
    T minElementVector();
    void deleteVector();
};

//======//
// FINI //
//======//