//======================//
// VectorPaul Functions //
//======================//

#include "IncludeLibs.h"

// member function

template <typename T>
void VectorPaul<T>::setDim(long i)
{
    dim = i;
}

// member function

template <typename T>
void VectorPaul<T>::allocateVector()
{
    p1 = new T [dim];
}

// member function

template <typename T>
void VectorPaul<T>::setVector(long i, T val)
{
    p1[i] = val;
}

// member function

template <typename T>
T VectorPaul<T>::getVector(long i)
{
    return p1[i];
}

// member function

template <typename T>
void VectorPaul<T>::sortVector()
{
    sort(p1, p1+dim);
}

// member function

template <typename T>
void VectorPaul<T>::randomShuffleVector()
{
    random_shuffle(p1, p1+dim);
}

// member function

template <typename T>
T VectorPaul<T>::maxElementVector()
{
    return *max_element(p1, p1+dim);
}

// member function

template <typename T>
T VectorPaul<T>::minElementVector()
{
    return *min_element(p1, p1+dim);
}

// member function

template <typename T>
void VectorPaul<T>::deleteVector()
{
    delete [] p1;
}


//======//
// FINI //
//======//