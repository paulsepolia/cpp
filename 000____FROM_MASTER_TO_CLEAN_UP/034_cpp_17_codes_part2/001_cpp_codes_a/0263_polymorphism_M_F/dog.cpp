
//======================//
// Dog class definition //
//======================//

#include <iostream>
#include "dog.h"

using std::cout;
using std::endl;

namespace pgg {
// default constructor

Dog::Dog() : Pet(), breed("No breed yet") {}

// member function

void Dog::print() const
{
    cout << "name: " << name << endl;
    cout << "breed: " << breed << endl;
}

// destructor

Dog::~Dog() {}

} // pgg

//======//
// FINI //
//======//

