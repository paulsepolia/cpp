
#include <iostream>
#include "pet.h"
#include "dog.h"

using std::cout;
using std::cin;
using std::endl;
using pgg::Pet;
using pgg::Dog;

int main()
{
    Dog vdog;
    Pet vpet;

    vdog.name = "Tiny";
    vdog.breed = "Great Dane";

    vpet = vdog; // here is the slicing problem

    cout << "The slicing problem:" << endl;

    //vpet.breed; // is illegal since class Pet has no member named breed

    vpet.print();

    cout << "Note that it was print from Pet that was invoked." << endl;

    // defeat the slicing problem

    cout << "The slicing problem defeated:" << endl;

    Pet * ppet;
    ppet = new Pet;
    Dog * pdog;
    pdog = new Dog;

    pdog->name = "Tine";
    pdog->breed = "Great Dane";

    ppet = pdog;

    ppet -> print();
    pdog -> print();

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//