//==========================//
// STL Containers Benchmark //
// Vector, Deque, List      //
//==========================//

#include <iostream>
#include <vector>
#include <deque>
#include <list>
#include <cmath>
#include <ctime>
#include <iomanip>

//using namespace std;

int main()
{
    // 1. variable declarations

    const long int I_MAX = static_cast<long int>(pow(10.0, 7.5));
    const long int J_MAX = 10000000;
    std::vector<double> vecA;
    std::vector<double> vecB;
    std::deque<double> dequeA;
    std::deque<double> dequeB;
    std::list<double> listA;
    std::list<double> listB;
    long int i;
    long int j;
    clock_t t1;
    clock_t t2;

    std::cout << std::fixed;
    std::cout << std::setprecision(5);
    std::cout << std::showpoint;

    // 2. main benchmark code

    for (j = 1; j <= J_MAX; j++) {
        std::cout << "------------------------------------------------------------------>> " << j << std::endl;
        std::cout << std::endl;

        // 3. build the sequence containers

        std::cout << "  1 --> build the sequence containers" << std::endl;
        std::cout << std::endl;

        #pragma omp parallel sections \
        shared(vecA, vecB, dequeA, dequeB, listA, listB)\
        private(i, t1, t2)\
        shared(std::cout)
        {
            #pragma omp section
            {
                t1 = clock();

                for (i = 0; i < I_MAX; i++)
                {
                    vecA.push_back(cos(static_cast<double>(i+0)));
                }

                t2 = clock();

                std::cout << "--> done with --> vecA   --> "
                << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                t1 = clock();

                for (i = 0; i < I_MAX; i++)
                {
                    dequeA.push_back(cos(static_cast<double>(i+1)));
                }

                t2 = clock();

                std::cout << "--> done with --> dequeA --> "
                << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                t1 = clock();

                for (i = 0; i < I_MAX; i++)
                {
                    listA.push_back(cos(static_cast<double>(i+2)));
                }

                t2 = clock();

                std::cout << "--> done with --> listA  --> "
                << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                t1 = clock();

                for (i = 0; i < I_MAX; i++)
                {
                    vecB.push_back(cos(static_cast<double>(i+3)));
                }

                t2 = clock();

                std::cout << "--> done with --> vecB   --> "
                << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                t1 = clock();

                for (i = 0; i < I_MAX; i++)
                {
                    dequeB.push_back(cos(static_cast<double>(i+4)));
                }

                t2 = clock();

                std::cout << "--> done with --> dequeB --> "
                << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                t1 = clock();

                for (i = 0; i < I_MAX; i++)
                {
                    listB.push_back(cos(static_cast<double>(i+5)));
                }

                t2 = clock();

                std::cout << "--> done with --> listB  --> "
                << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
        }

        // 4. get the size of the each sequence container

        std::cout << std::endl;
        std::cout << "  2 --> the sizes of the containers " << std::endl;
        std::cout << std::endl;
        std::cout << "--> vecA.size()   = " << vecA.size() << std::endl;
        std::cout << "--> dequeA.size() = " << dequeA.size() << std::endl;
        std::cout << "--> listA.size()  = " << listA.size() << std::endl;
        std::cout << "--> vecB.size()   = " << vecB.size() << std::endl;
        std::cout << "--> dequeB.size() = " << dequeB.size() << std::endl;
        std::cout << "--> listB.size()  = " << listB.size() << std::endl;
        std::cout << std::endl;

        // 5. swap the sequence containers

        std::cout << "  3 --> swap the sequence containers" << std::endl;
        std::cout << std::endl;

        #pragma omp parallel sections \
        shared(vecA, vecB, dequeA, dequeB, listA, listB)\
        private(t1,t2)\
        shared(std::cout)
        {
            #pragma omp section
            {
                t1 = clock();
                std::cout << "--> swap vecA with vecB" << std::endl;
                vecA.swap(vecB);
                t2 = clock();
                std::cout << "--> done with --> vecA   --> vecB   --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                t1 = clock();
                std::cout << "--> swap dequeA with dequeB" << std::endl;
                dequeA.swap(dequeB);
                t2 = clock();
                std::cout << "--> done with --> dequeA --> dequeB --> "
                << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                t1 = clock();
                std::cout << "--> swap listA with listB" << std::endl;
                listA.swap(listB);
                t2 = clock();
                std::cout << "--> done with --> listA  --> listB  --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
        }

        std::cout << std::endl;

        // 6. pop-back elements from the sequence containers

        std::cout << "  4 --> pop-back elements from the sequence containers" << std::endl;
        std::cout << std::endl;

        #pragma omp parallel sections \
        shared(vecA, vecB, dequeA, dequeB, listA, listB)\
        private(i,t1,t2)\
        shared(std::cout)
        {
            #pragma omp section
            {
                t1 = clock();
                std::cout << "--> pop-back elements from vecA" << std::endl;

                for (i = 0; i < I_MAX; i++)
                {
                    vecA.pop_back();
                }

                t2 = clock();
                std::cout << "--> done with --> vecA   --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                t1 = clock();
                std::cout << "--> pop-back elements from vecB" << std::endl;

                for (i = 0; i < I_MAX; i++)
                {
                    vecB.pop_back();
                }

                t2 = clock();
                std::cout << "--> done with --> vecB   --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                t1 = clock();
                std::cout << "--> pop-back elements from dequeA" << std::endl;

                for (i = 0; i < I_MAX; i++)
                {
                    dequeA.pop_back();
                }

                t2 = clock();
                std::cout << "--> done with --> dequeA --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                t1 = clock();
                std::cout << "--> pop-back elements from dequeB" << std::endl;

                for (i = 0; i < I_MAX; i++)
                {
                    dequeB.pop_back();
                }

                t2 = clock();
                std::cout << "--> done with --> dequeB --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                t1 = clock();
                std::cout << "--> pop-back elements from listA" << std::endl;

                for (i = 0; i < I_MAX; i++)
                {
                    listA.pop_back();
                }

                t2 = clock();
                std::cout << "--> done with --> listA  --> "
                << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                t1 = clock();
                std::cout << "--> pop-back elements from listB" << std::endl;
                for (i = 0; i < I_MAX; i++)
                {
                    listB.pop_back();
                }
                t2 = clock();
                std::cout << "--> done with --> listB  --> "
                << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
        }

        std::cout << std::endl;

        // 7. get the size of the lists

        std::cout << "  5 --> the size of the sequence containers is" << std::endl;
        std::cout << std::endl;
        std::cout << "--> vecA.size()   = " << vecA.size() << std::endl;
        std::cout << "--> vecB.size()   = " << vecB.size() << std::endl;
        std::cout << "--> dequeA.size() = " << dequeA.size() << std::endl;
        std::cout << "--> dequeB.size() = " << dequeB.size() << std::endl;
        std::cout << "--> listA.size()  = " << listA.size() << std::endl;
        std::cout << "--> listB.size()  = " << listB.size() << std::endl;
        std::cout << std::endl;

        // 8. clear the lists

        std::cout << "  6 --> clear the sequence containers " << std::endl;
        std::cout << std::endl;

        vecA.clear();
        std::cout << "--> done with --> vecA" << std::endl;
        vecB.clear();
        std::cout << "--> done with --> vecB" << std::endl;
        dequeA.clear();
        std::cout << "--> done with --> dequeA" << std::endl;
        dequeB.clear();
        std::cout << "--> done with --> dequeB" << std::endl;
        listA.clear();
        std::cout << "--> done with --> listA" << std::endl;
        listB.clear();
        std::cout << "--> done with --> listB" << std::endl;

        // 9. build the sequence containers again

        std::cout << std::endl;
        std::cout << "  7 --> build the sequence containers again " << std::endl;
        std::cout << std::endl;

        #pragma omp parallel sections \
        shared(vecA, vecB, dequeA, dequeB, listA, listB)\
        private(i, t1, t2)\
        shared(std::cout)
        {
            #pragma omp section
            {
                t1 = clock();

                for (i = 0; i < I_MAX; i++)
                {
                    vecA.push_back(cos(static_cast<double>(i+0)));
                }

                t2 = clock();
                std::cout << "--> done with --> vecA   --> "
                << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                t1 = clock();

                for (i = 0; i < I_MAX; i++)
                {
                    dequeA.push_back(cos(static_cast<double>(i+1)));
                }

                t2 = clock();

                std::cout << "--> done with --> dequeA --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                t1 = clock();

                for (i = 0; i < I_MAX; i++)
                {
                    listA.push_back(cos(static_cast<double>(i+2)));
                }

                t2 = clock();

                std::cout << "--> done with --> listA  --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                t1 = clock();

                for (i = 0; i < I_MAX; i++)
                {
                    vecB.push_back(cos(static_cast<double>(i+3)));
                }

                t2 = clock();

                std::cout << "--> done with --> vecB   --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                t1 = clock();

                for (i = 0; i < I_MAX; i++)
                {
                    dequeB.push_back(cos(static_cast<double>(i+4)));
                }

                t2 = clock();

                std::cout << "--> done with --> dequeB --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                t1 = clock();

                for (i = 0; i < I_MAX; i++)
                {
                    listB.push_back(cos(static_cast<double>(i+5)));
                }

                t2 = clock();

                std::cout << "--> done with --> listB  --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
        }

        std::cout << std::endl;

        // 10. clear the containers in parallel

        std::cout << "  8 --> clear the sequence containers in parallel " << std::endl;

        std::cout << std::endl;

        #pragma omp parallel sections \
        shared(vecA, vecB, dequeA, dequeB, listA, listB)\
        private(t1, t2)\
        shared(std::cout)
        {
            #pragma omp section
            {
                t1 = clock();
                std::cout << "--> clearing the vecA ..." << std::endl;
                vecA.clear();
                t2 = clock();
                std::cout << "--> done with --> vecA   --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                t1 = clock();
                std::cout << "--> clearing the vecB ..." << std::endl;
                vecB.clear();
                t2 = clock();
                std::cout << "--> done with --> vecB   --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                t1 = clock();
                std::cout << "--> clearing the dequeA ..." << std::endl;
                dequeA.clear();
                t2 = clock();
                std::cout << "--> done with --> dequeA --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                t1 = clock();
                std::cout << "--> clearing the dequeB ..." << std::endl;
                dequeB.clear();
                t2 = clock();
                std::cout << "--> done with --> dequeB --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                t1 = clock();
                std::cout << "--> clearing the listA ..." << std::endl;
                listA.clear();
                t2 = clock();
                std::cout << "--> done with --> listA  --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                t1 = clock();
                std::cout << "--> clearing the listB ..." << std::endl;
                listB.clear();
                t2 = clock();
                std::cout << "--> done with --> listB  --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
        }

        std::cout << std::endl;
    }

    return 0;
}

// FINI
