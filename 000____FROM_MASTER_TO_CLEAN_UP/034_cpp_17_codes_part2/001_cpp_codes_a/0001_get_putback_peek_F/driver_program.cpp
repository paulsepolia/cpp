//===============================//
// Functions: get, peek, putback //
//===============================//

//===============//
// code --> 0001 //
//===============//

// keywords : get, peek, putback, char, string

#include <iostream>

using std::endl;
using std::cin;
using std::cout;

// the main function

int main()
{
    char ch;

    cout << " --> 1" << endl;

    cout << " --> enter a string: " << endl;

    cout << " --> 2" << endl;

    cin.get(ch);

    cout << " --> 3" << endl;

    cout << " --> after first cin.get(ch); ch = " << ch << endl;

    cout << " --> 4" << endl;

    cin.get(ch);

    cout << " --> 5" << endl;

    cout << " --> after second cin.get(ch); ch = " << ch << endl;

    cout << " --> 6" << endl;

    cin.putback(ch);

    cout << " --> 7" << endl;

    cin.get(ch);

    cout << " --> 8" << endl;

    cout << " --> after putback and then cin.get(ch); ch = " << ch << endl;

    cout << " --> 9" << endl;

    ch = cin.peek();

    cout << " --> 10" << endl;

    cout << " --> after cin.peek(); ch = " << ch << endl;

    cout << " --> 11" << endl;

    cin.get(ch);

    cout << " --> 12" << endl;

    cout << " --> after cin.get(ch); ch = " << ch << endl;

    cout << " --> 13 --> exit" << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    cout << " --> 14 --> exit" << endl;

    return 0;
}

//======//
// FINI //
//======//

