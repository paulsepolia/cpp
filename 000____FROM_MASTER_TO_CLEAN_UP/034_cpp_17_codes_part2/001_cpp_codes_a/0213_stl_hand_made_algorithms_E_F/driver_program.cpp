
//=============================//
// creating a custom algorithm //
//=============================//

#include <iostream>
#include <vector>
#include <list>
#include <algorithm>

using std::cout;
using std::cin;
using std::endl;
using std::vector;
using std::list;

// an algorithm that doubles the values in specified range

template<class ForIter>
void times2(ForIter start, ForIter end)
{
    while(start != end) {
        *start = (*start) * 2;
        start++;
    }
}

// an algorithm that triples the values in specified range

template<class ForIter>
void times3(ForIter start, ForIter end)
{
    while(start != end) {
        *start = (*start) * 3;
        start++;
    }
}

// the main function

int main()
{
    vector<double> vA;
    list<double> listA;
    list<double>::iterator itList;

    // build the vector and the list

    for (int i = 0; i < 10; i++) {
        vA.push_back(i);
        listA.push_back(i+1);
    }

    // output the new vector

    cout << endl;

    cout << " --> vector original" << endl;

    for (unsigned int i = 0; i < vA.size(); i++) {
        cout << vA[i] << " ";
    }

    // output the new list

    cout << endl;

    cout << " --> list original" << endl;

    for (itList = listA.begin(); itList != listA.end(); itList++) {
        cout << *itList << " ";
    }

    // apply the hand-made algorithm

    times2(vA.begin(), vA.end());

    times2(listA.begin(), listA.end());

    // output the new vector

    cout << endl;

    cout << " --> vector times 2" << endl;

    for (unsigned int i = 0; i < vA.size(); i++) {
        cout << vA[i] << " ";
    }

    // output the new list

    cout << endl;

    cout << " --> list times 2" << endl;

    for (itList = listA.begin(); itList != listA.end(); itList++) {
        cout << *itList << " ";
    }

    // apply the hand-made algorithm

    times3(vA.begin(), vA.end());

    times3(listA.begin(), listA.end());

    // output the new vector

    cout << endl;

    cout << " --> vector times 3" << endl;

    for (unsigned int i = 0; i < vA.size(); i++) {
        cout << vA[i] << " ";
    }

    // output the new list

    cout << endl;

    cout << " --> list times 3" << endl;

    for (itList = listA.begin(); itList != listA.end(); itList++) {
        cout << *itList << " ";
    }

    // sentineling

    cout << endl;
    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

