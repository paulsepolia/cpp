
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include <cmath>
#include <cstdio>
#include <ctime>
#include <iomanip>

using std::endl;
using std::cin;
using std::cout;
using std::ofstream;
using std::istream;
using std::exit;
using std::string;
using std::ios;
using std::cos;
using std::pow;
using std::remove;
using std::clock;
using std::setprecision;
using std::fixed;

// the main function

int main()
{
    ofstream outStream;
    string fileName = "my_file.txt";
    const long I_MAX = static_cast<long>(pow(10.0, 7.0));
    const int K_MAX = 10000;
    int sentinel;
    double tmpA;
    clock_t t1;
    clock_t t2;

    cout << fixed;
    cout << setprecision(10);

    for (int k = 0; k < K_MAX; k++) {
        cout << "--------------------------------------------->> " << k << endl;

        // timing starts

        t1 = clock();

        // open the file and attach it to the stream

        outStream.open(fileName.c_str());

        // test if the opening was with success

        if (outStream.fail()) {
            cout << "Failed! Enter an integet to exit.";
            cin >> sentinel;
            exit(1);
        }

        // set the output format

        outStream.setf(ios::fixed);
        outStream.setf(ios::showpoint);
        outStream.setf(ios::showpos);
        outStream.precision(20);

        // write to the file

        cout << " --> writing data to a file and delete it" << endl;

        for (long i = 0; i < I_MAX; i++) {
            tmpA = cos(static_cast<double>(i));
            outStream.width(10);
            outStream << i << " --> ";
            outStream.width(20);
            outStream << tmpA << endl;
        }

        // flush and close the stream

        outStream.flush();
        outStream.close();

        // remove file

        remove(fileName.c_str());

        // timimg ends

        t2 = clock();

        cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;
    }

    // sentineling

    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

