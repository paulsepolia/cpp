//=====================================//
// Author: Pavlos G. Galiatsatos       //
// Date: 2013/10/12                    //
// Functions: pass by value, reference //
//=====================================//

#include <iostream>

using namespace std;

// 1. functions declaration

void addFirst(int& first, int& second);
void doubleFirst(int one, int two);
void squareFirst(int& ref, int val);

// 2. main function

int main()
{
    int num = 5;

    cout << "Inside main: num = " << num << endl;

    addFirst(num, num);

    cout << "Inside main after addFirst(num, num): num = " << num << endl;

    doubleFirst(num, num);

    cout << "Inside main after doubleFirst(num, num): num = " << num << endl;

    squareFirst(num, num);

    cout << "Inside main after squareFirst(num, num): num = " << num << endl;

    return 0;
}

// 3. functions definition

// a.

void addFirst(int& first, int& second)
{
    cout << "Inside addFirst: first = " << first << ", second = " << second << endl;

    first = first + 2;

    cout << "Inside addFirst: first = " << first << ", second = " << second << endl;

    second = second * 2;

    cout << "Inside addFirst: first = " << first << ", second = " << second << endl;
}

// b.

void doubleFirst(int one, int two)
{
    cout << "Inside doubleFirst: one = " << one << ", two = " << two << endl;

    one = one * 2;

    cout << "Inside doubleFirst: one = " << one << ", two = " << two << endl;

    two = two + 2;

    cout << "Inside doubleFirst: one = " << one << ", two = " << two << endl;
}

// c.

void squareFirst(int& ref, int val)
{
    cout << "Inside squareFirst: ref = " << ref << ", val = " << val << endl;

    ref = ref * ref;

    cout << "Inside squareFirst: ref = " << ref << ", val = " << val << endl;

    val = val + 2;

    cout << "Inside squareFirst: ref = " << ref << ", val = " << val << endl;
}


//======//
// FINI //
//======//
