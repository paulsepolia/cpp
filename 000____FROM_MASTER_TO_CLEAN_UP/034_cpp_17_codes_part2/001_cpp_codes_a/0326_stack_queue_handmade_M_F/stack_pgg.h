
//====================================//
// declaration of the stack_pgg class //
//====================================//

#ifndef STACK_PGG_H
#define STACK_PGG_H

#include <vector>
#include <cstdlib>

using std::vector;
using std::size_t;

template <typename T, typename CONT = vector<T> >
class stack_pgg {
public:

    stack_pgg<T, CONT>(); // default constructor
    ~stack_pgg<T, CONT>(); // destructor

    template <typename T2, typename CONT2>
    stack_pgg<T, CONT>(const stack_pgg<T2, CONT2> &); // copy constructor

    // member functions

    bool empty() const;
    size_t size() const;
    T & top();
    void push(const T &);
    void pop();

    template <typename T2, typename CONT2>
    void swap(stack_pgg<T2, CONT2> &);

    void emplace(const T &);

    // operators overload

    template <typename T2, typename CONT2>
    stack_pgg<T, CONT> & operator = (const stack_pgg<T2, CONT2> &); // assignment operator

private:

    CONT elems;
};

#endif // STACK_PGG_H

//======//
// FINI //
//======//
