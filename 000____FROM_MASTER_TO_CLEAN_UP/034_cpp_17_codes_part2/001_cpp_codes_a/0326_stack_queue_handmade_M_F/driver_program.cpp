
//===========================//
// Hand-made stack and queue //
//===========================//

//===============//
// code --> 0326 //
//===============//

// keywords: list, deque, vector, handmade, hand made, stack, queue

#include <iostream>
#include <cmath>
#include <iomanip>
#include <vector>
#include <deque>
#include <list>
#include "queue_pgg.h"
#include "queue_pgg.cpp"
#include "stack_pgg.h"
#include "stack_pgg.cpp"

using std::cout;
using std::cin;
using std::endl;
using std::pow;
using std::fixed;
using std::showpoint;
using std::showpos;
using std::setprecision;
using std::boolalpha;
using std::vector;
using std::deque;
using std::list;

// the main function

int main()
{
    // local variables and parameters

    const long long I_MAX = static_cast<long long>(pow(10.0, 7.3));
    const long long K_MAX = static_cast<long long>(pow(10.0, 5.0));
    stack_pgg<double, vector<double> > * s1;
    stack_pgg<double, deque<double> > * s2;
    queue_pgg<double, deque<double> > * q1;
    queue_pgg<double, list<double> > * q2;

    // adjust the output format

    cout << fixed;
    cout << showpos;
    cout << showpoint;
    cout << setprecision(15);
    cout << boolalpha;

    // the benchmark loop

    for (long long k = 0; k != K_MAX; k++) {
        // the counter

        cout << "-------------------------------------------------------------->> " << k << endl;

        // s1 --> the hand made stack container

        s1 = new stack_pgg<double, vector<double> >;

        // s1 --> build the stack

        cout << " --> build the hand-made stack, *s1" << endl;

        for (long i = 0; i != I_MAX; i++) {
            s1->push(static_cast<double>(i));
        }

        // s1 = s2 --> use the = operator

        cout << " --> *s2 = *s1 --> use the operator = ()" << endl;

        s2 = new stack_pgg<double, deque<double> >;

        *s2 = *s1;

        // s1 --> some values

        cout << " --> s1->empty() = " << s1->empty() << endl;
        cout << " --> s1->size()  = " << s1->size() << endl;
        cout << " --> s1->top()   = " << s1->top() << endl;
        s1->pop();
        cout << " --> s1->top()   = " << s1->top() << endl;

        // s1 --> pop the stack

        cout << " --> *s1 --> pop element-by-element the hand-made stack" << endl;

        while(!s1->empty()) {
            s1->pop();
        }

        // s1 --> some values

        cout << " --> s1->empty() = " << s1->empty() << endl;
        cout << " --> s1->size()  = " << s1->size() << endl;

        // s1 --> delete the stack

        delete s1;

        // s2 --> some values

        cout << " --> s2->empty() = " << s2->empty() << endl;
        cout << " --> s2->size()  = " << s2->size() << endl;
        cout << " --> s2->top()   = " << s2->top() << endl;
        s2->pop();
        cout << " --> s2->top()   = " << s2->top() << endl;

        // s2 --> pop the stack

        cout << " --> *s2 --> pop element-by-element the hand-made stack" << endl;

        while(!s2->empty()) {
            s2->pop();
        }

        // s2 --> some values

        cout << " --> s2->empty() = " << s2->empty() << endl;
        cout << " --> s2->size()  = " << s2->size() << endl;

        // s2 --> delete the container

        cout << " --> *s2 --> delete the hand-made stack" << endl;

        delete s2;

        // s1 --> build the container again

        cout << " --> build the two containers again, *s1 and *s2" << endl;

        s1 = new stack_pgg<double, vector<double> >;

        for (long long i = 0; i != I_MAX; i++) {
            s1->push(static_cast<double>(i));
        }

        // s2 --> build the container again

        s2 = new stack_pgg<double, deque<double> >;

        for (long long i = 0; i != I_MAX; i++) {
            s2->push(static_cast<double>(-i));
        }

        // get some elements

        cout << " --> s1->top() = " << s1->top() << endl;
        cout << " --> s2->top() = " << s2->top() << endl;

        // swap the two containers

        cout << " --> swap the two containers, *s1 and *s2" << endl;

        s2->swap(*s1);

        // get some elements

        cout << " --> s1->top() = " << s1->top() << endl;
        cout << " --> s2->top() = " << s2->top() << endl;

        // use the copy constructor

        cout << " --> use of the copy constructor to initialize s3(*s1) and s4(*s2)" << endl;

        stack_pgg<double, deque<double> > s3(*s1);
        stack_pgg<double, vector<double> > s4(*s2);

        // delete the containers

        cout << " --> delete the containers *s1, *s2" << endl;

        delete s1;
        delete s2;

        // get some elements

        cout << " --> s3.top() = " << s3.top() << endl;
        cout << " --> s4.top() = " << s4.top() << endl;

        // pop s3 and s4

        cout << " --> pop s3" << endl;

        while(!s3.empty()) {
            s3.pop();
        }

        cout << " --> pop s4" << endl;

        while(!s4.empty()) {
            s4.pop();
        }

        // use the emplace member function to build s3 and s4

        cout << " --> emplace the s3" << endl;

        for (long long i = 0; i != I_MAX; i++) {
            s3.push(static_cast<double>(i));
        }

        cout << " --> emplace the s4" << endl;

        for (long long i = 0; i != I_MAX; i++) {
            s4.push(static_cast<double>(i+1));
        }

        // get some elements

        cout << " --> s3.top() = " << s3.top() << endl;
        cout << " --> s4.top() = " << s4.top() << endl;

        // pop s3 and s4

        cout << " --> pop s3" << endl;

        while(!s3.empty()) {
            s3.pop();
        }

        cout << " --> pop s4" << endl;

        while(!s4.empty()) {
            s4.pop();
        }

        // queue game starts here


        // q1 --> the hand made queue container

        q1 = new queue_pgg<double, deque<double> >;

        // q1 --> build the queue

        cout << " --> build the hand-made queue, *q1" << endl;

        for (long i = 0; i != I_MAX; i++) {
            q1->push(static_cast<double>(i));
        }

        // q1 = q2 --> use the = operator

        cout << " --> *q2 = *q1 --> use the operator = ()" << endl;

        q2 = new queue_pgg<double, list<double> >;

        *q2 = *q1;

        // q1 --> some values

        cout << " --> q1->empty() = " << q1->empty() << endl;
        cout << " --> q1->size()  = " << q1->size() << endl;
        cout << " --> q1->front() = " << q1->front() << endl;
        cout << " --> q1->back()  = " << q1->back() << endl;
        q1->pop();
        cout << " --> q1->front() = " << q1->front() << endl;
        cout << " --> q1->back()  = " << q1->back() << endl;

        // q1 --> pop the queue

        cout << " --> *q1 --> pop element-by-element the hand-made queue" << endl;

        while(!q1->empty()) {
            q1->pop();
        }

        // q1 --> some values

        cout << " --> q1->empty() = " << q1->empty() << endl;
        cout << " --> q1->size()  = " << q1->size() << endl;

        // q1 --> delete the queue

        delete q1;

        // q2 --> some values

        cout << " --> q2->empty() = " << q2->empty() << endl;
        cout << " --> q2->size()  = " << q2->size() << endl;
        cout << " --> q2->front() = " << q2->front() << endl;
        cout << " --> q2->back()  = " << q2->back() << endl;
        q2->pop();
        cout << " --> q2->front() = " << q2->front() << endl;
        cout << " --> q2->back()  = " << q2->back() << endl;

        // q2 --> pop the queue

        cout << " --> *q2 --> pop element-by-element the hand-made queue" << endl;

        while(!q2->empty()) {
            q2->pop();
        }

        // q2 --> some values

        cout << " --> q2->empty() = " << q2->empty() << endl;
        cout << " --> q2->size()  = " << q2->size() << endl;

        // q2 --> delete the container

        cout << " --> *q2 --> delete the hand-made queue" << endl;

        delete q2;

        // q1 --> build the container again

        cout << " --> build the two containers again, *q1 and *q2" << endl;

        q1 = new queue_pgg<double, deque<double> >;

        for (long long i = 0; i != I_MAX; i++) {
            q1->push(static_cast<double>(i));
        }

        // q2 --> build the container again

        q2 = new queue_pgg<double, list<double> >;

        for (long long i = 0; i != I_MAX; i++) {
            q2->push(static_cast<double>(-i));
        }

        // get some elements

        cout << " --> q1->front() = " << q1->front() << endl;
        cout << " --> q2->front() = " << q2->front() << endl;
        cout << " --> q1->back()  = " << q1->back()  << endl;
        cout << " --> q2->back()  = " << q2->back()  << endl;

        // swap the two containers

        cout << " --> swap the two containers, *q1 and *q2" << endl;

        q2->swap(*q1);

        // get some elements

        cout << " --> q1->front() = " << q1->front() << endl;
        cout << " --> q2->front() = " << q2->front() << endl;
        cout << " --> q1->back()  = " << q1->back()  << endl;
        cout << " --> q2->back()  = " << q2->back()  << endl;

        // use the copy constructor

        cout << " --> use of the copy constructor to initialize q3(*q1) and q4(*q2)" << endl;

        queue_pgg<double, list<double> > q3(*q1);
        queue_pgg<double, deque<double> > q4(*q2);

        // delete the containers

        cout << " --> delete the containers *q1, *q2" << endl;

        delete q1;
        delete q2;

        // get some elements

        cout << " --> q3->front() = " << q3.front() << endl;
        cout << " --> q4->front() = " << q4.front() << endl;
        cout << " --> q3->back()  = " << q3.back()  << endl;
        cout << " --> q4->back()  = " << q4.back()  << endl;

        // pop q3 and q4

        cout << " --> pop q3" << endl;

        while(!q3.empty()) {
            q3.pop();
        }

        cout << " --> pop q4" << endl;

        while(!q4.empty()) {
            q4.pop();
        }

        // use the emplace member function to build q3 and q4

        cout << " --> emplace the q3" << endl;

        for (long long i = 0; i != I_MAX; i++) {
            q3.push(static_cast<double>(i));
        }

        cout << " --> emplace the q4" << endl;

        for (long long i = 0; i != I_MAX; i++) {
            q4.push(static_cast<double>(i+1));
        }

        // get some elements

        cout << " --> q3->front() = " << q3.front() << endl;
        cout << " --> q4->front() = " << q4.front() << endl;
        cout << " --> q3->back()  = " << q3.back()  << endl;
        cout << " --> q4->back()  = " << q4.back()  << endl;

        // pop q3 and q4

        cout << " --> pop q3" << endl;

        while(!q3.empty()) {
            q3.pop();
        }

        cout << " --> pop q4" << endl;

        while(!q4.empty()) {
            q4.pop();
        }
    }

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
