
//=======================//
// istream class example //
//=======================//

#include <iostream>
#include <fstream>

using std::endl;
using std::cout;
using std::cin;
using std::istream;
using std::filebuf;
using std::ios;

// the main function

int main()
{
    // local variables and parameters

    filebuf fbA;

    // open the txt file to read it

    fbA.open("test.txt", ios::in);

    // initialize the buffer stream

    istream is(&fbA);

    // read and cout

    while (is) {
        cout << char(is.get());
    }

    // close the filebuf stream

    fbA.close();

    return 0;
}

//======//
// FINI //
//======//

