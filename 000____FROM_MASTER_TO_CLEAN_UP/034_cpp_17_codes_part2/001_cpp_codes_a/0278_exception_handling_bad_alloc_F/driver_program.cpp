
//====================//
// exception handling //
//====================//

#include <iostream>
#include <new>

using std::cin;
using std::cout;
using std::endl;
using std::bad_alloc;


// the main function

int main()
{
    try {
        while(true) {
            new int[100000000ul];
        }
    } catch (bad_alloc & e) {
        cout << "Allocation failed: " << e.what() << endl;
    }

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
