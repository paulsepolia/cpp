//=======================//
// STL, vector, deque    //
// assign, insert, erase //
//=======================//

#include <iostream>
#include <vector>
#include <list>
#include <deque>
#include <set>
#include <cmath>
#include <iomanip>

using std::vector;
using std::list;
using std::deque;
using std::set;
using std::cout;
using std::cin;
using std::endl;

// the main function

int main ()
{
    // local variables and parameters

    long int I_MAX = static_cast<long int>(pow(10.0, 7.7));
    long int K_MAX = static_cast<long int>(pow(10.0, 7.0));
    long int i;
    long int k;

    for (k = 0; k < K_MAX; k++) {
        cout << "-------------------------------------------------------->> " << k << endl;

        //=========//
        // VECTORS //
        //=========//

        vector<double>* vecA = new vector<double> [1];
        vector<double>* vecB = new vector<double> [1];

        // build the (*vecA)

        cout << " --> building the (*vecA)..." << endl;

        for (i = 1; i <= I_MAX; i++) {
            (*vecA).push_back(cos(static_cast<double>(i)));
        }

        // assign (*vecA) to (*vecB)

        cout << " --> assigning (*vecA) to the (*vecB)..." << endl;

        (*vecB).assign((*vecA).begin(), (*vecA).end());

        // erase (*vecA)

        cout << " --> erasing (*vecA)..." << endl;

        (*vecA).erase((*vecA).begin(), (*vecA).end());

        // erase (*vecB)

        cout << " --> erasing (*vecB)..." << endl;

        (*vecB).erase((*vecB).begin(), (*vecB).end());

        // clear (*vecA)

        cout << " --> clearing (*vecA)..." << endl;

        (*vecA).clear();

        // clear (*vecB)

        cout << " --> clearing (*vecB)..." << endl;

        (*vecB).clear();

        // delete (*vecA)

        cout << " --> deleting (*vecA)..." << endl;

        delete [] vecA;

        // delete (*vecB)

        cout << " --> deleting (*vecB)..." << endl;

        delete [] vecB;

        //========//
        // DEQUES //
        //========//

        deque<double>* deqA = new deque<double> [1];
        deque<double>* deqB = new deque<double> [1];

        // build the (*deqA)

        cout << " --> building the (*deqA)..." << endl;

        for (i = 1; i <= I_MAX; i++) {
            (*deqA).push_back(cos(static_cast<double>(i)));
        }

        // assign (*deqA) to (*deqB)

        cout << " --> assigning (*deqA) to the (*deqB)..." << endl;

        (*deqB).assign((*deqA).begin(), (*deqA).end());

        // erase (*deqA)

        cout << " --> erasing (*deqA)..." << endl;

        (*deqA).erase((*deqA).begin(), (*deqA).end());

        // erase (*deqB)

        cout << " --> erasing (*deqB)..." << endl;

        (*deqB).erase((*deqB).begin(), (*deqB).end());

        // clear (*deqA)

        cout << " --> clearing (*deqA)..." << endl;

        (*deqA).clear();

        // clear (*deqB)

        cout << " --> clearing (*deqB)..." << endl;

        (*deqB).clear();

        // delete (*deqA)

        cout << " --> deleting (*deqA)..." << endl;

        delete [] deqA;

        // delete (*deqB)

        cout << " --> deleting (*deqB)..." << endl;

        delete [] deqB;

        //=======//
        // LISTS //
        //=======//

        deque<double>* listA = new deque<double> [1];
        deque<double>* listB = new deque<double> [1];

        // build the (*listA)

        cout << " --> building the (*listA)..." << endl;

        for (i = 1; i <= I_MAX; i++) {
            (*listA).push_back(cos(static_cast<double>(i)));
        }

        // assign (*listA) to (*listB)

        cout << " --> assigning (*listA) to the (*listB)..." << endl;

        (*listB).assign((*listA).begin(), (*listA).end());

        // erase (*listA)

        cout << " --> erasing (*listA)..." << endl;

        (*listA).erase((*listA).begin(), (*listA).end());

        // erase (*listB)

        cout << " --> erasing (*listB)..." << endl;

        (*listB).erase((*listB).begin(), (*listB).end());

        // clear (*listA)

        cout << " --> clearing (*listA)..." << endl;

        (*listA).clear();

        // clear (*listB)

        cout << " --> clearing (*listB)..." << endl;

        (*listB).clear();

        // delete (*listA)

        cout << " --> deleting (*listA)..." << endl;

        delete [] listA;

        // delete (*listB)

        cout << " --> deleting (*listB)..." << endl;

        delete [] listB;

        //======//
        // SETS //
        //======//

        vector<double>* vecC = new vector<double> [1];
        set<double>* setA    = new set<double> [1];
        set<double>* setB    = new set<double> [1];

        // build the (*vecC)

        cout << " --> building the (*vecC)..." << endl;

        for (i = 1; i <= I_MAX; i++) {
            (*vecC).push_back(cos(static_cast<double>(i)));
        }

        // build the (*setA)

        cout << " --> building the (*setA)..." << endl;

        (*setA).insert((*vecC).begin(), (*vecC).end());

        // build the (*setB)

        cout << " --> building the (*setB)..." << endl;

        (*setB).insert((*setA).begin(), (*setA).end());

        // delete (*vecC)

        cout << " --> deleting (*vecC)..." << endl;

        delete [] vecC;

        // delete (*setA)

        cout << " --> deleting (*setA)..." << endl;

        delete [] setA;

        // delete (*setB)

        cout << " --> deleting (*setB)..." << endl;

        delete [] setB;
    }

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
