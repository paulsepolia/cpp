
#ifndef CLAPTR_DECL_H
#define CLAPTR_DECL_H

//==========================//
// CLAPTR class declaration //
//==========================//

class CLAPTR {
public:

    // constructors

    CLAPTR();
    explicit CLAPTR(long);
    CLAPTR(long, double);
    CLAPTR(const CLAPTR &); // copy constructor
    // it is a must-have when the class contains pointers

    // member functions

    void setElement(long, double);
    double getElem(long) const;
    void printElem(long) const;
    long getSize() const;
    void printSize() const;
    void setSize(long);
    void reset();

    // = operator overload

    CLAPTR & operator = (const CLAPTR &); // overload of the = operator
    // it is a must-have when the class contains pointers

    // [] operator overload

    double & operator [] (long) const;

    // default destructor

    ~CLAPTR(); // it is a must-have when the class contains pointers

private:

    double * a;
    long dim;
};


///=====//
// FINI //
//======//

#endif // CLAPTR_DECL_H

