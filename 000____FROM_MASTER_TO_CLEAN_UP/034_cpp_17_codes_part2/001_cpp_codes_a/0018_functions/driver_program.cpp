//================================//
// Author: Pavlos G. Galiatsatos  //
// Date: 2013/10/12               //
// Functions: hand made functions //
//================================//

#include <iostream>

using namespace std;

// 1. functions declaration

double larger(double x, double y);
double compareThree(double x, double y, double z);

// 2. main function

int main()
{

    double one;
    double two;

    cout << "The larger of 5 and 10 is " << larger(5, 10) << endl;

    cout << "Enter two numbers: ";
    cin >> one >> two;
    cout << "The larger of " << one << " and " << two << " is " << larger(one, two) << endl;

    cout << "The largest of 43.48, 34.001 and 12.65 is "
         << compareThree(43.48, 34.001, 12.65) << endl;

    return 0;
}

// 3. functions definition

// a.

double larger(double x, double y)
{
    double max;

    if (x >= y) {
        max = x;
    } else {
        max = y;
    }

    return max;
}

// b.

double compareThree(double x, double y, double z)
{
    return larger(x, larger(y,z));
}

//======//
// FINI //
//======//
