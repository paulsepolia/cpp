
#ifndef EXTERNAL_H
#define EXTERNAL_H

//====================//
// benchmark function //
//====================//

#include <iostream>
#include <cmath>
#include "PFArrayDeclaration.h"
#include "PFArrayDefinition.h"
#include "PFArrayBakDeclaration.h"
#include "PFArrayBakDefinition.h"

using std::cin;
using std::cout;
using std::endl;
using pgg::PFArrayD;
using pgg::PFArrayDBak;

template<typename T, typename P>
void benchFun(const T dim)
{
    PFArrayD<T,P> obA(dim);

    for (T j = 0; j < dim; j++) {
        obA.addElement(cos(static_cast<P>(j)));
    }

    // make use of copy constructor

    PFArrayD<T,P> obB(obA);
    PFArrayD<T,P> obC(obB);

    // back up game

    PFArrayDBak<T,P> obBakA(dim);

    for (T j = 0; j < dim; j++) {
        obBakA.addElement(cos(static_cast<P>(j)));
    }

    obBakA.backup();

    obBakA.emptyArray();

    obBakA.restore();
}

#endif // EXTERNAL_H

//======//
// FINI //
//======//
