
//=============================//
// use a unary function object //
//=============================//

#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>

using std::cout;
using std::cin;
using std::endl;
using std::vector;
using std::count_if;
using std::unary_function;

// isEvenA,B,C function object
// determines whether a number is even or odd

//===================================================//
// isEvenA class inherited from unary_function class //
//===================================================//

class isEvenA : public unary_function<int, bool> {
public:
    typename std::unary_function<int, bool>::result_type
    operator () (std::unary_function<int, bool>::argument_type i)
    {
        return static_cast<isEvenA::result_type>(!(i%2));
    }
};

//============================================================//
// isEvenB template class inherited from unary_function class //
//============================================================//

template<class arg>
class isEvenB : public unary_function<arg, bool> {
public:
    typename std::unary_function<arg,bool>::result_type
    operator() (std::unary_function<int, bool>::argument_type i)
    {
        return static_cast<isEvenA::result_type>(!(i%2));
    }
};

//==================================================//
// hand made isEvenC template class function object //
//==================================================//

template<class argA, class argB>
class isEvenC {
public:
    argB operator () (argA i)
    {
        return static_cast<argB>(!(i%2));
    }
};

// the main function

int main()
{
    // local variables and parameters

    vector<int> vA;
    int counter;

    // build the vector

    for (int i = 0; i < 20; i++) {
        vA.push_back(i);
    }

    // display the vector

    for (unsigned int i = 0; i < vA.size(); i++) {
        cout << vA[i] << " ";
    }

    cout << endl;

    // count the even numbers

    counter = count_if(vA.begin(), vA.end(), isEvenA());

    cout << endl;
    cout << " --> using the function object inherited from unary_function ..." << endl;
    cout << endl;
    cout << " --> " << counter << " numbers are evenly divisible by 2" << endl;
    cout << endl;

    // count the even numbers

    counter = count_if(vA.begin(), vA.end(), isEvenB<int>());

    cout << " --> using the template function object inherited from unary_function ..." << endl;
    cout << endl;
    cout << " --> " << counter << " numbers are evenly divisible by 2" << endl;
    cout << endl;

    // count the even numbers

    counter = count_if(vA.begin(), vA.end(), isEvenC<int, bool>());

    cout << " --> using the template function object NOT inherited from anywhere ..." << endl;
    cout << endl;
    cout << " --> " << counter << " numbers are evenly divisible by 2" << endl;
    cout << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

