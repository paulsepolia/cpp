
#include <iostream>
#include <iomanip>
#include "money_definition.h"

using std::endl;
using std::cout;
using std::cin;
using std::fixed;
using std::setprecision;
using std::showpoint;
using std::showpos;
using std::boolalpha;
using std::noboolalpha;

// the main function

int main()
{
    // set the ouput format

    cout << fixed;
    cout << setprecision(2);
    cout << showpoint;
    cout << showpos;
    cout << boolalpha;

    // default constructor test

    Money monA;
    Money monB;
    Money monC;

    cout << " Money monA; --> monA.output();" << endl;

    monA.output();

    cout << " Money monB --> monB.output();" << endl;

    monB.output();

    cout << " Money monC --> monC.output();" << endl;

    monC.output();

    // test input member function

    double valA = 10.0;
    double valB = 20.0;
    double valC = 30.0;

    monA.input(valA);
    monB.input(valB);
    monC.input(valC);

    cout << " --> valA = 10.0 --> monA.input(valA) --> monA.output();" << endl;

    monA.output();

    cout << " --> valB = 20.0 --> monB.input(valB) --> monB.output();" << endl;

    monB.output();

    cout << " --> valC = 30.0 --> monC.input(valC) --> monC.output();" << endl;

    monC.output();

    // test + operator overload

    monC = monA + monB;

    cout << " --> monA.get() = " << monA.get() << endl;
    cout << " --> monB.get() = " << monB.get() << endl;
    cout << " --> monC = monA+monB --> monC.output();" << endl;

    monC.output();

    // test - operator overload

    monC = monA - monB;

    cout << " --> monA.get() = " << monA.get() << endl;
    cout << " --> monB.get() = " << monB.get() << endl;
    cout << " --> monC = monA-monB --> monC.output();" << endl;

    monC.output();

    // test == operator overload

    cout << (monA == monB) << endl;
    cout << (monA == monA) << endl;
    cout << (monB == monB) << endl;
    cout << (monC == monB) << endl;

    // test - unary operator overload

    monA = -monA;

    cout << " --> monA.get() = " << monA.get() << endl;
    cout << " --> monA = -monA --> monA.output();" << endl;

    cout << monA.get() << endl;

    monA = -monB;

    cout << " --> monB.get() = " << monB.get() << endl;
    cout << " --> monA = -monB --> monA.output();" << endl;

    cout << monA.get() << endl;

    // test ++ prefix operator overload

    ++monA;

    cout << " --> ++A" << endl;
    cout << monA.get() << endl;

    ++monA;

    cout << " --> ++A" << endl;
    cout << monA.get() << endl;

    // test ++ suffix operator overload

    monA++;

    cout << " --> A++" << endl;
    cout << monA.get() << endl;

    monA++;

    cout << " --> A++" << endl;
    cout << monA.get() << endl;

    // test -- prefix operator oveload

    --monA;

    cout << " --> --A" << endl;
    cout << monA.get() << endl;

    --monA;

    cout << " --> --A" << endl;
    cout << monA.get() << endl;

    // test -- suffix operator overload

    monA--;

    cout << " --> A--" << endl;
    cout << monA.get() << endl;

    monA--;

    cout << " --> A--" << endl;
    cout << monA.get() << endl;

    // test * operator overload

    cout << " --> monA.get() = " << monA.get() << endl;
    cout << " --> monB.get() = " << monB.get() << endl;
    cout << " --> monC = monA * monB --> monC.output();" << endl;

    monC = monA * monB;

    cout << monC.get() << endl;

    // test / operator overload

    cout << " --> monA.get() = " << monA.get() << endl;
    cout << " --> monB.get() = " << monB.get() << endl;
    cout << " --> monC = monA / monB --> monC.output();" << endl;

    monC = monA / monB;

    cout << monC.get() << endl;

    // test >, <, >=, =<

    cout << (monA > monB) << endl;
    cout << (monA < monB) << endl;
    cout << (monA >= monB) << endl;
    cout << (monA <= monB) << endl;
    cout << (monA <= monA) << endl;
    cout << (monA < monA) << endl;

    // test += operator overload

    monA.input(20.0);
    monB.input(30.0);
    monC.input(40.0);

    cout << " --> monA.get() = " << monA.get() << endl;
    cout << " --> monC.get() = " << monC.get() << endl;
    cout << " --> (monC += monA) --> monC.output();" << endl;

    monC += monA;

    cout << monA.get() << endl;
    cout << monC.get() << endl;

    // test -= operator overload

    monA.input(20.0);
    monB.input(30.0);
    monC.input(40.0);

    cout << " --> monA.get() = " << monA.get() << endl;
    cout << " --> monC.get() = " << monC.get() << endl;
    cout << " --> (monC -= monA) --> monC.output();" << endl;

    monC -= monA;

    cout << monA.get() << endl;
    cout << monC.get() << endl;

    // test *= operator overload

    monA.input(20.0);
    monB.input(30.0);
    monC.input(40.0);

    cout << " --> (monC *= monA) --> monC.output();" << endl;

    monC *= monA;

    cout << monA.get() << endl;
    cout << monC.get() << endl;

    // test /= operator overload

    monA.input(20.0);
    monB.input(30.0);
    monC.input(40.0);

    cout << " --> (monC /= monA) --> monC.output();" << endl;

    monC /= monA;

    cout << monA.get() << endl;
    cout << monC.get() << endl;

    // test automatic conversion type

    monA.input(20.0);
    monB.input(30.0);
    monC.input(40.0);

    cout << " --> (monB = monA + 15) --> monB.output();" << endl;
    cout << " --> (monC = 15 + monA) --> monC.output();" << endl;

    monB = monA + 15;
    monC = 20 + monA;

    cout << monA.get() << endl;
    cout << monB.get() << endl;
    cout << monC.get() << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

