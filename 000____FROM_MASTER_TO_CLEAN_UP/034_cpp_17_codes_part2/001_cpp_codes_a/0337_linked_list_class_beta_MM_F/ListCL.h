//===========================//
// Header file.              //
// ListCL Class Declaration. //
//===========================//

template <typename T, typename P>
class ListCL {
public:
    ListCL<T,P>();   // constructor
    ~ListCL<T,P>();  // destructor

    //=======================//
    // Member functions here. //
    //=======================//

    void AddElement(const T&);
    void AddElement(const T&, P);
    void RemoveElement();
    void ClearList();
    P LengthList();
    T GetListElement(P);
    T SetListElement(T, P);

    //========================//
    // Friend functions here. //
    //========================//

    template <typename T1, typename P1>
    friend void CreateList(ListCL<T1,P1>&, P1);

    template <typename T1, typename P1>
    friend void SetEqual(ListCL<T1,P1>&, ListCL<T1,P1>&);

    template <typename T1, typename P1>
    friend void AddList(ListCL<T1,P1>&, ListCL<T1,P1>&, ListCL<T1,P1>&);

    template <typename T1, typename P1>
    friend void SubtractList(ListCL<T1,P1>&, ListCL<T1,P1>&, ListCL<T1,P1>&);

    template <typename T1, typename P1>
    friend void MultiplyList(ListCL<T1,P1>&, ListCL<T1,P1>&, ListCL<T1,P1>&);

    template <typename T1, typename P1>
    friend void DivideList(ListCL<T1,P1>&, ListCL<T1,P1>&, ListCL<T1,P1>&);

    //============================//
    // Overloaded operators here. //
    //============================//

private:
    ElementCL<T>* m_pHead;
};

//==============//
// End of code. //
//==============//
