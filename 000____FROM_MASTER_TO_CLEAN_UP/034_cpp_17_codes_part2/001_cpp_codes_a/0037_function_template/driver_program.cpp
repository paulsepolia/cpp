//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/10/23              //
// Functions: Templates          //
//===============================//

#include <iostream>
#include <iomanip>

using namespace std;

// function definition

template <class T>
class Pair {
public:
    Pair(T, T); // constructor
    ~Pair(); // destructor
    void showQ();
private:
    T x;
    T y;
};

// main function

int main()
{
    // variables
    Pair<double> a(37.0, 5.0);
    Pair<int> u(37, 5);

    // set output

    cout << fixed;
    cout << setprecision(20);
    cout << showpoint;


    // outputs
    a.showQ();
    u.showQ();

    return 0;
}

// class constructor definition

template <class T>
Pair<T>::Pair(T x1, T y1): x(x1), y(y1) {};

// class function definition

template <class T>
void Pair<T>::showQ()
{
    cout << x/y << endl;
}

template <class T>
Pair<T>::~Pair() {};
// end
