
//====================//
// messy to neat file //
//====================//

#include <iostream>
#include <fstream>
#include <cstdlib>
#include "functions.h"

using std::cout;
using std::cin;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::exit;

// the main function

int main()
{
    ifstream fin;
    ofstream fout;
    int sentinel;

    // open the messy file

    fin.open("rawdata.txt");

    if (fin.fail()) {
        cout << "Input file opening failed. Enter an integer to exit: " << endl;
        cin >> sentinel;
        exit(1);
    }

    // open the neat file

    fout.open("neat.txt");

    if (fout.fail()) {
        cout << "Output file opening failed. Enter an integer to exit: " << endl;
        cin >> sentinel;
        exit(1);
    }

    // process the files

    makeNeat(fin, fout, 10, 20);

    // close the streams

    fin.close();
    fout.flush();
    fout.close();

    // sentineling

    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
