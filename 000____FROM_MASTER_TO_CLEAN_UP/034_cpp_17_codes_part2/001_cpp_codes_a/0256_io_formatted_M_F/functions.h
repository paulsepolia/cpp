
#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <iostream>
#include <fstream>
#include <iomanip>

using std::cout;
using std::cin;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::ios;
using std::fixed;
using std::showpos;
using std::showpoint;
using std::setw;

// makeNeat function declaration

void makeNeat(ifstream &, ofstream &, int, int);

// makeNeat function definition

void makeNeat(ifstream & messyFile, ofstream & neatFile, int numberAfterDecimalpoint, int fieldWidth)
{
    neatFile.setf(ios::fixed);
    neatFile.setf(ios::showpos);
    neatFile.setf(ios::showpoint);
    neatFile.precision(numberAfterDecimalpoint);

    cout.setf(ios::fixed);
    cout.setf(ios::showpos);
    cout.setf(ios::showpoint);
    cout.precision(numberAfterDecimalpoint);

    double next;

    while(messyFile >> next) { // satisfied if there us a next number to read
        cout << setw(fieldWidth) << next << endl;
        neatFile << setw(fieldWidth) << next << endl;
    }
}

//======//
// FINI //
//======//

#endif // FUNCTIONS_H
