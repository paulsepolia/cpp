#include <iostream>
#include <cstdint>
#include <iomanip>
#include <chrono>
#include <memory>
#include <vector>

class Base1 {
public:
    virtual ~Base1() = default;

    virtual void name() const {
        std::cout << " --> base1" << std::endl;
    }
};

class Derived1 : public Base1 {
public:
    void name() const final {
        std::cout << " --> derived1" << std::endl;
    }
};

int main() {

    {
        std::cout << " --> example --> 1 --> start" << std::endl;

        Base1 *b1 = new Derived1;
        const auto *d1 = dynamic_cast<Derived1 *>(b1);

        if (d1) {
            std::cout << "downcast from Base1 to Derived1 successful" << std::endl;
            d1->name();
        }

        delete b1;

        std::cout << " --> example --> 1 --> end" << std::endl;
    }

    {
        // WRONG PRACTICE

        std::cout << " --> example --> 2 --> start" << std::endl;

        Base1 *b1 = new Derived1;
        const auto *d1 = reinterpret_cast<Derived1 *>(b1);

        if(d1) {
            std::cout << "downcast from Base1 to Derived1 successful" << std::endl;
            d1->name();
        }

        delete b1;

        std::cout << " --> example --> 2 --> end" << std::endl;
    }

    {
        // WRONG PRACTICE

        std::cout << " --> example --> 3 --> start" << std::endl;

        auto *b1 = new Base1;
        const auto *d1 = reinterpret_cast<Derived1 *>(b1);

        std::cout << "downcast from Base1 to Derived1 successful" << std::endl;
        d1->name();

        delete b1;

        std::cout << " --> example --> 3 --> end" << std::endl;
    }

    {
        // WRONG PRACTICE

        std::cout << " --> example --> 4 --> start" << std::endl;

        Derived1 *b1 = reinterpret_cast<Derived1*>(new Base1);
        const auto *d1 = reinterpret_cast<Derived1 *>(b1);

        if(d1) {
            std::cout << "downcast from Base1 to Derived1 successful" << std::endl;
            d1->name();
        }

        delete b1;

        std::cout << " --> example --> 4 --> end" << std::endl;
    }
}