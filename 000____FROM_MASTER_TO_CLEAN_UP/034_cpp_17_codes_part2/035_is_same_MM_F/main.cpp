#include <iostream>
#include <type_traits>
#include <cstdint>

void print_separator() {
    std::cout << "-----" << std::endl;
}

int main() {

    std::cout << std::boolalpha;

    // some implementation-defined facts
    std::cout << std::is_same<int, std::int32_t>::value << std::endl;
    // usually true if 'int' is 32 bit
    std::cout << std::is_same<int, std::int64_t>::value << std::endl;
    // possibly true if ILP64 data model is used

    print_separator();

    // 'float' is never an integral type
    std::cout << std::is_same<float, std::int32_t>::value << std::endl; // false

    print_separator();

    // 'int' is implicitly 'signed'
    std::cout << std::is_same<int, int>::value << std::endl;          // true
    std::cout << std::is_same<int, unsigned int>::value << std::endl; // false
    std::cout << std::is_same<int, signed int>::value << std::endl;   // true

    print_separator();

    // unlike other types, 'char' is neither 'unsigned' nor 'signed'
    std::cout << std::is_same<char, char>::value << std::endl;          // true
    std::cout << std::is_same<char, unsigned char>::value << std::endl; // false
    std::cout << std::is_same<char, signed char>::value << std::endl;   // false

}