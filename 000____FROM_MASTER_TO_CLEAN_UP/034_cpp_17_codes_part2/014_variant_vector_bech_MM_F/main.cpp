#include <iostream>
#include <variant>
#include <string>
#include <vector>
#include <numeric>
#include <chrono>
#include <cmath>

const auto DIM1 = static_cast<uint64_t>(std::pow(10.0, 8.0));
const auto DO_MAX1 = static_cast<uint64_t>(std::pow(10.0, 1.0));

int main() {

    {
        double sum_loc = 0.0;

        const auto t1 = std::chrono::steady_clock::now();

        for (uint64_t k = 1; k <= DO_MAX1; ++k) {
            std::cout << "---------------------------------------->> run = " << k << std::endl;
            std::vector<double> v;

            std::cout << "---------------------------------------->> building..." << std::endl;

            for (uint64_t i = 0; i < DIM1; ++i) {
                v.push_back(static_cast<double>(i));
            }

            std::cout << "---------------------------------------->> build done..." << std::endl;
            std::cout << "---------------------------------------->> accumulating..." << std::endl;

            sum_loc += std::accumulate(v.begin(), v.end(), 0.0, [](const double &x, const double &y) {
                return x + y;
            });

            std::cout << "---------------------------------------->> accumulation done..." << std::endl;

            std::cout << "-->> sum_loc = " << sum_loc << std::endl;
        }

        const auto t2 = std::chrono::steady_clock::now();
        const auto td = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count();
        std::cout << "-->> td(vector<double>) = " << td << std::endl;
    }

    {
        double sum_loc = 0.0;

        const auto t1 = std::chrono::steady_clock::now();

        for (uint64_t k = 1; k <= DO_MAX1; ++k) {
            std::cout << "---------------------------------------->> run = " << k << std::endl;
            std::vector<std::variant<double, std::string>> v;

            std::cout << "---------------------------------------->> building..." << std::endl;

            for (uint64_t i = 0; i < DIM1; ++i) {
                v.emplace_back(static_cast<double>(i));
            }

            std::cout << "---------------------------------------->> build done!" << std::endl;
            std::cout << "---------------------------------------->> accumulating..." << std::endl;

            sum_loc += std::accumulate(v.begin(), v.end(), 0.0,
                                       [](const std::variant<double, std::string> &x,
                                          const std::variant<double, std::string> &y) {
                                           return std::get<double>(x) + std::get<double>(y);
                                       });

            std::cout << "---------------------------------------->> accumulation done!" << std::endl;

            std::cout << "-->> sum_loc = " << sum_loc << std::endl;
        }

        const auto t2 = std::chrono::steady_clock::now();
        const auto td = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count();
        std::cout << "-->> td(vector<variant<double,string>>) = " << td << std::endl;
    }

    {
        double sum_loc = 0.0;

        const auto t1 = std::chrono::steady_clock::now();

        for (uint64_t k = 1; k <= DO_MAX1; ++k) {
            std::cout << "---------------------------------------->> run = " << k << std::endl;
            std::vector<void *> v;

            std::cout << "---------------------------------------->> building..." << std::endl;

            for (uint64_t i = 0; i < DIM1; ++i) {
                v.push_back(reinterpret_cast<double *>(new double(i)));
            }

            std::cout << "---------------------------------------->> build done" << std::endl;
            std::cout << "---------------------------------------->> accumulating..." << std::endl;

            for (uint64_t i = 0; i < DIM1; i++) {
                sum_loc += *reinterpret_cast<double *>(v[i]);
            }

            for (uint64_t i = 0; i < DIM1; i++) {
                delete reinterpret_cast<double*>(v[i]);
            }

            std::cout << "---------------------------------------->> accumulation done..." << std::endl;

            std::cout << "-->> sum_loc = " << sum_loc << std::endl;
        }

        const auto t2 = std::chrono::steady_clock::now();
        const auto td = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count();
        std::cout << "-->> td(vector<void*>) = " << td << std::endl;
    }
}