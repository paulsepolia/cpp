#!/bin/bash

  g++-9.1.0 -O3         \
            -Wall       \
            -std=c++17  \
            -pthread    \
            -fopenmp    \
            -pedantic   \
            main.cpp    \
            -o x_gnu

