#include <iostream>
#include <vector>
#include <memory>
#include <functional>
#include <chrono>
#include <iomanip>
#include <cmath>

class benchmark_timer {
public:

    explicit benchmark_timer(const double &res) :
            _res(res),
            t1{std::chrono::steady_clock::now()},
            t2{std::chrono::steady_clock::now()} {};

    ~benchmark_timer() {
        t2 = std::chrono::steady_clock::now();
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count()};
        std::cout << " --> res = " << _res << std::endl;
        std::cout << " --> time used (secs) = " << total_time << std::endl;
    }

    auto set_res(const double &res) -> void {
        _res = res;
    }

private:

    double _res{};
    decltype(std::chrono::steady_clock::now()) t1{};
    decltype(std::chrono::steady_clock::now()) t2{};
};

class A {
public:

    explicit A(std::string name) : _name(std::move(name)) {}

    [[nodiscard]] auto get_name() const -> std::string {
        return _name;
    }

    auto set_name(std::string name) -> void {
        _name = std::move(name);
    }

    virtual ~A() = default;

private:
    std::string _name;
};

int main() {

    std::cout << std::setprecision(5);
    std::cout << std::scientific;
    const auto TRY_MAX{size_t(std::pow(10.0, 1.0))};
    const auto DO_MAX{size_t(std::pow(10.0, 5.0))};
    const auto NAME1{std::string("33333333333333333333333333333333333444444444444")};

    for (size_t ii = 0; ii < TRY_MAX; ii++) {

        std::cout << "-------------------------------------------->> # " << ii << std::endl;

        {
            std::cout << "-->> 1 --> inplace only" << std::endl;
            auto sum_tot{0.0};
            auto ot{benchmark_timer(0.0)};

            auto memory{std::malloc(sizeof(A))};

            for (size_t kk = 0; kk < DO_MAX; kk++) {
                auto p{new(memory) A(NAME1)};
                sum_tot += (double) p->get_name()[0];
                p->~A(); // ONLY WITH new inplace usage
            }
            ot.set_res(sum_tot);
            std::free(memory);
        }

        {
            std::cout << "-->> 2 --> inplace only --> proper way" << std::endl;
            auto sum_tot{0.0};
            auto ot{benchmark_timer(0.0)};

            auto memory{std::malloc(sizeof(A))};

            for (size_t kk = 0; kk < DO_MAX; kk++) {
                auto p{reinterpret_cast<A *>(memory)};
                std::uninitialized_fill_n(p, 1, A(NAME1));
                sum_tot += (double) p->get_name()[0];
                std::destroy_at(p);
            }
            ot.set_res(sum_tot);
            std::free(memory);
        }

        {
            std::cout << "-->> 3 --> malloc + inplace + free" << std::endl;

            auto sum_tot{0.0};
            auto ot{benchmark_timer(0.0)};

            for (size_t kk = 0; kk < DO_MAX; kk++) {

                auto memory{std::malloc(sizeof(A))};
                auto p{new(memory) A(NAME1)};
                sum_tot += (double) p->get_name()[0];
                p->~A();
                std::free(memory);
            }
            ot.set_res(sum_tot);
        }

        {
            std::cout << "-->> 4 --> malloc + inplace + free --> proper way" << std::endl;

            auto sum_tot{0.0};
            auto ot{benchmark_timer(0.0)};

            for (size_t kk = 0; kk < DO_MAX; kk++) {

                auto memory{std::malloc(sizeof(A))};
                auto p{reinterpret_cast<A *>(memory)};
                std::uninitialized_fill_n(p, 1, A(NAME1));
                sum_tot += (double) p->get_name()[0];
                std::destroy_at(p);
                std::free(memory);
            }
            ot.set_res(sum_tot);
        }

        // The following example can either crashes or leaks memory
        // Needs fixing

//        {
//            std::cout << "-->> 5 --> malloc + free" << std::endl;
//
//            auto sum_tot{0.0};
//            auto ot{benchmark_timer(0.0)};
//
//            for (size_t kk = 0; kk < DO_MAX; kk++) {
//
//                auto p{(A *) std::malloc(sizeof(A))};
//                *p = A(NAME1);
//                sum_tot += (double) p->get_name()[0];
//                std::free(p);
//                p->~A();
//            }
//            ot.set_res(sum_tot);
//        }

        {
            std::cout << "-->> 6 --> new + delete" << std::endl;

            auto sum_tot{0.0};
            auto ot{benchmark_timer(0.0)};

            for (size_t kk = 0; kk < DO_MAX; kk++) {

                auto p{new A(NAME1)};
                sum_tot += (double) p->get_name()[0];
                delete p;
            }
            ot.set_res(sum_tot);
        }
    }
}
