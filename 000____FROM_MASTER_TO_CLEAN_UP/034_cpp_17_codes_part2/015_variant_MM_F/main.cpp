#include <iostream>
#include <variant>
#include <cassert>
#include <vector>

class Triangle {
public:
    void render() { std::cout << "Drawing a triangle!" << std::endl; }
};

class Polygon {
public:
    void render() { std::cout << "Drawing a polygon!" << std::endl; }
};

class Sphere {
public:
    void render() { std::cout << "Drawing a sphere!" << std::endl; }
};

int main() {

    std::vector<std::variant<Triangle, Polygon, Sphere>> objects{
            Polygon(),
            Triangle(),
            Sphere(),
            Triangle(),
            Triangle(),
            Triangle(),
            Sphere(),
            Sphere()};

    auto call_render = [](auto &obj) { obj.render(); };

    for (auto &obj : objects) {
        std::visit(call_render, obj);
    }

}