// this is a code to find in a recursive way the
// largest element in an array

#include <iostream>

using namespace std;

// the declaration of the recursive function

int largest( const int list[],
             int lowerIndex,
             int upperIndex );

// the main function
int main()
{

    const int dimen = 100000000;
    int i;
    int k;
    const int kLim = 100;

    int *theArray;

    theArray = new int [dimen];

    for (i = 0; i < dimen; i++) {
        theArray[i] = i;
    }

    for (k = 1; k < kLim; k++ ) {

        cout << " the largest element is intArray: "
             << largest(theArray, 0, dimen-1);
        cout << endl;
    }

    return 0;
}

// the definition of the recursive function

int largest( const int list[],
             int lowerIndex,
             int upperIndex)
{
    int max;

    if (lowerIndex == upperIndex) { // size of the sublist is one
        return list[lowerIndex];
    } else {
        max = largest(list, lowerIndex+1, upperIndex);

        if (list[lowerIndex] >= max) {
            return list[lowerIndex];
        } else {
            return max;
        }
    }
}

// this is the end of the code file
