#include <iostream>

using namespace std;

int main()
{
    // declare variable
    double previous1;
    double previous2;
    long int counter;
    double current;
    double nthFibonacci;

    cout << "Enter the first two fibonacci numbers: ";                 // step 1
    cin >> previous1 >> previous2;                                     // step 2
    cout << endl;
    cout << "The first two fibonacci numbers are "
         << previous1 << " and " << previous2 << endl;                 // step 3
    cout << "Enter the position of the desired Fibonacci number: ";    // step 4
    cin >> nthFibonacci;                                               // step 5

    if ( nthFibonacci == 1 )
        current = previous1;
    else if ( nthFibonacci == 2 )
        current = previous2;
    else {
        counter = 3;

        // step 6
        while(  counter <= nthFibonacci ) {
            current = previous2 + previous1;
            previous1 = previous2;
            previous2 = current;
            counter++;
        } // end while
    } // end else

    cout << "The Fibonacci number at position "
         << nthFibonacci << " is " << current << endl;

    return 0;
}


