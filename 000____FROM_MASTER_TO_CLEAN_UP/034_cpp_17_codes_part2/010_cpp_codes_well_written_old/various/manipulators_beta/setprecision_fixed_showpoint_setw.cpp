// setprecision, fixed, showpoint

#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    double x, y, z;

    x = 15.674;
    y = 235.73;
    z = 9525.9864;

    cout << fixed << showpoint;

    cout << setprecision(2) << endl;

    cout << " x = " << x << endl;
    cout << " y = " << y << endl;
    cout << " z = " << z << endl;

    cout << setprecision(3) << endl;

    cout << " x = " << x << endl;
    cout << " y = " << y << endl;
    cout << " z = " << z << endl;


    const long int dimen = 500;
    long int i ;


    for ( i=1; i<dimen; i++ ) {
        cout << setprecision(i%130) << endl;
        cout << " x = " << x << endl;
        cout << " y = " << y << endl;
        cout << " z = " << z << endl;
    }

    cout << "12345678901234567890" << endl;

    int x1 = 19;
    int a1 = 345;
    double y1 = 76.384;

    cout << setw(5) << x1 << endl;
    cout << setw(5) << a1 << setw(5) << "Hi"
         << setw(5) << x1 << endl << endl;

    cout << setprecision(2);


    return 0;
}