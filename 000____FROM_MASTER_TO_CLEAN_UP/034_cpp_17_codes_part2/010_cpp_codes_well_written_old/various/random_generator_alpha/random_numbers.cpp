// this program demonstrates random numbers

#include <iostream>
#include <cstdlib>
#include <iomanip>

using namespace std;

int main()
{
    unsigned int seed;
    long double *array;
    const long int dimen = 10000000;
    array = new long double [dimen];
    long int i,j;
    const long int jLim=50000;
    long double ave;
    long double ave_total;
    long double ave_total_2;

    seed = 0;
    ave_total = 0;
    ave_total_2 = 0;

    for (j = 0; j < jLim; j++) {
        // call srand to set the seed before any random numbers are generated
        seed = seed + 1;
        srand(seed);

        // create a list with random numbers
        for (i = 0; i < dimen; i++ ) {
            array[i] = static_cast<long double>(rand());
        }

        // the average value of the random numbers
        ave = 0.0;
        for (i = 0; i < dimen; i++ ) {
            ave = ave + array[i];
        }

        ave_total = ave_total + ave/dimen;

        ave_total_2 = ave_total_2 + ave_total/(j+1);

        cout << fixed << showpoint << setprecision(10);
        cout << " --------------------------------->>> " << j << endl;
        cout << endl;
        cout << " the average value   is " << ave/dimen << endl;
        cout << endl;
        cout << " the total averare   is " << ave_total/(j+1) << endl;
        cout << endl;
        cout << " the total average 2 is " << ave_total_2/(j+1) << endl;
        cout << endl;
    }
    return 0;
}
