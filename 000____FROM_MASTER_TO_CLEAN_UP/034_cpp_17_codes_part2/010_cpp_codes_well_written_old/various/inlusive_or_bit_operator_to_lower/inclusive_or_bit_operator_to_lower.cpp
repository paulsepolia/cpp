#include <iostream>
using namespace std;

const int TOLOWER = 0x20;
void lower( char * ); // function prototype

int main()
{
    char word[81];  // storage for a complete line

    cout << " Enter a string of both uppercase and lowercase letters: " << endl;
    cin.getline(word,80,'\n');
    cout << endl;
    cout << " The string of letters just entered is:\n" << word << endl;
    lower(word);
    cout << endl;
    cout << " This string, in lowercase letters, is:" << endl;
    cout << word << endl;

    return 0;
}

void lower( char * word )
{
    while ( *word != '\0' ) {
        *word++ |= TOLOWER;
    }
}
