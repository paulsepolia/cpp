#include <iostream>
#include <cmath>

using namespace std;

// 1. functions prototype

void modregfalsi( long double, long double, long double, long int);
long double f( long double );

// 2. main function

int main()
{
    int imax;       // maximum number of iterations
    double a, b;    // left and right ends of the original interval
    double epsilon; // convergence tolerance

    // obtain the input data

    cout << " Enter the limits of the original search interval, a and b: ";
    cin >> a >> b;
    cout << " Enter the convergence criterion: ";
    cin >> epsilon;
    cout << " Enter th maximum number of iterations allowed: ";
    cin >> imax;

    modregfalsi( a, b, epsilon, imax );

    return 0;
}

// 3. A modified regula falsi function that finds roots of a function.
// The maximum number of iterations permitted is imax. The convergence
// criterion is that the fractional size of the search interval
// (x3-x1) / (b-a) is less than epsilon. A relaxation factor,
// RELAX, is used.

void  modregfalsi( long double a, long double b, long double epsilon, long int imax )
{
    const long double RELAX = 0.9; // the relaxation factor

    long int i;             // current iteration counter
    long double x1, x2, x3; // left, right, and midpoint of current interval
    long double f1, f2, f3; // function evaluated at these points
    long double width;      // width of original interval = (b-a)
    long double curwidth;   // width of current interval = (x3-x1)
    long double fines = 10.0E+5;
    long double ep2 = 10.0E-5;
    long double x4,xtmp;

    // echo back the passed input data

    cout << endl;
    cout << endl;
    cout << " The original search interval is from " << a << " to " << b << endl;
    cout << " The convergence criterion is interval < " << epsilon << endl;
    cout << " The maximum number of iterations allowed is " << imax << endl;

    // calculate the root

    for ( x4=a; x4<b; x4=x4+b/fines ) {
        x1 = x4;
        x3 = x4+b/fines;
        f1 = f(x1);
        f3 = f(x3);
        width = abs(b-a);

        // iterations

        for ( i = 1; i<imax; i++ ) {
            curwidth = (x3-x1) / width;
            x2 = x1 - width * curwidth * f1 / (f3-f1);
            f2 = f(x2);

            if ( abs(curwidth) < epsilon && abs(f2) < ep2 && (abs(x2)-abs(xtmp)> 1.0/fines) ) { // root is found
                cout << endl;
                cout << " A root at x = " << x2 << " was found "
                     << " in " << i << " iterations " << endl;
                cout << " The value of the function is " << f2 << endl;
                xtmp = x2;

//      return;
            } else { // check for left and right crossing
                if (f1*f2 < 0.0 ) { // check for crossing on the left
                    x3 = x2;
                    f3 = f2;
                    f1 = RELAX * f1;
                } else if ( f2*f3 < 0.0 ) { // check for crossing on the right
                    x1 = x2;
                    f1 = f2;
                    f3 = RELAX * f3;
                }
            }
        }

    } // big if
    cout << endl;
    cout << " After " << imax << " iterations, no root was found "
         << " within the convergence criterion " << endl;
    cout << " The search for a root has failed due to excessive iterations\n"
         << " after the maximum number of " << imax << " iterations. " << endl;

    return;
}

// function to evaluate

long double f ( long double x )
{
    const long double PI = 2*asin(1.0); // value of pi

    return ( exp(-x) - sin(0.5*PI*x) );
}
