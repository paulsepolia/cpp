
/* 0.

This file contains C++ codes from book :
Numerical Recipes in C++,
The Art of Scientific Computing.
William H. Press, Saul A. Teukolsky,
William T. Vetterling, Brian P. Flannery.
Cambridge University Press, Feb. 2002.

*/

#include <cmath>

using namespace std ;

/* 1.

Title : The 'sign_NR' function.

Interface : inline double sign_NR( const double& a , const double& b )

Purpose : If ( b >= 0 ) then the result is abs(a).
          if ( b < 0 ) then the result is -abs(a).
		  So the sign of resultant 'a' depends
		  on the sign of 'b'.

*/

inline double sign_NR( const double& a, const double& b )
{
    return b >= 0 ? ( a >= 0 ? a : -a ) : ( a >= 0 ? -a : a ) ;
}

/* 2.

Title :	The 'sqr_NR' Function.

Interface : double sqr_NR( double a )

Purpose : Takes a double value and gives back the square root of it.

*/

inline double sqr_NR( double a )
{
    return a * a ;
}


/* 3.

Title: The 'pythag_NR' Function.

Interface : inline double pythag_NR( double a , double b ).

Purpose : Takes the doubles a and b
          and gives back the sqrt( a*a + b*b ).

*/

double pythag_NR( double a , double b )
{
    double absa, absb ;
    absa = fabs(a) ;
    absb = fabs(b) ;
    if ( absa > absb )
        return absa * sqrt( 1.0 + sqr_NR( absb / absa ) ) ;
    else
        return ( absb == 0.0 ? 0.0 : absb * sqrt( 1.0 + sqr_NR( absa / absb ) ) );
}

/* 4.

Title : The 'eigsrt_NR' function.

Interface : void eigsrt_NR( double d[] , double** v , int n )

Purpose :

*/

void eigsrt_NR( double d[] , double** v , int n )
{
    int k,j,i;
    double p;

    for (i=1; i<n; i++) {
        p=d[k=i];
        for (j=i+1; j<=n; j++)
            if (d[j] >= p) p=d[k=j];
        if (k != i) {
            d[k]=d[i];
            d[i]=p;
            for (j=1; j<=n; j++) {
                p=v[j][i];
                v[j][i]=v[j][k];
                v[j][k]=p;
            }
        }
    }
}

/* 5.

Title :	The 'tqli_NR' function.

Interface : void tqli_NR( double [], double [], int n )

Purpose :  QL algorithm with implicit shifts, to determine the eigenvalues
           and eigenvectors of a real, symmetric, tridiagonal matrix.
           On input, d[1..n] contains the diagonal elements of the tridiagonal
		   matrix. On output, it returns the eigenvalues. The vector e[1..n]
		   inputs the subdiagonal elements of the tridiagonal matrix,
		   with e[1] arbitrary. On output e is destroyed.

*/

void tqli_NR( double d[], double e[], int n )
{
    int m, l, iter, i, k ;
    double s, r, p, g, f, dd, c, b ;

    for ( i = 2 ; i <= n ; i++ ) e[i-1] = e[i] ;
    e[n] = 0.0 ;
    for ( l = 1 ; l <= n ; l++ ) {
        iter = 0 ;
        do {
            for ( m = l ; m <= n-1 ; m++ ) {
                dd = fabs( d[m] ) + fabs( d[m+1] ) ;
                if ( double(fabs(e[m])+dd ) == dd ) break ;
            }
            if ( m != l ) {
                if ( iter++ == 30 )
                    cout << " Too many iterations in tqli_NR " << endl ;
                g = ( d[l+1] - d[l] ) / ( 2.0 * e[l] ) ;
                r = pythag_NR( g , 1.0 ) ;
                g = d[m] - d[l] + e[l] / ( g + sign_NR( r , g ) ) ;
                s = c = 1.0 ;
                p = 0.0 ;
                for ( i = m-1 ; i >= l ; i-- ) {
                    f = s * e[i] ;
                    b = c * e[i] ;
                    e[i+1] = ( r = pythag_NR( f , g ) );
                    if ( r == 0.0 ) {
                        d[i+1] -= p ;
                        e[m] = 0.0 ;
                        break ;
                    }
                    s = f / r ;
                    c = g / r ;
                    g = d[i+1] - p ;
                    r = ( d[i] - g ) * s + 2.0 * c * b ;
                    d[i+1] = g + ( p = s * r ) ;
                    g = c * r - b ;
                }
                if ( r == 0.0 && i >= l ) continue;
                d[l] -= p ;
                e[l] = g ;
                e[m] = 0.0 ;
            }
        } while ( m != l );
    }
}

/* Fini. */

