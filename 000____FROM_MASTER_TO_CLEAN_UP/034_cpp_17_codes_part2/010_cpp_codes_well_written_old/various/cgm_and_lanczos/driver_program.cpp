
//=========================================================
//
//  The Lancos Algorithm.
//
//  Author :: Pavlos G. Galiatsatos.
//
//  Date :: 2008, November.
//
//=========================================================

# include <iostream>
# include <ctime>
# include "F_Input_Screen.h"

using namespace std ;

int main()
{

//=========================================================
// 1. The Driver Program of Lanczos' algorithm.

//=========================================================
// 2. Description Of The Program:

// a. The Wikipedia's Lanczos' algorithm version.
// b. Use of complete re-orthogonalization.
// c. Use of Conjugate Gradient algorithm.
// d. Use of 'double' precision arithmetic.
// e. Storing into ram the Lanczos' vectors.

//=========================================================
// 3. Limitations :

// a. No use of any kind of sparse matrix format.

//=========================================================
// 4. Calling functions.

    Input_Screen_F() ;

//=========================================================
// 5. The sentinel.

    int sentinel ;
    cin >> sentinel ;

    return 0 ;

}


/* Fini. */
