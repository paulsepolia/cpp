// this program demonstrates a recursive function
// that performs a binary search on an integer array.

#include <iostream>

using namespace std;

// function prototype

long int binarySrc(long int *, long int, long int, long int);

// the main function

int main()
{
    long int *tests;
    const long int SIZE = 10000000;
    tests = new long int[SIZE];
    long int i;
    long int num;
    long int result;

    for (i = 0; i < SIZE; i++) {
        tests[i]=i;
    }

    cout << " enter the number to search for: ";
    cin >> num;

    result = binarySrc(tests, 0, SIZE-1, num);

    if (result == -1) {
        cout << " that number does not exist in the array\n";
    } else {
        cout << "That number is found at element " << result;
        cout << " in the array\n";
    }

    return 0;
}

// function definition

long int binarySrc(long int array[],
                   long int first,
                   long int last,
                   long int value)
{
    long int middle; // mid point of search

    if (first > last) { //base case
        return (-1);
    }

    middle = (first+last)/2;

    if (array[middle] == value) {
        return middle;
    }

    if (array[middle] < value) {
        return binarySrc(array, middle+1, last, value);
    } else {
        return binarySrc(array, first, middle-1, value);
    }
}
