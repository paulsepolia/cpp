//===============================//
// Driver program for the        //
// swapValues template function. //
//===============================//

#include <iostream>
using std::cout;
using std::endl;

#include "SwapTemplate.h"

int main()
{
    int integer1 = 1;
    int integer2 = 2;

    cout << "Original integer values are: "
         << integer1 << " " << integer2 << endl;

    swapValues(integer1, integer2);

    cout << "Swapped integer values are: "
         << integer1 << " " << integer2 << endl;

    char symbol1 = 'A';
    char symbol2 = 'B';

    cout << "Original character values are: "
         << symbol1 << " " << symbol2 << endl;

    swapValues(symbol1, symbol2);

    cout << "Swapped character values are: "
         << symbol1 << " " << symbol2 << endl;

    return 0;
}

//==============//
// End of code. //
//==============//
