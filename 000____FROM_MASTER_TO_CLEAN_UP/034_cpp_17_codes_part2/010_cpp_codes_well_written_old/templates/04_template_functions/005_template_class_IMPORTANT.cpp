//========================================//
// A class template with three arguments. //
//========================================//

#include <iostream>

using std::cout;
using std::endl;

// 1. Class sequence.

template <class T, long n = 10, class S = T>
class sequence {
public:
    sequence(T start, T incr);
    S sum();
    void deleteSeq();

private:
    T *r;
};

// 2. Constructor definition.

template <class T, long n, class S>
sequence<T, n, S>::sequence(T start, T incr)
{
    r = new T [n];

    for (long i = 0; i < n; i++) {
        r[i] = start + i * incr;
    }
}

// 3. Member function definition.

template <class T, long n, class S>
S sequence<T, n, S>::sum()
{
    S s = 0;
    for (long i = 0; i < n; i++) {
        s += r[i];
    }

    return s;
}

// 4. Destructor definition.

template <class T, long n, class S>
void sequence<T, n, S>::deleteSeq()
{
    delete [] r;
}

// 5. Main function.

int main()
{
    cout << " 1 --> Some typical examples." << endl;

    cout << " 2 --> Initialization phase." << endl;

    sequence<int, 4, long> a_int(1,2);
    sequence<float, 3> a_float(0.3F, 0.1F);
    sequence<double> a_double(1.0, 0.1);

    cout << " 3 --> Apply Sum phase." << endl;

    cout << a_int.sum() << endl;
    cout << a_float.sum() << endl;
    cout << a_double.sum() << endl;

    cout << " 4 --> Deallocation." << endl;

    a_int.deleteSeq();
    a_float.deleteSeq();
    a_double.deleteSeq();

    cout << " 5 --> Extreme examples." << endl;

    const long NUM_SEQ = 4 * 1E8;

    for (int k = 1; k <= 100; k++) {
        cout << " --------------------------------------------------->> " << k << endl;

        cout << " 6 --> Initialization phase." << endl;

        sequence<double, NUM_SEQ, double> b_double(1,1);
        sequence<double, NUM_SEQ, double> c_double(2,1);

        cout << " 7 --> Sum phase." << endl;

        cout << b_double.sum() << endl;
        cout << c_double.sum() << endl;

        cout << " 8 --> Deallocation." << endl;

        b_double.deleteSeq();
        c_double.deleteSeq();
    }

    return 0;
}

//==============//
// End of code. //
//==============//
