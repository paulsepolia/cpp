//===========================//
// This is the file sort.cpp //
//===========================//

// 1.

template<class T, class P>
void sort(T a[], P numberUsed)
{
    P indexOfNextSmallest;
    for (P index = 0; index < numberUsed - 1; index++) {
        indexOfNextSmallest = indexOfSmallest(a, index, numberUsed);
        swapValues(a[index], a[indexOfNextSmallest]);
    }
}

// 2.

template<class T>
void swapValues(T& variable1, T& variable2)
{
    T temp;

    temp = variable1;
    variable1 = variable2;
    variable2 = temp;
}

// 3.

template<class T, class P>
P indexOfSmallest(const T a[], P startIndex, P numberUsed)
{
    T min = a[startIndex];
    P indexOfMin = startIndex;

    for (P index = startIndex + 1; index < numberUsed; index++) {
        if (a[index] < min) {
            min = a[index];
            indexOfMin = index;
        }
    }

    return indexOfMin;
}

//==============//
// End of code. //
//==============//
