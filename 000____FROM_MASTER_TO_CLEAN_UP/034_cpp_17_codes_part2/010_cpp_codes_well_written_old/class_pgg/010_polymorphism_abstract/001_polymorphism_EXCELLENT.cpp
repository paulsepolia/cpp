// Important conclusions.

// 1. Making virtual a base function
//    you are able to use polymorphism to use
//    a different version of the same-name function
//    difined in the derived class.

// 2. Making virtual a base function
//    you are able to instantiate a derived class object
//    using a pointer to a base class object.
//    You have then full functionality of the virtual function
//    on that type of object.
//    If the function is not virtual then that type of object
//    calls only the base function.

// 3. When you have a virtual function
//    you have to make the base destructor virtual too.
//    Declare virtual the base function is must to have polymorphism.
//    Declare virtual the same-name derived function is optional.


//Polymorphic Bad Guy
//Demonstrates calling member functions dynamically

#include <iostream>

using namespace std;

//==========================//
// Enemy class declaration. //
//==========================//

class Enemy {
public:
    Enemy(int damage = 10);           // constructor
    virtual ~Enemy();                 // destructor
    void virtual Attack() const;      // I demand virtual-ity
    void Attack2() const;             // I do not want virtual-ity

protected:
    int* m_pDamage;
};

//=========================================//
// Enemy class member functions definition //
//=========================================//

// 1.

Enemy::Enemy(int damage)
{
    cout << " An Enemy is created." << endl;
    m_pDamage = new int(damage);
}

// 2.

Enemy::~Enemy()
{
    cout << " In Enemy destructor, deleting m_pDamage." << endl;
    delete m_pDamage;
    m_pDamage = 0;
}

// 3.

void Enemy::Attack() const
{
    cout << " An Enemy attacks and inflicts " << *m_pDamage << " damage points." << endl;
}

// 4.

void Enemy::Attack2() const
{
    cout << " DYO. An Enemy attacks and inflicts " << *m_pDamage << " damage points." << endl;
}
//=========================//
// Boss class declaration. //
//=========================//

class Boss: public Enemy {
public:
    Boss(int multiplier = 3);       // constructor
    virtual ~Boss();                // destructor
    void virtual Attack() const;    // inherited from class Enemy  // 'virtual' is optional
    void Attack2() const;           // inherited from class Enemy

protected:
    int* m_pMultiplier;
};

// 1.

Boss::Boss(int multiplier)
{
    cout << " A Boss is created." << endl;
    m_pMultiplier = new int(multiplier);
}

// 2.

Boss::~Boss()
{
    cout << " In Boss destructor, deleting m_pMultiplier." << endl;
    delete m_pMultiplier;
    m_pMultiplier = 0;
}

// 3.

void Boss::Attack() const
{
    cout << " A Boss attacks and inflicts "
         << (*m_pDamage) * (*m_pMultiplier) << " damage points." << endl;
}

// 4.

void Boss::Attack2() const
{
    cout << " A Boss attacks and inflicts "
         << (*m_pDamage) * (*m_pMultiplier) << " damage points." << endl;
}

//====================//
// The main function. //
//====================//

int main()
{
    cout << " ---->  1. Enemy object is created." << endl;
    Enemy oEnemyA;

    cout << " ---->  2. Boss object is created." << endl;
    Boss oBossA;

    cout << " ---->  3. Attack() Enemy class member function is called." << endl;
    oEnemyA.Attack();

    cout << " ---->  4. Attack2() Enemy class member function is called." << endl;
    oEnemyA.Attack2();

    cout << " ---->  5. Attack() Boss class member function is called." << endl;
    oBossA.Attack();

    cout << " ---->  6. Attack2() Boss class member function is called." << endl;
    oBossA.Attack2();

    cout << " ---->  7. Dynamic Boss object is created." << endl;
    Boss* pBossDyn;
    pBossDyn = new Boss();

    cout << " ---->  8. Calling Attack() virtual on dynamic Boss object." << endl;
    (*pBossDyn).Attack();

    cout << " ---->  9. Calling Attack2() non-virtual on dynamic Boss object." << endl;
    (*pBossDyn).Attack2();

    cout << " ----> 10. Dynamic Boss object is created through pointer to Enemy." << endl;
    Enemy* pBossTEnem;
    pBossTEnem = new Boss();

    cout << " ----> 11. Calling Attack() virtual on Boss object through pointer to Enemy." << endl;
    (*pBossTEnem).Attack();

    cout << " ----> 12. Calling Attack2() non-virtual on Boss object through pointer to Enemy." << endl;
    (*pBossTEnem).Attack2();

    cout << " ----> 13. Deleting pointer to Enemy." << endl;
    delete pBossTEnem;
    pBossTEnem = 0;

    cout << " ----> 14. Deleting pointer to Boss." << endl;
    delete pBossDyn;
    pBossDyn = 0;

    cout << " ---->  X. C++ system automatic call of hand-made destructors." << endl;

    return 0;
}

//==============//
// End of code. //
//==============//
