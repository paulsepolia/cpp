//===========================//
// Header file.              //
// ListCL Class Declaration. //
//===========================//

template <typename T, typename P>
class ListCL {
public:
    ListCL<T,P>();   // constructor
    ~ListCL<T,P>();  // destructor

    //=======================//
    // Member functions here. //
    //=======================//

    void AddElement(const T&);
    void AddElement(const T&, P);
    void RemoveElement();
    void ClearList();
    P LengthList();
    T GetListElement(P);
    T SetListElement(T, P);

    //========================//
    // Friend functions here. //
    //========================//

    template <typename T, typename P>
    friend void CreateList(ListCL<T,P>&, P);

    template <typename T, typename P>
    friend void SetEqual(ListCL<T,P>&, ListCL<T,P>&);

    template <typename T, typename P>
    friend void AddList(ListCL<T,P>&, ListCL<T,P>&, ListCL<T,P>&);

    template <typename T, typename P>
    friend void SubtractList(ListCL<T,P>&, ListCL<T,P>&, ListCL<T,P>&);

    template <typename T, typename P>
    friend void MultiplyList(ListCL<T,P>&, ListCL<T,P>&, ListCL<T,P>&);

    template <typename T, typename P>
    friend void DivideList(ListCL<T,P>&, ListCL<T,P>&, ListCL<T,P>&);

    //============================//
    // Overloaded operators here. //
    //============================//

private:
    ElementCL<T>* m_pHead;
};

//==============//
// End of code. //
//==============//
