//=================================================//
// Header File.                                    //
// DoublePairCL class member functions definition. //
//=================================================//

#include <iostream>
#include <cstdlib>

using namespace std;

double& DoublePairCL::operator[](int index)
{
    if (index == 1)
        return first;
    else if (index == 2)
        return second;
    else {
        cout << "Illegal index value." << endl;
        exit(1);
    }
}
