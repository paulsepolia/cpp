// Overriding Boss
// Demonstrates calling and overriding base member functions

#include <iostream>

using namespace std;

//==========================//
// Enemy class declaration. //
//==========================//

class Enemy {
public:
    Enemy(int damage = 10);      // constructor
    void virtual Taunt() const;  // made virtual to be overriden
    void virtual Attack() const; // made virtual to be overriden

private:
    int m_Damage;
};

//==========================================//
// Enemy class member functions definition. //
//==========================================//

// 1.

Enemy::Enemy(int damage):
    m_Damage(damage)
{}

// 2.

void Enemy::Taunt() const
{
    cout << "The enemy says he will fight you." << endl;
}

// 3.

void Enemy::Attack() const
{
    cout << "Attack! Inflicts " << m_Damage << " damage points." << endl;
}

//=========================//
// Boss class declaration. //
//=========================//

class Boss : public Enemy {
public:
    Boss(int damage = 30);
    void virtual Taunt() const;   // optional use of keyword virtual
    void virtual Attack() const;  // optional use if keyword virtual
};

//========================================//
// Boss class member function definition. //
//========================================//

// 1.

Boss::Boss(int damage):
    Enemy(damage) // call base class constructor with argument
{}

// 2.

void Boss::Taunt() const // override base class member function
{
    cout << "The boss says he will end your pitiful existence." << endl;
}

// 3.

void Boss::Attack() const // override base class memebr function
{
    Enemy::Attack(); // call base class memebr function
    cout << " And laughs at you." << endl;
}

//===================//
// the main function //
//===================//

int main()
{
    cout << "Enemy object:" << endl;
    Enemy anEnemy;
    anEnemy.Taunt();
    anEnemy.Attack();

    cout << "Boss object:" << endl;
    Boss aBoss;
    aBoss.Taunt();
    aBoss.Attack();

    return 0;
}

//==============//
// End of code. //
//==============//
