// Simple Boss 2.0
// Demonstrates inheritance
// and access control under inheritance

#include <iostream>
using namespace std;

//==========================//
// Enemy class declaration. //
//==========================//

class Enemy { // is the base class
public:
    Enemy();
    void Attack() const;

protected:
    int m_Damage; // inaccessible
};

Enemy::Enemy():
    m_Damage(10)
{
    cout << " ----> An Enemy class object has been born." << endl;
}

//=========================================//
// Enemy class member function definition. //
//=========================================//

void Enemy::Attack() const
{
    cout << " ----> Attack inflicts " << m_Damage << " damage points!" << endl;;
}

//=========================//
// Boss class declaration. //
//=========================//

class Boss: public Enemy {
public:
    Boss();
    void SpecialAttack() const;

private:
    int m_DamageMultiplier;
};

//========================================//
// Boss class member function definition. //
//========================================//

Boss::Boss():
    m_DamageMultiplier(3)
{
    cout << " ----> An Boss class object has been born." << endl;
}

void Boss::SpecialAttack() const
{
    cout << " ----> Special Attack inflicts " << (m_DamageMultiplier * m_Damage);
    cout << " damage points!" << endl;
}

//================//
// main function. //
//================//

int main()
{
    cout << " ----> Creating an enemy." << endl;
    Enemy enemy1;
    enemy1.Attack();

//  enemy1.m_Damage = 100;

    cout << " ----> Creating a boss." << endl;
    Boss boss1;

//  boss1.m_Damage = 100;
    boss1.Attack();
    boss1.SpecialAttack();

    return 0;
}
//
