//========================================//
// Driver program to the ElementCL class. //
//========================================//

#include <iostream>
#include <iomanip>

#include "ElementCL.h"
#include "ElementCLMemberFunctions.h"
#include "TypeDefinitions.h"

using namespace std;

int main()
{
    // local constants

    const TB DIM  = 1E6;
    const TB MAXT = 1E3;
    const TA ZERO =  0.00000000000000000000;
    const TA AA   = 11.11111111111111111111;
    const TA BB   = 22.22222222222222222222;

    // local variables

    TB i, j;
    TA atmp, btmp, ctmp, dtmp;

    // local objects

    ElementCL<TA>* elemArrayA;
    elemArrayA = new ElementCL<TA> [DIM];

    ElementCL<TA>* elemArrayB;
    elemArrayB = new ElementCL<TA> [DIM];

    ElementCL<TA> oA(AA);
    ElementCL<TA> oB(BB);

    // setting for the output format

    cout << setprecision(20) << fixed << endl;

    // code rest

    // 1. initializing the array

    for (i = 0; i < DIM; i++) {
        elemArrayA[i] = oA;
        elemArrayB[i] = oB;
    }

    cout << elemArrayA[0].GetElement() << endl;
    cout << elemArrayB[0].GetElement() << endl;

    // 2. test the overloaded operators
    //    some round-off errors occur

    oA.SetElement(ZERO);
    oB.SetElement(ZERO);

    for (j = 0; j < MAXT; j++) {
        for (i = 0; i < DIM; i++) {
            oA += elemArrayA[i];
            oB += elemArrayB[i];
        }
    }

    for (j = 0; j < MAXT; j++) {
        for (i = 0; i < DIM; i++) {
            oA -= elemArrayA[i];
            oB -= elemArrayB[i];
        }
    }

    atmp = oA.GetElement();
    btmp = oB.GetElement();

    ctmp = MAXT * DIM * elemArrayA[0].GetElement();
    dtmp = MAXT * DIM * elemArrayB[0].GetElement();

    cout << atmp << endl;
    cout << btmp << endl;
    cout << ctmp << endl;
    cout << dtmp << endl;

    cout << atmp - ctmp << endl;
    cout << btmp - dtmp << endl;

    //  X. Exiting.

    cout << endl;
    return 0;
}

//==============//
// End of code. //
//==============//
