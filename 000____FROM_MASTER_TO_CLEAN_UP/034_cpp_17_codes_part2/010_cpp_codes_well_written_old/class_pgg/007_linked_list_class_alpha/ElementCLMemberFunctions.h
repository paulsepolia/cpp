//===============================================//
// Header file.                                  //
// ElementCL class member functions definition.  //
//===============================================//

// 1. Constructor definition.

template <typename T>
ElementCL<T>::ElementCL(const T& elem):
    m_Elem(elem)
{}

// 2. Destructor definition.

template <typename T>
ElementCL<T>::~ElementCL() {}

// 3. SetElement.

template <typename T>
void ElementCL<T>::SetElement(const T& elem)
{
    m_Elem = elem;
}

// 4. GetElement.

template <typename T>
T ElementCL<T>::GetElement() const
{
    return m_Elem;
}

// 5. SetElementNext.

template <typename T>
void ElementCL<T>::SetElementNext(ElementCL<T>* nextElem)
{
    m_pElem = nextElem;
}

// 6. GetElementNext.

template <typename T>
ElementCL<T>* ElementCL<T>::GetElementNext() const
{
    return m_pElem;
}

//==============//
// End of code. //
//==============//
