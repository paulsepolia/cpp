//=======================================//
// Driver program to the MatrixCL class. //
//=======================================//

#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cmath>

#include "MatrixCL.h"
#include "MatrixCLMemberFunctions.h"
#include "MatrixCLOverloadedOperators.h"

#include "FunctionsFA.h"

#include "TypeDefinitions.h"

using namespace std;

int main()
{
    // local constants

    const TB  NUMROWS = 1E3;
    const TB  NUMCOLS = 1E3;
    const TB  TRIALS  = 10;
    const TA  DLOW    = 0.0;
    const TA  DHIGH   = 1.0;

    // local variables

    TA atmp;
    TB i, j, k;
    bool bo;

    // some objects

    MatrixCL<TA,TB> matrixA;
    MatrixCL<TA,TB> matrixB;
    MatrixCL<TA,TB> matrixC;

    // seeding the random generator

    srand(static_cast<unsigned>(20));

    // setting the precision of the outputs

    cout << setprecision(20) << fixed << endl;

    // main test code


    // create the matrix

    cout << "  1. ----> Create 3 matrices." << endl;

    matrixA.CreateF(NUMROWS, NUMCOLS);
    matrixB.CreateF(NUMROWS, NUMCOLS);
    matrixC.CreateF(NUMROWS, NUMCOLS);

    // reset the matrices

    cout << "  2. ----> Set the 3 matrices equal to zero." << endl;

    matrixA.SetEqualZeroF();
    matrixB.SetEqualZeroF();
    matrixC.SetEqualZeroF();

    cout << "  3. ----> Some values." << endl;
    cout << "  4. ----> " << matrixA.GetElementF(1,1) << endl;
    cout << "  5. ----> " << matrixB.GetElementF(2,2) << endl;
    cout << "  6. ----> " << matrixC.GetElementF(3,3) << endl;

    // set values to elements

    for (k = 0; k < NUMROWS; k++) {
        for (j = 0; j < NUMCOLS; j++) {
            //atmp = sin(double(k)+double(j));
            //atmp = random_number(DLOW, DHIGH);
            atmp = 5.0;
            matrixA.SetElementF(k, j, atmp);

            //atmp = cos(double(k)+double(j));
            //atmp = random_number(DLOW, DHIGH);
            atmp = 6.0;
            matrixB.SetElementF(k, j, atmp);
        }
    }

    // output some values

    cout << "  7. ----> Some values." << endl;

    atmp = matrixA.GetElementF(1,2);
    cout << "  8. ----> " << atmp << endl;

    atmp = matrixB.GetElementF(3,4);
    cout << "  9. ----> " << atmp << endl;

    // set matrixB equal to matrixA

    matrixB.SetEqualF(matrixA);

    cout << " 10. ----> SetEqualF." << endl;

    atmp = matrixA.GetElementF(5,6);
    cout << " 11. ----> " << atmp << endl;

    atmp = matrixB.GetElementF(7,8);
    cout << " 12. ----> " << atmp << endl;

    // add matrixA to matrixB and get back matrixC

    matrixC.AddF(matrixB, matrixA);

    cout << " 13. ----> AddF." << endl;

    atmp = matrixA.GetElementF(1,1);
    cout << " 14. ----> " << atmp << endl;

    atmp = matrixB.GetElementF(1,1);
    cout << " 15. ----> " << atmp << endl;

    atmp = matrixC.GetElementF(1,1);
    cout << " 16. ----> " << atmp << endl;

    // subtract matrixA from matrixB and get back matrixC

    matrixC.SubtractF(matrixB, matrixA);

    cout << " 17. ----> SubtractF." << endl;

    atmp = matrixA.GetElementF(1,1);
    cout << " 18. ----> " << atmp << endl;

    atmp = matrixB.GetElementF(1,1);
    cout << " 19. ----> " << atmp << endl;

    atmp = matrixC.GetElementF(1,1);
    cout << " 20. ----> " << atmp << endl;

    // times matrixA by matrixB and get back matrixC

    matrixC.TimesF(matrixA, matrixB);

    cout << " 21. ----> TimesF. " << endl;

    atmp = matrixA.GetElementF(4,5);
    cout << " 22. ----> " << atmp << endl;

    atmp = matrixB.GetElementF(4,5);
    cout << " 23. ----> " << atmp << endl;

    atmp = matrixC.GetElementF(4,5);
    cout << " 24. ----> " << atmp << endl;

    // set matrixB and matrixA equal to a number

    cout << " 25. ----> SetEqualNumberF." << endl;

    atmp = 3.0;
    matrixA.SetEqualNumberF(atmp);

    atmp = 4.0;
    matrixB.SetEqualNumberF(atmp);

    atmp = matrixA.GetElementF(10,1);
    cout << " 26. ----> " << atmp << endl;

    atmp = matrixB.GetElementF(11,12);
    cout << " 27. ----> " << atmp << endl;

    // dot matrixA and matrixB

    cout << " 28. ----> DotF." << endl;

    matrixC.DotF(matrixA, matrixB);

    atmp = matrixA.GetElementF(9,10);
    cout << " 29. ----> " << atmp << endl;

    atmp = matrixB.GetElementF(9,10);
    cout << " 30. ----> " << atmp << endl;

    atmp = matrixC.GetElementF(9,10);
    cout << " 31. ----> " << atmp << endl;

    // total the matrices.

    cout << " 32. ----> TotalF." << endl;

    atmp = matrixA.TotalF();
    cout << " 33. ----> " << atmp << endl;

    atmp = matrixB.TotalF();
    cout << " 34. ----> " << atmp << endl;

    atmp = matrixC.TotalF();
    cout << " 35. ----> " << atmp << endl;

    // test the '=' operator.

    cout << " 36. ----> Overloaded '='." << endl;

    matrixB = matrixA;
    matrixC = matrixB;

    cout << " 37. ----> " << matrixA.GetElementF(1,1) << endl;
    cout << " 38. ----> " << matrixB.GetElementF(1,1) << endl;
    cout << " 39. ----> " << matrixC.GetElementF(1,1) << endl;

    // set equal to zero

    matrixA.SetEqualZeroF();

    cout << " 40. ----> SetEqualZeroF." << endl;

    atmp = matrixA.GetElementF(2,3);

    cout << " 41. ----> " << atmp << endl;

    matrixB.SetEqualZeroF();

    atmp = matrixB.GetElementF(3,5);

    cout << " 42. ----> " << atmp << endl;

    // test the '==' operator.

    cout << " 43. ----> Set the 3 matrices equal to zero and compare." << endl;

    matrixA.SetEqualZeroF();
    matrixB.SetEqualZeroF();
    matrixC.SetEqualZeroF();

    bo = (matrixA == matrixB);
    cout << " 44. ----> " << bo << endl;

    bo = (matrixB == matrixC);
    cout << " 45. ----> " << bo << endl;

    bo = (matrixA == matrixC);
    cout << " 46. ----> " << bo << endl;

    // test the '==' operator and 'SetEqualNUmberF'.

    cout << " 47. ----> Set the 3 matrices equal to 10.0 and compare." << endl;

    matrixA.SetEqualNumberF(10.0);
    matrixB.SetEqualNumberF(10.0);
    matrixC = matrixA;

    bo = (matrixA == matrixB);
    cout << " 48. ----> " << bo << endl;

    bo = (matrixB == matrixC);
    cout << " 49. ----> " << bo << endl;

    bo = (matrixA == matrixC);
    cout << " 50. ----> " << bo << endl;

    // give RAM back to system

    matrixA.DeleteF();
    matrixB.DeleteF();
    matrixC.DeleteF();

    return 0;
}

//==============//
// End of code. //
//==============//
