#!/bin/bash

icpc -O3 -xHost -static -static-intel -Bstatic  \
     -parallel -par-threshold0 -par-report1     \
               -vec-threshold0 -vec-report1     \
     DriverProgram_A.cpp -o x_intpair

