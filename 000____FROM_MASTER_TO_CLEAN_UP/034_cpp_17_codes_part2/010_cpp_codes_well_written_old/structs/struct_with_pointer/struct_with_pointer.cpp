#include <iostream>
#include <iomanip>
using namespace std;

struct Test {
    int idNum;
    double * ptPay;
};

int main()
{
    Test emp;
    double pay = 456.20;

    emp.idNum = 12345;
    emp.ptPay = &pay;

    cout << setw(6) << setiosflags(ios::fixed)
         << setiosflags(ios::showpoint)
         << setprecision(2);

    cout << " Employee number " << emp.idNum
         << " was paid $ " << * emp.ptPay << endl;

    return 0;
}


