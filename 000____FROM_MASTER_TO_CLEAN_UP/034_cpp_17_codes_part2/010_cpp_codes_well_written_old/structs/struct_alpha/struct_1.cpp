#include <iostream>
using namespace std;

// 1. struct declaration

struct Date { // this is a global declaration
    int month;
    int day;
    int year;
};

// 2. the main function

int main()
{
    Date birth;

    birth.month = 12;
    birth.day = 28;
    birth.year = 86;

    cout << " My birth day is " << birth.month << '/'
         << birth.day << '/' << birth.year << endl;

    return 0;
}


