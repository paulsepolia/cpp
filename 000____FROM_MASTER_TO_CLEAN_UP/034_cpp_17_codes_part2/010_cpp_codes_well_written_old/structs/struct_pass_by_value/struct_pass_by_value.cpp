#include <iostream>
#include <iomanip>
using namespace std;

// 1. struct declaration

struct Employee { // this is a global declaration
    int idNum;
    double payRate;
    double hours;
};

// 2. function prototype

double calcNet( Employee );

// 3. the main function

int main()
{
    Employee emp = { 6782, 8.93, 40.5 };
    double netPay;

    netPay = calcNet( emp );  // pass copies of the values in emp

    // set output formats

    cout << setw(10)
         << setiosflags(ios::fixed)
         << setiosflags(ios::showpoint)
         << setprecision(3);

    cout << " The net pay for employee " << emp.idNum
         << " is $ " << netPay << endl;

    return 0;
}

// 4. function definition

double calcNet( Employee temp )
{
    return temp.payRate * temp.hours ;
}

