
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

// random generator function:
ptrdiff_t myrandom (ptrdiff_t i)
{
    return rand()%i;
}

// pointer object to it:
ptrdiff_t (*p_myrandom)(ptrdiff_t) = myrandom;

// main function.
int main()
{
// 1. initializing the random generator

    srand ( unsigned ( time (NULL) ) );

// 2. create an initialize an array

    const long int dimen=1;
    long int nums[dimen];
    long int i;

    for ( i=0; i<dimen; i++) {
        nums[i]=i;
    }

// 3. create a vector of strings using the above array

    vector<long int> vecAlpha(nums, nums+dimen);

// 4. output the size of the array.

    cout << endl;

    cout << " 1. The vector initially has a size of: " << endl;

    cout << endl;

    cout << static_cast<long int>( vecAlpha.size() ) << endl;

    cout << endl;

// 5. output the contents of the array.

    cout << " 2. The initial elements are: " << endl;

    cout << endl;

    for ( i=0; i< static_cast<long int>(vecAlpha.size()); i++ ) {
        cout << vecAlpha[i] << "  " ;
    }

    cout << endl;

// 6. add an element to the end of the vector

    const long int dimenB = 2500000;

    for (i=0; i<dimenB; i++ ) {
        vecAlpha.push_back(i-dimenB);
    }

    cout << endl;

    cout << " 3. After adding at the end, the vector has a size of: " << endl;

    cout << endl;

    cout << static_cast<long int>( vecAlpha.size() ) << endl;

    cout << endl ;

// 7. Output the last 10 elements.

    cout << " 4. The last 10 elements are: " << endl;

    cout << endl;

    for (i=dimenB-10; i<dimenB; i++) {
        cout << vecAlpha[i] << endl;
    }

    cout << endl;

// 8. using built-in random generator:

    cout << " 5. Now i am randomly suffling the vector - build in version. " << endl;

    cout << endl;

    random_shuffle ( vecAlpha.begin(), vecAlpha.end() );

// 9. Output the last 10 elements.

    cout << " 6. Now the last 10 elements are: " << endl;

    cout << endl ;

    for (i=dimenB-10; i<dimenB; i++) {
        cout << vecAlpha[i] << endl;
    }

// 10. using built-in random generator:

    cout << endl ;

    cout << " 7. Now I am randomly suffling the vector - my version. " << endl;

    cout << endl;

    random_shuffle ( vecAlpha.begin(), vecAlpha.end(), p_myrandom );

    cout << " 8. Now the last 10 elements are: " << endl;

    cout << endl;

    for (i=dimenB-10; i<dimenB; i++) {
        cout << vecAlpha[i] << endl;
    }

// 11. Using the "find" function

    vector<long int>::iterator it;

    cout << endl;

    cout << " 9. Using the 'find' function. " << endl;

    cout << endl;

    it = find( vecAlpha.begin(), vecAlpha.end(), -30 ) ;

    ++it;

    cout << " 10. The element following -30 is " << *it << endl;
    cout << endl;
    cout << "     The address of it is " << &(*it) << endl;

    cout << endl;

// 12. Now i am doing a binary search

    cout << endl ;

    cout << " 11. Now I am doing binary search for a -1 but it must me SORTED. " << endl;

    cout << endl ;

    sort(vecAlpha.begin(), vecAlpha.end());

    if (binary_search (vecAlpha.begin(), vecAlpha.end(), -1)) {
        cout << "found!\n";
    } else {
        cout << "not found.\n";
    }

// 13. The vector capacity is:

    cout << endl;

    cout << " 12. The vector capacity is: " << endl;

    cout << endl;

    cout << static_cast<int>( vecAlpha.capacity() ) << endl;

    cout << endl;

    cout << " 13. The vector max size is: " << endl;

    cout << endl;

    cout << static_cast<double>( vecAlpha.max_size() ) << endl;

    cout << endl;

// 14. Finding the maximum element.

    cout << " 14. The maximum element is: " << endl ;

    cout << endl;

    cout << *max_element(vecAlpha.begin(), vecAlpha.end()) << endl;

    cout << endl;

// 15. Finding the minimum element.

    cout << " 15. The minimum element is: " << endl ;

    cout << endl;

    cout << *min_element(vecAlpha.begin(), vecAlpha.end()) << endl;

    cout << endl;

// 16. Sorting the list.

    sort(vecAlpha.begin(), vecAlpha.end());

    cout << " 16. After sorting the vector has a size of: " << endl;

    cout << endl;

    cout << static_cast<long int>( vecAlpha.size() ) << endl;

    cout << endl;

// 17. The last 10 sorted elemnts.

    cout << " 17. The last 10 sorted elements are: " << endl;

    cout << endl;

    for ( i=static_cast<long int>(vecAlpha.size()-10); i<static_cast<long int>(vecAlpha.size()); i++ ) {
        cout << vecAlpha[i] << "  " ;
    }

    cout << endl;

    cout << endl;

    return 0;
}

// 18. FINI.
