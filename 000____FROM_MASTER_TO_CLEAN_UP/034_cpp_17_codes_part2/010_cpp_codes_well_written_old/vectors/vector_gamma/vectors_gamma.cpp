
// Fig 8.25: Vector.cpp
// Demonstrating C++ Standard Library class template vector.
#include <iostream>
using std::cout;
using std::cin;
using std::endl;

#include <iomanip>
using std::setw;

#include <vector>
using std::vector;

void outputVector( const vector< int > & ); // display the vector.
void inputVector( vector< int > & ); // input values into the vector.

int main()
{
    vector< int > integers1( 7 ); // 7-element vector < int >.
    vector< int > integers2( 10 ); // 10-element vector < int >.

    // print integers1 size and contents.
    cout << "Size of vector integer1 is " << integers1.size() << endl;
    cout << "Vector after initialization:" << endl;
    outputVector( integers1 );

    // print integers2 size and contents.
    cout << "Size of vector integer2 is " << integers2.size() << endl;
    cout << "Vector after initialization:" << endl;
    outputVector( integers2 );

    // input and print integers1 and integers2
    cout << "\nEnter 17 integers:" << endl;
    inputVector( integers1 );
    inputVector( integers2 );

    cout << "\nAfter input, the vectors contain:\n"
         << "integers1:" << endl;
    outputVector( integers1 );
    cout << "integers2:" << endl;
    outputVector( integers2 );

    // use inequality (!=) operator with vector objects.
    cout << "\nEvaluating: integers1 != integers2" << endl;

    if ( integers1 != integers2 )
        cout << "integers1 and integers2 are not equal" << endl;

    // create vector integers3 using integers1 as an initializer;
    // print size and contents.

    vector < int > integers3( integers1 ); // copy constructor
    cout << "Size of vector integer3 is " << integers3.size() << endl;
    cout << "Vector after initialization:" << endl;
    outputVector( integers3 );

    // use overloaded assignment (=) operator
    cout << "\n assigning integers2 to integers1:" << endl;
    integers1 = integers2; // assign integers2 to integers1

    outputVector(integers1);
    outputVector(integers2);

    if ( integers1 == integers2 )
        cout << "equal" << endl;

    integers1.at(15) = 1000 ;

} // end main.

// output vector contents.
void outputVector( const vector< int > & arrayA )
{
    size_t i; // declare control variable

    for ( i = 0; i < arrayA.size(); i++ ) {
        cout << setw(12) << arrayA[ i ];

        if ( ( i + 1 ) % 4 == 0 ) // 4 numbers per row of output
            cout << endl;
    } // end for.

    if ( i % 4 != 0 )
        cout << endl;
}  // end function outputVector.

// input  vector contents
void inputVector( vector< int > & arrayA )
{
    for ( size_t i = 0; i < arrayA.size(); i++ )
        cin >> arrayA[ i ];
} // end function inputVector.

