#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

struct TeleType {
    string name;
    string phoneNo;
    TeleType * nextaddr;
};

void display( TeleType * ); // function prototype

int main()
{
    TeleType t1 = { "Acme, Sam", "(555) 898-2392" };
    TeleType t2 = { "Dolan, Edith", "(555) 682-3104" };
    TeleType t3 = { "Lanfrank, John", "(555) 718-4581" };
    TeleType * first; // create a pointer to a structure

    first = &t1;        // store t1's   address in first
    t1.nextaddr = &t2;  // store t2's   address in t1.nextaddr
    t2.nextaddr = &t3;  // store t3's   address in t2.nextaddr
    t3.nextaddr = NULL; // store a NULL address in t3.nextaddr

    display( first );

    return 0;
}

void display ( TeleType *contents )
{
    while ( contents != NULL ) {
        cout << endl << setiosflags(ios::left)
             << setw(30) << contents -> name
             << setw(20) << contents -> phoneNo ;
        contents = contents -> nextaddr;
    }

    cout << endl;

    return;
}

