
#include <iostream>
using std::cout;
using std::endl;

#include <cstdlib>
using std::rand;
using std::srand;

#include <ctime>
using std::time;

#include <iomanip>
using std::setw;

#include <ctime>
using std::clock;

int main()
{
    const int arraySize = 5000000; // size of array a.
    int *data = new int [ arraySize ] ; // the dynamically allocated array.
    int insert; // temporary variable to hold element to insert.

    cout << " 1. Creating the array. " << endl;

    for( int i = 0 ; i < arraySize; i++ ) {
        data[ i ] = ( arraySize-i ) ;
    }

    // insertion sort
    // loop over the element of the array.

    clock_t t1;
    t1 = clock ();

    cout << " 2. Sorting the array. " << endl;

    for( int next = 1; next < arraySize; next++ ) {
        insert = data[ next ]; // store the value in the current element

        int moveItem = next;  // initialize location to place the element.

        // search for the location in which to put the current element
        while ( ( moveItem > 0 ) && ( data[ moveItem - 1 ] > insert ) ) {
            // shift element one slot to the right
            data[ moveItem ] = data[ moveItem -1 ];
            moveItem--;
        } // end while.

        data[ moveItem ] = insert; //place inserted element into the array
    } // end for

    clock_t t2;
    t2 = clock () ;

    cout << " 3. The last 10 elements. " << endl;

    // output sorted array.
    for( int i = arraySize-10 ; i < arraySize; i++ ) {
        cout << i << setw( 10 ) << data[ i ] << endl;
    }

    cout << endl;

    cout << " 4. Total real time = " << (t2-t1)/CLOCKS_PER_SEC << " seconds." << endl;
    return 0;
} // end main.