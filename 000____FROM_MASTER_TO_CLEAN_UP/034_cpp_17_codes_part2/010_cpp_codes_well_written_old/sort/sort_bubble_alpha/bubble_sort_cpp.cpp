
#include <iostream>
using namespace std;

// function prototype.
long int bubbleSort( long int * , long int  );

// main function
int main()
{
    // 1. the variables.

    const long int numelem=1000;
    long int i;
    long int * array = new long int[ numelem ];
    long int sentinel;
    long int moves;

    // 2. building the array to be sorted.

    for ( i=0; i<numelem; i++ ) {
        array[i] = numelem-i;
    }

    // 3. sorting the array.

    moves = bubbleSort( array, numelem ) ;

    // 4. some output.

    cout << " some output of the sorted list is " << endl;

    cout << endl;

    for( i=0; i<10; i++ ) {
        cout << array[i] << endl;
    }

    cout << endl;

    cout << " moves that done are: " << moves << endl;

    cin >> sentinel;

    return 0;
}

// 5. function definition.

long int bubbleSort( long int * num, long int  numelem )
{
    long int i,j,temp,moves=0;

    for ( i=0; i<(numelem-1); i++ ) {
        for ( j = 1; j< numelem; j++ ) {
            if( num[j] < num[j-1] ) { // if you have located a lower value
                temp = num[j];    // capture it
                num[j] = num[j-1];
                num[j-1] = temp;
                moves++;
            }
        }
    }
    return moves;
}
