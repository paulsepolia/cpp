// selection sort

#include <iostream>

using namespace std;

void selectionSort( int list[], int length );

int main()
{
    const int dimen = 400000;

    int *list  = new int [dimen];

    int i;

    for ( int i=0; i<dimen; i++ ) {
        list[i] = dimen-i;
    }

    selectionSort( list, dimen );

    cout << "After sorting, the list elements are:" << endl;

    for ( i=0; i < dimen; i++ ) {
        cout << list[i] << endl;
    }

    cout << endl;

    return 0;
}

// selection sort function

void selectionSort( int list[], int length )
{
    int index;
    int smallestIndex;
    int minIndex;
    int temp;

    for( index = 0; index < length-1; index++ ) {
        smallestIndex = index;

        for( minIndex=index+1; minIndex<length; minIndex++ ) {
            if ( list[minIndex] < list[smallestIndex] ) {
                smallestIndex = minIndex;
            }
        }

        temp = list[smallestIndex];
        list[smallestIndex] = list[index];
        list[index] = temp;
    }
}


