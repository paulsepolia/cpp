//==========================================================//
// Program to demonstrate use of the generic find function. //
//==========================================================//

#include <iostream>
#include <vector>
#include <algorithm>

using std::cin;
using std::cout;
using std::endl;
using std::vector;
using std::find;

int main()
{
    vector<char> line;

    cout << " Enter a line of text: " << endl;
    char next;
    cin.get(next);
    while (next != '\n') {
        line.push_back(next);
        cin.get(next);
    }

    vector<char>::const_iterator pos;
    pos = find(line.begin(), line.end(), 'e');
    // position of the first occurrence of 'e' in vector

    vector<char>::const_iterator p;
    cout << "You entered the following before you entered your first e:" << endl;
    for (p = line.begin(); p != pos; p++) {
        cout << *p;
    }
    cout << endl;

    cout << "You entered the following that:" << endl;
    for (p = pos; p != line.end(); p++) {
        cout << *p;
    }
    cout << endl;

    cout << "End of demonstration." << endl;

    return 0;
}

//==============//
// End of code. //
//==============//
