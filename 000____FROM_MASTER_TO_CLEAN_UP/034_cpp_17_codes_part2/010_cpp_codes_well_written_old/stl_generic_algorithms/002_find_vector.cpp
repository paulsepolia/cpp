//==========================================================//
// Program to demonstrate use of the generic find function. //
//==========================================================//

#include <iostream>
#include <vector>
#include <algorithm>

using std::cin;
using std::cout;
using std::endl;
using std::vector;
using std::find;

int main()
{
    // local variables

    long i;
    long elem;
    long k;

    // local constants

    const long DIMEN_VEC = 2*1E8;
    const long NUM_DO = 1E3;

    // containers and iterators

    vector<long>::const_iterator posA;
    vector<long>::const_iterator posB;
    vector<long>::const_iterator p;

    // main code

    for (k = 1; k <= NUM_DO; k++) {
        vector<long> *plineA = new vector<long>;
        vector<long> *plineB = new vector<long>;
        vector<long> *plineC = new vector<long>;

        cout << " -------------------------------------------------------->>> " << k << endl;

        cout << "  1. --> Building the vector of longs." << endl;

        for (i = 0; i < DIMEN_VEC; i++) {
            plineA -> push_back(i);
        }

        cout << "  2. --> Find. Looking for the last in element of the vector." << endl;

        posA = find(plineA -> begin(), plineA -> end(), DIMEN_VEC-1);

        cout << "  3. --> The position of the last element is: " << endl;

        cout << *posA << endl;

        cout << "  4. --> Looking for all the elements in the vector. " << endl;

        for (p = plineA -> begin(); p != plineA -> end(); p++) {
            elem = *p;
            posA = find(plineA -> begin(), plineA -> end(), elem);
            plineB -> push_back(elem);
            posB = find(plineB -> begin(), plineB -> end(), elem);
            plineC -> push_back(elem);
        }

        cout << "  5. --> Testing for equality. Must be True. " << endl;

        cout << ((*plineA) == (*plineB)) << endl;
        cout << ((*plineA) == (*plineC)) << endl;

        cout << "  6. --> Forcing equation." << endl;

        *plineA = *plineB;
        *plineC = *plineC;

        cout << "  7. --> Testing for equality. Must be True. " << endl;

        cout << ((*plineA) == (*plineB)) << endl;
        cout << ((*plineA) == (*plineC)) << endl;

        cout << "  8. --> Deleting the vectors. " << endl;

        delete plineA;
        delete plineB;
        delete plineC;

        cout << "  9. --> Repeat now." << endl;
    }

    return 0;
}

//==============//
// End of code. //
//==============//
