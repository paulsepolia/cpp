//===================================================================//
// Program to demonstrate bidirectional and random-access iterators. //
//===================================================================//

#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;

int main()
{
    vector<char> container;

    container.push_back('A');
    container.push_back('B');
    container.push_back('C');
    container.push_back('D');

    for (int i = 0; i < 4; i++) {
        cout << " container[ " << i << " ] == " << container[i] << endl;
    }

    vector<char>::iterator p;
    p = container.begin();

    cout << " The third entry is " << container[2] << endl; // special to vectors and arrays
    cout << " The third entry is " << p[2] << endl;         // works for any random-access iterator.
    cout << " The third entry is " << *(p+2) << endl;       // works for any random-access iterator.

    cout << " The second entry is " << container[1] << endl; // special to vectors and arrays
    cout << " The second entry is " << p[1] << endl;         // works for any random-access iterator.
    cout << " The second entry is " << *(p+1) << endl;       // works for any random-access iterator.

    cout << " Back to container[0]. " << endl;

    p = container.begin();

    cout << " which has value " << *p << endl;

    cout << " Two steps forward and one step back: " << endl;

    p++;

    cout << *p << endl;

    p++;

    cout << *p << endl;

    p--; // works for any bidirectional iterator

    cout << *p << endl;

    return 0;
}

//==============//
// End of code. //
//==============//
