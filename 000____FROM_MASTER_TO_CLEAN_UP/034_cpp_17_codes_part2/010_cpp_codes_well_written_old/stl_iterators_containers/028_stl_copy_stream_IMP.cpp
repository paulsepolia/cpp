//==============================//
// Using copy algorithm for I/O //
//==============================//

#include <fstream>
#include <iostream>
#include <iterator>
#include <vector>
#include <algorithm>

using std::endl;
using std::cout;
using std::vector;
using std::istream_iterator;
using std::ostream_iterator;
using std::ifstream;

typedef istream_iterator<long> istream_iter;

int main()
{
    vector<long> a;

    ifstream file("064_example.txt");

    if (file.fail()) {
        cout << " Cannot open file '064_example.txt'. " << endl;
        return 1;
    }

    cout << " 1 --> Reading from the disk." << endl;

    copy(istream_iter(file), istream_iter(), inserter(a, a.begin()));

    cout << " 2 --> Sorting the elements of the vector." << endl;

    sort(a.begin(), a.end());

    cout << " 3 --> Writing to the output unit." << endl;

    copy(a.begin(), a.end(), ostream_iterator<long>(cout, " "));

    cout << endl;

    return 0;
}

//==============//
// End of code. //
//==============//
