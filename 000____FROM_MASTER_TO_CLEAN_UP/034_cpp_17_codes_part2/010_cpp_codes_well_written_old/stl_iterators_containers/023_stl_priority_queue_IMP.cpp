//===========================================//
// A priority queue; P.top() is the element  //
// whose last digit is less than or equal to //
// that of the other elements.               //
//===========================================//

#include <iostream>
#include <vector>
#include <queue>

using std::endl;
using std::cout;
using std::priority_queue;
using std::vector;

// 1. Class definition.

class CompareLastDigits {
public:
    bool operator()(long x, long y)
    {
        return (x % 10 > y % 10);
    }
};

// 2. The main function.

int main()
{
    priority_queue<long, vector<long>, CompareLastDigits > PQ;

    long x;

    PQ.push(123);
    PQ.push(51);
    PQ.push(1000);
    PQ.push(17);

    while(!PQ.empty()) {
        x = PQ.top();
        cout << "Retrieved element: " << x << endl;
        PQ.pop();
    }

    return 0;
}

//==============//
// End of code. //
//==============//
