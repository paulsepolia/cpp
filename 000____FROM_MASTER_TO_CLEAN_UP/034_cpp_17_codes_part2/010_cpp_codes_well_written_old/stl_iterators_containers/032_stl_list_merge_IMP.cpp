//===========================//
// The list member functions //
// 'unique', 'sort'.         //
// 'remove' and 'erase'      //
//===========================//

#include <iostream>
#include <list>
#include <iterator>

using std::endl;
using std::cout;
using std::list;
using std::ostream_iterator;

// 1. Function definition.

void out(const char *s, const list<long> &L)
{
    cout << s;
    copy(L.begin(), L.end(), ostream_iterator<long>(cout, " "));
    cout << endl;
}

int main()
{
    // Local variables.

    long i;
    long k;

    // Local constants.

    const long LI_DIM = 7.0 * 1E7;

    // The main code loop.

    for (k = 0; k < 1000; k++) {

        cout << "------------------------------------------------------>>> " << k << endl;

        list<long> *pL1 = new list<long>;
        list<long> *pL2 = new list<long>;

        cout << "  1 --> Building the list *pL1." << endl;

        for (i = 0; i < LI_DIM; i++) {
            pL1 -> push_back(123);
        }

        cout << "  2 --> Building the list *pL2." << endl;

        for (i = 0; i < LI_DIM; i++) {
            pL2 -> push_back(124);
        }

        // Commented out for large outputs.
        //out("Initial contents: ", *pL1);
        //out("Initial contents: ", *pL2);

        cout << "  3 --> Sorting the list *pL1." << endl;

        (*pL1).sort();

        cout << "  4 --> Sorting the list *pL2." << endl;

        (*pL2).sort();

        cout << "  5 --> Merging the lists *pL1 to be in *pL2." << endl;

        (*pL2).merge(*pL1);

        cout << "  6 --> Merging the lists *pL2 to be in *pL1." << endl;

        (*pL1).merge(*pL2);

        cout << "  7 --> Sorting the list *pL2." << endl;

        (*pL2).sort();

        cout << "  8 --> Sorting the list *pL1." << endl;

        (*pL1).sort();

        cout << "  9 --> Eliminating the next-to identical elements --> (*pL1)" << endl;

        (*pL1).unique();

        cout << " 10 --> Eliminating the next-to identical elements --> (*pL2)" << endl;

        (*pL2).unique();

        cout << " 11 --> Some output." << endl;

        out("After (*pL1).unique(): ", *pL1);

        cout << " 12 --> Some output." << endl;

        out("After (*pL2).unique(): ", *pL2);

        cout << " 13 --> The size of the list *pL1 is " << endl;

        cout << pL1 -> size() << endl;

        cout << " 14 --> The size of the list *pL2. " << endl;

        cout << pL2 -> size() << endl;

        cout << " 15 --> Deletion of the list *pL1." << endl;

        delete pL1;

        cout << " 16 --> Deletion of the list *pL2." << endl;

        delete pL2;
    }

    return 0;
}

//==============//
// End of code. //
//==============//