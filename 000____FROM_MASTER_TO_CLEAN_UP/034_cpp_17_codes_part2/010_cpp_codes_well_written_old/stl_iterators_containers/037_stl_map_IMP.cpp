//=====//
// map //
//=====//

#include <iostream>
#include <string>
#include <map>
#include <sstream>

using std::endl;
using std::cout;
using std::string;
using std::map;
using std::stringstream;

// 1. Function definition.

string convertLongInt(long a)
{
    stringstream ss;   // 1. Create a stringstream
    ss << a;           // 2. Add number to the stream

    return ss.str();   // 3. Return a string with the contents of the stream
}

// 2. Type definition.

typedef map<string, long>   masL;
typedef map<string, double> masD;

// 3. The main function.

int main()
{
    // 1. Local constants.

    const long DIM_MA_A = 1E7;
    const long NUM_DO_A = 10;
    const long NUM_DO_B = 1E4;

    // 2. Local variables.

    long i;
    long k;
    string s;
    masL::iterator itMSL;
    masD::iterator itMSD;

    // 3. The main code.

    for (k = 1; k <= NUM_DO_B; k++) {

        cout << "------------------------------------------------>>> " << k << endl;

        masL *pMSL1 = new masL;
        masL *pMSL2 = new masL;

        cout << "  1 --> Building the *pMSL1." << endl;

        for (i = 0; i < DIM_MA_A; i++) {
            s = convertLongInt(DIM_MA_A-i);

            (*pMSL1)[s] = DIM_MA_A-i;
            //(*pMSL1)[s] = DIM_MA_A-i+1;
            //(*pMSL1)[s] = DIM_MA_A-i+2;
            //(*pMSL1)[s] = DIM_MA_A-i+3;
            //(*pMSL1)[s] = DIM_MA_A-i+4;
        }

        cout << "  2 --> Building the *pMSL2." << endl;

        for (i = 0; i < DIM_MA_A; i++) {
            s = convertLongInt(DIM_MA_A-i);

            (*pMSL2)[s] = DIM_MA_A-i;
            //(*pMSL2)[s] = DIM_MA_A-i+1;
            //(*pMSL2)[s] = DIM_MA_A-i+2;
            //(*pMSL2)[s] = DIM_MA_A-i+3;
            //(*pMSL2)[s] = DIM_MA_A-i+4;
        }

        cout << "  3 --> Test for equality *pMSL1 and *pMSL2." << endl;

        for (i = 1; i <= NUM_DO_A; i++) {
            if (*pMSL1 == *pMSL2) {
                cout << " Equal --> " << i << endl;
            } else {
                cout << " Not Equal --> " << i << endl;
            }
        }

        cout << "  4 --> Size of *pMSL1." << endl;

        cout << pMSL1 -> size() << endl;

        cout << "  5 --> Size of *pMSL2." << endl;

        cout << pMSL2 -> size() << endl;

        cout << "  6 --> Find and erase element by element in pMSL1." << endl;

        for (i = 0; i < DIM_MA_A; i++) {
            s = convertLongInt(DIM_MA_A-i);

            itMSL = pMSL1 -> find(s);

            pMSL1 -> erase(itMSL);
        }

        cout << "  7 --> Find and erase element by element in pMSL2." << endl;

        for (i = 0; i < DIM_MA_A; i++) {
            s = convertLongInt(DIM_MA_A-i);

            itMSL = pMSL2 -> find(s);

            pMSL2 -> erase(itMSL);
        }

        cout << "  8 --> Size of *pMSL1." << endl;

        cout << pMSL1 -> size() << endl;

        cout << "  9 --> Size of *pMSL2." << endl;

        cout << pMSL2 -> size() << endl;

        // free up memory here

        cout << " 10 --> Free up RAM." << endl;

        delete pMSL1;
        delete pMSL2;

        // about map and doubles now

        masD *pMSD1 = new masD;
        masD *pMSD2 = new masD;

        cout << " 11 --> Building the *pMSD1." << endl;

        for (i = 0; i < DIM_MA_A; i++) {
            s = convertLongInt(DIM_MA_A-i);

            (*pMSD1)[s] = static_cast<double>(DIM_MA_A-i);
            (*pMSD1)[s] = static_cast<double>(DIM_MA_A-i+1);
            (*pMSD1)[s] = static_cast<double>(DIM_MA_A-i+2);
            (*pMSD1)[s] = static_cast<double>(DIM_MA_A-i+3);
            (*pMSD1)[s] = static_cast<double>(DIM_MA_A-i+4);
        }

        cout << " 12 --> Building the *pMSD2." << endl;

        for (i = 0; i < DIM_MA_A; i++) {
            s = convertLongInt(DIM_MA_A-i);

            (*pMSD2)[s] = static_cast<double>(DIM_MA_A-i);
            (*pMSD2)[s] = static_cast<double>(DIM_MA_A-i+1);
            (*pMSD2)[s] = static_cast<double>(DIM_MA_A-i+2);
            (*pMSD2)[s] = static_cast<double>(DIM_MA_A-i+3);
            (*pMSD2)[s] = static_cast<double>(DIM_MA_A-i+4);
        }

        cout << " 13 --> Test for equality *pMSD1 and *pMSD2." << endl;

        for (i = 1; i <= NUM_DO_A; i++) {
            if (*pMSD1 == *pMSD2) {
                cout << " Equal --> " << i << endl;
            } else {
                cout << " Not Equal --> " << i << endl;
            }
        }

        cout << " 14 --> Size of *pMSD1." << endl;

        cout << pMSD1 -> size() << endl;

        cout << " 15 --> Size of *pMSD2." << endl;

        cout << pMSD2 -> size() << endl;

        cout << " 16 --> Find and erase element by element in pMSD1." << endl;

        for (i = 0; i < DIM_MA_A; i++) {
            s = convertLongInt(DIM_MA_A-i);

            itMSD = pMSD1 -> find(s);

            pMSD1 -> erase(itMSD);
        }

        cout << " 17 --> Find and erase element by element in pMSL2." << endl;

        for (i = 0; i < DIM_MA_A; i++) {
            s = convertLongInt(DIM_MA_A-i);

            itMSD = pMSD2 -> find(s);

            pMSD2 -> erase(itMSD);
        }

        cout << " 18 --> Size of *pMSD1." << endl;

        cout << pMSD1 -> size() << endl;

        cout << " 19 --> Size of *pMSD2." << endl;

        cout << pMSD2 -> size() << endl;

        // free up memory here

        cout << " 20 --> Free up RAM." << endl;

        delete pMSD1;
        delete pMSD2;

    }

    return 0;
}

//==============//
// End of code. //
//==============//