//=======================================================//
// Program to demonstrate use of the set template class. //
//=======================================================//

#include <iostream>
#include <set>

using std::cin;
using std::cout;
using std::endl;
using std::set;

int main()
{
    // local variables

    double tmpa;
    long tmpb;
    long i;
    long k;
    int sentinel;

    // local constants

    const long NUM_DO_A = 2*1E7;
    const long NUM_DO_C = 1*1E6;
    const long NUM_DO_B = 1E2;

    // stl containers

    set<double> *ps4;
    ps4 = new set<double>;

    // main code

    cout << "  0. Building the s4 set. " << endl;

    for (i = 0; i < NUM_DO_C; i++) {
        tmpa = static_cast<double>(i);
        (*ps4).insert(i);
    }

    for (k = 1; k <= NUM_DO_B; k++) {

        // STL containers

        set<double> *ps1;
        ps1 = new set<double>;

        set<double> *ps2;
        ps2 = new set<double>;

        set<double> *ps3;
        ps3 = new set<double>;

        // ======================================================= //

        cout << " ----------------------------------------------------->>> " << k << endl;

        cout << "  1. Building the s1 set. " << endl;

        for (i = 0; i < NUM_DO_A; i++) {
            tmpa = static_cast<double>(i);
            (*ps1).insert(tmpa);
        }

        cout << "  2. Equating s1, s2, and s3. " << endl;

        *ps2 = *ps1;
        cout << " s2 = s1 --> OKAY " << endl;
        *ps3 = *ps2;
        cout << " s3 = s2 --> OKAY " << endl;
        *ps1 = *ps1;
        cout << " s1 = s1 --> OKAY " << endl;

        cout << "  3. Size of s1, s2 and s3 sets. " << endl;

        cout << (*ps1).size() << endl;
        cout << (*ps2).size() << endl;
        cout << (*ps3).size() << endl;

        cout << "  4. Comparison of the s1, s2 and s3 sets. " << endl;

        cout << (*ps1 == *ps2) << endl;
        cout << (*ps1 == *ps3) << endl;
        cout << (*ps2 == *ps3) << endl;

        cout << "  5. Erasing all the elements of s1 set. " << endl;

        tmpb = ps1 -> size();
        for (i = 0; i < tmpb; i++) {
            tmpa = static_cast<double>(i);
            ps1 -> erase(tmpa);
        }

        cout << "  6. Size of s1, s2 and s3 sets. " << endl;

        cout << ps1 -> size() << endl;
        cout << ps2 -> size() << endl;
        cout << ps3 -> size() << endl;

        cout << "  7. Erasing all the elements of s2 set. " << endl;

        tmpb = ps2 -> size();
        for (i = 0; i < tmpb; i++) {
            tmpa = static_cast<double>(i);
            ps2 -> erase(tmpa);
        }

        cout << "  8. Size of s1, s2 and s3 sets. " << endl;

        cout << ps1 -> size() << endl;
        cout << ps2 -> size() << endl;
        cout << ps3 -> size() << endl;

        cout << "  9. Erasing all the elements of s3 set. " << endl;

        tmpb = ps3 -> size();
        for (i = 0; i < tmpb; i++) {
            tmpa = static_cast<double>(i);
            ps3 -> erase(tmpa);
        }

        cout << " 10. Size of s1, s2 and s3 sets. " << endl;

        cout << ps1 -> size() << endl;
        cout << ps2 -> size() << endl;
        cout << ps3 -> size() << endl;

        cout << " 11. Equating s1, s2, and s3 to s4. " << endl;

        *ps1 = *ps4;
        cout << " *ps1 = *ps4 --> OKAY " << endl;
        *ps2 = *ps4;
        cout << " *ps2 = *ps4 --> OKAY " << endl;
        *ps3 = *ps4;
        cout << " *ps3 = *ps4 --> OKAY " << endl;

        cout << " 12. Size of *ps1, *ps2 and *ps3 sets. " << endl;

        cout << ps1 -> size() << endl;
        cout << ps2 -> size() << endl;
        cout << ps3 -> size() << endl;

        delete ps1;
        delete ps2;
        delete ps3;
    }

    cin >> sentinel;

    return 0;
}

//==============//
// End of code. //
//==============//

