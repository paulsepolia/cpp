//=====================================================//
// Program to demonstrate the STL template class list. //
//=====================================================//

#include <iostream>
#include <deque>

using std::cout;
using std::cin;
using std::endl;
using std::deque;

int main()
{
    deque<double> listA;
    deque<double>::iterator iter;
    long i;
    long j;
    long breakStep;
    int sentinel;

    const long NUM_DO = 1E2;
    const long DIMEN_LIST_A = 1E8;
    const long DIMEN_LIST_B = 2E8;
    const long DIMEN_INSERT = 1E5;
    const long DIMEN_BREAK_A = 11;
    const long DIMEN_BREAK_B = 30;
    const long DIMEN_BREAK_C = 10;

    for (j = 0; j < NUM_DO; j++) {

        cout << "  Effort --------------------------------------> " << j+1 << endl;

        cout << "  1 --> Building the list. " << endl;
        cout << "  2 --> Using the push_back member function. " << endl;

        for (i = 1; i <= DIMEN_LIST_A; i++) {
            listA.push_back(i);
        }

        cout << "  3 --> Some output. " << endl;

        breakStep = 0;
        for (iter = listA.begin(); iter != listA.end(); iter++) {
            breakStep++;
            if (breakStep > DIMEN_BREAK_A) {
                break;
            }
            cout << *iter << " ";
        }

        cout << endl;

        cout << "  4 --> Setting all entries to 0. " << endl;

        for (iter = listA.begin(); iter != listA.end(); iter++) {
            *iter = 0;
        }

        cout << "  5 --> Some output. " << endl;

        breakStep = 0;
        for (iter = listA.begin(); iter != listA.end(); iter++) {
            breakStep++;
            if (breakStep > DIMEN_BREAK_A) {
                break;
            }
            cout << *iter << " ";
        }

        cout << endl;

        cout << "  6 --> The size of the list. " << endl;
        cout << listA.size() << endl;

        cout << "  7 --> Clear the list. " << endl;
        listA.clear();

        cout << "  8 --> The size of the list. " << endl;
        cout << listA.size() << endl;

        cout << "  9 --> Building the list. " << endl;
        cout << " 10 --> Using the push_back member function. " << endl;

        for (i = 1; i <= DIMEN_LIST_B; i++) {
            listA.push_back(i);
        }

        cout << " 11 --> The size of the list. " << endl;
        cout << listA.size() << endl;

        cout << " 12 --> Some output. " << endl;

        breakStep = 0;
        for (iter = listA.begin(); iter != listA.end(); iter++) {
            breakStep++;
            if (breakStep > DIMEN_BREAK_A) {
                break;
            }
            cout << *iter << " ";
        }

        cout << endl;

        cout << " 13 --> Clear the list. " << endl;
        listA.clear();

        cout << " 14 --> The size of the list. " << endl;
        cout << listA.size() << endl;

        cout << " 15 --> Building the list. " << endl;
        cout << " 16 --> Using the push_front member function. " << endl;

        for (i = 1; i <= DIMEN_LIST_B; i++) {
            listA.push_front(i);
        }

        cout << " 17 --> Insert some elements. " << endl;

        iter = listA.begin();
        breakStep = 0;
        for(i = 1; i <= DIMEN_INSERT; i++) {
            iter++;
            listA.insert(iter,10);
            iter++;
            iter++;

            breakStep++;
            if (breakStep <= DIMEN_BREAK_C) {
                cout << i << endl;
            }

        }

        cout << " 18 --> Erase the list element-by-element. " << endl;

        breakStep = 0;
        for (i = 1; i <= listA.size(); i++) {
            cout << i << " " << endl;
            iter = listA.begin();
            listA.erase(iter);

            breakStep++;
            if (breakStep > DIMEN_BREAK_B) {
                break;
            }
        }

        cout << " 19 --> The size of the list. " << endl;
        cout << listA.size() << endl;

        cout << " 20 --> Clear the list. " << endl;
        listA.clear();

        cout << " 21 --> The size of the list. " << endl;
        cout << listA.size() << endl;

    }

    cin >> sentinel;

    return 0;
}

//==============//
// End of code. //
//==============//

