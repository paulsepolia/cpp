//=======================================================//
// Program to demonstrate use of the map template class. //
//=======================================================//

#include <iostream>
#include <map>

using std::cout;
using std::endl;
using std::map;

int main()
{
    // variables

    long i;
    long k;
    long tmpb;
    double tmpa;

    // constants

    const long NUM_DO_A = 1E7;
    const long NUM_DO_B = 1E2;

    // main code

    for (k = 1; k <= NUM_DO_B; k++) {

        // stl containers

        map<long, double> *pma1 = new map<long, double>;
        map<long, double> *pma2 = new map<long, double>;
        map<long, double> *pma3 = new map<long, double>;

        cout << "  1. Building the ma1 map. " << endl;

        for (i = 0; i < NUM_DO_A; i++) {
            tmpa = static_cast<double>(i+1);
            (*pma1)[i] = tmpa;
        }

        cout << "  2. Equating pma1, pma2, and pma3. " << endl;

        *pma2 = *pma1;
        cout << " okay --> *pma2 = *pma1 " << endl;
        *pma3 = *pma1;
        cout << " okay --> *pma3 = *pma1 " << endl;

        cout << "  3. Sizes of the maps. " << endl;

        cout << pma1 -> size() << endl;
        cout << pma2 -> size() << endl;
        cout << pma3 -> size() << endl;

        cout << "  4. Erasing element by element *pma1 map. " << endl;

        tmpb = (*pma1).size();
        for (i = 0; i < tmpb; i++) {
            pma1 -> erase(i);
        }

        cout << "  5. Sizes of the maps. " << endl;

        cout << pma1 -> size() << endl;
        cout << pma2 -> size() << endl;
        cout << pma3 -> size() << endl;

        cout << "  6. Erasing element by element *pma2 map. " << endl;

        tmpb = (*pma2).size();
        for (i = 0; i < tmpb; i++) {
            pma2 -> erase(i);
        }

        cout << "  7. Sizes of the maps. " << endl;

        cout << pma1 -> size() << endl;
        cout << pma2 -> size() << endl;
        cout << pma3 -> size() << endl;

        cout << "  8. Erasing element by element *pma3 map. " << endl;

        tmpb = (*pma3).size();
        for (i = 0; i < tmpb; i++) {
            pma3 -> erase(i);
        }

        cout << "  9. Sizes of the maps. " << endl;

        cout << pma1 -> size() << endl;
        cout << pma2 -> size() << endl;
        cout << pma3 -> size() << endl;

        cout << "  X. Deleting the maps. " << endl;

        delete pma1;
        delete pma2;
        delete pma3;
    }

    return 0;
}

//==============//
// End of code. //
//==============//
