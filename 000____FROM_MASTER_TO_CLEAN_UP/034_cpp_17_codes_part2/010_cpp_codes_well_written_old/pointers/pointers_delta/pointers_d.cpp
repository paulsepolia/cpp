#include <iostream>

using namespace std;

int main()
{
    int *p;

    p = new int;
    *p = 54;

    cout << "  p = " <<  p << endl;
    cout << " &p = " << &p << endl;
    cout << " *p = " << *p << endl;

    delete p;
    p = NULL;

    p = new int;
    *p = 73;

    cout << "  p = " <<  p << endl;
    cout << " &p = " << &p << endl;
    cout << " *p = " << *p << endl;

    delete p;
    p = NULL;

    return 0;
}

