//==============//
// Header file. //
// PFArrayDCL.h //
//==============//

//==============================================================//
// This is the interface for the class PFArrayDCL.              //
// Objects of this type are partially filled arrays of doubles. //
//==============================================================//

#ifndef PFARRAYD_CL_H
#define PFARRAYD_CL_H

namespace PGG {

class PFArrayDCL {
public:
    PFArrayDCL();
    // Initializes with a capacity of 50.

    PFArrayDCL(int capacityValue);

    PFArrayDCL(const PFArrayDCL& pfaObject);

    void addElement(double element);
    // Precondition: The array is not full.
    // Postcondition: The element has been added.

    bool full() const;
    // Returns true if the array is full, false otherwise.

    int getCapacity() const;

    int getNumberUsed() const;

    void emptyArray();
    // Resets the number used to zero,
    // effectivelly emptying the array.

    double& operator[](int index);
    // Read and change access to elements 0 through numberUsed-1.

    PFArrayDCL& operator =(const PFArrayDCL& rightSide);

    ~PFArrayDCL();

protected:
    double *a; // for an array of doubles.
    int capacity; // for the size of the array.
    int used; // for the number of array positions currently in use.
};

} // end of namespace PGG

#endif // end of guard PFARRAYD_CL_H 

//==============//
// End of code. //
//==============//
