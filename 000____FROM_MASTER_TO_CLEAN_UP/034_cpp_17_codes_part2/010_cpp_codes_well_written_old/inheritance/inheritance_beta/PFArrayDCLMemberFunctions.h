//===========================//
// Header file.              //
// PFArrayDCLMemberFunctions.h //
//===========================//

// This is the implementation part of the PFArrayCL class.

#ifndef PFARRAYD_CL_MF_H
#define PFARRAYD_CL_MF_H

#include <iostream>
#include <cstdlib>
using std::cout;
using std::endl;

#include "PFArrayDCL.h"

namespace PGG {

// 1.

PFArrayDCL::PFArrayDCL()
    : capacity(50),
      used(0)
{
    a = new double [capacity];
}

// 2.

PFArrayDCL::PFArrayDCL(int size)
    : capacity(size),
      used(0)
{
    a = new double [capacity];
}

// 3.

PFArrayDCL::PFArrayDCL(const PFArrayDCL& pfaObject)
    : capacity(pfaObject.getCapacity()),
      used(pfaObject.getNumberUsed())
{
    a = new double [capacity];
    for (int i = 0; i < used; i++) {
        a[i] = pfaObject.a[i];
    }
}

// 4.

double& PFArrayDCL::operator[](int index)
{
    if (index >= used) {
        cout << "Illegal index in PFArrayDCL." << endl;
        exit(1);
    }

    return a[index];
}

// 5.

PFArrayDCL& PFArrayDCL::operator=(const PFArrayDCL& rightSide)
{
    if (capacity != rightSide.capacity) {
        delete [] a;
        a = new double [rightSide.capacity];
    }

    capacity = rightSide.capacity;
    used = rightSide.used;

    for (int i = 0; i < used; i++) {
        a[i] = rightSide.a[i];
    }

    return *this;
}

// 6.

PFArrayDCL::~PFArrayDCL()
{
    delete [] a;
}

} // end of namespace PGG

#endif // end of guard PFARRAYD_CL_MF_H

//==============//
// End of code. //
//==============//
