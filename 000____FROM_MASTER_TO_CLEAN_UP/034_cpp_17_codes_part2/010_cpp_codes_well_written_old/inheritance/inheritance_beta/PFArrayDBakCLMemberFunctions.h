//================================//
// Header file.                   //
// PFArrayDBakCLMemberFunctions.h //
//================================//

//==============================================================//
// This is the implementation file for the class PFArrayDBakCL. //
//==============================================================//

#ifndef PFARRAYDBAK_CL_MF_H
#define PFARRAYDBAK_CL_MF_H

#include <iostream>
using std::cout;

#include "PFArrayDCL.h"
#include "PFArrayDBakCL.h"

namespace PGG {

// 1.

PFArrayDBakCL::PFArrayDBakCL()
    : PFArrayDCL(), usedB(0)
{
    b = new double [capacity];
}

// 2.

PFArrayDBakCL::PFArrayDBakCL(int capacityValue)
    : PFArrayDCL(capacityValue),
      usedB(0)
{
    b = new double [capacity];
}

// 3.

PFArrayDBakCL::PFArrayDBakCL(const PFArrayDBakCL& oldObject)
    : PFArrayDCL(oldObject),
      usedB(0)
{
    b = new double [capacity];
    usedB = oldObject.usedB;
    for (int i = 0; i < usedB; i++) {
        b[i] = oldObject.b[i];
    }
}

// 4.

void PFArrayDBakCL::backup()
{
    usedB = used;
    for (int i = 0; i < usedB; i++) {
        b[i] = a[i];
    }
}

// 5.

void PFArrayDBakCL::restore()
{
    used = usedB;
    for (int i = 0; i < used; i++) {
        a[i] = b[i];
    }
}

// 6.

PFArrayDBakCL& PFArrayDBakCL::operator=(const PFArrayDBakCL& rightSide)
{
    int oldCapacity = capacity;
    PFArrayDCL::operator=(rightSide); // use of base class =
    if (oldCapacity != rightSide.capacity) {
        delete [] b;
        b = new double[rightSide.capacity];
    }

    usedB = rightSide.usedB;
    for(int i = 0; i < usedB; i++) {
        b[i] = rightSide.b[i];
    }

    return *this;
}

// 7.

PFArrayDBakCL::~PFArrayDBakCL() // the destructor of the base class is called automatically
{
    delete [] b;
}

} // End of namespace PGG

#endif // end of guard PFARRAYDBAK_CL_MF_H
