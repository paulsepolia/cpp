//=====================================//
// Header file.                        //
// SalariedEmployeeCLMemberFunctions.h //
//=====================================//

#ifndef SALARIED_EMPLOYEE_CL_MF_H
#define SALARIED_EMPLOYEE_CL_MF_H

#include <iostream>
#include <string>
using std::string;
using std::cout;
using std::endl;

#include "EmployeeCL.h"
#include "SalariedEmployeeCL.h"

namespace PGG {

// 1.

SalariedEmployeeCL::SalariedEmployeeCL()
    : EmployeeCL(),
      salary(0)
{
    // deliberately empty
}

// 2.

SalariedEmployeeCL::SalariedEmployeeCL(string theName,
                                       string theNumber,
                                       double theWeeklyPay)
    : EmployeeCL(theName, theNumber),
      salary(theWeeklyPay)
{
    // deliberately empty
}

// 3.

double SalariedEmployeeCL::getSalary() const
{
    return salary;
}

// 4.

void SalariedEmployeeCL::setSalary(double newSalary)
{
    salary = newSalary;
}

// 5.

void SalariedEmployeeCL::printCheck()
{
    setNetPay(salary);

    cout << endl;
    cout << "--------------------------------------------------" << endl;
    cout << "Pay of the order of " << getName() << endl;
    cout << "The sum of " << getNetPay() << " Dollars\n";
    cout << "--------------------------------------------------" << endl;
    cout << "Check Stub NOT NEGOTIABLE \n";
    cout << "Employee Number: " << getSsn() << endl;
    cout << "Salaried Empoyee. Regular Pay: "
         << salary << endl;
    cout << "--------------------------------------------------" << endl;
}

} // end of namespace PGG

#endif // end of guard SALARIED_EMPLOYEE_CL_MF_H

//==============//
// End of code. //
//==============//
