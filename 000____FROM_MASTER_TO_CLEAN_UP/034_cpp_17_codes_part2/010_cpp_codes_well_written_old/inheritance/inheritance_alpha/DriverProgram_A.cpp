//=============================//
// This is the driver program. //
//=============================//

#include <iostream>
using std::cout;
using std::endl;

#include "EmployeeCL.h"
#include "EmployeeCLMemberFunctions.h"
#include "HourlyEmployeeCL.h"
#include "HourlyEmployeeCLMemberFunctions.h"
#include "SalariedEmployeeCL.h"
#include "SalariedEmployeeCLMemberFunctions.h"

using PGG::EmployeeCL;
using PGG::HourlyEmployeeCL;
using PGG::SalariedEmployeeCL;

int main()
{
    HourlyEmployeeCL joe;
    joe.setName("Mighty Joe");
    joe.setSsn("123-45-6789");
    joe.setRate(20.50);
    joe.setHours(40);

    cout << "Check for "
         << joe.getName()
         << " for "
         << joe.getHours()
         << " hours "
         << endl;

    joe.printCheck();

    cout << endl;

    SalariedEmployeeCL boss("Mr. Big Shot", "987-65-4321", 10500.50);

    cout << "Check for "
         << boss.getName()
         << endl;

    boss.printCheck();

    return 0;
}

//==============//
// End of code. //
//==============//
