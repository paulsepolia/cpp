// Copy a file.

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>

using std::ifstream;
using std::ofstream;
using std::endl;
using std::cout;
using std::string;

int main()
{
    ifstream inStream;
    ofstream outStream;
    char nextChar;

    // open the input stream

    inStream.open("fileIn.txt");

    // test whether or not the opening was succesfull

    if (inStream.fail()) {
        cout << "Input file opening failed." << endl;
        exit(1);
    }

    // open the output stream

    outStream.open("fileOut.txt");

    // test whther or not the opening was succesfull

    if (outStream.fail()) {
        cout << "Output file opening failed." << endl;
        exit(1);
    }

    // get the n-th character
    // and write it in the output stream
    // until the input stream reaches the eof.

    string s1;
    string s2;

    inStream.get(nextChar);
    while (!inStream.eof()) {
        s1 = nextChar;
        s2 = s2 + s1;             // this update is very slow
        inStream.get(nextChar);
    }

    long i;
    const long LEN = s2.length();
    for (i = 0; i < LEN; i++) {
        outStream.put(s2[i]);
    }

    // a test.
    // cout << s2 << endl;

    return 0;
}

// end of code.

