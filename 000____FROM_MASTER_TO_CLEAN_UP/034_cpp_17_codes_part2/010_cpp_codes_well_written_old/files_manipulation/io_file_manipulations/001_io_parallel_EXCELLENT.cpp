// Parallel output.
// Writing to many opened file.

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

using std::stringstream;
using std::string;
using std::ofstream;
using std::cout;
using std::endl;

int main()
{
    const int NUM_FILES = 1E3;        // number of files to open to write
    const int NUM_LINES = 1E7;

    ofstream* pof;                    // array of ofstream objects --> step 1
    pof = new ofstream [NUM_FILES];   // array of ofstream objects --> step 2

    string strA = "outfile_";
    string strB = ".txt";
    string strC;
    stringstream ss;                  // create a stringstream
    int i;
    int j;
    int num = 11;

    // Creating the files here.

    for (i = 0; i < NUM_FILES; i++) {
        ss << i;                        // add number to the stream
        strC = strA + ss.str() + strB;

        char* ctmp = &strC[0];          // convert string to char *
        int len = strC.length();
        ctmp = new char [len];
        for (j = 0; j < len; j++) {
            ctmp[j] = strC[j];
        }

        //cout << strC << endl;
        //cout << ctmp << endl;
        //cout << len << endl;

        pof[i].open(ctmp);             // open the streams

        ss.str("");                    // clean the stream --> step 1
        ss.clear();                    // clean the stream --> step 2
    }

    // Writing to the disk here.

    for (j = 0; j < NUM_LINES; j++) {
        for (i = 0; i < NUM_FILES; i++) {
            pof[i] << num << " + " << num << endl;
        }
    }

    // Closing the streams.

    for (i = 0; i < NUM_FILES; i++) {
        pof[i].close();
    }

    return 0;
}
