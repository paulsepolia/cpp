// Copy a file.
// string::reserve
#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>

using namespace std;

int main ()
{
    ofstream outStream;  // the input stream
    ifstream inStream;   // the output stream
    string str;          // string to store the whole file
    long filesize;     // the size of the file

    // opening the input stream

    inStream.open("fileIn.txt");

    // test whether of not the opening was succesfull

    if (inStream.fail()) {
        cout << "Input file opening failed." << endl;
        exit(1);
    }

    // getting the size of the file

    cout << " tellg() starts. " << endl;

    filesize = inStream.tellg();

    cout << " tellg() ends. " << endl;

    // allocate RAM to hold the whole file

    cout << " reserve() starts. " << endl;

    str.reserve(filesize);

    cout << " reserve() ends. " << endl;
    cout << " filesize " << filesize << endl;

    // ???

    cout << " seekg() starts. " << endl;

    inStream.seekg(0);

    cout << " seekg() ends. " << endl;

    // the following statement brakes
    // for more than 4 GBytes ??

    cout << " copy starts. " << endl;

    while (!inStream.eof()) {
        str += inStream.get();
    }

    cout << " copy ends. " << endl;

    // open the output stream

    outStream.open("fileOut.txt");

    // test whether or not the opening was succesfull

    if (outStream.fail()) {
        cout << "Output file opening failed." << endl;
        exit(1);
    }

    // write to the hard disk

    cout << " main copy starts. " << endl;

    long i;
    const long LEN = str.length();
    for (i = 0; i < LEN; i++) {
        outStream.put(str[i]);
    }

    return 0;
}

// end of code.

