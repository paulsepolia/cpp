// Copy a file.

#include <iostream>
#include <fstream>
#include <cstdlib>

using std::ifstream;
using std::ofstream;
using std::endl;
using std::cout;

int main()
{
    ifstream inStream;
    ofstream outStream;
    char nextChar;

    // open the input stream

    inStream.open("fileIn.txt");

    // test whether or not the opening was succesfull

    if (inStream.fail()) {
        cout << "Input file opening failed." << endl;
        exit(1);
    }

    // open the output stream

    outStream.open("fileOut.txt");

    // test whther or not the opening was succesfull

    if (outStream.fail()) {
        cout << "Output file opening failed." << endl;
        exit(1);
    }

    // get the n-th character
    // and write it in the output stream
    // until the input stream reaches the eof.

    inStream.get(nextChar);
    while (!inStream.eof()) {
        outStream.put(nextChar);
        inStream.get(nextChar);
    }

    return 0;
}

// end of code.

