#include <iostream>
using namespace std;

int main()
{
    long int i;
    long int j;
    const long int dimenAlpha = 10E+1;
    const long int dimenBeta  = 10E+7;
    int sentinel;

    for ( i=0; i<dimenAlpha; i++) {
        long int *numArray = new long int [ dimenBeta ];

        for ( j=0; j<dimenBeta; j++ ) {
            numArray[j] = j;
        }

        if ( i % 5 == 0 ) cout << i << endl;

        delete [] numArray ;
    }

    cout << " enter an integer to exit: ";
    cin >> sentinel;

    return 0;
}
