// dynamic two dimensional arrays

#include <iostream>
#include <iomanip>

using namespace std;

// declaration of the external functions

// 1.

void fill(int **p, int rowSize, int columnSize);

// 2.

void print(int **p, int rowSize, int columnSize);

int main()
{
    int **board;

    int rows;
    int columns;

    cout << " Enter the number of rows and columns: ";
    cin >> rows >> columns ;
    cout << endl;

    // create the rows of board

    board = new int* [rows];

    // create the columns of board

    for (int row = 0; row < rows; row++) {
        board[row] = new int[columns];
    }

    // insert elements into board

    fill(board, rows, columns);

    cout << " Board: " << endl;

    print(board, rows, columns);

    return 0;
}

// definition of the external functions.

// 1.

void fill(int **p, int rowSize, int columnSize)
{
    for (int row = 0; row < rowSize; row++) {
        cout << "Enter " << columnSize << " number(s) for row "
             << " number " << row << ": ";

        for (int col = 0; col < columnSize; col++) {
            cin >> p[row][col];
        }

        cout << endl;
    }
}

// 2.

void print(int **p, int rowSize, int columnSize)
{
    for (int row = 0; row < rowSize; row++) {
        for (int col = 0; col < columnSize; col++) {
            cout << setw(5) << p[row][col];
        }

        cout << endl;
    }
}

// this is the end of the program
