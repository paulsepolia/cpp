#include <iostream>
using namespace std;

// 1. function prototype
long int findMax( long int *, long int );

// 2. main function
int main()
{
    const long int dimen = 10E+7;
    const int trials = 10E+4;
    long int * nums = new long int [ dimen ];
    long int i;
    double loc=0;


    for ( i=0; i<dimen; i++ ) {
        nums[i] = i ;
    }

    for ( i=0; i<trials; i++ ) {
        loc = loc + findMax(nums,dimen);
    }

    cout << " loc = " << loc << endl;

    return 0;
}

// 3. function definition

long int findMax( long int *vals, long int numels )
{
    long int i, max = *vals;

    for ( i = 1; i < numels; i++ ) {
        if ( max < *(vals+i) ) max = *(vals+i);
    }

    return max;
}

