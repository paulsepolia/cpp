//=====================//
// Virtual destructor. //
//=====================//

#include <iostream>

using std::cout;
using std::endl;

// 1. Class B --> The base class.

class B {
public:
    ~B() {}
};

// 2. Class D --> The derived class.

class D: public B {
public:
    D()
    {
        a = new int[1000000];
    }
    ~D()
    {
        delete[] a;
        cout << "Memory released!" << endl;
    }

private:
    int *a;
};

// 3. A function.

void f()
{
    B *p;

    p = new D; // p is a pointer to a B class
    // but points to a D class object. OKAY.
    // Memory is allocated for 1000000 integers.

    delete p;  // Released again, thanks to virtual destructor.
}

// 4. The main function.

int main()
{
    f();

    return 0;
}

//==============//
// End of code. //
//==============//

