//==============================================//
// This program finds out whether the left most //
// bit of type char is a sign bit.              //
//==============================================//

#include <iostream>

using std::cout;
using std::endl;

int main()
{
    signed char   s_ch = '\xFF';
    unsigned char u_ch = '\xFF';
    char            ch = '\xFF'; // binary s_ch = u_ch = ch = 11111111

    int s;
    int u;
    int i;

    s = s_ch; // From signed char to int
    u = u_ch; // From unsigned char to int
    i = ch;   // From char to int - system dependent

    cout << "For this C++ implementation, type char has ";

    if (i == s) {
        cout << "a sign bit." << endl;
    } else if (i == u) {
        cout << "no sign bit." << endl;
    } else {
        cout << "not been implemented correctly." << endl;
    }

    cout << " The character is --> s_ch --> " << s_ch << endl;
    cout << " The character is --> u_ch --> " << u_ch << endl;
    cout << " The character is -->   ch --> " << ch   << endl;

    cout << " The integer is --> s --> " << s << endl;
    cout << " The integer is --> u --> " << u << endl;
    cout << " The integer is --> i --> " << i << endl;

    return 0;
}

//==============//
// End of code. //
//==============//
