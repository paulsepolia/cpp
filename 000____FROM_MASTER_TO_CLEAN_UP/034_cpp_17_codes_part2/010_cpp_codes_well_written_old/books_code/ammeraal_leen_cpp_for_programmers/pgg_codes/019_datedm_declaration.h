//===========================//
// DateDM class header file. //
//===========================//

#ifndef DATEDM_DECLARATION_H
#define DATEDM_DECLARATION_H

class DateDM {
public:
    DateDM(int d = 1, int m = 1);
    int difference(const DateDM &date) const;

private:
    int day;
    int month;
};

#endif // end of guard DATEDM_DECLARATION_H

//==============//
// End of code. //
//==============//
