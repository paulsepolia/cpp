//===================//
// A priority queue. //
//===================//

#include <iostream>
#include <vector>
#include <queue>

using std::endl;
using std::cout;
using std::priority_queue;
using std::vector;

int main()
{
    priority_queue<long, vector<long> > PQ;

    long x;

    PQ.push(123);
    PQ.push(51);
    PQ.push(1000);
    PQ.push(17);

    while(!PQ.empty()) {
        x = PQ.top();
        cout << "Retrieved element: " << x << endl;
        PQ.pop();
    }

    return 0;
}

//==============//
// End of code. //
//==============//
