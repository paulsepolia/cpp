//============================//
// A constructor initializer. //
//============================//

#include <iostream>

using std::endl;
using std::cout;

// 1. TestInit class declaration and definition

class TestInit {
public:
    TestInit(int i, float &x)
        : ii(i), xx(x)
    {}

    void print() const
    {
        cout << ii << " " << xx << endl;
    }

private:
    const int ii;
    float &xx;
};

// 2. The main function.

int main()
{
    float y = 8.5F;
    int i = 10;
    TestInit t(i, y);

    t.print();

    return 0;
}

//==============//
// End of code. //
//==============//
