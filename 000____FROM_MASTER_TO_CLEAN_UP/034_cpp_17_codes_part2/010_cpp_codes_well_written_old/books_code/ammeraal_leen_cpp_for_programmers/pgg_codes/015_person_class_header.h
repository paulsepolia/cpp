//==================================//
// Person Class.                    //
// Header file of the class Person. //
//==================================//

#include <string>
#include <iostream>
#include <iomanip>

using std::string;
using std::setw;
using std::cout;
using std::endl;
using std::left;
using std::right;

class Person {
public:

    // 1. Hand made constructor.

    Person(const string &s = "", int yr = 0, bool m = true)
    {
        name = s;
        yearOfBirth = yr;
        male = m;
    }

    // 2. 'setName' member function.

    void setName(const string &s)
    {
        name = s;
    }

    // 3. 'setYear' member function.

    void setYear(int yr)
    {
        yearOfBirth = yr;
    }

    // 4. 'setMF' member function.

    void setMF(bool m)
    {
        male = m;
    }

    // 5. 'getName' member function.

    const string &getName() const
    {
        return name;
    }

    // 6. 'getAgeAtEnd2000' member function.

    int getAgeAtEnd2000() const
    {
        return (2000 - yearOfBirth);
    }

    // 7. 'print' meber function.

    void print() const
    {
        cout << setw(10) << left << this -> name                // you can delete the 'this ->'
             << setw(4)  << right << this -> getAgeAtEnd2000()  // you can delete the 'this ->'
             << ( this -> male ? " (M)" : " (F)") << endl;      // you can delete the 'this ->'
    }

private:

    string name;
    int yearOfBirth;
    bool male;

};

//==============//
// End of code. //
//==============//
