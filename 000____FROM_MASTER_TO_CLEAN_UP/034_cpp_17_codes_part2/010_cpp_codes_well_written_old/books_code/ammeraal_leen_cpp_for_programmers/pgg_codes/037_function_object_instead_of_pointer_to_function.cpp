//=============================================================//
// Function object used to pass functions to another function. //
//=============================================================//

#include <iostream>

using std::cout;
using std::endl;

// 1. 'fun' class.

class fun {
public:
    virtual double operator()(long k) const = 0;
};

// 2. 'reciprocal' class.

class reciprocal: public fun {
public:
    double operator()(long k) const
    {
        return (1.0/k);
    }
};

// 3. 'square' class.

class square: public fun {
public:
    double operator()(long k) const
    {
        return static_cast<double>(k) * k;
    }
};

// 4. 'funsum' function.

double funsum(long n, const fun &f)
{
    double s = 0;
    long i;

    for (i = 1; i <= n; ++i) {
        s += f(i);
    }

    return s;
}

// 5. The main function.

int main()
{
    const long NUM_DO = 1E10;

    cout << "Sum of five reciprocals: "
         << funsum(NUM_DO, reciprocal()) << endl;

    cout << "Sum of three squares: "
         << funsum(3, square()) << endl;

    return 0;
}

//==============//
// End of code. //
//==============//
