//=======================================//
// string - vector - algorithm - getline //
//=======================================//

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::vector;

int main()
{
    vector<string> v;
    cout << " Enter lines of text to be sorted," << endl;
    cout << " followed by the word stop:" << endl;

    for(;;) {
        string s;
        getline(cin, s);
        if (s == "stop")
            break;
        v.push_back(s);
    }

    sort(v.begin(), v.end());

    cout << " The same lines after sorting: " << endl;

    for (int i = 0; i < v.size(); i++) {
        cout << v[i] << endl;
    }

    return 0;
}

//==============//
// End of code. //
//==============//
