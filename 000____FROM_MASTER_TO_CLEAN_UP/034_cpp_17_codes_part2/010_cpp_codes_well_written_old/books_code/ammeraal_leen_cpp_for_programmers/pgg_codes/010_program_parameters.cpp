//============================================//
// Demonstration of using program parameters. //
//============================================//

#include <iostream>

using std::endl;
using std::cout;

int main(int argc, char *argv[])
{
    cout << " argc    = " << argc << endl;

    for (int i = 1; i < argc; i++) {
        cout << "argv[" << i << "] = " << argv[i] << endl;
    }

    return 0;
}

//==============//
// End of code. //
//==============//

