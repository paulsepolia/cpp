//==================//
// A call-operator. //
//==================//

#include <iostream>

using std::cout;
using std::endl;

// 1. 'iprint' class.

class iprint {
public:
    void operator()(int i) const
    {
        cout << " i = " << i << endl;
    }
};

// 2. The main function.

int main()
{
    iprint x;
    x(123);         // Output: 123
    iprint()(456);  // Output: 456

    return 0;
}

//==============//
// End of code. //
//==============//
