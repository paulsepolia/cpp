//=================================//
// Driver program to class DateDM. //
//=================================//

#include <iostream>
#include "019_datedm_declaration.h"
#include "019_datedm_definition.h"

using std::cout;
using std::cin;
using std::endl;

int main()
{
    int d;
    int m;

    cout << " Enter day and month numbers of date 1: " << endl;

    cin >> d >> m;

    DateDM date1(d, m);

    cout << " Enter day and month numbers of date 2: " << endl;

    cin >> d >> m;

    DateDM date2(d, m);

    cout << " Difference in a non-leap year: "
         << date1.difference(date2) << " days." << endl;

    return 0;
}

//==============//
// End of code. //
//==============//
