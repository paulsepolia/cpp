//=============================//
// Exception handling example. //
//=============================//

#include <iostream>

using std::cin;
using std::cout;
using std::endl;

// 1. 'detail' function definition.

void detail(int k)
{
    cout << " Start of detail function." << endl;

    if (k == 0) throw 123;

    cout << " End of detail function." << endl;
}

// 2. 'compute' function definition.

void compute(int i)
{
    cout << " Start of compute function." << endl;

    detail(i);

    cout << " End of compute function." << endl;
}

// 3. Main function.

int main()
{
    int x;

    cout << " Start of main function." << endl;

    cout << " Enter x (0 will throw an exception): ";

    cin >> x;

    try {
        compute(x);
    } catch (int i) {
        cout << " Exception: " << i << endl;
    }

    cout << " The End." << endl;

    return 0;
}

//==============//
// End of code. //
//==============//
