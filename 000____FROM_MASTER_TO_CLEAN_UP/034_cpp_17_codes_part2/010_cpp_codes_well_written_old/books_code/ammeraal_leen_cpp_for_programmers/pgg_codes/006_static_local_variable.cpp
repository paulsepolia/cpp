//==========================//
// A local static variable. //
//==========================//

#include <iostream>

using std::cout;
using std::endl;

// 1. Function declaration and definition

void f()
{
    static int i = 1;
    cout << i++ << endl;
}

int main()
{
    f();
    f();
    f();
    f();

    return 0;
}

//==============//
// End of code. //
//==============//

