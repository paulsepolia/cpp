//==============================//
// In-memory format conversion. //
//==============================//

#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>

using std::endl;
using std::cout;
using std::string;
using std::istringstream;
using std::ostringstream;
using std::fixed;
using std::setw;
using std::setprecision;

int main()
{
    // From a string s to numerical value x

    string s("1.234567890123456789E+123");
    istringstream istr(s);

    long double x;

    istr >> x;

    cout << setprecision(20) << endl;

    cout << " 1. The string value                is " << s << endl;
    cout << " 2. The numerical long double value is " << x << endl;

    // From numerical value y to string t:

    long double y = 1.234567890123456789E+123;

    ostringstream ostr;

    cout << " 3. The numerical long double value is " << y << endl;

    ostr << setw(20) << y;

    string t = ostr.str();

    cout << " 4. The corresponding string        is " << t << endl;

    return 0;
}

//==============//
// End of code. //
//==============//

