//=============================================================//
// Function object used to pass functions to another function. //
//=============================================================//

//=====//
// AND //
//=====//

//==========================//
// Function as an argument. //
//==========================//

//==========================================//
// Two different ways to do the same thing. //
//==========================================//

#include <iostream>
#include <iomanip>

using std::cout;
using std::endl;
using std::fixed;
using std::setprecision;

// 1. 'fun' class.

class fun {
public:
    virtual double operator()(long k) const = 0;
};

// 2. 'reciprocal' class.

class reciprocal: public fun {
public:
    double operator()(long k) const
    {
        return (1.0/k);
    }
};

// 3. 'square' class.

class square: public fun {
public:
    double operator()(long k) const
    {
        return static_cast<double>(k) * k;
    }
};

// 4. 'funsum' function.

double funsum(long n, const fun &f)
{
    double s = 0;
    long i;

    for (i = 1; i <= n; ++i) {
        s += f(i);
    }

    return s;
}

// 5. 'funsumB' function.

double funsumB(long n, double (*f)(long k))
{
    double s;
    long i;

    s = 0;

    for (i = 1; i <= n; i++) {
        s += (*f)(i);
    }

    return s;
}

// 6. 'reciprocalB' function.

double reciprocalB(long k)
{
    const double ONE = 1.0L;
    return (ONE / k);
}

// 7. 'squareB' function.

double squareB(long k)
{
    return (static_cast<double>(k)*k);
}

// 8. Main function.

int main()
{
    const double ONE = 1.0L;
    const long SUM_DO = 1*1E10;
    double tmpa;
    double tmpb;

    cout << endl;

    cout << " 1 --> Now running the function as argument." << endl;

    tmpa = funsumB(SUM_DO, &reciprocalB);

    cout << fixed << setprecision(20);

    cout << endl;

    cout << " 2 --> The result is " << tmpa << endl;

    cout << endl;

    cout << " 3 --> Now running the hand made do-loop." << endl;

    tmpb = 0.0;

    for (long i = 1; i <= SUM_DO; i++) {
        tmpb = tmpb + ONE / i;
    }

    cout << endl;

    cout << " 4 --> The result is " << tmpb << endl;

    cout << endl;

    cout << " 5 --> Sum of three squares is " << funsumB(3, &squareB) << endl;

    cout << endl;

    cout << " 6 --> Now running the class method." << endl;

    tmpb = funsum(SUM_DO, reciprocal());

    cout << endl;

    cout << " 7 --> The result is " << tmpb << endl;

    cout << endl;

    cout << " 8 --> Sum of three squares is " << funsum(3, square()) << endl;

    cout << endl;

    return 0;
}

//==============//
// End of code. //
//==============//
