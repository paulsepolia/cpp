// Heap data member
// Demonstrates an object with a dynamically allocated data member

#include <iostream>
#include <string>

using namespace std;

// class definition

class Critter {
public:
    Critter(const string& name="", int age=0); // constructor declaration
    ~Critter(); //destructor declaration
    Critter(const Critter& c); // copy constructor declaration
    Critter& Critter::operator=(const Critter& c); // overloaded assignment operator
    void Greet() const; // public member function

private:
    string* m_pName;
    int m_Age;
};

// constructor definition

Critter::Critter(const string& name, int age)
{
    cout << "Constructor called\n";
    m_pName = new string(name);
    m_Age = age;
}

// destructor definition

Critter::~Critter()
{
    cout << "Destructor called\n";
    delete m_pName;
}

// copy constructor definition

Critter::Critter(const Critter& c)
{
    cout << "Copy Constructor called\n";
    m_pName = new string(*(c.m_pName));
    m_Age = c.m_Age;
}

// overload assignment operator definition

Critter& Critter::operator=(const Critter& c)
{
    cout << "Overloaded Assignment Operator called:\n";
    if (this != &c) {
        delete m_pName;
        m_pName = new string(*(c.m_pName));
        m_Age = c.m_Age;
    }

    return *this;
}

// public constant member function definition

void Critter::Greet() const
{
    cout << "I am " << *m_pName << " and I am " << m_Age << " years old.\n";
    cout << "&m_pName: " << cout << &m_pName << endl;
}

// GLOBAL functions declaration

void testDestructor();
void testCopyConstructor(Critter aCopy);
void testAssignmentOp();

// main function

int main()
{
    testDestructor();
    cout << endl;

    Critter crit("Poochie", 5);
    crit.Greet();
    testCopyConstructor(crit);
    crit.Greet();

    cout << endl;

    testAssignmentOp();

    cout << endl;

    return 0;
}

// GLOBAL functions definition

// 1.

void testDestructor()
{
    Critter toDestroy("Rover", 3);
    toDestroy.Greet();
}

// 2.

void testCopyConstructor(Critter aCopy)
{
    aCopy.Greet();
}

// 3.

void testAssignmentOp()
{
    Critter crit1("crit1", 7);
    Critter crit2("crit2", 9);
    crit1 = crit2;

    crit1.Greet();
    crit2.Greet();

    cout << endl;

    Critter crit3("crit", 11);
    crit3 = crit3;
    crit3.Greet();
}

// end of code
