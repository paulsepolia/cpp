// Demonstrates returning a reference

#include <iostream>
#include <string>
#include <vector>

using namespace std;

// function declaration
// returns a reference to a string

string& refToElement(vector<string>& vec, int i);

// main function

int main()
{
    vector<string> vv;

    vv.push_back("sword");
    vv.push_back("armor");
    vv.push_back("shield");

    // displays string that the returned reference refers to

    cout << " Sending the returned reference to cout: " << endl;
    cout << refToElement(vv, 0) << endl << endl;

    // assigns one reference to another -- inexpensive assignment

    cout << " Assigning the returned reference to another reference. " << endl;

    string& rStr = refToElement(vv, 1);

    cout << " Sending the new reference to cout: " << endl;

    cout << rStr << endl << endl;

    // copies a string object -- EXPENSIVE assignment

    cout << " Assigning the returned reference to a string object. " << endl;

    string str = refToElement(vv, 2);

    cout << " Sending the new string object to cout: " << endl;

    cout << str << endl << endl;

    // altering the string object through a returned reference

    cout << " Altering an object through a returned reference. " << endl;
    rStr = "Healing Potion";

    cout << " Sending the altered object to cout: " << endl;
    cout << vv[1] << endl;

    return 0;
}

// function definition

string& refToElement(vector<string>& vec, int i)
{
    return vec[i];
}

// end of code
