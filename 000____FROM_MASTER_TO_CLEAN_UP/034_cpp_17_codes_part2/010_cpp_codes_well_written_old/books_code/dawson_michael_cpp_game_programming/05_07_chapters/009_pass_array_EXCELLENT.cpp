// Creating and passing an array
// Demonstrates all possible ways in C++ to pass an array
// to a function and back

#include <iostream>
#include <cstdlib>
#include <cmath>

using namespace std;

// functions declaration

double* build_array(double* const array, const long NUM_ELEM);
double* modify_array(double* const array, const long NUM_ELEM);
double* copy_array(double array[], const long NUM_ELEM);

// main function

int main()
{
    // number of array elements

    const long NUM_ELEM = 5*10E6;
    const long TRIALS = 10E2;
    long i;

    for (i = 0; i < TRIALS; i++) {
        // declaration of the array

        double* const array = new double[NUM_ELEM];

        // build the array

        cout << endl;
        cout << "  1. Build the array. " << endl;
        build_array(array, NUM_ELEM);

        // some output

        cout << endl;
        cout << "  2. Some output. " << endl;

        cout << endl;
        cout << " array[0] = " << array[0] << endl;
        cout << " array[1] = " << array[1] << endl;

        // modify the array

        cout << endl;
        cout << "  3. Modify the array. " << endl;
        modify_array(array, NUM_ELEM);

        // some output

        cout << endl;
        cout << "  4. Some output. " << endl;

        cout << endl;
        cout << " array[0] = " << array[0] << endl;
        cout << " array[1] = " << array[1] << endl;

        // copy the array

        cout << endl;
        cout << "  5. copy the array. " << endl;
        copy_array(array, NUM_ELEM);

        // some output

        cout << endl;
        cout << "  6. Some output. " << endl;

        cout << endl;
        cout << " array[0] = " << array[0] << endl;
        cout << " array[1] = " << array[1] << endl;

        // deallocating the RAM space.

        cout << endl;
        cout << "  7. Delete the RAM space occupied by the array. " << endl;

        // the counter.

        cout << endl;
        cout << "  ---------------------------------------------------------> " << i << endl;

        delete [] array;
    }

    // exiting

    return 0;
}

// functions definition

// 1.

double* build_array(double* const array, const long NUM_ELEM)
{
    long i;
    const double modA = 10.0;
    double tmpA;

    for (i = 0; i < NUM_ELEM; i++) {
        tmpA = static_cast<double>(rand());
        array[i] = modA - fmod(tmpA, modA);
    }

    return (&array[0]);
}

// 2.

double* modify_array(double* const array, const long NUM_ELEM)
{
    long i;
    const double modB = 11.0;
    double tmpB;

    for (i = 0; i < NUM_ELEM; i++) {
        tmpB = static_cast<double>(rand());
        array[i] = modB - fmod(tmpB, modB);
    }

    return (&array[0]);
}

// 3.

double* copy_array(double array[], const long NUM_ELEM)
{
    long i;
    const double modC = 12.0;
    double tmpC;

    for (i = 0; i < NUM_ELEM; i++) {
        tmpC = static_cast<double>(rand());
        array[i] = modC - fmod(tmpC, modC);
    }

    return (&array[0]);
}

// end of code
