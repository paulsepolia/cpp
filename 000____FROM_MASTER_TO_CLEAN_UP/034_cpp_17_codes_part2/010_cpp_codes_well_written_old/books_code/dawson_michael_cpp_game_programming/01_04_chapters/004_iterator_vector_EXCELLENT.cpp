// Demostration of iterators

#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main()
{
    const long SIZE_VEC = 7*10E3;
    long i;

    vector<string> v;
    v.push_back("1");
    v.push_back("2");
    v.push_back("3");

    vector<string>::iterator my_iter; // mporo na allaxo engenei to v
    vector<string>::const_iterator iter; // den mporo na allaxo to v

    cout << endl;
    cout << "  1. The initial items of the vector. " << endl;
    for(iter = v.begin(); iter != v.end(); ++iter) {
        cout << *iter << endl;
    }

    my_iter = v.begin();
    *my_iter = "battle axe";

    cout << endl;
    cout << "  2. Replacing the 1st item of the vector. " << endl;
    for(iter = v.begin(); iter != v.end(); ++iter) {
        cout << *iter << endl;
    }

    cout << endl;
    cout << "  3. A report on the size of the vector. " << endl;
    cout << " (*my_iter).size()   = " << (*my_iter).size() << endl;
    cout << "   my_iter -> size() = " << my_iter -> size() << endl;

    cout << endl;
    cout << "  4. Using the insert() function to add extra elements at the beginning. " << endl;
    cout << "     VERY SLOW PROCESS " << endl;
    for (i = 0; i < SIZE_VEC; i++) {
        v.insert(v.begin(), "DDDDDDDD");
        if (i%10000 == 0) cout << " i = " << i << endl;
    }

    cout << endl;
    cout << "  5. A report on the size of the vector. " << endl;
    cout << " v.size() = " << v.size() << endl;

    cout << endl;
    cout << "  6. Using clear function to clean the vector. " << endl;
    v.clear();
    cout << " v.size() = " << v.size() << endl;

    cout << endl;
    cout << "  7. Using the insert() function to add extra elements at the end. " << endl;
    cout << "     FAST PROCESS " << endl;
    for (i = 0; i < SIZE_VEC; i++) {
        v.insert(v.end(), "DDDDDDDD");
        if (i%10000 == 0) cout << " i = " << i << endl;
    }

    cout << endl;
    cout << "  8. Using clear function to clean the vector. " << endl;
    v.clear();
    cout << " v.size() = " << v.size() << endl;

    cout << endl;
    cout << "  9. Using the push_back() function to add extra elements at the end. " << endl;
    cout << "     FAST PROCESS " << endl;
    for (i = 0; i < SIZE_VEC; i++) {
        v.push_back("DDDDDDDD");
        if (i%10000 == 0) cout << " i = " << i << endl;
    }


}

