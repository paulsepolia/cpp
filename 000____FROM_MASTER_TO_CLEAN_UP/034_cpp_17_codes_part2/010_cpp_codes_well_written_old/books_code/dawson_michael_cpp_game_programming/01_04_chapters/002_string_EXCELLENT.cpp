// String tester
// Demonstrating string objects

#include <iostream>
#include <string>

using namespace std;

int main()
{

    const long int SIZE_STR = 5*10E7;
    const int TRIALS = 10E4;
    long i;

    for (i = 0; i < TRIALS; i++) {
        cout << endl;
        cout << " -----------------------------> " << i << endl;

        cout << endl;
        cout << "  1. Building the strings. " << endl;
        string t1(SIZE_STR, '4') ;
        string t2(SIZE_STR, '5') ;
        string t3(SIZE_STR, '6') ;

        cout << endl;
        cout << "  2. Some results. " << endl;
        cout << t1[1] << endl;
        cout << t2[1] << endl;
        cout << t3[1] << endl;
        cout << t1.size() << endl;
        cout << t2.size() << endl;
        cout << t3.size() << endl;

        cout << endl;
        cout << "  3. Searching the strings. " << endl;
        cout << (t1.find("10") == string::npos) << endl;
        cout << (t2.find("10") == string::npos) << endl;
        cout << (t3.find("10") == string::npos) << endl;

        cout << endl;
        cout << "   4. Erasing the strings. " << endl;
        t1.erase();
        t2.erase();
        t3.erase();

        cout << t1.size() << endl;
        cout << t2.size() << endl;
        cout << t3.size() << endl;

        cout << t1.empty() << endl;
        cout << t2.empty() << endl;
        cout << t3.empty() << endl;
    }

    return 0;
}

// end of code


