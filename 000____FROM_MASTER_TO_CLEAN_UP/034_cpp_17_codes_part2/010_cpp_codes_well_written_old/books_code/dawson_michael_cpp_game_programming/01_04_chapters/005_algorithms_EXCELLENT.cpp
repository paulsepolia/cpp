// Demonstrates algorithms

#include <iostream>
#include <vector>
#include <algorithm>
#include <ctime>
#include <cstdlib>

using namespace std;

int main()
{
    vector<double>::const_iterator iter;
    vector<double> scores;
    double score;
    long j;
    const long DIM = 10;
    const long SIZE_VEC = 2*10E6;
    const long SIZE_EX  = 2*10E6;

    cout << endl;
    cout << " Creating a list of scores. " << endl;

    scores.push_back(1500);
    scores.push_back(3500);
    scores.push_back(7500);

    j = 0;
    cout << endl;
    cout << " High scores: " << endl;
    for (iter = scores.begin(); iter != scores.end(); ++iter) {
        cout << *iter << " ";
        j++;
        if (j > DIM ) break;
    }

    cout << endl;
    cout << " Building a huge set of scores. " << endl;
    for (j = 0; j < SIZE_VEC; j++) {
        scores.push_back(static_cast<double>(j));
    }

    cout << endl;
    cout << " Finding a score. " << endl;
    cout << " Enter a score to find: ";
    cin >> score;
    iter = find(scores.begin(), scores.end(), score);
    cout << endl;
    if (iter != scores.end()) {
        cout << " Score found. " << endl;
    } else {
        cout << " Score not found. " << endl;
    }

    cout << endl;
    cout << " Randomizing scores.";
    srand(static_cast<unsigned int>(time(0)));
    random_shuffle(scores.begin(), scores.end());

    j = 0;
    cout << endl;
    cout << " High scores: " << endl;
    for (iter = scores.begin(); iter != scores.end(); ++iter) {
        cout << *iter << " ";
        j++;
        if (j > DIM ) break;
    }

    cout << endl;
    cout << " Sorting scores. " << endl;
    sort(scores.begin(), scores.end());

    j = 0;
    cout << endl;
    cout << " High scores: " << endl;
    for (iter = scores.begin(); iter != scores.end(); ++iter) {
        cout << *iter << " ";
        j++;
        if (j > DIM ) break;
    }

    cout << endl;
    cout << " Vector capacity versus size. " << endl;
    cout << " Capacity --> " << scores.capacity() << endl;
    cout << " Size     --> " << scores.size() << endl;

    cout << endl;
    cout << " Reserving extra memory. " << endl;
    scores.reserve(SIZE_EX);
    cout << endl;
    cout << " Vector capacity versus size. " << endl;
    cout << " Capacity --> " << scores.capacity() << endl;
    cout << " Size     --> " << scores.size() << endl;
    cout << endl;
    return 0;
}

// end of code
