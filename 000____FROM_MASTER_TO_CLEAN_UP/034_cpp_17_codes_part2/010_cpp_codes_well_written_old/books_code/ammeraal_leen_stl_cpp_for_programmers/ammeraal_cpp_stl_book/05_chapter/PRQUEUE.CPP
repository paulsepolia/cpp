// prqueue.cpp: A priority queue; a demonstration of
//   the member functions push, pop, empty and top.
// From: Ammeraal, L. (1997) STL for C++ Programmers,
//       Chichester: John Wiley.

#include <iostream>
#include <vector>
#include <functional>
#include <queue>

using namespace std;

int main()
{  priority_queue <int, vector<int>, less<int> > P;
   int x;
   P.push(123); P.push(51); P.push(1000); P.push(17);
   while (!P.empty())
   {  x = P.top();
      cout << "Retrieved element: " << x << endl;
      P.pop();
   }
   return 0;
}
