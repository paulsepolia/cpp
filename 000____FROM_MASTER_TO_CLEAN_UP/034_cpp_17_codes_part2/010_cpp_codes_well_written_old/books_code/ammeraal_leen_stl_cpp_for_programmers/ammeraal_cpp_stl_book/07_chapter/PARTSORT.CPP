// partsort.cpp: Partial sorting.
// From: Ammeraal, L. (1997) STL for C++ Programmers,
//       Chichester: John Wiley.
#include <iostream>
#include <string>
#include <algorithm>
using namespace std;
int main()
{  int a[10] = {9, 8, 7, 6, 5, 4, 3, 2, 1, 0};
   partial_sort(a, a+4, a+10);
   for (int k=0; k<10; k++)
      cout << a[k] << (k == 3 ? "   " : " ");
   cout << endl;
   return 0;
}
