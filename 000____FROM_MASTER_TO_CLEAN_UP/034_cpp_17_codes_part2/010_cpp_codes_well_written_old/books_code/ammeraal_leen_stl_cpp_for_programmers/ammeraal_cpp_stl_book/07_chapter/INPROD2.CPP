// inprod2.cpp: Product of powers computed by inner 
//              product.
// From: Ammeraal, L. (1997) STL for C++ Programmers,
//       Chichester: John Wiley.

#include <iostream>
#include <numeric>
#include <functional>
using namespace std;

double power(int x, int n)
{  double y = 1;
   for (int k=0; k<n; k++) y *= x;
   return y;  // x raised to the power n
}

int main()
{  int a[3] = {2, 3, 5}, b[3] = {4, 1, 2}, product=1;
   product = inner_product(a, a+3, b, product,
#if (defined(__BORLANDC__) && __BORLANDC__ <= 0x520)
      times<double>(), power);
#else
      multiplies<double>(), power); 
#endif
   cout << product << endl; 
   // 1 * power(2, 4) * power(3, 1) * power(5, 2) =
   // 1 * 16 * 3 * 25 = 1200
   return 0;
}
