// merge1.cpp: Merging two ranges that are in 
//             descending order.
// From: Ammeraal, L. (1997) STL for C++ Programmers,
//       Chichester: John Wiley.
#include <iostream>
#include <algorithm>
#include <functional>
using namespace std;

int main()
{  int a[5] = {30, 28, 15, 13, 11},
       b[4] = {25, 24, 15, 12},
       c[9];
   merge(a, a+5, b, b+4, c, greater<int>()); 
   copy(c, c+9, 
#if defined(__BORLANDC__) && __BORLANDC__ == 0x530   
      ostream_iterator<int, char, char_traits<char> >(cout, " "));
#else
      ostream_iterator<int>(cout, " "));
#endif
   cout << endl;
   return 0;
}

