// largedem.cpp: Type large in action.
//    To be linked with large.cpp.
// From: Ammeraal, L. (1997) STL for C++ Programmers,
//       Chichester: John Wiley.
#include "large.h"

int main()
{  large a = -10000, b = 10000U, c = 2000000L,
      d = "100000000000000000000", // 20 zeros
      x, y, z, u;
   x = (a * b * b + 1) * c;
   x -= c;       // x = a * b * b * c
   x /= a * b;   // x = b * c
   y = large("1234567890123") % large("1234567890000");
   if (x == b * c && y == large(123))
      cout << "Arithmetic OK" << endl;

   z = power(d, 100);  // d raised to the power 100
                       // = 10 raised to the power 2000
   u = sqrt(z);        // 10 raised to the power 1000
   if (u == power(large(10), 1000))
      cout << "u = '10 raised to the power 1000'" 
           << endl;

   if (u < power(large(11), 1000) && 
       u > power(large(9), 1000))
      cout << "Comparisons OK" << endl;

   vector<char> s;
   u.num2char(s);
   cout << "First character in output of u: " 
        << *(s.end() - 1) << endl;
   cout << "u consists of " << s.size() 
        << " decimal digits." << endl;

   z = d << 100; // z = d * (2 to the power 100)
   if (z == d * power(large(2), 100) && (z >> 100) == d)
      cout << "Shift operations OK" << endl;
   cout << -d << endl;

   cout << "Enter a large number x: ";
   cin >> x;
   cout << "2 * x = " << 2 * x << endl;
   a = "123456789123456789"; b = "999888777666";
   large q, r;
   a.divide(b, q, r, true);
   if (q != a/b || r != a%b)
      cout << "Function 'divide' incorrect." << endl;
   return 0;
}
