// map2a.cpp: Second version of telephone directory,
//            based on type 'string'.
// From: Ammeraal, L. (1997) STL for C++ Programmers,
//       Chichester: John Wiley.
#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <map>
using namespace std;
                                                 // !
typedef map<string, long, less<string> > directype;

// Read all characters until '\n' and store them in str,
// except for '\n', which is read but not stored.
void getaline(istream &is, string &str)  
{  char ch;
   str = "";                                     // !
   for (;;)
   {  is.get(ch);
      if (is.fail() || ch == '\n') break;
      str += ch;
   }
}

void ReadInput(directype &D)
{  ifstream ifstr("phone.txt");
   long nr;
   string str;                                   // !
   if (ifstr)
   {  cout << "Entries read from file phone.txt:\n";
      for (;;)
      {  ifstr >> nr;
         ifstr.get(); // skip space
         getaline(ifstr, str);                   // !
         if (!ifstr) break;
         cout << setw(9) << nr << " " << str << endl;
         D[str] = nr;                            // !
      }
   }
   ifstr.close();
}

void ShowCommands()
{  cout << 
      "Commands: ?name       : find phone number,\n"
      "          /name       : delete\n"
      "          !number name: insert (or update)\n"
      "          *           : list whole phonebook\n" 
      "          =           : save in file\n"
      "          #           : exit" << endl;
}

void ProcessCommands(directype &D)
{  ofstream ofstr;
   long nr;
   string str;                                   // !
   char ch;
   directype::iterator i;
   for (;;)
   {  cin >> ch; // skip any white-space and read ch.
      switch (ch){
      case '?': case '/': // find or delete:
         getaline(cin, str); 
         i = D.find(str);                        // !
         if (i == D.end()) 
            cout << "Not found.\n"; 
         else             // Key found
         if (ch == '?')   // 'Find' command
            cout << "Number: " << (*i).second << endl;
         else             // 'Delete' command
            D.erase(i);                          // !
         break;

      case '!':           // insert (or update)
         cin >> nr; 
         if (cin.fail())
         {  cout << "Usage: !number name\n"; 
            cin.clear();
            getaline(cin, str);                  // !
            break;
         }
         cin.get();       // skip space;
         getaline(cin, str);                    
         D[str] = nr;                            // !
         break;
      case '*':
         for (i = D.begin(); i != D.end(); i++)
            cout << setw(9) << (*i).second << " " 
                 << (*i).first << endl;
         break;
      case '=':
         ofstr.open("phone.txt");
         if (ofstr)
         {  for (i = D.begin(); i != D.end(); i++)
              ofstr << setw(9) << (*i).second << " " 
                    << (*i).first << endl;
            ofstr.close();
         }  else cout << "Cannot open output file.\n";
         break;
      case '#': break;
      default:
         cout << "Use: * (list), ? (find), = (save), "
            "/ (delete), ! (insert), or # (exit).\n";
         getaline(cin, str); break;              // !
      }
      if (ch == '#') break;      
   }
}

int main()
{  directype D;
   ReadInput(D);
   ShowCommands();
   ProcessCommands(D);      
   return 0;                                     // !
}
