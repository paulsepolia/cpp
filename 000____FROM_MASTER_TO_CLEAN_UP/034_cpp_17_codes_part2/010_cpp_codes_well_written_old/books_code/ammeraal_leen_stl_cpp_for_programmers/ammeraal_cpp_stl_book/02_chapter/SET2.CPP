// set.cpp: Two identical sets, created differently.
// From: Ammeraal, L. (1997) STL for C++ Programmers,
//       Chichester: John Wiley.
#include <iostream>
#include <set>
using namespace std;

int main()
{  set<int, less<int> > S, T;
   S.insert(10); S.insert(20); S.insert(30); 
   S.insert(10);
   T.insert(20); T.insert(30); T.insert(10);
   if (S == T) cout << "Equal sets, containing:\n";
   for (set<int, less<int> >::iterator i = T.begin(); 
      i != T.end(); i++)
         cout << *i << " ";
   cout << endl;
   return 0;
}
