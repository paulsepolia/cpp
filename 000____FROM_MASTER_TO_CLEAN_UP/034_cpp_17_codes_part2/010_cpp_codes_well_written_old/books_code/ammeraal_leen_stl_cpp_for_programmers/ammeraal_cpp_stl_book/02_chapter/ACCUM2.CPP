// accum2.cpp: Forming a product.
// From: Ammeraal, L. (1997) STL for C++ Programmers,
//       Chichester: John Wiley.

#include <iostream>
#include <numeric>
#include <algorithm>
#include <functional>

using namespace std;

int main()
{  const int N = 4;
   int a[N] = {2, 10, 5, 3}, prod = 1;
   prod = accumulate(a, a+N, prod, multiplies<int>());
   // ('muliplies' has replaced 'times')
   cout << "Product of all elements: " << prod << endl; 
   return 0;
}
