// boolint.cpp: Types bool and int are not identical.
// From: Ammeraal, L. (1997) STL for C++ Programmers,
//       Chichester: John Wiley.
#include <iostream.h>

int main()
{  cout << "sizeof(bool) = " << sizeof(bool) << endl;
   cout << "sizeof(int) = " << sizeof(int) << endl;
   bool B[100];
   cout << "With B defined as bool B[100], we have\n";
   cout << "sizeof(B) = " << sizeof(B) << endl;
   return 0;
}
