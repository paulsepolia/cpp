// replace.cpp: Replacing sequence elements.
// From: Ammeraal, L. (1997) STL for C++ Programmers,
//       Chichester: John Wiley.
#include <iostream>
#include <string>
#include <algorithm>
using namespace std;

int main()
{  char str[] = "abcabcabc";
   int n = strlen(str);
   replace(str, str+n, 'b', 'q');
   cout << str << endl;
   return 0;
}
