// A template function

#include <iostream>

using namespace std;

// 1. template function definition

template <class TT1>
TT1 f(TT1 x)
{
    TT1 x2;
    x2 = 2 * x;

    return (x2 + (x * x +1) / x2);
}

template <class TT2>
TT2 f2(TT2 x)
{
    TT2 x2;
    x2 = 2 * x;

    return (x2 + (x * x +1) / x2 + 10);
}


// 2. main function

int main()
{
    cout << f(5.0) << endl;
    cout << f(5) << endl;
    cout << f2(5.0) << endl;
    cout << f2(5) << endl;


    return 0;
}

// end of code

