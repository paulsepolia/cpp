// Copy algorithm
// Copying a vector to a list.
// Overwrite mode.
#include <iostream>
#include <vector>
#include <list>

using namespace std;

int main()
{
    const double SIZE_COMM = 10E8;
    vector<double> v;
    long int i;

    cout << endl;
    cout << "  1. Building the vector. " << endl;
    for (i = 0; i < SIZE_COMM; i++) {
        v.push_back(i+100);
    }

    cout << endl;
    cout << "  2. Declaration of the list to have the same size as the vector. " << endl;
    list<double> L(SIZE_COMM);

    cout << endl;
    cout << "  3. Copying the vector to the list. " << endl;
    copy(v.begin(), v.end(), L.begin()); // overwrite mode

    list<double>::iterator Li;

    Li = L.begin();
    cout << *Li << endl;
    Li = L.end();
    cout << *--Li << endl;

    return 0;
}

// end of code
