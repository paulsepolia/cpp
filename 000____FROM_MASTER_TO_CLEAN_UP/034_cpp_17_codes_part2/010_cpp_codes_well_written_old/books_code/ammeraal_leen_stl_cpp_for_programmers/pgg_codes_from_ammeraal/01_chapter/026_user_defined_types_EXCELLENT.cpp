// Merging records, with names as keys

#include <iostream>
#include <string>
#include <algorithm>
#include <cstring>

using namespace std;

// struct definition

struct entry {
    long int nr;
    char name[30];
};

// booll operator< definition

bool operator<(const entry &a, const entry &b)
{
    return strcmp(a.name, b.name) < 0;
}

// main function

int main()
{
    entry a[3] = {{10, "Betty"}, {11, "James"}, {80, "Aim"}};
    entry b[2] = {{16, "Fred"},  {20, "William"}};
    entry c[5];
    entry *p;

    merge(a, a+3, b, b+2, c);

    for (p = c; p != c+5; p++) {
        cout << p -> nr << " " << p -> name << endl;
    }

    cout << endl;

    return 0;
}

// end of code
