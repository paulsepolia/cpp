// insertions and deletions in a list
#include <iostream>
#include <cstdlib>
#include <list>

using namespace std;

// function definition

void showList(const char *str, const list<double> &L)
{
    list<double>::const_iterator i;
    const int DIMEN = 10;
    int j;

    cout << str << endl << " ";

    j = 0;
    for (i = L.begin(); i != L.end(); ++i) {
        cout << *i << " ";
        j++;
        if ( j >= DIMEN ) break;
    }

    cout << endl;
}

// main function

int main()
{
    list<double> L;
    double x;
    list<double>:: iterator i;
    const double DIMEN1 = 1*10E7;
    long int j;
    int sentinel;

    cout << "Enter an integer to start the process: ";
    cin >> sentinel;

    cout << endl;
    cout << " 1. creating the list --> push_back " << endl;
    for (j = 0; j < DIMEN1; j++) {
        L.push_back(rand()/10E8);
    }
    showList("Initial list:", L);

    cout << endl;
    cout << " 2. insertion at the beginning --> push_front " << endl;
    for (j = 0; j < DIMEN1; j++) {
        L.push_front(123);
    }
    showList("After inserting 123 at the beginning:", L);

    i = L.begin();
    cout << endl;
    cout << " 3. insertion at the second potision --> insert" << endl;
    for (j = 0; j < DIMEN1; j++) {
        L.insert(++i, 456);
    }
    showList("After inserting 456 at the second position:", L);

    i = L.end();
    cout << endl;
    cout << " 4. insertion just before the end --> insert" << endl;
    for (j = 0; j < DIMEN1; j++) {
        L.insert(--i, 999);
    }
    showList("After inserting 999 just before the end:", L);

    i = L.begin();
    x = *i;
    cout << endl;
    cout << " 5. deletion at the begining --> pop_front " << endl;
    for(j = 0; j < DIMEN1; j++) {
        L.pop_front();
    }
    cout << "Deleted at the beginning: " << x << endl;
    showList("After this deletion:", L);

    i = L.end();
    x = *(--i);
    cout << endl;
    for ( j = 0; j < DIMEN1; j++) {
        L.pop_back();
    }
    cout << "Deleted at the end:" << x << endl;
    showList("After this deletion:", L);

    for (j = 0; j < DIMEN1; j++) {
        i = L.begin();
        x = *(++i);
//    cout << "To be deleted: " << x << endl;
        L.erase(i);
    }
    cout << endl;
    showList("After this deletion (of second element):", L);

    cin >> sentinel;

    return 0;
}

// end of code
