// Sorting an array
#include <iostream>
#include <algorithm>
#include <cstdlib>
using namespace std;

int main()
{
    const long int SIZE_ARRAY = 10E8;
    const int DIMEN = 10;
    double *a;
    double *p;
    a = new double [SIZE_ARRAY];
    long int i;
    int sentinel;


    cout << " Enter a integer to start the process: ";
    cin >> sentinel;

    cout << endl;
    cout << " 1. Creating the array. " << endl;
    for (i = 0; i < SIZE_ARRAY; i++) {
        a[i] = rand() / 10E2;
    }

    cout << endl;
    cout << " 2. Sorting the array. " << endl;
    sort(a, a + SIZE_ARRAY);

    cout << endl;
    cout << " 3. After sorting the array: " << endl;
    i = 0;
    for (p=a; p != a+SIZE_ARRAY; p++) {
        cout << *p << " ";
        i++;
        if ( i > DIMEN ) break;
    }

    cout << endl;
    return 0;
}

// end of code
