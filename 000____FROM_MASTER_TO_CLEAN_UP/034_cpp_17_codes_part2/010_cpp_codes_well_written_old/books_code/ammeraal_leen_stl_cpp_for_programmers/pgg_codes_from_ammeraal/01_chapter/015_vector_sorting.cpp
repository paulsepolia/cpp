// sorting a vector
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int main()
{
    vector<int> v;
    vector<int>::iterator i;
    int x;

    cout << " Enter positive integers, followed by 0:\n";
    while(cin >> x, x != 0) {
        v.push_back(x);
    }

    sort(v.begin(), v.end());

    cout << " After sorting: \n";
    for (i=v.begin(); i != v.end(); ++i) {
        cout << *i << " ";
    }

    cout << endl;

    return 0;
}

// end of code


