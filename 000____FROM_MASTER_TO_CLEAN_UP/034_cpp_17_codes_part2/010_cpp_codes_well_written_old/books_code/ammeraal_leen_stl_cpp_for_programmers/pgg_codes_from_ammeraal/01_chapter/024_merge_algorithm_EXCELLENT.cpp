// merging a vector and aan array into a list
#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
using namespace std;

int main()
{
    vector<int> a(5);
    a[0] = 4;
    a[1] = 3;
    a[2] = -2;
    a[3] = 200;
    a[4] = 25;

    int b[6] = {9, 1, 3, 28, 30, 33};

    list<int> c; // list is initially empty

    merge(a.begin(), a.end(), b, b+6, inserter(c, c.begin()));

    list<int>::iterator i;
    for (i = c.begin(); i != c.end(); ++i) {
        cout << *i << " ";
    }

    cout << endl;
    return 0;

}

