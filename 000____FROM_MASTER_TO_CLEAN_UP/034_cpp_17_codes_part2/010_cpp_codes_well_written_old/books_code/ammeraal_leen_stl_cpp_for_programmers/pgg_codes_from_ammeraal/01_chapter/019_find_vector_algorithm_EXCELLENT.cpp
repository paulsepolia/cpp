// finding a given value in a vector
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
    const long int SIZE_VEC = 10E8;
    vector<long int> v(SIZE_VEC);
    vector<long int>:: iterator i;
    long int j;
    long int x;
    int sentinel;
    const long int offsetVal = 100;

    cout << endl;
    cout << "  0. Enter an integer to start the process: ";
    cin >> sentinel;

    cout << endl;
    cout << "  1. Building the vector. " << endl;
    for (j = 0; j < SIZE_VEC; j++) {
        v[j] = j - offsetVal;
    }

    cout << endl;
    cout << "  2. Enter the value to be searched for: ";
    cin >> x;

    cout << endl;
    cout << "  3. Searching for the value. " << endl;
    i = find(v.begin(), v.end(), x);

    cout << endl;
    cout << "  4. Results of searching. " << endl;
    if ( i == v.end() ) {
        cout << "  Not found\n";
    } else {
        cout << "  Found";
        if ( i == v.begin()) {
            cout << " as the first element";
        } else {
            cout << " as the " << *i << " element";
        }
    }

    cout << endl;
    return 0;
}

// end of code
