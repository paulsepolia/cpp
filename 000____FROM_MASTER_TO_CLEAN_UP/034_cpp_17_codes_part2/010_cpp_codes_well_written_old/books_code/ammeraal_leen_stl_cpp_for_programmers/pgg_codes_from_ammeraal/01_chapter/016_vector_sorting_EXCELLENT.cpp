// sorting a vector
#include <iostream>
#include <cstdlib>
#include <vector>
#include <algorithm>
using namespace std;

int main()
{
    vector<double> v;
    vector<double>::iterator i;
    long int j;
    long int k;
    const long int DIMEN1 = 10E8;
    const long int DIMEN2 = 10;
    double x;
    int sentinel;

    cout << " Enter positive integers, followed by 0:\n";
    cin >> sentinel;

    cout << endl;
    cout << " 1. building the vector --> push_back. " << endl;
    for(j = 0; j < DIMEN1; j++) {
        x = rand() / 10E2;
        v.push_back(x);
    }

    cout << endl;
    cout << " 2. Sorting the vector --> sort. " << endl;
    sort(v.begin(), v.end());

    cout << " After sorting: \n";
    j = 0;
    for (i = v.begin(); i != v.end(); ++i) {
        cout << *i << " ";
        j++;
        if ( j >= DIMEN2 ) break;
    }

    cout << endl;

    return 0;
}

// end of code


