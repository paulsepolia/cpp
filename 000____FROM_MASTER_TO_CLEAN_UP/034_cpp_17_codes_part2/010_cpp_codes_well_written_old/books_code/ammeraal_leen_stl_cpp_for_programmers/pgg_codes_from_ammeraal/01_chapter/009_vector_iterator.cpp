// reading and writing a variable number of
// nonzero integers (followed in the input by 0).

#include <iostream>
#include <vector>
using namespace std;

int main()
{
    vector<int> v;
    vector<int>::iterator i;
    vector<int>::reverse_iterator ri;
    int x;

    cout << "Enter positive integers, followed by 0:\n";

    cin >> x;
    v.push_back(x);
    while( x != 0 ) {
        cin >> x;
        v.push_back(x);
    }

    for (i = v.begin(); i != v.end(); ++i) {
        cout << *i << " ";
    }

    cout << endl;

    for (ri = v.rbegin(); ri != v.rend(); ++ri) {
        cout << *ri << " ";
    }

    cout << endl;

    return 0;
}

// end of code
