// finding a given value in a vector
#include <iostream>
#include <vector>
#include <list>
#include <deque>
#include <algorithm>

using namespace std;

int main()
{
    const long int SIZE_COMM = 10E8;

    vector<long int> v;
    vector<long int>:: iterator vi;

    list<long int> L;
    list<long int>:: iterator Li;

    deque<long int> w;
    deque<long int>:: iterator wi;

    long int *a;
    a = new long int [SIZE_COMM];

    long int j;
    long int x;
    int sentinel;
    const long int offsetVal = 100;

    cout << endl;
    cout << "  0. Enter the integer to be searced: ";
    cin >> x;

    cout << endl;
    cout << "  1. Building the vector. " << endl;
    for (j = 0; j < SIZE_COMM; j++) {
        v.push_back(j + offsetVal);
    }

    cout << endl;
    cout << "  2. Building the list. " << endl;
    for (j = 0; j < SIZE_COMM; j++) {
        L.push_back(j + offsetVal);
    }

    cout << endl;
    cout << "  3. Building the deque. " << endl;
    for (j = 0; j < SIZE_COMM; j++) {
        w.push_back(j + offsetVal);
    }

    cout << endl;
    cout << "  4. Building the array. " << endl;
    for (j = 0; j < SIZE_COMM; j++) {
        a[j] = j + offsetVal;
    }

    cout << endl;
    cout << "  6. Vector case: Searching for the value. " << endl;
    vi = find(v.begin(), v.end(), x);

    cout << endl;
    cout << "  7. Results of searching. " << endl;
    if ( vi == v.end() ) {
        cout << "     Not found\n";
    } else {
        cout << "     Found";
        if ( vi == v.begin()) {
            cout << " as the first element";
        } else {
            cout << " as the " << *vi << " element";
        }
    }

    cout << endl;
    cout << "  8. List case: Searching for the value. " << endl;
    Li = find(L.begin(), L.end(), x);

    cout << endl;
    cout << "  9. Results of searching. " << endl;
    if ( Li == L.end() ) {
        cout << "     Not found\n";
    } else {
        cout << "     Found";
        if ( Li == L.begin()) {
            cout << " as the first element";
        } else {
            cout << " as the " << *Li << " element";
        }
    }

    cout << endl;
    cout << " 10. Deque case: Searching for the value. " << endl;
    wi = find(w.begin(), w.end(), x);

    cout << endl;
    cout << " 11. Results of searching. " << endl;
    if ( wi == w.end() ) {
        cout << "     Not found\n";
    } else {
        cout << "     Found";
        if ( wi == w.begin()) {
            cout << " as the first element";
        } else {
            cout << " as the " << *wi << " element";
        }
    }

    cout << endl;
    cout << " 12. Array case: Searching for the value. " << endl;
    long int *p = find(a, a + SIZE_COMM, x);

    cout << endl;
    cout << " 13. Results of searching. " << endl;
    if ( p == a + SIZE_COMM ) {
        cout << "     Not found\n";
    } else {
        cout << "     Found";
        if ( p == a ) {
            cout << " as the first element";
        } else {
            cout << " as the " << *p << " element";
        }
    }

    cout << endl;
    cout << "  X. Enter an integer to exit: ";
    cin >> sentinel;

    return 0;
}

// end of code
