// initializing array, vector, deque, list

#include <iostream>
#include <cstdlib>
#include <vector>
#include <deque>
#include <list>
#include <algorithm>

using namespace std;

int main()
{
    const long int SIZE_COMM = 10E6;
    double *a;
    a = new double [SIZE_COMM];
    int sentinel;
    long int i;

    cout << endl;
    cout << " Enter an integer to start the process: ";
    cin >> sentinel;

    cout << endl;
    cout << "  1. Building the array." << endl;
    for (i = 0; i < SIZE_COMM; i++) {
        a[i] = rand() / 10E2;
    }

    cout << endl;
    cout << "  2. Initializing a vector identical to array. " << endl;
    vector<double> v(a, a + SIZE_COMM);

    cout << endl;
    cout << "  3. Initializing a deque identical to array. " << endl;
    deque<double> w(a, a + SIZE_COMM);

    cout << endl;
    cout << "  4. Initializing a list identical to array. " << endl;
    list<double> x(a, a + SIZE_COMM);

    cout << endl;
    cout << "  5. Sorting the array. " << endl;
    sort(a, a+SIZE_COMM);
    cout << a[0] << endl;
    cout << a[SIZE_COMM-1] << endl;

    cout << endl;
    cout << "  6. Sorting the vector. " << endl;
    sort(v.begin(), v.end());
    cout << *v.begin() << endl;
    cout << *(v.end()-1) << endl;

    cout << endl;
    cout << "  7. Sorting the deque. " << endl;
    sort(w.begin(), w.end());
    cout << *w.begin() << endl;
    cout << *(w.end()-1) << endl;

    cout << endl;
    cout << "  8. Erasing the vector. " << endl;
    v.erase(v.begin(), v.end());

    cout << endl;
    cout << "  9. Erasing the list. " << endl;
    x.erase(x.begin(), x.end());

    cout << endl;
    cout << " 10. Erasing the deque. " << endl;
    w.erase(w.begin(), w.end());

    cout << endl;
    cout << " 11. Allocating space for the vector. " << endl;
    vector<double> v2(5*SIZE_COMM);

    cout << endl;
    cout << " 12. Allocating space for the list. " << endl;
    list<double> x2(5*SIZE_COMM);

    cout << endl;
    cout << " 13. Allocating space for the deque. " << endl;
    deque<double> w2(5*SIZE_COMM);

    cout << endl;
    cout << " X. Enter an integer to exit: ";
    cin >> sentinel;

    return 0;
}
