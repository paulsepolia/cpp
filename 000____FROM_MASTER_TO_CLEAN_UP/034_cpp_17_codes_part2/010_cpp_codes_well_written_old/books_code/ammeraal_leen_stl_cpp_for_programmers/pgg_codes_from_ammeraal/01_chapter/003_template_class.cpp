// a template class

#include <iostream>

using namespace std;

template <class TT1>
class Pair {
public:
    Pair(TT1 x1, TT1 y1): x(x1), y(y1) {}
    void showQ();
private:
    TT1 x, y;
};

template <class TT2>
void Pair<TT2>::showQ()
{
    cout << x/y << endl;
}

int main()
{
    Pair<double> a(37.0, 5.0);
    Pair<int> u(37, 5);
    a.showQ();
    u.showQ();

    return 0;
}

