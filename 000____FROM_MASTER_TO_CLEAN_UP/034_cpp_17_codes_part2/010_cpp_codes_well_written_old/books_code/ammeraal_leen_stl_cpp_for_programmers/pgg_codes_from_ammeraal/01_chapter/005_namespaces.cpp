// The namespace concept

#include <iostream>
using namespace std;

// declaration of the namespaces

namespace A {
int i = 10;
}

namespace B {
int i = 20;
}

// definition of the functions

void fA()
{
    using namespace A;
    cout << "In fA: " << A::i << " " << B::i << " " << i << endl;
}

void fB()
{
    using namespace B;
    cout << "In fB: " << A::i << " " << B::i << " " << i << endl;
}

// the main function

int main()
{
    fA();
    fB();
    cout << "In main: " << A::i << " " << B::i << endl;
    using A::i;
    cout << i << endl;
    return 0;
}

// end of code
