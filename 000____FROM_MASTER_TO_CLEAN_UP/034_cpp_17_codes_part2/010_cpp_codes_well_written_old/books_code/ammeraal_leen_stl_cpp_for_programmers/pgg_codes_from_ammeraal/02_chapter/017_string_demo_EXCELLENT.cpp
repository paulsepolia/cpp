// strings as elements of list L and array a.

#include <iostream>
#include <string>
#include <list>
#include <algorithm>
#include <iterator>

using namespace std;

int main()
{
    string s("One");
    string t("Two");
    string u("Three");

    int k;

    list<string> L;
    list<string>::iterator Li;

    L.push_back(s);
    L.push_back(t);
    L.push_back(u);

    string a[3];
    copy(L.begin(), L.end(), a);

    // 1st way of output.
    for (k = 0; k < 3; k++) cout << a[k] << endl;

    // 2nd way of output.
    copy(L.begin(), L.end(), ostream_iterator<string>(cout, " "));

    // 3rd way of output.
    for (Li = L.begin(); Li != L.end(); Li++) {
        cout << *Li << endl;
    }

    return 0;
}

// end of code

