// Program to illustrate use of virtual function
// to defeat the slicing problem.

#include <string>
#include <iostream>

using std::string;
using std::cout;
using std::endl;

// 1.
// Class --> Pet.

class Pet {
public:
    string name;
    virtual void print() const;
};

// 2.
// Class --> Dog.

class Dog : public Pet {
public:
    string breed;
    virtual void print() const;
};

// 3.
// The main function.

int main()
{
    Dog vdog;
    Pet vpet;

    vdog.name = "Tiny";
    vdog.breed = "Great Dane";

    vpet = vdog;

    cout << "The slicing problem:" << endl;
    //vpet.breed; is illegal
    vpet.print();
    cout << "Note that it was print from Pet that was invoked." << endl;

    cout << "The slicing problem defeated:" << endl;

    Pet *ppet;
    ppet = new Pet;

    Dog *pdog;
    pdog = new Dog;

    pdog -> name = "Tiny";
    pdog -> breed = "Great Dane";

    ppet = pdog;

    ppet -> print();
    pdog -> print();

    return 0;
}

// 4.
// Definitions.

void Dog::print() const
{
    cout << "name: " << name << endl;
    cout << "breed: " << breed << endl;
}

// 5.
// Definitions.

void Pet::print() const
{
    cout << "name: " << name << endl;
}

//==============//
// End of code. //
//==============//
