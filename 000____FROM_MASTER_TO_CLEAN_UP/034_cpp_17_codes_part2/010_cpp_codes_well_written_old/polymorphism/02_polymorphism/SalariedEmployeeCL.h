//======================//
// Header file.         //
// SalariedEmployeeCL.h //
//======================//

// This is the interface for the class SalariedEmployeeCL.

#ifndef SALARIED_EMPLOYEE_CL_H
#define SALARIED_EMPLOYEE_CL_H

#include <string>
using std::string;

#include "EmployeeCL.h"

namespace PGG {
class SalariedEmployeeCL : public EmployeeCL {
public:
    SalariedEmployeeCL();
    SalariedEmployeeCL(string theName, string theSsn, double theWeeklySalary);
    double getSalary() const;
    void setSalary(double newSalary);
    virtual void printCheck();

private:
    double salary; // weekly
};

} // end of namespace PGG

#endif // end of guard SALARIED_EMPLOYEE_CL_H

//==============//
// End of code. //
//==============//
