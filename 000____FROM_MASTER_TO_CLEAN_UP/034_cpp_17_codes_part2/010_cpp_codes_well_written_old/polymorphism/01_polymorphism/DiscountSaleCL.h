//===================================//
// Header file.                      //
// DiscountSaleCL class declaration. //
//===================================//

#ifndef DISCOUNTSALE_CL_H
#define DISCOUNTSALE_CL_H

#include "SaleCL.h"

namespace PGG {

class DiscountSaleCL : public SaleCL {
public:
    DiscountSaleCL();
    DiscountSaleCL(double thePrice, double theDiscount);
    double getDiscount() const;
    void setDiscount(double newDiscount);
    virtual double bill() const;

private:
    double discount;
};

} // End of namespace PGG.

#endif // end of guard DISCOUNTSALE_CL_H

//==============//
// End of code. //
//==============//
