// this is header file of the class: Rectangle

#ifndef H_Rectangle
#define H_Rectangle

class Rectangle {
public:

    void setLength(double);
    void setWidth(double);
    double getLength();
    double getWidth();
    double getArea();

private:

    double length;
    double width;
};

#endif

// this is the end of the header file
