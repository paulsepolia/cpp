// this is the driver program of the class: Rectangle

#include <iostream>
#include "classRectangle.h"

using namespace std;

int main()
{
    Rectangle box;
    double boxLength;
    double boxWidth;

    // get the box length and width

    cout << "This program calculates the area of a rectangle.\n";
    cout << "What is the length? ";
    cin >> boxLength;
    cout << "What is the width? ";
    cin >> boxWidth;

    // call the member functions to set the box dimensions

    box.setLength(boxLength);
    box.setWidth(boxWidth);

    // call the member function to get the box information to display

    cout << "\nHere is the rectangle's data:\n";
    cout << "Length: " << box.getLength() << endl;
    cout << "Width : " << box.getWidth()  << endl;
    cout << "Area  : " << box.getArea()   << endl;

    return 0;
}


// this is the end of the driver program.
