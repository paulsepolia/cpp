// this is the implementation file of the class: Rectangle

#include <iostream>
#include "classRectangle.h"

using namespace std;

// 1.

void Rectangle::setLength(double len)
{
    if (len >= 0) {
        length = len;
    } else {
        length = 1.0;
        cout << "Invalid length. Using a default value of 1.\n";
    }
}

// 2.

void Rectangle::setWidth(double w)
{
    if (w >= 0) {
        width = w;
    } else {
        width = 1.0;
        cout << "Invalid width. Using a defauly value of 1.\n";
    }
}

// 3.

double Rectangle::getLength()
{
    return length;
}

// 4.

double Rectangle::getWidth()
{
    return width;
}

// 5.

double Rectangle::getArea()
{
    return width * length;
}

// this is the end of the implementation file

