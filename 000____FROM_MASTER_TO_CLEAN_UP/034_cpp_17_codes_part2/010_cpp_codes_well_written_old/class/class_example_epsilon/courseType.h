// this is the header file of the class: courseType

#ifndef H_courseType
#define H_courseType

#include <string>
#include <fstream>

using namespace std;

class courseType {
public:
    void setCourseInfo(string cName, string cNo, int credits);
    // function to set the course information
    // the course information is set according to the parameters.
    // postcondition: courseName = cName; courseNo = cNo;
    //                courseCredits = credits;

    void print(ostream& outF);
    // function to print the course information
    // this function sends the course information to the
    // output device specified by the parameter outF.
    // if the actual parameter to this function is the object cout
    // then the output is shown on the standard output device.
    // if the actual parameter is an ofstream variable, say,
    // outFile, then the output goes to the file specified by the outFile.

    int getCredits();
    // function to return the credit hours.
    // postcondition: the value of courseCredits is returned.

    string getCourseNumber();
    // function to return the course number.
    // postcondition: the value of courseNo is returned.

    string getCourseName();
    // function to return the course name.
    // postcondition: the value courseName is returned.

    courseType(string cName = "",
               string cNo = "",
               int credits = 0 );
    // constructor
    // the object is initialized according to the parameters.
    // postcondition: courseName = cName; courseNo = cNo;
    //                courseCredits = credits;

private:
    string courseName; // variable to store the course name
    string courseNo;   // variable to store the course number
    int courseCredits; // variable to store the credit hours
};

#endif

// this is the end of the header file
