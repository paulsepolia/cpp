//this is the implementation file of the class: studentType

#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include "personType.h"
#include "courseType.h"
#include "studentType.h"

using namespace std;

// 1.

void studentType::setInfo(string fName, string lName, int ID,
                          int nOfCourses, bool isTPaid,
                          courseType courses[],
                          char cGrades[])
{
    int i;

    setName(fName, lName);        // set the name
    sId = ID;                     // set the student ID
    isTuitionPaid = isTPaid;      // set isTuitionPaid
    numberOfCourses = nOfCourses; // set the number of courses

    // set the course information

    for (i=0; i<numberOfCourses; i++) {
        coursesEnrolled[i] = courses[i];
        coursesGrade[i] = cGrades[i];
    }

    sortCourses(); // sort the array coursesEnrolled
}

// 2.

studentType::studentType()
{
    numberOfCourses = 0;
    sId = 0;
    isTuitionPaid = false;

    for (int i = 0; i < 6; i++) {
        coursesGrade[i] = '*';
    }
}

// 3.

void studentType::print(ostream & outF, double tuitionRate)
{
    int i;

    outF << "Student Name: " << getFirstName()
         << " " << getLastName() << endl;

    outF << "Student ID: " << sId << endl;

    outF << "Number of courses enrolled: "
         << numberOfCourses << endl;

    outF << endl;

    outF << left;

    outF << "Course No" << setw(15) << "  Course Name"
         << setw(8) << "Gredits"
         << setw(6) << "Grade" << endl;

    outF << right;

    for (i = 0; i < numberOfCourses; i++) {
        coursesEnrolled[i].print(outF);

        if (isTuitionPaid) {
            outF << setw(4) << coursesGrade[i] << endl;
        } else {
            outF << setw(4) << "***" << endl ;
        }
    }

    outF << endl;

    outF << "Total number of credit hours: "
         << getHoursEnrolled() << endl;

    outF << fixed << showpoint << setprecision(2);

    if (isTuitionPaid) {
        outF << "Mid-Semester GPA: " << getGpa() << endl;
    } else {
        outF << "*** Grades are being held for not paying "
             << "the tuition. ***" << endl;
        outF << "Amount Due: $" << billingAmount(tuitionRate) << endl;
    }

    outF << "-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-" << endl << endl;
}

// 4.

int studentType::getHoursEnrolled()
{
    int totalCredits = 0;
    int i;

    for ( i = 0; i < numberOfCourses; i++ ) {
        totalCredits = totalCredits + coursesEnrolled[i].getCredits();
    }

    return totalCredits;
}

// 5.

double studentType::billingAmount(double tuitionRate)
{
    return tuitionRate * getHoursEnrolled();
}

// 6.

double studentType::getGpa()
{
    int i ;
    double sum = 0.0;

    for (i = 0; i < numberOfCourses; i++ ) {
        switch (coursesGrade[i]) {
        case 'A':
            sum += coursesEnrolled[i].getCredits() * 4;
            break;
        case 'B':
            sum += coursesEnrolled[i].getCredits() * 3;
            break;
        case 'C':
            sum += coursesEnrolled[i].getCredits() * 2;
            break;
        case 'D':
            sum += coursesEnrolled[i].getCredits() * 1;
            break;
        case 'F':
            sum += coursesEnrolled[i].getCredits() * 0;
            break;
        default:
            cout << "Invalid Course Grade." << endl;
        }
    }

    return sum / getHoursEnrolled();
}

// 7.

void studentType::sortCourses()
{
    int i, j;
    int minIndex;
    courseType temp;
    char tempGrade;
    string course1;
    string course2;

    for ( i = 0; i < numberOfCourses - 1; i++ ) {
        minIndex = i;

        for ( j = i + 1; j < numberOfCourses; j++ ) {
            course1 =
                coursesEnrolled[minIndex].getCourseNumber();
            course2 = coursesEnrolled[j].getCourseNumber();

            if (course1 > course2) {
                minIndex = j;
            }
        }
    }

    temp = coursesEnrolled[minIndex];
    coursesEnrolled[minIndex] = coursesEnrolled[i];
    coursesEnrolled[i] = temp;

    tempGrade = coursesGrade[minIndex];
    coursesGrade[minIndex] = coursesGrade[i];
    coursesGrade[i] = tempGrade;
}


// this is the end of the implementation file.
