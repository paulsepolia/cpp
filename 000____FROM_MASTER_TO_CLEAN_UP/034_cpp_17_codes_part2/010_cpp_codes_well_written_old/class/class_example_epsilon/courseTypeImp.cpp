// this is the implementation file of the class: courseType

#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>
#include "courseType.h"

using namespace std;


// 1.

void courseType::setCourseInfo(string cName, string cNo, int credits)
{
    courseName = cName;
    courseNo = cNo;
    courseCredits = credits;
}

// 2.

void courseType::print(ostream & outF)
{
    outF << left;
    outF << setw(8) << courseNo << "   ";
    outF << setw(15) << courseName;
    outF << right;
    outF << setw(3) << courseCredits << "   ";
}

// 3.

int courseType::getCredits()
{
    return courseCredits;
}

// 4.

string courseType::getCourseNumber()
{
    return courseNo;
}

// 5.

courseType::courseType( string cName, string cNo, int credits)
{
    courseName = cName;
    courseNo = cNo;
    courseCredits = credits;
}

// this is the end of the implementation file
