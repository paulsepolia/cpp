// driver program for the class clockType

// the main function

#include <iostream>
#include "clock_type_header.h"

using namespace std;

int main()
{
    clockType myClock;
    clockType yourClock;

    int hours;
    int minutes;
    int seconds;

    // set the time of object: myClock

    myClock.setTime(5, 4, 30);

    // print the time of object: myClock

    cout << " myClock: ";
    myClock.printTime();
    cout << endl;

    // print the time of object: yourClock

    cout << " yourClock: ";
    yourClock.printTime();
    cout << endl;

    // compare myClock and yourClock

    if ( myClock.equalTime(yourClock) ) {
        cout << " Both times are equal. " << endl;
    } else {
        cout << " The two times are not equal. " << endl;
    }

    // entering the hours, minutes and seconds

    cout << " Enter the hours, minutes and seconds: ";
    cin >> hours >> minutes >> seconds;
    cout << endl;

    // setting the time of myClock

    myClock.setTime(hours, minutes, seconds);

    // print the time of myClock

    cout << " New myClock: ";
    myClock.printTime();
    cout << endl;

    // increment the time of myClock by one second

    myClock.incrementSeconds();

    // print the time of myClock

    cout << " After incrementing myClock by one second, myClock: ";
    myClock.printTime();
    cout << endl;

    // retrieve the hours, minutes, and seconds of the object myClock

    myClock.getTime(hours, minutes, seconds);

    // output the value of hours, minutes, and seconds

    cout << " hours = " << hours
         << ", minutes = " << minutes
         << ", seconds = " << seconds << endl;

    // array of objects clockType

    const int dimen = 10000;
    int i;
    const int iLim = dimen;

    clockType * array_clock_type = new clockType [dimen];

    for ( i=0; i<iLim; i++ ) {
        cout << i << " ";
        array_clock_type[i].printTime();
        cout << endl;
    }

    delete [] array_clock_type;

    return 0;
}

// end of the driver program
