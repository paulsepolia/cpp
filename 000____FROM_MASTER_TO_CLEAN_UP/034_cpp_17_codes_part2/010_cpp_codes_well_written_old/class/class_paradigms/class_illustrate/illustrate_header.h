// class illustrate header file.
// illustrate_header.h

class illustrate {
public:
    static int count;
    // public static variable.

    void print() const;
    // function to output x, y, and count.

    void setX( int a);
    // function to set x.
    // postconditiion: x=a;

    static void incrementY();
    // static function
    // function to increment y by 1.
    // postcondition: y = y + 1

    illustrate( int a = 0 );
    // constructor
    // Postcondition: x=a;
    // If no value is specified for a, x = 0

private:
    int x;
    static int y;
};

