// this is the header file for the class: dispenserType

class dispenserType {
public:
    int getNoOfItems() const;
    // function to show the number of items in the machine.
    // postcondition: the value of the numberOfItems is returned.

    int getCost() const;
    // function to show the cost of the item;
    // postcondition: the value of cost is returned.

    void makeSale();
    // function to reduce the number of items by 1.
    // postcondition: numberOfItems--;

    dispenserType( int setNoOfItems = 50, int setCost = 50 );
    // constructor
    // sets the cost and number of items in the dispenser
    // to the values specified by the user.
    // postcondition: numberOfItems = setNoOfItems;
    //                cost = setCost;
    // if no value is specified for a parameter, then its default value
    // is assigned to the corresponding member variable.

private:
    int numberOfItems;
    // variable to store the number of items in the dispenser.

    int cost;
    // variable to store the cost of an item.
};
