//=============================================//
// Header file.                                //
// PFArrayD class member functions definition. //
//=============================================//

// 1.

PFArrayD::PFArrayD() : capacity(50), used(0)
{
    a = new double [capacity];
}

// 2.

PFArrayD::PFArrayD(int size) : capacity(size), used(0)
{
    a = new double [capacity];
}

// 3.

PFArrayD::PFArrayD(const PFArrayD& pfaObject)
    : capacity(pfaObject.getCapacity()), used(pfaObject.getNumberUsed())
{
    a = new double [capacity];
    for (int i = 0; i < used; i++) {
        a[i] = pfaObject.a[i];
    }
}

// 4.

void PFArrayD::addElement(double element)
{
    if (used >= capacity) {
        cout << " Attempt to exceed capacity in PFArrayD." << endl;
        exit(0);
    }

    a[used] = element;
    used++;
}

// 5.

double& PFArrayD::operator[](int index)
{
    if (index >= used) {
        cout << " Illegal index in PFArrayD." << endl;
        exit(0);
    }

    return a[index];
}

// 6.

PFArrayD& PFArrayD::operator=(const PFArrayD& rightSide)
{
    if (capacity != rightSide.capacity) {
        delete [] a;
        a  = new double [rightSide.capacity];
    }

    capacity = rightSide.capacity;

    used = rightSide.used;

    for (int i = 0; i < used; i++) {
        a[i] = rightSide.a[i];
    }

    return *this;
}

// 7.

PFArrayD::~PFArrayD()
{
    delete [] a;
}

//==============//
// End of code. //
//==============//
