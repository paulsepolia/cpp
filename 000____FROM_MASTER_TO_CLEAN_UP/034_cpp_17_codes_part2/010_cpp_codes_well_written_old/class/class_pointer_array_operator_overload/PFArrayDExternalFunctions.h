//=========================================//
// Header file.                            //
// PFArrayD external functions definition. //
//=========================================//

// 1.

void testPFArrayD()
{
    int cap;
    cout << " Enter capacity of this super array: ";
    cin >> cap;
    PFArrayD temp(cap);

    cout << " Enter up to " << cap << " nonnegative numbers." << endl;
    cout << " Place a negative number at the end." << endl;

    double next;
    cin >> next;
    while ((next >= 0) && (!temp.full())) {
        temp.addElement(next);
        cin >> next;
    }

    cout << " You entered the following "
         << temp.getNumberUsed() << " numbers." << endl;

    int index;
    int count = temp.getNumberUsed();

    for (index = 0; index < count; index++) {
        cout << temp[index] << " ";
    }

    cout << endl;
    cout << " (plus a sentinel value.)" << endl;
}

//==============//
// End of code. //
//==============//

