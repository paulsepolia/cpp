// this is the header file of the class: classCircle

#ifndef H_classCircle
#define H_classCircle

#include <iostream>
#include <cmath>

using namespace std;

class classCircle {
public:
    void setRadius(double r);
    double getArea();

private:
    double radius;
};

#endif

// this is the end of the header file
