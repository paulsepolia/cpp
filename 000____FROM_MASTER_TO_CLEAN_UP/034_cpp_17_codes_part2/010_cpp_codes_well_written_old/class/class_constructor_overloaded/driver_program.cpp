// this is the driver program

#include <iostream>
#include <iomanip>
#include "sale.h"

using namespace std;

int main()
{
    // define a sale object with 6% sales tax calculated on a $24.95 sale

    sale cashier1(0.06, 24.95);

    // define a sale object with a tax-exempt $24.95 sale

    sale cashier2(24.95);

    // format the output

    cout << fixed << showpoint << setprecision(10);

    // display the after-tax for each sale object

    cout << "\nWith a 0.06 sales tax rate, the total\n";
    cout << "of the $24.95 sale is $";
    cout << cashier1.getTotal() << endl;

    cout << "\nOn a tax-exempt purchase, the total\n";
    cout << "of the $24.95 sale is, of course, $";
    cout << cashier2.getTotal() << endl;

    return 0;
}
