// this is the implementation file of the class: sale

#include "sale.h"

// 1.
// constructor with 2 parameters
sale::sale(double rate, double cost)
{
    taxRate = rate;
    calcSale(cost);
}

// 2.
// constructor with 1 parameter
sale::sale(double cost)
{
    taxRate = 0;
    total = cost;
}

// 3.
// default constructor
sale::sale()
{
    taxRate = 0.0;
    total = 0.0;
}

// 4.
double sale::getTotal()
{
    return total;
}

//5.
void sale::calcSale(double cost)
{
    total = cost + (cost * taxRate);
}

// this is the end of the implementation file


