#include <iostream>
using namespace std;

// 1. class declaration section

class RoomType {
private:
    double length; // declare length as a double variable
    double width;  // declare width as a double variable

public:
    RoomType( double = 0.0, double = 0.0 ); // constructor's declaration statement
    void showRoomValues();
    void setNewRoomValues(double, double);
    void calculateRoomArea();
};

// 2. class implementation section

RoomType::RoomType( double l, double w ) // this is a constructor
{
    length = l;
    width = w;

    cout << " Created a new room object using the default constructor. " << endl;
}

void RoomType::showRoomValues() // this is an accessor
{
    cout << " length = " << length << endl;
    cout << " width = " << width << endl;

    return;
}

void RoomType::setNewRoomValues( double l, double w ) // this is a mutator
{
    length = l;
    width = w;

    return;
}

void RoomType::calculateRoomArea() // this perfoms a calculation
{
    cout << ( length * width );

    return;
}

// 3. main program

int main()
{
    RoomType roomOne( 12.5, 18.2 ); // declare a variable of type RoomType

    cout << " The values for this room are: " << endl;
    roomOne.showRoomValues(); // uses a class method on this object
    cout << " The floor area of this room is: ";
    roomOne.calculateRoomArea(); // uses another class method on this object

    roomOne.setNewRoomValues( 5.5, 10.3 ); // call the mutator

    cout << endl;
    cout << endl;
    cout << " The values for this room have been changed to: " << endl;

    roomOne.showRoomValues();

    cout << endl;
    cout << " The new floor area of this room is: ";
    roomOne.calculateRoomArea();

    cout << endl;

    return 0;
}
