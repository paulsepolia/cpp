// this is the header file of the class: bClass

class bClass {
public:
    void setData(double);
    void setData(char, double);
    void print() const;

    bClass(char = '*', double = 0.0);

protected:
    char bCh;

private:
    double bX;
};

// end of the header file.
