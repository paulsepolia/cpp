// this is the implementation file of the class: bClass

#include <iostream>
#include "base_class_header.h"

using namespace std;

// 1.
void bClass::setData( double u )
{
    bX = u;  // access of the private variable: bX
}

// 2.
void bClass::setData( char ch, double u)
{
    bCh = ch; // access of the protected variable: bCh
    bX = u;   // access of the private variable: bX
}

// 3.
void bClass::print() const
{
    cout << "Base class: bCh = " << bCh // access of the protected variable: bCh
         << ", bX = " << bX << endl;  // access of the private variable: bX
}

// 4.
bClass::bClass(char ch, double u)
{
    bCh = ch;  // access of the protected variable: bCh
    bX = u;    // access of the private variable: bX
}

// end of the implementation file.

