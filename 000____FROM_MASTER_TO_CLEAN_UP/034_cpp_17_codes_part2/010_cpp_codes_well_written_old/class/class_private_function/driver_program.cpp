// this is the driver program of the class: BigNum

#include <iostream>
#include "BigNum.h"

using namespace std;

// function prototype

void getNumbers(BigNum &);

int main()
{
    BigNum stockShares; // create an object

    cout << " This progra, determines which stock purchase involved\n"
         << " the largest number of shares of stock.\n\n";

    getNumbers(stockShares);

    cout << "\n The largest number of shares was "
         << stockShares.getBiggest() << ".\n";

    return 0;
}

// function definition

void getNumbers(BigNum &stock)
{
    int numTrans;
    int numShares;

    // get number of stock purchase transactions
    cout << " How many stock purchase have you made this month? ";
    cin >> numTrans;

    // Loop once for each purchase transaction
    for (int trans = 1; trans <= numTrans; trans++) {
        cout << "Transaction " << trans << " shares purchased: ";
        cin >> numShares;

        while(!stock.examineNum(numShares)) {
            cout << "The number entered should be greaer than 0.\n"
                 << "Re-enter the number of stock shares purchased: ";
            cin >> numShares;
        }
    }
}

// this is the end of the driver program
