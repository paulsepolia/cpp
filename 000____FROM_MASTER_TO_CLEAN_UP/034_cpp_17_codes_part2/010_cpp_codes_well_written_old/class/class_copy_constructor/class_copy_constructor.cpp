#include <iostream>
#include <iomanip>
using namespace std;

// 1. class declaration section

class Date {
private:
    int month;
    int day;
    int year;
public:
    Date( int=7, int=4, int=2005 ); // constructor
    Date( const Date& );            // copy constructor
    void showDate();                // member function to display a date
};

// 2. class implementation section

Date::Date( int mm, int dd, int yyyy )
{
    month = mm;
    day   = dd;
    year  = yyyy;
}

Date::Date( const Date& olddate )
{
    day   = olddate.day;     // assign the date
    month = olddate.month;   // assign the date
    year  = olddate.year;    // assign the year

    return;
}

void Date::showDate()
{
    cout << " The date is ";
    cout << setfill('0')
         << setw(2) << month << '/'
         << setw(2) << day   << '/'
         << setw(3) << year % 100; // extract the last 2 year digits
    cout << endl;

    return;
}

// 3. main program

int main()
{
    Date a(4,1,2007), b(12,18,2008); //declare 2 objects
    Date c(a);  // use the copy constructor
    Date d = b; // use the copy constructor

    cout << endl;
    cout << " The date stored in a is ";
    a.showDate();
    cout << endl;

    cout << " The date stored in b is ";
    b.showDate();
    cout << endl;

    cout << " The date stored in c is ";
    c.showDate();
    cout << endl;

    cout << " The date stored in d is ";
    d.showDate();
    cout << endl;

    return 0;
}

