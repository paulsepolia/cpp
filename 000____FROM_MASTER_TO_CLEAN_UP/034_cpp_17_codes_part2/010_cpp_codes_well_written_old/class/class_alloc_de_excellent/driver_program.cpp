// this is the driver program of the class: InvItem

#include <iostream>
#include <cstring>
#include "InvItem.h"

using namespace std;

int main()
{
    // use the constructor to creae an object.
    InvItem stock("Wrench", 20);

    // print the object;s details.
    cout << " item description: " << stock.getDesc() << endl;
    cout << " units on hand: " << stock.getUnits() << endl;

    return 0;
}
