// this is the header file of the class InvItem.
// purpose is to illustrate the use of constructors
// and destructors in allocation and deallocation of memory.

#ifndef H_InvItem
#define H_InvItem

class InvItem {
private:
    char *description;
    int units;

public:
    InvItem(char *desrc, int number);
    ~InvItem();
    char *getDesc();
    int getUnits();
};

#endif

// this is the end of the header file
