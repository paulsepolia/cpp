#include <iostream>
#include <iomanip>
using namespace std;

// 1. class declaration section

class Date {
private:
    int month;
    int day;
    int year;
public:
    Date( int=7, int=4, int=2005 ); // constructor
    void setDate( int, int, int );  // member function to copy a date
    void showDate();                // member function to display a date
};

// 2. class implementation section

Date::Date( int mm, int dd, int yyyy )
{
    month = mm;
    day = dd;
    year = yyyy;
}

void Date::setDate( int mm, int dd, int yyyy )
{
    month = mm;
    day = dd;
    year = yyyy;
    return;
}

void Date::showDate()
{
    cout << " The date is ";
    cout << setfill('0')
         << setw(2) << month << '/'
         << setw(2) << day   << '/'
         << setw(3) << year % 100; // extract the last 2 year digits
    cout << endl;

    return;
}

// 3. main program

int main()
{
    Date a, b, c(4,1,2000); //declare 3 objects

    b.setDate(12,25,2006); // assign values to b's data members
    a.showDate();          // display object a's value
    b.showDate();          // display object b's value
    c.showDate();          // display object c's value

    return 0;
}

