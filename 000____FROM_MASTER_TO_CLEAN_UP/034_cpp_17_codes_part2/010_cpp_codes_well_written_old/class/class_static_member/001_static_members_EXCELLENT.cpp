// Demonstartion of static member functions
// and of static member furiables.

#include <iostream>
using namespace std;

// Server class declaration

class Server {
public:
    Server(char letterName); // constructor
    static int getTurn();
    void serveOne();
    static bool stillOpen();

private:
    static int turn;
    static int lastServed;
    static bool nowOpen;
    char name;
};

// Static member variables initialization

int Server::turn = 0;
int Server::lastServed = 0;
bool Server::nowOpen = true;

// the main function

int main()
{
    Server s1('A');
    Server s2('B');

    int number;
    int count;

    do {
        cout << "How namy in your group? ";
        cin >> number;
        cout << "Your turns are: ";

        for (count = 0; count < number; count++) {
            cout << Server::getTurn() << " ";
        }

        cout << endl;
        s1.serveOne();
        s2.serveOne();
    } while (Server::stillOpen());

    cout << "Now closing service." << endl;

    return 0;
}

// class member functions definition

// 1.

Server::Server(char letterName) : name(letterName)
{
    /* Empty */
}

// 2.

int Server::getTurn()
{
    turn++;
    return turn;
}

// 3.

bool Server::stillOpen()
{
    return nowOpen;
}

// 4.

void Server::serveOne()
{
    if (nowOpen && lastServed < turn) {
        lastServed++;
        cout << "Server " << name
             << " now serving " << lastServed << endl;
    }

    if (lastServed >= turn) { // Everyone served
        nowOpen = false;
    }
}

// endo of code
