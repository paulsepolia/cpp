#include <iostream>
using namespace std;

// 1. class declaration section

class Employee {
private:
    static double taxRate;
    int idNum;
public:
    Employee( int = 0 ); // constructor
    void display() ; // access function
};

// 2. static member definition
double Employee::taxRate = 0.007;  // this defines taxRate

// 3. class implementation section

Employee::Employee( int num )
{
    idNum = num;
}

void Employee::display()
{
    cout << " Employee number " << idNum
         << " has a tax rate of " << taxRate << endl;

    return;
}

// 4. main function

int main()
{
    Employee emp1(11122), emp2(11133);

    emp1.display();
    emp2.display();

    return 0;
}
