// driver program for class: personType

#include <iostream>
#include <string>
#include "personType.h"

using namespace std;

int main()
{
    string first;
    string last;
    personType person_a;


    cout << " give the first name: ";
    cin >> first;
    cout << " the first name is " << first << endl;

    cout << " give the second name: ";
    cin >> last;
    cout << " the last name is " << last;

    person_a.setName( first, last );

    cout << endl;
    cout << " using the class personType:" << endl;
    person_a.print();
    person_a.getFirstName();
    person_a.getLastName();

    cout << endl;

    return 0;
}
