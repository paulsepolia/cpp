// this is the implementation file of the class: dateType

#include <iostream>
#include "dateType.h"

using namespace std;

// 1.

void dateType::setDate(int month, int day, int year)
{
    dMonth = month;
    dDay = day;
    dYear = year;
}

// 2.

int dateType::getDay() const
{
    return dDay;
}

// 3.

int dateType::getMonth() const
{
    return dMonth;
}

// 4.

int dateType::getYear() const
{
    return dYear;
}

// 5.

void dateType::printDate() const
{
    cout << dMonth << "-" << dDay << "-" << dYear;
}

// 6.
// constructor with parameters

dateType::dateType(int month, int day, int year)
{
    dMonth = month;
    dDay = day;
    dYear = year;
}

// this is the end of the implementation file
