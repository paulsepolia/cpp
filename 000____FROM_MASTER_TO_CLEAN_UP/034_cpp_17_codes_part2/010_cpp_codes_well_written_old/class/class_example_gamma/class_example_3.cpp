#include <iostream>
#include <iomanip>
using namespace std;

// 1. class declaration section

class Date {
private:
    int month;
    int day;
    int year;
public:
    Date( int=7, int=4, int=2005 ); // constructor
    Date( long int );               // another constructor
    void showDate();                // member function to display a date
};

// 2. class implementation section

Date::Date( int mm, int dd, int yyyy )
{
    month = mm;
    day = dd;
    year = yyyy;
}

Date::Date( long int yyyymmdd )
{
    year = int( yyyymmdd/10000.0 );                        // extract the year
    month = int ( ( yyyymmdd - year * 10000.0 ) / 100.0 ); // extract the month
    day = int( yyyymmdd - year*10000.0 - month*100.0 );    // extract the day

}

void Date::showDate()
{
    cout << " The date is ";
    cout << setfill('0')
         << setw(2) << month << '/'
         << setw(2) << day   << '/'
         << setw(2) << year % 100;  // extract the last 2 year digits
    cout << endl;
    return;
}
// 3. main program

int main()
{
    Date a, b(4,1,1998), c(20060515L); //declare 3 objects

    a.showDate();
    b.showDate();
    c.showDate();

    return 0;
}

