//==============================//
// Default exception for 'new'. //
//==============================//

#include <iostream>
#include <climits>
#include <cstdlib>
#include <new>

using std::cout;
using std::endl;
using std::exit;
using std::bad_alloc;

// 1. The main function.

int main()
{
    char *p;

    try {
        for(;;) {
            p = new char[INT_MAX];
            if (p == NULL) {
                cout << " The operator 'new' returns NULL (=0)." << endl;
                cout << " This is not in accordance with Standard C++." << endl;
                exit(1);
            }
        }
    } catch(bad_alloc) {
        cout << " A bad_alloc exception has been thrown,"
             << " which is correct." << endl;
    }

    cout << endl;

    return 0;
}

//==============//
// End of code. //
//==============//
