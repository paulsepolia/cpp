//============================//
// Subscripting operator with //
// array-bounds checking.     //
//============================//

#include <iostream>

using std::endl;
using std::cout;

// 1. Class OutOfBounds.

class OutOfBounds {
public:

    // a.

    OutOfBounds(int ii)
        : i(ii)
    {}

    // b.

    int indexValue()
    {
        return i;
    }

private:
    int i;
};

// 2. Class Array.

class Array {
public:
    int &operator[](int i)
    {
        if (i < 0 || i >= 10) {
            throw OutOfBounds(i);
        }

        return a[i];
    }

private:
    int a[10];
};

// 3. The main function.

int main()
{
    Array a;

    try {
        a[3] = 30;
        cout << " a[3] = " << a[3] << endl;

        a[100] = 1000; // the program throws an exception here.

        cout << " a[3] = " << a[3] << endl;
    } catch(OutOfBounds error) {
        cout << "Subscript value " << error.indexValue() << " out of bounds." << endl;
    }

    return 0;
}

//==============//
// End of code. //
//==============//
