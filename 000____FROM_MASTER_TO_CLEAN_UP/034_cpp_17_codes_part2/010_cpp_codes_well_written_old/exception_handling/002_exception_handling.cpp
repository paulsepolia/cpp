#include <iostream>
#include <cstdlib>
using namespace std;

int main()
{
    int numerator;
    int denominator;
    bool needDenominator = true;

    cout << " Enter the numerator (whole number only): ";
    cin  >> numerator;

    cout << " Enter the denominator (whole number only): ";

    while( needDenominator ) {
        cin  >> denominator;

        try {
            if ( denominator == 0 ) {
                throw denominator;     // an integer value is thrown.
            }
        }

        catch( int e ) {
            cout << " A denominator value of " << e << " is invalid." << endl;
            cout << " Please re-enter the denominator (whole number only): ";
            continue; // this sends control back to the while statement.
        }

        cout << numerator << '/' << denominator
             << " = " << double(numerator) / double(denominator) << endl;

        needDenominator = false;

    }

    return 0;

}
