//========================//
// Class with exceptions. //
//========================//

#include <iostream>
using std::cin;
using std::cout;
using std::endl;

// 1. Class declaration and definition.

class NoMilk {
public:
    // a.

    NoMilk() {}

    // b.

    NoMilk(int howMany)
        : count(howMany) {}

    // c.

    int getCount() const
    {
        return count;
    }

private:

    // d.

    int count;
};

// 2. Main program.

int main()
{

    int donuts;
    int milk;
    double dpg;

    try {
        cout << " Enter number of donuts: " << endl;
        cin >> donuts;
        cout << " Enter number of glasses of milk: " << endl;
        cin >> milk;

        if (milk <= 0) {
            throw NoMilk(donuts);
        }

        dpg = donuts / static_cast<double>(milk);

        cout << donuts << " donuts. " << endl;
        cout << milk << " glasses of milk. " << endl;
        cout << " You have " << dpg << " donuts for each glass of milk." << endl;
    } catch(NoMilk e) {
        cout << e.getCount() << " donuts, and No Milk!" << endl;
        cout << " Go buy some milk." << endl;
    }

    cout << "End of program." << endl;

    return 0;
}

//==============//
// End of code. //
//==============//
