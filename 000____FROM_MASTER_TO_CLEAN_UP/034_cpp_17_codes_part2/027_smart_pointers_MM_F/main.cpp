#include <iostream>
#include <iomanip>
#include <memory>
#include <cmath>

int main() {

    const auto DIM = static_cast<size_t>(std::pow(10.0, 3.0));

    {
        std::cout << " --> example --> 1 --> start" << std::endl;

        auto *pd = new double(200);

        std::shared_ptr<double> sp1;
        sp1.reset(pd);

        std::shared_ptr<double> sp2 = std::move(sp1);

        std::cout << " -->  *pd --> " << *pd << std::endl;
        if (sp1) {
            std::cout << " --> *sp1 --> " << *sp1 << std::endl;
        }
        std::cout << " --> *sp2 --> " << *sp2 << std::endl;
        std::cout << " -->   pd --> " << pd << std::endl;
        std::cout << " -->  sp1 --> " << sp1 << std::endl;
        std::cout << " -->  sp2 --> " << sp2 << std::endl;

        std::cout << " --> example --> 1 --> end" << std::endl;
    }

    {
        std::cout << " --> example --> 2 --> start" << std::endl;

        std::unique_ptr<double[]> upa1 = std::make_unique<double[]>(DIM);

        upa1[0] = 1.1;
        upa1[1] = 2.2;
        upa1[2] = 3.3;

        std::cout << upa1[0] << std::endl;
        std::cout << upa1[1] << std::endl;
        std::cout << upa1[2] << std::endl;

        std::unique_ptr<double[]> upa2 = std::move(upa1);

        if (&upa1[0]) {
            std::cout << upa1[0] << std::endl;
        }

        std::cout << upa2[0] << std::endl;
        std::cout << upa2[1] << std::endl;
        std::cout << upa2[2] << std::endl;

        upa1 = std::move(upa2);

        std::cout << upa1[0] << std::endl;
        std::cout << upa1[1] << std::endl;
        std::cout << upa1[2] << std::endl;

        std::cout << " --> example --> 2 --> end" << std::endl;
    }

    {
        std::cout << " --> example --> 3 --> start" << std::endl;

        std::shared_ptr<double[]> spa1(new double[DIM]);

        spa1[0] = 1.1;
        spa1[1] = 2.2;
        spa1[2] = 3.3;

        std::cout << spa1[0] << std::endl;
        std::cout << spa1[1] << std::endl;
        std::cout << spa1[2] << std::endl;

        std::shared_ptr<double[]> spa2 = std::move(spa1);

        if (&spa1[0]) {
            std::cout << spa1[0] << std::endl;
        }

        std::cout << spa2[0] << std::endl;
        std::cout << spa2[1] << std::endl;
        std::cout << spa2[2] << std::endl;

        spa1 = spa2;

        std::cout << spa1[0] << std::endl;
        std::cout << spa1[1] << std::endl;
        std::cout << spa1[2] << std::endl;

        std::cout << spa2[0] << std::endl;
        std::cout << spa2[1] << std::endl;
        std::cout << spa2[2] << std::endl;

        std::cout << " --> example --> 3 --> end" << std::endl;
    }
}