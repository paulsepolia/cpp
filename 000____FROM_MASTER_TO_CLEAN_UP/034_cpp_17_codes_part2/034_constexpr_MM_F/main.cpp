#include <iostream>
#include <iomanip>
#include <memory>
#include <cmath>

constexpr size_t fib(size_t n) {

    size_t res = 0;

    if (n == 0) {
        return 1;
    } else if (n == 1) {
        return 1;
    } else {
        res = fib(n - 1) + fib(n - 2);
    }

    return res;
}

int main() {

    const size_t MAX_FAC = 50;

    std::cout << std::setprecision(0);


    {
        std::cout << " --> example --> 1 --> start" << std::endl;


        std::cout << std::fixed << std::setw(5) << std::right << 41 << " --> "
                  << std::fixed << std::setw(15) << std::right << fib(41) << std::endl;

        std::cout << std::fixed << std::setw(5) << std::right << 42 << " --> "
                  << std::fixed << std::setw(15) << std::right << fib(42) << std::endl;

        std::cout << std::fixed << std::setw(5) << std::right << 43 << " --> "
                  << std::fixed << std::setw(15) << std::right << fib(43) << std::endl;

        std::cout << std::fixed << std::setw(5) << std::right << 44 << " --> "
                  << std::fixed << std::setw(15) << std::right << fib(44) << std::endl;

        std::cout << std::fixed << std::setw(5) << std::right << 45 << " --> "
                  << std::fixed << std::setw(15) << std::right << fib(45) << std::endl;

        std::cout << std::fixed << std::setw(5) << std::right << 46 << " --> "
                  << std::fixed << std::setw(15) << std::right << fib(46) << std::endl;

        std::cout << std::fixed << std::setw(5) << std::right << 47 << " --> "
                  << std::fixed << std::setw(15) << std::right << fib(47) << std::endl;

        std::cout << std::fixed << std::setw(5) << std::right << 48 << " --> "
                  << std::fixed << std::setw(15) << std::right << fib(48) << std::endl;

        std::cout << std::fixed << std::setw(5) << std::right << 49 << " --> "
                  << std::fixed << std::setw(15) << std::right << fib(49) << std::endl;

        std::cout << std::fixed << std::setw(5) << std::right << 50 << " --> "
                  << std::fixed << std::setw(15) << std::right << fib(50) << std::endl;

        std::cout << std::fixed << std::setw(5) << std::right << 60 << " --> "
                  << std::fixed << std::setw(15) << std::right << fib(60) << std::endl;

        std::cout << std::fixed << std::setw(5) << std::right << 70 << " --> "
                  << std::fixed << std::setw(15) << std::right << fib(70) << std::endl;

        std::cout << " --> example --> 1 --> end" << std::endl;
    }

    {
        std::cout << " --> example --> 2 --> start" << std::endl;

        for (size_t i = 0; i < MAX_FAC; i++) {
            std::cout << std::fixed << std::setw(5) << std::right << i << " --> "
                      << std::fixed << std::setw(15) << std::right << fib(i) << std::endl;
        }

        std::cout << " --> example --> 2 --> end" << std::endl;
    }
}
