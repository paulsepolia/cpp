
#include <iostream>
#include <cmath>
#include <memory>
#include <vector>
#include <iomanip>

using std::cout;
using std::endl;
using std::pow;
using std::vector;
using std::shared_ptr;
using std::fixed;
using std::setprecision;

// type definitions

typedef unsigned long int uli;
typedef const uli culi;

// the main program

int main()
{
    culi DIMEN(3*static_cast<uli>(pow(10.0, 0.0)));

    cout << fixed;
    cout << setprecision(10);

    vector<shared_ptr<double>> spv1;
    vector<shared_ptr<double>> spv2;

    // build the vector

    for(uli i = 0; i != DIMEN; i++) {
        double * pd(new double(i));
        shared_ptr<double> spd;
        spd.reset(pd);
        spv1.push_back(spd);
    }

    for(uli i = 0; i != DIMEN; i++) {
        cout << " -------------------------------------------------------------->> " << i << endl;
        cout << " -->  spv1[" << i << "]             = " <<  spv1[i] << endl;
        cout << " -->  spv1[" << i << "].get()       = " <<  spv1[i].get() << endl;
        cout << " -->  spv1[" << i << "].use_count() = " <<  spv1[i].use_count() << endl;
        cout << " --> *spv1[" << i << "].get()       = " << *spv1[i].get() << endl;
    }

    // build the second vector

    spv2 = spv1;

    // output --> 1

    for(uli i = 0; i != DIMEN; i++) {
        cout << " -------------------------------------------------------------->> " << i << endl;
        cout << " -->  spv1[" << i << "]             = " <<  spv1[i] << endl;
        cout << " -->  spv1[" << i << "].get()       = " <<  spv1[i].get() << endl;
        cout << " -->  spv1[" << i << "].use_count() = " <<  spv1[i].use_count() << endl;
        cout << " --> *spv1[" << i << "].get()       = " << *spv1[i].get() << endl;
    }

    // output --> 2

    for(uli i = 0; i != DIMEN; i++) {
        cout << " -------------------------------------------------------------->> " << i << endl;
        cout << " -->  spv2[" << i << "]             = " <<  spv2[i] << endl;
        cout << " -->  spv2[" << i << "].get()       = " <<  spv2[i].get() << endl;
        cout << " -->  spv2[" << i << "].use_count() = " <<  spv2[i].use_count() << endl;
        cout << " --> *spv2[" << i << "].get()       = " << *spv2[i].get() << endl;
    }

    // reset --> 1

    for(uli i = 0; i != DIMEN; i++) {
        cout << " -------------------------------------------------------------->> " << i << endl;
        spv1[i].reset();
        cout << " -->  spv1[" << i << "]             = " <<  spv1[i] << endl;
        cout << " -->  spv2[" << i << "]             = " <<  spv2[i] << endl;
        cout << " -->  spv1[" << i << "].get()       = " <<  spv1[i].get() << endl;
        cout << " -->  spv2[" << i << "].get()       = " <<  spv2[i].get() << endl;
        cout << " -->  spv1[" << i << "].use_count() = " <<  spv1[i].use_count() << endl;
        cout << " -->  spv2[" << i << "].use_count() = " <<  spv2[i].use_count() << endl;
        cout << " --> *spv2[" << i << "].use_get()   = " << *spv2[i].get() << endl;
    }

    // reset --> 2

    for(uli i = 0; i != DIMEN; i++) {
        cout << " -------------------------------------------------------------->> " << i << endl;
        spv2[i].reset();
        cout << " -->  spv2[" << i << "]             = " <<  spv2[i] << endl;
        cout << " -->  spv2[" << i << "].get()       = " <<  spv2[i].get() << endl;
        cout << " -->  spv2[" << i << "].use_count() = " <<  spv2[i].use_count() << endl;
    }

    return 0;
}

// end
