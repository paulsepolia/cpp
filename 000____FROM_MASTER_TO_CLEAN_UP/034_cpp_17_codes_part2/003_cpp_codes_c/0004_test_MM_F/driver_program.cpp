#include <iostream>

using std::cout;
using std::endl;

int* foo()
{
    int a = 12345;
    return &a;
}

// the main function

int main()
{
    int* p(foo());
    cout << " *p = " << *p << endl;
    *p = 8;
    cout << " *p = " << *p << endl;

    return 0;
}

// end
