//==============================================//
// driver program 						        //
// move contructor and move assignment operator //
// and unique_ptr 						        //
//==============================================//

#include "parameters.hpp"
#include "vec.hpp"
#include <iostream>
#include <cmath>
#include <iomanip>

using std::cout;
using std::endl;
using std::cos;
using std::sin;
using std::fixed;
using std::setprecision;
using std::showpos;
using std::boolalpha;

// the main program

int main()
{
    // local parameters

    culi DIMEN(static_cast<uli>(pow(10.0, 2.0)));
    culi TRIALS(10*static_cast<uli>(pow(10.0, 0.0)));

    // local variables

    bool tmp_bool;

    // adjust the output

    cout << setprecision(10);
    cout << fixed;
    cout << showpos;
    cout << boolalpha;

    // main benchmark

    for(uli i = 0; i != TRIALS; i++) {
        cout << "----------------------------------------------->> " << i << endl;

        cout << " --> declare v1" << endl;

        Vec<double> v1;

        cout << " --> declare v2" << endl;

        Vec<double> v2;

        cout << " --> check allocation --> v1" << endl;
        tmp_bool = v1.check_allocation();
        cout << " --> " << tmp_bool << endl;

        cout << " --> check allocation --> v2" << endl;
        tmp_bool = v2.check_allocation();
        cout << " --> " << tmp_bool << endl;

        cout << " --> allocate --> v1" << endl;

        v1.allocate(DIMEN);

        cout << " --> check allocation --> v1" << endl;
        tmp_bool = v1.check_allocation();
        cout << " --> " << tmp_bool << endl;

        cout << " --> build --> v1 " << endl;

        for(uli j = 0; j != DIMEN; j++) {
            v1.set(j, sin(static_cast<double>(j)));
        }

        cout << " --> get the element 10 from v1" << endl;
        cout << " --> v1.get(10) = " << v1.get(10) << endl;

        cout << " --> copy v1 to v2, using the copy assignment operator" << endl;

        v2 = v1;

        cout << " --> check allocation --> v1" << endl;
        tmp_bool = v1.check_allocation();
        cout << " --> " << tmp_bool << endl;

        cout << " --> check allocation --> v2" << endl;
        tmp_bool = v2.check_allocation();
        cout << " --> " << tmp_bool << endl;

        cout << " --> get the element 10 from v1" << endl;
        cout << " --> v1.get(10) = " << v1.get(10) << endl;

        cout << " --> get the element 10 from v2" << endl;
        cout << " --> v2.get(10) = " << v2.get(10) << endl;
    }

    cout << "exit" << endl;
    return 0;
}

// end
