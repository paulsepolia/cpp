//===============//
// errors header //
//===============//

#ifndef ERRORS_H
#define ERRORS_H

#include <string>
using std::string;

const string ERROR_001("ERROR:001:THE VECTOR IS NOT ALLOCATED");
const string ERROR_002("ERROR:002:INVALID INDEX");

#endif // ERRORS_H
