#include <iostream>
#include <cstdint>
#include <memory>
#include <iomanip>

void fun1() {
    std::cout << " --> fun --> 1" << std::endl;
    throw std::exception();
}

void fun2() noexcept(false) {
    std::cout << " --> fun --> 2" << std::endl;
    throw std::exception();
}

void fun3() noexcept(true) {
    std::cout << " --> fun --> 3" << std::endl;
    throw std::exception();
}

class A {

public:

    A() = default;

    A(const A &obj) = default;

    A &operator=(const A &obj) = default;

    A(A &&obj) noexcept(true) = default;

    A &operator=(A &&obj) noexcept(true) = default;

    virtual ~A() noexcept = default;
};

int main() {

    {
        std::cout << " --> example --> 0 -------------------> start" << std::endl;

        A a;

        std::cout << " --> example --> 0 -------------------> end" << std::endl;
    }

    {
        std::cout << " --> example --> 1 -------------------> start" << std::endl;

        try {
            fun1();
        }
        catch (const std::exception &e) {
            std::cout << " --> exit with --> " << e.what() << std::endl;
        }
        catch (...) {
            std::cout << " --> exit with --> ..." << std::endl;
        }

        std::cout << " --> example --> 1 -------------------> end" << std::endl;
    }

    {
        std::cout << " --> example --> 2 -------------------> start" << std::endl;

        try {
            fun2();
        }
        catch (const std::exception &e) {
            std::cout << " --> exit with --> " << e.what() << std::endl;
        }
        catch (...) {
            std::cout << " --> exit with --> ..." << std::endl;
        }

        std::cout << " --> example --> 2 -------------------> end" << std::endl;
    }

    {
        std::cout << " --> example --> 3 -------------------> start" << std::endl;

        try {
            fun3();
        }
        catch (const std::exception &e) {
            std::cout << " --> exit with --> " << e.what() << std::endl;
        }
        catch (...) {
            std::cout << " --> exit with --> ..." << std::endl;
        }

        std::cout << " --> example --> 3 -------------------> end" << std::endl;
    }
}