//=====================//
// find the difference //
//=====================//

#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// findTheDifference_v1

#define findTheDifference findTheDifference_v1

// the main function

int main()
{
    Solution sol1;
    std::string s1{};
    std::string s2{};
    char res('\0');

    // # 1
    cout << "------------------------------------>> 1" << endl;
    s1 = "123456789";
    s2 = "1234567890";
    res = sol1.findTheDifference(s1, s2);
    cout << " s1  = " << s1 << endl;
    cout << " s2  = " << s2 << endl;
    cout << " res = " << res << endl;

    // # 2
    cout << "------------------------------------>> 2" << endl;
    s1 = "123456789";
    s2 = "98765-4321";
    res = sol1.findTheDifference(s1, s2);
    cout << " s1  = " << s1 << endl;
    cout << " s2  = " << s2 << endl;
    cout << " res = " << res << endl;

    // # 3
    cout << "------------------------------------>> 2" << endl;
    s1 = "a";
    s2 = "aa";
    res = sol1.findTheDifference(s1, s2);
    cout << " s1  = " << s1 << endl;
    cout << " s2  = " << s2 << endl;
    cout << " res = " << res << endl;
}

// end
