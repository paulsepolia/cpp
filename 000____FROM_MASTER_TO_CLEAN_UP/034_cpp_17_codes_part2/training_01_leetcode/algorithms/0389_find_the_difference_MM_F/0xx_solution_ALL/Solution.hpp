//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <string>

class Solution {
public:

    // findTheDifference_v1
    char findTheDifference_v1(std::string, std::string);
};

#endif

// end
