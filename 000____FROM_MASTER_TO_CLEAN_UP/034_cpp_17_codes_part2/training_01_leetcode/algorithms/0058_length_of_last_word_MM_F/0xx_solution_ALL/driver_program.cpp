//=====================//
// length of last word //
//=====================//

#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// lengthOfLastWord_v1

#define lengthOfLastWord lengthOfLastWord_v1

// the main function

int main()
{
    Solution sol1;
    std::string s("");
    int res(0);

    cout << "------------------------------------>> 1" << endl;
    s = "123 456";
    res = sol1.lengthOfLastWord(s);
    cout << " s   = " << s << endl;
    cout << " res = " << res << endl;

    cout << "------------------------------------>> 2" << endl;
    s = "123456";
    res = sol1.lengthOfLastWord(s);
    cout << " s   = " << s << endl;
    cout << " res = " << res << endl;

    cout << "------------------------------------>> 3" << endl;
    s = "123456 123 1  2";
    res = sol1.lengthOfLastWord(s);
    cout << " s   = " << s << endl;
    cout << " res = " << res << endl;

    cout << "------------------------------------>> 4" << endl;
    s = "123456 123 1  2 ";
    res = sol1.lengthOfLastWord(s);
    cout << " s   = " << s << endl;
    cout << " res = " << res << endl;

    return 0;
}

// end
