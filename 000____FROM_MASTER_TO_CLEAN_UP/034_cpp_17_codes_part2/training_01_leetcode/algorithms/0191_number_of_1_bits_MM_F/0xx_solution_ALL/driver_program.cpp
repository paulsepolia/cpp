//=============//
// add strings //
//=============//

#include <iomanip>
#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// hammingWeight_v1

#define hammingWeight hammingWeight_v1

// the main function

int main()
{
    Solution sol1;
    int num(0);
    int res(0);

    cout << "----------------------------------->> 1" << endl;
    num = 11;
    res = sol1.hammingWeight(num);
    cout << " num = " << num << endl;
    cout << " res = " << res << endl;

    cout << "----------------------------------->> 2" << endl;
    num = 12;
    res = sol1.hammingWeight(num);
    cout << " num = " << num << endl;
    cout << " res = " << res << endl;

    return 0;
}

// end
