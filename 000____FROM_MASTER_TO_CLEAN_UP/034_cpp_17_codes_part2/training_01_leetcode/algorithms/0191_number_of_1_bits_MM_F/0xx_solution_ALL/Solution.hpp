//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <vector>

class Solution {
public:

    // hammingWeight_v1
    int hammingWeight_v1(unsigned int);
};

#endif

// end
