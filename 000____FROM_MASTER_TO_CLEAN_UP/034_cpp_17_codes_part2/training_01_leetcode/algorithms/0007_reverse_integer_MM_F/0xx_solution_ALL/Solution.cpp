//===========================//
// Solution class definition //
//===========================//

#include <string>
#include <algorithm>
#include <cmath>

using std::string;
using std::reverse;
using std::to_string;
using std::pow;

#include "Solution.hpp"

// reverseIntSTL

int Solution::reverseIntSTL(int x)
{
    // --> step # 1
    // --> get the sign

    bool isNegative(false);

    if(x < 0) {
        isNegative = true;
    }

    // --> step # 2
    // --> convert integer to string

    string str1(to_string(abs(x)));

    // --> step # 3
    // --> reverse the string

    string str1_rev(str1);
    reverse(str1_rev.begin(), str1_rev.end());

    // --> step # 4
    // --> convert the string to integer

    int res(0);

    try {
        res = stoi(str1_rev);
    } catch(...) {
        return 0;
    }

    // --> step # 5
    // --> get the sign back

    if(isNegative) {
        res = - res;
    }

    return res;
}

// reverseIntNoSTL

int Solution::reverseIntNoSTL(int x)
{
    // --> step # 1
    // --> get the sign

    bool isNegative(false);

    if(x < 0) {
        isNegative = true;
    }

    // --> step # 2

    int n(abs(x));
    int remainder(0);
    int res(0);

    while(n != 0) {
        remainder = n%10;
        res = res*10 + remainder;
        n /= 10;
    }

    // --> step # 3
    // --> convert the integer to string
    // --> and back to integer to catch the overflow

    string str_help(to_string(abs(x)));

    try {
        reverse(str_help.begin(), str_help.end());
        res = stoi(str_help);
    } catch(...) {
        return 0;
    }

    // --> step # 4
    // --> get the sign back

    if(isNegative) {
        res = - res;
    }

    return res;
}

//=====//
// end //
//=====//
