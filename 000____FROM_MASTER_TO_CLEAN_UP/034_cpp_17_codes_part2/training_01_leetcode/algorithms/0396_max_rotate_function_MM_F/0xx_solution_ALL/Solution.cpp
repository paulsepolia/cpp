//===========================//
// Solution class definition //
//===========================//

#include <algorithm>
#include <climits>
#include "Solution.hpp"

#include <iostream>
using std::endl;
using std::cout;

// maxRotateFunction_v1
// CORRECT BUT SLOW

int Solution::maxRotateFunction_v1(std::vector<int>& A)
{
    int sum(0);
    const unsigned int DIM(A.size());
    std::vector<int> vec{};
    unsigned int counter(0);

    if(DIM == 0) return 0;

    while(counter < DIM) {
        // get the sum
        for(unsigned int i = 1; i != DIM; i++) {
            sum = sum + i*A[i];
        }
        vec.push_back(sum);
        std::rotate(A.begin(), A.begin()+1, A.end());
        counter++;
        sum = 0;
    }

    int max_elem = *std::max_element(vec.begin(), vec.end());

    return max_elem;
}

// maxRotateFunction_v2
// CORRECT

int Solution::maxRotateFunction_v2(std::vector<int>& A)
{
    if (A.empty()) {
        return 0;
    }

    const int DIM(A.size());
    int sum(0);
    int res(INT_MIN);
    int temp(0);

    for (int i = 0; i < DIM; i++) {
        sum += A[i];
        temp += i * A[i];
    }
    res = temp;
    for (int i = 1; i < DIM; i++) {
        temp = sum - A.size() * A[A.size() - i] + temp;
        res = std::max(res, temp);
    }

    return res;
}

// end
