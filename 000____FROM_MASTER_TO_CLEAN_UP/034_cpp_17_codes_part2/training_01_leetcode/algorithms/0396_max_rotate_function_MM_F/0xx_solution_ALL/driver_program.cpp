//=============//
// add strings //
//=============//

#include <iomanip>
#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// maxRotateFunction_v1
// maxRotateFunction_v2

#define maxRotateFunction maxRotateFunction_v2

// the main function

int main()
{
    Solution sol1;
    int res(0);

    // # 1

    cout << "---------------------------------------------------------------->> 1" << endl;
    {
        std::vector<int> A{4,3,2,6};
        res = sol1.maxRotateFunction(A);
        cout << " res = " << res << endl;
    }

    return 0;
}

// end
