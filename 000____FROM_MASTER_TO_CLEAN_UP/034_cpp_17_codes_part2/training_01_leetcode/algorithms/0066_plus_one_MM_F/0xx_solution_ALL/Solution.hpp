//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <vector>

class Solution {
public:

    //  plusOne_v1
    std::vector<int> plusOne_v1(std::vector<int>&);

};

#endif

// end
