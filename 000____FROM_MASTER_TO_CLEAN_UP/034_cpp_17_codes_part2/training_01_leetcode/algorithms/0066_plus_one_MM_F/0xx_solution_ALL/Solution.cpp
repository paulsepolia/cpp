//===========================//
// Solution class definition //
//===========================//

#include <algorithm>
#include "Solution.hpp"

// plusOne_v1

std::vector<int> Solution::plusOne_v1(std::vector<int> & nums)
{
    // get the size
    const unsigned int DIM(nums.size());

    if(DIM == 1) {
        nums[0] = nums[0] + 1;

        if(nums[0] >= 10) {
            nums[0] = nums[0] - 10;
            auto it = nums.begin();
            nums.insert(it, 1);
        }

        return nums;
    }

    // reverse
    std::reverse(nums.begin(), nums.end());

    // adjust the digits
    nums[0] = nums[0] + 1;
    if (nums[0] >= 10) {
        for(unsigned int i = 0; i != DIM-1; i++) {
            if(nums[i] >= 10) {
                nums[i] = nums[i] - 10;
                nums[i+1] = nums[i+1] + 1;
            }
        }
    }

    // insert if needed
    if(nums[DIM-1] >= 10) {
        nums[DIM-1] = nums[DIM-1] - 10;
        nums.push_back(1);
    }

    // reverse again
    reverse(nums.begin(), nums.end());

    return nums;
}

// end
