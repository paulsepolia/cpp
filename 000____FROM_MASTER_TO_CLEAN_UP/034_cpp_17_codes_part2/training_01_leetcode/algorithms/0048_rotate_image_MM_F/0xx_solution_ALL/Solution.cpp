//===========================//
// Solution class definition //
//===========================//

#include "Solution.hpp"

// rotate_v1

void Solution::rotate_v1(std::vector<std::vector<int>> & matrix)
{
    // get the size
    const unsigned int DIM(matrix.size());
    // allocate RAM for mat_rot
    std::vector<std::vector<int>> mat_rot{};
    mat_rot.resize(DIM);
    for(unsigned int i = 0; i != DIM; i++) {
        mat_rot[i].resize(DIM);
    }

    // get the i row of the original matrix
    // and put it as DIM-i-1 column of the rotated matrix
    for(unsigned int i = 0; i != DIM; i++) {
        std::vector<int> tmp(matrix[i]);
        for(unsigned int j = 0; j != DIM; j++) {
            mat_rot[j][DIM-1-i] = tmp[j];
        }
    }
    matrix = mat_rot;
}

// end
