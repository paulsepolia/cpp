//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <vector>

class Solution {
public:

    // rotate_v1
    void rotate_v1(std::vector<std::vector<int>>&);
};

#endif

// end
