//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <vector>

class Solution {
public:

    // containsDuplicates_v1
    bool containsDuplicates_v1(std::vector<int>&);
};

#endif

// end
