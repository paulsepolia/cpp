//=============//
// gas station //
//=============//

#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// canCompleteCircuit_v1

#define canCompleteCircuit canCompleteCircuit_v1

// the main function

int main()
{
    Solution sol1;
    std::vector<int> gas{};
    std::vector<int> cost;
    int res(0);

    res = sol1.canCompleteCircuit(gas, cost);
    cout << " --> res = " << res << endl;
    return 0;
}

// end
