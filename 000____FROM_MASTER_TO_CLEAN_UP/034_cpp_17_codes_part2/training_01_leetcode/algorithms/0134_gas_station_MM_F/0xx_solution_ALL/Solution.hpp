//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

// Solution declaration

#include <vector>

class Solution {
public:

    // # 1 canCompleteCircuit_v1
    int canCompleteCircuit_v1(std::vector<int>&, std::vector<int>&);
};

#endif

// end
