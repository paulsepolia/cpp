//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

class Solution {
public:

    // int getSum_v1
    int getSum_v1(int, int);

    // int getSum_v2
    int getSum_v2(int, int);
};

#endif

// end
