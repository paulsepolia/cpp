//================//
// reverse string //
//================//

#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// reverseString_v1

#define reverseString reverseString_v1

// the main function

int main()
{
    Solution sol1;
    std::string str{};

    // # 1
    cout << "----------------------------------->> 1" << endl;
    str = "1234567890";
    cout << " str = " << str << endl;
    str = sol1.reverseString(str);
    cout << " str = " << str << endl;

    // # 2
    cout << "----------------------------------->> 2" << endl;
    str = "Pavlos G. Galiatsatos";
    cout << " str = " << str << endl;
    str = sol1.reverseString(str);
    cout << " str = " << str << endl;
    str = sol1.reverseString(str);
    cout << " str = " << str << endl;

    return 0;
}

// end
