//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <string>

class Solution {
public:

    // string reverseString_v1
    std::string reverseString_v1(std::string);
};

#endif

// end
