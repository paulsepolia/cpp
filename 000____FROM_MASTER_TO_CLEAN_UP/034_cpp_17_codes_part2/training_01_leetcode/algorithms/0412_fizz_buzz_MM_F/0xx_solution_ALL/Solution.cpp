//===========================//
// Solution class definition //
//===========================//

#include "Solution.hpp"

// fizzBuzz_v1

std::vector<std::string> Solution::fizzBuzz_v1(int n)
{
    std::vector<std::string> res{};
    const std::string S35("FizzBuzz");
    const std::string S3("Fizz");
    const std::string S5("Buzz");

    for(int i = 1; i <= n; i++) {
        if(i%3 == 0 && i%5 == 0) {
            res.push_back(S35);
        } else if(i%3 == 0) {
            res.push_back(S3);
        } else if(i%5 == 0) {
            res.push_back(S5);
        } else {
            res.push_back(std::to_string(i));
        }
    }

    return res;
}

// end
