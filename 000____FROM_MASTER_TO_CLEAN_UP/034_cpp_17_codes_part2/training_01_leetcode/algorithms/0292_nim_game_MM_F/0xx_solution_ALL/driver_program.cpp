//==========//
// Nim Game //
//==========//

#include <iomanip>
#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// canWinNim_v1

#define canWinNim canWinNim_v1

// the main function

int main()
{
    Solution sol1;
    bool res(false);
    int n(0);
    cout << std::fixed;
    cout << std::boolalpha;

    // # 1
    cout << "---------------------------------->> 1" << endl;
    n = 100;
    res = sol1.canWinNim(n);
    cout << " n   = " << n << endl;
    cout << " res = " << res << endl;

    // # 2
    cout << "---------------------------------->> 2" << endl;
    n = 404;
    res = sol1.canWinNim(n);
    cout << " n   = " << n << endl;
    cout << " res = " << res << endl;

    // # 3
    cout << "---------------------------------->> 2" << endl;
    n = 3;
    res = sol1.canWinNim(n);
    cout << " n   = " << n << endl;
    cout << " res = " << res << endl;

    return 0;
}

// end
