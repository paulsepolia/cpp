//===================================//
// remove element from a linked list //
//===================================//

#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// frequencySort_v1

#define frequencySort frequencySort_v1

// the main function

int main()
{
    Solution sol1;
    std::string s("");
    std::string res("");

    // # 1
    s = "1234";
    res = sol1.frequencySort(s);
    cout << "   s = " << s << endl;
    cout << " res = " << res << endl;

    return 0;
}

// end
