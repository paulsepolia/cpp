//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <string>

// Solution declaration

class Solution {
public:

    // frequencySort_v1
    std::string frequencySort_v1(std::string);
};

#endif

// end
