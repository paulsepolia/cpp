//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <string>

class Solution {
public:
    // isPalindrome_v1
    bool isPalindrome_v1(std::string);
};

#endif

// end
