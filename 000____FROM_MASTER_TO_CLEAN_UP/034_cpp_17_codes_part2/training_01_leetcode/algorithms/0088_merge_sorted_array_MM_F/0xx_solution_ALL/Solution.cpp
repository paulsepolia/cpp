//===========================//
// Solution class definition //
//===========================//

#include <algorithm>
#include "Solution.hpp"

// merge_v1

void Solution::merge_v1(std::vector<int>& nums1, int m, std::vector<int>& nums2, int n)
{
    std::vector<int> vec_tmp {};
    vec_tmp.resize(m+n);

    // merge using STL

    std::merge(nums1.begin(), nums1.begin()+m,
               nums2.begin(), nums2.begin()+n, vec_tmp.begin());

    // put in nums1

    nums1.clear();
    nums1.resize(m+n);
    nums1 = vec_tmp;

    // clear the tmp

    vec_tmp.clear();
    vec_tmp.resize(0);
    vec_tmp.shrink_to_fit();
}

// merge_v2

void Solution::merge_v2(std::vector<int>& nums1, int m, std::vector<int>& nums2, int n)
{
    std::vector<int> vec_tmp {};
    vec_tmp.resize(m+n);

    // merge here

    std::vector<int>::iterator first1 = nums1.begin();
    std::vector<int>::iterator last1 = nums1.begin()+m;
    std::vector<int>::iterator first2 = nums2.begin();
    std::vector<int>::iterator last2 = nums2.begin()+n;
    std::vector<int>::iterator result = vec_tmp.begin();

    while(true) {
        if (first1 == last1) {
            std::copy(first2,last2,result);
            break;
        }
        if (first2 == last2) {
            std::copy(first1,last1,result);
            break;
        }
        *result++ = (*first2 < *first1) ? *first2++ : *first1++;
    }

    // put in nums1

    nums1.clear();
    nums1.resize(m+n);
    nums1 = vec_tmp;

    // clear the tmp

    vec_tmp.clear();
    vec_tmp.resize(0);
    vec_tmp.shrink_to_fit();
}

// end
