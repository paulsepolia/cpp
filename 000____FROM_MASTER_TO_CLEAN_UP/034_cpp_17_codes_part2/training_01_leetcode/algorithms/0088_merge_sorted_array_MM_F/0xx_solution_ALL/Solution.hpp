//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <vector>

class Solution {
public:

    // merge_v1
    void merge_v1(std::vector<int>&, int, std::vector<int>&, int);

    // merge_v2
    void merge_v2(std::vector<int>&, int, std::vector<int>&, int);
};

#endif

//=====//
// end //
//=====//
