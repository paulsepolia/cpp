//=======//
// merge //
//=======//

#include <iostream>
#include <iomanip>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// merge_v1
// merge_v2

#define merge merge_v2

// the main function

int main()
{
    Solution sol1;
    std::vector<int> nums1 {};
    std::vector<int> nums2 {};
    int m(0);
    int n(0);

    cout << std::fixed;
    cout << std::showpos;

    // # 1

    m = 5;
    n = 5;
    nums1.resize(m+n);
    nums2.resize(n);
    nums1 = {1,2,3,4,5,0,0,0,0,0};
    nums2 = {1,2,3,4,5};

    sol1.merge(nums1, m, nums2, n);

    for(auto & i: nums1) {
        cout << std::right << i << " ";
    }
    cout << endl;

    // # 2

    m = 5;
    n = 3;
    nums1.resize(m+n);
    nums2.resize(n);
    nums1 = {1,2,3,4,5,0,0,0};
    nums2 = {-3,-2,-1};

    sol1.merge(nums1, m, nums2, n);

    for(auto & i: nums1) {
        cout << std::right << i << " ";
    }
    cout << endl;

    return 0;
}

//=====//
// end //
//=====//
