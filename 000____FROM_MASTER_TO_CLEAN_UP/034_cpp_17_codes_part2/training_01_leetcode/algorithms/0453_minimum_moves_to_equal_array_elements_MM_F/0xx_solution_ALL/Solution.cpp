//===========================//
// Solution class definition //
//===========================//

#include <algorithm>
#include <climits>
#include "Solution.hpp"

// minMoves_v1
// SLOW - NOT ACCEPTED

int Solution::minMoves_v1(std::vector<int>& nums)
{
    // get the size of the vector
    const unsigned int DIM(nums.size());

    // corner case
    if(DIM == 0) return 0;
    if(DIM == 1) return 0;

    // corner case
    std::vector<int> tmp(nums);
    auto it = std::unique(tmp.begin(), tmp.end());
    tmp.resize(std::distance(tmp.begin(),it));
    if(tmp.size() == 1) {
        return 0;
    }

    // sort the vector and increase
    int counter(0);
    while(true) {
        std::sort(nums.begin(), nums.end());
        for(unsigned int i = 0; i != DIM-1; i++) {
            nums[i]++;
        }
        std::vector<int> tmp(nums);
        auto it = std::unique(tmp.begin(), tmp.end());
        tmp.resize(std::distance(tmp.begin(),it));
        counter++;
        if(tmp.size() == 1) {
            break;
        }
    }
    return counter;
}

// minMoves_v2

int Solution::minMoves_v2(std::vector<int>& nums)
{
    // get the size of the vector
    const unsigned int DIM(nums.size());

    // corner case
    if(DIM == 0) return 0;
    if(DIM == 1) return 0;

    // corner case
    std::vector<int> tmp(nums);
    auto it = std::unique(tmp.begin(), tmp.end());
    tmp.resize(std::distance(tmp.begin(),it));
    if(tmp.size() == 1) {
        return 0;
    }

    // sort the vector and increase
    int counter(0);
    while(true) {
        // get the maximum element's location and not increase it
        auto it_max = std::max_element(nums.begin(), nums.end());
        for(auto it = nums.begin(); it != nums.end(); it++) {
            if(it != it_max) (*it)++;
        }
        std::vector<int> tmp(nums);
        auto it = std::unique(tmp.begin(), tmp.end());
        tmp.resize(std::distance(tmp.begin(),it));
        counter++;
        if(tmp.size() == 1) {
            break;
        }
    }
    return counter;
}

// minMoves_v3

int Solution::minMoves_v3(std::vector<int>& nums)
{
    if(nums.empty()) {
        return 0;
    }

    const unsigned int DIM(nums.size());
    int sum_loc(0);
    int min_loc(INT_MAX);

    for(unsigned int i = 0; i != DIM; ++i) {
        sum_loc += nums[i];
        min_loc = std::min(min_loc, nums[i]);
    }

    return (sum_loc - min_loc * DIM);
}

// end
