//=======================================//
// minimum moves to equal array elements //
//=======================================//

#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// minMoves_v1
// minMoves_v2
// minMoves_v3

#define minMoves minMoves_v3

// the main function

int main()
{
    int res(0);
    Solution sol1;

    // # 1
    {
        std::vector<int> nums{1,2,3,4};
        res = sol1.minMoves(nums);
        cout << " res = " << res << endl;
    }

    // # 2
    {
        std::vector<int> nums{1,2,3,4,5,6,7,8,9,10,11};
        res = sol1.minMoves(nums);
        cout << " res = " << res << endl;
    }

    return 0;
}

// end
