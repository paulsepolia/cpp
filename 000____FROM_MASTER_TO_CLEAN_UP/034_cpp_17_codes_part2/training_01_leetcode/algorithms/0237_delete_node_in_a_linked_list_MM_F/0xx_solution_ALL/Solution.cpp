//===========================//
// Solution class definition //
//===========================//

#include "Solution.hpp"

// deleteNode_v1

void Solution::deleteNode_v1(ListNode * node)
{
    node->val = node->next->val;
    node->next = node->next->next;
}

// end
