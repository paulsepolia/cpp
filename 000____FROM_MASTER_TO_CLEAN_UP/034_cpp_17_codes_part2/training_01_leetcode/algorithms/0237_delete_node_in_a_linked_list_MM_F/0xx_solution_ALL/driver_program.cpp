//=====================//
// reverse linked list //
//=====================//

#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// deleteNode_v1

#define deleteNode deleteNode_v1

// the main function

int main()
{
    ListNode * nA(0);
    Solution sol1;
    sol1.deleteNode(nA);

    cout << " &nA = " << &nA << endl;

    return 0;
}

// end
