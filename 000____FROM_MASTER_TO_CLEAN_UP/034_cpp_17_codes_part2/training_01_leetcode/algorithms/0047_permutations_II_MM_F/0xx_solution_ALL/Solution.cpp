//===========================//
// Solution class definition //
//===========================//

#include <algorithm>
#include "Solution.hpp"

// permuteUnique_v1

std::vector<std::vector<int>> Solution::permuteUnique_v1(std::vector<int> & nums)
{
    std::vector<std::vector<int>> res{};
    std::sort(nums.begin(), nums.end());

    do {
        res.push_back(nums);
    } while(std::next_permutation(nums.begin(), nums.end()));


    return res;
}
// end
