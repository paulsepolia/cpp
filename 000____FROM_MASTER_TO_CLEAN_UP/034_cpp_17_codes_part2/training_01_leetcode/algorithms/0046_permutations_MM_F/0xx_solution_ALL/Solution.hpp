//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <vector>

class Solution {
public:

    // permute_v1
    std::vector<std::vector<int>> permute_v1(std::vector<int> &);
};

#endif

// end
