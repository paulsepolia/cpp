//===========================//
// Excel Sheet Column Number //
//===========================//

#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// titleToNumber_v1

#define titleToNumber titleToNumber_v1

// the main function

int main()
{
    Solution sol1;
    int res(0);
    std::string s("");

    // # 1
    cout << "-------------------------->> 1" << endl;
    s = "A";
    res = sol1.titleToNumber(s);
    cout << " s   = " << s << endl;
    cout << " res = " << res << endl;

    // # 2
    cout << "-------------------------->> 2" << endl;
    s = "B";
    res = sol1.titleToNumber(s);
    cout << " s   = " << s << endl;
    cout << " res = " << res << endl;

    // # 3
    cout << "-------------------------->> 3" << endl;
    s = "Z";
    res = sol1.titleToNumber(s);
    cout << " s   = " << s << endl;
    cout << " res = " << res << endl;

    // # 4
    cout << "-------------------------->> 4" << endl;
    s = "AA";
    res = sol1.titleToNumber(s);
    cout << " s   = " << s << endl;
    cout << " res = " << res << endl;

    // # 5
    cout << "-------------------------->> 5" << endl;
    s = "AB";
    res = sol1.titleToNumber(s);
    cout << " s   = " << s << endl;
    cout << " res = " << res << endl;

    // # 6
    cout << "-------------------------->> 6" << endl;
    s = "AZ";
    res = sol1.titleToNumber(s);
    cout << " s   = " << s << endl;
    cout << " res = " << res << endl;

    // # 7
    cout << "-------------------------->> 7" << endl;
    s = "BA";
    res = sol1.titleToNumber(s);
    cout << " s   = " << s << endl;
    cout << " res = " << res << endl;

    // # 8
    cout << "-------------------------->> 8" << endl;
    s = "BB";
    res = sol1.titleToNumber(s);
    cout << " s   = " << s << endl;
    cout << " res = " << res << endl;

    // # 9
    cout << "-------------------------->> 9" << endl;
    s = "BZ";
    res = sol1.titleToNumber(s);
    cout << " s   = " << s << endl;
    cout << " res = " << res << endl;


    // # 10
    cout << "-------------------------->> 10" << endl;
    s = "CA";
    res = sol1.titleToNumber(s);
    cout << " s   = " << s << endl;
    cout << " res = " << res << endl;

    // # 11
    cout << "-------------------------->> 11" << endl;
    s = "CZ";
    res = sol1.titleToNumber(s);
    cout << " s   = " << s << endl;
    cout << " res = " << res << endl;

    // # 12
    cout << "-------------------------->> 12" << endl;
    s = "ZZZZ";
    res = sol1.titleToNumber(s);
    cout << " s   = " << s << endl;
    cout << " res = " << res << endl;

}

// end
