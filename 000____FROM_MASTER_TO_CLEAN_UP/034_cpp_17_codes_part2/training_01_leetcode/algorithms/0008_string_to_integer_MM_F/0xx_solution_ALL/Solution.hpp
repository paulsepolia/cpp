//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <string>

using std::string;

class Solution {
public:

    // stringToInt

    int stringToInt_v1(string);
};

#endif

//=====//
// end //
//=====//
