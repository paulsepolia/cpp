//===========================//
// Solution class definition //
//===========================//

#include <string>
#include <cmath>
#include <limits>
int imax = std::numeric_limits<int>::max();

using std::string;
using std::pow;
using std::numeric_limits;

#include "Solution.hpp"

#include <iostream>
using std::cout;
using std::endl;

// stringToInt

int Solution::stringToInt_v1(string str)
{
    const unsigned int SIZE_STR_TMP(str.length());
    bool isNegative(false);
    const unsigned int IMAX = numeric_limits<int>::max();
    const unsigned int TEN = 10;

    // step # 1
    // if the string is the null string

    if(SIZE_STR_TMP == 0) {
        return 0;
    }

    // step # 2a
    // if the string has a minus
    // capture it and set the flag to true
    // and drop it

    const char CHAR_MINUS('-');

    if(str[0] == CHAR_MINUS) {
        isNegative = true;
        str = str.substr(1, SIZE_STR_TMP);
    }

    // step # 2b
    // if the string has a plus
    // capture it and set the flag to true
    // and drop it

    const char CHAR_PLUS('+');

    if(str[0] == CHAR_PLUS) {
        isNegative = false;
        str = str.substr(1, SIZE_STR_TMP);
    }

    const unsigned int SIZE_STR(str.length());

    // step # 3
    // overflow case for negative

    if(isNegative && (SIZE_STR > TEN)) {
        return 0;
    }

    // step # 4
    // overflow case for positive

    if(!isNegative && (SIZE_STR > TEN)) {
        return 0;
    }

    // step # 5
    // convert the string to unsigned long int

    unsigned long int iL(0);
    unsigned long int sum(0);

    for(unsigned int i = 0; i < SIZE_STR; i++) {
        iL = (str[i]-'0') * pow(10.0, SIZE_STR-i-1);
        sum = sum + iL;
    }

    // step # 6
    // overflow case for positive

    if((sum > IMAX) && !isNegative) {
        return 0;
    }

    // step # 7
    // overflow case for negative

    if((sum-1 > IMAX) && isNegative) {
        return 0;
    }

    if(isNegative) {
        sum = -sum;
    }

    return sum;
}

//=====//
// end //
//=====//
