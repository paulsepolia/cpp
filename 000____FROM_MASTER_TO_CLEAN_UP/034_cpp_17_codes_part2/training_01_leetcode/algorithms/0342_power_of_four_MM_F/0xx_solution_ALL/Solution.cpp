//===========================//
// Solution class definition //
//===========================//

#include <cmath>
#include "Solution.hpp"

// isPowerOfFour_v1

bool Solution::isPowerOfFour_v1(int num)
{
    bool res(false);
    int tmp(num);

    // corner cases

    if(num <= 0) {
        return false;
    }

    if(num == 1) {
        return true;
    }

    // main algo

    int counter(0);
    if(num%4 == 0) {
        while(num /= 4) {
            counter++;
        }
        if(static_cast<int>(std::pow(4.0, counter)) == tmp) {
            res = true;
        }
    } else {
        res = false;
    }

    return res;
}

// end
