//===========================//
// Solution class definition //
//===========================//

#include <cmath>
#include "Solution.hpp"

// isPowerOfTwo_v1

bool Solution::isPowerOfTwo_v1(int num)
{
    bool res(false);
    int tmp(num);

    // corner cases

    if(num <= 0) {
        return false;
    }

    if(num == 1) {
        return true;
    }

    // main algo

    int counter(0);
    if(num%2 == 0) {
        while(num /= 2) {
            counter++;
        }
        if(static_cast<int>(std::pow(2.0, counter)) == tmp) {
            res = true;
        }
    } else {
        res = false;
    }

    return res;
}

// end
