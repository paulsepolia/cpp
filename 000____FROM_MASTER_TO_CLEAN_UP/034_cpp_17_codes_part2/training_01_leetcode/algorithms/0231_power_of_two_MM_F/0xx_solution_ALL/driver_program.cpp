//==============//
// power of two //
//==============//

#include <iomanip>
#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// isPowerOfTwo_v1

#define isPowerOfTwo isPowerOfTwo_v1

// the main function

int main()
{
    Solution sol1;
    bool res(false);
    int num(0);

    cout << std::fixed;
    cout << std::boolalpha;

    // # 1

    num = 2;
    res = sol1.isPowerOfTwo(num);
    cout << " num = " << num << endl;
    cout << " res = " << res << endl;

    // # 2

    num = 8;
    res = sol1.isPowerOfTwo(num);
    cout << " num = " << num << endl;
    cout << " res = " << res << endl;

    // # 3

    num = 7;
    res = sol1.isPowerOfTwo(num);
    cout << " num = " << num << endl;
    cout << " res = " << res << endl;

    // # 4

    num = 1;
    res = sol1.isPowerOfTwo(num);
    cout << " num = " << num << endl;
    cout << " res = " << res << endl;

    return 0;
}

// end
