//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

class Solution {
public:

    //  isPowerOfTwo_v1
    bool isPowerOfTwo_v1(int);
};

#endif

// end
