//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <vector>

class Solution {
public:

    // findDuplicates_v1
    std::vector<int> findDuplicates_v1(std::vector<int>&);

    // findDuplicates_v2
    std::vector<int> findDuplicates_v2(std::vector<int>&);

    // findDuplicates_v3
    std::vector<int> findDuplicates_v3(std::vector<int>&);
};

#endif

// end
