//===========================//
// Solution class definition //
//===========================//

#include <algorithm>
#include "Solution.hpp"

// findDuplicates_v1
// with extra space
// complexity N*log(N)

std::vector<int> Solution::findDuplicates_v1(std::vector<int> & nums)
{
    std::vector<int> res{};
    const unsigned int DIM(nums.size());

    // corner case

    if(DIM == 0) {
        return std::vector<int> {};
    }

    // sort the vector

    std::sort(nums.begin(), nums.end());

    // iterate the vector and get the duplicates

    for(unsigned int i = 0; i != DIM-1; i++) {
        if(nums[i] == nums[i+1]) {
            res.push_back(nums[i]);
        }
    }

    return res;
}

// findDuplicates_v2
// no extra space
// complexity N*log(N)

std::vector<int> Solution::findDuplicates_v2(std::vector<int> & nums)
{
    const unsigned int DIM(nums.size());

    // corner case
    if(DIM == 0) {
        return std::vector<int> {};
    }

    // sort the vector
    std::sort(nums.begin(), nums.end());

    // iterate the vector and get the duplicates
    unsigned int j(0);
    for(unsigned int i = 0; i != DIM-1; i++) {
        if(nums[i] == nums[i+1]) {
            nums[j] = nums[i];
            j++;
        }
    }

    // erase the elements after the j-th element
    nums.erase(nums.begin()+j, nums.end());

    return nums;
}

// findDuplicates_v3
// no extra space
// complexity N*log(N)

std::vector<int> Solution::findDuplicates_v3(std::vector<int> & nums)
{
    const unsigned int DIM(nums.size());

    // corner case
    if(DIM == 0) {
        return std::vector<int> {};
    }

    // sort the vector
    std::sort(nums.begin(), nums.end());

    // iterate the vector and get the duplicates
    unsigned int j(0);
    for(unsigned int i = 0; i != DIM-1; i++) {
        if(nums[i] == nums[i+1]) {
            nums[j] = nums[i];
            j++;
        }
    }

    // erase the elements after the j-th element
    // by resizing the vector
    nums.resize(j);

    return nums;
}

// end
// end
