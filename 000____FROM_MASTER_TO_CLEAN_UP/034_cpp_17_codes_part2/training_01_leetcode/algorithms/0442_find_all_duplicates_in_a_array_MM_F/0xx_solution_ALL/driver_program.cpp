//=============================//
// find duplicates in a vector //
//=============================//

#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// findDuplicates_v1
// findDuplicates_v2
// findDuplicates_v3

#define findDuplicates findDuplicates_v3

// the main function

int main()
{
    Solution sol1;
    std::vector<int> res{};

    {
        // # 1
        cout << "------------------------------------>> 1" << endl;
        std::vector<int> nums = {1,2,3,4,5,5,6};
        res = sol1.findDuplicates(nums);
        for(auto & i: res) {
            cout << i << " ";
        }
        cout << endl;
    }

    {
        // # 2
        cout << "------------------------------------>> 2" << endl;
        std::vector<int> nums = {1,1,2,2,3,3,4,4,5,5,6};
        res = sol1.findDuplicates(nums);
        for(auto & i: res) {
            cout << i << " ";
        }
        cout << endl;
    }

    {
        // # 3
        cout << "------------------------------------>> 3" << endl;
        std::vector<int> nums = {1,2,1,2,3,4,3,4,5,5,6,6};
        res = sol1.findDuplicates(nums);
        for(auto & i: res) {
            cout << i << " ";
        }
        cout << endl;
    }


}

// end
