//===========================//
// Solution class definition //
//===========================//

#include <algorithm>
#include "Solution.hpp"


// sortColors_v1

void Solution::sortColors_v1(std::vector<int> & nums)
{
    std::sort(nums.begin(), nums.end());
}

// sortColors_v2

void Solution::sortColors_v2(std::vector<int> & nums)
{
    const unsigned int DIM(nums.size());
    std::vector<int> red{};
    std::vector<int> white{};
    std::vector<int> blue{};
    for(unsigned int i = 0; i != DIM; i++) {
        if(nums[i] == 0) red.push_back(0);
        if(nums[i] == 1) white.push_back(1);
        if(nums[i] == 2) blue.push_back(2);
    }
    for(unsigned int i = 0; i != red.size(); i++) {
        nums[i] = 0;
    }
    for(unsigned int i = red.size(); i != red.size()+white.size(); i++) {
        nums[i] = 1;
    }
    for(unsigned int i = red.size()+white.size(); i != DIM; i++) {
        nums[i] = 2;
    }
}

// end
