//=============//
// sort colors //
//=============//

#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// sortColors_v1
// sortColors_v2

#define sortColors sortColors_v2

// the main function

int main()
{
    Solution sol1;

    // # 1
    cout << "-------------------------->> 1" << endl;
    std::vector<int> nums{1,2,0,1,2,0,1,1,1,2};
    sol1.sortColors(nums);
    for(const auto &i: nums) {
        cout << i << " ";
    }
    cout << endl;
}

// end
