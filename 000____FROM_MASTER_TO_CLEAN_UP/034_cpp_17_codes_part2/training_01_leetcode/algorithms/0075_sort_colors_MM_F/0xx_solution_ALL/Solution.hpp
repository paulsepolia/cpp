//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <vector>

class Solution {
public:

    // sortColors_v1
    void sortColors_v1(std::vector<int>&);

    // sortColors_v2
    void sortColors_v2(std::vector<int>&);
};

#endif

// end
