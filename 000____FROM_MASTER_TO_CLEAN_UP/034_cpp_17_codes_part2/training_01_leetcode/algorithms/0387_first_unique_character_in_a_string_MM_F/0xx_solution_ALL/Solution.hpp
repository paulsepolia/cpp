//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <string>

class Solution {
public:

    // firstUniqChar_v1
    int firstUniqChar_v1(std::string);
};

#endif

// end
