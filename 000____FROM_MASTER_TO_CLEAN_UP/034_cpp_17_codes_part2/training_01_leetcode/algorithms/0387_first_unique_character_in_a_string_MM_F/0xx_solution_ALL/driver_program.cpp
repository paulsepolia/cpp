//====================================//
// first unique character in a string //
//====================================//

#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// firstUniqChar_v1

#define firstUniqChar firstUniqChar_v1

// the main function

int main()
{
    Solution sol1;
    std::string s{};
    int res(0);

    // # 1
    cout << "------------------------------------>> 1" << endl;
    s = "1234567890";
    res = sol1.firstUniqChar(s);
    cout << " res  = " << res << endl;
    cout << " char = " << s[res] << endl;

    // # 2
    cout << "------------------------------------>> 2" << endl;
    s = "111120000";
    res = sol1.firstUniqChar(s);
    cout << " res  = " << res << endl;
    cout << " char = " << s[res] << endl;

    // # 3
    cout << "------------------------------------>> 2" << endl;
    s = "111100032";
    res = sol1.firstUniqChar(s);
    cout << " res  = " << res << endl;
    cout << " char = " << s[res] << endl;

    return 0;
}

// end
