//===========================//
// Solution class definition //
//===========================//

#include <vector>
#include <algorithm>
#include "Solution.hpp"

// sortList_v1

ListNode * Solution::sortList_v1(ListNode * head)
{
    // corner case
    if(head == 0) return 0;

    // get the head
    ListNode * const HEAD(head);

    // put the values in a vector
    std::vector<int> vecA{};
    while(head != 0) {
        vecA.push_back(head->val);
        head = head->next;
    }

    // sort the vector
    std::sort(vecA.begin(), vecA.end());

    // put back the values in the linked list
    head = HEAD;
    unsigned int i(0);
    while(head != 0) {
        head->val = vecA[i];
        head = head->next;
        ++i;
    }

    // reset and return
    head = HEAD;
    return head;
}

// sortList_v2

ListNode * Solution::sortList_v2(ListNode * head)
{
    return head;
}

// end
