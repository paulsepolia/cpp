//===========================//
// Solution class definition //
//===========================//

#include <algorithm>
#include "Solution.hpp"

// singleNumberIII_v1

std::vector<int> Solution::singleNumberIII_v1(std::vector<int> & nums)
{
    std::vector<int> res{};
    const unsigned int DIM(nums.size());

    // sort
    std::sort(nums.begin(), nums.end());

    // check if the numbers are not in the edges
    for(unsigned int i = 1; i != DIM-1; i++) {
        if(nums[i] != nums[i+1] && nums[i] != nums[i-1]) {
            res.push_back(nums[i]);
        }
    }

    // if yes return
    if(res.size() == 2) {
        return res;
    }

    // if only one then get the other
    if(res.size() == 1) {
        if(nums[DIM-2] != nums[DIM-1]) {
            res.push_back(nums[DIM-1]);
        } else {
            res.push_back(nums[0]);
        }
    }

    // if none then get the edges
    if(res.size() == 0) {
        res.push_back(nums[DIM-1]);
        res.push_back(nums[0]);
    }

    return res;
}

// end
