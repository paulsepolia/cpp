//===========================//
// Solution class definition //
//===========================//

#include <algorithm>
#include "Solution.hpp"

// setMatrixZeroes_v1

void Solution::setMatrixZeroes_v1(std::vector<std::vector<int>> & matrix)
{
    const unsigned int DIM1(matrix.size());
    const unsigned int DIM2(matrix[0].size());

    // get all rows and columns that should be zero

    std::vector<int> rows {};
    std::vector<int> cols {};

    for(unsigned int i = 0; i != DIM1; i++) {
        for(unsigned int j = 0; j != DIM2; j++) {
            if(matrix[i][j] == 0) {
                rows.push_back(i);
                cols.push_back(j);
            }
        }
    }

    // set to zero all the above detected rows

    for(unsigned int k = 0; k != rows.size(); k++) {
        for(unsigned int j = 0; j != DIM2; j++) {
            matrix[rows[k]][j] = 0;
        }
    }

    // set to zero all the above detected columns

    for(unsigned int k = 0; k != cols.size(); k++) {
        for(unsigned int j = 0; j != DIM1; j++) {
            matrix[j][cols[k]] = 0;
        }
    }
}

// setMatrixZeroes_v2

void Solution::setMatrixZeroes_v2(std::vector<std::vector<int>> & matrix)
{
    const unsigned int DIM1(matrix.size());
    const unsigned int DIM2(matrix[0].size());

    bool col0 = false;

    // # 0

    for(unsigned int i = 0; i != DIM1; i++) {
        if(matrix[i][0] == 0) {
            col0 = true;
        }
    }

    // # 1
    // Scan the matrix to mark all zeroes.

    // How the marking is done:
    // Scan each row, if a zero is detected then mark the
    // row as to become zero by setting the 1st element of it to zero
    // Do that for each row.
    // The zeroes in positions other than the 1st are marks
    // for the columns to be set to zero

    for(unsigned int i = 0; i != DIM1; i++) {
        for(unsigned int j = 0; j != DIM2; j++) {
            if(matrix[i][j] == 0) {
                matrix[i][0] = 0;
            }
        }
    }

    // # 2
    // Use the above marking to set columns to zero
    // How the scanning is done:
    // Scan each row, NOT the first element of it
    // If a zero is detected set the row to zero
    // Do that for all rows

    for(unsigned int i = 0; i != DIM1; i++) {
        for(unsigned int j = 1; j != DIM2; j++) {
            if(matrix[i][j] == 0) {
                for(unsigned int k = 0; k != DIM1; k++) {
                    matrix[k][j] = 0;
                }
            }
        }
    }

    // # 3
    // Use the above marking to set rows equal to zero
    // How the scanning is done:
    // Scan only the first column
    // if any zero is detected then set to zero the row

    for(unsigned int i = 0; i != DIM1; i++) {
        if(matrix[i][0] == 0) {
            for(unsigned int k = 0; k != DIM2; k++) {
                matrix[i][k] = 0;
            }
        }
    }

    // # 4

    if(col0) {
        for(unsigned int i = 0; i != DIM1; i++) {
            matrix[i][0] = 0;
        }
    }
}

// end
