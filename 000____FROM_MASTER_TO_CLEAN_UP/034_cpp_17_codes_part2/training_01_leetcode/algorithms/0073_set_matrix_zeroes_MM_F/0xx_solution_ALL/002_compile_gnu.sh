#!/bin/bash

  # 1. compile

  g++  -O3                \
       -Wall              \
       -std=gnu++17       \
	   Solution.cpp       \
       driver_program.cpp \
       -o x_gnu
