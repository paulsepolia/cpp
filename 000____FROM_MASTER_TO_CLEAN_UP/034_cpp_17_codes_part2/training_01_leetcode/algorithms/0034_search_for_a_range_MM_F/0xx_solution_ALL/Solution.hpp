//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <vector>

class Solution {
public:

    // searchRange_v1
    std::vector<int> searchRange_v1(std::vector<int>&, int);
};

#endif

// end
