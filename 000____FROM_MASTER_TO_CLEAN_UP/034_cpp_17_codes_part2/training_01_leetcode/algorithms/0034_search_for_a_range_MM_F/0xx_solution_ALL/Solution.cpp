//===========================//
// Solution class definition //
//===========================//

#include <algorithm>
#include "Solution.hpp"

#include <iostream>
using std::endl;
using std::cout;

// searchRange_v1

std::vector<int> Solution::searchRange_v1(std::vector<int>& nums, int target)
{
    const unsigned int DIM(nums.size());
    int counter(0);
    std::vector<int> res{};
    unsigned int index(0);

    for(unsigned int i = 0; i != DIM; i++) {
        if(nums[i] == target) {
            counter++;
            if(counter == 1) index = i;
        }
    }

    if(counter == 0) {
        res.push_back(-1);
        res.push_back(-1);
    } else {
        res.push_back(index);
        res.push_back(index+counter-1);
    }

    return res;
}


// end
