//=============//
// searh range //
//=============//

#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// searchRange_v1

#define searchRange searchRange_v1

// the main function

int main()
{
    Solution sol1;
    std::vector<int> res{};

    // # 1
    cout << "---------------------------------------------------------------->> 1" << endl;
    {
        std::vector<int> nums{5, 7, 7, 8, 8, 10};
        int target(8);
        res = sol1.searchRange(nums, target);
        for(const auto & i: res) {
            cout << i << " ";
        }
        cout << endl;
    }

    // # 2
    cout << "---------------------------------------------------------------->> 2" << endl;
    {
        std::vector<int> nums{5, 7, 7, 8, 8, 10};
        int target(11);
        res = sol1.searchRange(nums, target);
        for(const auto & i: res) {
            cout << i << " ";
        }
        cout << endl;
    }

    return 0;
}

// end
