//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <vector>

class Solution {
public:

    // # 1
    int remove_element_v1(std::vector<int>&, int);

    // # 2
    int remove_element_v2(std::vector<int>&, int);
};

#endif

//=====//
// end //
//=====//
