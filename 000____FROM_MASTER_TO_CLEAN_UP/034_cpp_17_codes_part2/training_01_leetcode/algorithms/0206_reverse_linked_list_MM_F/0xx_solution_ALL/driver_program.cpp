//=====================//
// reverse linked list //
//=====================//

#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// reverseList_v1
// reverseList_v2
// reverseList_v3

#define reverseList reverseList_v3

// the main function

int main()
{
    ListNode * nA(0);
    ListNode * res(0);
    Solution sol1;
    res = sol1.reverseList(nA);

    cout << " &res = " << &res << endl;

    return 0;
}

// end
