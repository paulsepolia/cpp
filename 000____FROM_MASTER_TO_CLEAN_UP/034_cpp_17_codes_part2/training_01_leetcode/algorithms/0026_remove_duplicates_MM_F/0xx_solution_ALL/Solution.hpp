//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <vector>

class Solution {
public:

    // # 1
    int remove_duplicates_v1(std::vector<int>&);

    // # 2
    int remove_duplicates_v2(std::vector<int>&);
};

#endif

//=====//
// end //
//=====//
