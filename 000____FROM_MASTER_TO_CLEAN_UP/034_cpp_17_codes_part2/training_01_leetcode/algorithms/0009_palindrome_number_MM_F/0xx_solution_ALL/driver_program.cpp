//===================//
// palindrome number //
//===================//
//==========================================================//
// The NoSTL version is 10-20 times faster than the STL one //
//==========================================================//

#include <iostream>
#include <iomanip>
#include <cmath>
#include <chrono>

using std::cout;
using std::endl;
using std::boolalpha;
using std::setw;
using std::fixed;
using std::setprecision;
using std::right;
using std::pow;
using namespace std::chrono;

#include "Solution.hpp"

// macros
// allowed names are:
// isPalindromeNoSTL
// isPalindromeSTL

#define isPalindrome isPalindromeNoSTL

// the main function

int main()
{
    // parameters

    const int I_MAX(static_cast<int>(pow(10.0, 5)));
    const int TRIALS(static_cast<int>(pow(10.0, 3)));
    const int SIZE_INT(15);

    // input fixed and local variables

    bool res(false);
    int pal_counter(0);
    int x1(0);
    cout << boolalpha;
    cout << setprecision(10);
    Solution sol1;
    auto t1 = system_clock::now();
    auto t2 = system_clock::now();
    duration<double> time_span;

    // input # 1

    x1 = 123456;
    res = sol1.isPalindrome(x1);
    cout << " --> 1  --> input " << setw(SIZE_INT) << right << x1
         << " --> " << " result = " << res << endl;

    // input # 2

    x1 = 12344321;
    res = sol1.isPalindrome(x1);
    cout << " --> 2  --> input " << setw(SIZE_INT) << right << x1
         << " --> " << " result = " << res << endl;

    // input # 3

    x1 = 0;
    res = sol1.isPalindrome(x1);
    cout << " --> 3  --> input " << setw(SIZE_INT ) << right << x1
         << " --> " << " result = " << res << endl;

    // input # 4
    // negative

    x1 = -1;
    res = sol1.isPalindrome(x1);
    cout << " --> 4  --> input " << setw(SIZE_INT) << right << x1
         << " --> " << " result = " << res << endl;

    // input # 5
    // overflow

    x1 = 123456789123456789;
    res = sol1.isPalindrome(x1);
    cout << " --> 5  --> input " << setw(SIZE_INT) << right << x1
         << " --> " << " result = " << res << endl;

    // input # 6
    // the benchmark
    // find all the palindorme numbers in a given range

    t1 = system_clock::now();

    for(int k = 0; k != TRIALS; k++) {
        // reset the counter

        pal_counter = 0;

        // count here

        for(int i = 0; i != I_MAX; i++) {
            if(sol1.isPalindrome(i)) {
                pal_counter++;
            }
        }
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);

    cout << " --> 6a --> total palindrome numbers in the range 0 to " << I_MAX
         << " is " << pal_counter << endl;
    cout << " --> 6b --> total time used for " << TRIALS << " loops is "
         << fixed << time_span.count() << endl;

    return 0;
}

//=====//
// end //
//=====//
