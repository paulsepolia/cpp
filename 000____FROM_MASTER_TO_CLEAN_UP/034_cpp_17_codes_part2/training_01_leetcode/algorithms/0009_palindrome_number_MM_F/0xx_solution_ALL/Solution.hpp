//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

class Solution {
public:

    // isPalindromeSTL

    bool isPalindromeSTL(int);

    // isPalindromeNoSTL

    bool isPalindromeNoSTL(int);
};

#endif

//=====//
// end //
//=====//
