//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <string>

class Solution {
public:

    // convertToTitle_v1
    std::string convertToTitle_v1(int);
};

#endif

// end
