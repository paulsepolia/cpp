//===========================//
// Solution class definition //
//===========================//

#include <algorithm>
#include "Solution.hpp"

#include <iostream>
using std::cout;
using std::endl;

// cenvertToTitle_v1

std::string Solution::convertToTitle_v1(int num)
{
    std::string r("");

    do {
        num -= 1;
        std::string s(1, 'A'+num%26);
        r = s + r;
        num /= 26;
    } while (num != 0);

    return r;
}

// end
