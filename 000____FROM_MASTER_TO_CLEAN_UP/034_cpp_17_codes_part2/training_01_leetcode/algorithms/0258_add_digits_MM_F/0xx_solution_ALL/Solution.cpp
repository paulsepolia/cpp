//===========================//
// Solution class definition //
//===========================//

#include <vector>
#include <string>
#include "Solution.hpp"

// addDigits_v1

int Solution::addDigits_v1(int num)
{
    // # 1
    // convert integer to a vector of integers

    std::vector<int> vec {};
    vec.resize(0);
    std::string str {};

    str = std::to_string(num);
    vec.resize(str.size());

    for(unsigned int i = 0; i != str.size(); i++) {
        int loc(str[i]-'0');
        vec[i] = loc;
    }

    // # 2
    // get the sum

    int sum(0);

    for(unsigned int i = 0; i != vec.size(); i++) {
        sum = sum + vec[i];
    }

    // # 3
    // check and call recursively

    if(sum <= 9) {
        return sum;
    } else {
        vec.clear();
        vec.resize(0);
    }

    return addDigits_v1(sum);
}

// addDigits_v2

int Solution::addDigits_v2(int num)
{
    int sum(0);

    while(num) {
        sum = sum + num%10;
        num = num/10;
    }

    if(sum <= 9) {
        return sum;
    }

    return addDigits_v2(sum);
}

// addDigits_v3

int Solution::addDigits_v3(int num)
{
here:
    int sum(0);

    while(num) {
        sum = sum + num%10;
        num = num/10;
    }

    if(sum <= 9) {
        return sum;
    } else {
        num = sum;
        goto here;
    }

    return sum;
}

// end
