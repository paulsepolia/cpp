//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <vector>

using std::vector;

class Solution {
public:

    // #1

    double medianTwoSortedArraysSTL_v1(vector<int>&, vector<int>&);
};

#endif

//=====//
// end //
//=====//
