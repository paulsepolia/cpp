//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <vector>

class Solution {
public:

    // twoSumSTL_v1
    std::vector<int> twoSumSTL_v1(std::vector<int> &, int);

    // twoSumNoSTL_v1
    std::vector<int> twoSumNoSTL_v1(std::vector<int> &, int);

    // twoSum_v3
    std::vector<int> twoSum_v3(std::vector<int> &, int);
};

#endif

// end
