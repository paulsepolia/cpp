//===========================//
// Solution class definition //
//===========================//

#include <algorithm>
#include "Solution.hpp"

// twoSumSTL_v1

std::vector<int> Solution::twoSumSTL_v1(std::vector<int> & nums, int target)
{
    // local parameters

    const unsigned int VEC_SIZE(nums.size());

    // local variables

    std::vector<int> vec_res {0,0};
    std::vector<int>::iterator it;

    // --> step # 1
    // --> check if the vector is empty
    // --> if yes then return the {0,0} vector

    if(VEC_SIZE == 0) {
        return vec_res;
    }

    // --> step # 2
    // --> iterate over the given vector and try to find the missing number

    int index1(0);
    int pos(0);
    int num(0);

    for(unsigned int i = 0; i != VEC_SIZE-1; i++) {
        num = target - nums[i]; // get the missing number
        it = std::find(nums.begin()+i+1, nums.end(), num); // try to find the num in the rest vector

        if(it != nums.end()) {
            index1 = i;
            pos = it - nums.begin(); // store the position
            break;
        } else if (i == VEC_SIZE-1 && it == nums.end()) {
            return vec_res; // if not found return the {0,0} vector
        }
    }

    // --> step # 3
    // --> put the results in the vector

    vec_res[0] = index1;
    vec_res[1] = pos;

    return vec_res;
}

// twoSumNoSTL_v1

std::vector<int> Solution::twoSumNoSTL_v1(std::vector<int> & nums, int target)
{
    // local parameters

    const unsigned int VEC_SIZE(nums.size());

    // local variables

    std::vector<int> vec_res {0,0};
    std::vector<int>::iterator it;

    // --> step # 1
    // --> check if the vector is empty
    // --> if yes then return an empty vector

    if(VEC_SIZE == 0) {
        return vec_res;
    }

    // --> step # 2
    // --> iterate over the given vector and try to find the missing number

    int index1(0);
    int index2(0);
    int num1(0);
    int num2(0);

    for(unsigned int i = 0; i != VEC_SIZE-1; i++) {
        num1 = nums[i];
        for(unsigned int j = i+1; j != VEC_SIZE; j++) {
            num2 = target - num1;
            if(num2 == nums[j]) {
                index1 = i;
                index2 = j;
                break;
            } else if (i == VEC_SIZE-2 && j == VEC_SIZE-1) {
                return vec_res;
            }
        }
    }

    // --> step # 3
    // --> put the results in the vector

    vec_res[0] = index1;
    vec_res[1] = index2;

    return vec_res;
}

// twoSum_v3

std::vector<int> Solution::twoSum_v3(std::vector<int> & nums, int target)
{
    const unsigned int DIM(nums.size());
    // local variables
    std::vector<int> vec_res{};
    std::vector<int>::iterator it;
    // --> step # 1
    // --> check if the vector is empty
    // --> if yes then return an empty vector
    if(DIM == 0) {
        return vec_res;
    }
    // --> step # 2
    // --> iterate over the given vector and try to find the missing number
    int index1(0);
    int index2(0);
    int num1(0);
    int num2(0);
    bool flg(false);
    for(unsigned int i = 0; i < DIM; i++) {
        num1 = nums[i];
        for(unsigned int j = i+1; j < DIM; j++) {
            num2 = target - num1;
            if(num2 == nums[j]) {
                index1 = i;
                index2 = j;
                flg = true;
                break;
            }
        }
        if(flg) break;
    }
    // --> step # 3
    // --> put the results in the vector
    if(!flg) return vec_res;
    vec_res.push_back(index1);
    vec_res.push_back(index2);
    return vec_res;
}

// end
