//===========//
// path sum  //
//===========//

#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// haPathSum_v1

#define hasPathSum hasPathSum_v1

// the main function

int main()
{
    TreeNode * p(0);
    int sum(0);
    bool res(false);
    Solution sol1;
    res = sol1.hasPathSum(p, sum);
    cout << " res = " << res << endl;
    return 0;
}

// end
