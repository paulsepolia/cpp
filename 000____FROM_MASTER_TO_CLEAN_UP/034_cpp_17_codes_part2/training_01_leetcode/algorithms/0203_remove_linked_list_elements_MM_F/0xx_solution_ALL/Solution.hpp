//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

// ListNode definition

struct ListNode {
    int val;
    ListNode * next;
    ListNode(int x) : val(x), next(0) {}
};

// Solution declaration

class Solution {
public:

    // removeElements_v1
    ListNode * removeElements_v1(ListNode *, int);

    // removeElements_v2
    ListNode * removeElements_v2(ListNode *, int);
};

#endif

// end
