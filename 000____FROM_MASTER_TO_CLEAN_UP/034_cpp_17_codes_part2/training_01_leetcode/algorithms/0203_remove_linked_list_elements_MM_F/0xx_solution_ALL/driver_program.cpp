//===================================//
// remove element from a linked list //
//===================================//

#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// removeElements_v1
// removeElements_v2

#define removeElements removeElements_v2

// the main function

int main()
{
    ListNode * nA(0);
    int val(10);
    ListNode * res(0);
    Solution sol1;
    res = sol1.removeElements(nA, val);

    cout << " &res = " << &res << endl;

    return 0;
}

// end
