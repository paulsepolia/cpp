//===========================//
// Solution class definition //
//===========================//

#include <vector>
#include "Solution.hpp"

// removeElements_v1

ListNode * Solution::removeElements_v1(ListNode * list, int val)
{
    // save the head of the node
    ListNode * const head(list);

    // get the legth of the linked list
    unsigned int length(0);

    while(list != 0) {
        ++length;
        list = list->next;
    }

    // corner case
    if (length == 0) return 0;

    // put the elements in a vector
    std::vector<ListNode*> vec{};
    list = head;

    while(list != 0) {
        if(list->val != val) {
            vec.push_back(list);
        }
        list = list->next;
    }

    // corner case
    if(vec.size() == length) {
        list = head;
        return list;
    }

    // corner case
    if(vec.size() == 0) {
        return 0;
    }

    // build the linked list again using the elements of the vector
    // initialize the list

    ListNode * list_new(vec[0]);

    // set the next to the last to value null
    vec[vec.size()-1]->next = 0;
    unsigned int i(0);

    // build here the list
    while(vec[i]->next != 0) {
        ++i;
        list_new->next = vec[i];
        list_new = list_new->next;
    }

    // initialize again and return
    list_new = vec[0];

    return list_new;
}

// removeElements_v2

ListNode * Solution::removeElements_v2(ListNode * head, int val)
{
    // remove element at the front with value val
    while(head && (head->val == val)) {
        head = head->next;
    }

    ListNode * aNode(head);

    // remove the element with value val
    while(aNode && aNode->next) {
        if(aNode->next->val == val) {
            aNode->next = aNode->next->next;    // here happens the remove
        } else {
            aNode = aNode->next;
        }
    }

    return head;
}

// end
