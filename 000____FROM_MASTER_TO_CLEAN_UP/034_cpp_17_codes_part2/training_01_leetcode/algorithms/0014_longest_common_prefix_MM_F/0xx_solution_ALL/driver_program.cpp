//==============================//
// longest common prefix number //
//==============================//

#include <iostream>
#include <iomanip>
#include <cmath>
#include <chrono>

using std::cout;
using std::endl;
using std::boolalpha;
using std::setw;
using std::fixed;
using std::setprecision;
using std::right;
using std::pow;
using namespace std::chrono;

#include "Solution.hpp"

// macros
// allowed names are:

#define longestCommonPrefix longestCommonPrefix_v1

// the main function

int main()
{
    // local parameters

    const int VEC_SIZE(static_cast<int>(pow(10.0, 6.0)));
    const int STRING_SIZE(static_cast<int>(pow(10.0, 1.0)));
    const int TRIALS(static_cast<int>(pow(10.0, 2.0)));

    // local variables

    vector<string> v1 {};
    Solution sol;
    string str_res("");
    auto t1 = system_clock::now();
    auto t2 = system_clock::now();
    duration<double> time_span;

    cout << fixed;
    cout << setprecision(10);

    // input # 1

    v1.clear();
    v1.shrink_to_fit();
    v1.resize(3);

    v1[0] = "2";
    v1[1] = "2";
    v1[2] = "2";

    str_res = sol.longestCommonPrefix(v1);

    cout << " -->  1 --> must be = 2" << endl;
    cout << " -->  2 --> str_res = " << str_res << endl;

    // input # 2

    v1.clear();
    v1.shrink_to_fit();
    v1.resize(3);

    v1[0] = "22222";
    v1[1] = "33333";
    v1[2] = "44444";

    str_res = sol.longestCommonPrefix(v1);

    cout << " -->  3 --> must be = " << endl;
    cout << " -->  4 --> str_res = " << str_res << endl;

    // input # 3

    v1.clear();
    v1.shrink_to_fit();
    v1.resize(3);

    v1[0] = "22222";
    v1[1] = "222222";
    v1[2] = "2222223";

    str_res = sol.longestCommonPrefix(v1);

    cout << " -->  5 --> must be = 22222" << endl;
    cout << " -->  6 --> str_res = " << str_res << endl;

    // input # 4

    v1.clear();
    v1.shrink_to_fit();
    v1.resize(0);

    str_res = sol.longestCommonPrefix(v1);

    cout << " -->  7 --> must be = " << endl;
    cout << " -->  8 --> str_res = " << str_res << endl;

    // input # 5

    v1.clear();
    v1.shrink_to_fit();
    v1.resize(4);

    v1[0] = "122222";
    v1[1] = "1222222";
    v1[2] = "12222223";
    v1[3] = "12";

    str_res = sol.longestCommonPrefix(v1);

    cout << " -->  9 --> must be = 12" << endl;
    cout << " --> 10 --> str_res = " << str_res << endl;

    // input # 6

    v1.clear();
    v1.shrink_to_fit();
    v1.resize(4);

    v1[0] = "15";
    v1[1] = "14";
    v1[2] = "13";
    v1[3] = "12";

    str_res = sol.longestCommonPrefix(v1);

    cout << " --> 11 --> must be = 1" << endl;
    cout << " --> 12 --> str_res = " << str_res << endl;

    // input # 7
    // benchmark # A

    v1.clear();
    v1.shrink_to_fit();
    v1.resize(VEC_SIZE);
    string str_com("");

    t1 = system_clock::now();

    for(int k = 0; k != TRIALS; k++) {
        // build the string

        str_com.clear();
        str_com.shrink_to_fit();

        for(int i = 0; i != STRING_SIZE; i++) {
            str_com.push_back('1');
        }

        // build the vector

        for(int i = 0; i != VEC_SIZE; i++) {
            v1[i] = str_com;
        }

        str_res = sol.longestCommonPrefix(v1);
    }

    cout << " --> 13 --> must be = " << str_com << endl;
    cout << " --> 14 --> str_res = " << str_res << endl;

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);

    cout << " --> 15 --> total time used for " << TRIALS << " loops is "
         << fixed << time_span.count() << endl;

    // input # 8
    // benchmark # B

    v1.clear();
    v1.shrink_to_fit();
    v1.resize(VEC_SIZE);

    t1 = system_clock::now();

    // build the vector
    // and the string

    for(int i = 0; i != VEC_SIZE; i++) {

        str_com.clear();
        str_com.shrink_to_fit();

        for(int j = 0; j != STRING_SIZE; j++) {
            char ch(*std::to_string(i%10).c_str());
            str_com.push_back(ch);
        }

        v1[i] = str_com;
    }

    // apply the algorithm

    for(int k = 0; k != TRIALS; k++) {
        str_res = sol.longestCommonPrefix(v1);
    }

    cout << " --> 16 --> must be = " << endl;
    cout << " --> 17 --> str_res = " << str_res << endl;

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);

    cout << " --> 18 --> total time used for " << TRIALS << " loops is "
         << fixed << time_span.count() << endl;

    return 0;
}

//=====//
// end //
//=====//
