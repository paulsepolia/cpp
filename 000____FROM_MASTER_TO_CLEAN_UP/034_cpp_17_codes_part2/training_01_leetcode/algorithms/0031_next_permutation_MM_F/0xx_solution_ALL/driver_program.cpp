//=================//
// next permuation //
//=================//

#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// nextPermuation_v1

#define nextPermutation nextPermutation_v1

// the main function

int main()
{
    Solution sol1;

    {
        cout << "-------------------------------------->> 1" << endl;
        std::vector<int> nums{1,2,3,4};
        sol1.nextPermutation(nums);
        for(const auto & i: nums) {
            cout << i << " ";
        }
        cout << endl;
    }
}

// end
