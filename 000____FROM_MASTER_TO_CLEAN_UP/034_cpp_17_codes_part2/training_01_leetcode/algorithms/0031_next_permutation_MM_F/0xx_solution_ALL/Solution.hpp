//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <vector>

class Solution {
public:

    // nextPermutation_v1
    void nextPermutation_v1(std::vector<int> &);
};

#endif

// end
