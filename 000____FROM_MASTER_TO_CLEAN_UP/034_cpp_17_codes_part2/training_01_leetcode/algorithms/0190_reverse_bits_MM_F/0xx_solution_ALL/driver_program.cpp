//==============//
// reverse bits //
//==============//

#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// reverseBits_v1
// reverseBits_v2

#define reverseBits reverseBits_v2

// the main function

int main()
{
    Solution sol1;
    unsigned int n(0);
    unsigned int res(0);

    n = 111;
    res = sol1.reverseBits(n);
    cout << " n   = " << n << endl;
    cout << " res = " << res << endl;

    n = 43261596;
    res = sol1.reverseBits(n);
    cout << " n   = " << n << endl;
    cout << " res = " << res << endl;

    return 0;
}

// end
