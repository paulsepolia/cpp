//===========================//
// Solution class definition //
//===========================//

#include <string>
#include <bitset>
#include <algorithm>
#include "Solution.hpp"

// reverseBits_v1

unsigned int Solution::reverseBits_v1(unsigned int n)
{
    std::string tmp("");

    for(int i = 31; i >= 0; i--) {
        tmp.push_back((n%2)+'0');
        n >>= 1;
    }

    std::bitset<32> bVal(tmp);

    return bVal.to_ulong();
}

// reverseBits_v2

unsigned int Solution::reverseBits_v2(unsigned int n)
{
    // convert the integer to string of bits
    std::string binary(std::bitset<32>(n).to_string());

    // reverse the bits
    std::reverse(binary.begin(), binary.end());

    // convert back to decimal
    unsigned int decimal(std::bitset<32>(binary).to_ulong());

    return decimal;
}

