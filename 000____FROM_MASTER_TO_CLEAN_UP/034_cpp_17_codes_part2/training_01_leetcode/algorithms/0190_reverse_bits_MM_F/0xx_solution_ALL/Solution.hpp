//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <vector>

class Solution {
public:

    // reverseBits_v1
    unsigned int reverseBits_v1(unsigned int);

    // reverseBits_v2
    unsigned int reverseBits_v2(unsigned int);
};

#endif

// end
