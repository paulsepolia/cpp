//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <vector>

class Solution {
public:

    //  thirdMax_v1
    int thirdMax_v1(std::vector<int> &);

    //  thirdMax_v2
    int thirdMax_v2(std::vector<int> &);

};

#endif

// end
