//==============//
// power of two //
//==============//

#include <iomanip>
#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// thirdMax_v1

#define thirdMax thirdMax_v2

// the main function

int main()
{
    Solution sol1;
    int res(0);

    {
        cout << "---------------------------------------->> 1" << endl;
        std::vector<int> nums = {1,2,3};
        res = sol1.thirdMax(nums);
        cout << " 3rd max = " << res << endl;
    }

    {
        cout << "---------------------------------------->> 2" << endl;
        std::vector<int> nums = {2,2};
        res = sol1.thirdMax(nums);
        cout << " 3rd max = " << res << endl;
    }

    {
        cout << "---------------------------------------->> 3" << endl;
        std::vector<int> nums = {-2,-2,-2,3,4,4,4,4};
        res = sol1.thirdMax(nums);
        cout << " 3rd max = " << res << endl;
    }


    return 0;
}

// end
