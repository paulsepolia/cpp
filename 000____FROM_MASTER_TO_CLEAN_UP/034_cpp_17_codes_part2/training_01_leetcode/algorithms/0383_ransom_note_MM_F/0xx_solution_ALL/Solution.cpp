//===========================//
// Solution class definition //
//===========================//

#include <algorithm>
#include "Solution.hpp"

// canConstruct_v1

bool Solution::canConstruct_v1(std::string s1, std::string s2)
{
    bool res(true);
    std::string::iterator it;

    for(unsigned int i = 0; i != s1.size(); i++) {
        char ch(s1[i]);
        it = std::find(s2.begin(), s2.end(), ch);
        if(it != s2.end()) {
            s2.erase(it);
        } else {
            res = false;
            break;
        }
    }

    return res;
}

// end
