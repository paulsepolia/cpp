//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <string>

class Solution {
public:

    // canConstruct_v1
    bool canConstruct_v1(std::string, std::string);
};

#endif

// end
