//=============//
// Ransom note //
//=============//

#include <iomanip>
#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// canConstruct_v1

#define canConstruct canConstruct_v1

// the main function

int main()
{
    Solution sol1;
    bool res(false);
    cout << std::fixed;
    cout << std::boolalpha;
    std::string s1{};
    std::string s2{};

    // # 1
    cout << "---------------------------------->> 1" << endl;
    s1 = "aa";
    s2 = "aab";
    res = sol1.canConstruct(s1, s2);
    cout << " s1  = " << s1 << endl;
    cout << " s2  = " << s2 << endl;
    cout << " res = " << res << endl;

    // # 2
    cout << "---------------------------------->> 2" << endl;
    s1 = "aa";
    s2 = "ab";
    res = sol1.canConstruct(s1, s2);
    cout << " s1  = " << s1 << endl;
    cout << " s2  = " << s2 << endl;
    cout << " res = " << res << endl;

    // # 3
    cout << "---------------------------------->> 3" << endl;
    s1 = "bg";
    s2 = "efjbdfbdgfjhhaiigfhbaejahgfbbgbjagbddfgdiaigdadhcfcj";
    res = sol1.canConstruct(s1, s2);
    cout << " s1  = " << s1 << endl;
    cout << " s2  = " << s2 << endl;
    cout << " res = " << res << endl;

    return 0;
}

// end
