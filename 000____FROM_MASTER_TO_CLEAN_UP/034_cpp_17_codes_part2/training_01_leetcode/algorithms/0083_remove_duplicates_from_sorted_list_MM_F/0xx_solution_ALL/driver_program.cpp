//===============================================//
// delete duplicates from an ordered linked list //
//===============================================//

#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// deleteDuplicates_v1
// deleteDuplicates_v2
// deleteDuplicates_v3

#define deleteDuplicates deleteDuplicates_v3

// the main function

int main()
{
    ListNode * nA(0);
    ListNode * res(0);
    Solution sol1;
    res = sol1.deleteDuplicates(nA);

    cout << " &res = " << &res << endl;

    return 0;
}

// end
