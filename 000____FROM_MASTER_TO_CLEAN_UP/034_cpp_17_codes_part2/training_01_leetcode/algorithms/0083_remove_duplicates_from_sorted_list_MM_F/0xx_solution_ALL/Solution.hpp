//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

// ListNode definition

struct ListNode {
    int val;
    ListNode * next;
    ListNode(int x) : val(x), next(0) {}
};

// Solution declaration

class Solution {
public:

    // deleteDuplicates_v1
    ListNode * deleteDuplicates_v1(ListNode *);

    // deleteDuplicates_v2
    ListNode * deleteDuplicates_v2(ListNode *);

    // deleteDuplicates_v3
    ListNode * deleteDuplicates_v3(ListNode *);
};

#endif

// end
