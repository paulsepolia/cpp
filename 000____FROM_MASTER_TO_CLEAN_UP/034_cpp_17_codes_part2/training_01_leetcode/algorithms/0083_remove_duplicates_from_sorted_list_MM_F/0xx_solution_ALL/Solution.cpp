//===========================//
// Solution class definition //
//===========================//

#include <map>
#include "Solution.hpp"

// deleteDuplicates_v1

ListNode * Solution::deleteDuplicates_v1(ListNode * list)
{
    // get the head of the list
    ListNode * const head(list);

    // get the length of the linked list
    unsigned int length(0);
    while(list != 0) {
        ++length;
        list = list->next;
    }

    // corner case
    if(length == 0) return 0;

    // put the elements in a map
    // to get rid of duplicates in the key field of integers
    std::map<int, ListNode*> listMap{};
    list = head;
    while(list != 0) {
        listMap.insert(std::pair<int, ListNode*>(list->val,list));
        list = list->next;
    }

    // build the list again
    list = listMap.begin()->second;
    // initialize the linked list
    auto it = listMap.begin();
    for(it = ++it; it != listMap.end(); it++) {
        list->next = it->second;
        list = list->next;
    }

    // finalize it
    list->next = 0;
    // initialize and return
    list = head;

    return list;
}

// deleteDuplicates_v2

ListNode * Solution::deleteDuplicates_v2(ListNode * head)
{
    if(head) {
        // initialize the new linked lists
        ListNode* aPrev = head;
        ListNode* aCurr = head->next;

        while(aCurr) {
            // delete current node
            if(aPrev->val == aCurr->val) {
                aPrev->next = aCurr->next;
            } else {
                aPrev = aCurr;
            }
            aCurr = aPrev->next;
        }
    }
    return head;
}

// deleteDuplicates_v3

ListNode * Solution::deleteDuplicates_v3(ListNode * head)
{
    ListNode * const HEAD(list);
    ListNode * res(HEAD);

    // corner case
    if(list == 0) return 0;

    // add only the non duplicates
    while(list != 0 && list->next != 0) {
        if(list->val != list->next->val) {
            res->next = list->next;
            res = res->next;
        }
        list = list->next;
    }

    // finalize the new list
    res->next = 0;

    // initialize again and return
    res = HEAD;

    return res;
}

// end
