//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <vector>

class Solution {
public:
    // getRow_v1
    std::vector<int> getRow_v1(int);
};

#endif

// end
