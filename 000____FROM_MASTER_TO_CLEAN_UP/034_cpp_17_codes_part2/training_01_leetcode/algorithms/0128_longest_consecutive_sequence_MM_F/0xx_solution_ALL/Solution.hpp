//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <vector>

class Solution {
public:

    // longestConsecutive_v1
    int longestConsecutive_v1(std::vector<int>&);
};

#endif

// end
