//===========================//
// Solution class definition //
//===========================//

#include "Solution.hpp"
#include <algorithm>

// longestConsecutive_v1

int Solution::longestConsecutive_v1(std::vector<int>& nums)
{
    const unsigned int DIM(nums.size());
    if(DIM == 0) return 0;
    if(DIM == 1) return 1;
    std::sort(nums.begin(), nums.end());
    auto it(std::unique(nums.begin(), nums.end()));
    nums.resize(std::distance(nums.begin(),it));
    int counter(1);
    int max_loc(1);
    for(unsigned int i = 0; i < DIM-1; i++) {
        long int v1(nums[i]+1);
        long int v2(nums[i+1]);
        if(v1 == v2) {
            ++counter;
        } else {
            if(counter > max_loc) {
                max_loc = counter;
            }
            // reset
            counter = 1;
        }
    }
    return ((max_loc > counter) ? max_loc : counter);
}

// end
