//===============================//
// Longest Consecutive Sequence  //
//===============================//

#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// longestConsecutive_v1

#define longestConsecutive longestConsecutive_v1

// the main function

int main()
{
    Solution sol1;
    int res(0);

    {
        // # 1
        cout << "------------------------------------>> 1" << endl;
        std::vector<int> nums = {100, 4, 200, 1, 3, 2};
        res = sol1.longestConsecutive(nums);
        cout << " res = " << res << endl;
    }

    {
        // # 2
        cout << "------------------------------------>> 2" << endl;
        std::vector<int> nums = {6,5,4,3,2,1};
        res = sol1.longestConsecutive(nums);
        cout << " res = " << res << endl;
    }

}

// end
