//===========================//
// Solution class definition //
//===========================//

#include <vector>
#include <algorithm>
#include "Solution.hpp"

#include <iostream>
using std::endl;
using std::cout;

// addStrings_v1

std::string Solution::addStrings_v1(std::string num1, std::string num2)
{
    const unsigned int DIM1(num1.size());
    const unsigned int DIM2(num2.size());
    const unsigned int DIM3(DIM1 <= DIM2 ? DIM1 : DIM2);
    int tmp(0);

    // reverse
    std::reverse(num1.begin(), num1.end());
    std::reverse(num2.begin(), num2.end());

    std::vector<int> vec{};
    for(unsigned int i = 0; i != DIM3; i++) {
        tmp = (num1[i]-'0') + (num2[i]-'0');
        vec.push_back(tmp);
    }

    // add elemet by element
    if(DIM3 == DIM1) {
        for(unsigned int i = DIM1; i != DIM2; i++) {
            tmp =  num2[i]-'0';
            vec.push_back(tmp);
        }
    } else if(DIM3 == DIM2) {
        for(unsigned int i = DIM2; i != DIM1; i++) {
            tmp = num1[i]-'0';
            vec.push_back(tmp);
        }
    }

    // modify the integers larger than 10
    for(unsigned int i = 0; i != vec.size()-1; i++) {
        if(vec[i] >= 10) {
            vec[i] = vec[i] - 10;
            vec[i+1] = vec[i+1] + 1;
        }
    }

    // reverse
    std::reverse(vec.begin(), vec.end());

    // convert to string
    std::string res{""};

    for(auto & i: vec) {
        res = res + std::to_string(i);
    }

    return res;
}

// end
