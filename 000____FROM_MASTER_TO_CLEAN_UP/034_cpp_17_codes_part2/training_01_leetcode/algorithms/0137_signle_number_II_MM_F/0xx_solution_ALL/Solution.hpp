//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <vector>

class Solution {
public:

    // singleNumberII_v1
    int singleNumberII_v1(std::vector<int>&);
};

#endif

//=====//
// end //
//=====//
