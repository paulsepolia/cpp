//================//
// max difference //
// max profit II  //
//================//

#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// maxProfitII_v1

#define maxProfitII maxProfitII_v1

// the main function

int main()
{
    Solution sol1;
    int res(0);

    {
        // # 1
        cout << "------------------------------------>> 1" << endl;
        std::vector<int> nums = {1,2,3,4,5,5,6};
        res = sol1.maxProfitII(nums);
        cout << " res = " << res << endl;
    }

    {
        // # 2
        cout << "------------------------------------>> 2" << endl;
        std::vector<int> nums = {6,5,4,3,2,1};
        res = sol1.maxProfitII(nums);
        cout << " res = " << res << endl;
    }

}

// end
