//=============//
// add strings //
//=============//

#include <iomanip>
#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// majority_v1

#define majority majority_v1

// the main function

int main()
{
    Solution sol1;
    int res(0);

    // # 1
    {
        cout << "---------------------------------------------------------------->> 1" << endl;
        std::vector<int> vec{1,2,3,3,3,3,3,4};
        res = sol1.majority(vec);
        cout << " major = " << res << endl;
    }

    // # 2
    {
        cout << "---------------------------------------------------------------->> 1" << endl;
        std::vector<int> vec{1,2,3,3,3,3,3,4,4,4,3};
        res = sol1.majority(vec);
        cout << " major = " << res << endl;
    }

    return 0;
}

// end
