//===========================//
// Solution class definition //
//===========================//

#include "Solution.hpp"

// generate_v1

std::vector<std::vector<int>> Solution::generate_v1(int numRows)
{
    std::vector<std::vector<int>> vec{};
    // constants
    const std::vector<int> R1{1};
    const std::vector<int> R2{1,1};
    const std::vector<int> R3{1,2,1};
    // corner cases
    if(numRows == 0) return std::vector<std::vector<int>> {};
    if(numRows < 0) numRows = -numRows;
    if(numRows == 1) {
        vec.push_back(R1);
        return vec;
    }
    if(numRows == 2) {
        vec.push_back(R1);
        vec.push_back(R2);
        return vec;
    }
    if(numRows == 3) {
        vec.push_back(R1);
        vec.push_back(R2);
        vec.push_back(R3);
        return vec;
    }
    // rest cases
    vec.push_back(R1);
    vec.push_back(R2);
    vec.push_back(R3);
    std::vector<int> tmp{};
    for(int i(4); i <= numRows; i++) {
        tmp.push_back(1);
        for(unsigned int j = 0; j != vec[i-2].size()-1; j++) {
            tmp.push_back(vec[i-2][j]+vec[i-2][j+1]);
        }
        tmp.push_back(1);
        vec.push_back(tmp);
        tmp.clear();
        tmp.shrink_to_fit();
    }
    return vec;
}

// end
