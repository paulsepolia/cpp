//=================//
// version compare //
//=================//

#include <iostream>
#include <iomanip>

using std::cout;
using std::endl;
using std::fixed;
using std::right;
using std::showpos;
using std::setprecision;

#include "Solution.hpp"

// macros
// allowed names are:

#define my_pow  my_pow_v4

// the main function

int main()
{
    Solution sol1;
    cout << fixed;
    cout << showpos;
    cout << setprecision(7);
    double res(0);
    double base_val(0);
    int exp_val(0);

    // # 1

    base_val = 10.0;
    exp_val = 0;
    res = sol1.my_pow(base_val, exp_val);
    cout << " -->  1" << endl;
    cout << " expected = " << "+1.0000000" << endl;
    cout << " res      = " << res << endl;

    // # 2

    base_val = 1.23456;
    exp_val = 2;
    res = sol1.my_pow(base_val, exp_val);
    cout << " -->  2" << endl;
    cout << " expected = " << "+1.5241384" << endl;
    cout << " res      = " << res << endl;

    // # 3

    base_val = 0.0;
    exp_val = 0;
    res = sol1.my_pow(base_val, exp_val);
    cout << " -->  3" << endl;
    cout << " expected = " << "+1.0000000" << endl;
    cout << " res      = " << res << endl;

    // # 4

    base_val = 0.0;
    exp_val = 2;
    res = sol1.my_pow(base_val, exp_val);
    cout << " -->  4" << endl;
    cout << " expected = " << "+0.0000000" << endl;
    cout << " res      = " << res << endl;

    // # 5

    base_val = 34.00515;
    exp_val = -3;
    res = sol1.my_pow(base_val, exp_val);
    cout << " -->  5" << endl;
    cout << " expected = " << "+0.0000254" << endl;
    cout << " res      = " << res << endl;

    // # 6

    base_val = 0.00001;
    exp_val = 2147483647;
    res = sol1.my_pow(base_val, exp_val);
    cout << " -->  6" << endl;
    cout << " expected = " << "+0.0000000" << endl;
    cout << " res      = " << res << endl;

    return 0;
}

//=====//
// end //
//=====//
