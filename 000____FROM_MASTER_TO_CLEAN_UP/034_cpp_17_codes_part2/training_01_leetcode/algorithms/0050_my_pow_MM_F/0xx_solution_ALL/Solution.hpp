//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

class Solution {
public:

    // my_pow_v1

    double my_pow_v1(double, int);

    // my_pow_v2

    double my_pow_v2(double, int);

    // my_pow_v3

    double my_pow_v3(double, int);

    // my_pow_v4

    double my_pow_v4(double, int);
};

#endif

//=====//
// end //
//=====//
