//========================//
// linked list with cycle //
//========================//

#include <iostream>
#include <iomanip>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// hasCycle_v1
// hasCycle_v2
// hasCycle_v3

#define hasCycle hasCycle_v3

// the main function

int main()
{
    ListNode * nA(0);
    bool res(false);
    cout << std::fixed;
    cout << std::boolalpha;
    Solution sol1;
    res = sol1.hasCycle(nA);

    cout << " res = " << res << endl;

    return 0;
}

// end
