//===========================//
// Solution class definition //
//===========================//

#include <vector>
#include <algorithm>
#include <set>
#include "Solution.hpp"

// hasCycle_v1

bool Solution::hasCycle_v1(ListNode * head)
{
    // corner cases
    if(head == 0) return false;
    if(head->next == 0) return false;

    // set a slow and a fast pointer
    ListNode * slow = head;
    ListNode * fast = head->next;

    while(fast != 0 && fast->next != 0) {
        if(fast == slow) {
            return true;
        }

        fast = fast->next->next;
        slow = slow->next;
    }

    return false;
}

// hasCycle_v2
// SLOW - NOT ACCEPTED

bool Solution::hasCycle_v2(ListNode * head)
{
    // corner cases
    if(head == 0) return false;
    if(head->next == 0) return false;

    // create a vector and put the pointers there
    // if you get same vaue break;
    std::vector<ListNode*> aVec{};
    ListNode * aNode = head;

    while(aNode != 0) {
        aVec.push_back(aNode);
        auto it = std::find(aVec.begin(), aVec.end(), aNode->next);
        if(it != aVec.end()) return true;
        aNode = aNode->next;
    }

    return false;
}

// hasCycle_v3

bool Solution::hasCycle_v3(ListNode * head)
{
    // corner cases
    if(head == 0) return false;
    if(head->next == 0) return false;

    // create a set and put the pointers there
    // check if the size is increazed after each insert
    std::set<ListNode*> aSet{};
    std::pair<std::set<ListNode*>::iterator, bool> ret;
    ListNode * aNode = head;

    while(aNode != 0) {
        ret = aSet.insert(aNode);
        if (ret.second == false) return true;
        aNode = aNode->next;
    }

    return false;
}

// end
