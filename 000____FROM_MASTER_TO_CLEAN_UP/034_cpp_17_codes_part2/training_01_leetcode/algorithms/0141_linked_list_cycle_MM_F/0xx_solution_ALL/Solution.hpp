//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

// ListNode definition

struct ListNode {
    int val;
    ListNode * next;
    ListNode(int x) : val(x), next(0) {}
};

// Solution declaration

class Solution {
public:

    // hasCycle_v1
    bool hasCycle_v1(ListNode *);

    // hasCycle_v2
    bool hasCycle_v2(ListNode *);

    // hasCycle_v3
    bool hasCycle_v3(ListNode *);
};

#endif

// end
