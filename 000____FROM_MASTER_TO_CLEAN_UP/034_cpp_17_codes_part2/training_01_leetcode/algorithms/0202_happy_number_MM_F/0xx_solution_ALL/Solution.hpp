//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <vector>

class Solution {
public:

    // happyNumber_v1
    bool happyNumber_v1(int);

    // happyNumber_v2
    bool happyNumber_v2(int);

    // happyNumber_v3
    bool happyNumber_v3(int);
};

#endif

//=====//
// end //
//=====//
