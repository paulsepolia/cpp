//==============//
// happy number //
//==============//

#include <iostream>
#include <string>
#include <iomanip>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// happyNumber_v1
// happyNumber_v2
// happyNumber_v3

#define happyNumber happyNumber_v3

// the main function

int main()
{
    Solution sol1;
    bool res(false);
    int input(0);

    cout << std::boolalpha;

    // # 1

    input = 1234;
    cout << " --> 01" << endl;
    cout << " --> input = " << input << endl;
    res = sol1.happyNumber(input);
    cout << " --> res = " << res << endl;

    // # 2

    input = 1;
    cout << " --> 02" << endl;
    cout << " --> input = " << input << endl;
    res = sol1.happyNumber(input);
    cout << " --> res = " << res << endl;

    // # 3

    input = 19;
    cout << " --> 03" << endl;
    cout << " --> input = " << input << endl;
    res = sol1.happyNumber(input);
    cout << " --> res = " << res << endl;

    // # 4

    input = 1111111;
    cout << " --> 04" << endl;
    cout << " --> input = " << input << endl;
    res = sol1.happyNumber(input);
    cout << " --> res = " << res << endl;

    // # 5

    input = 13;
    cout << " --> 05" << endl;
    cout << " --> input = " << input << endl;
    res = sol1.happyNumber(input);
    cout << " --> res = " << res << endl;

    // # 6

    input = 28;
    cout << " --> 06" << endl;
    cout << " --> input = " << input << endl;
    res = sol1.happyNumber(input);
    cout << " --> res = " << res << endl;

    return 0;
}

//=====//
// end //
//=====//
