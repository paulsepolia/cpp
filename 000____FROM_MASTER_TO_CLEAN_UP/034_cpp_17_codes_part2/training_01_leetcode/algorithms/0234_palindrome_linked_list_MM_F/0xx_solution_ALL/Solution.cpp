//===========================//
// Solution class definition //
//===========================//

#include <vector>
#include <algorithm>
#include "Solution.hpp"

// isPalindrome_v1

bool Solution::isPalindrome_v1(ListNode* head)
{
    // corner case
    if(head == 0) return true;

    // put the values of the linked list in a vector
    std::vector<int> vecA{};
    while(head != 0) {
        vecA.push_back(head->val);
        head = head->next;
    }

    // make a copy of the vector and reverse
    std::vector<int> vecB(vecA);
    std::reverse(vecB.begin(), vecB.end());

    // compare
    bool res(false);
    res = std::equal(vecA.begin(), vecA.end(), vecB.begin());

    return res;
}

// isPalindrome_v2

bool Solution::isPalindrome_v2(ListNode* head)
{
    // corner case
    if(head == 0) return true;

    // put the values of the linked list in a vector
    std::vector<int> vecA{};
    while(head != 0) {
        vecA.push_back(head->val);
        head = head->next;
    }

    // make a copy of the vector and reverse
    std::vector<int> vecB(vecA);
    std::reverse(vecB.begin(), vecB.end());

    // compare
    bool res(false);
    res = vecA == vecB;

    return res;
}

// end
