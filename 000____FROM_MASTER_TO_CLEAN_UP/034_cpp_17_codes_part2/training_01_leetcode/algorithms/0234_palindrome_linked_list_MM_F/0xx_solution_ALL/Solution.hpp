//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

// ListNode definition

struct ListNode {
    int val;
    ListNode * next;
    ListNode(int x) : val(x), next(0) {}
};

// Solution declaration

class Solution {
public:

    // isPalindorme_v1
    bool isPalindrome_v1(ListNode*);

    // isPalindorme_v2
    bool isPalindrome_v2(ListNode*);
};

#endif

// end
