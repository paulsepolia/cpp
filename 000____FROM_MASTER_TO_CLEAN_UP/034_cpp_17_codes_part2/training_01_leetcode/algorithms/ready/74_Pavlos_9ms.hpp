class Solution {
public:
    bool searchMatrix(vector<vector<int>>& matrix, int target)
    {
        // get the number of rows
        const unsigned int ROWS(matrix.size());
        // get the number of columns
        const unsigned int COLS(matrix[0].size());
        // corner cases
        if(ROWS == 0) return false;
        // check if the target can be in the matrix
        if(matrix[0][0] > target) return false;
        if(matrix[ROWS-1][COLS-1] < target) return false;
        // get the index of the row which may contain the target
        unsigned int irow(0);
        for(unsigned int i = 0; i != ROWS; i++) {
            if(matrix[i][0] <= target && target <= matrix[i][COLS-1]) {
                irow = i;
                break;
            }
        }
        bool res(std::binary_search(matrix[irow].begin(), matrix[irow].end(), target));
        return res;
    }
};
