class Stack {
public:
    // Push element x onto stack
    void push(int x)
    {
        _queue.push(x);
    }
    // Removes the element on top of the stack
    void pop()
    {
        std::queue<int> queue_tmp{};
        while(_queue.size() != 1) {
            queue_tmp.push(_queue.front());
            _queue.pop();
        }
        _queue.pop();
        while(!queue_tmp.empty()) {
            _queue.push(queue_tmp.front());
            queue_tmp.pop();
        }
    }
    // Get the top element
    int top()
    {
        return _queue.back();
    }
    // Return whether the stack is empty
    bool empty()
    {
        return _queue.empty();
    }
private:
    std::queue<int> _queue{};
};
