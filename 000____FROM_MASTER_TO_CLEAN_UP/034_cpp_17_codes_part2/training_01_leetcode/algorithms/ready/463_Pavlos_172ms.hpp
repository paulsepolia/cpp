class Solution {
public:
    int islandPerimeter(vector<vector<int>>& grid)
    {

        int res(0);
        // get the number of columns
        const unsigned int COLS(grid[0].size());
        // get the number of rows
        const unsigned int ROWS(grid.size());
        // create a new grid
        std::vector<std::vector<int>> grid2{};
        grid2.resize(ROWS+2);
        for(unsigned int i = 0; i != grid2.size(); i++) {
            grid2[i].resize(COLS+2);
        }
        // fill with the elements
        for(unsigned int i = 0; i != ROWS+2; i++) {
            for(unsigned int j = 0; j != COLS+2; j++) {
                if(i == 0 || i == ROWS+1 || j == 0 || j == COLS+1) {
                    grid2[i][j] = 2;
                } else {
                    grid2[i][j] = grid[i-1][j-1];
                }
            }
        }
        // scan the board
        for(unsigned int i = 1; i != ROWS+1; i++) {
            for(unsigned int j = 1; j != COLS+1; j++) {
                if(grid2[i][j] == 1) {
                    if(grid2[i-1][j] == 1 && grid2[i+1][j] == 1 && grid2[i][j+1] == 1 && grid2[i][j-1] == 1) {
                        continue;
                    } else if(grid2[i-1][j] != 1 && grid2[i+1][j] == 1 && grid2[i][j+1] == 1 && grid2[i][j-1] == 1) {
                        res++;
                    } else if(grid2[i-1][j] == 1 && grid2[i+1][j] != 1 && grid2[i][j+1] == 1 && grid2[i][j-1] == 1) {
                        res++;
                    } else if(grid2[i-1][j] == 1 && grid2[i+1][j] == 1 && grid2[i][j+1] != 1 && grid2[i][j-1] == 1) {
                        res++;
                    } else if(grid2[i-1][j] == 1 && grid2[i+1][j] == 1 && grid2[i][j+1] == 1 && grid2[i][j-1] != 1) {
                        res++;
                    } else if(grid2[i-1][j] != 1 && grid2[i+1][j] != 1 && grid2[i][j+1] == 1 && grid2[i][j-1] == 1) {
                        res++;
                        res++;
                    } else if(grid2[i-1][j] != 1 && grid2[i+1][j] == 1 && grid2[i][j+1] != 1 && grid2[i][j-1] == 1) {
                        res++;
                        res++;
                    } else if(grid2[i-1][j] != 1 && grid2[i+1][j] == 1 && grid2[i][j+1] == 1 && grid2[i][j-1] != 1) {
                        res++;
                        res++;
                    } else if(grid2[i-1][j] == 1 && grid2[i+1][j] != 1 && grid2[i][j+1] != 1 && grid2[i][j-1] == 1) {
                        res++;
                        res++;
                    } else if(grid2[i-1][j] == 1 && grid2[i+1][j] != 1 && grid2[i][j+1] == 1 && grid2[i][j-1] != 1) {
                        res++;
                        res++;
                    } else if(grid2[i-1][j] == 1 && grid2[i+1][j] == 1 && grid2[i][j+1] != 1 && grid2[i][j-1] != 1) {
                        res++;
                        res++;
                    } else if(grid2[i-1][j] != 1 && grid2[i+1][j] != 1 && grid2[i][j+1] != 1 && grid2[i][j-1] == 1) {
                        res++;
                        res++;
                        res++;
                    } else if(grid2[i-1][j] != 1 && grid2[i+1][j] == 1 && grid2[i][j+1] != 1 && grid2[i][j-1] != 1) {
                        res++;
                        res++;
                        res++;
                    } else if(grid2[i-1][j] == 1 && grid2[i+1][j] != 1 && grid2[i][j+1] != 1 && grid2[i][j-1] != 1) {
                        res++;
                        res++;
                        res++;
                    } else if(grid2[i-1][j] != 1 && grid2[i+1][j] != 1 && grid2[i][j+1] == 1 && grid2[i][j-1] != 1) {
                        res++;
                        res++;
                        res++;
                    } else if(grid2[i-1][j] != 1 && grid2[i+1][j] != 1 && grid2[i][j+1] != 1 && grid2[i][j-1] != 1) {
                        res++;
                        res++;
                        res++;
                        res++;
                    }
                }
            }
        }
        return res;
    }
};
