class Solution {
public:
    bool canJump(vector<int>& nums)
    {
        // get the size of nums
        const unsigned int DIM(nums.size());
        // corner case
        if(DIM == 1) return true;
        if(nums[0] == 0 && DIM > 1) return false;
        // check for the position of zeroes
        // if you get a zero try to go back to jump in a different way
        unsigned int i(0);
        bool flg(false);
        int counter(0);
        while(i < DIM-1) { // do not count the last element
            if(nums[i] == 0) {
                // go back and try to jump over it
                while(true) {
                    i--;
                    counter++;
                    if((nums[i] > counter)) {
                        break;
                    }
                    if(i == 0) {
                        flg = true;
                        break;
                    }
                }
                if(flg) break;
            }
            i++;
            i = i + counter;
            counter = 0;
        }
        if(!flg) return true;
        return false;
    }
};
