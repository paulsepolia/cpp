class RandomizedCollection {
public:
    /** Initialize your data structure here. */
    RandomizedCollection()
    {
        std::random_shuffle(_vec.begin(), _vec.end());
    }
    /** Inserts a value to the collection. Returns true if the collection did not already contain the specified element. */
    bool insert(int val)
    {
        auto it(std::find(_vec.begin(), _vec.end(), val));
        if(it == _vec.end()) {
            _vec.push_back(val);
            return true;
        }
        _vec.push_back(val);
        return false;
    }
    /** Removes a value from the collection. Returns true if the collection contained the specified element. */
    bool remove(int val)
    {
        if(!_vec.empty()) {
            auto it(std::find(_vec.begin(), _vec.end(), val));
            if(it != _vec.end()) {
                _vec.erase(it);
                return true;
            }
        }
        return false;
    }
    /** Get a random element from the collection. */
    int getRandom()
    {
        random_shuffle(_vec.begin(), _vec.end());
        return _vec[0];
    }
private:
    std::vector<int> _vec{};
};
/**
 * Your RandomizedCollection object will be instantiated and called as such:
 * RandomizedCollection obj = new RandomizedCollection();
 * bool param_1 = obj.insert(val);
 * bool param_2 = obj.remove(val);
 * int param_3 = obj.getRandom();
 */
