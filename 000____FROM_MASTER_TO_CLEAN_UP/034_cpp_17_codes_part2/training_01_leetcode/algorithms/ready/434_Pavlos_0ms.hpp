class Solution {
public:
    int countSegments(string s)
    {
        const unsigned int DIM(s.size());
        // corner case
        if(DIM == 0) return 0;
        // corner case
        // check if only space
        std::string tmp(s);
        std::sort(tmp.begin(), tmp.end());
        auto it(std::unique(tmp.begin(), tmp.end()));
        tmp.resize(std::distance(tmp.begin(),it));
        if(tmp == " ") return 0;
        // rest cases
        int counter(0);
        for(unsigned int i = 0; i != DIM-1; i++) {
            if(isspace(s[i]) && !isspace(s[i+1])) counter++;
        }
        if(s[0] != ' ') counter++;
        return counter;
    }
};
