class Solution {
public:
    string largestNumber(vector<int>& nums)
    {
        // get the size of the vector of numbers
        const unsigned int DIM(nums.size());
        // convert the nums to strings
        std::vector<std::string> vec{};
        for(unsigned int i = 0; i != DIM; i++) {
            vec.push_back(std::to_string(nums[i]));
        }
        // sort and reverse
        std::sort(vec.begin(), vec.end());
        std::reverse(vec.begin(), vec.end());
        // corner case
        if(DIM == 1) return vec[0];
        // reorder the non order well elements
here :
        int i(0);
        while(true) {
            if(vec[i]+vec[i+1] < vec[i+1]+vec[i]) {
                std::string tmp(vec[i]);
                vec[i] = vec[i+1];
                vec[i+1] = tmp;
                goto here;
            }
            i++;
            if(i == DIM-1) break;
        }
        // put them as a whole string
        std::string s("");
        for(unsigned int i = 0; i != vec.size(); i++) {
            s = s + vec[i];
        }
        // corner case
        // if the result string conatins only '0' make it "0"
        bool flg(true);
        for(unsigned int i = 0; i != s.size(); i++) {
            if(s[i] != '0') {
                flg = false;
            }
        }
        if(flg) return "0";
        return s;
    }
};
