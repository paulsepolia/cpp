#include <stack>

class Queue {
public:
    // Push element x to the back of queue
    void push(int x)
    {
        _stack1.push(x);
    }

    // Removes the element from in front of queue
    void pop(void)
    {
        // reverve the stack 1 to get the last element
        while(!_stack1.empty()) {
            _stack2.push(_stack1.top());
            _stack1.pop();
        }

        // put the elements back and pop
        _stack2.pop();

        // revert again
        while(!_stack2.empty()) {
            _stack1.push(_stack2.top());
            _stack2.pop();
        }
    }

    // Get the front element
    int peek(void)
    {
        while(!_stack1.empty()) {
            _stack2.push(_stack1.top());
            _stack1.pop();
        }
        int top_elem(_stack2.top());
        while(!_stack2.empty()) {
            _stack1.push(_stack2.top());
            _stack2.pop();
        }
        return top_elem;
    }

    // Return whether the queue is empty
    bool empty(void)
    {
        return _stack1.empty();
    }

private:

    std::stack<int> _stack1{};
    std::stack<int> _stack2{};
};
