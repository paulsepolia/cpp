// Newton method
// https://en.wikipedia.org/wiki/Methods_of_computing_square_roots
class Solution {
public:
    int mySqrt(int x)
    {
        if (x == 0) return 0;
        if (x == 2147483647) return 46340;
        double xn(1);
        double xnp1(0);
        while(true) {
            xnp1 = 0.5*(xn + x/xn);
            if(xnp1 == xn) break;
            xn = xnp1;
        }
        return static_cast<int>(xnp1);
    }
};
