class Solution {
public:
    int hIndex(vector<int>& citations)
    {
        int res(0);
        // get the dim
        const unsigned int DIM1(citations.size());
        if(DIM1 == 1 && citations[0] >= 1) return 1;
        if(DIM1 == 1 && citations[0] == 0) return 0;
        if(DIM1 == 0) return 0;
        // sort the array
        std::sort(citations.begin(), citations.end());
        // reverse the array
        std::reverse(citations.begin(), citations.end());
        // a vector to hold all possible h indices
        std::vector<int> vecH{};
        int h_index(0);
        while(true) {
            int counter(0);
            for(unsigned int i = 0; i != DIM1; i++) {
                if(h_index <= citations[i]) {
                    counter++;
                    if(h_index == 0) counter--;
                    if(counter == h_index) {
                        vecH.push_back(h_index);
                        break;
                    }
                }
            }
            h_index++;
            if(h_index > citations[0]) break;
        }
        std::sort(vecH.begin(), vecH.end());
        // get the maximum h index
        res = vecH[vecH.size()-1];
        return res;
    }
};
