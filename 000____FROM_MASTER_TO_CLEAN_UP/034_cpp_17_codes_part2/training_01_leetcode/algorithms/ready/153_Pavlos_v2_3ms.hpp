class Solution {
public:
    int findMin(vector<int>& nums)
    {
        const unsigned int DIM(nums.size());
        int res(0);
        bool flg(true);
        for(unsigned int i = 0; i != DIM-1; i++) {
            if(nums[i] > nums[i+1]) {
                res = nums[i+1];
                flg = false;
                break;
            }
        }
        if(flg) res = nums[0];
        return res;
    }
};
