class Solution {
public:
    int findKthLargest(vector<int>& nums, int k)
    {
        const unsigned int DIM(nums.size());
        std::sort(nums.begin(), nums.end());
        return nums[DIM-k];
    }
};
