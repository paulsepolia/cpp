class Solution {
public:
    int maximumGap(vector<int>& nums)
    {
        const unsigned int DIM(nums.size());
        // corner cases
        if(DIM == 0) return 0;
        if(DIM == 1) return 0;
        std::sort(nums.begin(), nums.end());
        int tmp_max(0);
        for(unsigned int i = 0; i < DIM-1; i++) {
            int tmp_loc = nums[i+1] - nums[i];
            if( tmp_loc > tmp_max) {
                tmp_max = tmp_loc;
            }
        }
        return tmp_max;
    }
}
