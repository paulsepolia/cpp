class Solution {
public:
    int longestPalindrome(string s)
    {
        // get the size of the string
        const unsigned int DIM1(s.size());
        // build a vector which holds the letter of the alphabet
        std::vector<char> vec1{};
        const char ALPHA1('a');
        const char ALPHA2('A');
        const char ZETA1('z');
        const char ZETA2('Z');
        // lower cases
        for(char i = ALPHA1; i <= ZETA1; i++) {
            vec1.push_back(i);
        }
        // upper cases
        for(char i = ALPHA2; i <= ZETA2; i++) {
            vec1.push_back(i);
        }
        const unsigned int DIM2(vec1.size());
        // get the number of times each letter is in the string
        std::vector<int> vec2{};
        int counter(0);
        for(unsigned int i = 0; i != DIM2; i++) {
            counter = 0;
            for(unsigned int j = 0; j != DIM1; j++) {
                if(vec1[i] == s[j]) {
                    ++counter;
                }
            }
            vec2.push_back(counter);
        }
        // find the even numbers and add them
        const unsigned int DIM3(vec2.size());
        int sum(0);
        for(unsigned int i = 0; i != DIM3; i++) {
            if(vec2[i]%2 == 0) {
                sum += vec2[i];
            }
        }
        // find the odd numbers larger than 1 and add them
        bool flg1(false);
        for(unsigned int i = 0; i != DIM3; i++) {
            if(vec2[i]%2 != 0) {
                sum += vec2[i]-1;
                flg1 = true;
            }
        }
        // find the if 1 exists
        bool flg2(false);
        for(unsigned int i = 0; i != DIM3; i++) {
            if(vec2[i] == 1) {
                flg2 = true;
                break;
            }
        }
        // add all of them
        if(flg1 || flg2) sum = sum + 1;
        return sum;
    }
};
