class Solution {
public:
    vector<int> topKFrequent(vector<int>& nums, int k)
    {
        // the result vector
        std::vector<int> res{};
        // get the size
        const unsigned int DIM(nums.size());
        // corner case
        if(DIM == 1) return nums;
        // sort
        std::sort(nums.begin(), nums.end());
        nums.push_back(nums[DIM-1]+1);
        // put the elements in a map
        std::multimap<int,int> map_res{};
        int counter(0);
        for(unsigned int i(0); i != DIM; i++) {
            if(nums[i] == nums[i+1]) {
                counter++;
            } else {
                map_res.insert(std::pair<int,int>(++counter,nums[i]));
                counter = 0;
            }
        }
        counter = 0;
        std::multimap<char,int>::reverse_iterator rit;
        for (auto rit(map_res.rbegin()); rit != map_res.rend(); ++rit) {
            res.push_back(rit->second);
            counter++;
            if(counter == k) break;
        }
        return res;
    }
};
