class Solution {
public:
    bool isSubsequence(string s, string t)
    {
        const unsigned int DIM1(s.size());
        const unsigned int DIM2(t.size());
        unsigned int k(0);
        unsigned int counter(0);
        for(unsigned int i(0); i != DIM1; i++) {
            for(unsigned int j(k); j != DIM2; j++) {
                if(s[i] == t[j]) {
                    k = j+1;
                    counter++;
                    break;
                }
            }
            if(DIM1-counter > DIM1-i) {
                break;
            }
        }
        bool res(false);
        if(counter == DIM1) res = true;
        return res;
    }
};
