/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */

class Solution {
public:
    /** @param head The linked list's head.
        Note that the head is guaranteed to be not null, so it contains at least one node. */
    Solution(ListNode* head)
    {
        _list = head;
    }

    /** Returns a random node's value. */
    int getRandom()
    {
        ListNode * const HEAD(_list);
        const int size(this->getSize());
        int v2(rand()%size+1);
        int counter(0);
        int res(0);
        while(v2 != counter) {
            res = _list->val;
            _list = _list->next;
            ++counter;
        }
        _list = HEAD;
        return res;
    }

private:
    int getSize()
    {
        int counter(0);
        ListNode * const HEAD(_list);
        while(_list != 0) {
            ++counter;
            _list = _list->next;
        }
        _list = HEAD;
        return counter;
    }
    int _len;
    ListNode * _list;
};

/**
 * Your Solution object will be instantiated and called as such:
 * Solution obj = new Solution(head);
 * int param_1 = obj.getRandom();
 */
