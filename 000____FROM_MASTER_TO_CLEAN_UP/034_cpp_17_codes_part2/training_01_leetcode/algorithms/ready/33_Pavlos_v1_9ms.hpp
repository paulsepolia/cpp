class Solution {
public:
    int search(vector<int>& nums, int target)
    {
        const unsigned int DIM(nums.size());
        // corner cases
        if(DIM == 0) return -1;
        if(DIM == 1) {
            if(nums[0] == target) return 0;
            return -1;
        }
        auto it = std::find(nums.begin(), nums.end(), target);
        if(it == nums.end()) return -1;
        return it-nums.begin();
    }
};
