class Solution {
public:
    Solution(vector<int> nums)
    {
        _tmp = nums;
    }

    int pick(int target)
    {
        // find the element
        std::vector<int> pos{};
        for(unsigned int i = 0; i != _tmp.size(); i++) {
            if(target == _tmp[i]) pos.push_back(i);
        }
        std::random_shuffle(pos.begin(), pos.end());
        return pos[0];
    }
private:
    std::vector<int> _tmp{};
};

/**
 * Your Solution object will be instantiated and called as such:
 * Solution obj = new Solution(nums);
 * int param_1 = obj.pick(target);
 */

