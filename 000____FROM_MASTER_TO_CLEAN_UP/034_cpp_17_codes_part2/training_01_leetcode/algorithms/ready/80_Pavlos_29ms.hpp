class Solution {
public:
    int removeDuplicates(vector<int>& nums)
    {
        if(nums.size() == 0) return 0;
        if(nums.size() == 1) return 1;
        if(nums.size() == 2) return 2;
        int i(0);
        while(true) {
            if(nums[i] == nums[i+1] && nums[i+1] == nums[i+2]) {
                nums.erase(nums.begin()+i);
                nums.shrink_to_fit();
                --i;
            }
            ++i;
            if(i+2 > nums.size()-1) break;
        }
        int res(nums.size());
        return res;
    }
};
