class RandomizedSet {
public:
    /** Initialize your data structure here. */
    RandomizedSet()
    {
        std::random_shuffle(_vec.begin(), _vec.end());
    }
    /** Inserts a value to the set. Returns true if the set did not already contain the specified element. */
    bool insert(int val)
    {
        auto it(std::find(_vec.begin(), _vec.end(), val));
        if(it == _vec.end()) {
            _vec.push_back(val);
            return true;
        }
        return false;
    }
    /** Removes a value from the set. Returns true if the set contained the specified element. */
    bool remove(int val)
    {
        if(!_vec.empty()) {
            auto it(std::find(_vec.begin(), _vec.end(), val));
            if(it != _vec.end()) {
                _vec.erase(it);
                return true;
            }
        }
        return false;
    }
    /** Get a random element from the set. */
    int getRandom()
    {
        random_shuffle(_vec.begin(), _vec.end());
        return _vec[0];
    }
private:
    std::vector<int> _vec{};
};
/**
 * Your RandomizedSet object will be instantiated and called as such:
 * RandomizedSet obj = new RandomizedSet();
 * bool param_1 = obj.insert(val);
 * bool param_2 = obj.remove(val);
 * int param_3 = obj.getRandom();
 */
