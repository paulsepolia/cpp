//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <string>

class Solution {
public:

    // isAnagram_v1
    bool isAnagram_v1(std::string, std::string);
};

#endif

// end
