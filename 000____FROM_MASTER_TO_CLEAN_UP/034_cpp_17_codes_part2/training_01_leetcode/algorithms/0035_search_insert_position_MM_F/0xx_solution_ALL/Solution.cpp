//===========================//
// Solution class definition //
//===========================//

#include "Solution.hpp"

// searchInsert_v1

int Solution::searchInsert_v1(std::vector<int>& nums, int target)
{
    const unsigned int DIM(nums.size());
    int index(-1);

    for(unsigned int i = 0; i != DIM; i++) {
        if(nums[i] >= target) {
            index = static_cast<int>(i);
            break;
        }
    }

    if(index == -1) {
        index = static_cast<int>(DIM);
    }

    return index;
}

// end
