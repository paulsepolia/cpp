//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <vector>

class Solution {
public:

    // searchInsert_v1
    int searchInsert_v1(std::vector<int>&, int);
};

#endif

// end
