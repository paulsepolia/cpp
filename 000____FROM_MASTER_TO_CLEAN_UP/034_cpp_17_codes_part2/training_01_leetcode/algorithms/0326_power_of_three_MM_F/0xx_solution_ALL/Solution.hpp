//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

class Solution {
public:

    //  isPowerOfThree_v1
    bool isPowerOfThree_v1(int);
};

#endif

// end
