//===========================//
// Solution class definition //
//===========================//

#include <algorithm>
#include "Solution.hpp"

// intersectionII_v1

std::vector<int> Solution::intersectionII_v1(std::vector<int> & nums1, std::vector<int> & nums2)
{
    if(nums1.size() == 0 || nums2.size() == 0) {
        return std::vector<int> {};
    }

    std::vector<int> h1(nums1);
    std::vector<int> h2(nums2);

    std::sort(h1.begin(), h1.end());
    std::sort(h2.begin(), h2.end());

    auto it1 = std::unique(h1.begin(), h1.end());
    auto it2 = std::unique(h2.begin(), h2.end());

    h1.resize(std::distance(h1.begin(), it1));
    h2.resize(std::distance(h2.begin(), it2));

    std::vector<int> res1{};
    std::vector<int> res2{};

    // res1

    it1 = nums1.begin();
    for(unsigned int i = 0; i != h2.size(); i++) {
here1:
        it1 = find(it1, nums1.end(), h2[i]);
        if (it1 != nums1.end()) {
            res1.push_back(h2[i]);
            it1++;
            goto here1;
        } else if(it1 == nums1.end()) {
            it1 = nums1.begin();
        }
    }

    // res2

    it2 = nums2.begin();
    for(unsigned int i = 0; i != h1.size(); i++) {
here2:
        it2 = find(it2, nums2.end(), h1[i]);
        if (it2 != nums2.end()) {
            res2.push_back(h1[i]);
            it2++;
            goto here2;
        } else if(it2 == nums2.end()) {
            it2 = nums2.begin();
        }
    }

    // unique set of elements

    std::vector<int> uniq(res1);
    it1 = std::unique(uniq.begin(), uniq.end());
    uniq.resize(std::distance(uniq.begin(), it1));

    // counts1

    std::vector<int> counts1{};
    for(unsigned int i = 0; i != uniq.size(); i++) {
        int tmp(std::count(res1.begin(), res1.end(), uniq[i]));
        counts1.push_back(tmp);
    }

    // counts2

    std::vector<int> counts2{};
    for(unsigned int i = 0; i != uniq.size(); i++) {
        int tmp(std::count(res2.begin(), res2.end(), uniq[i]));
        counts2.push_back(tmp);
    }

    // res final
    std::vector<int> res{};
    for(unsigned int i = 0; i != counts1.size(); i++) {
        if(counts1[i] > counts2[i]) {
            for(int j = 0; j != counts2[i]; j++) {
                res.push_back(uniq[i]);
            }
        } else {
            for(int j = 0; j != counts1[i]; j++) {
                res.push_back(uniq[i]);
            }
        }
    }

    return res;
}

// intersectionII_v2

std::vector<int> Solution::intersectionII_v2(std::vector<int> & nums1, std::vector<int> & nums2)
{
    if(nums1.size() == 0 || nums2.size() == 0) {
        return std::vector<int> {};
    }

    std::sort(nums1.begin(), nums1.end());
    std::sort(nums2.begin(), nums2.end());

    std::vector<int> res{};
    res.resize(nums1.size()+nums2.size());

    // set the intersection

    auto it = std::set_intersection(nums1.begin(), nums1.end(),
                                    nums2.begin(), nums2.end(), res.begin());

    res.resize(it - res.begin());

    return res;
}

// end
