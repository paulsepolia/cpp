//===========================//
// Solution class definition //
//===========================//

#include <algorithm>
#include "Solution.hpp"

// intersection_v1

std::vector<int> Solution::intersection_v1(std::vector<int> & nums1, std::vector<int> & nums2)
{
    if(nums1.size() == 0 || nums2.size() == 0) {
        return std::vector<int> {};
    }

    std::sort(nums1.begin(), nums1.end());
    std::sort(nums2.begin(), nums2.end());

    auto it = std::unique(nums1.begin(), nums1.end());
    nums1.resize(std::distance(nums1.begin(),it));

    it = std::unique(nums2.begin(), nums2.end());
    nums2.resize(std::distance(nums2.begin(),it));

    std::vector<int> res{};

    for(unsigned int i = 0; i != nums2.size(); i++) {
        it = find(nums1.begin(), nums1.end(), nums2[i]);
        if (it != nums1.end()) {
            res.push_back(nums2[i]);
        }
    }

    it = std::unique(res.begin(), res.end());
    res.resize(std::distance(res.begin(),it));

    return res;
}

// intersection_v2

std::vector<int> Solution::intersection_v2(std::vector<int> & nums1, std::vector<int> & nums2)
{
    if(nums1.size() == 0 || nums2.size() == 0) {
        return std::vector<int> {};
    }

    std::sort(nums2.begin(), nums2.end());

    auto it = std::unique(nums2.begin(), nums2.end());
    nums2.resize(std::distance(nums2.begin(),it));

    std::vector<int> res{};

    for(unsigned int i = 0; i != nums2.size(); i++) {
        it = find(nums1.begin(), nums1.end(), nums2[i]);
        if (it != nums1.end()) {
            res.push_back(nums2[i]);
        }
    }

    it = std::unique(res.begin(), res.end());
    res.resize(std::distance(res.begin(),it));

    return res;
}

// intersection_v3

std::vector<int> Solution::intersection_v3(std::vector<int> & nums1, std::vector<int> & nums2)
{
    if(nums1.size() == 0 || nums2.size() == 0) {
        return std::vector<int> {};
    }

    std::sort(nums1.begin(), nums1.end());
    std::sort(nums2.begin(), nums2.end());

    std::vector<int> res{};
    res.resize(nums1.size()+nums2.size());

    // set the intersection

    auto it = std::set_intersection(nums1.begin(), nums1.end(),
                                    nums2.begin(), nums2.end(), res.begin());

    res.resize(it - res.begin());

    // make unique

    it = std::unique(res.begin(), res.end());
    res.resize(std::distance(res.begin(),it));

    return res;
}

// end
