#include <iostream>
#include <thread>
#include <unistd.h>
#include <cassert>

class ProcessorLimiter {
public:

    explicit ProcessorLimiter(uint32_t numberOfRequestedProcessors) : _cpuSetOriginal{} {

        auto numberOfAvailableProcessors = GetNumberOfAvailableProcessorsInternal(_cpuSetOriginal);

        if (numberOfAvailableProcessors == numberOfRequestedProcessors) {
            return;
        }

        if (numberOfAvailableProcessors < numberOfRequestedProcessors) {
            std::cout << "--> error --> 1" << std::endl;
            exit(-1);
        }

        cpu_set_t cpuSetNew;

        CPU_ZERO(&cpuSetNew);

        for (uint32_t i = 0; i < numberOfRequestedProcessors; ++i) {
            CPU_SET(static_cast<int>(i), &cpuSetNew);
        }

        if (sched_setaffinity(getpid(), sizeof(cpuSetNew), &cpuSetNew) == -1) {

            std::cout << "--> error --> 2" << std::endl;
            exit(-1);
        }
    }

    ~ProcessorLimiter() {

        // Reset to original affinity settings

        auto returnValue = sched_setaffinity(getpid(), sizeof(_cpuSetOriginal), &_cpuSetOriginal);
        (void) returnValue;  // [[maybe_unused]]

        assert(returnValue != -1);
    }

    // Return the number of available processors for this process

    static uint32_t GetNumberOfAvailableProcessors() {
        cpu_set_t cpuSet;
        return GetNumberOfAvailableProcessorsInternal(cpuSet);
    }

    cpu_set_t _cpuSetOriginal;

    static uint32_t GetNumberOfAvailableProcessorsInternal(cpu_set_t &cpuSet) {

        CPU_ZERO(&cpuSet);

        if (sched_getaffinity(getpid(), sizeof(cpuSet), &cpuSet) == -1) {
            std::cout << "--> error --> 3" << std::endl;
            exit(-1);
        }

        return CPU_COUNT(&cpuSet);
    }
};


auto main() -> int {

    {
        std::cout << " ----------------------->> example --> 1" << std::endl;
        std::cout << " pgg --> 1 --> std::thread::hardware_concurrency() = "
                  << std::thread::hardware_concurrency() << std::endl;

        std::cout << " pgg --> 2 --> sysconf(_SC_NPROCESSORS_ONLN) = "
                  << sysconf(_SC_NPROCESSORS_ONLN) << std::endl;
    }

    {
        std::cout << " ----------------------->> example --> 2" << std::endl;

        auto ob1{ProcessorLimiter(2)};

        std::cout << " pgg --> 0 --> ob1.GetNumberOfAvailableProcessors() = "
                  << ob1.GetNumberOfAvailableProcessors() << std::endl;
        cpu_set_t cpuSet;

        std::cout << " pgg --> 1 --> ob1.GetNumberOfAvailableProcessorsInternal(cpuSet) = "
                  << ob1.GetNumberOfAvailableProcessorsInternal(cpuSet) << std::endl;

        std::cout << " pgg --> 2 --> std::thread::hardware_concurrency() = "
                  << std::thread::hardware_concurrency() << std::endl;
        std::cout << " pgg --> 3 --> sysconf(_SC_NPROCESSORS_ONLN) = "
                  << sysconf(_SC_NPROCESSORS_ONLN) << std::endl;
    }

    {
        std::cout << " ----------------------->> example --> 3" << std::endl;

        auto ob1{ProcessorLimiter(8)};

        std::cout << " pgg --> 0 --> ob1.GetNumberOfAvailableProcessors() = "
                  << ob1.GetNumberOfAvailableProcessors() << std::endl;

        cpu_set_t cpuSet;

        std::cout << " pgg --> 1 --> ob1.GetNumberOfAvailableProcessorsInternal(cpuSet) = "
                  << ob1.GetNumberOfAvailableProcessorsInternal(cpuSet) << std::endl;

        std::cout << " pgg --> 2 --> std::thread::hardware_concurrency() = "
                  << std::thread::hardware_concurrency() << std::endl;

        std::cout << " pgg --> 3 --> sysconf(_SC_NPROCESSORS_ONLN) = "
                  << sysconf(_SC_NPROCESSORS_ONLN) << std::endl;
    }

    {
        std::cout << " ----------------------->> example --> 4" << std::endl;

        auto ob1{ProcessorLimiter(4)};

        std::cout << " pgg --> 0 --> ob1.GetNumberOfAvailableProcessors() = "
                  << ob1.GetNumberOfAvailableProcessors() << std::endl;

        cpu_set_t cpuSet;

        std::cout << " pgg --> 1 --> ob1.GetNumberOfAvailableProcessorsInternal(cpuSet) = "
                  << ob1.GetNumberOfAvailableProcessorsInternal(cpuSet) << std::endl;

        std::cout << " pgg --> 2 --> std::thread::hardware_concurrency() = "
                  << std::thread::hardware_concurrency() << std::endl;

        std::cout << " pgg --> 3 --> sysconf(_SC_NPROCESSORS_ONLN) = "
                  << sysconf(_SC_NPROCESSORS_ONLN) << std::endl;
    }

    {
        std::cout << " ----------------------->> example --> 4" << std::endl;


        std::cout << " pgg --> 0 --> ProcessorLimiter::GetNumberOfAvailableProcessors() = "
                  << ProcessorLimiter::GetNumberOfAvailableProcessors() << std::endl;

        cpu_set_t cpuSet;

        std::cout << " pgg --> 1 --> ProcessorLimiter::GetNumberOfAvailableProcessorsInternal(cpuSet) = "
                  << ProcessorLimiter::GetNumberOfAvailableProcessorsInternal(cpuSet) << std::endl;

        std::cout << " pgg --> 2 --> std::thread::hardware_concurrency() = "
                  << std::thread::hardware_concurrency() << std::endl;

        std::cout << " pgg --> 3 --> sysconf(_SC_NPROCESSORS_ONLN) = "
                  << sysconf(_SC_NPROCESSORS_ONLN) << std::endl;
    }

    {
        std::cout << " ----------------------->> example --> 5" << std::endl;

        std::cout << " pgg --> 0 --> ProcessorLimiter::GetNumberOfAvailableProcessors() = "
                  << ProcessorLimiter::GetNumberOfAvailableProcessors() << std::endl;

        cpu_set_t cpuSet;

        std::cout << " pgg --> 1 --> ProcessorLimiter::GetNumberOfAvailableProcessorsInternal(cpuSet) = "
                  << ProcessorLimiter::GetNumberOfAvailableProcessorsInternal(cpuSet) << std::endl;

        std::cout << " pgg --> 2 --> std::thread::hardware_concurrency() = "
                  << std::thread::hardware_concurrency() << std::endl;

        std::cout << " pgg --> 3 --> sysconf(_SC_NPROCESSORS_ONLN) = "
                  << sysconf(_SC_NPROCESSORS_ONLN) << std::endl;

        auto ob1{ProcessorLimiter(4)};

        std::cout << " pgg --> 4 --> ob1.GetNumberOfAvailableProcessors() = "
                  << ob1.GetNumberOfAvailableProcessors() << std::endl;

        std::cout << " pgg --> 5 --> ob1.GetNumberOfAvailableProcessorsInternal(cpuSet) = "
                  << ob1.GetNumberOfAvailableProcessorsInternal(cpuSet) << std::endl;

        std::cout << " pgg --> 6 --> std::thread::hardware_concurrency() = "
                  << std::thread::hardware_concurrency() << std::endl;

        std::cout << " pgg --> 7 --> sysconf(_SC_NPROCESSORS_ONLN) = "
                  << sysconf(_SC_NPROCESSORS_ONLN) << std::endl;
    }

}