#include <iostream>

template<typename T>
void line_printer(const T &x) {

    if constexpr (std::is_integral_v<T>) {
        std::cout << "num: " << x << std::endl;
    } else if constexpr (std::is_floating_point_v<T>) {
        const auto frac = x - static_cast<long>(x);
        std::cout << "flt: " << x << ", frac " << frac << std::endl;
    } else if constexpr(std::is_pointer_v<T>) {
        std::cout << "ptr, ";
        line_printer(*x);
    } else
        std::cout << x << std::endl;
}

template<typename ... Args>
void print_with_info(Args ... args) {
    (line_printer(args), ...); // fold expression over the comma operator
}

int main() {
    const int i = 10;
    const float f = 2.56f;
    const double d = 2.57;
    print_with_info(&i, &f, &d, 30);
}

// Line 5, 7, 11: if constexpr - to discard code at compile time,
// used to match the template parameter.

// Line 5, 7, 11: _v variable templates for type traits -
// no need to write std::trait_name<T>::value.

// Line 20: Fold Expressions inside print_with_info.
// This feature simplifies variadic templates.
// In the example we invoke line_printer() over all input arguments.