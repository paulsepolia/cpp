//===================//
// parallel for_each //
//===================//

//!!!!!!!!!!!!!//
// WRONG USAGE //
//!!!!!!!!!!!!!//

#include <iostream>
#include <vector>
#include <iterator>
#include <numeric>
#include <cmath>
#include <ctime>
#include <iomanip>

#include <parallel/algorithm>
#include <parallel/settings.h>

using std::cout;
using std::endl;
using std::ostream_iterator;
using std::vector;
using std::pow;
using std::fixed;
using std::setprecision;
using std::clock;

// type definition

typedef unsigned long int uli;

// functor

struct Sum {
    Sum()
    {
        sum = 0;
    }
    void operator()(const double & n)
    {
        sum += n;
    }
    double sum;
};

// the main function

int main()
{
    cout << fixed;
    cout << setprecision(5);

    //===================//
    // parallel settings //
    //===================//

    __gnu_parallel::_Settings s;
    s.algorithm_strategy = __gnu_parallel::force_parallel;
    __gnu_parallel::_Settings::set(s);

    //==========================//
    // end of parallel settings //
    //==========================//

    const uli DIM1 = 4 * uli(pow(10.0, 8.0));
    const uli TRIALS1 = 1 * uli(pow(10.0, 2.0));
    const uli DIV1 = 10;

    vector<double> v1(DIM1);
    time_t t1;
    time_t t2;

    cout << " --> build v1" << endl;

    iota(v1.begin(), v1.end(), 0.0);

    cout << " --> for_each v1 in parallel" << endl;

    t1 = clock();

    Sum sA;

    for (uli i = 0; i != TRIALS1; i++) {
        if (i%DIV1 == 0) {
            cout << " -------------------------------------------------------->> " << i << endl;
        }
        sA = std::__parallel::for_each(v1.begin(), v1.end(), Sum());
    }

    t2 = clock();
    cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

    cout << " --> some results :" << endl;

    cout << " --> v1[0] = " << v1[0] << endl;
    cout << " --> v1[1] = " << v1[1] << endl;
    cout << " --> v1[2] = " << v1[2] << endl;
    cout << " --> v1[3] = " << v1[3] << endl;

    cout << " --> sA.sum = " << sA.sum << endl;

    return 0;
}

// end
