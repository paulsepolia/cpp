//==============//
// parallel all //
//==============//

#include <iostream>
#include <vector>
#include <iterator>
#include <cmath>
#include <ctime>
#include <iomanip>
#include <chrono>
#include <functional>

#include <parallel/algorithm>
#include <parallel/numeric>
#include <parallel/settings.h>

using std::cout;
using std::endl;
using std::vector;
using std::pow;
using std::fixed;
using std::setprecision;
using namespace std::chrono;
using std::plus;
using std::boolalpha;

// type definition

typedef unsigned long int uli;

//==========//
// FUNCTORS //
//==========//

// functor # 1

template <typename T>
struct Sum {
public:

    // constructor

    Sum() : sum(0) {}

    // functor

    void operator()(const T & n)
    {
        sum += n;
    }

private:

    // local variable

    T sum;
};

// functor # 2

template<typename T>
struct UniqueNumber {
    double operator () ()
    {
        return T(123.456);
    }
};

// functor # 3

template<typename T>
struct ac_functor {
    T operator()(const T & x, const T & y)
    {
        return x+y;
    }
};

// functor # 4

template <typename T>
struct product_functor {
    T operator()(const T & x, const T & y)
    {
        return x*y;
    }
};

// functor # 5

template <typename T>
struct equal_functor {
    bool operator()(const T & i, const T & j)
    {
        return (i == j);
    }
};

//===========//
// FUNCTIONS //
//===========//

// function # 1

template<typename T>
T ac_fun(const T & x, const T & y)
{
    return x+y;
}

// function # 2

template <typename T>
T product_fun(const T & x, const T & y)
{
    return x*y;
}

// function # 3

template <typename T>
bool equal_fun(const T & i, const T & j)
{
    return (i == j);
}

// the main function

int main()
{
    cout << fixed;
    cout << setprecision(5);
    cout << boolalpha;

    //===================//
    // parallel settings //
    //===================//

    //==========================================================//
    // FORCE ALL POSSIBLE ALGORITHMS TO BE EXECUTED IN PARALLEL //
    //==========================================================//

    __gnu_parallel::_Settings s;
    s.algorithm_strategy = __gnu_parallel::force_parallel;
    __gnu_parallel::_Settings::set(s);

    //==========================//
    // end of parallel settings //
    //==========================//

    const uli DIM1 = 1 * uli(pow(10.0, 8.0));
    const uli TRIALS1 = 5 * uli(pow(10.0, 0.0));
    const uli TRIALS2 = 5 * uli(pow(10.0, 0.0));
    const double ELEM1 = 1.0;
    const double INIT = 0.0;

    double a1;
    bool res_bool;

    vector<double> v1;
    vector<double> v3;
    v1.resize(DIM1);
    v3.resize(DIM1);

    auto t1 = system_clock::now();
    auto t2 = system_clock::now();
    duration<double> time_span;

    // vector constructor

    cout << " --> vector constructor --> non-parallel" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        vector<double> v1(DIM1, ELEM1);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // iota

    cout << " --> iota --> non-parallel" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        iota(v1.begin(), v1.end(), 0.0);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // random_shuffle

    cout << " --> random_shuffle --> parallel" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS1; i++) {
        random_shuffle(v1.begin(), v1.end());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // sort

    cout << " --> sort --> parallel" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS1; i++) {
        sort(v1.begin(), v1.end());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // stable_sort

    cout << " --> stable_sort --> parallel" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS1; i++) {
        stable_sort(v1.begin(), v1.end());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // find

    cout << " --> find --> parallel" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        find(v1.begin(), v1.end(), -1.0);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // find_if

    cout << " --> find_if --> parallel" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        find_if(v1.begin(), v1.end(), [](const double & i) {
            return (i < -1.0);
        });
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // transform

    cout << " --> transform --> parallel" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        transform(v1.begin(), v1.end(), v1.begin(), [](double & i) {
            return i++;
        });
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // search

    cout << " --> search --> parallel" << endl;

    vector<double> v2(2, -1);

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        search(v1.begin(), v1.end(), v2.begin(), v2.end());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // search_n

    cout << " --> search_n --> parallel" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        search_n(v1.begin(), v1.end(), 2, -11.22);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // for_each

    cout << " --> for_each --> parallel" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        for_each(v1.begin(), v1.end(), Sum<double>());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // generate

    cout << " --> generate --> parallel" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        generate(v1.begin(), v1.end(), UniqueNumber<double>());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // generate_n

    cout << " --> generate_n --> parallel" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        generate_n(v1.begin(), DIM1, UniqueNumber<double>());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // unique_copy

    cout << " --> unique_copy --> parallel" << endl;

    iota(v1.begin(), v1.end(), 0.0);
    random_shuffle(v1.begin(), v1.end());

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        unique_copy(v1.begin(), v1.end(), v3.begin());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // accumulate # 1

    cout << " --> accumulate # 1 --> default --> parallel" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        accumulate(v1.begin(), v1.end(), INIT);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // accumulate # 2

    cout << " --> accumulate # 2 --> functor --> parallel" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        accumulate(v1.begin(), v1.end(), INIT, ac_functor<double>());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // accumulate # 3

    cout << " --> accumulate # 3 --> function --> parallel" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        accumulate(v1.begin(), v1.end(), INIT, &ac_fun<double>);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // inner_product # 1

    cout << " --> inner_product # 1 --> functions --> parallel" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {

        a1 = inner_product(v1.begin(), v1.end(), v3.begin(),
                           INIT,
                           &ac_fun<double>,
                           &product_fun<double>);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> inner_product result = " << a1 << endl;

    // inner_product # 2

    cout << " --> inner_product # 2 --> functors --> parallel" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {

        a1 = inner_product(v1.begin(), v1.end(), v3.begin(),
                           INIT,
                           ac_functor<double>(),
                           product_functor<double>());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> inner_product result = " << a1 << endl;

    // equal # 1

    cout << " --> equal # 1 --> default --> parallel" << endl;

    iota(v1.begin(), v1.end(), 0.0);
    iota(v3.begin(), v3.end(), 0.0);

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        res_bool = equal(v1.begin(), v1.end(), v3.begin());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> equal result = " << res_bool << endl;

    // equal # 2

    cout << " --> equal # 2 --> functions --> parallel" << endl;

    iota(v1.begin(), v1.end(), 0.0);
    iota(v3.begin(), v3.end(), 0.0);

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        res_bool = equal(v1.begin(), v1.end(), v3.begin(), &equal_fun<double>);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> equal result = " << res_bool << endl;

    // equal # 3

    cout << " --> equal # 3 --> functors --> parallel" << endl;

    iota(v1.begin(), v1.end(), 0.0);
    iota(v3.begin(), v3.end(), 0.0);

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        res_bool = equal(v1.begin(), v1.end(), v3.begin(), equal_functor<double>());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> equal result = " << res_bool << endl;

    //===========//
    // LAST THIS //
    //===========//

    // nth_element

    cout << " --> nth_element --> parallel " << endl;

    iota(v1.begin(), v1.end(), 0.0);
    random_shuffle(v1.begin(), v1.end());

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS2; i++) {
        nth_element(v1.begin(), v1.begin()+i, v1.end());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    // results

    cout << " --> some results :" << endl;

    cout << " --> v1[0] = " << v1[0] << endl;
    cout << " --> v1[1] = " << v1[1] << endl;
    cout << " --> v1[2] = " << v1[2] << endl;
    cout << " --> v1[3] = " << v1[3] << endl;
    cout << " --> v1[TRIALS2-1] = " << v1[TRIALS2-1]
         << " --> TRAILS2-1 = " << TRIALS2-1 << endl;

    return 0;
}

// end
