#!/bin/bash

  # 1. compile

  g++  -O3                 \
       -Wall               \
       -std=gnu++17        \
	   -fopenmp            \
	   -march=native       \
	   -fwhole-program     \
	   -D_GLIBCXX_PARALLEL \
       driver_program.cpp  \
       -o x_gnu
