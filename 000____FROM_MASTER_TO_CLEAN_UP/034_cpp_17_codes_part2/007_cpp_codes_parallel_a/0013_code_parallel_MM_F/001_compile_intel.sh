#!/bin/bash

  # 1. compile

  icpc -O3                 \
	   -ipo                \
       -Wall               \
	   -wd1202             \
       -std=gnu++17        \
	   -qopenmp            \
	   -D_GLIBCXX_PARALLEL \
       driver_program.cpp  \
       -o x_intel
