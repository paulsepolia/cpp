//==============//
// parallel all //
//==============//

#include <iostream>
#include <vector>
#include <iterator>
#include <cmath>
#include <ctime>
#include <iomanip>

#include <parallel/algorithm>
#include <parallel/numeric>
#include <parallel/settings.h>

using std::cout;
using std::endl;
using std::vector;
using std::pow;
using std::fixed;
using std::setprecision;
using std::clock;

// type definition

typedef unsigned long int uli;

// the main function

int main()
{
    cout << fixed;
    cout << setprecision(5);

    //===================//
    // parallel settings //
    //===================//

    //==========================================================//
    // FORCE ALL POSSIBLE ALGORITHMS TO BE EXECUTED IN PARALLEL //
    //==========================================================//

    __gnu_parallel::_Settings s;
    s.algorithm_strategy = __gnu_parallel::force_parallel;
    __gnu_parallel::_Settings::set(s);

    //==========================//
    // end of parallel settings //
    //==========================//

    const uli DIM1 = 1 * uli(pow(10.0, 8.0));
    const uli TRIALS1 = 1 * uli(pow(10.0, 1.0));
    const uli TRIALS2 = 1 * uli(pow(10.0, 2.0));
    const double ELEM1 = 1.0;

    vector<double> v1;
    v1.resize(DIM1);

    cout << " --> vector construrtor --> non-parallel" << endl;

    for (uli i = 0; i != TRIALS2; i++) {
        vector<double> v1(DIM1, ELEM1);
    }

    cout << " --> iota --> non-parallel" << endl;

    for (uli i = 0; i != TRIALS2; i++) {
        iota(v1.begin(), v1.end(), 0.0);
    }

    cout << " --> random_shuffle --> parallel" << endl;

    for (uli i = 0; i != TRIALS1; i++) {
        random_shuffle(v1.begin(), v1.end());
    }

    cout << " --> sort --> parallel" << endl;

    for (uli i = 0; i != TRIALS1; i++) {
        sort(v1.begin(), v1.end());
    }

    cout << " --> stable_sort --> parallel" << endl;

    for (uli i = 0; i != TRIALS1; i++) {
        stable_sort(v1.begin(), v1.end());
    }

    cout << " --> find --> parallel" << endl;

    for (uli i = 0; i != TRIALS2; i++) {
        find(v1.begin(), v1.end(), -1.0);
    }

    cout << " --> find_if --> parallel" << endl;

    for (uli i = 0; i != TRIALS2; i++) {
        find_if(v1.begin(), v1.end(), [](const double & i) {
            return (i < -1.0);
        });
    }

    cout << " --> transform --> parallel" << endl;

    for (uli i = 0; i != TRIALS2; i++) {
        transform(v1.begin(), v1.end(), v1.begin(), [](double & i) {
            return i++;
        });
    }

    cout << " --> search --> parallel" << endl;

    vector<double> v2(2, -1);

    for (uli i = 0; i != TRIALS2; i++) {
        search(v1.begin(), v1.end(), v2.begin(), v2.end());
    }

    cout << " --> nth_element --> parallel " << endl;

    for (uli i = 0; i != TRIALS2; i++) {
        nth_element(v1.begin(), v1.begin()+i, v1.end());
    }

    cout << " --> some results :" << endl;

    cout << " --> v1[0] = " << v1[0] << endl;
    cout << " --> v1[1] = " << v1[1] << endl;
    cout << " --> v1[2] = " << v1[2] << endl;
    cout << " --> v1[3] = " << v1[3] << endl;
    cout << " --> v1[TRIALS2-1] = " << v1[TRIALS2-1]
         << " --> TRAILS2-1 = " << TRIALS2-1 << endl;

    return 0;
}

// end
