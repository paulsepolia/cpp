#include <iostream>
#include <cstdint>
#include <memory>

int main() {

    {
        std::cout << " --> example --> 1 -------------------> start" << std::endl;

        double d = 111.11;
        auto *pd = new double(d);
        std::shared_ptr<double> spd1(pd);

        std::cout << &d << std::endl;
        std::cout << spd1 << std::endl;
        std::cout << pd << std::endl;
        std::cout << d << std::endl;
        std::cout << *spd1 << std::endl;
        std::cout << *pd << std::endl;

        spd1.reset();

        std::cout << &d << std::endl;
        std::cout << spd1 << std::endl;
        std::cout << pd << std::endl;
        std::cout << d << std::endl;
        if (spd1) std::cout << *spd1 << std::endl;
        std::cout << *pd << std::endl;

        std::cout << " --> example --> 1 -------------------> end" << std::endl;
    }

    {
        std::cout << " --> example --> 2 -------------------> start" << std::endl;

        double d = 111.11;
        auto *pd = &d;
        std::shared_ptr<double> spd1(pd);

        std::cout << &d << std::endl;
        std::cout << spd1 << std::endl;
        std::cout << pd << std::endl;
        std::cout << d << std::endl;
        std::cout << *spd1 << std::endl;
        std::cout << *pd << std::endl;

        // THIS IS AN ERROR
        // SHARED_PTR TRIES TO DELETE A NON-DYNAMIC ALLOCATED SPACE

        spd1.reset();

        std::cout << " --> example --> 2 -------------------> end" << std::endl;
    }
}