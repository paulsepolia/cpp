
#include <iostream>
#include <vector>

using std::vector;
using std::cout;
using std::endl;

// the solution

int solution(vector<int> &A)
{
    unsigned int i = 0;
    int min = 0;
    int max = 0;
    int profit = 0;
    int max_profit = 0;
    min = max = A[0];
    profit = 0;

    for(i = 1; i < A.size(); i++) {

        if(A[i] < min) {
            min = max = A[i];
        } else if(A[i] > max) {
            max = A[i];
        }

        profit = max-min;

        if(max_profit < profit) {
            max_profit = profit;
        }
    }

    return max_profit;
}

// the main function

int main()
{
    vector<int> A({23171, 21011, 21123, 21366, 21013, 21367});

    cout << solution(A) << endl;

    return 0;
}

// end
