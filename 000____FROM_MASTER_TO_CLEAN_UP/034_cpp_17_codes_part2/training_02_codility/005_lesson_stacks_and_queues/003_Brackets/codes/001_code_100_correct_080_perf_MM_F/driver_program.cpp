
#include <iostream>
#include <vector>

using std::endl;
using std::cout;
using std::string;

// the solution

int solution(string &S)
{

    const int DIM = S.length();
    int sol = 1;

    int countL1 = 0;
    int countR1 = 0;

    int countL2 = 0;
    int countR2 = 0;

    int countL3 = 0;
    int countR3 = 0;

    if (DIM == 0) {
        sol = 1;
    }

    for (int i = 0; i != DIM; i++) {

        if (S[i]=='(') {
            countL1 = countL1 + 1;
        }

        if (S[i]=='[') {
            countL2 = countL2 + 1;
        }

        if (S[i]=='{') {
            countL3 = countL3 + 1;
        }

        if (S[i]==')') {
            countR1 = countR1 + 1;
        }

        if (S[i]==']') {
            countR2 = countR2 + 1;
        }

        if (S[i]=='}') {
            countR3 = countR3 + 1;
        }

        if ((countL1 - countR1) < 0) {
            sol = 0;
            break;
        }

        if ((countL2 - countR2) < 0) {
            sol = 0;
            break;
        }

        if ((countL3 - countR3) < 0) {
            sol = 0;
            break;
        }
    }

    if (countL1 != countR1) {
        sol = 0;
    }

    if (countL2 != countR2) {
        sol = 0;
    }

    if (countL3 != countR3) {
        sol = 0;
    }

    return sol;

}

// the main function

int main()
{
    string s1 = "({}()))";
    string s2 = "(({}()))";
    string s3 = "()(([][][]{}()))";

    cout << solution(s1) << endl;
    cout << solution(s2) << endl;
    cout << solution(s3) << endl;

    return 0;
}

// end
