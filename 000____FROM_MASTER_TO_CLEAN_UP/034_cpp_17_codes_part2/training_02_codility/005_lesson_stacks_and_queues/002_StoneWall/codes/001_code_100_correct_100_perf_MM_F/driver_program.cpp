
#include <vector>
#include <stack>
#include <iostream>

using std::cout;
using std::endl;
using std::vector;
using std::stack;

// the solution

int solution(vector<int> &H)
{
    int blocks = 0;
    unsigned int i = 0;
    stack<int> blocks_stack;

    for(i = 0; i < H.size(); ++i) {
        while(!blocks_stack.empty() && H[i] < blocks_stack.top()) {
            blocks_stack.pop();
        }

        if(blocks_stack.empty() || H[i] > blocks_stack.top()) {
            blocks_stack.push(H[i]);
            ++blocks;
        }
    }

    return blocks;
}

// the main function

int main()
{
    vector<int> H( {8,8,5,7,9,8,7,4,8});

    cout << solution(H) << endl;


    return 0;
}

// end
