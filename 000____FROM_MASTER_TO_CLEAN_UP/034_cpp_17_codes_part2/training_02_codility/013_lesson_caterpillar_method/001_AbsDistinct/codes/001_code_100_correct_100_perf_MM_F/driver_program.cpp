
#include <iostream>
#include <cmath>
#include <algorithm>
#include <iostream>

using std::endl;
using std::cout;
using std::abs;
using std::vector;
using std::transform;
using std::sort;

// abs function

double op_abs(double x)
{
    return abs(x);
}

// the solution

int solution(vector<int> &A)
{
    // apply the op_abs function to each element

    transform(A.begin(), A.end(), A.begin(), op_abs);

    // sort the vector

    sort(A.begin(), A.end());

    // take the unique elements

    vector<int>::iterator it = unique(A.begin(),A.end());

    // resize

    A.resize(distance(A.begin(), it));

    // get the size of the vector
    // which is the desired result;

    long long int SIZE = A.size();

    return SIZE;
}

// the main function

int main()
{
    vector<int> A = {-5, -3, -1, 0, 3, 6};

    cout << solution(A) << endl;

    return 0;
}

// END
