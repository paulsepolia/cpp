#include <iostream>
#include <numeric>
#include <vector>

using namespace std;

// the solution

int solution(vector<int> &A)
{
    long long int N = A.size();

    if (N == 0) return 1;

    if ((N == 1) && (A[0] == 2) ) return 1;
    if ((N == 1) && (A[0] == 1) ) return 2;

    double sumA = (1 + N)*(2 + static_cast<double>(N))/2.0;
    double init = 0;
    double sumB = accumulate(A.begin(),A.end(),init);

    return static_cast<int>(sumA-sumB);
}

// the main program

int main()
{
    vector<int> A = {1,2,3,4};

    cout << solution(A) << endl;

    return 0;
}

// END
