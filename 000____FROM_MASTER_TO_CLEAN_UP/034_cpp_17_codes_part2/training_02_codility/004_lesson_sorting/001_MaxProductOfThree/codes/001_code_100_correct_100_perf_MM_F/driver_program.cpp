
#include <algorithm>
#include <vector>

using namespace std;

int solution(vector<int> &A)
{
    sort(A.begin(), A.end());

    long long int SIZE = A.size();

    long long int res = 0;

    if (SIZE == 3) {
        res = A[SIZE-3] * A[SIZE-2] * A[SIZE-1];

        return res;
    }

    if (A[SIZE-1] <= 0) {
        res = A[SIZE-3] * A[SIZE-2] * A[SIZE-1];
    } else if (A[0] >= 0) {
        res = A[SIZE-3] * A[SIZE-2] * A[SIZE-1];
    } else if (A[0] <= 0 && A[1] > 0) {
        res = A[SIZE-3] * A[SIZE-2] * A[SIZE-1];
    } else if (A[0] < 0 && A[1] < 0) {
        res = max(A[SIZE-3] * A[SIZE-2] * A[SIZE-1], A[0]*A[1]*A[SIZE-1]);
    }

    return res;
}

// the main solution

int main()
{
    return 0;
}

// END
