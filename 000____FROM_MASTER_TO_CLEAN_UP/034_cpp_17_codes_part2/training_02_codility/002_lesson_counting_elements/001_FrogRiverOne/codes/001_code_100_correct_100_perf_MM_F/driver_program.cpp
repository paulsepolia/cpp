
#include <vector>
#include <iostream>

using std::vector;
using std::cout;
using std::endl;

// the solution

int solution(int X, vector<int> &A)
{
    int N = A.size();

    vector<bool> positions(X, 0); // indicate if leaves are present for positions 1 to X;

    // total sum of leave positions (1 + 2 + 3 + ... + X)

    unsigned int total = X*(1+X)/2;

    unsigned int sum = 0;

    for(int i = 0; i < N; i++) {

        if(A[i] <= X) {
            // mark each leave position only once and add it to sum
            if(!positions[A[i]-1]) {
                positions[A[i]-1] = 1;
                sum += A[i];
            }
        }

        // if all leaf positions from 1 to X exist
        // at minute i, we're done

        if(sum == total) {
            return i;
        }

    }

    return -1;
}

// the main function

int main()
{
    vector<int> A(100);

    A[0] = 1;
    A[1] = 3;
    A[2] = 1;
    A[3] = 4;
    A[4] = 2;
    A[5] = 3;
    A[6] = 5;
    A[7] = 4;

    cout << solution(5, A) << endl;

    return 0;
}

// END
