#include <iostream>

using std::endl;
using std::cout;

// the solution

int solution(int A, int B, int K)
{
    int sol = 0;

    if ((A%K) == 0) {
        sol = (B - A)/K + 1;
    } else {
        sol = (B - (A - A%K))/K;
    }

    return sol;
}

// the main function

int main()
{
    cout << solution(10,20,3) << endl;
    cout << solution(10,24,3) << endl;
    cout << solution(10,10,100) << endl;

    return 0;
}

// end
