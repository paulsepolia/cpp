#include <iostream>

using std::endl;
using std::cout;

// the solution

int solution(int A, int B, int K)
{
    long long i;
    long long k = 0;
    long long ik = 0;

    for(i = A; i <= B; i++) {
        if (((A+ik)%K) == 0) {
            k++;
        }
        ik++;
    }

    return k;
}

// the main function

int main()
{
    cout << solution(10,20,3) << endl;
    cout << solution(10,24,3) << endl;
    cout << solution(10,10,100) << endl;

    return 0;
}

// end
