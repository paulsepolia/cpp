#include <iostream>
#include <vector>

using std::endl;
using std::cout;
using std::vector;

// the solution

int solution(vector<int> &A)
{
    unsigned int i;
    unsigned int counter = 0;
    const unsigned int DIM = A.size();
    unsigned int ik = 0;

    for (i = 0; i != DIM; i++) {
        if (A[i] == 1) {
            counter = counter + i-ik;
            ik++;
        }
    }

    if (counter > 1000000000) {
        return -1;
    }

    return counter;
}

// the main function

int main()
{
    vector<int> A( {0,1,0,1,1});

    cout << " --> example 1 --> " << solution(A) << endl;

    return 0;
}

// end
