#include <iostream>
#include <chrono>
#include <iomanip>
#include "/opt/intel/mkl/include/mkl.h"

constexpr auto ROWS{(uint32_t(2 * 1024))};
constexpr auto COLS{ROWS};
constexpr auto NT(4);

class benchmark_timer {
public:

    explicit benchmark_timer(const double &res) :
            _res(res),
            t1{std::chrono::steady_clock::now()},
            t2{std::chrono::steady_clock::now()} {};

    ~benchmark_timer() {
        t2 = std::chrono::steady_clock::now();
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count()};
        std::cout << " --> res = " << _res << std::endl;
        std::cout << " --> time used (secs) = " << total_time << std::endl;
    }

    auto set_res(const double &res) -> void {
        _res = res;
    }

private:

    double _res{};
    decltype(std::chrono::steady_clock::now()) t1{};
    decltype(std::chrono::steady_clock::now()) t2{};
};

auto allocate_dense_matrix(double **mat) -> double ** {

    mat = new double *[ROWS];

    for (uint32_t i = 0; i < ROWS; i++) {
        mat[i] = new double[COLS];
    }

    return mat;
}

auto deallocate_dense_matrix(double **mat) -> void {

    for (uint32_t i = 0; i < ROWS; i++) {
        delete[] mat[i];
    }

    delete[] mat;
}

auto build_dense_matrix(double **mat) -> double ** {

    for (uint32_t i = 0; i < ROWS; i++) {
        for (uint32_t j = 0; j < COLS; j++) {
            mat[i][j] = (double) (i * COLS + j);
        }
    }

    return mat;
}

auto allocate_dense_matrix_mkl(double *mat_mkl) -> double * {

    mat_mkl = (double *) mkl_malloc(ROWS * COLS * sizeof(double), 64);

    return mat_mkl;
}

auto deallocate_dense_matrix_mkl(double *mat_mkl) -> void {
    mkl_free(mat_mkl);
}

auto build_dense_matrix_mkl(double *mat_mkl) -> double * {

    for (uint32_t i = 0; i < ROWS; i++) {
        for (uint32_t j = 0; j < COLS; j++) {
            mat_mkl[i * COLS + j] = (double) (i * COLS + j);
        }
    }

    return mat_mkl;
}

// basic serial implementation

double **mat_mult_v1(double **mat1, double **mat2, double **mat_res) {

    for (uint32_t i = 0; i < ROWS; i++) {
        for (uint32_t j = 0; j < COLS; j++) {
            for (uint32_t k = 0; k < ROWS; k++) {
                mat_res[i][j] += mat1[i][k] * mat2[k][j];
            }
        }
    }

    return mat_res;
}

// loop interchange optimization - serial implementation

double **mat_mult_v2(double **mat1, double **mat2, double **mat_res) {

    for (uint32_t i = 0; i < ROWS; i++) {
        for (uint32_t k = 0; k < ROWS; k++) {
            for (uint32_t j = 0; j < COLS; j++) {
                mat_res[i][j] += mat1[i][k] * mat2[k][j];
            }
        }
    }

    return mat_res;
}

// basic OpenMP implementation

double **mat_mult_v3(double **mat1, double **mat2, double **mat_res) {

#pragma omp parallel for \
    shared(mat1, mat2, mat_res) \
    num_threads(NT)
    for (uint32_t i = 0; i < ROWS; i++) {
        for (uint32_t j = 0; j < COLS; j++) {
            for (uint32_t k = 0; k < ROWS; k++) {
                mat_res[i][j] += mat1[i][k] * mat2[k][j];
            }
        }
    }

    return mat_res;
}

// loop interchange optimization - OpenMP implementation

double **mat_mult_v4(double **mat1, double **mat2, double **mat_res) {

#pragma omp parallel for \
    shared(mat1, mat2, mat_res) \
    num_threads(NT)
    for (uint32_t i = 0; i < ROWS; i++) {
        for (uint32_t k = 0; k < ROWS; k++) {
            for (uint32_t j = 0; j < COLS; j++) {
                mat_res[i][j] += mat1[i][k] * mat2[k][j];
            }
        }
    }

    return mat_res;
}

// MKL dgemm

double *mat_mult_v5(const double *mat1_mkl, const double *mat2_mkl, double *mat_res_mkl) {

    constexpr double alpha = 1.0;
    constexpr double beta = 0.0;

    cblas_dgemm(CblasRowMajor,
                CblasNoTrans,
                CblasNoTrans,
                ROWS,
                ROWS,
                ROWS,
                alpha,
                mat1_mkl,
                ROWS,
                mat2_mkl,
                ROWS,
                beta,
                mat_res_mkl,
                ROWS);

    return mat_res_mkl;
}


auto main() -> int {

    std::cout << std::fixed;
    std::cout << std::setprecision(8);


    auto mat1{(double **) (nullptr)};
    auto mat2{(double **) (nullptr)};
    auto mat3{(double **) (nullptr)};

    auto mat1_mkl{(double *) (nullptr)};
    auto mat2_mkl{(double *) (nullptr)};
    auto mat3_mkl{(double *) (nullptr)};

    {
        std::cout << " ---->> 1 -->> allocate dense matrices" << std::endl;
        std::cout.flush();

        benchmark_timer ot(0);
        mat1 = allocate_dense_matrix(mat1);
        mat2 = allocate_dense_matrix(mat2);
        mat1_mkl = allocate_dense_matrix_mkl(mat1_mkl);
        mat2_mkl = allocate_dense_matrix_mkl(mat2_mkl);

        ot.set_res(mat1[0][0] + mat2[0][0] + mat1_mkl[0] + mat2_mkl[0]);
    }

    {
        std::cout << " ---->> 2 -->> build dense matrices" << std::endl;

        benchmark_timer ot(0);
        mat1 = build_dense_matrix(mat1);
        mat2 = build_dense_matrix(mat2);
        mat1_mkl = build_dense_matrix_mkl(mat1_mkl);
        mat2_mkl = build_dense_matrix_mkl(mat2_mkl);

        ot.set_res(mat1[0][0] + mat2[0][0] + mat1_mkl[0] + mat2_mkl[0]);
    }

    {
        std::cout << " ---->> 3 -->> multiply dense matrices --> v1 (Basic + serial)" << std::endl;

        mat3 = allocate_dense_matrix(mat3);

        benchmark_timer ot(0);

        mat3 = mat_mult_v1(mat1, mat2, mat3);

        ot.set_res(mat1[0][0] + mat2[0][0] + mat3[0][0]);

        std::cout << " --> mat_res[0][0] = " << mat3[0][0] << std::endl;
        std::cout.flush();
    }

    {
        std::cout << " ---->> 4 -->> multiply dense matrices --> v2 (Loop interchange)" << std::endl;

        mat3 = allocate_dense_matrix(mat3);

        benchmark_timer ot(0);

        mat3 = mat_mult_v2(mat1, mat2, mat3);

        ot.set_res(mat1[0][0] + mat2[0][0] + mat3[0][0]);

        std::cout << " --> mat_res[0][0] = " << mat3[0][0] << std::endl;
        std::cout.flush();
    }

    {
        std::cout << " ---->> 5 -->> multiply dense matrices --> v3 (Basic + OpenMP)" << std::endl;

        mat3 = allocate_dense_matrix(mat3);

        benchmark_timer ot(0);

        mat3 = mat_mult_v3(mat1, mat2, mat3);

        ot.set_res(mat1[0][0] + mat2[0][0] + mat3[0][0]);

        std::cout << " --> mat_res[0][0] = " << mat3[0][0] << std::endl;
        std::cout.flush();
    }

    {
        std::cout << " ---->> 6 -->> multiply dense matrices --> v4 (Loop interchange + OpenMP)" << std::endl;

        mat3 = allocate_dense_matrix(mat3);

        benchmark_timer ot(0);

        mat3 = mat_mult_v4(mat1, mat2, mat3);

        ot.set_res(mat1[0][0] + mat2[0][0] + mat3[0][0]);

        std::cout << " --> mat_res[0][0] = " << mat3[0][0] << std::endl;
        std::cout.flush();
    }

    {
        std::cout << " ---->> 7 -->> multiply dense matrices --> v5 (Gnu+MKL)" << std::endl;

        mat3_mkl = allocate_dense_matrix_mkl(mat3_mkl);

        benchmark_timer ot(0);

        mat3_mkl = mat_mult_v5(mat1_mkl, mat2_mkl, mat3_mkl);

        ot.set_res(mat1_mkl[0] + mat2_mkl[0] + mat3_mkl[0]);

        std::cout << " --> mat_res[0][0] = " << mat3_mkl[0] << std::endl;
        std::cout.flush();
    }

    deallocate_dense_matrix(mat1);
    deallocate_dense_matrix(mat2);
    deallocate_dense_matrix(mat3);
    deallocate_dense_matrix_mkl(mat1_mkl);
    deallocate_dense_matrix_mkl(mat2_mkl);
    deallocate_dense_matrix_mkl(mat3_mkl);

}
