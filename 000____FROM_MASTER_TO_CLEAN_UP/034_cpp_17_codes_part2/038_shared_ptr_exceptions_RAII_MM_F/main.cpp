#include <iostream>
#include <cstdint>
#include <iomanip>
#include <chrono>
#include <memory>
#include <vector>

class A {
public:

    explicit A(std::vector<double> v) : _v{std::move(v)} {

        std::cout << " --> A --> constructor" << std::endl;
        _pd = new double(200.0);

        // prevent the memory leak by attaching a shared pointer to the raw pointer
        //std::shared_ptr<double> spd(_pd);

        throw std::exception();
        // if throw during construction of the object then the object is incomplete
        // and the is no call of destructor to free up the heap
        // so we get back a memory leak
    }

    virtual ~A() {
        delete _pd;
        std::cout << " --> A --> destructor" << std::endl;
    }

    A(const A &obj) : _v{obj._v}, _pd(new double(*obj._pd)) {
        std::cout << " --> A --> copy constructor" << std::endl;
    }

    A(A &&obj) noexcept : _v{std::move(obj._v)} {

        _pd = obj._pd;
        obj._pd = nullptr;
        std::cout << " --> A --> move constructor" << std::endl;
    }

    A &operator=(const A &obj) {

        std::cout << " --> A --> copy assignment operator" << std::endl;

        if (this != &obj) {
            _v = obj._v;
            _pd = new double(*obj._pd);
        }
        return *this;
    }

    A &operator=(A &&obj) noexcept {

        std::cout << " --> A --> move assignment operator" << std::endl;

        if (this != &obj) {
            _v = std::move(obj._v);
            _pd = obj._pd;
            obj._pd = nullptr;
        }
        return *this;
    }

private:

    std::vector<double> _v;
    double *_pd = nullptr;
};

int main() {

    {
        std::cout << " --> example --> 1 --> start" << std::endl;

        try {
            std::shared_ptr<A> sp1(new A({1.0, 2.0, 3.0, 4.0}));
        }
        catch (const std::exception &e) {
            std::cout << " --> pgg --> 1 --> " << e.what() << std::endl;
        }

        std::cout << " --> example --> 1 --> end" << std::endl;
    }

    {
        std::cout << " --> example --> 2 --> start" << std::endl;

        try {
            std::shared_ptr<A> sp2(std::make_shared<A>(A({1.0, 2.0, 3.0, 4.0})));
        }
        catch (const std::exception &e) {
            std::cout << " --> pgg --> 2 --> " << e.what() << std::endl;
        }

        std::cout << " --> example --> 2 --> end" << std::endl;
    }
}