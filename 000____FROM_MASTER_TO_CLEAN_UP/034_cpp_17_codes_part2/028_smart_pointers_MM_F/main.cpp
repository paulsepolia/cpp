#include <iostream>
#include <iomanip>
#include <memory>
#include <cmath>

int main() {

    {
        std::cout << " --> example --> 1 --> start" << std::endl;

        std::unique_ptr<double> up1 = std::make_unique<double>(200.0);

        std::cout << up1.get() << std::endl;
        std::cout << *up1 << std::endl;

        std::shared_ptr<double> sp1;

        std::cout << " --> sp1.use_count() = " << sp1.use_count() << std::endl;

        sp1 = std::move(up1);

        std::cout << " --> sp1.use_count() = " << sp1.use_count() << std::endl;

        std::cout << sp1.get() << std::endl;
        std::cout << *sp1 << std::endl;

        std::shared_ptr<double> sp2(sp1);

        std::cout << " --> sp1.use_count() = " << sp1.use_count() << std::endl;
        std::cout << " --> sp2.use_count() = " << sp2.use_count() << std::endl;

        std::cout << sp1.get() << std::endl;
        std::cout << *sp1 << std::endl;
        std::cout << sp2.get() << std::endl;
        std::cout << *sp2 << std::endl;

        std::cout << " --> example --> 1 --> end" << std::endl;
    }

    {
        std::cout << " --> example --> 2 --> start" << std::endl;

        std::unique_ptr<double> up = std::make_unique<double>(200.0);

        std::cout << up.get() << std::endl;
        std::cout << *up << std::endl;

        *up = 300;

        std::cout << up.get() << std::endl;
        std::cout << *up << std::endl;

        std::cout << " --> example --> 2 --> end" << std::endl;
    }

    {
        std::cout << " --> example --> 3 --> start" << std::endl;

        std::unique_ptr<double> up1 = std::make_unique<double>(200.0);

        std::cout << up1.get() << std::endl;
        std::cout << *up1 << std::endl;

        *up1 = 300;

        std::cout << up1.get() << std::endl;
        std::cout << *up1 << std::endl;

        up1.reset(new double(400));

        std::cout << up1.get() << std::endl;
        std::cout << *up1 << std::endl;

        std::cout << " --> example --> 3 --> end" << std::endl;
    }
}