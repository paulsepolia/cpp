#include <boost/thread.hpp>
#include <iostream>

int32_t index_local(0);

void print_fun() {
    std::cout << "------------------>> " << ++index_local << std::endl;
}

int main() {

    const int32_t I_DO_MAX(10);

    boost::thread_group threads{};

    for (uint32_t i(0); i != I_DO_MAX; i++) {
        threads.create_thread(print_fun);
    }

    threads.join_all();

    return 0;
}
