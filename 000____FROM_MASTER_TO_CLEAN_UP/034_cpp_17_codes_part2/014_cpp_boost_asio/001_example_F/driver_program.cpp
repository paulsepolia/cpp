#include <boost/thread.hpp>
#include <boost/chrono.hpp>
#include <iostream>

const int32_t DO_MAX(10);
const int32_t TIME_SLEEP1(500);
const int32_t TIME_SLEEP2(20*500);

void print1() {
    for (int32_t i(0); i < DO_MAX; i++) {
        boost::this_thread::sleep_for(boost::chrono::microseconds{TIME_SLEEP1});
        std::cout << "--> print --> 1 --> line --> " << i << std::endl;
    }
}

void print2() {
    for (int32_t i(0); i < DO_MAX; i++) {
        boost::this_thread::sleep_for(boost::chrono::microseconds{TIME_SLEEP2});
        std::cout << "--> print --> 2 --> line --> " << i << std::endl;
    }
}

int main() {

    boost::thread_group threads{};
    threads.create_thread(print1);
    threads.create_thread(print2);
    threads.join_all();

    return 0;
}