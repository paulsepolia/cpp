#!/bin/bash

  icpc  -O3                \
        -Wall              \
        -pthread           \
        -qopenmp           \
        -std=gnu++14       \
        -pedantic          \
        -pedantic-errors   \
        driver_program.cpp \
        -L/opt/boost/1.64.0/lib -lboost_thread \
        -L/opt/boost/1.64.0/lib -lboost_filesystem \
        -L/opt/boost/1.64.0/lib -lboost_system \
        -o x_intel
