#include <iostream>
#include <memory>
#include <map>
#include <string>

class A {
public:

    explicit A(std::string name) : _name(std::move(name)) {}

    const auto &get_name() const {
        return _name;
    }

    auto set_name(std::string name) {
        _name = std::move(name);
    }

    virtual ~A() {
        std::cout << " --> destructor --> " << _name << std::endl;
    }

private:

    std::string _name;
};

int main() {

    std::map<std::shared_ptr<A>, std::string> m1;

    {
        std::cout << "step --> 1" << std::endl;

        auto spa = std::make_shared<A>(A("name1"));

        std::cout << "step --> 2" << std::endl;

        m1.insert({spa, "1"});

        std::cout << "step --> 3" << std::endl;

        spa.reset(new A("name2"));

        m1.insert({spa, "2"});

        spa.reset(new A("name3"));

        m1.insert({spa, "3"});

        for (const auto &el: m1) {
            std::cout << el.first->get_name() << " --> " << el.first << " --> " << el.second << std::endl;
        }
    }

    for (const auto &el: m1) {
        std::cout << el.first->get_name() << " --> "
                  << el.first << " --> "
                  << el.second << std::endl;
    }

    {
        for (auto &el: m1) {

            el.first->set_name("new global name");

            std::cout << el.first->get_name() << " --> "
                      << el.first << " --> "
                      << el.second << std::endl;
        }
    }

    for (const auto &el: m1) {
        std::cout << el.first->get_name() << " --> "
                  << el.first << " --> "
                  << el.second << std::endl;
    }

    auto a1 = std::make_shared<double>(100);
    auto a2 = std::make_shared<double>(200);

    std::cout << "pgg --> a1 = " << a1 << std::endl;
    std::cout << "pgg --> a2 = " << a2 << std::endl;
    std::cout << "pgg --> (a1 < a2) = " << (a1 < a2) << std::endl;
    std::cout << "pgg --> (a1 >= a2) = " << (a1 >= a2) << std::endl;
    std::cout << "pgg --> (a1 == a2) = " << (a1 == a2) << std::endl;

    return 0;
}