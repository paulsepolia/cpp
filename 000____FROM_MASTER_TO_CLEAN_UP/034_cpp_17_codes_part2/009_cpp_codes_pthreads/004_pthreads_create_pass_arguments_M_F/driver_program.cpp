//===================================================================//
//  A "hello world" Pthreads program which demonstrates one safe way //
//  to pass arguments to threads during thread creation.             //
//===================================================================//

#include <iostream>
#include <pthread.h>
#include <ctime>
#include <cerrno>
#include <unistd.h>

#define NUM_THREADS 8

using std::cout;
using std::endl;

// an array

char * messages [NUM_THREADS];

// a function

void * PrintHello(void * threadid)
{
    int * id_ptr;
    int taskid;
    struct timespec req;
    struct timespec rem;

    int msec = 1000;

    req.tv_sec = msec / 1000;
    req.tv_nsec = (msec % 1000) * 1000000;

    nanosleep(&req, &rem);

    id_ptr = static_cast<int *>(threadid);
    taskid = *id_ptr;
    cout << " Thread " << taskid << " : " << messages[taskid] << endl;
    pthread_exit(NULL);
}

// the main function

int main()
{
    pthread_t threads [ NUM_THREADS ];
    int * taskids [ NUM_THREADS ];
    int rc;
    int t;

    messages[0] = (char*)"English: Hello World!";
    messages[1] = (char*)"French: Bonjour, le monde!";
    messages[2] = (char*)"Spanish: Hola al mundo";
    messages[3] = (char*)"Klingon: Nuq neH!";
    messages[4] = (char*)"German: Guten Tag, Welt!";
    messages[5] = (char*)"Russian: Zdravstvytye, mir!";
    messages[6] = (char*)"Japan: Sekai e konnichiwa!";
    messages[7] = (char*)"Latin: Orbis, te saluto!";

    for(t = 0; t < NUM_THREADS; t++) {
        taskids[t] = (int *) malloc(sizeof(int));
        *taskids[t] = t;

        cout << " Creating thread :" << t << endl;

        rc = pthread_create(&threads[t], NULL, PrintHello, (void *) taskids[t]);

        if (rc) {
            cout << " ERROR; return code from pthread_create() is : " << rc << endl;
            exit(-1);
        }
    }

    pthread_exit(NULL);

    return 0;
}

//======//
// FINI //
//======//
