
#include <iostream>
#include <string>
#include <map>
#include <random>

using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::map;
using std::random_device;
using std::uniform_int_distribution;

//===================//
// the main function //
//===================//

int main()
{
    random_device rd;
    map<int, int> hist;
    uniform_int_distribution<int> dist(0, 9);

    for (int n = 0; n < 20000; ++n) {
        ++hist[dist(rd)]; // note: demo only: the performance of many
        // implementations of random_device degrades sharply
        // once the entropy pool is exhausted. For practical use
        // random_device is generally only used to seed
        // a PRNG such as mt19937
    }
    for (auto p : hist) {
        cout << p.first << " : " << string(p.second/100, '*') << endl;
    }
}

//=====//
// END //
//=====//
