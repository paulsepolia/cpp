// condition_variable example

#include <iostream>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <vector>

constexpr auto NT{(size_t) 10};

auto mtx{std::mutex{}};
auto cv1{std::condition_variable{}};
auto cv2{std::condition_variable{}};
auto ready{(bool) false};

auto print_id1(uint32_t id) -> void {

    std::unique_lock<std::mutex> lck(mtx);

    std::cout << " --> thread --> before --> thread id = " << id << std::endl;
    std::cout.flush();

    while (!ready) {
        std::cout << " --> I am a thread waiting here --> thread id = " << id << std::endl;
        std::cout.flush();
        cv1.wait(lck);
        std::cout << " --> I am a thread and NOT waiting anymore --> thread id = " << id << std::endl;
        std::cout.flush();
    }

    std::cout << " --> thread --> after --> " << id << std::endl;
    std::cout.flush();
}

auto print_id2(uint32_t id) -> void {

    std::unique_lock<std::mutex> lck(mtx);

    std::cout << " --> thread --> before --> thread id = " << id << std::endl;
    std::cout.flush();

    while (!ready) {
        std::cout << " --> I am a thread waiting here --> thread id = " << id << std::endl;
        std::cout.flush();
        cv2.wait(lck);
        std::cout << " --> I am a thread and NOT waiting anymore --> thread id = " << id << std::endl;
        std::cout.flush();
    }

    std::cout << " --> thread --> after --> " << id << std::endl;
    std::cout.flush();
}

auto go1() -> void {

    std::cout << " --> I am in go1() function now..." << std::endl;

    std::unique_lock<std::mutex> lck(mtx);

    ready = true;

    std::cout << " --> I am in go1() function now... below lock" << std::endl;

    std::this_thread::sleep_for(std::chrono::seconds(1));

    std::cout << " --> mark --> notify_one() --> 1" << std::endl;

    cv1.notify_one();

    std::this_thread::sleep_for(std::chrono::seconds(1));

    std::cout << " --> mark --> notify_one() --> 2" << std::endl;

    cv1.notify_one();
}

auto go2() -> void {

    std::cout << " --> I am in go2() function now..." << std::endl;

    std::unique_lock<std::mutex> lck(mtx);

    ready = true;

    std::cout << " --> I am in go2() function now... below lock" << std::endl;

    std::this_thread::sleep_for(std::chrono::seconds(1));

    std::cout << " --> mark --> notify_one() --> 1" << std::endl;

    cv2.notify_one();

    std::this_thread::sleep_for(std::chrono::seconds(1));

    std::cout << " --> mark --> notify_one() --> 2" << std::endl;

    cv2.notify_one();
}

auto go_again1() -> void {

    std::cout << " --> I am in go_again1() function now..." << std::endl;

    std::unique_lock<std::mutex> lck(mtx);

    ready = true;

    std::cout << " --> waiting 20 seconds to notify all..." << std::endl;

    std::this_thread::sleep_for(std::chrono::seconds(20));

    cv1.notify_all();
}

auto go_again2() -> void {

    std::cout << " --> I am in go_again2() function now..." << std::endl;

    std::unique_lock<std::mutex> lck(mtx);

    ready = true;

    std::cout << " --> waiting 20 seconds to notify all..." << std::endl;

    std::this_thread::sleep_for(std::chrono::seconds(20));

    cv2.notify_all();
}

auto main() -> int {

    auto threads{std::vector<std::thread>{}};

    std::cout << " --> spawn threads here..." << std::endl;

    for (size_t i = 0; i < NT; i++) {
        threads.emplace_back(std::thread(print_id1, i));
    }

    for (size_t i = 0; i < NT; i++) {
        threads.emplace_back(std::thread(print_id2, i));
    }

    std::cout << " --> sleep the main thread here to give ";
    std::cout << "time all the spawn threads to reach the 'wait'..." << std::endl;

    std::this_thread::sleep_for(std::chrono::seconds(20));

    go1();

    go2();

    go_again1();

    go_again2();

    std::cout << " --> join threads vector" << std::endl;

    for (auto &th : threads) {
        th.join();
    }
}