#include <iostream>
#include <variant>
#include <vector>

int main() {

    std::vector<std::variant<double, std::string>> vr;

    vr.emplace_back(111.1);
    vr.emplace_back("p1");
    vr.emplace_back("p2");
    vr.emplace_back("p3");
    vr.emplace_back(222.2);

    const auto get_vr = [](const auto &el) {
        std::cout << el << std::endl;
    };

    for (const auto &el : vr) {
        std::visit(get_vr, el);
    }
}