//==========//
// includes //
//==========//

#include <iostream>
#include <algorithm>
#include <vector>
#include <list>
#include <deque>
#include <cmath>
#include <iomanip>
#include <ctime>
#include <thread>

using std::cout;
using std::cin;
using std::endl;
using std::list;
using std::vector;
using std::deque;
using std::fixed;
using std::setprecision;
using std::showpoint;
using std::sort;
using std::thread;
using std::ref;

//======//
// FINI //
//======//
