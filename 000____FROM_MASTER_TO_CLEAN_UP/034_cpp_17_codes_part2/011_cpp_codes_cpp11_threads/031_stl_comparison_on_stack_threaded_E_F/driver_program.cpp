//======================//
// STL clear and delete //
//======================//

#include "includes.h"
#include "bench_fun.h"

// the main program

int main()
{
    // variables and parameters

    const long I_MAX = static_cast<long>(pow(10.0, 7.0));
    const int NUM_THREADS = 8;

    long k;
    int i;
    clock_t t1;
    clock_t t2;
    const long K_MAX = static_cast<long>(pow(10.0, 6.0));
    double tFun[NUM_THREADS];
    vector<thread> vecThreads;

    // main bench loop

    for (k = 0; k < K_MAX; k++) {
        cout << endl;
        cout << "------------------------------------------------------------>>> " << k << endl;
        cout << endl;

        t1 = clock();

        // set to zero the time counters

        for (i = 0; i < NUM_THREADS; i++) {
            tFun[i] = 0.0;
        }

        // spawn threads

        for (i = 0; i < NUM_THREADS; i++) {
            vecThreads.push_back(thread(&bench_fun, I_MAX, ref(tFun[i])));
        }

        // join threads

        for (i = 0; i < NUM_THREADS; i++) {
            vecThreads[i].join();
        }

        t2 = clock();

        double tMainAll = 0.0;

        tMainAll = (t2-t1)/static_cast<double>(CLOCKS_PER_SEC);

        // the time in main and in function

        double tFunAll = 0.0;
        for (i = 0; i < NUM_THREADS; i++) {
            tFunAll = tFunAll + tFun[i];
        }

        // clear vector

        cout << "xx1 --> clear the threads vector ..." << endl;

        vecThreads.clear();

        cout << "xx2 --> total time for benchmark function in main --> " << tMainAll << endl;
        cout << "xx3 --> total time for benchmark function         --> " << tFunAll << endl;
        cout << "xx4 --> total deallocation time on stack          --> " << tMainAll - tFunAll << endl;
    }

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
