//====================//
// benchmark function //
//====================//

#include "includes.h"

// the main program

void bench_fun(const long I_MAX, double & tTotal)
{
    // variables and parameters

    long i;
    clock_t t1;
    clock_t t2;
    double tAll [7];

    // set the output

    cout << fixed;
    cout << setprecision(5);
    cout << showpoint;

    // main bench loop

    list<double>   listA;
    vector<double> vecA;
    deque<double>  deqA;

    // build the vector

    t1 = clock();

    cout << "  1 --> build the vector ..." << endl;

    for (i = 0; i < I_MAX; i++) {
        vecA.push_back(static_cast<double>(cos(i)));
    }

    t2 = clock();

    tAll[0] = (t2-t1)/static_cast<double>(CLOCKS_PER_SEC);

    cout << "  2 --> done with the vector                      --> " << tAll[0] << endl;

    // build the list

    t1 = clock();

    cout << "  3 --> build the list ..." << endl;

    for (i = 0; i < I_MAX; i++) {
        listA.push_back(static_cast<double>(cos(i)));
    }

    t2 = clock();

    tAll[1] = (t2-t1)/static_cast<double>(CLOCKS_PER_SEC);

    cout << "  4 --> done with the list                        --> " << tAll[1] << endl;

    // build the deque

    t1 = clock();

    cout << "  5 --> build the deque ..." << endl;

    for (i = 0; i < I_MAX; i++) {
        deqA.push_back(static_cast<double>(cos(i)));
    }

    t2 = clock();

    tAll[2] = (t2-t1)/static_cast<double>(CLOCKS_PER_SEC);

    cout << "  6 --> done with the deque                       --> " << tAll[2] << endl;

    // sort the vector

    t1 = clock();

    cout << "  7 --> sort the vector ..." << endl;

    sort(vecA.begin(), vecA.end());

    t2 = clock();

    tAll[3] = (t2-t1)/static_cast<double>(CLOCKS_PER_SEC);

    cout << "  8 --> done with the vector                      --> " << tAll[3] << endl;

    sort(vecA.begin(), vecA.end());

    // sort the list

    t1 = clock();

    cout << "  9 --> sort the list ..." << endl;

    listA.sort();

    t2 = clock();

    tAll[4] = (t2-t1)/static_cast<double>(CLOCKS_PER_SEC);

    cout << " 10 --> done with the list                        --> " << tAll[4] << endl;

    // sort the deque

    t1 = clock();

    cout << " 11 --> sort the deque ..." << endl;

    sort(deqA.begin(), deqA.end());

    t2 = clock();

    tAll[5] = (t2-t1)/static_cast<double>(CLOCKS_PER_SEC);

    cout << " 12 --> done with the deque                       --> " << tAll[5] << endl;

    tAll[6] = 0.0;
    for (int i = 0; i <= 5; i++) {
        tAll[6] = tAll[6] + tAll[i];
    }

    tTotal = tAll[6];

    cout << " 13 --> done with the benchmark function          --> " << tTotal << endl;
}

//======//
// FINI //
//======//
