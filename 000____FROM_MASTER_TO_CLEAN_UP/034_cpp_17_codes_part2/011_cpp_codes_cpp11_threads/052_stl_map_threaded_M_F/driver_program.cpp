
//=========//
// stl map //
//=========//

#include <iostream>
#include <map>
#include <cmath>
#include <vector>
#include <thread>

using std::endl;
using std::cout;
using std::cin;
using std::map;
using std::pow;
using std::pair;
using std::iterator;
using std::vector;
using std::thread;

// the benchmark function

void benchFun(const long DIMEN_MAX)
{
    // local variables and parameters

    const long K_MAX = static_cast<long>(pow(10.0, 5.0));

    map<long, long>::iterator pA;

    for (long k = 0; k < K_MAX; k++) {
        // counter

        cout << "-------------------------------------------->> " << k << endl;

        // map declaration

        map<long, long> * mA = new map<long, long>;

        // build map

        cout << " --> build the map" << endl;

        for (long i = 0; i < DIMEN_MAX; i++) {
            mA->insert(pair<long, long>(i,i+1));
        }

        // display some contents of map

        cout << " --> display some elements" << endl;

        pA = mA->begin();

        for (long i = 0 ; i < 1; i++, pA++) {
            cout <<  " --> " << pA->first << ", " << pA->second << endl;
        }

        // erase the map

        cout << " --> erase the map" << endl;

        mA->erase(mA->begin(), mA->end());

        // delete the maps

        cout << " --> delete the map" << endl;

        delete mA;
    }
}

// the main function

int main()
{
    // local variables and parameters

    const long int DIMEN_MAX = static_cast<long int>(pow(10.0, 7.2));
    const int NUM_THREADS = 4;
    vector<thread> vecThreads;

    // span the threads

    for (int j = 0; j < NUM_THREADS; j++) {
        vecThreads.push_back(thread(benchFun, DIMEN_MAX));
    }

    // join the threads

    for (auto& t : vecThreads) {
        t.join();
    }

    // clear the threads vector

    vecThreads.clear();

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
