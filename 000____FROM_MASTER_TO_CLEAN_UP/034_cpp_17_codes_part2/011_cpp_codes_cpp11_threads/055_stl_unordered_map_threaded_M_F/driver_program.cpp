
//=========//
// stl map //
//=========//

#include <iostream>
#include <unordered_map>
#include <cmath>
#include <vector>
#include <thread>
#include <iomanip>

using std::endl;
using std::cout;
using std::cin;
using std::unordered_map;
using std::pow;
using std::pair;
using std::iterator;
using std::vector;
using std::thread;
using std::cos;
using std::setprecision;
using std::showpos;
using std::showpoint;

// the benchmark function

void benchFun(const long DIMEN_MAX)
{
    // local variables and parameters

    const long K_MAX = static_cast<long>(pow(10.0, 5.0));
    const int I_MAX = 10;
    unordered_map<double, long>::iterator pA;

    for (long k = 0; k < K_MAX; k++) {
        // counter

        cout << "-------------------------------------------->> " << k << endl;

        // unordered map declaration

        unordered_map<double, long> * mA = new unordered_map<double, long>;

        // build unordered_map

        cout << " --> build the unordered map" << endl;

        for (long i = 0; i < DIMEN_MAX; i++) {
            mA->insert(pair<double, long>(cos(static_cast<double>(i)),i+1));
        }

        // display some contents of map

        cout << " --> display some elements" << endl;

        pA = mA->begin();

        for (long i = 0 ; i < I_MAX; i++, pA++) {
            cout << pA->first << ", " << pA->second << endl;
        }

        // erase the map

        cout << " --> erase the unordered map" << endl;

        for (long i = 0; i < DIMEN_MAX; i++) {
            mA->erase(mA->find(cos(static_cast<double>(i))));
        }

        // display the size

        cout << " --> unordered map size --> (*mA).size() = " << mA ->size() << endl;

        // delete the maps

        cout << " --> delete the unordered map" << endl;

        delete mA;
    }
}

// the main function

int main()
{
    // local variables and parameters

    const long int DIMEN_MAX = static_cast<long>(pow(10.0, 7.2));
    const int NUM_THREADS = 4;
    vector<thread> vecThreads;

    // set the output format

    cout << setprecision(20);
    cout << showpos;
    cout << showpoint;

    // span the threads

    for (int j = 0; j < NUM_THREADS; j++) {
        vecThreads.push_back(thread(benchFun, DIMEN_MAX));
    }

    // join the threads

    for (auto& t : vecThreads) {
        t.join();
    }

    // clear the threads vector

    vecThreads.clear();

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
