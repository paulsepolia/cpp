
//================================//
// storing class objects in a map //
//================================//

// use a map to create a phone directory

#include <iostream>
#include <map>
#include <string>
#include <vector>
#include <thread>
#include <cmath>
#include <iomanip>

using std::cout;
using std::cin;
using std::endl;
using std::map;
using std::string;
using std::pair;
using std::vector;
using std::thread;
using std::pow;
using std::to_string;
using std::setprecision;
using std::showpos;
using std::showpoint;

// this is the key class

class name {
public:
    name() : str ("") {}
    explicit name(string s) : str(s) {}
    string get() const
    {
        return str;
    }

private:
    string str;
};

// must define less than relative to name objects

bool operator < (name a, name b)
{
    return a.get() < b.get();
}

// this is the value class

class phoneNum {
public:

    phoneNum() : str ("") {}
    explicit phoneNum(string s) : str (s) {}
    string get() const
    {
        return str;
    }

private:
    string str;
};

// the benchmark function

void benchFun(const long DIMEN_MAX)
{
    // local variables and parameters

    const long K_MAX = static_cast<long>(pow(10.0, 6.0));
    const int I_MAX = 20;
    map<name, phoneNum>::iterator p;

    for (long k = 0; k < K_MAX; k++) {
        // counter

        cout << "------------------------------------------>> " << k << endl;

        map<name, phoneNum>::iterator p;
        map<name, phoneNum> * directory = new map<name, phoneNum>;

        // put names and numbers into map

        cout << " --> build the map with the objects" << endl;

        for (long i = 0; i < DIMEN_MAX; i++) {
            directory->insert(pair<name, phoneNum>(name(to_string(i)), phoneNum(to_string(i+1))));
        }

        // output the contents of the directory

        cout << " --> display the first " << I_MAX << " some elements ..." << endl;

        p = directory->begin();

        for (int i = 0; i < I_MAX; i++, p++) {
            cout << " name : " << p->first.get() << ", phone : " <<  p->second.get() << endl;
        }

        // delete the directory

        cout << " --> delete the map with the objects" << endl;

        delete directory;
    }
}

// the main function

int main()
{
    // local variables and parameters

    const long int DIMEN_MAX = static_cast<long>(pow(10.0, 6.5));
    const int NUM_THREADS = 4;
    vector<thread> vecThreads;

    // set the output format

    cout << setprecision(20);
    cout << showpos;
    cout << showpoint;

    // span the threads

    for (int j = 0; j < NUM_THREADS; j++) {
        vecThreads.push_back(thread(benchFun, DIMEN_MAX));
    }

    // join the threads

    for (auto& t : vecThreads) {
        t.join();
    }

    // clear the threads vector

    vecThreads.clear();

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
