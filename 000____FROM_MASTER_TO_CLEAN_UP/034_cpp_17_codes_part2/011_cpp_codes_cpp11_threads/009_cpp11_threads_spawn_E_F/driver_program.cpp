//================//
// C++11, Threads //
//================//

#include <iostream>
#include <ctime>
#include <cmath>
#include <iomanip>
#include <thread>

// a function

void funA(int &i)
{
    i = i%20;
}

void funB(int i)
{
    i = i%20;
}

// the main function

int main()
{
    // local variables and parameters

    int i;
    int i1;
    int i2;
    const int I_MAX = static_cast<int>(pow(10.0, 7.0));

    // setting the output format

    std::cout << std::fixed;
    std::cout << std::setprecision(5);
    std::cout << std::showpos;
    std::cout << std::showpoint;

    // the main do-loop

    for(i = 0; i < I_MAX; i++) {
        i1 = +i;
        i2 = -i;
        std::thread th1(funA, std::ref(i1));
        std::thread th2(funB, i2);
        std::cout << " --> th1 --> i1 = " << i1 << std::endl;
        std::cout << " --> th2 --> i2 = " << i2 << std::endl;
        th1.join();
        th2.join();
    }

    int sentinel;
    std::cin >> sentinel;
}

//======//
// FINI //
//======//
