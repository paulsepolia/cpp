
//=======================//
// move_iterator example //
//=======================//

#include <iostream>
#include <iterator>
#include <vector>
#include <algorithm>
#include <cmath>
#include <thread>
#include <iomanip>

using std::cout;
using std::cin;
using std::endl;
using std::move_iterator;
using std::vector;
using std::copy;
using std::pow;
using std::thread;
using std::fixed;
using std::setprecision;
using std::showpos;
using std::showpoint;

// the benchmark function

void benchFun(const long & DIMEN_MAX)
{
    const long K_MAX = static_cast<long>(pow(10.0, 5.0));

    for (long k = 0; k < K_MAX; k++) {
        // counter display

        cout << "------------------------------------------->> " << k << endl;

        // local variables and parameters

        vector<double> * vecA = new vector<double>;
        vector<double> * vecB = new vector<double>;
        typedef vector<double>::iterator itVecDouble;

        // build the vectors

        for (long i = 0; i < DIMEN_MAX; i++) {
            vecA->push_back(static_cast<double>(i));
            vecB->push_back(0.0);
        }

        // copy using move_iterator

        copy( move_iterator<itVecDouble>(vecA->begin()),
              move_iterator<itVecDouble>(vecA->end()),
              vecB->begin() );

        cout << " (*vecA)[0] = " << (*vecA)[0] << endl;
        cout << " (*vecA)[1] = " << (*vecA)[1] << endl;
        cout << " (*vecA)[2] = " << (*vecA)[2] << endl;
        cout << " (*vecA)[3] = " << (*vecA)[3] << endl;

        cout << endl;

        cout << " (*vecB)[0] = " << (*vecB)[0] << endl;
        cout << " (*vecB)[1] = " << (*vecB)[1] << endl;
        cout << " (*vecB)[2] = " << (*vecB)[2] << endl;
        cout << " (*vecB)[3] = " << (*vecB)[3] << endl;

        // vecA now contains unspecified values;
        // clear it:

        vecA->clear();

        // delete the vectors

        cout << endl;
        cout << " --> delete vecA " << endl;
        cout << endl;

        delete vecA;

        cout << endl;
        cout << " --> delete vecB " << endl;
        cout << endl;

        delete vecB;
    }
}

// the main function

int main()
{
    // local variables and parameters

    const long DIMEN_MAX = static_cast<long>(pow(10.0, 7.8));
    const int NUM_THREADS = 4;
    vector<thread> vecThreads;

    // set the output format

    cout << setprecision(20);
    cout << showpos;
    cout << showpoint;

    // span the threads

    for (int j = 0; j < NUM_THREADS; j++) {
        vecThreads.push_back(thread(benchFun, DIMEN_MAX));
    }

    // join the threads

    for (auto& t : vecThreads) {
        t.join();
    }

    // clear the threads vector

    vecThreads.clear();

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
