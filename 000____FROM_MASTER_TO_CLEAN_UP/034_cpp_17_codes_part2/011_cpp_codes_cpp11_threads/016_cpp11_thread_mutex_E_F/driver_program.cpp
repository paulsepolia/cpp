//=========================//
// mutex::try_lock example //
//=========================//

#include <iostream>       // std::cout
#include <thread>         // std::thread
#include <mutex>          // std::mutex

// global definitions

volatile int counter(0); // non-atomic counter
std::mutex mtx;          // locks access to counter

// a function

void attempt_10k_increases()
{
    for (int i = 0; i < 10000; ++i) {
        if (mtx.try_lock()) {
            // only increase if currently not locked:
            ++counter;
            mtx.unlock();
        }
    }
}

// the main function

int main()
{
    int k;
    const int K_MAX = 2000000;

    for(k = 0; k < K_MAX; k++) {
        std::cout << "----------------------------------------------------->> " << k << std::endl;

        std::thread threads[10];

        // spawn 10 threads:

        counter = 0;

        for (int i = 0; i < 10; ++i) {
            threads[i] = std::thread(attempt_10k_increases);
        }

        for (auto& th : threads) th.join();
        std::cout << counter << " successful increases of the counter." << std::endl;
    }

    int sentinel;
    std::cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
