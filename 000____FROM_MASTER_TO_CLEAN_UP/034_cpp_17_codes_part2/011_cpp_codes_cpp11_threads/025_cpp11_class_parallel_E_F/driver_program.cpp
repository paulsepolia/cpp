//=================//
// friend function //
//=================//

#include <iostream>
#include <cmath>
#include <vector>
#include <list>
#include <deque>
#include <iomanip>
#include <string>
#include <thread>

// using

using std::cout;
using std::cin;
using std::endl;
using std::vector;
using std::deque;
using std::list;
using std::fixed;
using std::setprecision;
using std::showpoint;
using std::string;
using std::thread;

// a class with a friend function

template <typename T>
class ContainerCL {
public:

    void buildContainer();
    void clearContainer();
    void shrinkToFitContainer();
    void showDim();
    void getSize();

private:

    const long DIM_VEC = static_cast<long>(pow(10.0, 7.6));
    T containerA;
};

// buildContainer --> member function definition

template <typename T>
void ContainerCL<T>::buildContainer()
{
    for (long i = 0; i < DIM_VEC; i++) {
        containerA.push_back(cos(static_cast<double>(i)));
    }
}

// clearContainer --> member function definition

template <typename T>
void ContainerCL<T>::clearContainer()
{
    containerA.clear();
}

// shrinkToFitContainer --> member function definition

template <typename T>
void ContainerCL<T>::shrinkToFitContainer()
{
    containerA.shrink_to_fit();
}

// showDim --> member function

template <typename T>
void ContainerCL<T>::showDim()
{
    cout << " --> DIM_VEC = " << DIM_VEC << endl;
}

// a function

void runBenchVec(ContainerCL<vector<double>> obj , string s1)
{
    cout << " --> building the " << s1 << endl;

    obj.buildContainer();

    cout << " --> clearing the " << s1 << endl;

    obj.clearContainer();
}

// a function

void runBenchDeq(ContainerCL<deque<double>> obj , string s1)
{
    cout << " --> building the " << s1 << endl;

    obj.buildContainer();

    cout << " --> clearing the " << s1 << endl;

    obj.clearContainer();
}

// a function

void runBenchList(ContainerCL<list<double>> obj , string s1)
{
    cout << " --> building the " << s1 << endl;

    obj.buildContainer();

    cout << " --> clearing the " << s1 << endl;

    obj.clearContainer();
}


// the main function

int main()
{
    ContainerCL<vector<double>> vecA;
    ContainerCL<deque<double>> deqA;
    ContainerCL<list<double>> listA;
    const int MAX_DO = 100000;

    for (int i = 0; i < MAX_DO; i++) {
        cout << "----------------------------------------------------------->> " << i << " / " << MAX_DO << endl;

        thread th01(runBenchVec, vecA, "vector1");
        thread th02(runBenchVec, vecA, "vector2");
        thread th03(runBenchVec, vecA, "vector3");
        thread th04(runBenchVec, vecA, "vector4");

        th01.join();
        th02.join();
        th03.join();
        th04.join();

        thread th05(runBenchDeq, deqA, "deque1");
        thread th06(runBenchDeq, deqA, "deque2");
        thread th07(runBenchDeq, deqA, "deque3");
        thread th08(runBenchDeq, deqA, "deque4");

        th05.join();
        th06.join();
        th07.join();
        th08.join();

        thread th09(runBenchList, listA, "list1");
        thread th10(runBenchList, listA, "list2");
        thread th11(runBenchList, listA, "list3");
        thread th12(runBenchList, listA, "list4");

        th09.join();
        th10.join();
        th11.join();
        th12.join();
    }

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
