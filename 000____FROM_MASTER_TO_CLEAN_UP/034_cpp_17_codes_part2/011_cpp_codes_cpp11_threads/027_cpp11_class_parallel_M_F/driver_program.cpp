//=================//
// friend function //
//=================//

#include <iostream>
#include <cmath>
#include <vector>
#include <list>
#include <deque>
#include <iomanip>
#include <string>
#include <thread>
#include <sstream>

// using

using std::cout;
using std::cin;
using std::endl;
using std::vector;
using std::deque;
using std::list;
using std::fixed;
using std::setprecision;
using std::showpoint;
using std::string;
using std::thread;
using std::stringstream;

// a class with a friend function

template <typename T>
class ContainerCL {
public:

    void buildContainer();
    void clearContainer();
    void shrinkToFitContainer();
    void showDim();
    void getSize();

private:

    const long DIM_VEC = static_cast<long>(pow(10.0, 7.6));
    T containerA;
};

// buildContainer --> member function definition

template <typename T>
void ContainerCL<T>::buildContainer()
{
    for (long i = 0; i < DIM_VEC; i++) {
        containerA.push_back(cos(static_cast<double>(i)));
    }
}

// clearContainer --> member function definition

template <typename T>
void ContainerCL<T>::clearContainer()
{
    containerA.clear();
}

// shrinkToFitContainer --> member function definition

template <typename T>
void ContainerCL<T>::shrinkToFitContainer()
{
    containerA.shrink_to_fit();
}

// showDim --> member function

template <typename T>
void ContainerCL<T>::showDim()
{
    cout << " --> DIM_VEC = " << DIM_VEC << endl;
}

// a template function

template <typename T>
void runBench(ContainerCL<T> obj , string s1)
{
    cout << " --> building the " << s1 << endl;

    obj.buildContainer();

    cout << " --> clearing the " << s1 << endl;

    obj.clearContainer();
}

// the main function

int main()
{
    // local variables and parameters

    const int MAX_DO = 100000;
    const int NUM_THREADS = 4;
    int i;
    int j;
    vector<thread> vecThreads;
    string * sVe = new string [NUM_THREADS];
    string * sDe = new string [NUM_THREADS];
    string * sLi = new string [NUM_THREADS];
    stringstream ss;
    string s_tmp;

    // create the names

    for (j = 0; j < NUM_THREADS; j++) {
        // create the name

        ss << j;
        ss >> s_tmp;
        sVe[j] = "vector_" + s_tmp;
        sDe[j] = "deque_" + s_tmp;
        sLi[j] = "list_" + s_tmp;

        // reset stream - must step

        ss.str(string());
        ss.clear();
    }

    // main do-loop

    for (i = 0; i < MAX_DO; i++) {

        ContainerCL<vector<double>> * vecA = new ContainerCL<vector<double>> [1];
        ContainerCL<deque<double>> * deqA = new ContainerCL<deque<double>> [1];
        ContainerCL<list<double>> * listA = new ContainerCL<list<double>> [1];

        cout << "----------------------------------------------------------->> " << i << " / " << MAX_DO << endl;

        //=========//
        // vectors //
        //=========//

        // spawn threads

        for (j = 0; j < NUM_THREADS; j++) {
            vecThreads.push_back(thread(&runBench<vector<double>>, *vecA, sVe[j]));
        }

        // join the threads

        for (auto& t : vecThreads) {
            t.join();
        }

        // clear the thread vector

        vecThreads.clear();

        delete [] vecA;

        //========//
        // deques //
        //========//

        // spawn threads

        for (j = 0; j < NUM_THREADS; j++) {
            vecThreads.push_back(thread(&runBench<deque<double>>, *deqA, sDe[j]));
        }

        // join the threads

        for (auto& t : vecThreads) {
            t.join();
        }

        // clear the thread vector

        vecThreads.clear();

        delete [] deqA;

        //=======//
        // lists //
        //=======//

        // spawn threads

        for (j = 0; j < NUM_THREADS; j++) {
            vecThreads.push_back(thread(&runBench<list<double>>, *listA, sLi[j]));
        }

        // join the threads

        for (auto& t : vecThreads) {
            t.join();
        }

        // clear the thread vector

        vecThreads.clear();

        delete [] listA;
    }

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
