
//========================//
// priority_queue adaptor //
//========================//

#include <iostream>
#include <queue>
#include <deque>
#include <cmath>
#include <vector>
#include <thread>

using std::endl;
using std::cout;
using std::cin;
using std::deque;
using std::queue;
using std::priority_queue;
using std::vector;
using std::pow;
using std::thread;

// the bench function

void benchFun(const long int DIMEN_MAX)
{
    const long K_MAX = static_cast<long>(pow(10.0, 5.0));

    for(long k = 0; k < K_MAX; k++) {
        // counter

        cout << "------------------------------------------------>> " << k << endl;

        // declare the adaptors

        priority_queue<double> * qA = new priority_queue<double>;
        priority_queue<double, deque<double>> * qB = new priority_queue<double, deque<double>>;
        priority_queue<double, vector<double>> * qC = new priority_queue<double, vector<double>>;

        // build the containers

        cout << " --> build --> default priority_queue" << endl;

        for (long i = 0; i < DIMEN_MAX; i++) {
            qA->push(static_cast<double>(i));
        }

        cout << " --> build --> priority_queue via deque" << endl;

        for (long i = 0; i < DIMEN_MAX; i++) {
            qB->push(static_cast<double>(i));
        }

        cout << " --> build --> priority_queue via vector" << endl;

        for (long i = 0; i < DIMEN_MAX; i++) {
            qC->push(static_cast<double>(i));
        }

        // pop the containers

        cout << " --> pop --> default queue" << endl;

        for (long i = 0; i < DIMEN_MAX; i++) {
            qA->pop();
        }

        cout << " --> pop --> priority_queue via deque" << endl;

        for (long i = 0; i < DIMEN_MAX; i++) {
            qB->pop();
        }

        cout << " --> pop --> priority_queue via vector" << endl;

        for (long i = 0; i < DIMEN_MAX; i++) {
            qC->pop();
        }

        // free RAM

        cout << " --> delete --> default priority_queue" << endl;

        delete qA;

        cout << " --> delete --> priority_queue via deque" << endl;

        delete qB;

        cout << " --> delete --> priority_queue via vector" << endl;

        delete qC;
    }
}

// the main function

int main()
{
    // local variables and parameters

    const long int DIMEN_MAX = static_cast<long int>(pow(10.0, 7.2));
    const int NUM_THREADS = 4;
    vector<thread> vecThreads;

    // span the threads

    for (int j = 0; j < NUM_THREADS; j++) {
        vecThreads.push_back(thread(benchFun, DIMEN_MAX));
    }

    // join the threads

    for (auto& t : vecThreads) {
        t.join();
    }

    // clear the threads vector

    vecThreads.clear();

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
