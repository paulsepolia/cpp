
//==================================//
// demonstrate find() and find_if() //
//==================================//

#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <cmath>
#include <iomanip>
#include <thread>

using std::cout;
using std::cin;
using std::endl;
using std::vector;
using std::string;
using std::find;
using std::find_if;
using std::cos;
using std::pow;
using std::setprecision;
using std::showpos;
using std::showpoint;
using std::thread;

// the value i am looking for

const long DIMEN_MAX = static_cast<long>(pow(10.0, 5.0));
const long myValA = DIMEN_MAX-1;
const double myValB = cos(static_cast<double>(myValA));

// the comparison function

bool isMyNum(const double & val)
{
    if (val == myValB) {
        return true;
    } else {
        return false;
    }
}

// the main function

void benchFun(const long & DIMEN_MAX)
{
    // local variables and parameters

    const long K_MAX = static_cast<long>(pow(10.0, 5.0));
    double myValC;

    // benchmark loop

    for (long k = 0; k < K_MAX; k++) {
        // the counter

        cout << "------------------------------------------>> " << k << endl;

        vector<double> * vA = new vector<double>;
        vector<double>::iterator pA;

        // build the vector of doubles

        cout << " --> build the vector" << endl;

        for (long i = 0; i < DIMEN_MAX; i++) {
            vA->push_back(cos(static_cast<double>(i)));
        }

        // find the value

        cout << " --> find the value" << endl;

        for (long i = 0; i < DIMEN_MAX; i++) {
            myValC = cos(static_cast<double>(i));
            pA = find(vA->begin(), vA->end(), myValC);
        }

        cout << " --> value = " << myValC << endl;
        cout << " --> *pA++ = " << *pA++ << endl;

        // find the value

        cout << " --> find the value using the comparison function" << endl;

        for (long i = 0; i < DIMEN_MAX; i++) {
            pA = find_if(vA->begin(), vA->end(), isMyNum);
        }

        cout << " --> value = " << cos(static_cast<double>(myValA)) << endl;
        cout << " --> *pA++ = " << *pA++ << endl;

        // delete the vector

        delete vA;
    }
}

// the main function

int main()
{
    // local variables and parameters

    const int NUM_THREADS = 8;
    vector<thread> vecThreads;

    // set the output format

    cout << setprecision(20);
    cout << showpos;
    cout << showpoint;

    // span the threads

    for (int j = 0; j < NUM_THREADS; j++) {
        vecThreads.push_back(thread(benchFun, DIMEN_MAX));
    }

    // join the threads

    for (auto& t : vecThreads) {
        t.join();
    }

    // clear the threads vector

    vecThreads.clear();

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//