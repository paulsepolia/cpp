
//================================//
// vector<bool> and prime numbers //
//================================//

// Sieve of Eratosthenes to generate
// all prime numbers below a given limit

#include <iostream>
#include <vector>
#include <cmath>
#include <thread>
#include <iomanip>

using std::cout;
using std::cin;
using std::endl;
using std::vector;
using std::pow;
using std::sqrt;
using std::boolalpha;
using std::noboolalpha;
using std::setprecision;
using std::fixed;
using std::showpos;
using std::showpoint;
using std::thread;

// the benchmark primes function

void primesFun(const long long int & N_MAX)
{
    // local variables and parameters

    const long K_MAX = static_cast<long int>(pow(10.0, 5.0));
    long long int i;
    long long int k;
    long long int sqrtN;
    long long int count;

    for (long kk = 0; kk != K_MAX; kk++) {
        cout << "-------------------------------------------------------------------->> "
             << kk << endl;

        vector<long long int> * vecPrimes = new vector<long long int>;

        cout << " --> build and initialize the bool vector" << endl;

        vector<bool> S(N_MAX, true);

        sqrtN = static_cast<long long int>(sqrt(static_cast<double>(N_MAX))) + 1LL;

        // initially, all S[i] are true.
        // S[i] = false if and when
        // we find i is not a prime number.

        // Sieve of Eratosthenes

        cout << " --> find the indices of non-prime numbers" << endl;

        for (i = 2; i < sqrtN; i++) {
            if (S[i]) {
                for (k = i*i; k < N_MAX; k = k+i) {
                    S[k] = false;
                }
            }
        }

        // count the primes

        cout << " --> count the prime numbers" << endl;

        count = 0LL;

        for (i = 2; i < N_MAX; i++) {
            if (S[i]) {
                vecPrimes->push_back(i);
                count++;
            }
        }

        // outputs

        cout << " --> there are " << count << " prime numbers less than " << N_MAX << endl;

        cout << " --> largest prime number less than " << N_MAX
             << " is " << (*vecPrimes)[count-1] << "." << endl;

        // display all the primes less than N_MAX

        //for (i = 0; i != count; i++)
        //{
        //	cout << " --> " << i << " --> " << vecPrimes[i] << endl;
        //}

        // clear vector holding the indices

        cout << " --> clear vector holding the indices" << endl;

        S.clear();

        // delete vecPrimes

        cout << " --> delete vector holding the primes" << endl;

        delete vecPrimes;
    }
}

// the main function

int main()
{
    // local variables and parameters

    const long long int N_MAX = static_cast<long long int>(pow(10.0, 9.0));
    const int NUM_THREADS = 8;
    vector<thread> vecThreads;

    // set the output format

    cout << setprecision(20);
    cout << showpos;
    cout << showpoint;

    // span the threads

    for (int j = 0; j < NUM_THREADS; j++) {
        vecThreads.push_back(thread(primesFun, N_MAX));
    }

    // join the threads

    for (auto& t : vecThreads) {
        t.join();
    }

    // clear the threads vector

    vecThreads.clear();

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
