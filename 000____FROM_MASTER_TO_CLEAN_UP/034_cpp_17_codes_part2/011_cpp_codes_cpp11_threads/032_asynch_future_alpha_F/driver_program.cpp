//==================//
// future and async //
//==================//

#include <iostream>
#include <future>
#include <thread>
#include <iomanip>

using std::endl;
using std::cout;
using std::cin;
using std::future;
using std::async;
using std::launch;
using std::setprecision;
using std::fixed;
using std::showpos;
using std::showpoint;

// function definition

double fib(int n)
{
    if (n < 3) {
        return 1.0;
    } else {
        return fib(n-1) + fib(n-2);
    }
}

// the main function

int main()
{
    // local variables and parameters

    int val = 50;
    const int I_MAX = 10000;

    // output style

    cout << setprecision(10);
    cout << fixed;
    cout << showpos;
    cout << showpoint;

    // main loop

    for (int i = 0; i < I_MAX; i++) {

        cout << "-------------------------------------------------->> " << i << endl;

        future<double> f1 = async(launch::async, [val] () {
            return fib(val);
        });
        future<double> f2 = async(launch::async, [val] () {
            return fib(val);
        });
        future<double> f3 = async(launch::async, [val] () {
            return fib(val);
        });
        future<double> f4 = async(launch::async, [val] () {
            return fib(val);
        });
        future<double> f5 = async(launch::async, [val] () {
            return fib(val);
        });
        future<double> f6 = async(launch::async, [val] () {
            return fib(val);
        });
        future<double> f7 = async(launch::async, [val] () {
            return fib(val);
        });
        future<double> f8 = async(launch::async, [val] () {
            return fib(val);
        });

        cout << " --> waiting ..." << endl;

        f1.wait();
        f2.wait();
        f3.wait();
        f4.wait();
        f5.wait();
        f6.wait();
        f7.wait();
        f8.wait();

        cout << " --> f1 --> " << f1.get() << endl;
        cout << " --> f2 --> " << f2.get() << endl;
        cout << " --> f3 --> " << f3.get() << endl;
        cout << " --> f4 --> " << f4.get() << endl;
        cout << " --> f5 --> " << f5.get() << endl;
        cout << " --> f6 --> " << f6.get() << endl;
        cout << " --> f7 --> " << f7.get() << endl;
        cout << " --> f8 --> " << f8.get() << endl;
    }

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
