//================//
// STL and thread //
//================//

#include <iostream>
#include <thread>
#include <cmath>
#include <iomanip>
#include <ctime>
#include <queue>

// global parameters

const long int I_MAX = static_cast<long int>(pow(10.0, 7.5));
const long int J_MAX = static_cast<long int>(pow(10.0, 8.0));

// function: void funA()

void funA()
{
    long int i;
    std::queue<double> *dqA = new std::queue<double> [1];

    for(i = 0; i < I_MAX; i++) {
        (*dqA).emplace(cos(static_cast<double>(i)));
    }

    for(i = 0; i < I_MAX; i++) {
        (*dqA).pop();
    }

    for(i = 0; i < I_MAX; i++) {
        (*dqA).push(sin(static_cast<double>(i)));
    }

    delete [] dqA;
}

// function: void funB()

void funB()
{
    long int i;

    std::queue<double> *dqA = new std::queue<double> [1];

    for(i = 0; i < I_MAX; i++) {
        (*dqA).emplace(cos(static_cast<double>(i)));
    }

    for(i = 0; i < I_MAX; i++) {
        (*dqA).pop();
    }

    for(i = 0; i < I_MAX; i++) {
        (*dqA).push(sin(static_cast<double>(i)));
    }

    delete [] dqA;
}

// main function

int main()
{
    // 1. local variables and parameters

    long int j;
    clock_t t1;
    clock_t t2;
    double tAll;

    // 2. set the output look

    std::cout << std::fixed;
    std::cout << std::setprecision(5);
    std::cout << std::showpoint;

    // 3. main do-loop

    for (j = 1; j < J_MAX; j++) {
        std::cout << std::endl;
        std::cout << "------------------------------------------------------------->> " << j << std::endl;
        std::cout << std::endl;
        std::cout << " 8 threads are executing 2 functions, 4 threads per function ...." << std::endl;
        std::cout << std::endl;

        // 4. timing

        t1 = clock();

        // 5. create the threads

        std::thread th1(funA);  // spawn new thread that calls funA
        std::thread th2(funB);  // spawn new thread that calls funB
        std::thread th3(funA);  // spawn new thread that calls funA
        std::thread th4(funB);  // spawn new thread that calls funB
        std::thread th5(funA);  // spawn new thread that calls funA
        std::thread th6(funB);  // spawn new thread that calls funB
        std::thread th7(funA);  // spawn new thread that calls funA
        std::thread th8(funB);  // spawn new thread that calls funB

        // 6. a message

        std::cout << " --> main, funA and funB now execute concurrently!" << std::endl;

        // 7. synchronize the threads

        th1.join();   // pauses until first finishes
        std::cout << " --> thread --> th1 --> ended" << std::endl;
        th2.join();   // pauses until second finishes
        std::cout << " --> thread --> th2 --> ended" << std::endl;
        th3.join();   // pauses until second finishes
        std::cout << " --> thread --> th3 --> ended" << std::endl;
        th4.join();   // pauses until second finishes
        std::cout << " --> thread --> th4 --> ended" << std::endl;
        th5.join();   // pauses until first finishes
        std::cout << " --> thread --> th5 --> ended" << std::endl;
        th6.join();   // pauses until second finishes
        std::cout << " --> thread --> th6 --> ended" << std::endl;
        th7.join();   // pauses until second finishes
        std::cout << " --> thread --> th7 --> ended" << std::endl;
        th8.join();   // pauses until second finishes
        std::cout << " --> thread --> th8 --> ended" << std::endl;

        // 8. timing

        t2 = clock();
        tAll = static_cast<double>(t2-t1)/CLOCKS_PER_SEC;

        // 9. reporting

        std::cout << std::endl;
        std::cout << " --> all threads done --> " << tAll << std::endl;
        std::cout << std::endl;
    }

    int sentinel;
    std::cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
