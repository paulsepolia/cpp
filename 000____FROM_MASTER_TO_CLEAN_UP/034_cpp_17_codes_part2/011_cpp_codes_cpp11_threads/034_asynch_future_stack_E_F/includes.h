//============//
// includes.h //
//============//

#include <iostream>
#include <future>
#include <thread>
#include <iomanip>
#include <cmath>
#include <list>
#include <ctime>
#include <algorithm>

using std::endl;
using std::cout;
using std::cin;
using std::future;
using std::async;
using std::launch;
using std::setprecision;
using std::fixed;
using std::showpos;
using std::showpoint;
using std::list;
using std::pow;
using std::sort;
using std::clock;

//======//
// FINI //
//======//
