//=====================//
// build list function //
//=====================//

#include "includes.h"

// function definition

double buildList(const int DIM_MAX)
{
    list<double> listA;
    clock_t t1;
    clock_t t2;
    double tall1;
    double tall2;

    cout << "  1 --> build list" << endl;

    t1 = clock();

    for (long i = 0; i < DIM_MAX; i++) {
        listA.push_back(cos(static_cast<double>(i)));
    }

    t2 = clock();

    tall1 = (t2-t1)/static_cast<double>(CLOCKS_PER_SEC);

    cout << "  2 --> done with list --> " << tall1 << endl;

    cout << "  3 --> sort the list" << endl;

    t1 = clock();

    listA.sort();

    t2 = clock();

    tall2 = (t2-t1)/static_cast<double>(CLOCKS_PER_SEC);

    cout << "  4 --> done with the list --> " << tall2 << endl;

    return (tall1 + tall2);
}

//======//
// FINI //
//======//
