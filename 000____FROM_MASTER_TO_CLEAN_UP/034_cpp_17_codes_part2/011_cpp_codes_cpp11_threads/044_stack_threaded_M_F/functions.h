
//===========//
// functions //
//===========//

#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <iostream>
#include <stack>
#include <iomanip>
#include <cmath>
#include <vector>
#include <thread>
#include <algorithm>

using std::stack;
using std::endl;
using std::cout;
using std::cin;
using std::fixed;
using std::setprecision;
using std::showpos;
using std::showpoint;
using std::pow;
using std::vector;
using std::thread;
using std::sort;

// the benchmark function -> benchFunHeap()

void benchFunHeap(const long & I_MAX)
{
    const long J_MAX = static_cast<long>(pow(10.0, 7.0));

    // set format

    cout << setprecision(10);
    cout << fixed;
    cout << showpoint;
    cout << showpos;

    // benchmark loop

    for (long j = 0; j < J_MAX; j++) {

        cout << "--------------------------------------------->> " << j << endl;

        stack<double> * stackA = new stack<double>;
        stack<double> * stackB = new stack<double>;
        vector<double> * vecA = new vector<double>;
        vector<double> * vecB = new vector<double>;

        cout << " --> push" << endl;

        for (long i = 0; i < I_MAX; i++) {
            stackA->push(sin(static_cast<double>(i)));
            stackB->push(cos(static_cast<double>(i)));
        }

        cout << " --> pop" << endl;

        for (long i = 0; i < I_MAX; i++) {
            stackA->pop();
            stackB->pop();
        }

        cout << " --> emplace stack" << endl;

        for (long i = 0; i < I_MAX; i++) {
            stackA->emplace(sin(static_cast<double>(i)));
            stackB->emplace(cos(static_cast<double>(i)));
        }

        cout << " --> swap" << endl;

        stackA->swap(*stackB);

        cout << " --> sort stack via vector" << endl;

        for (long i = 0; i < I_MAX; i++) {
            vecA->push_back(stackA->top());
            vecB->push_back(stackB->top());

            stackA->pop();
            stackB->pop();
        }

        sort(vecA->begin(),vecA->end());
        sort(vecB->begin(),vecB->end());

        for (long i = 0; i < I_MAX; i++) {
            stackA->push((*vecA)[i]);
            stackB->push((*vecB)[i]);
        }

        cout << " --> pop" << endl;

        for (long i = 0; i < I_MAX; i++) {
            stackA->pop();
            stackB->pop();
        }

        cout << " --> delete" << endl;

        delete stackA;
        delete stackB;
        delete vecA;
        delete vecB;

        stackA = NULL;
        stackB = NULL;
        vecA = NULL;
        vecB = NULL;
    }
}

// the benchmark function --> benchFunStack

void benchFunStack(const long & I_MAX)
{
    const long J_MAX = static_cast<long>(pow(10.0, 7.0));

    // set format

    cout << setprecision(10);
    cout << fixed;
    cout << showpoint;
    cout << showpos;

    // benchmark here

    for (long j = 0; j < J_MAX; j++) {

        cout << "--------------------------------------------->> " << j << endl;

        stack<double> stackA;
        stack<double> stackB;
        vector<double> vecA;
        vector<double> vecB;

        cout << " --> push" << endl;

        for (long i = 0; i < I_MAX; i++) {
            stackA.push(sin(static_cast<double>(i)));
            stackB.push(cos(static_cast<double>(i)));
        }

        cout << " --> pop" << endl;

        for (long i = 0; i < I_MAX; i++) {
            stackA.pop();
            stackB.pop();
        }

        cout << " --> emplace" << endl;

        for (long i = 0; i < I_MAX; i++) {
            stackA.emplace(sin(static_cast<double>(i)));
            stackB.emplace(cos(static_cast<double>(i)));
        }

        cout << " --> swap" << endl;

        stackA.swap(stackB);

        cout << " --> sort stack via vector" << endl;

        for (long i = 0; i < I_MAX; i++) {
            vecA.push_back(stackA.top());
            vecB.push_back(stackB.top());

            stackA.pop();
            stackB.pop();
        }

        sort(vecA.begin(),vecA.end());
        sort(vecB.begin(),vecB.end());

        for (long i = 0; i < I_MAX; i++) {
            stackA.push(vecA[i]);
            stackB.push(vecB[i]);
        }

        cout << " --> pop" << endl;

        for (long i = 0; i < I_MAX; i++) {
            stackA.pop();
            stackB.pop();
        }

        cout << " --> delete" << endl;
    }
}

#endif // FUNCTIONS_H

//======//
// FINI //
//======//

