//===============//
// C++11, Thread //
//===============//

#include <iostream>
#include <iomanip>
#include <ctime>
#include <cmath>
#include <thread>

// definition of a function

void fun_black_box_light()
{
    double sumLoc = 0.0;
    double iLoc = 0.0;
    const double MY_BOUND_MAX = 19.0;
    clock_t t1;
    clock_t t2;
    double tall;

    std::cout << " --> LIGHT speaks: The LIGHT function is being executed." << std::endl;

    t1 = clock();

    while(sumLoc <= MY_BOUND_MAX) {
        iLoc = iLoc + 1.0;
        sumLoc = sumLoc + 1.0/iLoc;
    }

    t2 = clock();
    tall = (t2-t1)/static_cast<double>(CLOCKS_PER_SEC);

    std::cout << " --> LIGHT speaks: The LIGHT function has finished execution." << std::endl;
    std::cout << " --> LIGHT speaks: " << MY_BOUND_MAX << " --> reached in --> " << tall << std::endl;
    std::cout << " --> LIGHT speaks: " << sumLoc << " <--> " << iLoc << std::endl;
}

// definition of "void do_something()_else" function

void fun_black_box_heavy()
{
    double sumLoc = 0.0;
    double iLoc = 0.0;
    const double MY_BOUND_MAX = 22.0;
    clock_t t1;
    clock_t t2;
    double tall;

    std::cout << " --> HEAVY speaks: The HEAVY function is being executed." << std::endl;

    t1 = clock();

    while(sumLoc <= MY_BOUND_MAX) {
        iLoc = iLoc + 1.0;
        sumLoc = sumLoc + 1.0/iLoc;
    }

    t2 = clock();
    tall = (t2-t1)/static_cast<double>(CLOCKS_PER_SEC);

    std::cout << " --> HEAVY speaks: The HEAVY function has finished execution." << std::endl;
    std::cout << " --> HEAVY speaks: " << MY_BOUND_MAX << " --> reached in --> " << tall << std::endl;
    std::cout << " --> HEAVY speaks: " << sumLoc << " <--> " << iLoc << std::endl;
}

// a class with a function to be threaded

class a_heavy_task {
public:
    void operator()() const
    {
        fun_black_box_heavy();
    }
};

// a class with a function to be threaded

class a_light_task {
public:
    void operator()() const
    {
        fun_black_box_light();
    }
};

// main program

int main()
{
    // 1. local variables and parameters

    const long int I_MAX = 10000L;
    a_heavy_task f_heavy;
    a_light_task f_light;
    long int i;

    // 2. set the output format

    std::cout << std::fixed;
    std::cout << std::setprecision(5);
    std::cout << std::showpoint;

    // 3. main do-loop

    for (i = 0; i < I_MAX; i++) {
        std::cout << std::endl;
        std::cout << "-------------------------------------------------->> " << i << " / " << I_MAX << std::endl;
        std::cout << std::endl;
        std::cout << " In each new iteration program launches two heavy duty threads." << std::endl;
        std::cout << " So, gradually the program will end up using (2*iteration) threads + main thread" << std::endl;
        std::cout << " The whole process will become very slow and heavy!" <<std::endl;
        std::cout << std::endl;
        std::cout << " --> 1 -->  I am calling the object function: f_heavy()" << std::endl;
        std::cout << " --> 2 -->  Two C++11 threads call independently f_heavy()" << std::endl;
        std::cout << " --> 3 -->  Then the two threads are detached immediatelly." << std::endl;

        // 2. start the two threads

        std::thread my_thread1(f_heavy);
        std::thread my_thread2(f_heavy);

        // 3. join the launched two threads

        my_thread1.detach();
        my_thread2.detach();

        // 4. report

        std::cout << " --> 4 --> The two threads have been detached." << std::endl;

        // 5. continue to launch a light duty function

        std::cout << " --> 5 --> Calling the object function f_light();" << std::endl;
        std::cout << " --> 6 --> f_light() needs to be completed before any progress." << std::endl;
        std::cout << " --> 7 --> f_light uses the main() thread." << std::endl;

        f_light();

        std::cout << " --> 8 --> f_light() has been completed. Next iteration now starts." << std::endl;
        std::cout << " --> 9 --> The next iteration will launch two new heavy duty threads," << std::endl;
        std::cout <<	 "           and detach them immediately." << std::endl;
    }

    // 4. sentinel

    int sentinel;
    std::cout << " -------> 10 --> All the light works are done but not heavy ones." << std::endl;
    std::cout << " -------> 11 --> You can enter an integer to exit anytime." << std::endl;
    std::cout << " -------> 12 --> WARNING: The deatched heavy duty threads may still running and reporting soon!" << std::endl;
    std::cout << " -------> 13 --> If you enter an integer to exit you will kill them normaly." << std::endl;
    std::cout << " -------> 14 --> Anyway, if you want to exit just enter an integer here: " << std::endl;
    std::cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//