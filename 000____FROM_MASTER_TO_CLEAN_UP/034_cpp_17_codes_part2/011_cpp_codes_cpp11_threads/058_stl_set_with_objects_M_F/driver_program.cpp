
//================================//
// storing class objects in a set //
//================================//

#include <iostream>
#include <set>
#include <string>
#include <vector>
#include <thread>
#include <cmath>
#include <iomanip>

using std::cout;
using std::cin;
using std::endl;
using std::set;
using std::string;
using std::vector;
using std::thread;
using std::pow;
using std::to_string;
using std::setprecision;
using std::showpos;
using std::showpoint;

// this class stores states and their capitals

class state {
public:
    state() : name (""), capital ("") {}
    explicit state(string s) : name (s), capital ("") {}
    state(string n, string c) : name (n), capital (c) {}

    string get_name() const
    {
        return name;
    }
    string get_capital() const
    {
        return capital;
    }

private:
    string name;
    string capital;
};

// compare objects using name of state

bool operator < (state a, state b)
{
    return a.get_name() < b.get_name();
}


// the benchmark function

void benchFun(const long DIMEN_MAX)
{
    // local variables and parameters

    const long K_MAX = static_cast<long>(pow(10.0, 6.0));
    const int I_MAX = 20;
    set<state>::iterator p;

    for (long k = 0; k < K_MAX; k++) {
        // counter

        cout << "------------------------------------------>> " << k << endl;

        set<state> * states = new set<state>;

        // put names and numbers into set

        cout << " --> build the set with the objects" << endl;

        for (long i = 0; i < DIMEN_MAX; i++) {
            states->insert(state(to_string(i), to_string(i+1)));
        }

        // output the contents of the set

        cout << " --> display the first " << I_MAX << " some elements ..." << endl;

        p = states->begin();

        for (int i = 0; i < I_MAX; i++, p++) {
            cout << " name : " << p->get_name() << ", capital : " <<  p->get_capital() << endl;
        }

        // erase the set

        cout << " --> erase the set" << endl;

        states -> erase(states->begin(), states->end());

        // delete the directory

        cout << " --> delete the set with the objects" << endl;

        delete states;
    }

}

// the main function

int main()
{
    // local variables and parameters

    const long int DIMEN_MAX = static_cast<long>(pow(10.0, 6.5));
    const int NUM_THREADS = 4;
    vector<thread> vecThreads;

    // set the output format

    cout << setprecision(20);
    cout << showpos;
    cout << showpoint;

    // span the threads

    for (int j = 0; j < NUM_THREADS; j++) {
        vecThreads.push_back(thread(benchFun, DIMEN_MAX));
    }

    // join the threads

    for (auto& t : vecThreads) {
        t.join();
    }

    // clear the threads vector

    vecThreads.clear();

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
