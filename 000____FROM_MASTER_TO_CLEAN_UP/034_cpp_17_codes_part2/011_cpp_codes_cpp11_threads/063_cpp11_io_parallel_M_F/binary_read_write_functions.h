
#ifndef FUNCTIONS_H
#define FUNCTIONS_H

//============================//
// Read and Write binary file //
// using buffers              //
//============================//

#include <iostream>
#include <fstream>
#include <thread>
#include <ctime>
#include <vector>
#include <string>

// using

using std::ofstream;
using std::ifstream;
using std::cout;
using std::endl;
using std::ios;
using std::flush;
using std::this_thread::get_id;
using std::to_string;
using std::string;

// functions prototypes

void writeFun(const string &, const long &, double *);
void readFun(const string &, const long &);

// functions definitions

//=======================//
// binary write function //
//=======================//

void writeFun(const string & fileName, const long & dim, double * arrayLoc)
{
    // local variables and parameters

    ofstream fileOut;
    clock_t t1;
    clock_t t2;

    // start time

    t1 = clock();

    // open the file

    fileOut.open(fileName.c_str(), ios::out | ios::binary | ios::trunc);

    // check if the file opened with success

    if (!fileOut.is_open()) {
        cout << "Error! The file stream is not opened. Abort." << endl;
    }

    // go to the beggining of the file

    fileOut.seekp(0);

    // write to the file

    fileOut.write(reinterpret_cast<char*>(&arrayLoc[0]), dim * sizeof(double));

    // flush the buffer to ensure the data has been written

    fileOut.flush();

    // close the file

    fileOut.close();

    // end time

    t2 = clock();

    // report

    cout << " --> writing done --> thread id = "
         << get_id()
         << " --> time used = "
         << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;
}

//======================//
// binary read function //
//======================//

void readFun(const string & fileName, const long & dim)
{
    // local variables and parameters

    ifstream fileIn;
    clock_t t1;
    clock_t t2;

    double * arrayLoc = new double [dim];

    // start time

    t1 = clock();

    // open the file

    fileIn.open(fileName.c_str(), ios::in | ios::binary);

    // rewind the file

    fileIn.seekg(0, fileIn.beg);

    // read the file

    fileIn.read(reinterpret_cast<char*>(&arrayLoc[0]), dim * sizeof(double));

    // close the file

    fileIn.close();

    // delete the container

    delete [] arrayLoc;

    // end time

    t2 = clock();

    // report

    cout << " --> reading done --> thread id = "
         << get_id()
         << " --> time used = "
         << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;
}

//======//
// FINI //
//======//

#endif // FUNCTIONS_H

