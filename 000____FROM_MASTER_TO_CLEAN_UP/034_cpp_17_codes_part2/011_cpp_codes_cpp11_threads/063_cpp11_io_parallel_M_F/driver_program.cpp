
//============================//
// Read and Write binary file //
// using buffers              //
//============================//

#include <iostream>
#include <string>
#include <cmath>
#include <thread>
#include <ctime>
#include <iomanip>
#include <vector>
#include "binary_read_write_functions.h"

// using

using std::string;
using std::cout;
using std::endl;
using std::thread;
using std::this_thread::get_id;
using std::showpoint;
using std::setprecision;
using std::fixed;
using std::vector;
using std::to_string;

// the main function

int main()
{
    // variables and parameters

    const long ARRAY_DIM = static_cast<long>(pow(10.0, 8.0));
    const int NUM_THREADS = 2;
    double* arrayLocal = new double [ARRAY_DIM];
    const long I_MAX = 1000000;
    long i;
    int j;
    string stringFileName;
    vector<thread> vecThreads;
    string * fileName = new string [NUM_THREADS];

    // create the file names

    for (j = 0; j < NUM_THREADS; j++) {
        stringFileName = "my_file_" + to_string(j) + ".bin";

        fileName[j] = stringFileName;
    }

    // set the output format

    cout << fixed;
    cout << setprecision(5);
    cout << showpoint;

    // build the array

    for (i = 0; i < ARRAY_DIM; i++) {
        arrayLocal[i] = cos(static_cast<double>(i));
    }

    // main do-loop

    for (i = 0; i < I_MAX; i++) {
        cout << endl;
        cout << "---------------------------------------------------------------------------------->> "
             << i << " / " << I_MAX << endl;
        cout << endl;

        // span the threads

        for (j = 0; j < NUM_THREADS; j++) {
            vecThreads.push_back(thread(writeFun, fileName[j], ARRAY_DIM, arrayLocal));
        }

        // join the threads

        for (auto& t : vecThreads) {
            t.join();
        }

        // clear the threads vector

        vecThreads.clear();

        cout << endl;

        // spawn the threads

        for (j = 0; j < NUM_THREADS; j++) {
            vecThreads.push_back(thread(readFun, fileName[j], ARRAY_DIM));
        }

        // join the threads

        for (auto& t : vecThreads) {
            t.join();
        }

        // clear the threads container

        vecThreads.clear();
    }

    // exit

    return 0;
}

//======//
// FINI //
//======//

