//===============//
// parallel beep //
//===============//

#include <iostream>
#include <thread>

// using

using std::endl;
using std::cin;
using std::cout;
using std::thread;

// beepFun1

void beepFun1()
{
    while(true) {
        cout << '\a' << endl;
    }
}

// beepFun2

void beepFun2()
{
    while(true) {
        cout << '\a' << endl;
    }
}

// beepFun3

void beepFun3()
{
    while(true) {
        cout << '\a' << endl;
    }
}


// the main function

int main()
{
    // report

    cout << " --> the parallel beep program" << endl;
    cout << " --> make many parallel beeps please..." << endl;

    // start parallel threads

    thread th01(beepFun1);
    thread th02(beepFun2);
    thread th03(beepFun3);
    thread th04(beepFun1);
    thread th05(beepFun2);
    thread th06(beepFun3);
    thread th07(beepFun1);
    thread th08(beepFun2);
    thread th09(beepFun3);
    thread th10(beepFun1);
    thread th11(beepFun2);
    thread th12(beepFun3);
    thread th13(beepFun1);
    thread th14(beepFun2);
    thread th15(beepFun3);
    thread th16(beepFun1);
    thread th17(beepFun2);
    thread th18(beepFun3);
    thread th19(beepFun1);
    thread th20(beepFun2);

    // detach the parallel threads

    th01.join();
    th02.join();
    th03.join();
    th04.join();
    th05.join();
    th06.join();
    th07.join();
    th08.join();
    th09.join();
    th10.join();
    th11.join();
    th12.join();
    th13.join();
    th14.join();
    th15.join();
    th16.join();
    th17.join();
    th18.join();
    th19.join();
    th20.join();

    // return here

    return 0;
}

//======//
// FINI //
//======//