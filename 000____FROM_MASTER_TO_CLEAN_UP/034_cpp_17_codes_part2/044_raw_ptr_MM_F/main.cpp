#include <iostream>
#include <cstdint>
#include <memory>

int main() {

    {
        std::cout << " --> example --> 1 -------------------> start" << std::endl;

        auto *pd = new double(111.11);

        std::cout << " pd    = " << pd << std::endl;
        std::cout << " *pd   = " << *pd << std::endl;

        delete pd;
        pd = nullptr;

        std::cout << " pd    = " << pd << std::endl;
        if (pd) std::cout << " *pd   = " << *pd << std::endl;

        std::cout << " --> example --> 1 -------------------> end" << std::endl;
    }

}