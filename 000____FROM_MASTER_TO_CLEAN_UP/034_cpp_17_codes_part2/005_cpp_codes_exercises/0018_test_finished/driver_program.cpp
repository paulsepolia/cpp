
//===============//
// test --> 0018 //
//===============//

// Status --> FINISHED

//==============================================================================
//
// --> the main points here are:
//
// --> 1 --> pass by constant reference an object
//           results in NO CONSTRUCTOR execution
//
// --> 2 --> pass by value an object results
//	     in copy constructor call
//
// --> 3 --> return a; (and the value is not a reference)
//           statement in a function
//           results in a call of copy constructor
//
//==============================================================================

#include <iostream>

using std::endl;
using std::cout;
using std::cin;

// class --> A

class A {
public:

    // constructor

    explicit A(int n = 0) : m_n(n)
    {
        cout << " --> constructor --> A" << endl;
    }

    // destructor

    ~A()
    {
        cout << " --> destructor --> ~A" << endl;
    }

    // copy constructor

    A(const A & a)
    {
        cout << " --> copy constructor --> A" << endl;

        this->m_n = a.m_n;
    }

    // operator = , the assignment operator

    A & operator = (const A & a)
    {
        cout << " --> A & operator = (const A &)--> A" << endl;

        this->m_n = a.m_n;
        ++m_assignment_calls;
        return *this;
    }

    // static variable

    static int m_assignment_calls;

private:

    int m_n;
};

// initialize static members

int A::m_assignment_calls = 0;

// function --> f

A f(const A & a)
{
    cout << " --> function --> f" << endl;

    return a;
}

// function --> g

A g(const A a)
{
    cout << " --> function --> g" << endl;

    return a;
}

// function --> k1

A & kre(A & a)
{
    cout << " --> function --> kre" << endl;

    return a;
}

// function --> kva

A & kva(A a)
{
    cout << " --> function --> kva" << endl;

    return a;
}

// the main function

int main()
{
    {
        // local innermost scope --> starts

        cout << " --> 1" << endl;

        A a(3); // creates the object a of type A
        // to do so, calls the constructor a

        cout << " --> 2" << endl;

        A b = a; // creates the object b of type A
        // which is equal to a, to do so calls the
        // copy constructor
        // DOES NOT CALL THE ASSIGNMENT OPERATOR

        cout << " --> 3" << endl;

        cout << A::m_assignment_calls << endl; // it is zero, since
        // the assignment operator
        // has not been called yet

        cout << " --> 4" << endl;

        b = g(a); // pass by value an object of type A
        // to do so, calls the copy constructor
        // then calls the body of function g
        // then calls the copy constructor to return a tmp value
        // then calls the assignment operator
        // and then calls the destructor two times, each
        // for the related copy constructor calls

        cout << " --> 5" << endl;

        g(b); // pass by value an object of type A
        // to do so, calls the copy constructor
        // then calls the main body of function g
        // then calls the copy constructor for the return tmp variable
        // there is NO ASSIGNMENT STATEMENT HERE
        // finally calls two times the destructor for
        // the equivalent calls of the copy constructor

        cout << " --> 6" << endl;

        cout << A::m_assignment_calls << endl; // it is 1
        // becuase of the step -->4

        cout << " --> 7" << endl;

        const A & c = f(b); // pass by reference the object b
        // returns a tmp value so calls the copy constructor
        // DOES NOT CALL ANYTHING ELSE
        // that tmp object dealocated at the exit of the scope

        cout << " --> 8" << endl;

        const A & c1 = b; // DOES NOT CALL ANYTHING

        cout << " --> 9" << endl;

        cout << A::m_assignment_calls << endl; // it is still 1

        cout << " --> 10" << endl;

        const A c2 = kre(b); // calls copy constructor to construct c2
        // NOTHING ELSE

        cout << " --> 11" << endl;

        const A c3 = kva(b); // calls copy constructor to pass by value
        // calls copy constructor to construct c3
        // calls destructor to destroy the 1st copy constructor call

        cout << " --> 12 --> end" << endl;

    } // local innermost scope --> ends

    cout << " --> 13 --> exit" << endl;

    // at the exit of the innermost local scope
    // the two objects a and b are being destroyed by calling
    // the destructor ~A
    // ALSO THE tmp return object in the step 7 is being destroyed
    // also the c2 and c3 are being destroyed

    // sentineling

    int sentinel;
    cin >> sentinel;

    cout << " --> 14 --> exit" << endl;

    return 0;
}

//======//
// FINI //
//======//

