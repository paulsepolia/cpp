
///==============//
// test --> 0030 //
//===============//

// Status --> FINISHED

#include <algorithm>
#include <iostream>
#include <iterator>

using std::begin;
using std::end;
using std::copy;
using std::unique;
using std::cout;
using std::cin;
using std::endl;
using std::ostream_iterator;

//  a function

bool compare(int a, int b)
{
    return a % 2 == b % 2;
}

// the main function

int main()
{
    cout << " --> 1" << endl;

    int a[] = {3, 1, 4, 6, 1, 3};

    cout << " --> 2" << endl;

    auto b = begin(a);

    cout << " --> 3" << endl;

    auto e = end(a);

    cout << " --> 4" << endl;

    e = unique(b, e, compare); // {3, 1, 4, 6, 1, 3} --> original
    // {1, 1, 0, 0, 1, 1} --> to compare
    // {3,1}, {4,6}, {1,3}
    // and unique keeps the first occurances
    // {3, 4, 1}


    cout << " --> 5" << endl;

    copy(b, e, ostream_iterator<int>(cout));

    cout << endl;

    cout << " --> 6" << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

