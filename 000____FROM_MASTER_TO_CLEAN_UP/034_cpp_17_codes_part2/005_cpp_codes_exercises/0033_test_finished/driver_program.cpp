
//===============//
// test --> 0033 //
//===============//

#include <iostream>
#include <stdexcept>

using std::cout;
using std::cin;
using std::endl;
using std::exception;
using std::logic_error;

// class --> A

class A {
public:

    // constructor

    A(int n)
    {
        cout << " --> constructor --> A" << endl;

        if (0 == n) {
            throw logic_error("0");
        }
    }

    // destructor

    ~A()
    {
        cout << " --> destructor --> ~A" << endl;
    }

};

// the main function

int main()
{
    {
        // local innermost scope --> starts

        cout << " --> 1" << endl;

        A * p0 = nullptr; // DOES NOT CALL ANYTHING

        cout << " --> 2" << endl;

        A * p1 = nullptr; // DOES NOT CALL ANYTHING

        cout << " --> 3" << endl;

        A * p2 = nullptr; // DOES NOT CALL ANYTHING

        cout << " --> 4" << endl;

        try {
            cout << " --> 5" << endl;

            p1 = new A(1); // calls the constructor A

            cout << " --> 6" << endl;

            p0 = new A(0); // calls the constructor A
            // throws an exception
            // calls the destructor for the p1
            // the p0 never created

            cout << " --> 7" << endl;

            p2 = new A(2);

            cout << " --> 8" << endl;
        } catch (const exception &) {
            // catches the exception
            cout << " --> 9" << endl;

            cout << 3 << endl;

            cout << " --> 10" << endl;
        }

        cout << " --> 11" << endl;

        cout << ((0 != p1) ? 1 : 0) << endl;
        // p1 is in an undefined state
        // never created and destroyed completely
        // so it is true --> 1

        cout << " --> 12" << endl;

        cout << ((0 != p0) ? 1 : 0) << endl;
        // p0 is NULL so it is false --> 0
        // p0 points to an object which is destroyed

        cout << " --> 13" << endl;

        cout << ((0 != p2) ? 1 : 0) << endl;
        // p2 is NULL so it is false --> 0

        cout << " --> 14" << endl;

        delete p1;

        cout << " --> 15" << endl;

        delete p0;

        cout << " --> 16" << endl;

        delete p2;

        cout << " --> 17" << endl;

    }

    cout << " --> 18 --> exit" << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    cout << " --> 19 --> exit" << endl;

    return 0;
}

//======//
// FINI //
//======//

