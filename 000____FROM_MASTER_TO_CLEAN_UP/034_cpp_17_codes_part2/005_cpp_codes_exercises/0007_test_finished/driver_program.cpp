
//===============//
// test --> 0007 //
//===============//

// Status --> FINISHED

//==============================================================================
//
// --> the main points are:
//
// --> 1 --> does an interpret_cast<char*> to an object of type A,
//	     then does an interpret_cast<char*> to a member variable
//	     then takes the difference of the two pointers
//	     which is the size of the object ... somehow
//
//==============================================================================

#include <cstddef>
#include <iostream>

using std::endl;
using std::cin;
using std::cout;

// class --> A

class A {
public:

    // constructor

    A() : m_x(0)
    {
        cout << " --> constructor --> A" << endl;
    }

    // destructor

    virtual ~A()
    {
        cout << " --> destructor --> ~A" << endl;
    }

    // function --> member_offset

    static ptrdiff_t member_offset(const A &a)
    {
        cout << " --> function --> A --> member_offset" << endl;

        const char * p = reinterpret_cast<const char*>(&a);

        cout << " --> &a = " << &a << endl;

        const char * q = reinterpret_cast<const char*>(&a.m_x);

        cout << " --> &a.m_x = " << &a.m_x << endl;

        return (q-p);
    }

private:

    int m_x;
};

// class --> B

class B : public A {
public:

    // constructor

    B() : A(), m_x('a')
    {
        cout << " --> constructor --> B" << endl;
    }

    // destructor

    virtual ~B()
    {
        cout << " --> destructor --> ~B" << endl;
    }

    // static public member

    static int m_n;

    // function --> member_offset

    static ptrdiff_t member_offset(const B &b)
    {
        cout << " --> function --> B --> member_offset" << endl;

        const char * p = reinterpret_cast<const char*>(&b);

        cout << " --> &b = " << &b << endl;

        const char * q = reinterpret_cast<const char*>(&b.m_x);

        cout << " --> &b.m_x = " << &b.m_x << endl;

        return (q-p);
    }

private:

    char m_x;
};

int B::m_n = 1;

// class --> C

class C {
public:

    // constructor

    C() : m_x(0)
    {
        cout << " --> constructor --> C" << endl;
    }

    // destructor

    virtual ~C()
    {
        cout << " --> destructor --> ~C" << endl;
    }

    // function --> member_offset

    static ptrdiff_t member_offset(const C &c)
    {
        cout << " --> function --> C --> member_offset" << endl;

        const char * p = reinterpret_cast<const char*>(&c);

        cout << " --> &c = " << &c << endl;

        const char * q = reinterpret_cast<const char*>(&c.m_x);

        cout << " --> &c.m_x = " << &c.m_x << endl;

        return (q-p);
    }

private:

    int m_x;
};

// the main function

int main()
{
    {
        // local innermost scope --> starts

        cout << " --> 1" << endl;

        A a; // creates the object a of type A
        // to do so calls the constructor A

        cout << " --> 2" << endl;

        B b; // creates an object of type B,
        // to do so calls calls the constructor A
        //	and then the constructor B

        cout << " --> 3" << endl;

        C c; // creates the object c of type C
        // to do so calls the constructor

        cout << " --> 4" << endl;

        cout << ((A::member_offset(a) == 0) ? 0 : 1) << endl;
        // it is false, so it outputs 1

        cout << " --> 5" << endl;

        cout << " A::member_offset(a) = " << A::member_offset(a) << endl;

        cout << " --> 6" << endl;

        cout << ((B::member_offset(b) == 0) ? 0 : 2) << endl;
        // it is false, so it outputs 2

        cout << " --> 7" << endl;

        cout << " B::member_offset(b) = " << B::member_offset(b) << endl;

        cout << " --> 8" << endl;

        cout << ((A::member_offset(b) == 0) ? 0 : 3) << endl;
        // it is false, so it outputs 3

        cout << " --> 9" << endl;

        cout << " A::member_offset(b) = " << A::member_offset(b) << endl;

        cout << " --> 10" << endl;

        cout << ((C::member_offset(c) == 0) ? 0 : 4) << endl;
        // it is false, so it outputs 4

        cout << " --> 11" << endl;

        cout << " C::member_offset(c) = " << C::member_offset(c) << endl;

        // at the exit of the innermost local scope
        // 4 destructors are being called since
        // I have created 3 objects, but the object B
        // calls 2 destructors (base and derived)
        // The objects while exiting their scope
        // are being destroyed in LIFO fashion
        // since the stack memory is a "stack container" ...

    } // local innermost scope --> ends

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

