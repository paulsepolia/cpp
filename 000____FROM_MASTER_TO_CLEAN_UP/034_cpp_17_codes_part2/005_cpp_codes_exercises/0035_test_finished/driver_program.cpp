//===============//
// test --> 0035 //
//===============//

// Status --> FINISHED

#include <iostream>

using std::cout;
using std::endl;

//==============//
// class Aminus //
//==============//

class Aminus {
protected:
    static int iterN;
};

// static member initialization

int Aminus::iterN = 0;

//=====================//
// template class A<N> //
//=====================//

template<unsigned int N>
class A : public Aminus {
public:

    A()
    {
        cout << " --> constructor --> A --> N = " << N << endl;
        iterN++;
        cout << " --> iterN = " << iterN << endl;
    }

    ~A()
    {
        cout << " --> destructor --> ~A --> N = " << endl;
        iterN--;
        cout << " --> iterN = " << iterN << endl;
    }

private:

    A<N-1> m_a;
};

//=======================================//
// a specific instance of the class A<N> //
//=======================================//

template<>
class A<0> {
public:

    A()
    {
        iter0++;
        cout << " --> constructor --> A<0> --> iter0 = " << iter0 << endl;
    }

    ~A()
    {
        iter0--;
        cout << " --> destructor --> ~A<0> --> iter0 = " << iter0 << endl;
    }

private:

    static int iter0;
};

// static member initialization

int A<0>::iter0 = 0;

// the main function

int main()
{
    {
        cout << " --> 1" << endl;

        A<10>(); // the private member A<N-1> is being called recursively
        // and for A<0> the specific class is called
        // and then the recursion works backwards

        cout << " --> 2" << endl;

    }

    return 0;
}

// end
