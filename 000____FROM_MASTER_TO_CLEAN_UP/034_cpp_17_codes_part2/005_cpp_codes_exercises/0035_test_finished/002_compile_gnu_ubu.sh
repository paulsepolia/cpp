#!/bin/bash

  g++ 	-O3                \
      	-Wall              \
      	-std=c++0x         \
		-ftemplate-depth=10000 \
      	driver_program.cpp \
          -o x_gnu_ubu
