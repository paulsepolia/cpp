
//===============//
// test --> 0001 //
//===============//

// Status --> FINISHED

//==============================================================================
//
// The main points are the following:
//
// 1 --> A a = b; --> use of copy constructor
//
// 2 --> A a(b);  --> use of copy constructor
//
// 3 --> a = b;   --> use of assignment operator
//		      either that provided by the system
//		      or that provided by the developer
//
// 4 --> void f(A & a) --> pass by reference an object of type A
//		       --> if the object is already then no constructor is called
//		       --> if the is an implict conversion then the constructor is called
//
// 5 --> void f(A a)   --> pass by value an object of type A
//		       --> since the object is being copied the
//		       --> copy constructor is being called
//
// 6 --> A & a = b;    --> Does not create any new object, so
//		       --> there is no call to any kind of constructors
//
// 7 --> A * k;        --> Does not create any new object, so
// 		       --> there is no call to any kind of constructors
//
// 8 --> A * p = new A(c); --> creates an object of type A, which is equal to c
//			   --> to do that calls the copy constructor
//			   --> p points to that object
//
// 9 --> delete p;         --> calls the destructor
//
//==============================================================================

#include <iostream>
#include <iomanip>

using std::endl;
using std::cout;
using std::cin;
using std::boolalpha;

// class --> A

class A {
public:

    // constructor

    A(int n = 0, int k = 10) : m_n (n)
    {
        cout << " --> constructor --> A" << endl;
        cout << "d" << endl;
    }

    // copy constructor

    A(const A & a) : m_n (a.m_n)
    {
        cout << " --> copy constructor --> A" << endl;
        cout << "c" << endl;
    }

    // operator ==

    bool operator == (const A & a1) const
    {
        cout << " --> operator == --> A" << endl;
        return (this->m_n == a1.m_n);
    }

    // destructor ~A

    virtual ~A()
    {
        cout << " --> destructor --> ~A" << endl;
    }

    // operator =

    A & operator = (const A & a)
    {
        cout << " --> operator --> =" << endl;

        this->m_n = a.m_n;
        return *this;
    }

private:

    int m_n;
};

// funtion --> f1

void f1(const A & a1, const A & a2 = A(1,1))
{
    cout << " --> function --> f1" << endl;
}

// funtion --> f2

void f2(const A a1, const A a2 = A(1,1))
{
    cout << " --> function --> f2" << endl;
}

// the main function

int main()
{
    cout << boolalpha;

    {
        // local innermost scope --> starts

        cout << " --> 1" << endl;

        A k1;            // creates the object k1 of type A
        // calls the constructor A

        cout << " --> 2" << endl;

        A k2(k1);        // creates the object k2 of type A
        // calls the copy contructor

        cout << " --> 3" << endl;

        A k3();          // nothing, k3 is a function with return type A
        // which does not take any arguments

        cout << " --> 4" << endl;

        A & k4 = k1;     // nothing, k4 is a placeholder for k1

        cout << " --> 5" << endl;

        A * k5;          // nothing, k5 is a pointer to a type A

        cout << " --> 6" << endl;

        A a(2,2);        // creates the object a
        // calls the constructor A

        cout << " --> 7" << endl;

        A b(3,3);        // calls the constructor A
        // creates the object b

        cout << " --> 8" << endl;

        const A c(a);    // creates the const object c
        // calls the copy constructor

        cout << " --> 9" << endl;

        const A & d = c; // nothing

        cout << " --> 10" << endl;

        const A e = b;   // create the const object e
        // calls the copy constructor
        // DOES NOT CALL the compiler provided
        // assignment operator

        cout << " --> 11" << endl;

        k5 = &k1; // k5 points to k1 object

        cout << " --> 12" << endl;

        cout << "&k1 = " << &k1 << endl;

        cout << " --> 13" << endl;

        cout << "&k4 = " << &k4 << endl;

        cout << " --> 14" << endl;

        cout << " k5 = " <<  k5 << endl;

        cout << " --> 15" << endl;

        cout << " k1 == k4 ==> " << (k1 == k4) << endl;

        cout << " --> 16" << endl;

        cout << " k1 == *k5 ==> " << (k1 == *k5) << endl;

        cout << " --> 17" << endl;

        b = d; // calls the provided assignment operator

        cout << " --> 18" << endl;

        a = e; // calls the provided assignment operator

        cout << " --> 19" << endl;

        A * p = new A(c); // creates and object to which p point
        // calls the copy constructor

        cout << " --> 20" << endl;

        A * q = & a; // nothing

        cout << " --> 21" << endl;

        static_cast<void>(q); // nothing

        cout << " --> 22" << endl;

        delete p; // calls the destructor A

        cout << " --> 23" << endl;

        f1(a, a); // calls the function f1
        // DOES NOT call any kind of constructros,
        // since I use pass by reference and the objects
        // already have been created elsewhere

        cout << " --> 24" << endl;

        f1(1, 1); // calls the function f1
        // the constructor is NOT declared 'explicit'
        // so there is an implicit conversion from int to A
        // So the constructor is being called 2 times
        // to create two objects
        // and then the destructor is being called twice

        cout << " --> 25" << endl;

        f1(e, e); // calls the function f1
        // DOES NOT call any kind of constructors,
        // since I use pass by reference and the
        // objects have already been created elsewhere

        cout << " --> 26" << endl;

        f2(3, 3); // calls the function f1
        // the constructor in NOT declared 'explicit'
        // so there is an implicit conversion from int to A
        // So the constructor is being called 2 times
        // to create two objects and then the destructor
        // is being called twice, since the created objects
        // are not in use anywhere

        cout << " --> 27" << endl;

        f2(e, e); // calls the function f2,
        // pass by value the object e, so
        // the copy constructor is being called two times
        // because the created objects by the copy constructor
        // are not in use the next step is the system to call
        // the destructor twice

        cout << " --> 28" << endl;
    }

    cout << " --> 29 --> exit" << endl;

    int sentinel;
    cin >> sentinel;

    cout << " --> 30 --> exit" << endl;

    return 0;
}

//======//
// FINI //
//======//

