
//===============//
// test --> 0016 //
//===============//

// Status --> FINISHED

//==============================================================================
//
// --> the main points are:
//
// --> 1 --> You can define a function f which throws an exception.
// 	     The function can be recursive.
//	     When the function throws an exception then the programs
//  	     terminates, or if the function is inside a try-catch block
//	     then the exception is being caught by the catch(exception &e) part
//	     and the function returns. The rest of the program progress normally
//
//==============================================================================

#include <iostream>
#include <stdexcept>

using std::endl;
using std::cin;
using std::cout;
using std::logic_error;
using std::exception;

// class --> A

class A {
public:

    // constructor

    A(int n) : m_n(n)
    {
        cout << " --> constructor --> A" << endl;
        cout << m_n << endl;
    }

    // destructor

    ~A()
    {
        cout << " --> destructor --> ~A" << endl;
        cout << m_n << endl;
    }

private:

    int m_n;
};

// function --> f

int f(int n)
{
    cout << " --> f --> 1" << endl;

    if (n == 1) {
        cout << " --> f --> 2" << endl;

        throw logic_error("0");

        cout << " --> f --> 3" << endl; // never reaches that point
    }

    cout << " --> f --> 4" << endl;

    A l(n); // creates the object l of type A
    // to do so, calls the constructor A with n = 5

    cout << " --> f --> 5" << endl; // then goes here

    return f(n - 1) * n / (n - 1); // and the goes here, so calls again f with n = 4
    // and the whole process is repeated until n = 1
    // if n == 1 then enters the if clause and
    // throws a logic_error("0") and returns from the function
    // Returning from the function, destroyes all the locally
    // created objects, so it calls the destructor ~A
    // in the reverse order
}

// the main function

int main()
{
    {
        // local innermost scope --> starts

        cout << " --> 1" << endl;

        try {
            cout << " --> 2" << endl;

            int r = f(5); // it is a recursive function
            // never goes beyond that point

            cout << " --> 3" << endl; // NEVER REACHES THAT POINT

            cout << " --> r = " << r << endl; // NEVER REACHES THAT POINT

            cout << " --> 4" << endl; // NEVER REACHES THAT POINT

            A a(r); // NEVER REACHES THAT POINT

            cout << " --> 5" << endl; // NEVER REACHES THAT POINT
        } catch (const exception &e) {
            cout << " --> 6" << endl; // is here after the step 2 is over

            cout << e.what() << endl; // the value here is "0"

            cout << " --> 7" << endl; // and then is here
        }

        cout << " --> 8 --> end" << endl;

    } // local innermost scope --> ends

    // at the exit of the innermost local scope there is
    // no call to any destructor, since the object were local to function
    // and have been destroyed at the return of the function, step 2

    cout << " --> 9 --> exit" << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    cout << " --> 10 --> exit" << endl;

    return 0;
}

//======//
// FINI //
//======//

