
//===============//
// test --> 0032 //
//===============//

// Status --> FINISHED

#include <iostream>

using std::cout;
using std::cin;
using std::endl;

// --> a template class

template<unsigned N>
class A {
public:

    A()
    {
        cout << " --> constructor --> A" << endl;
        cout << N << endl;
    }

    ~A()
    {
        cout << " --> destructor --> ~A" << endl;
    }

private:

    A<N-1> m_a;
};

// --> a specific class

template<>
class A<0> {
public:

    A()
    {
        cout << " --> constructor --> A<0>" << endl;
        cout << 'A' << endl;
    }

    ~A()
    {
        cout << " --> destructor --> ~A<0>" << endl;
    }
};

// the main function

int main()
{
    {
        // local innermost scope --> starts

        cout << " --> 1" << endl;

        A<5>(); // the private member A<N-1> is being called recursively
        // and for A<0> the specific class is called
        // and then the recursion works backwards

        cout << " --> 2" << endl;

    } // local innermost scope --> ends

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

