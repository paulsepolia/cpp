
//===============//
// test --> 0027 //
//===============//

// Status --> FINISHED

#include <iostream>
#include <stdexcept>

using std::logic_error;
using std::cin;
using std::cout;
using std::endl;
using std::exception;

// class --> A

class A {
public:

    // constructor

    A(int n) : m_n(n)
    {
        cout << " --> constructor --> A" << endl;

        if (0 == n) {
            throw logic_error("0");
        }
    }

    // destructor

    virtual ~A()
    {
        cout << " --> destructor --> ~A" << endl;
        cout << m_n << endl;
    }

public:

    const int m_n;
};

int main()
{
    {
        // local innermost scope --> starts

        cout << " --> 1" << endl;

        try {
            cout << " --> 2" << endl;

            A a(1); // calls constructor

            cout << " --> 3" << endl;

            A b(0); // calls constructor
            // throws an exception
            // calls destructor for the a(1) object
            // pass control to 'catch'
            // NEVER CREATES the b(0) object
            // so NEVER CALLS the destructor for b(0)

            cout << " --> 4" << endl;

            A c(2);

            cout << " --> 5" << endl;
        } catch (const exception &) {
            cout << " --> 6" << endl;
            cout << 3 << endl;
            cout << " --> 7" << endl;
        }

        cout << " --> 8" << endl;

    } // local innermost scope --> ends

    cout << " --> 9 --> exit" << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    cout << " --> 10 --> exit" << endl;

    return 0;
}

//======//
// FINI //
//======//

