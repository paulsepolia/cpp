
//===============//
// test --> 0023 //
//===============//

// Status --> FINISHED

#include <algorithm>
#include <cassert>
#include <cstddef>
#include <functional> // std::mem_fn
#include <iostream>
#include <memory> // std::default_delete
#include <vector>

using std::cout;
using std::endl;
using std::cin;
using std::vector;
using std::stable_sort;
using std::mem_fn;
using std::default_delete;

// class --> A

class A {
public:

    // constructor

    A() : m_size(sizeof(A))
    {
        cout << " --> constructor --> A" << endl;
    }

public:

    virtual void f() const
    {
        cout << " --> function --> f --> A" << endl;
        cout << 1 << endl;
    }

    // destructor

    virtual ~A()
    {
        cout << " --> destructor --> ~A" << endl;
    }

public:

    static bool compare(const A *a, const A *b)
    {
        cout << " --> function --> compare --> 1" << endl;
        assert(a != nullptr);
        cout << " --> function --> compare --> 2" << endl;
        assert(b != nullptr);
        cout << " --> function --> compare --> 3" << endl;

        return a->m_size < b->m_size;
    }

protected:

    size_t m_size;
};

// class --> B

class B : public A {
public:

    // constructor

    B() : m_b(nullptr)
    {
        cout << " --> constructor --> B" << endl;
        m_size = sizeof(B);
    }

    // destructor

    ~B()
    {
        cout << " --> destructor --> ~B" << endl;
    }

public:

    virtual void f() const
    {
        cout << " --> function --> f --> B" << endl;
        cout << 2 << endl;
    }

private:

    char *m_b;
};

// class --> C

class C : public A {
public:

    // constructor

    C()
    {
        cout << " --> constructor --> C" << endl;
        m_size = sizeof(C);
    }

    // destructor

    ~C()
    {
        cout << " --> destructor --> ~C" << endl;
    }

public:

    virtual void f() const
    {
        cout << " --> function --> f --> C" << endl;
        cout << 3 << endl;
    }

private:

    static int *m_c;
};

int *C::m_c = nullptr;

// the main function

int main()
{
    {
        // local innermost scope --> starts

        cout << " --> 1" << endl;

        vector<A*> v({ new C, new B, new A}); // calls the constructors C, A,
        // then B, A and then A

        cout << " --> 2" << endl;

        stable_sort(v.begin(), v.end(), A::compare); // calls the function compare many times
        // the assert is wrong
        // so is not executed

        cout << " --> 3" << endl;

        for_each(v.begin(), v.end(), mem_fn(&A::f));

        cout << " --> 4" << endl;

        cout << endl;

        for_each(v.begin(), v.end(), default_delete<A>());

        cout << " --> 5" << endl;

    } // local innermost scope --> ends

    cout << " --> 6 --> exit" << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    cout << " --> 7 --> exit" << endl;

    return 0;
}

//======//
// FINI //
//======//

