
//===============//
// test --> 0006 //
//===============//

// Status --> FINISHED

#include <algorithm>
#include <functional>
#include <iostream>
#include <iterator>
#include <list>

using std::cout;
using std::cin;
using std::endl;
using std::list;
using std::transform;
using std::copy;
using std::ostream_iterator;

// the main function

int main()
{
    cout << " --> 1" << endl;

    typedef list<int> L; // L is another name for the type
    // list<int>

    cout << " --> 2" << endl;

    L l(5); // this is a list of integers of size 5

    cout << " --> 3" << endl;

    typedef L::const_iterator CI; // CI is another name for the type
    // list<int>::const_iterator

    cout << " --> 4" << endl;

    CI cb = l.begin();

    cout << " --> 5" << endl;

    CI ce = l.end();

    cout << " --> 6" << endl;

    typedef L::iterator I; // I is another name for the type
    // list<int>::iterator

    cout << " --> 7" << endl;

    I b = l.begin();

    // --> 1

    cout << " --> 8" << endl;

    CI::value_type n; // n is uninitialized (GNU C++ warns about that fact))

    cout << " --> 9" << endl;

    cout << " --> n = " << n << endl; // n is 0 but is not for sure

    // --> 2

    cout << " --> 10" << endl;

    CI::value_type n1 = 100;

    cout << " --> 11" << endl;

    cout << " --> n1 = " << n1 << endl; // n is 100 for sure

    // --> 3

    cout << " --> 11" << endl;

    transform(cb, --ce, ++b, [] (CI::value_type n) {
        return ++n;
    });

    cout << " --> 12" << endl;

    copy(l.begin(), l.end(), ostream_iterator<CI::value_type>(cout));

    cout << endl;

    // --> 4

    cout << " --> 13" << endl;

    transform(cb, ce, b, [] (CI::value_type n) {
        return n;
    });

    cout << " --> 14" << endl;

    copy(l.begin(), l.end(), ostream_iterator<CI::value_type>(cout));

    cout << endl;

    // --> 5

    cout << " --> 15" << endl;

    transform(cb, ce, b, [] (CI::value_type n) {
        return n++;
    });

    cout << " --> 16" << endl;

    copy(l.begin(), l.end(), ostream_iterator<CI::value_type>(cout));

    cout << endl;

    // --> 6

    cout << " --> 17" << endl;

    transform(cb, ce, b, [] (CI::value_type n) {
        return ++n;
    });

    cout << " --> 18" << endl;

    copy(l.begin(), l.end(), ostream_iterator<CI::value_type>(cout));

    cout << endl;

    // --> 7

    cout << " --> 19" << endl;

    transform(cb, ce, b, [] (CI::value_type n = 10) {
        return ++n;
    });
    // the initialization does not play any role !
    // I DO NOT KNOW WHY

    cout << " --> 20" << endl;

    copy(l.begin(), l.end(), ostream_iterator<CI::value_type>(cout));

    cout << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

