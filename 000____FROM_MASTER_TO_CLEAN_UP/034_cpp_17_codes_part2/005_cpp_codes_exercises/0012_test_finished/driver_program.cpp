
//===============//
// test --> 0012 //
//===============//

// Status --> FINISHED

//==============================================================================
//
// --> the main points here are:
//
// --> 1 -->  void f(A, A), pass by value the two objects
//	      to do so, the copy constructor takes action two times
//	      the local to function scope objects are being destroyed
//	      at the return of the function
//
// --> 2 --> void f(A&, A&), pass by reference the two objects
//	     to do so, NONE of the constructors is being executed
//
// --> 3 --> void f(1,2), works for both pass by value and pass by reference schemes
//			  because the constructor is not declared explicit
//			  the pass-by-value case WORKS THE SAME as the
//			  pass-by-reference case so, the constructor takes action two times.
//			  At the exit of the function scope the destructor
//			  is being called twice
//
//==============================================================================

#include <iostream>
#include <iomanip>

using std::endl;
using std::cout;
using std::cin;
using std::boolalpha;

// class --> A

class A {
public:

    // constructor with zero arguments

    A() : m_n (-1)
    {
        cout << " --> constructor --> A --> zero arguments" << endl;
    }

    // constructor with one argument

    A(int n) : m_n (n)
    {
        cout << " --> constructor --> A --> one argument" << endl;
    }

    // constructor with two arguments

    A(int n, int m) : m_n (n)
    {
        cout << " --> constructor --> A --> two arguments" << endl;
    }

    // copy constructor

    A(const A & a) : m_n (a.m_n)
    {
        cout << " --> copy constructor --> A" << endl;
    }

    // destructor

    ~A()
    {
        cout << " --> destructor --> ~A" << endl;
    }

    // assignment operator =

    // replaces the default provided by the compiler
    // DOES NOT replace the copy constructor
    // which is still being executed in the statement
    // A a1 = a2;

    A & operator = (const A & a1)
    {
        cout << " --> A & operator = (const A &)" << endl;

        this->m_n = a1.m_n;

        return *this;
    }

    // operator ==

    bool operator == (const A & a1) const
    {
        return (this->m_n == a1.m_n);
    }


private:

    int m_n;
};

// function --> f1

void f1(const A & x1, const A & x2)
{
    cout << " --> function --> f1" << endl;
}

// function --> f2

void f2(const A x1, const A x2)
{
    cout << " --> function --> f2" << endl;
}

// the main function

int main()
{
    // adjust the output format

    cout << boolalpha;

    {
        // local innermost scope --> starts

        cout << " --> 1" << endl;

        A a1; // creates the object a1 of type A, to do so
        // calls the constructor A with zero arguments

        cout << " --> 2" << endl;

        A a2(2); // creates the object a2 of type A, to do so
        // calls the constructor A with one argument

        cout << " --> 3" << endl;

        A a3(3, 4); // creates the object a3 of type A, to do so
        // calls the constructorA with two arguments

        cout << " --> 4" << endl;

        const A a4(a1);  // creates the object a4 of type A, to do so
        // calls the copy constructor

        cout << " --> 5" << endl;

        const A a5 = a1; // creates the object a5 of type A, to do so
        // calls the copy constructor
        // DOES NOT CALL the provided overloaded operator =

        cout << " --> 6" << endl;

        cout << " --> a5 == a1 ==> " << (a5==a1) << endl; // it is true

        cout << " --> 7" << endl;

        A & a6(a1); // a6 is a reference of type A to a1
        // is not a fully fleged object, so
        // nothing is being called

        cout << " --> 8" << endl;

        cout << " --> a6 == a1 ==> " << (a6==a1) << endl; // it is true

        cout << " --> 9" << endl;

        A & a7 = a1; // a7 is a reference of type A to a1
        // is not a fuly flegged object, so
        // nothing is being called

        cout << " --> 10" << endl;

        cout << " --> a1 == a7 ==> " << (a1==a7) << endl; // it is true

        cout << " --> 11" << endl;

        A * p1; // p1 is a pointer of type A

        cout << " --> 12" << endl;

        p1 = new A; // calls the constructor A with zero arguments

        cout << " --> 13" << endl;

        p1 = new A(1); // calls the constructor A with one argument

        cout << " --> 14" << endl;

        p1 = new A(1, 2); // calls the constructor A with two arguments

        cout << " --> 15" << endl;

        delete p1; // calls the destructor ~A

        cout << " --> 16" << endl;

        a1 = a2; // calls the operator =
        // if operator = is not defined by the programmer
        // then the compiler creates one and the C++ system uses it

        cout << " --> 17" << endl;

        a7 = a1; // calls the operator =
        // if operator = does not exist then uses the
        // that provided by the compiler

        cout << " --> 18" << endl;

        p1 = &a1; // does not call anything

        cout << " --> 19" << endl;

        a6 = a2; // calls the operator =
        // if operator = does not exist then uses the
        // that provided by the compiler

        cout << " --> 20" << endl;

        f1(a1, a2); // pass by reference the objects a1 and a2
        // since the objects a1 and a2 are already created elsewhere
        // no constructor is being called

        cout << " --> 21" << endl;

        A a10 = 10; // object a10 is being created
        // there is an impicit conversion equaivalent to A a10 = A(10);

        cout << " --> 22" << endl;

        A a11 = A(10); // SAME as step 21

        cout << " --> 23" << endl;

        cout << " --> (a10 == a11) = " << (a10 == a11) << endl; // LOOK STEPS 21 and 22

        cout << " --> 24" << endl;

        f1(1, 2); // pass by reference the objects 1 and 2
        // 1 and 2 create two object in the following way
        // 1 --> A(1) , 2 --> A(2)
        // there is an implicit conversion from int to A
        // ALSO calls two times the destructor

        cout << " --> 25" << endl;

        f2(3, 4); // calls two times the constructor with one argument, since
        // the constructor is NOT declared 'explicit'
        // so there is an implicit conversion from int to A
        // ALSO calls two times the destructor
        // IT IS THE SAME AS STEP 24

        cout << " --> 26" << endl;

        f2(a1, a2); // calls two times the copy constructor
        // during the pass by value scheme mechanism
        // ALSO calls two times the destructor

        cout << " --> 27 --> end" << endl;

    } // local innermost scope --> ends

    cout << " --> 28 --> exit" << endl;

    // at the exit of this scope
    // the destructor is being called 7 times for the objects
    // a1, a2, a3, a4, a5, a10, a11
    // a6 and a7 are placeholders for a1 ...

    int sentinel;
    cin >> sentinel;

    cout << " --> 29 --> exit" << endl;

    return 0;
}

//======//
// FINI //
//======//

