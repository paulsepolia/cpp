//===============//
// test --> 0036 //
//===============//

// Status --> FINISHED

#include <iostream>
#include <cmath>

using std::cout;
using std::cin;
using std::endl;
using std::pow;

//==============//
// class Aminus //
//==============//

class Aminus {
protected:
    static unsigned long int iter;
};

unsigned long int Aminus::iter = 0;

//=====================//
// template class A<N> //
//=====================//

template<unsigned long int N>
class A : private Aminus {
public:

    A()
    {
        iter++;
        cout << " --> constructor --> A --> N = " << N << endl;
        cout << " --> iter = " << iter << endl;
    }

    ~A()
    {
        iter--;
        cout << " --> destructor --> ~A --> N = " << N << endl;
        cout << " --> iter = " << iter << endl;
    }
};

// the main function

int main()
{
    const unsigned long int IMAX = static_cast<unsigned long int>(pow(10.0, 1.0));

    for(unsigned long int i = 0; i != IMAX; i++) {
        cout << " ----------------------------------------------->> i = " << i << endl;
        //A<i>(); // ERROR: i must be a compile-time constant
        A<10>();
    }

    return 0;
}

// end
