#include <iostream>
#include <cstdint>
#include <iomanip>
#include <functional>
#include <random>
#include <chrono>

template<typename T>
void print_vector(const std::vector<T> &vec) {
    for (const auto &el: vec) {
        std::cout << el << " ";
    }
    std::cout << std::endl;
}

template<typename T>
double apply_function_T(T f, const double &arg) {
    return f(arg);
}

double mult2(const double &arg) {
    return 2.0 * arg;
}

double mult3(const double &arg) {
    return 3.0 * arg;
}

template<typename T>
bool sort_function_T(const T &a, const T &b) {
    return a < b;
}

template<typename T>
struct sort_class_T {
    bool operator()(const T &a, const T &b) {
        return a < b;
    }
};


std::uint64_t seed_f() {
    return std::chrono::system_clock::now().time_since_epoch().count();
}

int main() {

    {
        std::cout << " --> example --> 1 --> start" << std::endl;

        std::function<double(const double &)> f = [](const double &x) { return 2 * x; };

        std::cout << apply_function_T(f, 10.0) << std::endl;

        std::cout << " --> example --> 1 --> end" << std::endl;
    }

    {
        std::cout << " --> example --> 2 --> start" << std::endl;

        std::function<double(const double &)> f1 = mult2;

        std::cout << apply_function_T(f1, 10.0) << std::endl;

        std::function<double(const double &)> f2 = mult3;

        std::cout << apply_function_T(f2, 10.0) << std::endl;

        std::cout << " --> example --> 2 --> end" << std::endl;
    }

    {
        std::cout << " --> example --> 3 --> start" << std::endl;

        std::vector<double> vec{-1.0, 2.0, 3.0, 1.0, 9.0};

        print_vector(vec);

        std::sort(std::begin(vec), std::end(vec));

        print_vector(vec);

        std::shuffle(std::begin(vec), std::end(vec), std::default_random_engine(seed_f()));

        print_vector(vec);

        std::sort(std::begin(vec), std::end(vec), [](double a1, double a2) { return a1 < a2; });

        print_vector(vec);

        std::sort(std::begin(vec), std::end(vec), [](double a1, double a2) { return a1 > a2; });

        print_vector(vec);

        std::sort(std::begin(vec), std::end(vec), sort_function_T<double>);

        print_vector(vec);

        std::shuffle(std::begin(vec), std::end(vec), std::default_random_engine(seed_f()));

        print_vector(vec);

        std::function<bool(const double &, const double &)> f1 = sort_function_T<double>;

        std::sort(std::begin(vec), std::end(vec), f1);

        print_vector(vec);

        std::shuffle(std::begin(vec), std::end(vec), std::default_random_engine(seed_f()));

        print_vector(vec);

        std::function<bool(const double &, const double &)> f2 = sort_class_T<double>();

        std::sort(std::begin(vec), std::end(vec), f2);

        print_vector(vec);

        std::cout << " --> example --> 3 --> end" << std::endl;
    }
}