#include <iostream>
#include <memory>
#include <map>
#include <string>

class A {

public:

    explicit A(uint32_t x) : x_(x) {}

    uint32_t x_;

    virtual void f1() const {
        std::cout << " --> base " << std::endl;
    }

    virtual ~A() {
        std::cout << "----> ~A" << std::endl;
        f1();
    }
};

class B : public A {
public:

    B() : A(100), x_(101) {}

    void f1() const final {
        std::cout << " --> derived " << std::endl;
    }

    uint32_t x_;

    ~B() final {
        std::cout << "----> ~B" << std::endl;
        f1();
    }

};

int main() {

    B b1;

    return 0;
}