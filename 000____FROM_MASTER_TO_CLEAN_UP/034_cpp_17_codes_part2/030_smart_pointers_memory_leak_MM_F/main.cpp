#include <iostream>
#include <iomanip>
#include <memory>
#include <cmath>

class A {
public:
    A() : sp(nullptr) {}

    std::shared_ptr<A> sp;
};

int main() {

    {
        std::cout << " --> example --> 1 --> start" << std::endl;

        std::shared_ptr<A> sp1 = std::make_shared<A>();
        std::shared_ptr<A> sp2 = std::make_shared<A>();

        std::cout << " --> sp1.use_count() = " << sp1.use_count() << std::endl;
        std::cout << " --> sp2.use_count() = " << sp2.use_count() << std::endl;

        sp1->sp = sp2;

        std::cout << " --> sp1.use_count() = " << sp1.use_count() << std::endl;
        std::cout << " --> sp2.use_count() = " << sp2.use_count() << std::endl;

        sp2->sp = sp1;

        std::cout << " --> sp1.use_count() = " << sp1.use_count() << std::endl;
        std::cout << " --> sp2.use_count() = " << sp2.use_count() << std::endl;

        std::cout << " --> example --> 1 --> end" << std::endl;
    }
}