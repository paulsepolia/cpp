#!/bin/bash

  # compile

  g++ -O3 -c driver_program.cpp 

  # link

  g++  driver_program.o                    \
       /usr/lib/x86_64-linux-gnu/libpcre.a \
       -static                             \
       -o x_gnu

  # clean

  rm *.o
