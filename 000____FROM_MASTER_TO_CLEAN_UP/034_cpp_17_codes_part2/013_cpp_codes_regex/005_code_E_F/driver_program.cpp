
//======================//
// regex_search example //
//======================//

#include <iostream>
#include <string>
#include <regex>

using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::regex;
using std::smatch;
using std::regex_search;

// the main program

int main ()
{
    cout << "-------------------->> 1" << endl;

    string tmp ("this subject has a submarine as a subsequence");
    string s = "";

    cout << " --> build the string" << endl;

    for (int i = 0; i != 10; i++) {
        s = s + tmp;
    }

    cout << "-------------------->> 2" << endl;

    smatch m;

    cout << "-------------------->> 3" << endl;

    regex e ("\\b(sub)([^ ]*)");   // matches words beginning by "sub"
    // WORKS ONLY FOR g++-4.9.2 and g++-5.2.0
    // BREAKS FOR INTEL 15.1 and g++-4.6.4

    cout << "-------------------->> 4" << endl;

    cout << "Target sequence: " << s << endl;
    cout << "Regular expression: /\\b(sub)([^ ]*)/" << endl;
    cout << "The following matches and submatches were found:" << endl;

    while (regex_search (s,m,e)) {
        for (auto x:m) {
            cout << x << " ";
        }

        cout << endl;
        s = m.suffix().str();

        cout << " s = " << s << endl;
    }

    return 0;
}

// END
