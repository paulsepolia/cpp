#!/bin/bash

  # compile

  icc -c driver_program.c

  # produce executable

  icc  driver_program.o                     \
       /usr/lib/x86_64-linux-gnu/libpcre.so \
       -o x_intel

  # clean

  rm driver_program.o

