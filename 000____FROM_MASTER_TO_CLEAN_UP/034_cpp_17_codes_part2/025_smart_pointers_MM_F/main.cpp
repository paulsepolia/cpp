#include <iostream>
#include <iomanip>
#include <set>
#include <memory>

class A {

public:

    A() = default;

    explicit A(double x) : _x(x) {}

    auto get_value() const {
        return _x;
    };

    auto set_value(double x) {
        _x = x;
    };

    virtual ~A() = default;

    bool operator<(const A &obj) const {
        return _x < obj._x;
    }

private:

    double _x{0};
};

class compare_shared_ptr {
public:

    template<typename T>
    auto operator()(const std::shared_ptr<T> &sp1, const std::shared_ptr<T> &sp2) const {
        return *sp1 < *sp2;
    }
};

class compare_unique_ptr {
public:

    template<typename T>
    auto operator()(const std::unique_ptr<T> &up1, const std::unique_ptr<T> &up2) const {
        return *up1 < *up2;
    }
};


int main() {

    {
        std::cout << " --> example --> 1" << std::endl;

        A a1(10);
        A a2(9);
        A a3(21);

        std::set<A> sa;

        sa.insert(a1);
        sa.insert(a2);
        sa.insert(a3);

        for (const auto &el: sa) {
            std::cout << el.get_value() << std::endl;
        }
    }

    {
        std::cout << " --> example --> 2" << std::endl;

        A a1;
        A a2;
        A a3;

        std::set<A> sa;

        sa.insert(a1);
        sa.insert(a2);
        sa.insert(a3);

        for (const auto &el: sa) {
            std::cout << el.get_value() << std::endl;
        }
    }

    {
        std::cout << " --> example --> 3" << std::endl;

        A a1;
        A a2;
        A a3;

        std::set<A> sa;

        a1.set_value(10);
        a2.set_value(9);
        a3.set_value(8);

        sa.insert(a1);
        sa.insert(a2);
        sa.insert(a3);

        for (const auto &el: sa) {
            std::cout << el.get_value() << std::endl;
        }
    }

    {
        std::cout << " --> example --> 4" << std::endl;

        std::shared_ptr<A> pa1;
        std::shared_ptr<A> pa2;
        std::shared_ptr<A> pa3;

        std::set<std::shared_ptr<A>> spa;

        spa.insert(std::make_shared<A>(A(10)));
        spa.insert(std::make_shared<A>(A(10)));
        spa.insert(std::make_shared<A>(A(10)));

        for (const auto &el: spa) {
            std::cout << el->get_value() << std::endl;
        }
    }

    {
        std::cout << " --> example --> 5" << std::endl;

        const std::shared_ptr<A> pa1 = std::make_shared<A>(10);
        const std::shared_ptr<A> pa2 = std::make_shared<A>(11);
        const std::shared_ptr<A> pa3 = std::make_shared<A>(12);

        std::set<std::shared_ptr<A>, compare_shared_ptr> spa;

        spa.insert(pa1);
        spa.insert(pa2);
        spa.insert(pa3);

        for (const auto &el: spa) {
            std::cout << el->get_value() << std::endl;
        }
    }

    {
        std::cout << " --> example --> 6" << std::endl;

        const std::shared_ptr<A> pa1 = std::make_shared<A>(10);
        const std::shared_ptr<A> pa2 = std::make_shared<A>(10);
        const std::shared_ptr<A> pa3 = std::make_shared<A>(10);

        std::set<std::shared_ptr<A>, compare_shared_ptr> spa;

        spa.insert(pa1);
        spa.insert(pa2);
        spa.insert(pa3);

        for (const auto &el: spa) {
            std::cout << el->get_value() << std::endl;
        }
    }

    {
        std::cout << " --> example --> 7" << std::endl;

        std::unique_ptr<A> pa1 = std::make_unique<A>(10);
        std::unique_ptr<A> pa2 = std::make_unique<A>(10);
        std::unique_ptr<A> pa3 = std::make_unique<A>(10);

        std::set<std::unique_ptr<A>, compare_unique_ptr> upa;

        upa.insert(std::move(pa1));
        upa.insert(std::move(pa2));
        upa.insert(std::move(pa3));

        for (const auto &el: upa) {
            std::cout << el->get_value() << std::endl;
        }
    }

    {
        std::cout << " --> example --> 8" << std::endl;

        std::unique_ptr<A> pa1 = std::make_unique<A>(10);
        std::unique_ptr<A> pa2 = std::make_unique<A>(11);
        std::unique_ptr<A> pa3 = std::make_unique<A>(12);

        std::set<std::unique_ptr<A>, compare_unique_ptr> upa;

        upa.insert(std::move(pa1));
        upa.insert(std::move(pa2));
        upa.insert(std::move(pa3));

        for (const auto &el: upa) {
            std::cout << el->get_value() << std::endl;
        }
    }
}