//==================//
// reinterpret_cast //
//==================//

#include <iostream>
#include <typeinfo>

using std::cout;
using std::endl;

// function --> f1

int f1()
{
    return 42;
}

// function --> f2

double f2()
{
    return 12.34;
}

// function --> f3

long double f3()
{
    return 34.56;
}

// class A1

class A1 {
public:
    virtual double f1()
    {
        return 12.34;
    }
};

// class A2

class A2 : public A1 {
public:
    virtual double f1()
    {
        return 56.78;
    }
};

// class A3

class A3 : public A2 {
public:
    virtual double f1()
    {
        return 20.00;
    }
};

//===================//
// the main function //
//===================//

int main()
{
    int i1 = 10;
    double d1 = 12.34;
    long double ld1 = 34.56;

    cout << typeid(i1).name()  << endl;
    cout << typeid(d1).name()  << endl;
    cout << typeid(ld1).name() << endl;
    cout << typeid(f1).name()  << endl;
    cout << typeid(f2).name()  << endl;
    cout << typeid(f3).name()  << endl;
    cout << typeid(A1).name()  << endl;
    cout << typeid(A2).name()  << endl;
    cout << typeid(A3).name()  << endl;

    A1 a1;
    A2 a2;
    A3 a3;

    cout << " --> a1.f1() = " << a1.f1() << endl;
    cout << " --> a2.f1() = " << a2.f1() << endl;
    cout << " --> a3.f1() = " << a3.f1() << endl;

    cout << typeid(a1).name() << endl;
    cout << typeid(a2).name() << endl;
    cout << typeid(a3).name() << endl;

    cout << typeid(a1.f1()).name() << endl;
    cout << typeid(a2.f1()).name() << endl;
    cout << typeid(a3.f1()).name() << endl;


    return 0;
}
