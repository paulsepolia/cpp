//==============//
// late binding //
//==============//

#include <iostream>

using std::endl;
using std::cout;

// class --> Base

class Base {
public:

    virtual void function1()
    {
        cout << " Base --> fun1" << endl;
    };
};

// class --> Base2

class Base2 {
public:
    void function1()
    {
        cout << " Base2 --> fun1" << endl;
    }
};

// class --> D1

class D1: public virtual Base {
public:

    virtual void function1()
    {
        cout << " D1 --> fun1" << endl;
    };
};

// class --> D2

class D2: public virtual Base {
public:

    virtual void function1()
    {
        cout << " D2 --> fun1" << endl;
    };
};

// class --> E1

class E1: public D1, public D2 {
public:

    virtual void function1()
    {
        cout << " E1 --> fun1" << endl;
    };

    virtual void function2()
    {
        cout << " E1 --> fun2" << endl;
    };
};

// class --> E2

class E2: public Base2 {
public:
    virtual void function1()
    {
        cout << " E2 --> fun1" << endl;
    };
};

// the main function

int main()
{
    // local variables

    Base b1;
    D1 d1;
    D2 d2;
    E1 e1;

    //============//
    // base class //
    //============//

    cout << " ---------------------------------->  1" << endl;
    cout << " b1.function1();" << endl;

    b1.function1();

    //==================//
    // derived D1 class //
    //==================//

    cout << " ---------------------------------->  2" << endl;
    cout << " d1.function1();" << endl;

    d1.function1();

    //==================//
    // derived D2 class //
    //==================//

    cout << " ---------------------------------->  3" << endl;
    cout << " d2.function1();" << endl;

    d2.function1();

    //=================//
    // derived E class //
    //=================//

    cout << " ---------------------------------->  4" << endl;
    cout << " e1.function1();" << endl;
    cout << " e1.function1();" << endl;

    e1.function1();
    e1.function2();

    //====================//
    // base class pointer //
    //====================//

    Base * pb1(nullptr);

    // # 1

    cout << " ---------------------------------->  5" << endl;

    // pb1 is of type Base pointer
    // pb1 is initialized using address of a Base object
    // so we call the Base functions

    cout << " pb1 = &b1;" << endl;
    cout << " pb1->function1();" << endl;

    pb1 = &b1;
    pb1->function1();

    // # 2

    cout << " ---------------------------------->  6" << endl;

    // pb1 is of type Base pointer
    // pb1 is initialized using address of a derived D1 object
    // so we call the derived functions (if any)

    // UPCASTING

    cout << " pb1 = &d1;" << endl;
    cout << " pb1->function1();" << endl;

    pb1 = &d1;
    pb1->function1();

    // # 3

    cout << " ---------------------------------->  7" << endl;

    // UPCASTING
    // pb1 is of type Base pointer
    // pb1 is initialized using address of a derived D2 object
    // so we call the derived functions (if any)

    cout << " pb1 = &d2;" << endl;
    cout << " pb1->function1();" << endl;

    pb1 = &d2;
    pb1->function1();

    //==========================//
    // derived D1 class pointer //
    //==========================//

    D1 * pd1(nullptr);

    cout << " ---------------------------------->  8" << endl;

    cout << " pd1 = &d1;" << endl;
    cout << " pd1->function1();" << endl;

    pd1 = &d1;
    pd1->function1();

    //==========================//
    // derived D2 class pointer //
    //==========================//

    D2 * pd2(nullptr);

    cout << " ---------------------------------->  9" << endl;

    cout << " pd2 = &d2;" << endl;
    cout << " pd2->function1();" << endl;

    pd2 = &d2;
    pd2->function1();

    //==========================//
    // derived E1 class pointer //
    //==========================//

    E1 * pe1(nullptr);

    cout << " ----------------------------------> 10" << endl;

    pe1 = &e1;

    cout << " pe1 = &e1;" << endl;
    cout << " pe1->function1();" << endl;
    cout << " pe1->function2();" << endl;

    pe1->function1();
    pe1->function2();


    // ERRORS:

    // # 1
    // The base class 'Base' is virtual
    // so cannot convert the pointer from (Base*) to (D1*)
    // DOWNCASTING ERROR

    //pd1 = &b1;

    // # 2
    // The base class 'Base' is virtual
    // so cannot convert the pointer from (Base*) to (D2*)
    // DOWNCASTING ERROR

    //pd2 = &b1;

    // # 3
    // The base class 'Base' is virtual
    // so cannot convert the pointer from (Base*) to (E1*)
    // DOWNCASTING ERROR

    //pe1 = &b1;

    // # 4
    // Invalid conversion from D1* to E1*;
    // DOWNCASTING ERROR

    //pe1 = &d1;

    // # 5
    // The following compiles but is can lead to runtime errors
    // since the pe1 pointer is initialized with a not complete object of type *D1
    // DOWNCASTING RUNTIME ERROR

    //pe1 = static_cast<E1*>(&d1);

    // # 6
    // The following compiles but fails always
    // since the pe1 pointer is initialized with a not complete object of type *D1
    // But we can check if the pointer is NULL and take action
    // IT WORKS OKAY
    // DOWNCASTING

    //pe1 = dynamic_cast<E1*>(&d1);

    //==============//
    // dynamic cast //
    //==============//

    // # 1

    cout << " ----------------------------------> 11" << endl;

    // DOWNCAST

    pb1 = new D1;
    pd1 = dynamic_cast<D1*>(pb1);

    if(pd1) {
        cout << " dynamic cast of base pointer pb1 to derived pd1" << endl;
    }

    // # 2

    cout << " ----------------------------------> 12" << endl;

    // DOWNCAST

    pb1 = new D2;
    pd2 = dynamic_cast<D2*>(pb1);

    if(pd2) {
        cout << " dynamic cast of base pointer pb1 to derived pd2" << endl;
    }

    // # 3

    cout << " ----------------------------------> 13" << endl;

    // DOWNCAST

    pb1 = new E1;
    pe1 = dynamic_cast<E1*>(pb1);

    if(pe1) {
        cout << " dynamic cast of base pointer pb1 to derived pe1" << endl;
    }

    // # 4

    cout << " ----------------------------------> 14" << endl;

    // DOWNCAST

    pb1 = new Base;
    pe1 = dynamic_cast<E1*>(pb1);

    if(pe1) {
        cout << " dynamic cast of base pointer pb1 to derived pe1" << endl;
    } else if(pe1 == 0) {
        cout << " FAILURE trying to dynamic cast base pointer to derived pointer" << endl;
    }

    //=====================//
    // Non-virtual classes //
    //=====================//

    // # 1

    Base2 b2;
    E2 e2;

    Base2 * pb2(nullptr);
    E2 * pe2(nullptr);

    cout << " ----------------------------------> 15" << endl;
    cout << " pb2 = &b2;" << endl;
    cout << " pb2->function1();" << endl;

    pb2 = &b2; // OKAY
    pb2->function1();

    cout << " ----------------------------------> 16" << endl;
    cout << " pb2 = &e2;" << endl;
    cout << " pb2->function1();" << endl;

    pb2 = &e2; // OKAY::Upcasting derived to base
    pb2->function1();

    // ERRORS

    //pe2 = &b2; // ERROR::COMPILE TIME ERROR::DOWNCASTING base to derived
    //pe2 = dynamic_cast<E2*>(&b2); // ERROR::COMPILE TIME ERROR::Base is not polymorphic

    cout << " ----------------------------------> 17" << endl;
    cout << " pe2 = &e2;" << endl;
    cout << " pe2->function1();" << endl;

    pe2 = &e2; // OKAY
    pe2->function1();

    // # 2

    pb2 = new Base2; // OKAY
    pb2 = new E2;    // OKAY::UPCASTING
    //pe2 = new Base2; // ERROR::COMPILE TIME ERROR::DOWNCASTING
    pe2 = new E2;	  // OKAY

    //pe2 = dynamic_cast<E2*>(pb2); //ERROR::COMPLIE TYPE ERROR::pb2 must be a pointer
    // of a polymorphic base class

    return 0;
}

// end
