//=============//
// linked list //
//=============//

#include <cmath>
#include <vector>
#include <iostream>
using std::endl;
using std::cout;

#define DEBUG
//#undef DEBUG

// list definition

class list {
public:
    std::vector<int> * pdata1;
    list * next;
};

typedef unsigned long int uli;

// the main function

int main()
{
    const uli DIM(std::pow(10.0, 3.0));
    const uli DIM2(std::pow(10.0, 6.0));
    const uli TR(2*std::pow(10.0, 1.0));

    for(int k = 0; k != TR; k++) {

        cout << "-------------------------------->> k = " << k << endl;

        // # 1 create nodes

        list * head(new list);
        list * node(new list);

        // initialize the head

        head->next = node;

        // build the nodes

        for(uli i = 0; i != DIM; i++) {
            node->pdata1 = new std::vector<int>;
            node->pdata1->resize(DIM2);
            for(uli j = 0; j != node->pdata1->size(); j++) {
                node->pdata1->at(j) = j;
            }
            node->next = new list;
            node = node->next;
        }

        // finalize the nodes

        node->next = 0;
        node->pdata1 = new std::vector<int>;

        // delete the nodes
        node = head;
        while(node->next) {
            list * q;
            q = node->next;
            q->pdata1->clear();
            q->pdata1->shrink_to_fit();
            node->next = q->next;
            delete q->pdata1;
            delete q;
        }

        // delete the head
        delete head;
    }

    return 0;
}

// END
