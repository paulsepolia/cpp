
#include <iostream>

using std::endl;
using std::cout;
using std::cin;

// base class --> B1

class B1 {
public:

    B1()
    {
        cout << " --> constructor --> B1" << endl;
        x1 = 10;
        x2 = 20;
        x3 = 30;
    }

    virtual ~B1() {}

private:

    int x1;
    int x2;
    int x3;
};

// derived class --> D1

class D1 : public B1 {
public:

    D1() : B1()
    {
        cout << " --> constructor --> D1" << endl;
        x4 = 40;
        x5 = 50;
        x6 = new int (60);
    }

    ~D1()
    {
        cout << " --> destructor --> ~D1" << endl;
        if (x6) {
            cout << " --> delete --> ~D1" << endl;
            delete x6;
        }
    }

private:

    int x4;
    int x5;
    int * x6;
};

// base class

class Base {
public:

    Base(B1 * x): x_loc(x)
    {
        cout << " --> constructor --> Base" << endl;
    }

    virtual ~Base()
    {
        cout << " --> destructor --> ~Base" << endl;
        if (x_loc) {
            cout << " --> delete --> ~Base" << endl;
            delete x_loc;
        }
    }

private:

    B1 * x_loc;
};

// derived class

class Derived: public Base {
public:

    Derived(): Base(new D1())
    {
        cout << " --> constructor --> Derived" << endl;
    }
};

// the main program

int main()
{
    Base a1(new D1());
    Derived a2;

    return 0;
}

// end
