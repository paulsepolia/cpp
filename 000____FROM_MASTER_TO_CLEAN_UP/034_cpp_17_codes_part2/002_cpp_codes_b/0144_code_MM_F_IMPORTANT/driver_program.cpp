//==============================================//
// driver program 						   //
// move contructor and move assignment operator //
// and unique_ptr 						   //
//==============================================//

#include "parameters.hpp"
#include "vec.hpp"
#include <iostream>
#include <cmath>
#include <iomanip>

using std::cout;
using std::endl;
using std::cos;
using std::sin;
using std::fixed;
using std::setprecision;
using std::showpos;
using std::boolalpha;

// the main program

int main()
{
    cout << setprecision(10);
    cout << fixed;
    cout << showpos;
    cout << boolalpha;

    bool tmp_bool;

    for(uli i = 0; i != TRIALS; i++) {
        cout << "----------------------------------------------->> " << i << endl;

        Vec<double> v1;
        Vec<double> v2;

        cout << " ---------->> check allocation" << endl;
        tmp_bool = v1.check_allocation();
        cout << " ---------->> v1 --> " << tmp_bool << endl;
        tmp_bool = v2.check_allocation();
        cout << " ---------->> v2 --> " << tmp_bool << endl;

        cout << " ---------->> allocate" << endl;

        v1.allocate(DIMEN);
        v2.allocate(DIMEN);

        cout << " ---------->> check allocation" << endl;
        tmp_bool = v1.check_allocation();
        cout << " ---------->> v1 --> " << tmp_bool << endl;
        tmp_bool = v2.check_allocation();
        cout << " ---------->> v2 --> " << tmp_bool << endl;

        cout << " ---------->> build --> v1" << endl;

        for(uli j = 0; j != DIMEN; j++) {
            v1.set(j, sin(static_cast<double>(j)));
        }

        cout << " ---------->> build --> v2" << endl;

        for(uli j = 0; j != DIMEN; j++) {
            v2.set(j, cos(static_cast<double>(j+1)));
        }

        // some operations # 1

        Vec<double> v3;
        v3.allocate(v1.get_size());

        cout << " --> v3 = add(v1,v2);" << endl;
        v3.add(v1,v2);
        cout << " --> v1[10] = " << v1[10] << endl;
        cout << " --> v2[10] = " << v2[10] << endl;
        cout << " --> v3[10] = " << v3[10] << endl;

        cout << " --> v3 = subtract(v1,v2);" << endl;
        v3.subtract(v1,v2);
        cout << " --> v1[10] = " << v1[10] << endl;
        cout << " --> v2[10] = " << v2[10] << endl;
        cout << " --> v3[10] = " << v3[10] << endl;

        cout << " --> v3 = multiply(v1,v2);" << endl;
        v3.multiply(v1,v2);
        cout << " --> v1[10] = " << v1[10] << endl;
        cout << " --> v2[10] = " << v2[10] << endl;
        cout << " --> v3[10] = " << v3[10] << endl;

        cout << " --> v3 = divide(v1,v2);" << endl;
        v3.divide(v1,v2);
        cout << " --> v1[10] = " << v1[10] << endl;
        cout << " --> v2[10] = " << v2[10] << endl;
        cout << " --> v3[10] = " << v3[10] << endl;

        // some operations # 2

        cout << " --> v3 = add(v1,val);" << endl;
        const double val = 10.0;
        v3.add(v1,val);
        cout << " --> v1[10] = " << v1[10] << endl;
        cout << " --> v3[10] = " << v3[10] << endl;

        cout << " --> v3 = subtract(v1,val);" << endl;
        v3.subtract(v1,val);
        cout << " --> v1[10] = " << v1[10] << endl;
        cout << " --> v3[10] = " << v3[10] << endl;

        cout << " --> v3 = multiply(v1,val);" << endl;
        v3.multiply(v1,val);
        cout << " --> v1[10] = " << v1[10] << endl;
        cout << " --> v3[10] = " << v3[10] << endl;

        cout << " --> v3 = divide(v1,val);" << endl;
        v3.divide(v1,val);
        cout << " --> v1[10] = " << v1[10] << endl;
        cout << " --> v3[10] = " << v3[10] << endl;

        // some operations # 3

        cout << " --> v3 = add(val,v1);" << endl;
        v3.add(val,v1);
        cout << " --> v1[10] = " << v1[10] << endl;
        cout << " --> v3[10] = " << v3[10] << endl;

        cout << " --> v3 = subtract(val,v1);" << endl;
        v3.subtract(val,v1);
        cout << " --> v1[10] = " << v1[10] << endl;
        cout << " --> v3[10] = " << v3[10] << endl;

        cout << " --> v3 = multiply(val,v1);" << endl;
        v3.multiply(val,v1);
        cout << " --> v1[10] = " << v1[10] << endl;
        cout << " --> v3[10] = " << v3[10] << endl;

        cout << " --> v3 = divide(val,v1);" << endl;
        v3.divide(val,v1);
        cout << " --> v1[10] = " << v1[10] << endl;
        cout << " --> v3[10] = " << v3[10] << endl;

        // some operations # 4

        cout << " --> v3 = v1+v2;" << endl;
        v3 = v1+v2;
        cout << " --> v1[10] = " << v1[10] << endl;
        cout << " --> v2[10] = " << v2[10] << endl;
        cout << " --> v3[10] = " << v3[10] << endl;

        cout << " --> v3 = v1-v2;" << endl;
        v3 = v1-v2;
        cout << " --> v1[10] = " << v1[10] << endl;
        cout << " --> v2[10] = " << v2[10] << endl;
        cout << " --> v3[10] = " << v3[10] << endl;

        cout << " --> v3 = v1*v2;" << endl;
        v3 = v1*v2;
        cout << " --> v1[10] = " << v1[10] << endl;
        cout << " --> v2[10] = " << v2[10] << endl;
        cout << " --> v3[10] = " << v3[10] << endl;

        cout << " --> v3 = v1/v2;" << endl;
        v3 = v1/v2;
        cout << " --> v1[10] = " << v1[10] << endl;
        cout << " --> v2[10] = " << v2[10] << endl;
        cout << " --> v3[10] = " << v3[10] << endl;

        // some operations # 5

        cout << " --> v3 = v1+val;" << endl;
        v3 = v1+val;
        cout << " --> v1[10] = " << v1[10] << endl;
        cout << " --> v3[10] = " << v3[10] << endl;

        cout << " --> v3 = v1-val;" << endl;
        v3 = v1-val;
        cout << " --> v1[10] = " << v1[10] << endl;
        cout << " --> v3[10] = " << v3[10] << endl;

        cout << " --> v3 = v1*val;" << endl;
        v3 = v1*val;
        cout << " --> v1[10] = " << v1[10] << endl;
        cout << " --> v3[10] = " << v3[10] << endl;

        cout << " --> v3 = v1/val;" << endl;
        v3 = v1/val;
        cout << " --> v1[10] = " << v1[10] << endl;
        cout << " --> v3[10] = " << v3[10] << endl;

        // some operations # 6

        cout << " --> v3 = val+v1;" << endl;
        v3 = val+v1;
        cout << " --> v1[10] = " << v1[10] << endl;
        cout << " --> v3[10] = " << v3[10] << endl;

        cout << " --> v3 = val-v1;" << endl;
        v3 = val-v1;
        cout << " --> v1[10] = " << v1[10] << endl;
        cout << " --> v3[10] = " << v3[10] << endl;

        cout << " --> v3 = val*v1;" << endl;
        v3 = val*v1;
        cout << " --> v1[10] = " << v1[10] << endl;
        cout << " --> v3[10] = " << v3[10] << endl;

        cout << " --> v3 = val/v1;" << endl;
        v3 = val/v1;
        cout << " --> v1[10] = " << v1[10] << endl;
        cout << " --> v3[10] = " << v3[10] << endl;

        // copy assignment operator

        cout << " ---------->> v1 = v2;" << endl;

        cout << " --> before" << endl;
        cout << " --> v1[10] = " << v1[10] << endl;
        cout << " --> v2[10] = " << v2[10] << endl;

        v1 = v2;

        cout << " --> after" << endl;
        cout << " --> v1[10] = " << v1[10] << endl;
        cout << " --> v2[10] = " << v2[10] << endl;

        // move assignment operator

        cout << " ---------->> v1 = move(v2);" << endl;

        cout << " --> before" << endl;
        cout << " --> v1[10] = " << v1[10] << endl;
        cout << " --> v2[10] = " << v2[10] << endl;

        v1 = move(v2);

        cout << " --> after" << endl;
        cout << " --> v1[10] = " << v1[10] << endl;
        cout << " --> v2 is not allocated anymore" << endl;

        // some tests here

        //v2.get(10); // throws and error and quits
        //v2.set(10, 10.0); // throws an error and quits
        //v1.set(1000, 10.0);
        //v1.set(-1, 10.0);
        //v1.get(-10);
        //v2.get(0);
        //v2.get(-100);
        //v2.get(10);

        uli v1_size = v1.get_size();
        uli v2_size = v2.get_size();
        uli v3_size = v3.get_size();

        cout << " ---------->> v1_size = " << v1_size << endl;
        cout << " ---------->> v2_size = " << v2_size << endl;
        cout << " ---------->> v3_size = " << v3_size << endl;

        cout << " ---------->> deallocate" << endl;

        v1.deallocate();
        v2.deallocate();
        v3.deallocate();

        cout << " ---------->> check allocation" << endl;
        tmp_bool = v1.check_allocation();
        cout << " ---------->> v1 --> " << tmp_bool << endl;
        tmp_bool = v2.check_allocation();
        cout << " ---------->> v2 --> " << tmp_bool << endl;
        tmp_bool = v3.check_allocation();
        cout << " ---------->> v3 --> " << tmp_bool << endl;
    }

    cout << " p--> exit" << endl;
    return 0;
}

// end
