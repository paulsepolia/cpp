//======================//
// class Vec definition //
//======================//

#include "vec_declaration.hpp"
#include <utility>
#include <iostream>
#include <string>
#include <cstdlib>

using std::move;
using std::cout;
using std::endl;
using std::string;

// constructor

template<typename T>
Vec<T>::Vec() : _size(0), _p()
{}

// destructor

template<typename T>
Vec<T>::~Vec()
{
    if(_p) {
        _p.reset();
    }
}

// copy constructor

template<typename T>
Vec<T>::Vec(const Vec<T> & other)
{
    if(this != &other) {
        if(other._p) {
            this->deallocate();
            this->allocate(other._size);
            for(uli i = 0; i != other._size; i++) {
                this->set(i, other.get(i));
            }
        } else {
            this->deallocate();
        }
    }
}

// move constructor

template<typename T>
Vec<T>::Vec(Vec<T> && other)
{
    this->_size = other._size;
    this->_p = move(other._p);
}

// copy assignment operator

template<typename T>
const Vec<T> & Vec<T>::operator=(const Vec<T> & other)
{
    if(this != &other) {
        if(other._p.get()) {
            this->deallocate();
            this->allocate(other._size);
            for(uli i = 0; i != other._size; i++) {
                this->set(i, other.get(i));
            }
        } else if(other._p.get() == 0) {
            this->deallocate();
        }
    }

    return *this;
}

// move assignment operator

template<typename T>
const Vec<T> & Vec<T>::operator=(Vec<T> && other)
{
    this->deallocate();
    this->allocate(other._size);
    this->_p = move(other._p);

    other._p.reset();
    other._size = 0;

    return *this;
}

//==================//
// member functions //
//==================//

// allocate

template<typename T>
void Vec<T>::allocate(const uli & val_size)
{
    this->deallocate();
    this->_p.reset(new T [val_size]);
    this->_size = val_size;
}

// check_allocation

template<typename T>
bool Vec<T>::check_allocation() const
{
    bool tmp_val = false;
    if(this->_p) {
        tmp_val = true;
    }

    return tmp_val;
}

// deallocate

template<typename T>
void Vec<T>::deallocate()
{
    if(this->_p) {
        this->_p.reset();
        this->_size = 0;
    }
}

// get

template<typename T>
T Vec<T>::get(const uli & index) const throw(string)
{
    if(!this->check_allocation()) {
        throw_error_and_quit(ERROR_001);
    }

    if(((this->get_size()-1) < index) || (index < 0)) {
        throw_error_and_quit(ERROR_002);
    }

    return this->_p.get()[index];
}

// set

template<typename T>
void Vec<T>::set(const uli & index, const T & val)
{
    if(!this->check_allocation()) {
        throw_error_and_quit(ERROR_001);
    }

    if(((this->get_size()-1) < index) || (index < 0)) {
        throw_error_and_quit(ERROR_002);
    }

    this->_p.get()[index] = val;
}

// operator[]

template<typename T>
T Vec<T>::operator[](const uli & index)
{
    return this->get(index);
}

// get_size

template<typename T>
uli Vec<T>::get_size() const
{
    return this->_size;
}

// add # 1

template<typename T>
void Vec<T>::add(const Vec<T> & vec1, const Vec<T> & vec2)
{
    this->deallocate();
    this->allocate(vec1._size);

    for(uli i = 0; i != this->_size; i++) {
        this->_p.get()[i] = vec1._p.get()[i] + vec2._p.get()[i];
    }
}

// add # 2

template<typename T>
void Vec<T>::add(const Vec<T> & vec1, const T & val)
{
    this->deallocate();
    this->allocate(vec1._size);

    for(uli i = 0; i != this->_size; i++) {
        this->_p.get()[i] = vec1._p.get()[i] + val;
    }
}

// add # 3

template<typename T>
void Vec<T>::add(const T & val, const Vec<T> & vec1)
{
    this->deallocate();
    this->allocate(vec1._size);

    for(uli i = 0; i != this->_size; i++) {
        this->_p.get()[i] = vec1._p.get()[i] + val;
    }
}

// subtract # 1

template<typename T>
void Vec<T>::subtract(const Vec<T> & vec1, const Vec<T> & vec2)
{
    this->deallocate();
    this->allocate(vec1._size);

    for(uli i = 0; i != this->_size; i++) {
        this->_p.get()[i] = vec1._p.get()[i] - vec2._p.get()[i];
    }
}

// subtract # 2

template<typename T>
void Vec<T>::subtract(const Vec<T> & vec1, const T & val)
{
    this->deallocate();
    this->allocate(vec1._size);

    for(uli i = 0; i != this->_size; i++) {
        this->_p.get()[i] = vec1._p.get()[i] - val;
    }
}

// subtract # 3

template<typename T>
void Vec<T>::subtract(const T & val, const Vec<T> & vec1)
{
    this->deallocate();
    this->allocate(vec1._size);

    for(uli i = 0; i != this->_size; i++) {
        this->_p.get()[i] = val -vec1._p.get()[i];
    }
}

// multiply # 1

template<typename T>
void Vec<T>::multiply(const Vec<T> & vec1, const Vec<T> & vec2)
{
    this->deallocate();
    this->allocate(vec1._size);

    for(uli i = 0; i != this->_size; i++) {
        this->_p.get()[i] = vec1._p.get()[i] * vec2._p.get()[i];
    }
}

// multiply # 2

template<typename T>
void Vec<T>::multiply(const Vec<T> & vec1, const T & val)
{
    this->deallocate();
    this->allocate(vec1._size);

    for(uli i = 0; i != this->_size; i++) {
        this->_p.get()[i] = vec1._p.get()[i] * val;
    }
}

// multiply # 3

template<typename T>
void Vec<T>::multiply(const T & val, const Vec<T> & vec1)
{
    this->deallocate();
    this->allocate(vec1._size);

    for(uli i = 0; i != this->_size; i++) {
        this->_p.get()[i] = vec1._p.get()[i] * val;
    }
}

// divide # 1

template<typename T>
void Vec<T>::divide(const Vec<T> & vec1, const Vec<T> & vec2)
{
    this->deallocate();
    this->allocate(vec1._size);

    for(uli i = 0; i != this->_size; i++) {
        this->_p.get()[i] = vec1._p.get()[i] / vec2._p.get()[i];
    }
}

// divide # 2

template<typename T>
void Vec<T>::divide(const Vec<T> & vec1, const T & val)
{
    this->deallocate();
    this->allocate(vec1._size);

    for(uli i = 0; i != this->_size; i++) {
        this->_p.get()[i] = vec1._p.get()[i] / val;
    }
}

// divide # 3

template<typename T>
void Vec<T>::divide(const T & val, const Vec<T> & vec1)
{
    this->deallocate();
    this->allocate(vec1._size);

    for(uli i = 0; i != this->_size; i++) {
        this->_p.get()[i] = val / vec1._p.get()[i];
    }
}

// operator + # 1

template<typename T>
Vec<T> Vec<T>::operator+(const Vec<T> & other)
{
    Vec<T> res;
    res.add(*this, other);
    return move(res); // force to use move assignmnet operator
    // works equally fast the "return res;" version
    // since the system uses the provided move assignment operator
}

// operator + # 2

template<typename T>
Vec<T> Vec<T>::operator+(const T & val)
{
    Vec<T> res;
    res.add(*this, val);
    return move(res);
}

// operator + # 3

template<typename T>
Vec<T> operator+(const T & val, const Vec<T> & vec)
{
    Vec<T> res;
    res.add(val, vec);
    return move(res);
}

// operator - # 1

template<typename T>
Vec<T> Vec<T>::operator-(const Vec<T> & other)
{
    Vec<T> res;
    res.subtract(*this, other);
    return move(res);
}

// operator - # 2

template<typename T>
Vec<T> Vec<T>::operator-(const T & val)
{
    Vec<T> res;
    res.subtract(*this, val);
    return move(res);
}

// operator - # 3

template<typename T>
Vec<T> operator-(const T & val, const Vec<T> & vec)
{
    Vec<T> res;
    res.subtract(val, vec);
    return move(res);
}

// operator * # 1

template<typename T>
Vec<T> Vec<T>::operator*(const Vec<T> & other)
{
    Vec<T> res;
    res.multiply(*this, other);
    return move(res);
}

// operator * # 2

template<typename T>
Vec<T> Vec<T>::operator*(const T & val)
{
    Vec<T> res;
    res.multiply(*this, val);
    return move(res);
}

// operator * # 3

template<typename T>
Vec<T> operator*(const T & val, const Vec<T> & vec)
{
    Vec<T> res;
    res.multiply(val, vec);
    return move(res);
}

// operator / # 1

template<typename T>
Vec<T> Vec<T>::operator/(const Vec<T> & other)
{
    Vec<T> res;
    res.divide(*this, other);
    return move(res);
}

// operator / # 2

template<typename T>
Vec<T> Vec<T>::operator/(const T & val)
{
    Vec<T> res;
    res.divide(*this, val);
    return move(res);
}

// operator / # 3

template<typename T>
Vec<T> operator/(const T & val, const Vec<T> & vec)
{
    Vec<T> res;
    res.divide(val, vec);
    return move(res);
}

//==========================//
// private member functions //
//==========================//

// throw_error_and_quit

template<typename T>
string Vec<T>::throw_error_and_quit(string my_str) const
{
    try {
        throw my_str;
    } catch(string & error_code) {
        cout << error_code << endl;
        exit(EXIT_FAILURE);
    }
}

// end
