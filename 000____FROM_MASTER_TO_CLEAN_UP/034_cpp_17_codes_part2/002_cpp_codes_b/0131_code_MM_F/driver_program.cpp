//==================//
// reinterpret_cast //
//==================//

#include <cassert>
#include <iostream>

using std::cout;
using std::endl;
using std::hex;
using std::dec;

// function

int f()
{
    return 42;
}

//===================//
// the main function //
//===================//

int main()
{
    int i = 7;

    //=============================//
    // pointer to integer and back //
    //=============================//

    // assign the address of an int object to pointers of various types

    uintptr_t v1 = reinterpret_cast<uintptr_t>(&i); // static_cast is an error
    int* ip1 = &i;
    void* vp1 = &i;
    int* ip2 = static_cast<int*>(&i);
    int* ip3 = const_cast<int*>(&i);
    int* ip4 = (int*)(&i);
    int* ip5 = reinterpret_cast<int*>(&i);
    unsigned long int * ip6 = reinterpret_cast<unsigned long int*>(&i);

    // get the address - must be the same

    cout << " --> &i  = "   << hex << &i << endl;
    cout << " --> v1  = 0x" << hex << v1 << endl;
    cout << " --> ip1 = "   << hex << ip1 << endl;
    cout << " --> vp1 = "   << hex << vp1 << endl;
    cout << " --> ip2 = "   << hex << ip2 << endl;
    cout << " --> ip3 = "   << hex << ip3 << endl;
    cout << " --> ip4 = "   << hex << ip4 << endl;
    cout << " --> ip5 = "   << hex << ip5 << endl;
    cout << " --> ip6 = "   << hex << ip6 << endl;

    // get the value

    cout << " --> i    = " << i << endl;
    //cout << " --> *v1  = " << *v1 << endl;  // ERROR: must be a pointer to a complete object type
    cout << " --> *ip1 = " << *ip1 << endl;
    //cout << " --> *vp1 = " << *vp1 << endl; // ERROR: must be a pointer to a complete object type
    cout << " --> *ip2 = " << *ip2 << endl;
    cout << " --> *ip3 = " << *ip3 << endl;
    cout << " --> *ip4 = " << *ip4 << endl;
    cout << " --> *ip5 = " << *ip5 << endl;
    cout << " --> *ip6 = " << *ip6 << endl;

    assert(ip1 == &i);
    assert(ip2 == &i);
    assert(ip3 == &i);
    assert(ip4 == &i);
    assert(ip5 == &i);
    assert(vp1 == &i);
    //assert(v1  == &i); // ERROR: operands are incompatible
    //assert(ip6 == &i); // ERROR: operands are incompatible

    // get the value of the void pointers
    // by interpreting their pointing value to the defined pointer type

    int* p1 = reinterpret_cast<int*>(v1);
    int* p3 = reinterpret_cast<int*>(vp1);
    cout << " --> *p1 = " << *p1 << endl;
    cout << " --> *p3 = " << *p3 << endl;

    assert(p1 == &i);
    assert(p3 == &i);

    // pointer to function to another and back

    void(*fp1)() = reinterpret_cast<void(*)()>(f);
    //fp1(); //undefined behavior
    int(*fp2)() = reinterpret_cast<int(*)()>(fp1);
    cout << dec << fp2() << endl; // safe

    // type aliasing through pointer
    char* p2 = reinterpret_cast<char*>(&i);
    if(p2[0] == '\x7') {
        cout << "This system is little-endian" << endl;
    } else {
        cout << "This system is big-endian" << endl;
    }

    // type aliasing through reference
    reinterpret_cast<unsigned int&>(i) = 42;
    cout << i << endl;
}

