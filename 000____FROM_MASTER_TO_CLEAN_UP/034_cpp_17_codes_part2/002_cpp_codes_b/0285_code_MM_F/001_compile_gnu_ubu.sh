#!/bin/bash

  # 1. compile

  g++-5   -O3                \
          -Wall              \
          -std=gnu++17       \
          driver_program.cpp \
          -o x_gnu_ubu
