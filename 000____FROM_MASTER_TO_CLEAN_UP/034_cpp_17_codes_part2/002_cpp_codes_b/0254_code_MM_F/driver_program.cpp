//=====================//
// erasing from vector //
//=====================//

#include <iostream>
#include <vector>
#include <algorithm>

using std::cout;
using std::endl;

int main()
{
    std::vector<int> my_vec;

    // set some values (from 1 to 10)

    for (int i = 20; i <= 30; i++) {
        my_vec.push_back(i);
    }

    cout << " 1 --> my_vec.size() = " << my_vec.size() << endl;

    // erase the "2"

    int val(20);

    my_vec.erase(std::remove(my_vec.begin(), my_vec.end(), val), my_vec.end());

    cout << " 2 --> my_vec.size() = " << my_vec.size() << endl;

    // # 2

    my_vec.clear();
    my_vec.shrink_to_fit();

    for (int i = 20; i <= 30; i++) {
        my_vec.push_back(20);
    }

    cout << " 3 --> my_vec.size() = " << my_vec.size() << endl;

    val = 20;

    my_vec.erase(std::remove(my_vec.begin(), my_vec.end(), val), my_vec.end());

    cout << " 4 --> my_vec.size() = " << my_vec.size() << endl;

    return 0;
}

// end
