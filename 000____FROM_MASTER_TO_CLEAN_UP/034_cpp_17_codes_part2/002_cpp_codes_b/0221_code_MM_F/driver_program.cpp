//=================//
// dynamic binding //
//=================//

#include <iostream>
#include <typeinfo>

using std::cout;
using std::endl;

// class --> A1

class A1 {
public:

    virtual void gen_fun1(A1 &)
    {
        cout << " --> Hello from A1" << endl;
    }

    virtual void gen_fun2(A1 &)
    {
        cout << " --> Hello from A1" << endl;
    }
};

// class --> A2

class A2: public A1 {
public:

    virtual void gen_fun1(A1 &)
    {
        cout << " --> Hello from A2" << endl;
    }

    virtual void gen_fun2(A2 &)
    {
        cout << " --> Hello from A2" << endl;
    }
};

// class --> A3

class A3: public A2 {
public:

    virtual void gen_fun1(A1 &)
    {
        cout << " --> Hello from A3" << endl;
    }

    virtual void gen_fun2(A3 &)
    {
        cout << " --> Hello from A3" << endl;
    }
};

// the main function

int main()
{
    A1 a1;
    A2 a2;
    A3 a3;

    // # 1

    cout << " -------------------------------------->>  1" << endl;

    a1.gen_fun1(a1);
    a2.gen_fun1(a1);
    a3.gen_fun1(a1);

    // # 2

    cout << " -------------------------------------->>  2" << endl;

    a1.gen_fun2(a3);
    a2.gen_fun2(a3);
    a3.gen_fun2(a3);

    // # 3

    cout << " -------------------------------------->>  3" << endl;

    a1.gen_fun2(a1);

    //a2.gen_fun2(static_cast<A2>(a1)); // ERROR
    //a3.gen_fun2(static_cast<A3>(a1)); // ERROR

    //a2.gen_fun2(reinterpret_cast<A2>(a1)); // ERROR
    //a3.gen_fun2(reinterpret_cast<A3>(a1)); // ERROR

    //a2.gen_fun2((A2)a1); // ERROR
    //a3.gen_fun2((A3)a1); // ERROR

    // # 4

    cout << " -------------------------------------->>  4" << endl;

    A1 * pa1(nullptr);

    a1.gen_fun2(*pa1);
    a2.gen_fun2(*dynamic_cast<A2*>(pa1));
    a3.gen_fun2(*dynamic_cast<A3*>(pa1));

    a1.gen_fun2(*pa1);
    a2.gen_fun2(*static_cast<A2*>(pa1));
    a3.gen_fun2(*static_cast<A3*>(pa1));

    a1.gen_fun2(*pa1);
    a2.gen_fun2(*reinterpret_cast<A2*>(pa1));
    a3.gen_fun2(*reinterpret_cast<A3*>(pa1));

    return 0;
}

// end
