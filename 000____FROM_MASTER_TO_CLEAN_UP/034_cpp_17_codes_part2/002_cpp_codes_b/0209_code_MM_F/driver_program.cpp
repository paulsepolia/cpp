//===================//
// drop constantness //
//===================//

#include <iostream>
#include <typeinfo>

using std::cout;
using std::endl;

// struct --> A

struct A {
    double x;
};

const A* a = new A {1};

decltype(a->x) y;       // type of y is double (declared type)
decltype((a->x)) z = y; // type of z is const double& (lvalue expression)

// function --> add

template<typename T, typename U>
auto add(T t, U u) -> decltype(t + u); // return type depends on template parameters

// the main function

int main()
{
    cout << "------------------------------------->>  1" << endl;

    cout << " typeid(A).name() = " << typeid(A).name() << endl;
    cout << " typeid(y).name() = " << typeid(y).name() << endl;
    cout << " typeid(z).name() = " << typeid(z).name() << endl;
    cout << " typeid(add).name() = " << typeid(add<double, long double>).name() << endl;

    cout << "------------------------------------->>  2" << endl;

    unsigned long int i = 33;
    decltype(i) j = i * 2;

    cout << " typeid(j).name() = " << typeid(j).name() << endl;
    cout << " typeid(i).name() = " << typeid(i).name() << endl;

    cout << " i = " << i << endl;
    cout << " j = " << j << endl;

    cout << "------------------------------------->>  3" << endl;

    auto f = [](int a, int b) -> int {
        return a * b;
    };

    decltype(f) g(f); // the type of a lambda function is unique and unnamed

    i = f(2, 2);
    j = g(3, 3);

    cout << " typeid(f).name() = " << typeid(f).name() << endl;
    cout << " typeid(g).name() = " << typeid(g).name() << endl;

    cout << " i = " << i << endl;
    cout << " j = " << j << endl;

    return 0;
}

// end
