//=================//
// diamond problem //
//=================//

#include <iostream>
#include <iomanip>

using std::endl;
using std::cout;
using std::boolalpha;

// function --> 1

const int & fun1(const int & x)
{
    cout << " fun1 --> x = " << x << endl;
    return x;
}

// the main function

int main()
{
    cout << boolalpha;

    // const int and (const int* const) types

    int x1 = 10;

    x1 = fun1(x1);

    cout << " x1 = " << x1 << endl;

    int & x2(x1);

    int * p1(const_cast<int*>(&fun1(x2)));

    cout <<  " (p1 == &x2) = " << (p1 == &x2) << endl;

    int x3 = 111;

    *p1 = x3;

    cout << " *p1 = " << *p1 << endl;
    cout << "  x1 = " <<  x1 << endl;
    cout << "  x2 = " <<  x2 << endl;
    cout << "  x3 = " <<  x3 << endl;

    const int & x4(x1);

    cout << "  x4 = " << x4 << endl;
    p1 = const_cast<int*>(&x4);
    *p1 = 200;
    cout << "  x4 = " << x4 << endl;

    const int * px5;
    px5 = p1;
    cout << "*px5 = " << *px5 << endl;

    int * px6(const_cast<int*>(px5));

    *px6 = 300;

    cout << " *p1 = " << *p1 << endl;
    cout << "  x1 = " <<  x1 << endl;
    cout << "  x2 = " <<  x2 << endl;
    cout << "  x3 = " <<  x3 << endl;
    cout << "  x4 = " <<  x4 << endl;

    // function pointer

    const int & (*pfun1)(const int &);

    pfun1 = fun1;

    pfun1 (x1); // THE SAME
    (*pfun1)(x1); // THE SAME

    return 0;
}

// end
