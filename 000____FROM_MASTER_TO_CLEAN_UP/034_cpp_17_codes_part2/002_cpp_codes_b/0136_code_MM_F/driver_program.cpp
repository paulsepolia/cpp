//===================//
// function pointers //
//===================//

#include <iostream>

using std::cout;
using std::cin;
using std::endl;

// function --> fun1

void fun1(int x)
{
    cout << " --> fun1(int) # 1 --> x = " << x << endl;
}

// function --> fun1

void fun1(double x)
{
    cout << " --> fun1(double) # 2 --> x = " << x << endl;
}

// function --> fun1

template<typename T>
void fun1(T x)
{
    cout << " --> fun1(T) # 3 --> x = " << x << endl;
}

// the main function

int main()
{
    // declare a pointer to a function which takes int and returns void

    void (*pfun1)(int);

    // declare a pointer to a function which takes double and returns void

    void (*pfun2)(double);

    // delcare a opinter to a funcion which takes double and returns void

    void (*pfun3)(double);

    // accessing fun1(int) through pfun1

    pfun1 = &fun1;

    // accessing fun1(double) through pfun2

    pfun2 = &fun1;

    // accessing fun1(double) through pfun3

    pfun3 = &fun1<double>;

    cout << " --> pfun1(10) = ";
    pfun1(10);

    cout << " --> pfun2(10.11) = ";
    pfun2(10.11);

    cout << " --> pfun3(20.12345) = ";
    pfun3(20.12345);

    return 0;
}
