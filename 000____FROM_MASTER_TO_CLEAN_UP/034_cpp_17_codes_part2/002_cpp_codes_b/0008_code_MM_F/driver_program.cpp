//=========//
// pointer //
//=========//

#include <iostream>

using std::cout;
using std::cin;
using std::endl;

// the main function

int main ()
{
    // pointer with constant value
    // the only way to change the value of the pointer is
    // by changing the address it points to

    const int * p1;
    const int * p2 = new int (10);
    p1 = p2;

    cout << " --> p1 = " << p1 << endl;
    cout << " --> p2 = " << p2 << endl;

    cout << " --> *p1 = " << *p1 << endl;
    cout << " --> *p2 = " << *p2 << endl;

    // pointer with constant value and reference
    // you can not change either the value of the pointer
    // or the address it points to

    const int * const p3 = new int (11);

    cout << " -->  p3 = " <<  p3 << endl;
    cout << " --> *p3 = " << *p3 << endl;

    // pointer with constant reference but not value
    // you can not change the address it points to
    // but you can chane the value of it

    int * const p4 = new int (22);

    cout << " -->  p4 = " <<  p4 << endl;
    cout << " --> *p4 = " << *p4 << endl;

    int * p5 = new int (33) ;

    *p4 = *p5;

    cout << " -->  p4 = " <<  p4 << endl;
    cout << " --> *p4 = " << *p4 << endl;

    cout << " -->  p5 = " <<  p5 << endl;
    cout << " --> *p5 = " << *p5 << endl;

    return 0;
}

// END


