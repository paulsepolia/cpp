
#include <iostream>
#include <cmath>

using std::endl;
using std::cout;
using std::cin;
using std::pow;

// the main program

int main()
{
    const unsigned long int DIM = pow(10.0, 9.0); // stack
    const int TRIALS = 100000; // stack

    cout << " --> declare my array" << endl; // to console

    int * myArray; // stack

    for (int ik = 0; ik != TRIALS; ik++) {
        cout << " ------------------------------------------------------->> " << ik << endl;
        cout << " --> declare" << endl;

        myArray = new int [DIM]; // heap

        cout << " --> build my array" << endl;

        for (unsigned long int i = 0; i != DIM; i++) {
            myArray[i] = i+ik;
        }

        cout << " --> delete my array" << endl;

        delete [] myArray;
    }

    int sentinel;
    cin >> sentinel;

    return 0;
}

// end
