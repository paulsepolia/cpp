//===================//
// function pointers //
//===================//

#include <iostream>
#include <typeinfo>
#include <memory>

using std::cout;
using std::endl;
using std::move;

// function --> 1

template <typename T>
T fun1(const T & x1, const T & x2)
{
    return (x1+x2);
}

// function --> 2

template <typename T>
auto fun2(const T & x1, const T & x2) -> decltype(x1+x2)
{
    T x3 {x1+x2};
    return move(x3);
}

// function pointer --> 3

int (*pfun3)(const int & x1, const int & x2);
double (*pfun4)(const double & x1, const double & x2);

// the main function

int main()
{
    // # 1

    int x1 {10};
    int x2 {20};

    cout << " fun1<int>(x1,x2) = " << fun1<int>(x1,x2) << endl;
    cout << " fun1<double>(x1,x2) = " << fun1<double>(x1,x2) << endl;

    cout << " fun2<int>(x1,x2) = " << fun2<int>(x1,x2) << endl;
    cout << " fun2<double>(x1,x2) = " << fun2<double>(x1,x2) << endl;

    // #2

    int x3 {11};
    double x4 {22};

    cout << " fun1<int>(x3,x4) = " << fun1<int>(x3,x4) << endl;
    cout << " fun1<double>(x3,x4) = " << fun1<double>(x3,x4) << endl;

    cout << " fun2<int>(x3,x4) = " << fun2<int>(x3,x4) << endl;
    cout << " fun2<double>(x3,x4) = " << fun2<double>(x3,x4) << endl;

    // #3

    void * v1(nullptr);

    cout << "   v1 = " <<  v1 << endl;
    v1 = &x3;
    cout << " (*static_cast<int*>(v1)) = " << (*static_cast<int*>(v1)) << endl;
    cout << " (*reinterpret_cast<int*>(v1)) = " << (*reinterpret_cast<int*>(v1)) << endl;

    // # 4

    pfun3 = fun1<int>;

    cout << " pfun3(x3,x4) = " << pfun3(x3,x4) << endl;

    pfun4 = fun1<double>;

    cout << " pfun4(x3,x4) = " << pfun4(x3,x4) << endl;

    // # 5

    cout << " sizeof(int)           = " << sizeof(int) << endl;
    cout << " sizeof(long int)      = " << sizeof(long int) << endl;
    cout << " sizeof(long long int) = " << sizeof(long long int) << endl;
    cout << " sizeof(int*)          = " << sizeof(int*) << endl;
    cout << " sizeof(const int*)    = " << sizeof(const int*) << endl;
    cout << " sizeof(double*)       = " << sizeof(double*) << endl;
    cout << " sizeof(char*)         = " << sizeof(char*) << endl;
    cout << " sizeof(long int*)     = " << sizeof(long int*) << endl;
    cout << " sizeof(long double*)  = " << sizeof(long double*) << endl;
    cout << " sizeof(void*)         = " << sizeof(void*) << endl;
    //cout << " sizeof(fun1<int>)     = " << sizeof(fun1<int>) << endl; // ERROR
    cout << " sizeof(pfun3)         = " << sizeof(pfun3) << endl;
    cout << " sizeof(pfun4)         = " << sizeof(pfun4) << endl;

    return 0;
}

// end
