//================//
// move symantics //
//================//

#include <iostream>
#include <utility>
#include <string>

using std::cout;
using std::endl;
using std::move;
using std::string;

// # 1

int x;

// # 2

int getInt()
{
    return x;
}

// # 3

int && getRvalueInt()
{
    // notice that it's fine to move a primitive type,
    // remember std::move is just a cast
    return std::move(x);
}

// Clearly in the first case, despite the fact that getInt() is an rvalue,
// there is a copy of the variable x being made.
// We can even see this by writing a little helper function:

void printAddress(const int& v) // const ref to allow binding to rvalues
{
    cout << (&v) << endl;
}

// the main function

int main()
{
    printAddress(getInt());
    printAddress(x);

    printAddress(getRvalueInt());
    printAddress(x);

    return 0;
}

// end
