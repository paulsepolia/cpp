
#include <iostream>
#include <vector>
#include <numeric>
#include <string>
#include <functional>

using std::cout;
using std::endl;
using std::vector;
using std::string;
using std::to_string;
using std::multiplies;

// the main function

int main()
{
    vector<int> v {1,   2,  3,  4,  5,  6,  7,  8,  9, 10,
                   11, 12, 13, 14, 15, 16, 17, 18, 19, 20
                  };

    // sum

    int sum = accumulate(v.begin(), v.end(), 0);

    // product

    double product = accumulate(v.begin(), v.end(), 1.0, multiplies<double>());

    // string minus

    string s1 = accumulate(v.begin()+1, v.end(), to_string(v[0]),
    [](const string & a, int b) {
        return (a + "-" + to_string(b));
    });

    // string add

    string s2 = accumulate(v.begin()+1, v.end(), to_string(v[0]),
    [](const string & a, int b) {
        return (a + "+" + to_string(b));
    });

    // string times

    string s3 = accumulate(v.begin()+1, v.end(), to_string(v[0]),
    [](const string & a, int b) {
        return (a + "*" + to_string(b));
    });

    cout << " --> sum = " << sum << endl;
    cout << " --> product = " << product << endl;
    cout << " --> dash-separated string  = " << s1 << endl;
    cout << " --> plus-separated string  = " << s2 << endl;
    cout << " --> times-separated string = " << s3 << endl;

    return 0;
}

// end
