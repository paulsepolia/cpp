//=============//
// memory leak //
//=============//

// using intel's -xHost
// ALTHOUGH THERE IS A MEMORY LEAK IN HEAP SPACE
// IT IS NOT DETECTABLE BY VALGRIND

#include <iostream>
#include <cmath>

using std::cout;
using std::cin;
using std::endl;
using std::pow;

// type definition

typedef unsigned long int uli;

// global parameters

const uli DIM = static_cast<uli>(pow(10.0, 2.0));
const uli DIM_B = 4*static_cast<uli>(pow(10.0, 6.0));

// the main function

int main ()
{
    for (uli i = 0; i != DIM; i++) {
        int * p = new int[DIM_B];
        for (uli j = 0; j != DIM_B; j++) {
            p[j] = j;
        }

        if (i == 0) {
            cout << p[1] << endl;
        }

//		delete [] p;
    }

    int * p = new int;
    *p = 11;
    cout << "*p = " << *p << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

// END
