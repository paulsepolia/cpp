
//===========================//
// get address of a function //
//===========================//

#include <cstdio>
#include <iostream>

using std::cout;
using std::endl;

// function --> foo

double foo (double x)
{
    return x*x;
}

// the main function

int main ()
{

    double (*fun1)(double) = &foo;
    double (*fun2)(double) =  foo;

    cout << " --> fun1(10) = " << fun1(10) << endl;
    cout << " --> fun2(10) = " << fun2(10) << endl;

    cout << " --> (void*)(foo)  = " << (void*)(foo)  << endl;
    cout << " --> (void*)(&foo) = " << (void*)(&foo) << endl;
    cout << " --> (void*)(fun1) = " << (void*)(fun1) << endl;
    cout << " --> (void*)(fun2) = " << (void*)(fun2) << endl;

    int a[10];

    cout << " -->  a = " <<  a << endl;
    cout << " --> &a = " << &a << endl;

    return 0;
}

// end
