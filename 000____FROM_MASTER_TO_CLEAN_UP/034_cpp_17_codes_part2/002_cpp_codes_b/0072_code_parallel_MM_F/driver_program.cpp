//==================================//
// parallel lexicographical compare //
//==================================//

#include <iostream>
#include <vector>
#include <cmath>
#include <iomanip>
#include <chrono>

#include <parallel/algorithm>
#include <parallel/settings.h>

using std::cout;
using std::endl;
using std::vector;
using std::pow;
using std::fixed;
using std::setprecision;
using namespace std::chrono;
using std::boolalpha;

// type definition

typedef unsigned long int uli;

// comparison function

template <typename T>
bool comp_fun(const T & x, const T & y)
{
    return (x < y);
}

// comparison functor

template <typename T>
struct comp_functor {
    bool operator()(const T & x, const T & y)
    {
        return (x < y);
    }
};

// the main function

int main()
{
    cout << fixed;
    cout << setprecision(5);
    cout << boolalpha;

    //===================//
    // parallel settings //
    //===================//

    //==========================================================//
    // FORCE ALL POSSIBLE ALGORITHMS TO BE EXECUTED IN PARALLEL //
    //==========================================================//

    __gnu_parallel::_Settings s;
    s.algorithm_strategy = __gnu_parallel::force_parallel;
    __gnu_parallel::_Settings::set(s);

    //==========================//
    // end of parallel settings //
    //==========================//

    // local parameters

    const uli DIM1 = 2 * uli(pow(10.0, 9.0));
    const uli TRIALS1 = 1 * uli(pow(10.0, 1.0));

    // local variables

    bool res_bool;
    vector<char> v1(DIM1, 'a');
    vector<char> v2(DIM1, 'a');
    auto t1 = system_clock::now();
    auto t2 = system_clock::now();
    duration<double> time_span;

    // lexicographical_compare # 1

    cout << " --> lexicographical_compare # 1 --> default" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS1; i++) {
        res_bool = lexicographical_compare(v1.begin(), v1.end(), v2.begin(), v2.end());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> res_bool = " << res_bool << endl;

    // lexicographical_compare # 2

    cout << " --> lexicographical_compare # 2 --> lamda function" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS1; i++) {
        res_bool = lexicographical_compare(v1.begin(), v1.end(), v2.begin(), v2.end(),
        [](const char & x, const char & y) {
            return (x < y);
        });
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> res_bool = " << res_bool << endl;

    // lexicographical_compare # 3

    cout << " --> lexicographical_compare # 3 --> function" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS1; i++) {
        res_bool = lexicographical_compare(v1.begin(), v1.end(),
                                           v2.begin(), v2.end(),
                                           &comp_fun<char>);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> res_bool = " << res_bool << endl;

    // lexicographical_compare # 4

    cout << " --> lexicographical_compare # 4 --> functor" << endl;

    t1 = system_clock::now();

    for (uli i = 0; i != TRIALS1; i++) {
        res_bool = lexicographical_compare(v1.begin(), v1.end(),
                                           v2.begin(), v2.end(),
                                           comp_functor<char>());
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;
    cout << " --> res_bool = " << res_bool << endl;

    return 0;
}

// end
