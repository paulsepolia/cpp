//==================//
// INHERITANCE GAME //
//==================//

#include "classesNonPolymorphic.h"
#include <iostream>

using std::endl;
using std::cout;

//===================//
// the main function //
//===================//

int main()
{
    {
        // A

        cout << " --> A a1;" << endl;
        A a1;
        cout << " --> A a2(a1);" << endl;
        A a2(a1);

        // B

        cout << " --> B b1;" << endl;
        B b1;
        cout << " --> B b2(b1);" << endl;
        B b2(b1);

        // C

        cout << " --> C c1;" << endl;
        C c1;
        cout << " --> C c2(c1);" << endl;
        C c2(c1);

        // D

        cout << " --> D d1;" << endl;
        D d1;
        cout << " --> D d2(d1);" << endl;
        D d2(d1);

        cout << " --> exit --> 1" << endl;
    }

    cout << " --> MIDDLE ----------------------------------------------->> 1" << endl;

    {
        cout << " --> A a1;" << endl;
        A a1;
        cout << " --> B b1;" << endl;
        B b1;
        cout << " --> C c1;" << endl;
        C c1;
        cout << " --> D d1;" << endl;
        D d1;
        cout << " --> A a2(b1);" << endl;
        A a2(b1);
        cout << " --> B b2(c1);" << endl;
        B b2(c1);
        cout << " --> C c2(d1);" << endl;
        C c2(d1);
        cout << " --> D d2(d1);" << endl;
        D d2(d1);

        cout << " --> exit --> 2" << endl;
    }

    cout << " --> MIDDLE ----------------------------------------------->> 2" << endl;

    {
        cout << " --> A a1;" << endl;
        A a1;
        cout << " --> B b1;" << endl;
        B b1;
        cout << " --> C c1;" << endl;
        C c1;
        cout << " --> D d1;" << endl;
        D d1;
        cout << " --> A a2(d1);" << endl;
        A a2(d1);
        cout << " --> B b2(d1);" << endl;
        B b2(d1);
        cout << " --> C c2(d1);" << endl;
        C c2(d1);
        cout << " --> D d2(d1);" << endl;
        D d2(d1);

        cout << " --> exit --> 3" << endl;
    }

    cout << " --> MIDDLE ----------------------------------------------->> 3" << endl;

    return 0;
}

// end
