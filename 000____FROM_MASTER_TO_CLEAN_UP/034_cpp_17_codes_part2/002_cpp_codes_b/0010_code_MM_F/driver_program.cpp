//=============//
// copy memory //
//=============//

#include <iostream>
#include <cstring>
#include <ctime>
#include <iomanip>

using std::cout;
using std::cin;
using std::endl;
using std::memcpy;
using std::clock;
using std::fixed;
using std::setprecision;

// the main function

int main ()
{
    const int K_MAX = 100;
    time_t t1;
    time_t t2;

    cout << fixed;
    cout << setprecision(10);

    for (int ik = 0; ik != K_MAX; ik++) {

        cout << "------------------------------------------->> " << ik << endl;

        const int DIM = 600000000;
        int * p1 = new int [DIM];

        cout << "----->> 1 --> build pointer p1" << endl;

        t1 = clock();

        for (int i = 0; i != DIM; i++) {
            p1[i] = i;
        }

        t2 = clock();
        cout << "--> time used  = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

        cout << "--> sizeof(p1) = " << sizeof(p1) << endl;

        int * p2 = new int [DIM];

        cout << "--> sizeof(p2) = " << sizeof(p2) << endl;

        cout << "----->> 2 --> memcpy p1 to p2" << endl;

        t1 = clock();

        memcpy(p2, p1, DIM*sizeof(int));

        t2 = clock();
        cout << "--> time used  = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

        cout << "--> p1[10]     = " << p1[10] << endl;
        cout << "--> p2[10]     = " << p2[10] << endl;

        cout << "--> p1[DIM-1]  = " << p1[DIM-1] << endl;
        cout << "--> p2[DIM-1]  = " << p2[DIM-1] << endl;

        cout << "----->> 3 --> memcmp p1 vs p2" << endl;

        t1 = clock();

        int n = memcmp(p2, p1, DIM*sizeof(int));

        t2 = clock();
        cout << "--> time used  = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

        cout << "--> comparison = " << n << endl;

        cout << "----->> 4 --> delete heap" << endl;

        t1 = clock();

        delete [] p1;
        delete [] p2;

        p1 = nullptr;
        p2 = nullptr;

        t2 = clock();
        cout << "--> time used  = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

    }

    return 0;
}

// END
