//===================//
// drop constantness //
//===================//

#include <iostream>
#include <typeinfo>

using std::cout;
using std::endl;

// the main function

int main()
{
    //===========//
    // const int //
    //===========//

    cout << "------------------------------------->>  1" << endl;

    const int x1 = -1;
    const int x2 (-2);
    const int x3 {-3};

    cout << "  x1 = " << x1 << endl;
    cout << "  x2 = " << x2 << endl;
    cout << "  x3 = " << x3 << endl;

    //=============//
    // int const * //
    //=============//

    cout << "------------------------------------->>  2" << endl;

    int const * px1(nullptr);
    int const * px2(nullptr);
    int const * px3(nullptr);

    cout << " px1 = " << px1 << endl;
    cout << " px2 = " << px2 << endl;
    cout << " px3 = " << px3 << endl;

    px1 = &x1;
    px2 = &x2;
    px3 = &x3;

    cout << " px1 = " << px1 << endl;
    cout << " px2 = " << px2 << endl;
    cout << " px3 = " << px3 << endl;

    //===================//
    // int const * const //
    //===================//

    cout << "------------------------------------->>  3" << endl;

    int const * const px4(nullptr); // useless pointer to NULL
    int const * const px5(nullptr); // useless pointer to NULL
    int const * const px6(nullptr); // useless pointer to NULL

    cout << " px4 = " << px4 << endl;
    cout << " px5 = " << px5 << endl;
    cout << " px6 = " << px6 << endl;

    //=============//
    // int * const //
    //=============//

    cout << "------------------------------------->>  4" << endl;

    int x7 {-10};
    int x8 {-11};
    int x9 {-12};

    int * const px7(&x7);
    int * const px8(&x8);
    int * const px9(&x9);

    cout << " *px7 = " << *px7 << endl;
    cout << " *px8 = " << *px8 << endl;
    cout << " *px9 = " << *px9 << endl;

    *px7 = 100;
    *px8 = 200;
    *px9 = 300;

    cout << " x7 = " << x7 << endl;
    cout << " x8 = " << x8 << endl;
    cout << " x9 = " << x9 << endl;

    //====================//
    // volatile const int //
    //====================//

    volatile const int x10 {
        111
    };
    int * px10 {nullptr};
    px10 = const_cast<int*>(&x10);

    // before

    cout << " x10 = " << x10 << endl;

    *px10 = 222;

    // after

    cout << " x10 = " << x10 << endl;

    return 0;
}

// end
