//===================//
// function pointers //
//===================//

#include <iostream>
#include <sstream>

using std::cout;
using std::cin;
using std::endl;
using std::stringstream;
using std::hex;

typedef unsigned long long int ulli;

// function --> funA # 1

void funA(void * x)
{
    ulli y;
    stringstream stream_loc;
    stream_loc << x;
    stream_loc >> hex >> y;

    cout << " --> funA # 1 --> x = " << y << endl;
}

// the main function

int main()
{
    //=======================//
    // function --> funA # 1 //
    //=======================//

    // declare a pointer to a function which takes an integer
    // and returns a void

    void (*foo)(void *); // foo is a function pointer

    foo = funA;

    ulli x1 = 14;
    ulli x2 = 15;
    ulli x3 = 12345678901234567890ULL;

    foo(reinterpret_cast<void*>(x1));
    foo(reinterpret_cast<void*>(x2));
    foo(reinterpret_cast<void*>(x3));

    funA(reinterpret_cast<void*>(x1));
    funA(reinterpret_cast<void*>(x2));
    funA(reinterpret_cast<void*>(x3));

    return 0;
}

// end
