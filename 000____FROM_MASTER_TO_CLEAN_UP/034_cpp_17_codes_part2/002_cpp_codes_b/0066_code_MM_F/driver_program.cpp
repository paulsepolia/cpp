//===============//
// parallel copy //
//===============//

#include <iostream>
#include <vector>
#include <iterator>
#include <numeric>
#include <cmath>

#include <parallel/algorithm>
#include <parallel/settings.h>

using std::cout;
using std::endl;
using std::ostream_iterator;
using std::vector;
using std::pow;

// type definition

typedef unsigned long int uli;

// the main function

int main()
{
    __gnu_parallel::_Settings s;
    s.algorithm_strategy = __gnu_parallel::force_parallel;
    __gnu_parallel::_Settings::set(s);

    const uli DIM1 = uli(pow(10.0, 8.0));
    const uli TRIALS1 = uli(pow(10.0, 2.0));

    vector<double> v1(DIM1);

    cout << " --> build v1" << endl;

    iota(v1.begin(), v1.end(), 0.0);

    vector<double> v2(DIM1);

    cout << " --> copy v1 in a parallel way to v2" << endl;

    for (uli i = 0; i != TRIALS1; i++) {
        copy(v1.begin(), v1.end(), v2.begin()); // copy is serial // no parallel version
    }

    cout << " --> some results :" << endl;

    cout << " --> v1[0] = " << v1[0] << endl;
    cout << " --> v1[1] = " << v1[1] << endl;
    cout << " --> v1[2] = " << v1[2] << endl;
    cout << " --> v1[3] = " << v1[3] << endl;

    cout << " --> v2[0] = " << v2[0] << endl;
    cout << " --> v2[1] = " << v2[1] << endl;
    cout << " --> v2[2] = " << v2[2] << endl;
    cout << " --> v2[3] = " << v2[3] << endl;

    return 0;
}

// end
