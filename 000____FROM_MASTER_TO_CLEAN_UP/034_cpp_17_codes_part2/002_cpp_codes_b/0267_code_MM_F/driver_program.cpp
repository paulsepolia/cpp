//=============//
// linked list //
//=============//

#include <cmath>
#include <iostream>
using std::endl;
using std::cout;

// the linkList definition

class list {
public:
    int data;
    list * next;
};

// the main function

int main()
{
    const int DIM(std::pow(10.0,7.0));

    //=============//
    // example # 1 //
    //=============//

    // # 1
    // create the head of the linked list

    list * head(new list);
    head->data = 10;
    head->next = 0;

    // # 2
    // free up RAM

    delete head;

    //=============//
    // example # 2 //
    //=============//

    // # 1
    // create the head of the linked list

    head = new list;
    list * node(new list);
    head->next = node;

    for(int i = 0; i != DIM; i++) {
        node->data = i;
        node->next = new list;
        node = node->next;
    }

    // # 2
    // mark the end of the linked list

    node->next = 0;

    // # 3
    // get the head of the node

    list * q;
    node = head;
    while(node->next) {
        q = node->next;
        node->next = q->next;
        delete q;
    }

    delete head;
}

// END
