
//===========================//
// usage of reinterpret_cast //
//===========================//

#include <cassert>
#include <iostream>
#include <typeinfo>

using std::cout;
using std::endl;
using std::hex;
using std::dec;

// function

int f()
{
    return 42;
}

// the main function

int main()
{
    int i1 = 7;
    double d1 = 8.0;

    cout << " --> typeid(i1).name()      = " << typeid(i1).name() << endl;
    cout << " --> typeid(int).name()     = " << typeid(int).name() << endl;
    cout << " --> typeid(*int).name()    = " << typeid(int*).name() << endl;
    cout << " --> typeid(*double).name() = " << typeid(double*).name() << endl;
    cout << " --> typeid(double).name()  = " << typeid(double).name() << endl;
    cout << " --> typeid(d1).name()      = " << typeid(d1).name() << endl;

    // pointer to integer and back

    uintptr_t v1 = reinterpret_cast<uintptr_t>(&i1); // static_cast is an error

    cout << "The value of &i1 is 0x" << hex << v1 << endl;

    int * p1 = reinterpret_cast<int*>(v1);

    assert(p1 == &i1);

    // pointer to function to another and back

    void(*fp1)() = reinterpret_cast<void(*)()>(f);

    // fp1(); undefined behavior

    int(*fp2)() = reinterpret_cast<int(*)()>(fp1);

    cout << " dec << fp2() = " << dec << fp2() << endl; // safe

    // type aliasing through pointer

    char * p2 = reinterpret_cast<char*>(&i1);

    if(p2[0] == '\x7') {
        cout << "This system is little-endian" << endl;
    } else {
        cout << "This system is big-endian" << endl;
    }

    // type aliasing through reference

    reinterpret_cast<unsigned int&>(i1) = 42;

    cout << i1 << endl;

    return 0;
}

// end
