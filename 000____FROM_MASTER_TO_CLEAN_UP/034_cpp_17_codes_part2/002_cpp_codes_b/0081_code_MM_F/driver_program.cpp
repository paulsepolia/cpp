//====================//
// exception handling //
//====================//

#include <stdexcept>
#include <iostream>
#include <bitset>
#include <typeinfo>

using std::endl;
using std::cerr;
using std::cout;
using std::bitset;
using std::exception;

// the main function

int main()
{
    try {
        bitset<33> bitsetA;
        bitsetA[32] = 1;
        bitsetA[0] = 1;
        unsigned long int x = bitsetA.to_ulong();
        cout << " x = " << x << endl;
    } catch (exception &e) {
        cerr << "Caught " << e.what() << endl;
        cerr << "Type " << typeid(e).name() << endl;
    };
}

// end
