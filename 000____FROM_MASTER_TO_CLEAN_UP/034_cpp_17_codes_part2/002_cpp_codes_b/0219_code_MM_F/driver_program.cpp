//=====================//
// upcast and downcast //
//=====================//

#include <iostream>
#include <typeinfo>

using std::cout;
using std::endl;

// class --> A

class A1 {
public:

    A1(): p1(new int(-1)) {}

    int * p1;

    virtual ~A1()
    {
        cout << " ~A1() --> 1" << endl;
        if(p1) {
            cout << " ~A1() --> 2" << endl;
            cout << " *p1 = " << *p1 << endl;
            delete p1;
            p1 = 0;
        }
    }
};

// class --> A2

class A2: public A1 {
public:

    A2(): A1(), p2(new int(-2)) {}

    int * p2;

    virtual ~A2()
    {
        cout << " ~A2() --> 1" << endl;
        if(p2) {
            cout << " ~A2() --> 2" << endl;
            cout << " *p1 = " << *p1 << endl;
            cout << " *p2 = " << *p2 << endl;
            delete p2;
            p2 = 0;
        }
    }
};

// class --> A3

class A3: public A2 {
public:

    A3(): A2(), p3(new int(-3)) {}

    int * p3;

    virtual ~A3()
    {
        cout << " ~A3() --> 1" << endl;
        if(p3) {
            cout << " ~A3() --> 3" << endl;
            cout << " *p1 = " << *p1 << endl;
            cout << " *p2 = " << *p2 << endl;
            cout << " *p3 = " << *p3 << endl;
            delete p3;
            p3 = 0;
        }
    }
};

// the main function

int main()
{
    //============//
    // 1 # scopes //
    //============//

    cout << " --> scope # 1" << endl;

    {
        A1 a1;
    }

    cout << " --> scope # 2" << endl;

    {
        A2 a2;
    }

    cout << " --> scope # 3" << endl;

    {
        A3 a3;
    }

    cout << " --> scope # 4" << endl;

    {

        A1 a1;
        A2 a2;
        A3 a3;

        *a1.p1 = 11;

        *a2.p1 = 21;
        *a2.p2 = 22;

        *a3.p1 = 31;
        *a3.p2 = 32;
        *a3.p3 = 33;

        delete a3.p1;
        delete a3.p2;
        delete a3.p3;

        a3.p1 = 0;
        a3.p2 = 0;
        a3.p3 = 0;

    }

    //============//
    // 2 # upcast //
    //============//

    cout << " --> scope # 5" << endl;

    {
        A1 * pa1(nullptr);
        A2 * pa2(nullptr);
        A3 * pa3(nullptr);

        pa1 = new A1;
        pa2 = new A2;
        pa3 = new A3;

        cout << "  pa1->p1 = " << pa1->p1 << endl;
        cout << "  pa2->p2 = " << pa2->p2 << endl;
        cout << "  pa3->p3 = " << pa3->p3 << endl;

        cout << " *pa1->p1 = " << *pa1->p1 << endl;
        cout << " *pa2->p2 = " << *pa2->p2 << endl;
        cout << " *pa3->p3 = " << *pa3->p3 << endl;

        cout << " --> delete pa1;" << endl;
        delete pa1;
        pa1 = 0;

        cout << " --> delete pa2;" << endl;
        delete pa2;
        pa2 = 0;

        cout << " --> delete pa3;" << endl;
        delete pa3;
        pa3 = 0;
    }

    cout << " --> scope # 6" << endl;

    {
        A1 * pa1(nullptr);
        A2 * pa2(nullptr);
        A3 * pa3(nullptr);

        pa1 = new A1;
        pa2 = new A2;
        pa3 = new A3;

        cout << "  pa1->p1 = " << pa1->p1 << endl;
        cout << "  pa2->p2 = " << pa2->p2 << endl;
        cout << "  pa3->p3 = " << pa3->p3 << endl;

        cout << " *pa1->p1 = " << *pa1->p1 << endl;
        cout << " *pa2->p2 = " << *pa2->p2 << endl;
        cout << " *pa3->p3 = " << *pa3->p3 << endl;

        cout << " *pa3->p1 = " << *pa3->p1 << endl;
        cout << " *pa3->p2 = " << *pa3->p2 << endl;
        cout << " *pa3->p3 = " << *pa3->p3 << endl;

        *pa3->p1 = 51;
        *pa3->p2 = 52;
        *pa3->p3 = 53;

        cout << " *pa1->p1 = " << *pa1->p1 << endl;
        cout << " *pa2->p2 = " << *pa2->p2 << endl;
        cout << " *pa3->p3 = " << *pa3->p3 << endl;

        cout << " *pa3->p1 = " << *pa3->p1 << endl;
        cout << " *pa3->p2 = " << *pa3->p2 << endl;
        cout << " *pa3->p3 = " << *pa3->p3 << endl;

        cout << " --> delete pa1;" << endl;
        delete pa1;
        pa1 = 0;

        cout << " --> delete pa2;" << endl;
        delete pa2;
        pa2 = 0;

        cout << " --> delete pa3;" << endl;
        delete pa3;
        pa3 = 0;
    }

    cout << " --> scope # 7" << endl;

    {
        A1 * pa1(nullptr);
        A2 * pa2(nullptr);
        A3 * pa3(nullptr);

        pa1 = new A1;
        pa2 = new A2;
        pa3 = new A3;

        *pa1->p1 = 61;
        *pa2->p1 = 62;
        *pa3->p1 = 63;

        cout << " *pa1->p1 = " << *pa1->p1 << endl;
        cout << " *pa2->p1 = " << *pa2->p1 << endl;
        cout << " *pa3->p1 = " << *pa3->p1 << endl;

        // delete here

        delete pa1;
        pa1 = 0;
        delete pa2;
        pa2 = 0;

        // assign new addresses

        pa1 = pa3;
        pa2 = pa3;

        cout << " *pa1->p1 = " << *pa1->p1 << endl;
        cout << " *pa2->p1 = " << *pa2->p1 << endl;
        cout << " *pa3->p1 = " << *pa3->p1 << endl;

        delete pa3;
        pa3 = 0;
    }

    cout << " --> scope # 8" << endl;

    {
        A1 * pa1(nullptr);
        A2 * pa2(nullptr);
        A3 * pa3(nullptr);

        pa1 = new A1;
        pa2 = new A2;
        pa3 = new A3;

        delete pa1;
        pa1 = 0;

        delete pa2;
        pa2 = 0;

        pa1 = dynamic_cast<A1*>(pa3);
        pa2 = dynamic_cast<A2*>(pa3);
        pa3 = dynamic_cast<A3*>(pa3);

        delete pa3;
        pa3 = 0;
    }

    cout << " --> scope # 9" << endl;

    {
        A1 * pa1(nullptr);
        A2 * pa2(nullptr);
        A3 * pa3(nullptr);

        pa1 = new A3;
        pa2 = new A3;
        pa3 = new A3;

        delete pa1;
        pa1 = 0;

        delete pa2;
        pa2 = 0;

        pa1 = dynamic_cast<A1*>(pa3);
        pa2 = dynamic_cast<A2*>(pa3);
        pa3 = dynamic_cast<A3*>(pa3);

        delete pa3;
        pa3 = 0;
    }

    return 0;
}

// end
