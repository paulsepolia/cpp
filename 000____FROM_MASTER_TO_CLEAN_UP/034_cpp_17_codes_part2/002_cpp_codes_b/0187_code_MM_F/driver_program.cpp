//==============//
// late binding //
//==============//

#include <iostream>

using std::endl;
using std::cout;

// class --> B1

class B1 {
public:
    void fun1()
    {
        cout << " Base --> fun1" << endl;
    };
};

// class --> D1

class D1: public B1 {
public:
    virtual void fun1()
    {
        cout << " D1 --> fun1" << endl;
    };
};

// class --> D2

class D2: public D1 {
public:
    virtual void fun1()
    {
        cout << " D2 --> fun1" << endl;
    };
};

// the main function

int main()
{
    // local variables

    B1 b1;
    D1 d1;
    D2 d2;
    B1 * pb1(nullptr);
    D1 * pd1(nullptr);
    D2 * pd2(nullptr);

    //====================================//
    // check way of calling the functions //
    //====================================//

    cout << "--------------------------------------->>  1" << endl;

    b1.fun1();
    d1.fun1();
    d2.fun1();

    //======================================//
    // using pointers to call the functions //
    //======================================//

    // # 1

    cout << "--------------------------------------->>  2" << endl;

    pb1 = &b1;
    pb1->fun1();

    pd1 = &d1;
    pd1->fun1();

    pd2 = &d2;
    pd2->fun1();

    // # 2

    cout << "--------------------------------------->>  3" << endl;

    // UPCASTING TO BASE TYPE POINTERS

    pb1 = &d1;
    pb1->fun1();

    pb1 = &d2;
    pb1->fun1();

    pd1 = &d2;
    pd1->fun1();

    // # 3

    cout << "--------------------------------------->>  4" << endl;

    // DOWNCASTING

    //pd1 = &b1; //ERROR::COMPILE TIME::DOWNCASTING
    //pd2 = &b1; //ERROR::COMPILE TIME::DOWNCASTING
    //pd2 = &d1; //ERROR::COMPILE TIME::DOWNCASTING

    //pd1 = dynamic_cast<D1*>(&b1); //ERROR::COMPILE TIME::DOWNCASTING::SOURCE IS NOT POLYMORPHIC
    pd2 = dynamic_cast<D2*>(&d1); //OKAY::DOWNCASTING::CAN NEVER SUCCEED
    pd2 = static_cast<D2*>(&d1); //OKAY::DOWNCASTING::RUN TIME ERROR

    // UPCASTING

    pb1 = dynamic_cast<B1*>(&d1); //OKAY::UPCASTING
    if(pb1) {
        cout << "&d1 is treated as a B1 pointer" << endl;
    }

    pb1 = dynamic_cast<B1*>(&d2); //OKAY::UPCASTING
    if(pb1) {
        cout << "&d2 is treated as a B1 pointer" << endl;
    }

    pd1 = dynamic_cast<D1*>(&d2); //OKAY::UPCASTING
    if(pb1) {
        cout << "&d2 is treated as a D1 pointer" << endl;
    }

    return 0;
}

// end
