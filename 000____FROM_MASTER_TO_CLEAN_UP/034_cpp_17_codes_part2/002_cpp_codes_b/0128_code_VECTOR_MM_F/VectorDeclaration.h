//==========================//
// Vector class declaration //
//==========================//

#ifndef VECTOR_DECLARATION_H
#define VECTOR_DECLARATION_H

// declaration

template <class T>
class Vector {
public:

    // constructor # 1

    Vector();

    // constructor # 2

    Vector(const unsigned int &);

    // copy constructor

    Vector(const Vector<T> &);

    // move constructor

    Vector(Vector<T> &&);

    // destructor

    virtual ~Vector();

    // public member functions

    inline bool Empty() const;            // returns whether the vector is empty or not
    inline unsigned int Size() const;     // get the size of the vector
    inline void PushBack(const T &);      // adds item at the end of the vector
    inline unsigned int Capacity() const; // get the actual capacity of the vector
    inline void ShrinkToFit();            // makes Capacity and Size of same value
    inline void Clear();                  // removes all elements
    inline void Insert(const unsigned int &, const T &); // insert at position index
    inline void Erase(const unsigned int &); // erase element at position index
    inline void Resize(const unsigned int &); // resize the vector

    template <class ... Args>
    inline void Emplace(Args && ... args);  // create and push at the end

    inline T GetElement(const unsigned int &) const; // get element at position index
    inline void SetElement(const unsigned int &, const T &); // set element at position index
    inline void Swap(Vector<T> &) noexcept;  // swap contents

    // copy assignment operator

    inline void operator = (const Vector<T> &);

    // move assignment operator

    inline void operator = (Vector<T> &&);

    // [] operator

    inline const T & operator [] (const unsigned int &) const;

private:

    // private member functions

private:

    unsigned int m_size; // the actual size of the array
    T * m_data; // the actual data array
};

#endif // VECTOR_DECLARATION_H
