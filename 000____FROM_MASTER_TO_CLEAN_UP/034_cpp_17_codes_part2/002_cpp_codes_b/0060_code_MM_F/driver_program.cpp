//======//
// list //
//======//

#include <iostream>
#include <list>
#include <cmath>
#include <random>
#include <iomanip>

using std::cout;
using std::endl;
using std::list;
using std::pow;
using std::random_device;
using std::mt19937;
using std::uniform_real_distribution;
using std::fixed;
using std::setprecision;

// type definitions

typedef unsigned long int uli;

// a predicate implemented as a class

struct is_less_than_ten {
    const double TEN = 10.0;
    bool operator() (const double & value)
    {
        return (value <= TEN);
    }
};

// class: List

template <typename T>
class List : public list<T> {
};

// the main function

int main()
{
    // local parameters

    const uli DIM1 = 3 * uli(pow(10.0, 6.0));
    const uli TRIALS1 = 10;

    // local parameters

    List<double> L1;

    // fill with random numbers

    cout << fixed;
    cout << setprecision(5);

    for (uli ik = 0; ik != TRIALS1; ik++) {

        cout << "----------------------------------------------------------->> " << ik << endl;

        cout << " --> declare random device" << endl;

        random_device rd;
        mt19937 gen(rd());
        uniform_real_distribution<> dis(0, 11);

        cout << " --> build list" << endl;

        for (uli i = 0; i < DIM1; i++) {
            L1.push_back(dis(gen));
        }

        cout << " --> L1.size() = " << L1.size() << endl;

        // remove_if list

        cout << " --> remove_if list" << endl;

        L1.remove_if(is_less_than_ten());

        cout << " --> L1.size() = " << L1.size() << endl;

        cout << " --> clear list" << endl;

        L1.clear();

        cout << " --> reset the internal state of the random generator" << endl;

        dis.reset();
    }

    return 0;
}

// end
