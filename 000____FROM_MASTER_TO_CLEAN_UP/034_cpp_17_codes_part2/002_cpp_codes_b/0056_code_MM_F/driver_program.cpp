//======//
// list //
//======//

#include <iostream>
#include <list>

using std::cout;
using std::endl;
using std::list;

// compare only integral part:

bool mycomparison(const double & first, const double & second)
{
    return (int(first) < int(second));
}

// the main function

int main ()
{

    list<double> first, second;

    first.push_back(3.1);
    first.push_back(2.2);
    first.push_back(2.9);

    second.push_back(3.7);
    second.push_back(7.1);
    second.push_back(1.4);

    first.sort();
    second.sort();

    first.merge(second);

    cout << "first contains:";

    for (auto it = first.begin(); it != first.end(); ++it) {
        cout << ' ' << *it;
    }

    cout << endl;

    // second is now empty

    second.push_back(2.1);

    first.merge(second, mycomparison);

    cout << "first contains:";

    for (auto it = first.begin(); it != first.end(); ++it) {
        cout << ' ' << *it;
    }

    cout << endl;

    return 0;
}

// end
