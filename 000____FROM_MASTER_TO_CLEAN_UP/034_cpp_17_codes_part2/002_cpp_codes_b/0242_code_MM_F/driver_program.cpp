//===========//
// templates //
//===========//

#include <iostream>

using std::cout;
using std::endl;

// swap with pointers

void swap(int * p1, int * p2)
{
    cout << " --> swap with pointers" << endl;
    int v1(*p1);
    *p1 = *p2;
    *p2 = v1;
}

// swap with references

void swap(int & p1, int & p2)
{
    cout << " --> swap with references" << endl;
    int v1(p1);
    p1 = p2;
    p2 = v1;
}

// swap template

template<typename T>
void swap(T & p1, T & p2)
{
    cout << " --> swap template references" << endl;
    T v1(p1);
    p1 = p2;
    p2 = v1;
}

template<typename T>
void swap(T * p1, T * p2)
{
    cout << " --> swap template pointers" << endl;
    T v1(*p1);
    *p1 = *p2;
    *p2 = v1;
}

// the main function

int main()
{
    // # 1

    int x1 = 10;
    int x2 = 11;

    swap(&x1, &x2);

    cout << " x1 = " << x1 << endl;
    cout << " x2 = " << x2 << endl;

    // # 2

    int * p1 = new int(20);
    int * p2 = new int(30);

    swap(p1, p2);

    cout << "*p1 = " << *p1 << endl;
    cout << "*p2 = " << *p2 << endl;

    delete p1;
    delete p2;

    // # 3

    int x3 = 22;
    int x4 = 33;

    swap(x3, x4);

    cout << " x3 = " << x3 << endl;
    cout << " x4 = " << x4 << endl;

    // # 4

    double x5(10);
    double x6(20);

    double & x7(x5);
    double & x8(x6);

    swap(x7, x8);

    cout << " x5 = " << x5 << endl;
    cout << " x6 = " << x6 << endl;
    cout << " x7 = " << x7 << endl;
    cout << " x8 = " << x8 << endl;

    swap(&x5, &x6);

    cout << " x5 = " << x5 << endl;
    cout << " x6 = " << x6 << endl;
    cout << " x7 = " << x7 << endl;
    cout << " x8 = " << x8 << endl;

    return 0;
}

// end
