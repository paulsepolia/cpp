//=====================//
// POLYMORPHIC CLASSES //
//=====================//

#ifndef CLASSES_POLYMORPHIC_H
#define CLASSES_POLYMORPHIC_H

#include <iostream>

using std::endl;
using std::cout;

//=========//
// class A //
//=========//

class A {
public:

    // # 1 --> constructor

    A()
    {
        cout << " --> constructor --> A" << endl;
    }

    // # 2 --> fun

    virtual void fun()
    {
        cout << " --> fun --> A" << endl;
    }

    // # 3 --> destructor

    virtual ~A()
    {
        cout << " --> destructor --> ~A" << endl;
    }
};

//=========//
// class B //
//=========//

class B : public A {
public:

    // # 1 --> constructor

    B()
    {
        cout << " --> constructor --> B" << endl;
    }

    // # 2 --> fun

    virtual void fun()
    {
        cout << " --> fun --> B" << endl;
    }

    // # 3 --> destructor

    virtual ~B()
    {
        cout << " --> destructor --> ~B" << endl;
    }
};

//=========//
// class C //
//=========//

class C : public B {
public:

    // # 1 --> constructor

    C()
    {
        cout << " --> constructor --> C" << endl;
    }

    // # 2 --> fun

    virtual void fun()
    {
        cout << " --> fun --> C" << endl;
    }

    // # 3 --> destructor

    virtual ~C()
    {
        cout << " --> destructor --> ~C" << endl;
    }
};

//=========//
// class D //
//=========//

class D : public C {
public:

    // # 1 --> constructor

    D()
    {
        cout << " --> constructor --> D" << endl;
    }

    // # 2 --> fun

    virtual void fun()
    {
        cout << " --> fun --> D" << endl;
    }

    // # 3 --> destructor

    virtual ~D()
    {
        cout << " --> destructor --> ~D" << endl;
    }
};

#endif // CLASSES_POLYMORPHIC_H

// end
