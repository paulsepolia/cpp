//=================//
// diamond problem //
//=================//

#include <iostream>

using std::endl;
using std::cout;

// class --> B

class B {
public:
};

// class --> D1

class D1: public B {
public:
};

// class --> D2

class D2: public B {
public:
};

// class --> E1

class E1: virtual public D1, public D2 {
public:
};

// the main function

int main()
{
    B b1;
    D1 d1;
    D2 d2;
    E1 e1;

    // I GOT BACK WARNINGS ONLY FOR THE UNUSED VARIABLES:
    // THE UNUSED VARIABLES ARE: b1, d1, d2
    // IF I DO NOT DECLARE VIRTUAL THE INHERITANCE OF D1 and/or D2
    // THEN I DO GET BACK WARNINGS FOR THE b1, d1, d2, e1
    // WHICH MEANS THAT the objects b1, d1, d2, e1 ARE CONSTRUCTED IN COMPILATION TIME!

    return 0;
}

// end
