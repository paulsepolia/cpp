//===============//
// parallel copy //
//===============//

#include <iostream>
#include <vector>
#include <iterator>
#include <numeric>
#include <cmath>

#include <parallel/algorithm>
#include <parallel/settings.h>

using std::cout;
using std::endl;
using std::ostream_iterator;
using std::vector;
using std::pow;

// type definition

typedef unsigned long int uli;

// the main function

int main()
{
    // parallel settings

    __gnu_parallel::_Settings s;
    s.algorithm_strategy = __gnu_parallel::force_parallel;
    __gnu_parallel::_Settings::set(s);

    const int threads_wanted = 4;
    omp_set_dynamic(false);
    omp_set_num_threads(threads_wanted);

    // end of parallel settings

    const uli DIM1 = uli(pow(10.0, 8.0));
    const uli TRIALS1 = uli(pow(10.0, 1.0));

    vector<double> v1(DIM1);

    cout << " --> build v1" << endl;

    iota(v1.begin(), v1.end(), 0.0);

    cout << " --> sort v1 in a parallel" << endl;

    for (uli i = 0; i != TRIALS1; i++) {
        std::__parallel::sort(v1.begin(), v1.end());
    }

    cout << " --> some results :" << endl;

    cout << " --> v1[0] = " << v1[0] << endl;
    cout << " --> v1[1] = " << v1[1] << endl;
    cout << " --> v1[2] = " << v1[2] << endl;
    cout << " --> v1[3] = " << v1[3] << endl;

    return 0;
}

// end
