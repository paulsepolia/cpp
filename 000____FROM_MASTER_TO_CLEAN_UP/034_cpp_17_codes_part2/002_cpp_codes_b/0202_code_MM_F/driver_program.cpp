//=================//
// diamond problem //
//=================//

#include <iostream>

using std::endl;
using std::cout;

// the main function

int main()
{
    const int x1(10);

    cout << "   x1 = " << x1 << endl;

    const int * px1;

    px1 = &x1;

    cout << " *px1 = " << *px1 << endl;

    int * px2;

    px2 = const_cast<int*>(px1);

    *px2 = 100;

    cout << " *px1 = " << *px1 << endl;
    cout << " *px2 = " << *px2 << endl;

    return 0;
}

// end
