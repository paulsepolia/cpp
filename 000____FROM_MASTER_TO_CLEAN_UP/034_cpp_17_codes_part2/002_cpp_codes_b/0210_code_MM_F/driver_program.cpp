//===================//
// drop constantness //
//===================//

#include <iostream>
#include <typeinfo>

using std::cout;
using std::endl;

// struct --> A

struct A {
    double x1;
    double x2;
};

// class --> B

class B {
public:
    double x1;
    double x2;
};

// the main function

int main()
{
    cout << "------------------------------------->>  1" << endl;

    A a1;
    cout << " a1.x1 = " << a1.x1 << endl;
    cout << " a1.x2 = " << a1.x2 << endl;

    A a2 {-1};
    cout << " a2.x1 = " << a2.x1 << endl;
    cout << " a2.x2 = " << a2.x2 << endl;

    A a3 {-2, -3};
    cout << " a3.x1 = " << a3.x1 << endl;
    cout << " a3.x2 = " << a3.x2 << endl;

    cout << "------------------------------------->>  2" << endl;

    cout << " typeid(A).name()  = " << typeid(A).name() << endl;
    cout << " typeid(a1).name() = " << typeid(a1).name() << endl;
    cout << " typeid(a2).name() = " << typeid(a2).name() << endl;
    cout << " typeid(a3).name() = " << typeid(a3).name() << endl;

    B b1;
    cout << " b1.x1 = " << b1.x1 << endl;
    cout << " b1.x2 = " << b1.x2 << endl;

    B b2 {-1};
    cout << " b2.x1 = " << b2.x1 << endl;
    cout << " b2.x2 = " << b2.x2 << endl;

    B b3 {-2, -3};
    cout << " b3.x1 = " << b3.x1 << endl;
    cout << " b3.x2 = " << b3.x2 << endl;

    cout << "------------------------------------->>  3" << endl;

    cout << " typeid(B).name()  = " << typeid(B).name() << endl;
    cout << " typeid(b1).name() = " << typeid(b1).name() << endl;
    cout << " typeid(b2).name() = " << typeid(b2).name() << endl;
    cout << " typeid(b3).name() = " << typeid(b3).name() << endl;

    cout << " typeid(int).name()     = " << typeid(int).name() << endl;
    cout << " typeid(int*).name()    = " << typeid(int *).name() << endl;
    cout << " typeid(int&).name()    = " << typeid(int &).name() << endl;
    cout << " typeid(int**).name()   = " << typeid(int **).name() << endl;
    cout << " typeid(int***).name()  = " << typeid(int ****).name() << endl;
    cout << " typeid(int****).name() = " << typeid(int ****).name() << endl;

    cout << " typeid(const int *).name() = " << typeid(const int *).name() << endl;
    cout << " typeid(int const *).name() = " << typeid(int const *).name() << endl;
    cout << " typeid(int const * const).name() = " << typeid(int const * const).name() << endl;
    cout << " typeid(const int * const).name() = " << typeid(const int * const).name() << endl;

    cout << " typeid(const int**).name()   = " << typeid(const int **).name() << endl;
    cout << " typeid(const int***).name()  = " << typeid(const int ***).name() << endl;
    cout << " typeid(const int****).name() = " << typeid(const int ****).name() << endl;


    cout << " typeid(int const* const*).name()   = " << typeid(int const* const*).name() << endl;
    cout << " typeid(int const* const* const*).name()   = "
         << typeid(int const* const* const*).name() << endl;

    return 0;
}

// end
