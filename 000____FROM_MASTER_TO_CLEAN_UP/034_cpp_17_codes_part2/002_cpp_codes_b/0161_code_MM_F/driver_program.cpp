//===================//
// garbage collector //
//===================//

#include <iostream>
#include <memory>
#include <set>
#include <cmath>

using std::cout;
using std::endl;
using std::shared_ptr;
using std::pow;
using std::set;

//==================//
// type definitions //
//==================//

typedef unsigned long int uli;
typedef const uli culi;

//==================//
// comparison class //
//==================//

template<typename T>
class is_less_shared_ptr {
public:

    bool operator()(const shared_ptr<T> & val_a, const shared_ptr<T> & val_b)
    {
        return (val_a.get() < val_b.get());
    }
};

//=========================//
// class declaration --> A //
//=========================//

class A {
public:

    A();
    virtual ~A();
    static set<shared_ptr<A>, is_less_shared_ptr<A>> _garbageA1;
    static set<shared_ptr<A>, is_less_shared_ptr<A>> _garbageA2;
};

//========================//
// class definition --> A //
//========================//

// static member initialization

set<shared_ptr<A>, is_less_shared_ptr<A>> A::_garbageA1 {};
set<shared_ptr<A>, is_less_shared_ptr<A>> A::_garbageA2 {};

// constructor

A::A()
{
    cout << " --> constructor --> A" << endl;
}

// destructor

A::~A()
{
    cout << " --> destructor --> ~A" << endl;
}

//=================//
// local functions //
//=================//

void fun1()
{
    // local parameters

    culi SIZE(10*static_cast<uli>(pow(10.0, 0.0)));
    culi TRIALS(2*static_cast<uli>(pow(10.0, 0.0)));

    // local variables

    set<shared_ptr<A>, is_less_shared_ptr<A>>::iterator it;

    // for-loop

    for(uli j = 0; j != TRIALS; j++) {
        cout << "-------------------------------------------------------->> " << j << endl;

        for(uli i = 0; i != SIZE; i++) {

            cout << " A * pb = new A;" << endl;

            A * pb = new A;

            // garbage collection

            shared_ptr<A> spa;
            spa.reset(pb);

            cout << " A::_garbageA1.insert(spa);" << endl;

            A::_garbageA1.insert(spa);

            cout << " --> A::_garbageA1.size() = " << A::_garbageA1.size() << endl;
            cout << " --> A::_garbageA2.size() = " << A::_garbageA2.size() << endl;
        }

        cout << " --> backup set A::_garbageA1" << endl;
        cout << " A::_garbageA2 = A::_garbageA1;" << endl;

        A::_garbageA2 = A::_garbageA1;

        cout << " --> A::_garbageA1.size() = " << A::_garbageA1.size() << endl;
        cout << " --> A::_garbageA2.size() = " << A::_garbageA2.size() << endl;

        cout << " --> clear set A::_garbageA1" << endl;

        A::_garbageA1.clear();

        cout << " --> A::_garbageA1.size() = " << A::_garbageA1.size() << endl;
        cout << " --> A::_garbageA2.size() = " << A::_garbageA2.size() << endl;
    }

    cout << endl;
    cout << " --> After the end of the loop" << endl;
    cout << endl;
}

//==================//
// the main program //
//==================//

int main()
{
    fun1();

    return 0;
}

// end
