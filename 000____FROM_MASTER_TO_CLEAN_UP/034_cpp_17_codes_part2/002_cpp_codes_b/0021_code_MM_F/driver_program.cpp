
//==================================================//
// WITHOUT MOVE CONSTRUCTOR AND ASSIGNMENT OPERATOR //
// IT IS SLOW AND MORE MEMORY CONSUMING             //
//==================================================//

#include "parameters.h"
#include "memory_block.h"

#include <iostream>
#include <vector>
#include <cmath>
#include <ctime>
#include <iomanip>

using std::cout;
using std::endl;
using std::vector;
using std::pow;
using std::clock;
using std::fixed;
using std::setprecision;

// the main function

int main()
{
    const uli DIM = 5 *static_cast<unsigned long int>(pow(10.0, 4.0));
    const uli TRIALS = static_cast<unsigned long int>(pow(10.0, 1.0));

    time_t t1;
    time_t t2;

    cout << fixed;
    cout << setprecision(10);

    for (uli ik = 0; ik != TRIALS; ik++) {

        cout << "------------------------------------------------------------>> " << ik << endl;

        // create a vector object and add a few elements to it

        cout << " --> A1 --> declare vector<MemoryBlock> v" << endl;

        vector<MemoryBlock> v;

        cout << " --> A2 --> end of declare vector<MemoryBlock> v" << endl;

        cout << " --> A3 --> v.push_back(MemoryBlock(i)) --> start" << endl;

        t1 = clock();

        for (uli i = 0; i != DIM; i++) {
            v.push_back(MemoryBlock(20000));
        }

        t2 = clock();
        cout << " --> Time to push back = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

        cout << " --> A4 --> v.push_back(MemoryBlock(i)) --> end" << endl;

        // insert a new element into the second position of the vector

        cout << " --> A5 --> v.insert(v.begin() + 1, MemoryBlock(50)) --> start" << endl;

        v.insert(v.begin() + 1, MemoryBlock(50));

        cout << " --> A6 --> v.insert(v.begin() + 1, MemoryBlock(50)) --> end" << endl;

        // clear elements of vector

        cout << " --> A7 --> v.clear() --> start" << endl;

        t1 = clock();

        v.clear();

        t2 = clock();
        cout << " --> Time to clear = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

        cout << " --> A8 --> v.clear() --> end" << endl;

        // shrink to fit

        t1 = clock();

        cout << " --> A9 --> v.shrink_to_fit() --> start" << endl;

        v.shrink_to_fit();

        t2 = clock();
        cout << " --> Time to shrink to fit = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

        cout << " --> A10 --> v.shrink_to_fit() --> end" << endl;

    }


    return 0;
}

// end
