//=================//
// diamond problem //
//=================//

#include <iostream>

using std::endl;
using std::cout;

// class --> B

class B {
public:

    // default constructor

    B()
    {
        cout << " --> B()" << endl;
    }

    // default destructor

    ~B()
    {
        cout << " --> ~B()" << endl;
    }
};

// class --> D1

class D1: public B {
public:

    // default constructor

    D1(): B()
    {
        cout << " --> D1()" << endl;
    }

    // default destructor

    ~D1()
    {
        cout << " --> ~D1()" << endl;
    }
};

// class --> D2

class D2: public B {
public:

    // default constructor

    D2(): B()
    {
        cout << " --> D2()" << endl;
    }

    // default destructor

    ~D2()
    {
        cout << " --> ~D2()" << endl;
    }
};

// class --> E1

class E1: public D1, public D2 {
public:

    // default constructor

    E1(): D1(), D2()
    {
        cout << " --> E1()" << endl;
    }

    // default destructor

    ~E1()
    {
        cout << " --> ~E1()" << endl;
    }
};

// the main function

int main()
{
    // WITHOUT VIRTUAL FUNCTIONS
    // WITHOUT VIRTUAL INHERITANCE
    // WITHOUT NON_DEFUALT CONSTRUCTORS
    // THE SITUATION IS SIMPLE
    // as the following

    cout << "------------------------------------------------>>  1" << endl;

    B b1;

    cout << "------------------------------------------------>>  2" << endl;

    D1 d1;

    cout << "------------------------------------------------>>  3" << endl;

    D2 d2;

    cout << "------------------------------------------------>>  4" << endl;

    E1 e1;

    cout << "------------------------------------------------>>  END" << endl;

    return 0;
}

// end
