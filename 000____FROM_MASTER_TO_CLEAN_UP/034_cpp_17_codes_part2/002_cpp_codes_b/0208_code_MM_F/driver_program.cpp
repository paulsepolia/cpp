//===================//
// drop constantness //
//===================//

#include <iostream>
#include <iomanip>
#include <typeinfo>

using std::endl;
using std::cout;
using std::boolalpha;

// function --> 1

int * fun1(const int & x)
{
    cout << " fun1 --> x = " << x << endl;
    return const_cast<int*>(&x);
}

// function --> 2

int * fun2(volatile const int & x)
{
    cout << " fun2 --> x = " << x << endl;
    return const_cast<int*>(&x);
}


// the main function

int main()
{
    cout << boolalpha;

    //=============//
    // SCOPE --> 1 //
    //=============//

    {
        cout << " --> SCOPE -----------------------------> 1" << endl;
        int x1 = 10;
        int * px1(nullptr);
        px1 = fun1(x1);
        *px1 = 20;
        cout << " x1 = " << x1 << endl;
        cout << " (px1 == &x1) = " << (px1 == &x1) << endl;
    }

    //=============//
    // SCOPE --> 2 //
    //=============//

    {
        cout << " --> SCOPE -----------------------------> 2" << endl;
        const int x1 = 10;
        int * px1(nullptr);
        px1 = fun1(x1);
        *px1 = 20;
        cout << " x1 = " << x1 << endl; // UNDEFINED BEHAVIOUR
        cout << " (px1 == &x1) = " << (px1 == &x1) << endl;
    }

    //=============//
    // SCOPE --> 3 //
    //=============//

    {
        cout << " --> SCOPE -----------------------------> 3" << endl;
        volatile const int x1 = 10;
        int * px1(nullptr);
        px1 = fun2(x1);
        *px1 = 20;
        cout << " x1 = " << x1 << endl; // EXPECTED BEHAVIOUR BECAUSE OF volatile keyword
        cout << " (px1 == &x1) = " << (px1 == &x1) << endl;
    }

    //=============//
    // SCOPE --> 4 //
    //=============//

    {
        cout << " --> SCOPE -----------------------------> 4" << endl;
        volatile const int x1 = 10;
        int * px1(nullptr);
        px1 = const_cast<int*>(&x1);
        *px1 = 20;
        cout << " x1 = " << x1 << endl; // EXPECTED BEHAVIOUR BECAUSE OF volatile keyword
        cout << " (px1 == &x1) = " << (px1 == &x1) << endl;
    }

    return 0;
}

// end
