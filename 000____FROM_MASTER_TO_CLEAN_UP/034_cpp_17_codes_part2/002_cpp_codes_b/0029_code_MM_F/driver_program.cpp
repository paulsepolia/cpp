//================//
// move symantics //
//================//

#include <iostream>
#include <utility>
#include <vector>
#include <cmath>

using std::cout;
using std::endl;
using std::move;
using std::vector;
using std::pow;

// type definitions

typedef unsigned long int uli;

// the main function

int main()
{
    // local parameters

    const uli DIM = 3 * static_cast<uli>(pow(10.0, 8.0));
    const uli TRIALS1 = static_cast<uli>(pow(10.0, 8.0));
    const uli TRIALS2 = static_cast<uli>(pow(10.0, 1.0));

    // local variables

    vector<double> v1;
    vector<double> v2;
    uli x = 0;

    // build the vector

    cout << " --> build the vector" << endl;

    for (uli i = 0; i != DIM; i++) {
        v1.push_back(i+0.1234);
    }

    // move v1 to v2

    cout << " --> move the vectors --> fast process" << endl;

    for (uli i = 0; i != TRIALS1; i++) {
        v2 = move(v1);
        v1 = move(v2);
        x = x+1;
    }

    cout << " -->  &v1   = " << &v1    << endl;
    cout << " -->  v1[0] = " <<  v1[0] << endl;
    cout << " --> &v1[0] = " << &v1[0] << endl;
    cout << " -->  &v2   = " << &v2    << endl;
    cout << " --> &v2[0] = " << &v2[0] << endl;
    cout << " --> x = " << x << endl;

    // copy v1 to v2

    cout << " --> copy the vectors --> slow process" << endl;

    for (uli i = 0; i != TRIALS2; i++) {
        cout << "----------------------------------->> copy --> " << i << endl;
        v2 = v1;
        v1 = v2;
    }

    cout << " --> &v1    = " << &v1    << endl;
    cout << " -->  v1[0] = " <<  v1[0] << endl;
    cout << " --> &v1[0] = " << &v1[0] << endl;
    cout << " --> &v2    = " << &v2    << endl;
    cout << " -->  v2[0] = " <<  v2[0] << endl;
    cout << " --> &v2[0] = " << &v2[0] << endl;

    return 0;
}

// END
