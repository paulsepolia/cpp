
#include "memory_block.h"

#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;

// the main function

int main()
{
    // create a vector object and add a few elements to it

    cout << " --> A1 --> declare vector<MemoryBlock> v" << endl;

    vector<MemoryBlock> v;

    cout << " --> A2 --> end of declare vector<MemoryBlock> v" << endl;

    cout << " --> A3 --> v.push_back(MemoryBlock(25)) --> start" << endl;

    v.push_back(MemoryBlock(25));

    cout << " --> A4 --> v.push_back(MemoryBlock(25)) --> end" << endl;

    cout << " --> A5 --> v.push_back(MemoryBlock(75)) --> start" << endl;

    v.push_back(MemoryBlock(75));

    cout << " --> A6 --> v.push_back(MemoryBlock(75)) --> end" << endl;

    // insert a new element into the second position of the vector

    cout << " --> A7 --> v.insert(v.begin() + 1, MemoryBlock(50)) --> start" << endl;

    v.insert(v.begin() + 1, MemoryBlock(50));

    cout << " --> A8 --> v.insert(v.begin() + 1, MemoryBlock(50)) --> end" << endl;

    return 0;
}

// end
