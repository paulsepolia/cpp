//==============//
// char pointer //
//==============//

#include <cstring>
#include <iostream>
#include <cmath>
#include <string>

using std::cout;
using std::endl;
using std::pow;
using std::string;
using std::to_string;

// type definitions

typedef unsigned long int uli;
typedef const uli culi;

// the main function

int main()
{
    // local parameters

    culi I_MAX1(static_cast<uli>(pow(10.0, 5.0)));
    culi I_MAX3(static_cast<uli>(2+pow(10.0, 1.0)));
    culi I_MAX2(static_cast<uli>(1));

    // local variables

    string str1("");

    // build the string

    cout << " --> 1 --> building the string" << endl;

    // step --> 1

    for(uli i = 0; i != I_MAX1; i++) {
        str1 = str1 + to_string(i);
    }

    // step --> 2

    for(uli i = 0; i != I_MAX3; i++) {
        cout << " --> step " << (i+1) << endl;

        for(uli i = 0; i != I_MAX2; i++) {
            str1 = str1 + str1;
        }
    }

    // get the size of string in GBytes

    cout << " --> 2 --> str1.size() = "
         << (str1.size() * sizeof(string::value_type)) / pow(1024.0, 3.0) << endl;

    // char pointer

    cout << " --> 3 --> assign the string to the const char *" << endl;

    const char * var1 = str1.c_str();

    cout << "---------------------------------------> 1" << endl;

    cout << "&var1         = " << &var1 << endl;
    //cout << " var1         = " <<  var1 << endl; // whole string is outputted here
    cout << "*var1         = " << *var1 << endl;
    cout << "*(var1+1)     = " << *(var1+1) << endl;
    cout << "*(var1+2)     = " << *(var1+2) << endl;
    cout << "var1[0]       = " << var1[0] << endl;
    cout << "var1[1]       = " << var1[1] << endl;
    cout << "var1[2]       = " << var1[2] << endl;
    cout << "strlen(var1)  = " << strlen(var1) << endl;
    cout << "str1.length() = " << str1.length() << endl;

    cout << "---------------------------------------> END" << endl;

    return 0;
}

// end
