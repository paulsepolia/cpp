//=========================================//
// late binding 					   //
// I DO NOT THINK IT IS LATE BINDING SINCE //
// THE COMPILER DETECTS IF THERE IS NO     //
// TYPES TO BE ABLE TO ASSIGN              //
//=========================================//

#include <iostream>
#include <memory>

using std::endl;
using std::cout;
using std::dynamic_pointer_cast;

class Base {
public:
    virtual void function1()
    {
        cout << " Base --> fun1" << endl;
    };

    virtual void function2()
    {
        cout << " Base --> fun2" << endl;
    };
};

class D1: public Base {
public:
    virtual void function1()
    {
        cout << " D1 --> fun1" << endl;
    };
};

class D2: public Base {
public:
    virtual void function2()
    {
        cout << " D2 --> fun2" << endl;
    };
};

// the main function

int main()
{
    Base a1;
    D1 d1;
    D2 d2;

    // base class

    cout << " ----------------------------------> 1" << endl;

    a1.function1();
    a1.function2();

    // derived D1 class

    cout << " ----------------------------------> 2" << endl;

    d1.function1();
    d1.function2();

    // derived D2 class

    cout << " ----------------------------------> 3" << endl;

    d2.function1();
    d2.function2();

    // base class pointer

    Base * pa1(0);

    // # 1

    cout << " ----------------------------------> 4" << endl;

    pa1 = &a1; // pa1 is of type Base pointer
    // pa1 is initialized using address of a Base object
    // so we call the Base functions

    pa1->function1();
    pa1->function2();

    // # 2

    cout << " ----------------------------------> 5" << endl;

    pa1 = &d1; // pa1 is a Base type pointer
    // it is initialized using address of a Derived D1 object
    // so we call the virtual table of D1 class

    pa1->function1();
    pa1->function2();

    // # 3

    cout << " ----------------------------------> 6" << endl;

    pa1 = &d2;

    pa1->function1();
    pa1->function2();

    // derived D1 class pointer

    D1 * pd1(0);

    // # 1

    cout << " ----------------------------------> 7" << endl;

    //pd1 = &a1; // ERROR

    pd1 = dynamic_cast<D1*>(&a1); // THIS IS SEGMENTATION FAULT
    // CAN NEVER SUCCEED

    //pd1->function1();
    //pd1->function2();

    // # 2

    cout << " ----------------------------------> 8" << endl;

    pd1 = &d1;

    pd1->function1();
    pd1->function2();

    // # 3

    cout << " ----------------------------------> 9" << endl;

    // pd1 = &d2; // ERROR

    //pd1->function1();
    //pd1->function2();

    return 0;
}

// end
