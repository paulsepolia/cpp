#!/bin/bash

  # 1. compile

  g++-6.1.0   -O0                \
			-g0               \
              -Wall              \
              -std=gnu++17       \
              driver_program.cpp \
              -o x_gnu_610
