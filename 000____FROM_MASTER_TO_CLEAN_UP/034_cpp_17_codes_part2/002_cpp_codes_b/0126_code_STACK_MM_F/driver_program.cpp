//===============================//
// driver program to Stack class //
//===============================//

#include "Stack.h"
#include <iostream>
#include <iomanip>

using std::cout;
using std::cin;
using std::endl;
using std::fixed;
using std::setprecision;

// the main function

int main()
{
    Stack<int> s1;
    Stack<double> s2;

    // # 0

    cout << fixed;
    cout << setprecision(5);

    // # 1

    cout << " --> push some integers" << endl;

    s1.Push(10);
    s1.Push(11);
    s1.Push(12);
    s1.Push(13);
    s1.Push(14);

    // # 2

    cout << " --> pop some elements" << endl;

    cout << " --> s1.Pop() = " << s1.Pop() << endl;
    cout << " --> s1.Pop() = " << s1.Pop() << endl;
    cout << " --> s1.Pop() = " << s1.Pop() << endl;
    cout << " --> s1.Pop() = " << s1.Pop() << endl;
    cout << " --> s1.Pop() = " << s1.Pop() << endl;

    // # 3

    cout << " --> Push some doubles" << endl;

    s2.Push(10);
    s2.Push(11);
    s2.Push(12);
    s2.Push(13);
    s2.Push(14);
    s2.Push(15);
    s2.Push(16);
    s2.Push(17);

    // # 4

    cout << " --> pop some doubles" << endl;

    cout << " --> s2.Pop() = " << s2.Pop() << endl;
    cout << " --> s2.Pop() = " << s2.Pop() << endl;
    cout << " --> s2.Pop() = " << s2.Pop() << endl;
    cout << " --> s2.Pop() = " << s2.Pop() << endl;
    cout << " --> s2.Pop() = " << s2.Pop() << endl;

    return 0;
}

// end
