//=====================//
// static_pointer_cast //
//=====================//

#include <iostream>
#include <memory>

using std::cout;
using std::cin;
using std::endl;
using std::shared_ptr;
using std::static_pointer_cast;
using std::make_shared;

// struct A

struct A {
    static const char * static_type;
    const char * dynamic_type;
    A()
    {
        dynamic_type = static_type;
    }
};

// struct B

struct B: A {
    static const char * static_type;
    B()
    {
        dynamic_type = static_type;
    }
};

// static initializations

const char* A::static_type = "class A";
const char* B::static_type = "class B";

// the main function

int main ()
{
    shared_ptr<A> foo;
    shared_ptr<B> bar;

    foo = make_shared<A>();

    // cast of potentially incomplete object, but ok as a static cast:

    bar = static_pointer_cast<B>(foo);

    cout << "foo's static  type: " << foo->static_type << endl;
    cout << "foo's dynamic type: " << foo->dynamic_type << endl;
    cout << "bar's static  type: " << bar->static_type << endl;
    cout << "bar's dynamic type: " << bar->dynamic_type << endl;

    return 0;
}

// END


