//========================//
// Stack class definition //
//========================//

#ifndef STACK_DEFINITION_H
#define STACK_DEFINITION_H

#include <utility>
using std::move;

// constructor

template <class T>
Stack<T>::Stack() {}

// copy constructor

template <class T>
Stack<T>::Stack(const Stack<T> & other)
{
    *this = other;
}

// move constructor

template <class T>
Stack<T>::Stack(Stack<T> && other)
{
    *this = move(other);
}

// destructor

template <class T>
Stack<T>::~Stack() {}

// Empty

template <class T>
inline bool Stack<T>::Empty() const
{
    return m_data.empty();
}

// Size

template <class T>
inline unsigned int Stack<T>::Size() const
{
    return m_data.size();
}

// Top

template <class T>
inline T Stack<T>::Top() const
{
    return m_data.top();
}

// Push

template <class T>
inline void Stack<T>::Push(const T & elem)
{
    m_data.push(elem);
}

// Pop

template <class T>
inline void Stack<T>::Pop()
{
    m_data.pop();
}

// Swap

template <class T>
inline void Stack<T>::Swap(Stack<T> & other) noexcept
{
    m_data.swap(other.m_data);
}

// Emplace

template <class T>
template <class ... Args>
inline void Stack<T>::Emplace(Args && ... args)
{
    m_data.emplace(args ...);
}

// copy assignment operator

template <class T>
inline void Stack<T>::operator = (const Stack<T> & other)
{
    m_data = other.m_data;
}

// move assignment operator

template <class T>
inline void Stack<T>::operator = (Stack<T> && other)
{
    m_data = move(other.m_data);
}

#endif // STACK_DEFINITION_H
