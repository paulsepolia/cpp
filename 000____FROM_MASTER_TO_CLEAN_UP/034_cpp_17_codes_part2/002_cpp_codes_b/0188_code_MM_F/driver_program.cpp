//==============//
// late binding //
//==============//

#include <iostream>

using std::endl;
using std::cout;

// class --> B1

class B1 {
public:
    void fun1()
    {
        cout << " Base --> fun1" << endl;
    };

    virtual void fun2()
    {
        cout << " Base --> fun2" << endl;
    };
};

// class --> D1

class D1: public B1 {
public:
    virtual void fun1()
    {
        cout << " D1 --> fun1" << endl;
    };

    virtual void fun2()
    {
        cout << " D1 --> fun2" << endl;
    };
};

// class --> D2

class D2: public D1 {
public:
    virtual void fun1()
    {
        cout << " D2 --> fun1" << endl;
    };

    virtual void fun2()
    {
        cout << " D2 --> fun2" << endl;
    };
};

// Generic function which makes use
// of automatic upcasting and polymorphism

void fun_generic(B1 * obj)
{
    obj->fun1();
    obj->fun2();
}

// Function to make use of downcasting

void fun_downcast(D2 * obj)
{
    cout << " obj = " << obj << endl;
    obj->fun1();
    obj->fun2();
}

// the main function

int main()
{
    // local variables

    B1 b1;
    D1 d1;
    D2 d2;
    B1 * pb1(nullptr);
    D1 * pd1(nullptr);
    D2 * pd2(nullptr);

    //============================//
    // UPCASTING AND POLYMORPHISM //
    //============================//

    // # 1
    // Automatic upcasting of &d1, &d2 to B1*
    // so they are treated as B1 pointers (B1*) and
    // (a) they call only the fun1 of the Base class B1 because IT IS NOT VIRTUAL IN BASE
    // (b) they call the fun2 of the base class if we pass a base reference,
    //     they call the fun2 of the derived D1 class if we pass a D1 reference,
    //     they call the fun2 of the derived D2 class if we pass a D2 reference,
    //     because fun2 is virtual in base class B1

    cout << " ------------------------------------->  1" << endl;
    cout << " fun_generic(&b1);" << endl;

    // nothing special here
    fun_generic(&b1);

    cout << " ------------------------------------->  2" << endl;
    cout << " fun_generic(&d1);" << endl;

    // (1) use of automatic upcasting: &d1 is upcasted to B1*
    // (2) use of polymorphism: call of fun2 of D1 class, since
    //	  the pointer address is that of D1 class

    fun_generic(&d1);

    cout << " ------------------------------------->  3" << endl;
    cout << " fun_generic(&d2);" << endl;

    // (1) use of automatic upcasting: &d2 is upcasted to B1*
    // (2) use of polymorphism: call of fun2 of D2 class, since
    //     pointer address is that of D2 class

    fun_generic(&d2);

    cout << " ------------------------------------->  4" << endl;
    cout << " pb1 = &b1;" << endl;
    cout << " fun_generic(pb1);" << endl;

    // nothing special here
    pb1 = &b1;
    fun_generic(pb1);

    cout << " ------------------------------------->  5" << endl;
    cout << " pd1 = &d1;" << endl;
    cout << " fun_generic(&d1);" << endl;

    // (1) use of automatic upcasting: pd1 is upcasted to B1*
    // (2) use of polymorphism: call of fun2 of D1 class, since
    //     pointer address is that of D1 class

    pd1 = &d1;
    fun_generic(pd1);

    cout << " ------------------------------------->  6" << endl;
    cout << " pd2 = &d2;" << endl;
    cout << " fun_generic(&d2);" << endl;

    // (1) use of automatic upcasting: pd2 is upcasted to B1*
    // (2) use of polymorphism: call of fun2 of D2 class, since
    //     pointer address is that of D2 class

    pd2 = &d2;
    fun_generic(pd2);

    //=============//
    // DOWNCASTING //
    //=============//

    cout << " ------------------------------------->  7" << endl;
    cout << " pb1 = &b1;" << endl;
    pb1 = &b1;

    cout << " fun_downcast(static_cast<D2*>(pb1));" << endl;
    //fun_downcast(static_cast<D2*>(pb1)); // SEGMENTATION FAULT

    cout << " fun_downcast(dynamic_cast<D2*>(pb1));" << endl;
    //fun_downcast(dynamic_cast<D2*>(pb1)); // SEGMENTATION FAULT

    cout << " ------------------------------------->  8" << endl;
    cout << " pb1 = &d1;" << endl;
    pb1 = &d1;

    cout << " &d1 = " << &d1 << endl;
    cout << " pb1 = " << pb1 << endl;

    // pb1 is of type B1*
    // points to an object of type D1
    // is static casted to D2*

    cout << " fun_downcast(static_cast<D2*>(pb1));" << endl;
    fun_downcast(static_cast<D2*>(pb1));

    cout << " fun_downcast(dynamic_cast<D2*>(pb1));" << endl;
    //fun_downcast(dynamic_cast<D2*>(pb1)); // SEGMENTATION FAULT

    cout << " ------------------------------------->  9" << endl;
    cout << " pb1 = &d2;" << endl;
    pb1 = &d2;

    cout << " &d2 = " << &d2 << endl;
    cout << " pb1 = " << pb1 << endl;

    // pb1 is of type B1*
    // points to an object of type D2
    // is static casted to D2*

    cout << " fun_downcast(static_cast<D2*>(pb1));" << endl;
    fun_downcast(static_cast<D2*>(pb1));

    cout << " fun_downcast(dynamic_cast<D2*>(pb1));" << endl;
    fun_downcast(dynamic_cast<D2*>(pb1));

    cout << " -------------------------------------> 10" << endl;
    cout << " pd1 = &d2;" << endl;
    pd1 = &d2;

    cout << " &d2 = " << &d2 << endl;
    cout << " pd1 = " << pd1 << endl;

    // pd1 is of type D1*
    // points to an object of type D2
    // is static casted to D2*

    cout << " fun_downcast(static_cast<D2*>(pd1));" << endl;
    fun_downcast(static_cast<D2*>(pd1));

    cout << " fun_downcast(dynamic_cast<D2*>(pd1));" << endl;
    fun_downcast(dynamic_cast<D2*>(pd1));

    pd1 = &d2;

    return 0;
}

// end
