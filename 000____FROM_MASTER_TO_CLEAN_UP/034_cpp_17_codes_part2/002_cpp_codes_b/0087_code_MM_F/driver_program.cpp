//===============//
// mutex example //
//===============//

#include <iostream>
#include <thread>
#include <mutex>

using std::cout;
using std::endl;
using std::thread;
using std::mutex;

std::mutex mtx;  // mutex for critical section

// thread safe function

void print_block(int n, char c)
{
    // critical section (exclusive access to std::cout signaled by locking mtx):

    mtx.lock();

    for (int i = 0; i < n; ++i) {
        cout << c;
    }

    cout << endl;

    mtx.unlock();
}

// the main function

int main()
{
    std::thread th1(print_block, 50, '*');
    std::thread th2(print_block, 50, '$');

    th1.join();
    th2.join();

    return 0;
}

// end
