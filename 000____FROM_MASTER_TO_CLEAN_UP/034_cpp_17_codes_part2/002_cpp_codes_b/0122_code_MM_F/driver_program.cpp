//=====================================//
// function object vs function pointer //
//=====================================//

#include <iostream>
#include <iomanip>
#include <vector>
#include <cmath>
#include <numeric>
#include <chrono>

using std::cout;
using std::cin;
using std::endl;
using std::fixed;
using std::setprecision;
using std::showpos;
using std::showpoint;
using std::vector;
using std::pow;
using std::accumulate;
using namespace std::chrono;

// accumA
// takes a pointer to a function

template <class inputIter, class T>
T accumA(inputIter first, inputIter last, T init, T (*bin_fun)(const T& x, const T& y))
{
    while (first != last) {
        init = bin_fun(init, *first);
        ++first;
    }

    return init;
}

// accumB
// takes a function object

template <class inputIter, class T, class binaryFunction>
T accumB(inputIter first, inputIter last, T init, binaryFunction bin_fun)
{
    while (first != last) {
        init = bin_fun(init, *first);
        ++first;
    }

    return init;
}

// function object

template <class T>
class addFunObj {
public:
    T operator() (const T & x, const T & y) const
    {
        return x+y;
    }
};

// function

template <class T>
inline T addFun(const T & x, const T & y)
{
    // that interface here must match the
    // interface of accumA
    // That is why we prefer function objects
    return x+y;
}

// the main function

int main()
{
    // local variables and parameters

    const long long DIM_MAX = static_cast<long long>(pow(10.0, 5.0));
    const long long K_MAX = static_cast<long long>(pow(10.0, 5.0));
    const long long CALL_MAX = static_cast<long long>(pow(10.0, 5.0));
    auto t1 = system_clock::now();
    auto t2 = system_clock::now();
    duration<double> time_span;
    double res;
    double init;
    addFunObj<double> addObj;

    // output format

    cout << fixed;
    cout << showpos;
    cout << showpoint;
    cout << setprecision(20);

    // for loop

    for (long long k = 0; k < K_MAX; k++) {
        // the counter

        cout << "-------------------------------------------------------->> " << k << endl;

        // local variables

        vector<double> *VA = new vector<double>;
        double * AA = new double [DIM_MAX];

        // build the vector

        cout << endl;
        cout << " --> build the vector" << endl;
        cout << endl;

        for (long long i = 0; i != DIM_MAX; i++) {
            VA->push_back(cos(static_cast<double>(i)));
        }

        // apply the handmade accumB

        cout << " --> apply the handmade accumB for function objects"
             << " --> addFunObj<double>()" << endl;

        t1 = system_clock::now();

        for (long long i = 0; i != CALL_MAX; i++) {
            res = 0;
            init = 0.0;
            res = accumB(VA->begin(), VA->end(), init, addFunObj<double>());
        }

        cout << " --> result = " << res << endl;
        t2 = system_clock::now();
        time_span = duration_cast<duration<double>>(t2-t1);
        cout << " --> time used = " << time_span.count() << endl;

        cout << endl;

        // apply the handmade accumB

        cout << " --> apply the handmade accumB for function objects"
             << " --> addObj" << endl;

        t1 = system_clock::now();

        for (long long i = 0; i != CALL_MAX; i++) {
            res = 0;
            init = 0.0;
            res = accumB(VA->begin(), VA->end(), init, addObj);
        }

        cout << " --> result = " << res << endl;
        t2 = system_clock::now();
        time_span = duration_cast<duration<double>>(t2-t1);
        cout << " --> time used = " << time_span.count() << endl;

        cout << endl;

        // apply the STL accumulate

        cout << " --> apply the STL accumulate for function objects"
             << " --> addObj" << endl;

        t1 = system_clock::now();

        for (long long i = 0; i != CALL_MAX; i++) {
            res = 0;
            init = 0.0;
            res = accumulate(VA->begin(), VA->end(), init, addObj);
        }

        cout << " --> result = " << res << endl;
        t2 = system_clock::now();
        time_span = duration_cast<duration<double>>(t2-t1);
        cout << " --> time used = " << time_span.count() << endl;

        cout << endl;

        // apply the STL accumulate

        cout << " --> apply the STL accumulate for function objects"
             << " --> addFunObj<double>()" << endl;

        t1 = system_clock::now();

        for (long long i = 0; i != CALL_MAX; i++) {
            res = 0;
            init = 0.0;
            res = accumulate(VA->begin(), VA->end(), init, addFunObj<double>());
        }

        cout << " --> result = " << res << endl;
        t2 = system_clock::now();
        time_span = duration_cast<duration<double>>(t2-t1);
        cout << " --> time used = " << time_span.count() << endl;

        cout << endl;

        // apply the handmade accumA

        cout << " --> apply the handmade accumA for function pointers"
             << " --> addFun" << endl;

        t1 = system_clock::now();

        for (long long i = 0; i != CALL_MAX; i++) {
            res = 0;
            init = 0.0;
            res = accumA(VA->begin(), VA->end(), init, &addFun);
        }

        cout << " --> result = " << res << endl;
        t2 = system_clock::now();
        time_span = duration_cast<duration<double>>(t2-t1);
        cout << " --> time used = " << time_span.count() << endl;

        // delete the vector

        delete VA;

        // build the array

        cout << endl;
        cout << " --> build the array" << endl;
        cout << endl;

        for (long long i = 0; i != DIM_MAX; i++) {
            AA[i]=cos(static_cast<double>(i));
        }

        // apply the handmade accumulate_pgg_b

        cout << " --> apply the handmade accumB for function objects"
             " --> addFunObj<double>()" << endl;

        t1 = system_clock::now();

        for (long long i = 0; i != CALL_MAX; i++) {
            res = 0;
            init = 0.0;
            res = accumB(&AA[0], &AA[DIM_MAX], init, addFunObj<double>());
        }

        cout << " --> result = " << res << endl;
        t2 = system_clock::now();
        time_span = duration_cast<duration<double>>(t2-t1);
        cout << " --> time used = " << time_span.count() << endl;

        cout << endl;

        // apply the handmade accumB

        cout << " --> apply the handmade accumB for function objects"
             << " --> addObj" << endl;

        t1 = system_clock::now();

        for (long long i = 0; i != CALL_MAX; i++) {
            res = 0;
            init = 0.0;
            res = accumB(&AA[0], &AA[DIM_MAX], init, addObj);
        }

        cout << " --> result = " << res << endl;
        t2 = system_clock::now();
        time_span = duration_cast<duration<double>>(t2-t1);
        cout << " --> time used = " << time_span.count() << endl;

        cout << endl;

        // apply the STL accumulate

        cout << " --> apply the STL accumulate for function objects"
             << " --> addObj" << endl;

        t1 = system_clock::now();

        for (long long i = 0; i != CALL_MAX; i++) {
            res = 0;
            init = 0.0;
            res = accumulate(&AA[0], &AA[DIM_MAX], init, addObj);
        }

        cout << " --> result = " << res << endl;
        t2 = system_clock::now();
        time_span = duration_cast<duration<double>>(t2-t1);
        cout << " --> time used = " << time_span.count() << endl;

        cout << endl;

        // apply the STL accumulate

        cout << " --> apply the STL accumulate for function objects"
             << " --> addFunObj<double>()" << endl;

        t1 = system_clock::now();

        for (long long i = 0; i != CALL_MAX; i++) {
            res = 0;
            init = 0.0;
            res = accumulate(&AA[0], &AA[DIM_MAX], init, addFunObj<double>());
        }

        cout << " --> result = " << res << endl;
        t2 = system_clock::now();
        time_span = duration_cast<duration<double>>(t2-t1);
        cout << " --> time used = " << time_span.count() << endl;

        cout << endl;

        // apply the handmade accumA

        cout << " --> apply the handmade accumA for function pointers"
             << " --> addFun" << endl;

        t1 = system_clock::now();

        for (long long i = 0; i != CALL_MAX; i++) {
            res = 0;
            init = 0.0;
            res = accumA(&AA[0], &AA[DIM_MAX], init, &addFun);
        }

        cout << " --> result = " << res << endl;
        t2 = system_clock::now();
        time_span = duration_cast<duration<double>>(t2-t1);
        cout << " --> time used = " << time_span.count() << endl;

        cout << endl;

        // delete the array

        delete [] AA;
    }

    return 0;
}

// end
