//=============//
// linked list //
//=============//

#include <cmath>
#include <iostream>
using std::endl;
using std::cout;

#define DEBUG
//#undef DEBUG

// list definition

class list {
public:
    int data;
    list * next;
};

// the main function

int main()
{
    //=============//
    // example # 1 //
    //=============//

    // # 1
    // create an array of nodes

    const int DIM(std::pow(10.0,2.0));
    list * nodes(new list [DIM]);

    // # 2
    // set the data element of each node

    for(int i = 0; i != DIM; i++) {
        nodes[i].data = i;
    }

    // # 3
    // set the data element of each node

#ifdef DEBUG
    for(int i = 0; i != DIM; i++) {
        cout << " nodes[" << i << "]= " << nodes[i].data << endl;
    }
#endif

    // # 4
    // connect the nodes

    for(int i = 0; i != DIM-1; i++) {
        nodes[i].next = &nodes[i+1];
    }

    // set end of nodes

    nodes[DIM-1].next = 0;

    // # 5
    // traverse the nodes

#ifdef DEBUG
    for(int i = 0; i != DIM-3; i++) {
        cout << " -------------------------->> i = " << i << endl;
        cout << " nodes[i].data = " << nodes[i].data << endl;
        cout << " nodes[i].next->data = " << nodes[i].next->data << endl;
        cout << " nodes[i].next->next->data = " << nodes[i].next->next->data << endl;
        cout << " nodes[i].next->next->next->data = " << nodes[i].next->next->next->data << endl;
    }
#endif

    // # 6
    // another way to traverse the nodes

    list * connection;
    connection = &nodes[0];

#ifdef DEBUG
    for(int i = 0; i != DIM; i++) {
        cout << " -------------------------->> i = " << i << endl;
        cout << " connection->data = " << connection->data << endl;
        connection = connection->next;
    }
#endif

    // # 7
    // delete nodes

    delete [] nodes;

    return 0;
}

// END
