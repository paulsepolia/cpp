//===================//
// garbage collector //
//===================//

#include <iostream>
#include <memory>
#include <set>
#include <cmath>

using std::cout;
using std::endl;
using std::shared_ptr;
using std::pow;
using std::set;

//==================//
// type definitions //
//==================//

typedef unsigned long int uli;
typedef const uli culi;

//==================//
// comparison class //
//==================//

template<typename T>
class is_less_shared_ptr {
public:

    bool operator()(const shared_ptr<T> & val_a, const shared_ptr<T> & val_b)
    {
        return (val_a.get() < val_b.get());
    }
};

//=========================//
// class declaration --> A //
//=========================//

class A {
public:

    A();
    virtual ~A();
    static set<shared_ptr<A>, is_less_shared_ptr<A>> _garbage;
};

//========================//
// class definition --> A //
//========================//

// static member initialization

set<shared_ptr<A>, is_less_shared_ptr<A>> A::_garbage {};

// constructor

A::A() {}

// destructor

A::~A() {}

//==================//
// the main program //
//==================//

int main()
{
    culi SIZE(static_cast<uli>(pow(10.0, 1.0)));
    culi TRIALS(1*static_cast<uli>(pow(10.0, 1.0)));

    for(uli j = 0; j != TRIALS; j++) {
        cout << "-------------------------------------------------------->> " << j << endl;

        for(uli i = 0; i != SIZE; i++) {
            A * pb = new A;

            // garbage collection

            shared_ptr<A> spa;
            spa.reset(pb);
            A::_garbage.insert(spa);
            cout << " --> A::_garbage.size() = " << A::_garbage.size() << endl;
        }
    }

    cout << " --> Please wait ... the destructor is being called "
         << (TRIALS*SIZE) << " times" << endl;
    cout << " --> In each time resets a shared pointer to null" << endl;

    return 0;
}

// end
