#!/bin/bash

  # 1. compile

  g++-6.2.0 -O3                 \
            -Wall               \
            -std=gnu++17        \
		    -fopenmp            \
		    -march=native       \
	       -D_GLIBCXX_PARALLEL \
		    functors.cpp        \
		    functions.cpp       \
            driver_program.cpp  \
            -o x_gnu_620
