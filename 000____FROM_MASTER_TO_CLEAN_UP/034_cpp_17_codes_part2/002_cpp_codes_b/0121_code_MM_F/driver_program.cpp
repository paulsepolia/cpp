//===========================//
// function pointers example //
//===========================//

#include <iostream>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <iomanip>

using std::endl;
using std::cout;
using std::cin;
using std::pow;
using std::rand;
using std::qsort;
using std::time;
using std::fixed;
using std::setprecision;

// function --> 1

// we use void * arguments so as to be able to use any type of agruments
// that is the C-way to pass any type of arguments
// C++ uses templates, but also the above syntax,
// so it is perfectly legal to use that in a C++ pure code

int int_sorter(const void * first_arg, const void * second_arg)
{
    int first = * (int*) first_arg;
    int second = * (int*) second_arg;

    // sort here

    if (first < second) {
        return -1;
    } else if (first == second) {
        return 0;
    } else {
        return 1;
    }
}

// function --> 2

// we use void * arguments so as to be able to use any type of agruments
// that is the C-way to pass any type of arguments
// C++ uses templates, but also the above syntax,
// so it is perfectly legal to use that in a C++ pure code

int dbl_sorter(const void * first_arg, const void * second_arg)
{
    double first = * (double*) first_arg;
    double second = * (double*) second_arg;

    // sort here

    if (first < second) {
        return -1;
    } else if (first == second) {
        return 0;
    } else {
        return 1;
    }
}

// the main function

int main()
{
    // local variables and parameters

    const int DIMEN_MAX = static_cast<int>(pow(10.0, 7.0));
    int * arrayInt = new int [DIMEN_MAX];
    double * arrayDbl = new double [DIMEN_MAX];
    int i;

    // adjust output

    cout << fixed;
    cout << setprecision(5);

    // build the array

    srand(4);

    // integers

    for (i = 0; i < DIMEN_MAX; i++) {
        arrayInt[i] = rand();
    }

    srand(4);

    // doubles

    for (i = 0; i < DIMEN_MAX; i++) {
        arrayDbl[i] = static_cast<double>(rand());
    }

    // sort the array of integers

    cout << " --> sort the array of integers" << endl;

    qsort(arrayInt, DIMEN_MAX, sizeof(int), int_sorter);

    // sort the array of doubles

    cout << " --> sort the array of doubles" << endl;

    qsort(arrayDbl, DIMEN_MAX, sizeof(double), dbl_sorter);

    // display some results

    for ( i = 0; i < 10; i++ ) {
        cout << " --> " << i << " --> " << arrayInt[i]
             << " --> " <<  arrayDbl[i] << endl;
    }

    // delete the array

    delete [] arrayInt;
    delete [] arrayDbl;

    return 0;
}

//======//
// FINI //
//======//

