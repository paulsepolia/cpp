//====================//
// exception handling //
//====================//

#include <stdexcept>
#include <limits>
#include <iostream>

using std::endl;
using std::cerr;
using std::cout;
using std::numeric_limits;
using std::invalid_argument;

// a class: MyClass

class MyClass {
public:
    void MyFunc(char c)
    {
        cout << " --> c = " << c << endl;
        cout << " --> int(c) = " << int(c) << endl;
        if(c < numeric_limits<char>::max()) {
            throw invalid_argument("MyFunc argument too large.");
        }
    }
};

// the main function

int main()
{
    cout << " --> numeric_limits<char>::max() = " << numeric_limits<char>::max() << endl;
    cout << " --> numeric_limits<char>::min() = " << numeric_limits<char>::min() << endl;
    cout << " --> int(numeric_limits<char>::max()) = " << int(numeric_limits<char>::max()) << endl;
    cout << " --> int(numeric_limits<char>::min()) = " << int(numeric_limits<char>::min()) << endl;

    MyClass A1;

    try {
        //A1.MyFunc(256); //cause an exception to throw
        A1.MyFunc('1'); //cause an exception to throw

    } catch(invalid_argument & e) {
        cerr << e.what() << endl;
        return -1;
    }

    return 0;
}

// end
