
//===========================//
// shuffle algorithm example //
//===========================//

#include <iostream>     // cout
#include <algorithm>    // shuffle, random_shuffle
#include <random>       // default_random_engine
#include <chrono>       // chrono::system_clock
#include <ctime>

using std::cout;
using std::endl;
using std::shuffle;
using std::random_shuffle;
using std::sort;
using std::is_sorted;
using std::default_random_engine;
using std::chrono::system_clock;

// the main function

int main ()
{
    // local parameters

    const int TRIALS = 10;
    const int DIM = 1000000;

    // local variables

    bool flag;
    time_t t1;
    time_t t2;

    // build the array

    int * foo = new int [DIM];

    for (int i = 0; i != DIM; i++) {
        foo[i] = i;
    }

    // obtain a time-based seed

    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();

    // shuffle // uniform random number generator

    t1 = clock();

    for (int i = 0; i != TRIALS; i++) {

        cout << "------------------------------------------->> A -->> " << i << endl;

        shuffle(foo, foo+DIM, default_random_engine(seed));

        sort(foo, foo+DIM, [](const int & i, const int j) {
            return i < j ;
        });

        flag = is_sorted(foo, foo+DIM, [](const int & i, const int & j) {
            return i < j;
        });
    }

    t2 = clock();

    cout << " flag = " << flag << endl;
    cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

    // shuffle again

    t1 = clock();

    for (int i = 0; i != TRIALS; i++) {

        cout << "------------------------------------------->> B -->> " << i << endl;

        random_shuffle(foo, foo+DIM);

        sort(foo, foo+DIM, [](const int & i, const int j) {
            return i < j;
        });

        flag = is_sorted(foo, foo+DIM, [](const int & i, const int & j) {
            return i < j;
        });
    }

    t2 = clock();

    cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;
    cout << " flag = " << flag << endl;

    return 0;
}

// END
