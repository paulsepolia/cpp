
//==================//
// move constructor //
//==================//

#include <string>
#include <iostream>
#include <iomanip>

using std::string;
using std::cout;
using std::endl;
using std::move;
using std::quoted;

// struct --> A

struct A {

    string s;

    // default construnctor

    A() : s("test") {}

    // copy construnctor

    A(const A& o) : s(o.s)
    {
        cout << " --> 1 --> inside copy constructor" << endl;
        cout << " --> 1 --> so ... move failed!" << endl;
    }

    // copy assignment operator

    const A & operator = (const A & other)
    {
        cout << " --> 2 --> inside copy assignment operator" << endl;

        this->s = other.s;
        return *this;
    }

    // move construnctor

    A(A&& o) : s(move(o.s))
    {
        cout << " --> 3 --> inside move constructor" << endl;
        cout << " --> 3 --> so ... move succeded!" << endl;
    }

    // move assignment operator

    A & operator = (A && other)
    {

        cout << " --> 4 --> inside move assignment operator" << endl;

        if (this != &other) {
            this->s = move(other.s);
        }

        return *this;
    }
};

// struct --> B

struct B : public A {

    string s2;
    int n;

    // implicit move contructor B::(B&&)
    // calls A's move constructor
    // calls s2's move constructor
    // and makes a bitwise copy of n
};

// function --> A

A f(A a)
{
    return a;
}

// the main function

int main()
{
    cout << " --> trying to move A" << endl;

    cout << " --> execute --> A a1  = f(A());" << endl;

    A a1 = f(A()); // move-construct from rvalue temporary

    cout << " --> A().s = " << A().s << endl;
    cout << " --> a1.s  = " << a1.s << endl;

    cout << " --> execute --> A();" << endl;

    A(); // NOTHING HAPPENS

    cout << " --> a1.s = " << a1.s << endl;

    cout << " --> execute --> move(a1);" << endl;

    move(a1); // NOTHING HAPPENS

    cout << " --> a1.s = " << a1.s << endl;

    cout << " --> execute --> f(A());" << endl;

    f(A()); // enters move constructor;

    cout << " --> execute --> A a2  = move(a1);" << endl;

    A a2 = move(a1); // move-construct from xvalue

    cout << " --> a2.s  = " << a2.s << endl;
    cout << " --> a1.s  = " << a1.s << endl;

    cout << " --> execute --> a2 = a1;" << endl;

    a2 = a1;

    cout << " --> a2.s  = " << a2.s << endl;
    cout << " --> a1.s  = " << a1.s << endl;

    A a11;
    A a22;

    cout << " --> execute --> a22 = move(a11);" << endl;

    a22 = move(a11);

    cout << " --> a22.s  = " << a22.s << endl;
    cout << " --> a11.s  = " << a11.s << endl;

    cout << " --> A a3;" << endl;

    A a3;

    cout << " --> a3.s = " << a3.s << endl;

    cout << " --> execute --> A a4 = f(a3)" << endl;

    A a4 = f(a3);

    cout << " a3.s = " << a3.s << endl;
    cout << " a4.s = " << a4.s << endl;

    cout << " --> trying to move B" << endl;

    cout << " --> B b1;" << endl;

    B b1;

    cout << " --> before move --> b1.s = " << quoted(b1.s) << endl;

    cout << " --> execute --> B b2 = move(b1);" << endl;

    B b2 = move(b1); // calls implicit move ctor

    cout << " --> after move --> b1.s = " << quoted(b1.s) << endl;

    return 0;
}

// END
