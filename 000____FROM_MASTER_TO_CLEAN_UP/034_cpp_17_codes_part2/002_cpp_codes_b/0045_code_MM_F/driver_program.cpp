
//===========================//
// usage of bcopy and memcpy //
//===========================//

#include <iostream>
#include <cmath>
#include <cstring>
#include <ctime>
#include <iomanip>

using std::cout;
using std::endl;
using std::pow;
using std::clock;
using std::fixed;
using std::setprecision;

// type definition

typedef unsigned long int uli;

// the main function

int main()
{
    uli DIM = 2*static_cast<uli>(pow(10.0, 2.0));
    uli TRIALS = 2*static_cast<uli>(pow(10.0, 1.0));

    double * a1 = new double (-1.0);
    double * a2 = new double [DIM];
    time_t t1;
    time_t t2;

    cout << fixed;
    cout << setprecision(10);

    cout << " *a1    = " << *a1 << endl;
    cout << "  a1[0] = " << a1[0] << endl;


    // build a2

    cout << " --> build a2" << endl;

    t1 = clock();

    for (uli j = 0; j != TRIALS; j++) {
        for (uli i = 0; i != DIM; i++) {
            a2[i] = static_cast<double>(i+1);
        }
    }

    t2 = clock();
    cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

    // memcpy

    cout << " --> memcpy" << endl;

    t1 = clock();

    for (uli i = 0; i != TRIALS; i++) {
        memcpy(a1, a2, DIM*sizeof(double)); // produces segmentation fault
        // access restricted piece of memory
    }

    t2 = clock();
    cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

    // bcopy

    cout << " --> bcopy" << endl;

    t1 = clock();

    for (uli i = 0; i != TRIALS; i++) {
        bcopy(a1, a2, DIM*sizeof(double)); // produces segmentation fault due to
        // access restricted piece of memory
    }

    t2 = clock();
    cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

    // test if are the same

    cout << " --> test" << endl;

    for (uli i = 0; i != DIM; i++) {
        if (bool(static_cast<int>(a1[0] - a2[i]))) {
            cout << " --> error" << endl;
            break;
        } else if (i == (DIM-1)) {
            cout << " --> they are equal" << endl;
        }
    }

    return 0;
}

// end
