//================//
// move symantics //
//================//

#include <iostream>
#include <utility>

using std::cout;
using std::endl;
using std::move;

int x(10);

// function --> fun1

int fun1()
{
    return 11;
}

// function --> fun2

const int & fun2()
{
    return x;
}

// function --> fun3

int && fun3()
{
    return move(x);
}

// function --> fun4

int fun4()
{
    return x;
}

// function --> fun5

int && fun5(int & x)
{
    return move(x);
}

// function --> fun6

const int & fun6(const int & x)
{
    return x;
}

// print address function

void printAd(const int & v)
{
    cout << reinterpret_cast<const void*>(&v) << endl;
}

// the main function

int main()
{
    printAd(x);

    printAd(fun1());

    printAd(fun2());

    printAd(fun3());

    printAd(fun4());

    printAd(fun5(x));

    printAd(fun6(x));

    return 0;
}

// end
