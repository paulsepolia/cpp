
//================//
// all_of example //
//================//

#include <iostream>
#include <algorithm>
#include <array>

using std::cout;
using std::cin;
using std::endl;
using std::array;
using std::all_of;

// the function

bool f_oddA(const int & i)
{
    return bool(i%2);
}

// the functor

template<class T>
struct f_oddB {
    bool operator()(const T & i)
    {
        return bool(i%2);
    }
};

// the main function

int main ()
{
    array<int,8> foo = {3,5,7,11,13,17,19,23};

    if (all_of(foo.begin(), foo.end(), f_oddA)) {
        cout << "All the elements are odd numbers." << endl;
    }

    if (all_of(foo.begin(), foo.end(), f_oddB<int>())) {
        cout << "All the elements are odd numbers." << endl;
    }

    return 0;
}

// END
