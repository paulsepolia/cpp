//===============================//
// MULTIPLE INHERITANCE 4 LEVELS //
//===============================//

#include "multipleInheritance4Levels.h"
#include <iostream>

using std::endl;
using std::cout;

//===================//
// the main function //
//===================//

int main()
{
    {
        cout << " --> A a1;" << endl;
        A a1;
        cout << " --> B b1;" << endl;
        B b1;
        cout << " --> C c1;" << endl;
        C c1;
        cout << " --> D d1;" << endl;
        D d1;
        cout << " --> E e1;" << endl;
        E e1;
        cout << " --> F f1;" << endl;
        F f1;

        cout << " --> a1.fun();" << endl;
        a1.fun();
        cout << " --> b1.fun();" << endl;
        b1.fun();
        cout << " --> c1.fun();" << endl;
        c1.fun();
        cout << " --> d1.fun();" << endl;
        d1.fun();
        cout << " --> e1.fun();" << endl;
        e1.fun();
        cout << " --> f1.fun();" << endl;
        f1.fun();

        cout << " --> exit --> 1" << endl;
    }
    cout << " --> MIDDLE ----------------------------------------------->> 1" << endl;
    {
        cout << " --> A * pa1 = new F;" << endl;
        A * pa1 = new F;
        cout << " --> B * pb1 = new F;" << endl;
        B * pb1 = new F;
        cout << " --> C * pc1 = new F;" << endl;
        C * pc1 = new F;
        cout << " --> D * pd1 = new F;" << endl;
        D * pd1 = new F;
        cout << " --> E * pe1 = new F;" << endl;
        E * pe1 = new F;
        cout << " --> F * pf1 = new F;" << endl;
        F * pf1 = new F;

        cout << " --> pa1->fun();" << endl;
        pa1->fun();
        cout << " --> pb1->fun();" << endl;
        pb1->fun();
        cout << " --> pc1->fun();" << endl;
        pc1->fun();
        cout << " --> pd1->fun();" << endl;
        pd1->fun();
        cout << " --> pe1->fun();" << endl;
        pe1->fun();
        cout << " --> pf1->fun();" << endl;
        pf1->fun();

        cout << " --> delete pa1;" << endl;
        delete pa1;
        cout << " --> delete pb1;" << endl;
        delete pb1;
        cout << " --> delete pc1;" << endl;
        delete pc1;
        cout << " --> delete pd1;" << endl;
        delete pd1;
        cout << " --> delete pe1;" << endl;
        delete pe1;
        cout << " --> delete pf1;" << endl;
        delete pf1;


        cout << " --> exit --> 2" << endl;
    }

    cout << " --> MIDDLE ----------------------------------------------->> 2" << endl;

    return 0;
}

// end
