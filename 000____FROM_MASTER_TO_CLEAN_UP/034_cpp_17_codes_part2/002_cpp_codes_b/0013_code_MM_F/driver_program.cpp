//=============//
// copy memory //
//=============//

#include <iostream>
#include <ctime>
#include <iomanip>
#include <cmath>
#include <typeinfo>

using std::cout;
using std::cin;
using std::endl;
using std::clock;
using std::fixed;
using std::setprecision;
using std::pow;

// type definition

typedef unsigned long int uli;

// global parameters

const uli DIM = static_cast<uli>(pow(10.0, 8.0));

// struct A

struct A {

public:

    // default constructor

    A(): dim(DIM)
    {
        p = new double [dim];
        for (uli i = 0; i != dim; i++) {
            p[i] = i;
        }
    }

    // destructor

    virtual ~A()
    {
        delete [] p;
        p = nullptr;
    }

    // variables

    uli dim;
    double * p;

};

// struct B

struct B: private A {
    B(): A() {}
    virtual ~B() {}
};

// struct C

struct C: private B {
    C(): B() {}
    virtual ~C() {}
};

// struct D

struct D: private C {
    D(): C() {}
    virtual ~D() {}
};


// the main function

int main ()
{
    const int K_MAX = 10;
    time_t t1;
    time_t t2;

    cout << fixed;
    cout << setprecision(10);

    {
        A a1;

        cout << " sizeof(a1) = " << sizeof(a1) << endl;
        cout << " sizeof(A)  = " << sizeof(A) << endl;
        cout << " typeid(A).name()  = " << typeid(A).name() << endl;
        cout << " typeid(a1).name() = " << typeid(a1).name() << endl;

        B b1;

        cout << " sizeof(b1) = " << sizeof(b1) << endl;
        cout << " sizeof(B)  = " << sizeof(B) << endl;
        cout << " typeid(B).name()  = " << typeid(B).name() << endl;
        cout << " typeid(b1).name() = " << typeid(b1).name() << endl;

        C c1;

        cout << " sizeof(c1) = " << sizeof(c1) << endl;
        cout << " sizeof(C)  = " << sizeof(C) << endl;
        cout << " typeid(C).name()  = " << typeid(C).name() << endl;
        cout << " typeid(c1).name() = " << typeid(c1).name() << endl;

        D d1;

        cout << " sizeof(d1) = " << sizeof(d1) << endl;
        cout << " sizeof(D)  = " << sizeof(D) << endl;
        cout << " typeid(D).name()  = " << typeid(D).name() << endl;
        cout << " typeid(d1).name() = " << typeid(d1).name() << endl;
    }

    cout << "--> derived --> level --> 1" << endl;

    t1 = clock();
    for (int ik = 0; ik != K_MAX; ik++) {

        cout << "------------------------------------------------>> " << ik << endl;

        B b1;
        B b2;
        B b3;
    }

    t2 = clock();
    cout << "--> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

    cout << "--> base" << endl;

    t1 = clock();
    for (int ik = 0; ik != K_MAX; ik++) {

        cout << "------------------------------------------------>> " << ik << endl;

        A a1;
        A a2;
        A a3;
    }

    t2 = clock();
    cout << "--> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

    cout << "--> derived --> level --> 2" << endl;

    t1 = clock();
    for (int ik = 0; ik != K_MAX; ik++) {

        cout << "------------------------------------------------>> " << ik << endl;

        C c1;
        C c2;
        C c3;
    }

    t2 = clock();
    cout << "--> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

    cout << "--> derived --> level --> 3" << endl;

    t1 = clock();
    for (int ik = 0; ik != K_MAX; ik++) {

        cout << "------------------------------------------------>> " << ik << endl;

        D d1;
        D d2;
        D d3;
    }

    t2 = clock();
    cout << "--> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

    return 0;
}

// END
