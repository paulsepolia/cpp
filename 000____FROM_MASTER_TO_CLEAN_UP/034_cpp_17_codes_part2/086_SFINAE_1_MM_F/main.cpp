#include <iostream>

template<typename T>
auto f(T i, typename T::t &j) -> void {
    std::cout << "f(T i, typename T::t& j)" << std::endl;
}

template<typename T>
auto f(T i, T j) -> void {
    std::cout << "f(T i, T j)" << std::endl;
}

struct A {
    struct t {
        int i;
    };

    t i;
};

auto main() -> int {

    A a{5};

    f(a, a.i);
    f(5,7);

    f<int>(5, 7);
}