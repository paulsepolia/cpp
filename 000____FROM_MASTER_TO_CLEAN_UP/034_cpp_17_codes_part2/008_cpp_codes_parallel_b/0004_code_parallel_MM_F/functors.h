#ifndef FUNCTORS_H
#define FUNCTORS_H

//======================//
// FUNCTORS DECLARATION //
//======================//

// functor # 1

template <typename T>
struct Sum {
public:
    Sum();
    void operator()(const T & n);
private:
    T sum;
};

// functor # 2

template<typename T>
struct UniqueNumber {
    T operator () ();
};

// functor # 3

template<typename T>
struct ac_functor {
    T operator()(const T &, const T &);
};

// functor # 4

template <typename T>
struct product_functor {
    T operator()(const T &, const T &);
};

// functor # 5

template <typename T>
struct equal_functor {
    bool operator()(const T &, const T &);
};

// functor # 6

template <typename T>
struct isOdd_functor {
    bool operator()(const T &);
};

// end

#endif // FUNCTORS_H
