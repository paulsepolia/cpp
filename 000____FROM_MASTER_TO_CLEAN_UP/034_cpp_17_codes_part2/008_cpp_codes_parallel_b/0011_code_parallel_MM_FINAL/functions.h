#ifndef FUNCTIONS_H
#define FUNCTIONS_H

//=======================//
// FUNCTIONS DECLARATION //
//=======================//

#include <cstdlib>
#include "functions.h"

using std::rand;

// type definition

typedef unsigned long int uli;

// function # 1

template <class T> inline
T ac_fun(const T & x, const T & y)
{
    return x+y;
}

// function # 2

template <class T> inline
T product_fun(const T & x, const T & y)
{
    return x*y;
}

// function # 3

template <class T> inline
bool equal_fun(const T & i, const T & j)
{
    return (!(i < j) && !(i > j));
}

// function # 4

template <class T> inline
bool isOdd_fun(const T & i)
{
    return ((uli)(i)%2 == 1);
}

// function # 5

template <class T> inline
bool lessThanMinusOne_fun(const T & i)
{
    return (i < -1);
}

// function # 6

template <class T> inline
T double_fun(const T & x)
{
    return 2*x;
}

// function # 7

template <class T> inline
T UniqueNumber_fun()
{
    return T(123.456);
}

// function # 8

template <class T> inline
bool strComp_fun(const T & x, const T & y)
{
    return (x < y);
}

// function # 9

template <class T> inline
T fun_pp(T & x)
{
    return x++;
}

// function # 10
// random generator function

template <class T> inline
int rand_fun(const T & i)
{
    return (rand()%static_cast<uli>(i));
}

// function # 11

template <class T> inline
bool lessThan_fun(const T & x, const T & y)
{
    return (x < y);
}

// end

#endif // FUNCTIONS_H
