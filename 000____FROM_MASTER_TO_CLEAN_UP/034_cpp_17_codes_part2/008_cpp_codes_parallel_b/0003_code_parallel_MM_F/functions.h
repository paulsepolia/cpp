#ifndef FUNCTIONS_H
#define FUNCTIONS_H

//=======================//
// FUNCTIONS DECLARATION //
//=======================//

// function # 1

template<typename T>
T ac_fun(const T &, const T &);

// function # 2

template <typename T>
T product_fun(const T &, const T &);

// function # 3

template <typename T>
bool equal_fun(const T &, const T &);

// end

#endif // FUNCTIONS_H
