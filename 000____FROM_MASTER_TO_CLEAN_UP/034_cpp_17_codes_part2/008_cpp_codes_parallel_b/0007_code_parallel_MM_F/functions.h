#ifndef FUNCTIONS_H
#define FUNCTIONS_H

//=======================//
// FUNCTIONS DECLARATION //
//=======================//

// function # 1

template<typename T>
T ac_fun(const T &, const T &);

// function # 2

template <typename T>
T product_fun(const T &, const T &);

// function # 3

template <typename T>
bool equal_fun(const T &, const T &);

// function # 4

template <typename T>
bool isOdd_fun(const T &);

// function # 5

template <typename T>
bool lessThanMinusOne_fun(const T &);

// function # 6

template <typename T>
T double_fun(const T &);

// function # 7

template <typename T>
T UniqueNumber_fun();

// function # 8

template <typename T>
bool strComp_fun(const T &, const T &);

// function # 9

template <typename T>
T fun_pp(T &);

// function # 10

template <typename T>
int rand_fun(const T &);

// function # 11

template <typename T>
bool lessThan_fun(const T &, const T &);

// end

#endif // FUNCTIONS_H
