#!/bin/bash

  # 1. compile

  g++  -O3                  \
       -Wall                \
       -std=c++0x           \
	  -fopenmp             \
	  -march=native        \
	  -D_GLIBCXX_PARALLEL  \
	  functors.cpp         \
	  functions.cpp        \
       driver_program.cpp   \
       -o x_gnu_ubu
