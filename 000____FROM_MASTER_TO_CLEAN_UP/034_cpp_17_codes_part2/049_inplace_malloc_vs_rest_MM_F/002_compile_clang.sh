#!/bin/bash

  clang++   -O0 -g            \
            -Wall          \
            -std=c++17     \
            -stdlib=libc++ \
            -pthread       \
            -pedantic      \
            main.cpp       \
            -o x_clang -lc++fs

