#include <iostream>
#include <vector>
#include <string>
#include <memory>
#include <functional>
#include <chrono>
#include <iomanip>
#include <cmath>

class benchmark_timer {
public:

    explicit benchmark_timer(const double &res) :
            _res(res),
            t1{std::chrono::steady_clock::now()},
            t2{std::chrono::steady_clock::now()} {};

    ~benchmark_timer() {
        t2 = std::chrono::steady_clock::now();
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count()};
        std::cout << " --> res = " << _res << std::endl;
        std::cout << " --> time used (secs) = " << total_time << std::endl;
    }

    auto set_res(const double &res) -> void {
        _res = res;
    }

private:

    double _res{};
    decltype(std::chrono::steady_clock::now()) t1{};
    decltype(std::chrono::steady_clock::now()) t2{};
};

int main() {

    std::cout << std::setprecision(5);
    std::cout << std::scientific;
    const auto DO_MAX{size_t(std::pow(10.0, 6.0))};

    for (size_t ii = 0; ii < 100; ii++) {

        std::cout << "-------------------------------------------->> # = " << ii << std::endl;

        {
            std::cout << "-->> 1 --> inplace only" << std::endl;
            auto memory{std::malloc(sizeof(double))};
            auto sum_tot{0.0};

            benchmark_timer ot(0.0);

            for (size_t kk = 0; kk < DO_MAX; kk++) {
                auto p{new(memory) double(1.0)};
                sum_tot += *p;
            }
            ot.set_res(sum_tot);
            std::free(memory);
        }
        {
            std::cout << "-->> 2 --> malloc + inplace + free" << std::endl;

            auto sum_tot{0.0};
            benchmark_timer ot(0.0);

            for (size_t kk = 0; kk < DO_MAX; kk++) {
                auto memory{std::malloc(sizeof(double))};
                auto p{new(memory) double(1.0)};
                sum_tot += *p;
                std::free(memory);
            }
            ot.set_res(sum_tot);
        }
        {
            std::cout << "-->> 3 --> malloc + free" << std::endl;

            auto sum_tot{0.0};
            benchmark_timer ot(0.0);

            for (size_t kk = 0; kk < DO_MAX; kk++) {
                auto p{(double *) std::malloc(sizeof(double))};
                *p = 1.0;
                sum_tot += *p;
                std::free(p);
            }
            ot.set_res(sum_tot);
        }
        {
            std::cout << "-->> 4 --> new + delete" << std::endl;

            auto sum_tot{0.0};
            benchmark_timer ot(0.0);

            for (size_t kk = 0; kk < DO_MAX; kk++) {
                auto p{new double(1.0)};
                sum_tot += *p;
                delete p;
            }
            ot.set_res(sum_tot);
        }
    }
}
