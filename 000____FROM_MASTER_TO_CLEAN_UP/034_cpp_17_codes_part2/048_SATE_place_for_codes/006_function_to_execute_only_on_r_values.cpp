#include <iostream>
#include <cmath>
#include <string>

class A {
public:

    // force the compiler to generate all the default 5 functions

    A() = default;

    A(const A &) = default;

    A(A &&) noexcept = default;

    auto operator=(const A &) -> A & = default;

    auto operator=(A &&) noexcept -> A & = default;

    virtual ~A() = default;
};

struct Foo {
    auto func() &&{
        _index++;
        std::cout << "--> I can execute only r-values!!! --> index = " << _index << std::endl;
    }

    uint64_t _index{0};
};

auto main() -> int {

    auto a = Foo{};

    for (uint64_t i = 1; i <= 10; i++) {
        std::cout << "------------------------->> " << i << std::endl;
        std::move(a).func();
    }
}