#include <iostream>
#include <cmath>
#include <string>
#include <vector>
#include <optional>
#include <algorithm>
#include <any>

// the rule of five

class A {
public:
    A() = default;

    ~A() = default;

    A(const A &) = default;

    A(A &&) noexcept = default;

    auto operator=(const A &) -> A & = default;

    auto operator=(A &&) noexcept -> A & = default;

    [[nodiscard]] auto get_string() const -> std::string {
        return _s;
    }

    auto set_string(const std::string &s) -> void {
        _s = s;
    }

private:

    std::string _s{};
};

template<typename T>
auto is_that_type(const std::any &a) -> bool {
    return typeid(T) == a.type();
}

auto main() -> int {

    {
        std::cout << "--------------------------------------------->> 1" << std::endl;

        A a;
        a.set_string("123456");
        std::cout << a.get_string() << std::endl;

        std::vector<A> v;
        v.push_back(a);
        v.push_back(a);
        v.push_back(a);
        v.push_back(std::move(a));

        for (const auto &el: v) {
            std::cout << el.get_string() << std::endl;
        }

        std::cout << "problem here --> 1" << std::endl;
    }

    {
        std::cout << "--------------------------------------------->> 2" << std::endl;

        auto ve = std::vector<std::optional<double>>{{3},
                                                     {},
                                                     {},
                                                     {4},
                                                     {-1},
                                                     {10}};

        std::sort(ve.begin(), ve.end());

        for (const auto &el: ve) {
            if (el.has_value()) {
                std::cout << el.value() << std::endl;
            }
        }
    }

    {
        std::cout << "--------------------------------------------->> 3" << std::endl;

        auto a = std::any{};
        a = std::string{"12345"};

        auto &str_ref = std::any_cast<std::string &>(a);
        std::cout << str_ref << std::endl;

        auto str_copy = std::any_cast<std::string>(a);
        std::cout << str_copy << std::endl;

        a = 123.456f;
        auto float_copy = std::any_cast<float>(a);
        std::cout << float_copy << std::endl;

        auto &float_ref = std::any_cast<float &>(a);
        std::cout << float_ref << std::endl;

        float_ref = 456.789f;

        std::cout << std::any_cast<float>(a) << std::endl;

        std::cout << a.type().name() << std::endl;
        std::cout << typeid(float).name() << std::endl;

        std::cout << is_that_type<float>(a) << std::endl;
        std::cout << is_that_type<double>(a) << std::endl;
        std::cout << is_that_type<std::string>(a) << std::endl;

        a = std::vector<std::string>{"123", "456"};

        std::cout << is_that_type<std::vector<std::string>>(a) << std::endl;


    }

}