#include <iostream>
#include <thread>
#include <mutex>
#include <queue>
#include <condition_variable>

auto cv{std::condition_variable{}};
auto q{std::queue<double>{}};
auto mtx{std::mutex{}};
constexpr double done{-1.0};

auto generate_doubles() -> void {

    for (auto i: {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, done}) {

        std::this_thread::sleep_for(std::chrono::seconds(2));

        {
            std::lock_guard<std::mutex> lock(mtx);
            for (uint64_t kk = 0; kk < 3; kk++) {
                q.push(i);
            }
        }

        cv.notify_one();
    }
}

auto print_doubles() -> void {

    auto i{(double) 0.0};

    while (i != done) {

        std::cout << "---> i have something for you...." << std::endl;

        auto lock{std::unique_lock<std::mutex>{mtx}};

        // test something here
        // and if it is true then make the thread to wait
        // and the release the lock, so the other thread can consume the lock
        // and notify the here-thread to wake-up, so the here-thread wakes up
        // and continues, but the test is not valid anymore
        // and the while loop while(q.empty()) exits ...

        while (q.empty()) {
            cv.wait(lock);
        }

        i = q.front();
        q.pop();

        if (i != done) {
            std::cout << " got --> " << i << std::endl;
        }
    }
}

auto main() -> int {

    std::cout << std::boolalpha;

    auto producer{std::thread{generate_doubles}};
    auto consumer{std::thread{print_doubles}};

    producer.join();
    consumer.join();
}