#include <iostream>
#include <vector>
#include <cmath>

class AR5 {
public:

    explicit AR5(size_t dim) : _dim{dim}, _ptr(new double[dim]) {}

    auto set_element(size_t i, double val) {
        _ptr[i] = val;
    }

    [[nodiscard]] auto get_element(size_t i) const {
        return _ptr[i];
    }

    AR5(const AR5 &obj) {
        _dim = obj._dim;
        _ptr = new double[_dim];
        std::copy(obj._ptr, obj._ptr + _dim, _ptr);
    }

    [[nodiscard]] auto get_dim() const {
        return _dim;
    }

    auto &operator=(const AR5 &obj) {

        if (this != &obj) {
            delete[] _ptr;
            _ptr = nullptr;
            _dim = obj._dim;
            _ptr = new double[_dim];
            std::copy(obj._ptr, obj._ptr + _dim, _ptr);
        }

        return *this;
    }

    AR5(AR5 &&obj) noexcept : _dim{obj._dim}, _ptr{obj._ptr} {
        obj._dim = 0;
        obj._ptr = nullptr;
    }

    auto &operator=(AR5 &&obj) noexcept {

        if (this != &obj) {
            _dim = obj._dim;
            _ptr = obj._ptr;
            _dim = 0;
            obj._ptr = nullptr;
        }

        return *this;
    }

    virtual ~AR5() {
        delete[] _ptr;
        _ptr = nullptr;
    };

private:
    size_t _dim{0};
    double *_ptr{nullptr};
};

auto main() -> int {

    std::cout.precision(10);
    std::cout << std::fixed;

    const auto DIM_MAX = static_cast<size_t>(std::pow(10.0, 5.0));

    {
        std::cout << "-------------------------------------------->> 1" << std::endl;

        auto o1{AR5(DIM_MAX)};

        o1.set_element(1, 1);
        o1.set_element(10, 10);

        const auto o2 = o1;

        std::cout << o1.get_element(1) << std::endl;
        std::cout << o2.get_element(1) << std::endl;

        std::cout << o1.get_element(10) << std::endl;
        std::cout << o2.get_element(10) << std::endl;
    }

    {
        std::cout << "-------------------------------------------->> 2" << std::endl;

        auto o1{AR5(DIM_MAX)};

        o1.set_element(1, 1);
        o1.set_element(10, 10);

        const auto o2 = std::move(o1);

        std::cout << o2.get_element(1) << std::endl;
        std::cout << o2.get_element(10) << std::endl;
    }

    {
        std::cout << "-------------------------------------------->> 3" << std::endl;

        auto o1{AR5(DIM_MAX)};

        o1.set_element(1, 1);
        o1.set_element(10, 10);

        auto o2{AR5(0)};
        o2 = std::move(o1);

        std::cout << o2.get_element(1) << std::endl;
        std::cout << o2.get_element(10) << std::endl;
    }

    {
        std::cout << "-------------------------------------------->> 4" << std::endl;

        std::vector<AR5> v;

        for (size_t i = 0; i < 10; i++) {
            v.emplace_back(AR5(DIM_MAX));
        }

        for (auto &el: v) {
            for (size_t i = 0; i < el.get_dim(); i++) {
                el.set_element(i, static_cast<double>(i));
            }
        }

        auto sum_loc{0.0};

        for (auto &el: v) {
            for (size_t i = 0; i < el.get_dim(); i++) {
                sum_loc += el.get_element(i);
            }
        }

        std::cout << sum_loc << std::endl;
    }
}