#include <iostream>
#include <cmath>
#include <thread>
#include <mutex>
#include <cassert>
#include <iomanip>
#include <vector>

const auto DO_MAX{ (size_t)(std::pow(10.0, 9.0)) };
constexpr auto NUM_TH{ uint32_t(8) };
constexpr auto ONE_LOC{ double(1.0) };
constexpr auto ZERO_LOC{ double(0.0) };

auto sum_mutex{ std::mutex{} };

auto res{ 0.0 };

auto sum_mtx_slow(size_t do_times) -> double
{
	for (size_t kk{ 0 }; kk < do_times; kk++)
	{
		auto lock{ std::lock_guard<std::mutex>{sum_mutex} };
		res += ONE_LOC;
	}
	return res;
}

auto sum_mtx_fast(size_t do_times) -> double
{
	auto lock{ std::lock_guard<std::mutex>{sum_mutex} };

	for (size_t kk{ 0 }; kk < do_times; kk++)
	{
		res += ONE_LOC;
	}
	return res;
}


int main() {

	std::cout << std::boolalpha;
	std::cout << std::setprecision(5);
	std::cout << std::fixed;

	std::cout << "---->> 1 --> create threads" << std::endl;

	//auto fun = sum_mtx_fast;
	auto fun = sum_mtx_slow;

	auto vec{ std::vector<std::thread>() };

	for (uint32_t i{ 0 }; i < NUM_TH; i++)
	{
		vec.emplace_back(std::thread(fun, DO_MAX));
	}

	std::cout << "---->> 2 --> join threads" << std::endl;

	for (auto& el : vec)
	{
		el.join();
	}

	std::cout << "---->> 3 --> res = " << (res == DO_MAX * NUM_TH) << std::endl;
}