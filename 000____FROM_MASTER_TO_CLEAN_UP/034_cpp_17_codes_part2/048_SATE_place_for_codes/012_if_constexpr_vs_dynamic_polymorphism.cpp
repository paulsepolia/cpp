#include <iostream>
#include <vector>
#include <memory>
#include <functional>
#include <chrono>
#include <iomanip>
#include <cmath>

class benchmark_timer {
public:

    explicit benchmark_timer(const double &res) :
            _res(res),
            t1{std::chrono::steady_clock::now()},
            t2{std::chrono::steady_clock::now()} {};

    ~benchmark_timer() {
        t2 = std::chrono::steady_clock::now();
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count()};
        std::cout << " --> res = " << _res << std::endl;
        std::cout << " --> time used (secs) = " << total_time << std::endl;
    }

    auto set_res(const double &res) -> void {
        _res = res;
    }

private:

    double _res{};
    decltype(std::chrono::steady_clock::now()) t1{};
    decltype(std::chrono::steady_clock::now()) t2{};
};

class A {
public:

    explicit A(std::string name) : _name(std::move(name)) {}

    [[nodiscard]] auto get_name() const -> std::string {
        return _name;
    }

    auto set_name(std::string name) -> void {
        _name = std::move(name);
    }

    virtual ~A() = default;

private:
    std::string _name;
};

//====================================================================//
// usage of if constexpr to achieve static(compile time) polymorphism //
//====================================================================//

struct Bear {
    auto roar() const -> void {
        std::cout << "roar" << std::endl;
    }
};

struct Duck {
    auto quack() const -> void {
        std::cout << "quack" << std::endl;
    }
};

struct Cat {
    auto miaou() const -> void {
        std::cout << "miaou" << std::endl;
    }
};

template<typename Animal>
auto speak(const Animal &a) {
    if constexpr (std::is_same_v<Animal, Bear>) {
        a.roar();
    } else if constexpr (std::is_same_v<Animal, Duck>) {
        a.quack();
    } else if constexpr (std::is_same_v<Animal, Cat>) {
        a.miaou();
    }
}

//========================================//
// classic way to of dynamic polymorphism //
//========================================//

struct Animal {

    virtual auto speak() const -> void {
        std::cout << "can not speak right now" << std::endl;
    }

    virtual ~Animal() = default;
};

struct Bear1 : public Animal {

    auto roar() const -> void {
        std::cout << "roar" << std::endl;
    }

    auto speak() const -> void override {
        roar();
    }
};

struct Duck1 : public Animal {

    auto quack() const -> void {
        std::cout << "quack" << std::endl;
    }

    auto speak() const -> void override {
        quack();
    }
};

struct Cat1 : public Animal {

    auto miaou() const -> void {
        std::cout << "miaou" << std::endl;
    }

    auto speak() const -> void override {
        miaou();
    }
};

template<typename Animal>
auto speak1(const Animal &a) {
    a.speak();
}

int main() {

    //===========================================//
    // 'if constexpr' way of static polymorphism //
    //===========================================//

    {
        std::cout << "------------------------->> 1" << std::endl;
        const auto bear{Bear()};
        speak(bear);
    }
    {
        std::cout << "------------------------->> 2" << std::endl;
        const auto duck{Duck()};
        speak(duck);
    }
    {
        std::cout << "------------------------->> 3" << std::endl;
        const auto cat{Cat()};
        speak(cat);
    }

    //==========================================//
    // classic way of dynamic polymorphism - v1 //
    //==========================================//

    {
        std::cout << "------------------------->> 4" << std::endl;
        const Animal *p{new Bear1()};
        p->speak();
    }
    {
        std::cout << "------------------------->> 5" << std::endl;
        const Animal *p{new Duck1()};
        p->speak();
    }
    {
        std::cout << "------------------------->> 6" << std::endl;
        const Animal *p{new Cat1()};
        p->speak();
    }

    //==========================================//
    // classic way of dynamic polymorphism - v2 //
    //==========================================//

    {
        std::cout << "------------------------->> 7" << std::endl;
        const auto bear{Bear1()};
        speak1(bear);
    }
    {
        std::cout << "------------------------->> 8" << std::endl;
        const auto duck{Duck1()};
        speak1(duck);
    }
    {
        std::cout << "------------------------->> 9" << std::endl;
        const auto cat{Cat1()};
        speak1(cat);
    }

    //==========================================//
    // classic way of dynamic polymorphism - v3 //
    //==========================================//

    {
        std::cout << "------------------------->>10" << std::endl;
        const Animal *p{new Bear1()};
        speak1(*p);
    }
    {
        std::cout << "------------------------->>11" << std::endl;
        const Animal *p{new Duck1()};
        speak1(*p);
    }
    {
        std::cout << "------------------------->>12" << std::endl;
        const Animal *p{new Cat1()};
        speak1(*p);
    }

}