//====================//
// classes definition //
//====================/

#include <iostream>

using std::cout;
using std::endl;

#include "ClassesIAnimal.h"

//===============//
// class --> Cat //
//===============//

// fun --> 1

int Cat::GetNumberOfLegs() const
{
    return 4;
}

// fun --> 2

void Cat::Speak()
{
    cout << "I am a cat!" << endl;
}

// fun --> 3

void Cat::Free()
{
    delete this;
}


// fun --> 4

static IAnimal * __stdcall Cat::Create()
{
    return new Cat();
}

//===============//
// class --> Dog //
//===============//

// fun --> 1

int Dog::GetNumberOfLegs() const
{
    return 4;
}

// fun --> 2

void Dog::Speak()
{
    cout << "I am a dog!" << endl;
}

// fun --> 3

void Dog::Free()
{
    delete this;
}


// fun --> 4

static IAnimal * __stdcall Dog::Create()
{
    return new Dog();
}

//==================//
// class --> Spider //
//==================//

// fun --> 1

int Spider::GetNumberOfLegs() const
{
    return 8;
}

// fun --> 2

void Spider::Speak()
{
    cout << "I am a Spider!" << endl;
}

// fun --> 3

void Spider::Free()
{
    delete this;
}

// fun --> 4

static IAnimal * __stdcall Spider::Create()
{
    return new Spider();
}

//=================//
// class --> Horse //
//=================//

// fun --> 1

int Horse::GetNumberOfLegs() const
{
    return 4;
}

// fun --> 2

void Horse::Speak()
{
    cout << "I am a Horse!" << endl;
}

// fun --> 3

void Horse::Free()
{
    delete this;
}

// fun --> 4

static IAnimal * __stdcall Horse::Create()
{
    return new Horse();
}

// end
