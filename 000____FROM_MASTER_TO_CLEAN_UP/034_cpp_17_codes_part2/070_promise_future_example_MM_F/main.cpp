#include <iostream>
#include <future>
#include <vector>
#include <chrono>

class benchmark_timer {
public:

    explicit benchmark_timer(const double &res) :
            _res(res),
            t1{std::chrono::steady_clock::now()},
            t2{std::chrono::steady_clock::now()} {};

    ~benchmark_timer() {
        t2 = std::chrono::steady_clock::now();
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count()};
        std::cout << " --> res = " << _res << std::endl;
        std::cout << " --> time used (secs) = " << total_time << std::endl;
    }

    auto set_res(const double &res) -> void {
        _res = res;
    }

private:

    double _res{};
    decltype(std::chrono::steady_clock::now()) t1{};
    decltype(std::chrono::steady_clock::now()) t2{};
};

auto set_promise_value(std::promise<std::vector<double>> &prom, std::vector<double> &&val) -> void {

    std::cout << " --> I am about to set a value to the promise I gave but I am sleeping right now..." << std::endl;
    std::this_thread::sleep_for(std::chrono::milliseconds(10000));
    std::cout << " --> I am setting a value to my promise..." << std::endl;
    prom.set_value(std::move(val));
    std::cout << " --> My promised is fulfilled!" << std::endl;
}

auto get_future_value(std::future<std::vector<double>> &fut, std::vector<double> &val) -> void {

    std::cout << " --> I am waiting for my future to get its promise..." << std::endl;
    val = fut.get();
}

constexpr auto DIM_MAX{(size_t) 50'000'000};

auto main() -> int {

    {
        std::cout << "------------------------------------------->> 1" << std::endl;

        auto prom{std::promise<std::vector<double>>{}};
        auto fut{std::future<std::vector<double>>{prom.get_future()}};
        auto vv{std::vector<double>()};

        {
            std::cout << " -->> build data" << std::endl;
            auto ot{benchmark_timer(0.0)};
            for (size_t i = 0; i < DIM_MAX; i++) {
                vv.push_back((double) i);
            }
            ot.set_res(123.456);
        }

        {
            std::cout << " -->> set promise" << std::endl;
            auto ot{benchmark_timer(0.0)};
            prom.set_value(vv);
            ot.set_res(123.456);
        }

        {
            auto ot{benchmark_timer(0.0)};
            std::cout << " -->> get promise" << std::endl;
            const auto val{fut.get()};
            ot.set_res(123.456);
            std::cout << " --> val[1] = " << val[1] << std::endl;
        }
    }

    {
        std::cout << "------------------------------------------->> 2" << std::endl;

        auto prom{std::promise<std::vector<double>>{}};
        auto fut{std::future<std::vector<double>>{prom.get_future()}};
        auto vv{std::vector<double>()};

        {
            std::cout << " -->> build data" << std::endl;
            auto ot{benchmark_timer(0.0)};
            for (size_t i = 0; i < DIM_MAX; i++) {
                vv.push_back((double) i);
            }
            ot.set_res(123.456);
        }

        {
            std::cout << " -->> set promise" << std::endl;
            auto ot{benchmark_timer(0.0)};
            prom.set_value(std::move(vv));
            ot.set_res(123.456);
        }

        {
            auto ot{benchmark_timer(0.0)};
            std::cout << " -->> get promise" << std::endl;
            const auto val{std::move(fut.get())};
            ot.set_res(123.456);
            std::cout << " --> val[1] = " << val[1] << std::endl;
        }
    }

    {
        std::cout << "------------------------------------------->> 3" << std::endl;

        auto prom{std::promise<std::vector<double>>{}};
        auto fut{std::future<std::vector<double>>{prom.get_future()}};
        auto vv{std::vector<double>()};

        {
            std::cout << " -->> build data" << std::endl;
            auto ot{benchmark_timer(0.0)};
            for (size_t i = 0; i < DIM_MAX; i++) {
                vv.push_back((double) i);
            }
            ot.set_res(123.456);
        }

        std::cout << " -->> get promise" << std::endl;

        auto my_promised_data{std::vector<double>{}};

        std::cout << " --> starting thread to get the promised data" << std::endl;

        auto th1{std::thread(get_future_value, std::ref(fut), std::ref(my_promised_data))};

        std::cout << " -->> starting thread to set the promise" << std::endl;

        auto th2{std::thread(set_promise_value, std::ref(prom), std::move(vv))};

        th1.join();
        th2.join();

        std::cout << " --> my promised data[0] = " << my_promised_data[0] << std::endl;
        std::cout << " --> my promised data[1] = " << my_promised_data[1] << std::endl;
        std::cout << " --> my promised data[2] = " << my_promised_data[2] << std::endl;
    }
}
