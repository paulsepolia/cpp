
#include "memory_block.h"
#include <iostream>
#include <vector>

using std::cout;
using std::cin;
using std::endl;
using std::vector;

// the main function

int main()
{
    // create a vector object and add a few elements to it

    int tmp;

    vector<MemoryBlock> v;

    cout << " --> add element MemoryBlock(25)" << endl;
    cin >> tmp;

    v.push_back(MemoryBlock(25));

    cout << " --> add element MemoryBlock(75)" << endl;
    cin >> tmp;

    v.push_back(MemoryBlock(75));

    // insert a new element into the second position of the vector

    cout << " --> insert element MemoryBlock(50)" << endl;
    cin >> tmp;

    v.insert(v.begin() + 1, MemoryBlock(50));

    // return

    cout << " --> exiting" << endl;

    cin >> tmp;

    return 0;
}

