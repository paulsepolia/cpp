//======================================//
// Codility ‘Tape Equilibrium’ Solution //
//======================================//

//================================================================//
// Short Problem Definition                                       //
// Minimize the value |(A[0] + … + A[P-1]) – (A[P] + … + A[N-1])| //
//================================================================//

//=================//
// solution by PGG //
//=================//

// you can use includes, for example:

#include <algorithm>
#include <vector>
#include <iostream>
#include <cfloat>

using std::cout;
using std::endl;
using std::vector;
using std::min;

// the solution

int solution(vector<int> &A)
{
    int sol(0);
    unsigned int i(0);
    unsigned int DIM = A.size();
    const double init(0.0);
    double tmp;
    double min_val = DBL_MAX;

    if (DIM == 2) {
        sol = abs(A[0]-A[1]);
    } else {
        for (i = 1; i != DIM-1; i++) {
            double a1 = accumulate(A.begin(), A.begin()+i, init);
            double a2 = accumulate(A.begin()+i, A.end(), init);
            tmp = abs(a1 - a2);
            min_val = min(tmp, min_val);
        }

        sol = min_val;
    }

    return sol;

}

// the main function

int main()
{
    vector<int> A( {1,2,3,4});
    int sol;

    sol = solution(A);

    cout << " sol = " << sol << endl;

    return 0;
}

// end
