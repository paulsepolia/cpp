
//===============================================//
// vector vs list comparison to sort and iterate //
// NOTE: the elements stored in vector and list  //
// are object of a class occupying lot of RAM    //
//===============================================//

#include <iostream>
#include <vector>
#include <list>
#include <ctime>
#include <cmath>
#include <algorithm>
#include <iomanip>

using std::cout;
using std::endl;
using std::cin;
using std::vector;
using std::list;
using std::clock;
using std::cos;
using std::sort;
using std::setprecision;
using std::fixed;
using std::copy;

// class : BIG_DATA

class BIG_DATA {

public:

    // constructor

    explicit BIG_DATA(int len) : dim(len)
    {
        dat = new double [dim];
    }

    // destructor

    ~BIG_DATA()
    {
        if (dat != 0) {
            delete [] dat;
        }
    }

    // copy constructor

    BIG_DATA(const BIG_DATA & other)
        : dim(other.dim),
          dat(new double[other.dim])
    {

        copy(other.dat, other.dat + dim, this->dat);
    }

    // operator <

    bool operator <(const BIG_DATA & elem) const
    {
        return (this->dim < elem.dim);
    }

    // operator =

    BIG_DATA & operator =(
        const BIG_DATA & elem)
    {
        for (int i = 0; i < this->dim; ++i) {

            this->dat[i] = elem.dat[i];
        }

        return *this;
    }

    // deallocate

    void deallocate()
    {
        if (this->dat != 0) {
            delete [] this->dat;
        }
        dat = 0;
    }

private:

    int dim;
    double * dat;
};

// the main function

int main()
{
    const int DIM = 30000;
    const int K_MAX = 100000;

    for (int k = 0; k != K_MAX; ++k) {

        cout << "------------------------------------------------>> " << K_MAX << endl;

        vector<BIG_DATA> * vec = new vector<BIG_DATA>[1];
        list<BIG_DATA> * ls = new list<BIG_DATA>[1];
        clock_t t1;
        clock_t t2;

        cout << fixed;
        cout << setprecision(10);

        // build vector

        cout << " --> build the vector" << endl;

        for (int i = 0; i != DIM; ++i) {
            vec[0].push_back(BIG_DATA(i));
        }

        // iterate over the vector

        cout << " --> iterate over the vector" << endl;

        t1 = clock();

        for (unsigned int i = 0; i != vec->size(); ++i) {
            BIG_DATA xxx = vec[0][i];
        }

        t2 = clock();

        cout << " --> time used to iterate over the vector = "
             << (t2-static_cast<double>(t1))/CLOCKS_PER_SEC << endl;

        // sort vector

        cout << " --> sort the vector" << endl;

        t1 = clock();

        sort(vec[0].begin(), vec[0].end());

        t2 = clock();

        cout << " --> time used to sort the vector = "
             << (t2-static_cast<double>(t1))/CLOCKS_PER_SEC << endl;

        // delete vector

        cout << " --> delete vector" << endl;

        // that step is not needed
        // since the second delete [] vec;
        // it is enough

        for (unsigned int i = 0; i != vec->size(); ++i) {
            vec[0][i].deallocate();
        }

        delete [] vec;

        // build_list

        cout << " --> build the list" << endl;

        for (int i = 0; i != DIM; ++i) {
            ls[0].push_back(BIG_DATA(i));
        }

        // iterate over the list

        cout << " --> iterate over the list" << endl;

        t1 = clock();

        list<BIG_DATA>::iterator it;

        for (it = ls[0].begin(); it != ls[0].end(); ++it) {
            BIG_DATA xxx = *it;
        }

        t2 = clock();

        cout << " --> time used to iterate over the list = "
             << (t2-static_cast<double>(t1))/CLOCKS_PER_SEC << endl;

        // sort list

        cout << " --> sort the list" << endl;

        t1 = clock();

        ls[0].sort();

        t2 = clock();

        cout << " --> time used to sort the list = "
             << (t2-static_cast<double>(t1))/CLOCKS_PER_SEC << endl;

        // delete list

        cout << " --> delete list" << endl;

        delete [] ls;
    }

    // return

    int sentinel;
    cin >> sentinel;

    return 0;
}
