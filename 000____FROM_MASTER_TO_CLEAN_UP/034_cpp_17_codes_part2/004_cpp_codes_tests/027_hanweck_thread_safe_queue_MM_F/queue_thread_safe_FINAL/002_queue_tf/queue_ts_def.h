//===========================//
// queue_ts class definition //
//===========================//

#ifndef QUEUE_TS_DEF_H
#define QUEUE_TS_DEF_H

#include "queue_ts_dec.h"

#include <queue>
#include <mutex>
using std::queue;
using std::mutex;

#include <iostream>
using std::cout;
using std::endl;

std::mutex mtx;

// 1 --> constructor

template <class T>
queue_ts<T>::queue_ts() {};

// 2 --> push # 1

template <class T>
void queue_ts<T>::push(const T & val)
{
    mtx.lock();

    this->queue<T>::push(val);

    mtx.unlock();
}

// 3 --> push # 2

template <class T>
void queue_ts<T>::push(T & val)
{
    mtx.lock();

    this->queue<T>::push(val);

    mtx.unlock();
}

// 4 -> copy constructor

template <class T>
queue_ts<T>::queue_ts(const queue_ts<T> & q)
{
    for (int i = 0; i != this->size(); i++) {
        this->pop();
    }

    queue<T> qTmp(q); // PROBLEM HERE

    for (int i = 0; i != q.size(); i++) {
        this->push(qTmp.front());
        qTmp.pop();
    }
}

#endif // QUEUE_TS_DEF_H

// end

