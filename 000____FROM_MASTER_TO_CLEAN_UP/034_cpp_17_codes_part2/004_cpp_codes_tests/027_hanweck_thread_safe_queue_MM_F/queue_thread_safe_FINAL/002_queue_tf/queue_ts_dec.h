//============================//
// queue_ts class declaration //
//============================//

#ifndef QUEUE_TS_DEC_H
#define QUEUE_TS_DEC_H

#include <queue>

using std::queue;

template <class T>
class queue_ts : public queue<T> {

public:

    // construnctor

    queue_ts();

    // copy construnctor

    queue_ts(const queue_ts<T> &);

    // member functions

    void push(const T &);
    void push(T &);
};

#endif // QUEUE_TS_DEC_H

// end
