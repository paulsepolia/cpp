//============================//
// queueThreadSafe definition //
//============================//

#ifndef QUEUE_THREAD_SAFE_DEF_H
#define QUEUE_THREAD_SAFE_DEF_H

#include "queueThreadSafeDeclaration.h"

#include <iostream>
#include <queue>
#include <mutex>
#include <thread>

using std::queue;
using std::mutex;
using std::cout;
using std::endl;
using std::unique_lock;
using std::thread;

// 0 --> main thread

thread::id main_thread_id = std::this_thread::get_id();

// 1 --> constructor

template <class T>
queueThreadSafe<T>::queueThreadSafe() {};

// 2 --> push

template <class T>
void queueThreadSafe<T>::push(const T & val)
{
    unique_lock<mutex> lck(m_mtx);
    m_queue.push(val);
    lck.unlock();
    m_cv.notify_one();
}

// 3 --> copy constructor

template <class T>
queueThreadSafe<T>::queueThreadSafe(const queueThreadSafe & q)
{
    if (main_thread_id == std::this_thread::get_id()) {

        cout << " A++ " << endl;

        unsigned int d_loc = m_queue.size();

        cout << " A-- " << endl;

        for (unsigned int i = 0; i != d_loc; i++) {
            cout << " A --> " << i << endl;
            m_queue.pop();
            cout << " B --> " << i << endl;
        }

        queue<T> qTmp(q); // PROBLEM HERE (OXI)

        d_loc = q.size();

        for (unsigned int i = 0; i != d_loc; i++) {
            m_queue.push(qTmp.front());
            qTmp.pop();
        }
    }
}

// 4 --> pop

template <class T>
void queueThreadSafe<T>::pop()
{
    unique_lock<mutex> lck(m_mtx);

    while(m_queue.empty()) {
        m_cv.wait(lck);
    }

    m_queue.pop();
    lck.unlock();
    m_cv.notify_one();
}

// 5 --> back

template <class T>
T  queueThreadSafe<T>::back()
{
    unique_lock<mutex> lck(m_mtx);
    T val = m_queue.back();

    return val;
}

// 6 --> front

template <class T>
T  queueThreadSafe<T>::front()
{
    unique_lock<mutex> lck(m_mtx);

    while(m_queue.empty()) {
        m_cv.wait(lck);
    }

    T val = m_queue.front();
    lck.unlock();
    m_cv.notify_one();

    return val;
}

// 7 --> emplace

template <class T>
template <class... Args>
void queueThreadSafe<T>::emplace(Args && ... args)
{
    unique_lock<mutex> lck(m_mtx);
    m_queue.emplace(args ...);
    lck.unlock();
    m_cv.notify_one();
}

// 8 --> empty

template <class T>
bool queueThreadSafe<T>::empty() const
{
    unique_lock<mutex> lck(m_mtx);
    bool val = m_queue.empty();

    return val;
}

// 9 --> size

template <class T>
unsigned int queueThreadSafe<T>::size() const
{
    unique_lock<mutex> lck(m_mtx);
    unsigned int val = m_queue.size();

    return val;
}

// 10 --> swap

template <class T>
void queueThreadSafe<T>::swap(queueThreadSafe & other) noexcept
{
    unique_lock<mutex> lck(m_mtx);
    m_queue.swap(other);
}

// 11 --> operator =

template <class T>
queueThreadSafe<T> & queueThreadSafe<T>::operator = (const queueThreadSafe<T> & other)
{
    unique_lock<mutex> lck(m_mtx);
    this->m_queue = other.m_queue;

    return *this;
}

// 12 --> operator ==

template<typename T>
bool queueThreadSafe<T>::operator == (const queueThreadSafe<T> & other) const
{
    unique_lock<mutex> lck(m_mtx);
    bool bval = (this->m_queue == other.m_queue);

    return bval;
}

#endif // QUEUE_THREAD_SAFE_DEF_H

// end
