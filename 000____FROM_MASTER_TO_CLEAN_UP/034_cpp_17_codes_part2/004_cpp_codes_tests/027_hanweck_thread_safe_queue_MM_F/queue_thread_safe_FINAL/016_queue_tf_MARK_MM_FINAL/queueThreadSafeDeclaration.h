//=============================//
// queueThreadSafe declaration //
//=============================//

#ifndef QUEUE_THREAD_SAFE_DECL_H
#define QUEUE_THREAD_SAFE_DECL_H

#include <queue>
#include <mutex>
#include <condition_variable>

using std::queue;
using std::mutex;
using std::condition_variable;

// class queueThreadSafe

template <class T>
class queueThreadSafe {

public:

    // member functions

    void push(const T &);
    void push(T &&);
    T pop();
    bool empty() const;
    unsigned int size() const;

    template <class ... Args>
    void emplace(Args && ... args);

private:

    queue<T> m_queue;
    mutable mutex m_mtx;
    condition_variable m_cv;
};

#endif // QUEUE_THREAD_SAFE_DECL_H
