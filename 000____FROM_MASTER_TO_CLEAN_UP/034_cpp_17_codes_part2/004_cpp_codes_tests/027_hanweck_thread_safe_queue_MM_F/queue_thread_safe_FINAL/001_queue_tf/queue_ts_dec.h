//============================//
// queue_ts class declaration //
//============================//

#ifndef QUEUE_TS_DEC_H
#define QUEUE_TS_DEC_H

#include <queue>
#include <mutex>

using std::queue;
using std::mutex;

template <class T>
class queue_ts : public queue<T> {

public:

    void push(const T &);
    void push(T &);

private:

    mutex mtx;  // mutex for critical section
};

#endif // QUEUE_TS_DEC_H

// end
