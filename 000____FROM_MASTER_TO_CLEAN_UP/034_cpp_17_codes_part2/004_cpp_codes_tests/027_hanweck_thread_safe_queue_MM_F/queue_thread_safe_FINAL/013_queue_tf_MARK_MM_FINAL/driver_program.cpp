//===================================//
// driver program to queueThreadSafe //
//===================================//

#include "queueThreadSafe.h"
#include <iostream>
#include <thread>
#include <iomanip>

using std::endl;
using std::cout;
using std::thread;
using std::boolalpha;

// function: doWork

void doWork(const int & DIM, queueThreadSafe<int> q)
{
    // push elements

    cout << " --> 1 --> push elements" << endl;

    for (int i = 0; i != DIM; i++) {
        q.push(i);
    }

    cout << " --> 2 --> size of queue = " << q.size() << endl;

    // emplace elements

    cout << " --> 3 --> emplace elements" << endl;

    for (int i = 0; i != DIM; i++) {
        q.emplace(i);
    }

    cout << " --> 4 --> size of queue = " << q.size() << endl;

    // back

    cout << " --> 5 --> q.back() = " << q.back() << endl;

    // front

    cout << " --> 6 --> q.front() = " << q.front() << endl;

    // pop elements

    cout << " --> 7 --> pop elements" << endl;

    unsigned int SIZE_LOC = q.size();

    for (unsigned int i = 0; i != SIZE_LOC; i++) {
        q.pop();
    }

    cout << " --> 8 --> size of queue = " << q.size() << endl;
}

// the main function

int main()
{
    // local parameters

    const int DIM = 4;
    const int NT = 10;

    // local variables

    queueThreadSafe<int> q1;

    // adjust the output field

    cout << boolalpha;

    // threads array

    thread * th = new thread [NT];

    // spawn threads

    for (int i = 0; i != NT; i++) {
        th[i]= thread(doWork, DIM, q1);
    }

    // join threads

    for (int i = 0; i != NT; i++) {
        th[i].join();
    }

    // q2

    queueThreadSafe<int> q2;

    for (int i = 0; i != DIM; i++) {
        q2.push(i);
    }

    // q3

    queueThreadSafe<int> q3(q2);

    cout << " --> q2.size() = " << q2.size() << endl;
    cout << " --> q3.size() = " << q3.size() << endl;

    // set q3 equal to q1

    q1 = q3;

    cout << " --> q1.size() = " << q1.size() << endl;
    cout << " --> q3.size() = " << q3.size() << endl;

    // test if they are equal;

    cout << " --> (q1 == q3) = " << (q1 == q3) << endl;

    // delete threads array

    delete [] th;

    return 0;
}

// end
