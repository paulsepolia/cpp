//============================//
// queue_ts class declaration //
//============================//

#ifndef QUEUE_TS_DEC_H
#define QUEUE_TS_DEC_H

#include <queue>
#include <mutex>
#include <condition_variable>

using std::queue;
using std::mutex;
using std::condition_variable;

// class queue_ts

template <class T>
class queue_ts : public queue<T> {

public:

    // construnctor

    queue_ts();

    // copy construnctor

    queue_ts(const queue_ts &);

    // member functions

    void push(const T &);
    void push(T &);
    void pop();
    T back();
    T front();
    bool empty() const;
    unsigned int size() const;
    void swap(queue_ts &) noexcept;

    template <class ... Args>
    void emplace(Args && ... args);

private:

    mutable mutex mtx;
    condition_variable cv;
};

#endif // QUEUE_TS_DEC_H

// end
