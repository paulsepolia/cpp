//===========================//
// queue_ts class definition //
//===========================//

#ifndef QUEUE_TS_DEF_H
#define QUEUE_TS_DEF_H

#include "queue_ts_dec.h"

#include <iostream>
#include <queue>
#include <mutex>

using std::queue;
using std::mutex;
using std::cout;
using std::endl;
using std::unique_lock;

// 1 --> constructor

template <class T>
queue_ts<T>::queue_ts() {};

// 2 --> push # 1

template <class T>
void queue_ts<T>::push(const T & val)
{
    this->mtx.lock();
    this->queue<T>::push(val);
    this->mtx.unlock();
}

// 3 --> push # 2

template <class T>
void queue_ts<T>::push(T & val)
{
    this->mtx.lock();
    this->queue<T>::push(val);
    this->mtx.unlock();
}

// 4 -> copy constructor

template <class T>
queue_ts<T>::queue_ts(const queue_ts & q)
{
    for (unsigned int i = 0; i != this->size(); i++) {
        this->pop();
    }

    queue<T> qTmp(q); // HERE THERE IS A PROBLEM

    for (unsigned int i = 0; i != q.size(); i++) {
        this->push(qTmp.front());
        qTmp.pop();
    }
}

// 5 --> pop

template <class T>
void queue_ts<T>::pop()
{
    this->mtx.lock();
    this->queue<T>::pop();
    this->mtx.unlock();
}

// 6 --> back

template <class T>
T  queue_ts<T>::back()
{
    unique_lock<mutex> lck(this->mtx);
    T val = this->queue<T>::back();
    return val;
}

// 7 --> front

template <class T>
T  queue_ts<T>::front()
{
    unique_lock<mutex> lck(this->mtx);
    T val = this->queue<T>::front();
    return val;
}

// 8 --> emplace

template <class T>
template <class... Args>
void queue_ts<T>::emplace(Args && ... args)
{
    this->mtx.lock();
    this->queue<T>::emplace(args ...);
    this->mtx.unlock();
}

// 9 --> empty

template <class T>
bool queue_ts<T>::empty() const
{
    unique_lock<mutex> lck(this->mtx);
    bool val = this->queue<T>::empty();
    return val;
}

// 10 --> size

template <class T>
unsigned int queue_ts<T>::size() const
{
    unique_lock<mutex> lck(this->mtx);
    unsigned int val = this->queue<T>::size();
    return val;
}

// 11 --> swap

template <class T>
void queue_ts<T>::swap(queue_ts & other) noexcept
{
    this->mtx.lock();
    this->queue<T>::swap(other);
    this->mtx.unlock();
}

#endif // QUEUE_TS_DEF_H

// end

