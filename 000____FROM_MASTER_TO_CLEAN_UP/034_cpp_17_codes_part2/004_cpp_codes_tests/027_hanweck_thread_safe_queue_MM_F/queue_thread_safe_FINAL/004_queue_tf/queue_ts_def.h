//===========================//
// queue_ts class definition //
//===========================//

#ifndef QUEUE_TS_DEF_H
#define QUEUE_TS_DEF_H

#include "queue_ts_dec.h"

#include <iostream>
#include <queue>
#include <mutex>
using std::queue;
using std::mutex;
using std::cout;
using std::endl;

// define

std::mutex mtx;

// 1 --> constructor

template <class T>
queue_ts<T>::queue_ts() {};

// 2 --> push # 1

template <class T>
void queue_ts<T>::push(const T & val)
{
    mtx.lock();
    this->queue<T>::push(val);
    mtx.unlock();
}

// 3 --> push # 2

template <class T>
void queue_ts<T>::push(T & val)
{
    mtx.lock();
    this->queue<T>::push(val);
    mtx.unlock();
}

// 4 -> copy constructor

template <class T>
queue_ts<T>::queue_ts(const queue<T> & q)
{
    mtx.lock();

    for (int i = 0; i != this->size(); i++) {
        this->pop();
    }

    queue<T> qTmp(q);

    for (int i = 0; i != q.size(); i++) {
        this->push(qTmp.front());
        qTmp.pop();
    }

    mtx.unlock();
}

// 5 --> pop

template <class T>
void queue_ts<T>::pop()
{
    mtx.lock();
    this->queue<T>::pop();
    mtx.unlock();
}

// 6 --> back

template <class T>
T  queue_ts<T>::back()
{
    mtx.lock();
    T val = this->queue<T>::back();
    mtx.unlock();
    return val; // HERE THERE IS A PROBLEM # 1
}

// 7 --> front

template <class T>
T  queue_ts<T>::front()
{
    mtx.lock();
    T val = this->queue<T>::front();
    mtx.unlock();
    return val; // HERE THERE IS A PROBLEM # 2
}

// 8 --> emplace

template <class T>
template <class... Args>
void queue_ts<T>::emplace(Args && ... args)
{
    mtx.lock();
    this->queue<T>::emplace(args ...);
    mtx.unlock();
}

// 9 --> empty

template <class T>
bool queue_ts<T>::empty() const
{
    mtx.lock();
    bool val = this->queue<T>::empty();
    mtx.unlock();
    return val; // HERE THERE IS A PROBLEM # 3
}

// 10 --> size

template <class T>
unsigned int queue_ts<T>::size() const
{
    mtx.lock();
    unsigned int val = this->queue<T>::size();
    mtx.unlock();
    return val; // HERE THERE IS A PROBLEM # 4
}

// 11 --> swap

template <class T>
void queue_ts<T>::swap(queue_ts & other) noexcept
{
    mtx.lock();
    this->queue<T>::swap(other);
    mtx.unlock();
}

#endif // QUEUE_TS_DEF_H

// end

