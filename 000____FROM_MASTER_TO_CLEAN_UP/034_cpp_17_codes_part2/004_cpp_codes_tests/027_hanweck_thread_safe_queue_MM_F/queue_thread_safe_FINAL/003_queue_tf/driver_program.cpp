//============================//
// driver program to queue_tf //
//============================//

#include "queue_ts.h"
#include <iostream>
#include <thread>

using std::endl;
using std::cout;
using std::thread;

// doWorkA function

void doWorkA(const int & DIM, queue_ts<int> q)
{
    // push elements

    for (int i = 0; i != DIM; i++) {
        q.push(i);
        cout << " --> W1 --> q.size() = " << q.size() << endl;
    }

    // back

    cout << " --> W2 --> q.back() = " << q.back() << endl;

    // front

    cout << " --> W3 --> q.front() = " << q.front() << endl;

    // pop elements

    for (unsigned int i = 0; i != q.size(); i++) {
        q.pop();
        cout << " --> W4 --> popping ... --> q.size() = " << q.size() << endl;
    }
}

// the main function

int main()
{
    // local parameters

    const int DIM = 4;

    // local variables

    queue_ts<int> q1;

    thread th1(doWorkA, DIM, q1);
    thread th2(doWorkA, DIM, q1);

    th1.join();
    th2.join();

    return 0;
}

// end
