//============================//
// driver program to queue_tf //
//============================//

#include "queue_ts.h"
#include <iostream>
#include <thread>

using std::endl;
using std::cout;
using std::thread;

// doWorkA function

void doWorkA(const int & DIM, queue_ts<int> q)
{
    // push elements

    for (int i = 0; i != DIM; i++) {
        q.push(i);
        cout << " --> W1 --> q.size() = " << q.size() << endl;
    }

    // emplace elements

    for (int i = 0; i != DIM; i++) {
        cout << " --> W2A --> q.size() = " << q.size() << endl;
        q.emplace(i);
        cout << " --> W2B --> q.size() = " << q.size() << endl;
    }

    // back

    cout << " --> W2 --> q.back() = " << q.back() << endl;

    // front

    cout << " --> W3 --> q.front() = " << q.front() << endl;

    // pop elements

    for (unsigned int i = 0; i != q.size(); i++) {
        q.pop();
        cout << " --> W4 --> popping ... --> q.size() = " << q.size() << endl;
    }

    // swap

    queue_ts<double> q1;
    queue_ts<double> q2;

    for (int i = 0; i != DIM; i++) {
        q1.push(i);
        q2.push(DIM-i);
    }

    cout << " --> BEFORE" << endl;

    cout << " --> q1.front() = " << q1.front() << endl;
    cout << " --> q2.front() = " << q2.front() << endl;
    cout << " --> q1.back() = " << q1.back() << endl;
    cout << " --> q2.back() = " << q2.back() << endl;

    q1.swap(q2);

    cout << " --> AFTER" << endl;

    cout << " --> q1.front() = " << q1.front() << endl;
    cout << " --> q2.front() = " << q2.front() << endl;
    cout << " --> q1.back() = " << q1.back() << endl;
    cout << " --> q2.back() = " << q2.back() << endl;
}

// the main function

int main()
{
    // local parameters

    const int DIM = 4;

    // local variables

    queue_ts<int> q1;

    // spawn threads

    thread th1(doWorkA, DIM, q1);
    thread th2(doWorkA, DIM, q1);
    thread th3(doWorkA, DIM, q1);
    thread th4(doWorkA, DIM, q1);

    // join threads

    th1.join();
    th2.join();
    th3.join();
    th4.join();

    return 0;
}

// end
