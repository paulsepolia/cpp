
# 1. I used the STL queue<T> and I produced a thread-safe version of it

# 2. To do so, I defined thread-safe versions of 
     "push", "pop", "empty", "size" and "emplace" functions

# 3. To make the functions thread-safe I lock the entire scope of each function
     using the "unique_lock". 

     (a) In case of the two versions of "push" and "emplace",
         I manually release the lock of the scope after the "push"/"emplace" operation
         when it is safe to do so, to reduce the amount of time the threads are locked
         (reduce lock contention), and I inform/notify one thread to wake up.
 
     (b) In case of "pop" function, I lock the scope using "unique_lock",
	    then I check if the queue is empty, if yes I push the thread to wait,
         if not then I get the first in the queue element and then I pop it
         and return it by value.

     (c) In case of the functions "size" and "empty" I lock the entire scope
         and return by value.

	Pavlos G. Galiatsatos
         
