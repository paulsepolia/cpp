
# 1. The main goal is to have only one function to
	add and delete elements from the queue.
	For that purpose the "push" functions adds elements
     to the end of the queue and removes the popped elements
     from the begining of the queue.
	So if only one thread calls the "push" function
	and the other thread reads (pops) elements using the "pop" function
     the queue is a single-producer-single-consumer-lock-free-queue.

	Pavlos G. Galiatsatos
  
