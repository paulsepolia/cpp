
#ifndef QUEUE_SPSC_DECL_H
#define QUEUE_SPSC_DECL_H

#include "queueSPSCDecl.h"
#include <list>

using std::list;

#include <iostream>
using std::cout;
using std::endl;

// constructor

template <typename T>
queueSPSC<T>::queueSPSC()
{
    m_list.push_back(T());
    m_first = m_list.begin();
    m_last = m_list.end();
}

// push

template <typename T>
void queueSPSC<T>::push(const T & val)
{
    m_list.push_back(val); // push element at the end
    m_last = m_list.end();  // get the position of the element pushed
    m_list.erase(m_list.begin(), m_first); // remove the elements from the beginning popped
}

// pop

template <typename T>
bool queueSPSC<T>::pop(T & val)
{
    typename list<T>::iterator next_loc = m_first; // get the position of the first
    // in the queue element

    ++next_loc; // go to the next to the first

    if (next_loc != m_last) { // compare if the next to the first is the last
        m_first = next_loc;  // if not then next is the first
        val = *m_first;      // get the value
        return true;
    }

    return false;
}

// size

template <class T>
unsigned int queueSPSC<T>::size() const
{
    return m_list.size();
}

#endif // QUEUE_SCSP_DEF_H
