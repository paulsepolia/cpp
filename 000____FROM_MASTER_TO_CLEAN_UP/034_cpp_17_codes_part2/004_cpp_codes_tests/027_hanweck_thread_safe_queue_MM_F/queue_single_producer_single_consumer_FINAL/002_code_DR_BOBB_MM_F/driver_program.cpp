
#include <iostream>
#include <thread>
#include <list>

using std::cout;
using std::endl;
using std::thread;
using std::list;

// class LockFreeQueue

template <class T>
class queueLockFree {
public:

    // constructor

    queueLockFree<T>();

    // member functions

    void push(const T &);
    bool pop(T &);

private:

    list<T> m_list;
    typename list<T>::iterator m_head;
    typename list<T>::iterator m_tail;
};

// functions definition

template <class T>
queueLockFree<T>::queueLockFree()
{
    m_list.push_back(T());
    m_head = m_list.begin();
    m_tail = m_list.end();
}

template <class T>
void queueLockFree<T>::push(const T & val)
{
    m_list.push_back(val);
    m_tail = m_list.end();
    m_list.erase(m_list.begin(), m_head);
}

template <class T>
bool queueLockFree<T>::pop(T & val)
{
    typename list<T>::iterator next_loc = m_head;

    ++next_loc;

    if (next_loc != m_tail) {
        m_head = next_loc;
        val = * m_head;
        return true;
    }

    return false;
}

// the main function

int main()
{
    const int DIM = 40000000;
    queueLockFree<int> q1;
    int val = 0;

    for (int i = 0; i != DIM; i++) {
        q1.push(i);
        q1.pop(val);
    }

    cout << " --> val = " << val << endl;
}

// end
