
#ifndef QUEUE_SPSC_DECL_H
#define QUEUE_SPSC_DECL_H

#include "queueSPSCDecl.h"
#include <list>

using std::list;

#include <iostream>
using std::cout;
using std::endl;

// constructor

template <class T>
queueSPSC<T>::queueSPSC()
{
    m_list.push_back(T());
    m_first = m_list.begin();
    m_last = m_list.end();
}

// push

template <class T>
void queueSPSC<T>::push(const T & val)
{
    m_list.push_back(val);
    m_last = m_list.end();
    m_list.erase(m_list.begin(), m_first);
}

// pop

template <class T>
bool queueSPSC<T>::pop(T & val)
{
    typename list<T>::iterator next_loc = m_first;

    ++next_loc;

    if (next_loc != m_last) {
        m_first = next_loc;
        val = *m_first;
        return true;
    }

    return false;
}

// size

template <class T>
unsigned int queueSPSC<T>::size() const
{
    return m_list.size();
}

#endif // QUEUE_SCSP_DEF_H
