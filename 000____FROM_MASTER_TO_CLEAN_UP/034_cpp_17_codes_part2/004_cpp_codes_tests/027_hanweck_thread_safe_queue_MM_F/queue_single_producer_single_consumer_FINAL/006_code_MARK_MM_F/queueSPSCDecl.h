#ifndef QUEUE_SPSC_DEF_H
#define QUEUE_SPSC_DEF_H

#include <list>
using std::list;

// class queueSCSP

template <class T>
class queueSPSC {
public:

    // constructor

    queueSPSC<T>();

    // member functions

    void push(const T &);
    bool pop(T &);
    unsigned int size() const;

private:

    list<T> m_list;
    typename list<T>::iterator m_first;
    typename list<T>::iterator m_last;
};

#endif // QUEUE_SPSC_DEF_H
