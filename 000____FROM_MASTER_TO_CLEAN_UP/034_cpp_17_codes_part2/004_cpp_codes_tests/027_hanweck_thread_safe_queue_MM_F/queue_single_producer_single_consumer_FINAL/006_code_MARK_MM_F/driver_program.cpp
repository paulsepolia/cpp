//=============================//
// driver program to queueSPSC //
//=============================//

#include "queueSPSC.h"
#include <iostream>
#include <thread>

using std::cout;
using std::endl;
using std::thread;

// function doWork

void doWork(const int & DIM, int id)
{
    queueSPSC<int> q1;
    int val = 0;

    cout << " 1 --> q1.size() = " << q1.size() << endl;

    if (id == 1 || id == 2) {
        // push elements

        for (int i = 0; i != DIM; i++) {
            cout << " 2 --> q1.size() = " << q1.size() << endl;
            q1.push(i);
        }
    }

    if (id == 2) {
        // pop elements

        for (int i = 0; i != DIM; i++) {
            cout << " 3 --> q1.size() = " << q1.size() << endl;
            q1.pop(val);
            cout << " 4 --> val = " << val << endl;
        }
    }
}

// the main function

int main()
{
    const int DIM = 4;
    const int NT = 2;

    // threads array

    thread * th = new thread [NT];

    // spawn threads

    for (int i = 0; i != NT; i++) {
        th[i]= thread(doWork, DIM, i+1);
    }

    // join threads

    for (int i = 0; i != NT; i++) {
        th[i].join();
    }

    // delete threads array

    delete [] th;
}

// end
