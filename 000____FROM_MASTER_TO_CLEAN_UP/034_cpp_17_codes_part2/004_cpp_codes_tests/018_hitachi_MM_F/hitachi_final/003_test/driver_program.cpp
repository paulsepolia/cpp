
#include <iostream>    // cout
#include <string>
#include <iomanip>
#include <vector>
#include <algorithm>

using namespace std;

// Purpose:

// The purpose of the code is to return "true"
// when the input unsigned int "n" is a power of "2".
// So, it returns "true" when n = 1,2,4,8,16,32,64,128,256 ... etc
// That is done comparing bit-by-bit using the AND binary operator "&",
// the "n" and the "n-1" values.
// When the "n" is of power of "2" then there are NO corresponing "1"
// in the binary representations of "n" and "n-1",
// since the binary patterns look like the following:

// for n = 1

// n       = 1,  0 0 0 0 0 1   numerical value 2 to the power of 0
// n-1     = 0,  0 0 0 0 0 0   numerical value 0
// n&(n-1) = 0,  0 0 0 0 0 0   equals 0


// for n = 2

// n       = 2, 0 0 0 0 1 0   numerical value 2 to the power of 1
// n-1     = 1, 0 0 0 0 0 1   numerical value 1
// n&(n-1) = 0, 0 0 0 0 0 0   equals 0


// for n = 4

// n       = 4, 0 0 0 1 0 0   numerical value 2 to the power of 2
// n-1     = 3, 0 0 0 0 1 1   numerical value 3
// n&(n-1) = 0, 0 0 0 0 0 0   equals 0


// for n = 8

// n       = 8, 0 0 1 0 0 0   numerical value 2 to the power of 3
// n-1     = 7, 0 0 0 1 1 1   numerical value 7
// n&(n-1) = 0, 0 0 0 0 0 0   equals 0

// etc

// in any other case there are "corresponding" "1s" so the result in not "000000000000"
// so it is not equal to "0" so it is false

// e.g.

// for n = 7

// n   = 7, 0 0 0 1 1 1   numerical value 7
// n-1 = 6, 0 0 0 1 1 0   numerical value 6

// etc

// Problems

// When the input is 0, then the (n-1) is unsigned int(-1) = UINT_MAX, so

// for n = 0

// n       = 0,         all are zeros
// n-1     = UINT_MAX,  all are ones
// n&(n-1) = 0,         all are zeros
// so the program reports that "0" is a power of "2",
// which is not correct.


// Notes:

// I did not used any book.
// Time taken: 10 minutes.

bool f(unsigned int n)
{
    return (n&(n-1)) == 0;
}

int main()
{

    cout << boolalpha;

    unsigned int i;

    for (i = 0; i < 2; ++i) {

        cout << " -->  " << i << " -->" << f(-i) << endl;
    }

    i = 0;

    cout << i-1 << endl;
    return 0;

}

// FINI
