
#include <iostream>    // cout
#include <string>
#include <iomanip>
#include <vector>
#include <algorithm>
#include <climits>

using namespace std;

unsigned int f(unsigned int n)
{
    return -n&7;
}

// Purpose:

// The purpose of the code is to return the result
// of the subtraction of the "module(input,8)" from "8".

// Problems:

// If the input is divided evenly by "8" then the result is "0" and not "8"
// An equivalent function is the following:
//
// unsigned int f2(unsigned int i)
// {
//     return ((8-i%8 < 8) ? (8-i%8) : 0);
// }

// Another thing to note is the use of (-n).
// "n" is unsigned int, so "-n" becomes "UINT_MAX-(n-1)"
// The behaviour of overflowed/underflowed unsigned int is well defined
// so there is no problem.

// Notes:
// I did not use any book. I searced on internet.
// Time taken: 30 minutes

int main()
{
    cout << boolalpha;
    unsigned int k = 0;

    cout << f(1) << endl;
    for (unsigned int i = -2000; i != 200000; ++i) {
//          cout << " i = " << i << " --> " << f(i) << endl;
//		cout << " i = " << i << " --> " << f2(i) << endl;
//          if (f(i)-f2(i)) {
//               cout << "false" << endl;
//          }
        //cout << f(i) - f2(i) << endl;

//		if (((k+1)*8)%7 == 0)
//		{
//			k++;
//			cout << " new = " << k*8-i << endl;
//		}
    }

    cout << " max = " << UINT_MAX << endl;
    k = -1;
    cout << " k = -1 " << k << endl;
    k = -2;
    cout << " k = -2 " << k << endl;
    k = -3;
    cout << " k = -3 " << k << endl;
    k = -4;
    cout << " k = -4 " << k << endl;





    return 0;
}

// FINI
