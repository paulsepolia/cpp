
#include <iostream>    // cout
#include <string>
#include <iomanip>
#include <vector>
#include <algorithm>

using namespace std;

bool f(int x)
{
    return !((x&7));
}

bool f2(int x)
{
    return !(x%8);
}

// Purpose:

// The purpose of the code is to return "true"
// if the input integer is divided evenly by "8"
// and "false" otherwise.

// Problems:

// There are no potential problems.

// Notes:

// I did not used any book.
// Time taken 10 minutes.

int main()
{
    cout << boolalpha;

    for (int i = -1000000; i != 10000000; ++i) {
        //    cout << " i = " << i << " -->" << f(i) << endl;
        //    cout << " i = " << i << " -->" << f2(i) << endl;

        if (f(i)!=f2(i)) {
            cout << "ERROR = " << i << endl;
            break;
        }
    }

    cout << !bool(0%8) << endl;
    cout << !bool(0&7) << endl;

//	cout << bool(-16%8) << endl;
    return 0;
}

// FINI
