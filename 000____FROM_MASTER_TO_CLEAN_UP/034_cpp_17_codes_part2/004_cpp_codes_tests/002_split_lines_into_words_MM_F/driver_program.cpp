
#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <regex>
#include <iterator>

using std::cout;
using std::cin;
using std::endl;
using std::vector;
using std::string;
using std::ifstream;
using std::regex;
using std::sregex_token_iterator;
using std::ostream_iterator;

// the main function

int main()
{
    // input variables

    const string fname = "input.txt"; // name of file
    string line;
    string word;
    ifstream ifs;
    regex rgx("\\s+");
    int count_words = 0;
    vector<int> vecA;
    vector<int> vecD;

    // open the file

    ifs.open(fname);

    // read line by line and count how many words has each line

    while(getline(ifs, line)) { // get each line

        // split each line into words and count them

        if(!line.empty()) {
            sregex_token_iterator iter(line.begin(),
                                       line.end(),
                                       rgx,
                                       -1);

            sregex_token_iterator end;

            for ( ; iter != end; ++iter) {
                count_words++;
            }

            vecA.push_back(count_words); // put the number in vecA
        } else {
            count_words = 0;
            vecA.push_back(count_words); // put the number in vecA
        }

        count_words = 0;
    }

    // close file

    ifs.close();

    // get the number of lines

    unsigned int lines_num = vecA.size();

    // open file

    ifs.open(fname);

    // declare a pointer to a vector
    // each vector contais the words of each line

    vector<int> * vecB = new vector<int> [lines_num];

    // build the vectors-lines

    for (unsigned int i = 0; i != lines_num; ++i) {
        for (int j = 0; j != vecA[i]; ++j) {
            int num;
            ifs >> num;
            vecB[i].push_back(num);
        }
    }


    // output words of each line

    for (unsigned int i = 0; i != lines_num; ++i) {
        unsigned int DIM = vecB[i].size();

        for (unsigned int j = 0; j != DIM; ++j) {
            cout << vecB[i][j];
            vecD.push_back(*min_element(vecB[i].begin(), vecB[i].end()));
            if (j != DIM-1)
                cout << " ";
        }

        cout << endl;
    }

    ostream_iterator<int> out_it(cout, "\n");

    copy(vecD.begin(), vecD.end(), out_it);

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}
