
#include<iostream>
#include<map>

using std::endl;
using std::cout;
using std::map;
using std::multimap;
using std::pair;

class mcla {
public:

    multimap<int, int> m1;
};

class mstruct {
public:

    multimap<int, int> m1;
};

// the main function

int main()
{

    mcla A1;
    mcla A2;
    multimap<int, int>::iterator p;
    const int DIM = 10000;

    for (int i = 0; i != DIM; ++i) {
        A1.m1.insert(pair<int, int>(i, i));
    }

    for (int i = 0; i != DIM; ++i) {
        A2.m1.insert(pair<int, int>(i, i));
    }


    for (p = A1.m1.begin(); p != A1.m1.end(); p++) {
        cout << " i : " << p->first << ", j : " <<  p->second << endl;
    }

    for (p = A2.m1.begin(); p != A2.m1.end(); p++) {
        cout << " i : " << p->first << ", j : " <<  p->second << endl;
    }


    cout << endl;

    return 0;
}

// END
