
// Write a line of code that assigns a variable of type float
// the maximum value that can be stored
// in its type, using facilities provided by the STL

#include <iostream>
#include <limits>

using std::cout;
using std::endl;
using std::numeric_limits;

int main ()
{
    float max_val;

    max_val = numeric_limits<float>::max();

    cout << "Maximum value of type float is " << max_val << endl;

    return 0;
}

// END
