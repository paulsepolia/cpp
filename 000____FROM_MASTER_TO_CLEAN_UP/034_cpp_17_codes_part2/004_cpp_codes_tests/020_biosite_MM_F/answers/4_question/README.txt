
(a) 	Describe the difference between a std::list and a std::vector.

	1.

	std::vector is a random access container. You can access any alement in O(1) time.
     std::list is a not a random access container. You need to access 
	all the elements from the head in order to reach the desired element.
	So you need O(N) time to access a given element.
 
	2.
 
	std::vector stores its element in consecutive positions in RAM, so you can use
	pointer arithmetic to access them. The access is very fast and uses minimal RAM.
     std::list does not store its element in consecutive positions in RAM.
	Each elemnt of a list points to the next element. This makes the list to consume a lot of RAM.

(b) 	In what situation might it be better to use a std::list rather than a std::vector.

	1.
	
	The only situation where std::list is superior to std::vector is when you need to
	insert and/or earse at inner locations very frequently. Because list needs constant time O(1)
	for that type of operations. std::vector is slow when you need to insert/erase an element
	at inner locations because you need to replace (copy to tmp loacation and then back)
     the whole sequence of elements after the element that needs to be inserted/removed.

(c) 	In what situation might it be better to use a std::set rather than another type of container.

	1.

	When you want a container that stores only unique elements in a specific order 
	(given by the developer).

(d) 	What design pattern, commonly used in the STL, facilitates the use of standard algorithms such as
	std::find and std::fill.

	1.

	The desing pattern in use in STL algorithms such as std::find and std::fill
     is the "iterator"

(e) 	Write a line of code that assigns a variable of type float the maximum value that can be stored
	in its type, using facilities provided by the STL.

     1.

	Is writen in the driver_programm.cpp file

