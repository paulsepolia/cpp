
#include <iostream>
#include <chrono>
#include <random>
#include <numeric>
#include <iomanip>

using std::cout;
using std::cin;
using std::endl;
using std::default_random_engine;
using std::uniform_real_distribution;
using std::accumulate;
using std::fixed;
using std::setprecision;

//=========================================================//
// Class: arrayOneTen                                      //
//											    //
// Works only for floating point types: float, double, etc //
//											    //
// The objects are arrays of floating point types          //
// of the given size (the default size is ten).            //
//											    //
// The elements are initialized to be in the range from    //
// one to ten.									    //
//=========================================================//

template <class T>
class arrayOneTen {
public:

    // default constructor

    arrayOneTen() : dim (10)
    {
        p = new T[10];
        unsigned int seed = std::chrono::system_clock::now().time_since_epoch().count();
        default_random_engine gen (seed);
        uniform_real_distribution<T> dist (1,10);

        for (unsigned int i = 0; i != dim; i++) {
            p[i] = dist(gen);
        }
    }

    // construnctor with argument

    arrayOneTen(unsigned int dim_loc) : dim (dim_loc), p(new T[dim_loc])
    {
        unsigned int seed = std::chrono::system_clock::now().time_since_epoch().count();
        default_random_engine gen (seed);
        uniform_real_distribution<T> dist (1,10);

        for (unsigned int i = 0; i != dim; i++) {
            p[i] = dist(gen);
        }
    }

    // operator []

    T operator [](const int & index) const
    {
        return this->p[index];
    }

    // sum

    T sum() const
    {
        return accumulate(this->p, this->p+this->dim, 0.0);
    }

    // destructor

    virtual ~arrayOneTen()
    {
        if (p != 0) {
            delete [] p; // avoid heap memory leaks
            p = 0;
        }
    }

private:

    unsigned int dim;
    T * p;
};

// the main program

int main()
{
    const unsigned int DIM = 5;
    cout << fixed;
    cout << setprecision(6);

    arrayOneTen<float> a1; // this is the array of ten floats in the range from one to ten
    arrayOneTen<double> a2(DIM); // this is the array of DIM doubles in the range from one to ten

    // output --> 1

    for (unsigned int i = 0; i != 10; i++) {
        cout << i << " --> " << a1[i] << endl;
    }

    // output --> 2

    cout << " total sum is --> " << a1.sum() << endl;

    // output --> 3

    for (unsigned int i = 0; i != DIM; i++) {
        cout << i << " --> " << a2[i] << endl;
    }

    // output --> 4

    cout << " total sum is --> " << a2.sum() << endl;

    return 0;
}

// end
