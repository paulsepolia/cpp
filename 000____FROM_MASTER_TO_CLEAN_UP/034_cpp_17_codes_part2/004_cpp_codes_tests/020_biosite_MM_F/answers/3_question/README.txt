
(a)  What do you expect the output of the program to be?
	
	We evaluate the average value of the sum
   	of the ten floats {1,2,3,4,5,6,7,8,9,10} which is 5.5 
   	according to the general form (1/2*n*(1+n))/n = 1/2*(1+n)
   	where n is the total number of elements.

(b)  Although this program appears to work correctly, it leaks memory, why? 

	It leaks memory because the base class destructor is not declared virtual
	and the base class type pointer p, when is being deleted,
     never calls the derived destructor ~my_derived_class. Calls only 
	the base destructor ~my_base_class. But the deallocation happens in the
	derived destructor. So, there is a memory leak.

(c)  How would you fix this program so that it performs the correct cleanup?

	I added the virtual keyword to the base destructor ~my_base_class.

(d) 	The main function in this program explicitly creates the object with new and destroys it with delete,
	what might be a better way to write such a simple code fragment; you do not have to write the code,
	a single sentence answer is sufficient 

	Just replace the declaration statemenet with teh following
	and delete the "delete p;" statement.

     unique_ptr<my_base_class> p(new my_derived_class(N));
