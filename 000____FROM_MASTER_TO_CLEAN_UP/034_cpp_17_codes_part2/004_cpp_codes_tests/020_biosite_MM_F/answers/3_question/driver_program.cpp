
#include <iostream>
#include <memory>

using std::unique_ptr;

class my_base_class {
public:

    my_base_class() : m_store(NULL) { }

    virtual ~my_base_class() { } // always add the virtual keyword
    // when dealing with pointers
    // and inheritance

    void set_element(size_t index, float value)
    {
        m_store[index] = value;
    }

    virtual void process_data() = 0;

protected:

    float* m_store;

};

class my_derived_class : public my_base_class {

public:

    my_derived_class(size_t buffer_size)
    {
        m_store = new float[buffer_size];
        m_buffer_size = buffer_size;
    }

    virtual ~my_derived_class()   // always add virtual keyword when dealing with pointers
    {
        delete [] m_store;       // and inheritance
        // (it is not mandatory here the virtual keyword)
        // (the program does not leak memory without it, but it is a good practice)
    }

    void process_data()
    {
        float total = 0;
        for( size_t i = 0; i < m_buffer_size; ++i )
            total += m_store[i];
        total /= float(m_buffer_size);
        std::cout << "\nThe mean value is: " << total << std::endl;
    }

private:

    size_t m_buffer_size;

};

/*
int main(int argc, char* argv[])
{
     const int N = 10;

     my_base_class* p = new my_derived_class(N);

     for( int n = 0; n < N; ++n )
          p->set_element(n, 1+n);

     p->process_data();

     delete p;

     return 0;
}

*/

// main program with unique_ptr

int main(int argc, char* argv[])
{
    const int N = 10;

    unique_ptr<my_base_class> p( new my_derived_class(N));

    for( int n = 0; n < N; ++n )
        p->set_element(n, 1+n);

    p->process_data();

    return 0;
}
