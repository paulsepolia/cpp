
#include <iostream>
#include <vector>

using std::cout;
using std::endl;

class my_class {
public:
    typedef std::vector<float> storage_type;

    my_class(std::vector<float> v1)
    {
        m_data = &v1;
    }

    void set_data(storage_type* data)
    {
        m_data = data;
    }

    float sum_data()
    {
        float sum = 0;
        for( size_t i = 0;  i < m_data->size(); ++i ) {
            sum += (*m_data)[i];
        }
        return sum;
    }

private:

    std::vector<float>* m_data;
};

int main(int argc, char* argv[])
{
    std::vector<float> data;

    data.push_back(1);
    data.push_back(2);
    data.push_back(4);

    my_class object(data);

    std::cout << "\nThe sum of the data is: " << object.sum_data();
}

// END
