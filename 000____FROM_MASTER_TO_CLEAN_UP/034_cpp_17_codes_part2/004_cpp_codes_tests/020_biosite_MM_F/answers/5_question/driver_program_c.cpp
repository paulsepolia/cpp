
#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;

class my_class {
public:
    typedef vector<float> storage_type;

    my_class() {}

    void set_data(const storage_type & data)
    {
        m_data = data;
    }

    float sum_data()
    {
        float sum = 0;
        for( size_t i = 0;  i < m_data.size(); ++i ) {
            sum += m_data[i];
        }
        return sum;
    }

private:

    std::vector<float> EMPTY = {};
    std::vector<float> & m_data = EMPTY;
};

int main(int argc, char* argv[])
{
    vector<float> data;

    data.push_back(1);
    data.push_back(2);
    data.push_back(4);

    my_class object;

    object.set_data(data);  // works with and without this line

    std::cout << "\nThe sum of the data is: " << object.sum_data();
}

// END
