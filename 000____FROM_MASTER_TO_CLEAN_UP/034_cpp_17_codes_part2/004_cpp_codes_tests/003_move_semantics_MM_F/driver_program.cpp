
#include "memory_block.h"
#include <iostream>
#include <vector>

using std::cout;
using std::cin;
using std::endl;
using std::vector;

// the main function

int main()
{
    // create a vector object and add a few elements to it

    vector<MemoryBlock> v;

    v.push_back(MemoryBlock(25));
    v.push_back(MemoryBlock(75));

    // insert a new element into the second position of the vector

    v.insert(v.begin() + 1, MemoryBlock(50));

    int sentinel;
    //cin >> sentinel;

    return 0;
}

