//======================================//
// Codility ‘Tape Equilibrium’ Solution //
//======================================//

//================================================================//
// Short Problem Definition                                       //
// Minimize the value |(A[0] + … + A[P-1]) – (A[P] + … + A[N-1])| //
//================================================================//

#include <iostream>
#include <algorithm>
#include <climits>
#include <vector>

using std::vector;
using std::min;
using std::cout;
using std::endl;

//========================================//
// solution by http://www.martinkysel.com //
//========================================//

int solution(vector<int> & A)
{
    unsigned int size = A.size();
    vector<long long int> parts;
    parts.reserve(size+1);

    long long last = 0;

    for (unsigned int i = 0; i < size-1; i++) {
        if (i == 0) {
            parts.push_back(A[i]);
        } else {
            parts.push_back(A[i]+parts[i-1]);
        }

        if (i == size-2) {
            last = parts[i]+ A[i+1];
        }
    }

    long long int solution2 = LLONG_MAX;

    for(unsigned int i = 0; i < parts.size(); i++) {
        solution2 = min(solution2,
                        static_cast<long long int>(abs(last - 2 * parts[i])));
    }

    return solution2;
}

// the main function

int main()
{
    vector<int> A( {1,2,3,4,});
    int sol;

    sol = solution(A);

    cout << " sol = " << sol << endl;

    return 0;
}

// end
