//================//
// HEAVY INTEGERS //
//================//

#include <iostream>
#include <string>

using std::endl;
using std::cout;
using std::string;
using std::to_string;

// the solution

int solution(int A, int B)
{
    // local parameter

    const double SEVEN = 7.00;
    const int NT = 4;

    // local variables

    string s;
    int DIM;
    double sum;
    int k = 0;
    int i;
    int j;

    // main loop

    #pragma omp parallel default(none)\
    num_threads(NT)\
    private(i)\
    private(j)\
    private(sum)\
    private(DIM)\
    private(s)\
    shared(A)\
    shared(B)\
    reduction(+:k)
    {
        #pragma omp for

        for (i = A; i <= B; i++) {
            s = to_string(i);
            DIM = s.length();
            sum = 0.0;

            for (j = 0; j != DIM; j++) {
                sum = sum + s[j]-'0';
            }

            sum = sum/DIM;

            if (sum > SEVEN) {
                k++;
            }
        }
    }

    return k;
}

// the main function

int main()
{
    int A = 1;
    int B = 100000000;

    int sol = solution(A,B);

    cout << " --> number of heavy integers = " << sol << endl;

    return 0;
}

// END
