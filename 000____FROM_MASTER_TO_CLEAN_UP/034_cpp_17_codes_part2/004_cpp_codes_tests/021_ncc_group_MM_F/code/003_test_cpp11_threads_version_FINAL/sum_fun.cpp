// function definition

#include <iostream>
#include <thread>

using std::this_thread::get_id;
using std::cout;
using std::endl;
using std::hash;

void sum_fun(const double * vA, const double * vB, double * vC,
             const int & DIM, const int & THREADS_NUM, const int & index)
{
    const int i_start = index * (DIM/THREADS_NUM);
    const int i_end = (index+1) * (DIM/THREADS_NUM);

    for (int i = i_start; i < i_end; ++i) {
        vC[i] = vA[i] * vB[i];
    }

    if ((DIM%THREADS_NUM) != 0) {
        for (int i = THREADS_NUM * (DIM/THREADS_NUM); i <  DIM; ++i) {
            vC[i] = vA[i] * vB[i];
        }
    }
}

// end
