//============================//
// parallel dot C++11 threads //
//============================//

#include "sum_fun.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <iomanip>
#include <thread>

using std::cout;
using std::endl;
using std::ifstream;
using std::ios;
using std::vector;
using std::fixed;
using std::setprecision;
using std::thread;

// the main function

int main()
{
    // local parameters

    const int THREADS_NUM = 3; // number of C++11 threads
    const int DIM = 10; // dimension of the vectors

    // local variables

    double * vA = new double [DIM];
    double * vB = new double [DIM];
    double * vC = new double [DIM];
    double sum_loc = 0.0;
    vector<thread> vecThreads;
    int i;

    if (DIM < THREADS_NUM) {
        cout << "ERROR: Number of threads must be less equal to dimension of the vector" << endl;
        return -1;
    }

    // puts some numbers in the vectors
    // put zeros into the vector vC

    for (i = 0; i != DIM; ++i) {
        vA[i] = i;
        vB[i] = i+1;
        vC[i] = 0.0;
    }

    // span threads

    for (i = 0; i != THREADS_NUM; ++i) {
        vecThreads.push_back(thread(sum_fun, vA, vB, vC, DIM, THREADS_NUM, i));
    }

    // join the threads

    for (auto & t : vecThreads) {
        t.join();
    }

    // clear the threads container

    vecThreads.clear();

    // evaluate the sum

    for (i = 0; i != DIM; ++i) {
        sum_loc = sum_loc + vC[i];
    }

    // output

    cout << fixed;
    cout << setprecision(5);

    cout << " dot product = " << sum_loc << endl;

    // free up the heap

    delete [] vA;
    delete [] vB;
    delete [] vC;

    return 0;
}

// end
