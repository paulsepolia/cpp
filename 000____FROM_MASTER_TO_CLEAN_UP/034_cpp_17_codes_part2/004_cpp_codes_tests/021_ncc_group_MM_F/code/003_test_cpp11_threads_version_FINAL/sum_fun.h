#ifndef SUM_FUN_H

// function declaration

void sum_fun(const double *, const double *, double *,
             const int &, const int &, const int &);

// end

#endif // SUM_FUN_H
