//=================================//
// read and write in reverse order //
// ascii files                     //
//=================================//

#include <iostream>
#include <fstream>
#include <vector>

using std::cout;
using std::endl;
using std::ifstream;
using std::ios;
using std::vector;

// the main function

int main()
{
    //  local variables declaration

    ifstream inFile;
    vector<char> vecA;
    char ch;

    // open the unit

    inFile.open("file_input.txt", ios::in);

    // get the first char to test

    inFile.get(ch);
    vecA.push_back(ch);

    // read the file and put each character in the vector

    while(!inFile.eof()) {
        inFile.get(ch);
        vecA.push_back(ch);
    }

    // get the size of the vector

    const long int DIM = static_cast<long int>(vecA.size());

    // write in reverse order into the screen

    for(long int i = DIM-2; i >= 0; --i) {
        cout << vecA[i];
    }

    // close the unit

    inFile.close();

    return 0;
}

// END
