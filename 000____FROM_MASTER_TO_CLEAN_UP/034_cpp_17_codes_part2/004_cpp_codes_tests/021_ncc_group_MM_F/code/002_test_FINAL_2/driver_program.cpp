
#include <iostream>
#include <cstdio>
#include <ctime>

using std::cout;
using std::endl;

// the main function

int main()
{
    time_t now = time(0);

    // Convert now to tm struct for UTC

    tm* gmtm = gmtime(&now);

    if (gmtm != NULL) {
        cout << "The UTC date and time is: " << asctime(gmtm) << endl;
    }

    return 0;
}

// end
