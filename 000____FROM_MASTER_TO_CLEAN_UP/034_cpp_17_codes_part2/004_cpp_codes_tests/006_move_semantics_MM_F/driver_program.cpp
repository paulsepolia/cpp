
#include "memory_block.h"
#include <iostream>
#include <vector>

using std::cout;
using std::cin;
using std::endl;
using std::vector;

// the main function

int main()
{
    // local variables

    vector<MemoryBlock> v;
    const unsigned long int DIM = 10UL;

    // build the vector

    cout << " --> push elements into the vector" << endl;

    for (unsigned long i = 0UL; i != DIM; ++i) {
        cout << " --> push element ------------------------------------------------------>> " << i << endl;
        v.push_back(MemoryBlock(i));
    }

    // insert elements into the vector

    cout << " --> insert elements into the vector" << endl;

    for (unsigned long i = 0UL; i != DIM; ++i) {
        cout << " --> insert element ---------------------------------------------------->> " << i << endl;
        v.insert(v.begin() + 1, MemoryBlock(i));
    }

    // return

    cout << " --> exiting" << endl;

    return 0;
}

