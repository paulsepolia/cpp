
#include <iostream>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <string>
#include <ctime>
#include <iomanip>

using std::cout;
using std::cin;
using std::endl;
using std::vector;
using std::rotate;
using std::rand;
using std::srand;
using std::string;
using std::time;
using std::random_shuffle;
using std::setw;
using std::left;

//==============================//
// local functions declarations //
//==============================//

// # 1 --> build_reel

void build_reel(vector<string> &);

// # 2 --> rotate_reel

void random_rotate_reel(vector<string> &);

//==================//
// the main program //
//==================//

int main()
{
    //==================//
    // local parameters //
    //==================//

    const int DIM_REEL = 12;
    const int I_DO_MAX = 100;
    const int DELIM_LOC = 10;
    const int DIM_VERTICAL = 3;

    // number of elements for each reel

    const int data1 [] = {1,6,5,4,4,4,3,3,2,1,2,4};
    const int data2 [] = {1,6,6,5,4,2,2,3,3,2,2,3};
    const int data3 [] = {1,6,5,5,4,3,3,3,3,2,2,2};
    const int data4 [] = {1,6,6,5,3,3,4,2,3,2,2,2};
    const int data5 [] = {1,4,4,5,4,3,4,4,4,0,3,3};

    // symbols

    const string symbols[] = {"wild", "s1", "s2",
                              "s3", "s4", "s5",
                              "s6", "s7", "s8",
                              "s9", "scatter", "bonus"
                             };

    //=================//
    // local variables //
    //=================//

    // reels declaration and definition as vectors of strings

    vector<string> reel1;
    vector<string> reel2;
    vector<string> reel3;
    vector<string> reel4;
    vector<string> reel5;

    // reel # 1 --> step --> alpha

    for (int i = 0; i != DIM_REEL; i++) {
        for (int j = 0; j != data1[i]; j++) {
            reel1.push_back(symbols[i]);
        }
    }

    // reel # 2 --> step --> alpha

    for (int i = 0; i != DIM_REEL; i++) {
        for (int j = 0; j != data2[i]; j++) {
            reel2.push_back(symbols[i]);
        }
    }

    // reel # 3 --> step --> alpha

    for (int i = 0; i != DIM_REEL; i++) {
        for (int j = 0; j != data3[i]; j++) {
            reel3.push_back(symbols[i]);
        }
    }

    // reel # 4 --> step --> alpha

    for (int i = 0; i != DIM_REEL; i++) {
        for (int j = 0; j != data4[i]; j++) {
            reel4.push_back(symbols[i]);
        }
    }

    // reel # 5 --> step --> alpha

    for (int i = 0; i != DIM_REEL; i++) {
        for (int j = 0; j != data5[i]; j++) {
            reel5.push_back(symbols[i]);
        }
    }

    // reels output

    for (int i = 0; i != DIM_REEL; i++) {
        cout << reel1[i] << ", "
             << reel2[i] << ", "
             << reel3[i] << ", "
             << reel4[i] << ", "
             << reel5[i] << endl;
    }

    // build reels --> step --> beta (final)

    srand(unsigned(time(0)));

    build_reel(reel1);
    build_reel(reel2);
    build_reel(reel3);
    build_reel(reel4);
    build_reel(reel5);

    // reels random rotation

    for (int iL = 0; iL != I_DO_MAX; iL++) {

        cout << "---------------------------------------------------------->> " << iL <<  endl;

        random_rotate_reel(reel1);
        random_rotate_reel(reel2);
        random_rotate_reel(reel3);
        random_rotate_reel(reel4);
        random_rotate_reel(reel5);

        // reels output

        for (int i = 0; i != DIM_VERTICAL; i++) {
            cout << setw(DELIM_LOC) << left << reel1[i] << " "
                 << setw(DELIM_LOC) << left << reel2[i] << " "
                 << setw(DELIM_LOC) << left << reel3[i] << " "
                 << setw(DELIM_LOC) << left << reel4[i] << " "
                 << setw(DELIM_LOC) << left << reel5[i] << endl;
        }
    }

    return 0;
}

//============================//
// local functions definition //
//============================//

// # 1 --> build_reel

void build_reel(vector<string> & reel_loc)
{
    bool iflag = true;
    vector<string>::iterator it;

    while(iflag) {
        random_shuffle(reel_loc.begin(), reel_loc.end());

        it = adjacent_find(reel_loc.begin(), reel_loc.end());
        if (it == reel_loc.end()) {
            iflag = false;
        }
    }
}

// # 2 --> random_rotate_reel

void random_rotate_reel(vector<string> & reel_loc)
{
    const unsigned int SIZE_REEL = reel_loc.size();
    const unsigned long DIVISOR = (static_cast<unsigned long>(RAND_MAX) + 1UL) / SIZE_REEL;
    const unsigned int kRot = rand() / DIVISOR;

    rotate(reel_loc.begin(), reel_loc.begin()+kRot, reel_loc.end());
}

// fini
