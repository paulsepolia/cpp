
#include "parameters.h"
#include "functions.h"

#include <algorithm>

using std::rotate;
using std::random_shuffle;
using std::count;

// # 1 --> build_reel

void build_reel(vector<string> & reel_loc)
{
    bool iflag = true;
    vector<string>::iterator it;

    while(iflag) {
        random_shuffle(reel_loc.begin(), reel_loc.end());

        it = adjacent_find(reel_loc.begin(), reel_loc.end());
        if (it == reel_loc.end()) {
            iflag = false;
        }
    }
}

// # 2 --> random_rotate_reel

void random_rotate_reel(vector<string> & reel_loc)
{
    const unsigned int SIZE_REEL = reel_loc.size();
    const unsigned long DIVISOR = (static_cast<unsigned long>(RAND_MAX) + 1UL) / SIZE_REEL;
    const unsigned int kRot = rand() / DIVISOR;

    rotate(reel_loc.begin(), reel_loc.begin()+kRot, reel_loc.end());
}

// # 3 --> create payline

void create_payline(const int * payline_loc,
                    vector<string> & vec_payline,
                    const vector<vector<string>> & reels_all)
{
    for (int i = 0; i != DIM_HORIZONTAL; i++) {
        int j = payline_loc[i];

        vec_payline.push_back(reels_all[i][j]);
    }
}

// # 4 --> find possible winning paylines

void possible_win_paylines(const vector<string> * vec_paylines_loc,
                           const string * symbols_loc,
                           vector<int> & poss_win_loc)
{
    for (int i = 0; i != NUM_PAYLINES; i++) {
        for (int j = 0; j != NUM_SYMBOLS; j++) {
            int count_symbols =
                count(vec_paylines_loc[i].begin(),
                      vec_paylines_loc[i].end(),
                      symbols_loc[j]);

            if (count_symbols >= 2) {
                poss_win_loc.push_back(i);
                poss_win_loc.push_back(j);

            }
        }
    }
}

// end
