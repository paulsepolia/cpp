
#include "parameters.h"
#include "functions.h"

#include <iostream>
#include <vector>
#include <cstdlib>
#include <string>
#include <ctime>
#include <iomanip>

using std::cout;
using std::cin;
using std::endl;
using std::vector;
using std::srand;
using std::string;
using std::time;
using std::setw;
using std::left;

#include <algorithm>
using std::count;

//==================//
// the main program //
//==================//

int main()
{
    //==================//
    // local parameters //
    //==================//

    const int I_DO_MAX = 10000;
    const int DELIM_LOC_A = 15;

    //=================//
    // local variables //
    //=================//

    // reels declaration and definition as vectors of strings

    vector<string> * reel = new vector<string> [NUM_REELS];

    // build reels --> step --> alpha

    for (int k = 0; k != NUM_REELS; k++) {
        for (int i = 0; i != NUM_SYMBOLS; i++) {
            for (int j = 0; j != data1[i]; j++) {
                reel[k].push_back(symbols[i]);
            }
        }
    }

    // play game here

    for (int iL = 0; iL != I_DO_MAX; iL++) {

        cout << "-------------------------------------------------------->> Spin: " << iL <<  endl;

        // initialize random generator

        srand(static_cast<unsigned int>(time(0)));

        // build reels --> step --> beta (no same consecutive element)

        for (int i = 0; i != NUM_REELS; i++) {
            build_reel(reel[i]);
        }

        // reels random rotation

        for (int i = 0; i != NUM_REELS; i++) {
            random_rotate_reel(reel[i]);
        }

        // reels output

        cout << endl;
        cout << "-------------------->> reels layout" << endl;
        cout << endl;

        for (int i = 0; i != DIM_VERTICAL; i++) {
            cout << setw(DELIM_LOC_A) << left << reel[0][i] << " "
                 << setw(DELIM_LOC_A) << left << reel[1][i] << " "
                 << setw(DELIM_LOC_A) << left << reel[2][i] << " "
                 << setw(DELIM_LOC_A) << left << reel[3][i] << " "
                 << setw(DELIM_LOC_A) << left << reel[4][i] << endl;
        }

        // create reels_all

        vector<vector<string>> * reels_all = new vector<vector<string>> [1];

        for (int i = 0; i != NUM_REELS; i++) {
            reels_all[0].push_back(reel[i]);
        }

        // create paylines

        vector<string> * vec_paylines = new vector<string> [NUM_PAYLINES];

        for (int i = 0; i != NUM_PAYLINES; i++) {
            create_payline(payline[i], vec_paylines[i], reels_all[0]);
        }

        // output all paylines

        cout << endl;
        cout << "-------------------->> paylines" << endl;
        cout << endl;

        for (int i = 0; i != NUM_PAYLINES; i++) {
            for (int j = 0; j != DIM_HORIZONTAL; j++) {
                cout << setw(DELIM_LOC_A) << left << vec_paylines[i][j] << " ";
            }
            cout << endl;
        }

        cout << endl;

        // find possible winning paylines

        vector<int> * possible_line_symbol = new vector<int> [1];

        possible_win_paylines(vec_paylines, symbols, possible_line_symbol[0]);

        // check if the vector is empty

        vector<string> * win_symbol = new vector<string> [4];

        bool iflag3 = false;
        bool iflag4 = false;

        if (possible_line_symbol[0].empty()) {
            cout << " --> NO POSSIBLE WINNING LINES" << endl;
        } else {

            // check each possible winning payline
            // for consecutive elements

            int k = 0;
            for (unsigned int i = 0; i != possible_line_symbol[0].size()/3; i++) {

                int line_num = possible_line_symbol[0][k];
                int counter = possible_line_symbol[0][k+2];

                // counter equals 2
                // check only for two consecutive same elements

                if (counter == 2) {
                    for (unsigned int j = 0; j != vec_paylines[line_num].size()-1; j++) {
                        if (vec_paylines[line_num][j] == vec_paylines[line_num][j+1]) {
                            win_symbol[0].push_back(symbols[possible_line_symbol[0][k+1]]);
                        }
                    }
                }

                // counter equal 3
                // check first for three consecutive same elements
                // and if not then check for two same consecutive same elements

                if (counter == 3) {
                    // check for 3 same
                    for (unsigned int j = 0; j != vec_paylines[line_num].size()-2; j++) {
                        if ((vec_paylines[line_num][j] == vec_paylines[line_num][j+1])&&
                                (vec_paylines[line_num][j+1] == vec_paylines[line_num][j+2])) {
                            win_symbol[1].push_back(symbols[possible_line_symbol[0][k+1]]);

                            iflag3 = true;
                        }
                    }

                    if (!iflag3) {
                        // check for 2 same
                        for (unsigned int j = 0; j != vec_paylines[line_num].size()-1; j++) {
                            if (vec_paylines[line_num][j] == vec_paylines[line_num][j+1]) {
                                win_symbol[0].push_back(symbols[possible_line_symbol[0][k+1]]);
                            }
                        }
                    }
                }

                // counter equals 4
                // check first for four same consequtive elements

                // reset flags

                iflag3 = false;

                if (counter == 4) {
                    // check for 4 same
                    for (unsigned int j = 0; j != vec_paylines[line_num].size()-3; j++) {
                        if ((vec_paylines[line_num][j] == vec_paylines[line_num][j+1])&&
                                (vec_paylines[line_num][j+1] == vec_paylines[line_num][j+2])&&
                                (vec_paylines[line_num][j+2] == vec_paylines[line_num][j+3])) {
                            win_symbol[2].push_back(symbols[possible_line_symbol[0][k+1]]);

                            iflag4 = true;
                        }
                    }

                    if (!iflag4) {
                        // check for 3 same
                        for (unsigned int j = 0; j != vec_paylines[line_num].size()-2; j++) {
                            if ((vec_paylines[line_num][j] == vec_paylines[line_num][j+1])&&
                                    (vec_paylines[line_num][j+1] == vec_paylines[line_num][j+2])) {
                                win_symbol[1].push_back(symbols[possible_line_symbol[0][k+1]]);
                                iflag3 = true;
                            }
                        }
                    }

                    if (!iflag3) {
                        // check for 2 same
                        for (unsigned int j = 0; j != vec_paylines[line_num].size()-1; j++) {
                            if (vec_paylines[line_num][j] == vec_paylines[line_num][j+1]) {
                                win_symbol[0].push_back(symbols[possible_line_symbol[0][k+1]]);
                            }
                        }
                    }
                }

                // counter equals 5

                if (counter == 5) {

                    win_symbol[3].push_back(symbols[possible_line_symbol[0][k+1]]);
                }

                // update counter

                k=k+3;
            }

            // results

            // two consecutive same elements

            if (!win_symbol[0].empty()) {
                cout << " --> consecutive same elements = TWO" << endl;

                for (unsigned int i = 0; i != win_symbol[0].size(); i++) {
                    cout << win_symbol[0][i] << " ";
                }
                cout << endl;
            }

            // three consecutive same elements

            if (!win_symbol[1].empty()) {
                cout << " --> consecutive same elements = THREE" << endl;

                for (unsigned int i = 0; i != win_symbol[1].size(); i++) {
                    cout << win_symbol[1][i] << " ";
                }
                cout << endl;
            }

            // four consecutive same elements

            if (!win_symbol[2].empty()) {
                cout << " --> consecutive same elements = FOUR" << endl;

                for (unsigned int i = 0; i != win_symbol[2].size(); i++) {
                    cout << win_symbol[2][i] << " ";
                }
                cout << endl;
            }

            // five consecutive same elements

            if (!win_symbol[3].empty()) {
                cout << " --> consecutive same elements = FIVE" << endl;

                for (unsigned int i = 0; i != win_symbol[3].size(); i++) {
                    cout << win_symbol[3][i] << " ";
                }
                cout << endl;
            }
        }

        // delete win_symbol

        delete [] win_symbol;

        // delete possible_line_symbol

        delete [] possible_line_symbol;

        // delete reels_all

        delete [] reels_all;

        // delete vec_paylines

        delete [] vec_paylines;
    }

    return 0;
}

// end
