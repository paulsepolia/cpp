
#ifndef PARAMETERS_H
#define PARAMETERS_H

#include <string>

using std::string;

const int NUM_SYMBOLS = 12;
const int DIM_VERTICAL = 3;
const int DIM_HORIZONTAL = 5;
const int NUM_REELS = 5;
const int NUM_PAYLINES = 15;

const int DELIM_LOC_A = 15;

// number of elements for each reel

const int data1 [NUM_SYMBOLS] = {1,6,5,4,4,4,3,3,2,1,2,4};
const int data2 [NUM_SYMBOLS] = {1,6,6,5,4,2,2,3,3,2,2,3};
const int data3 [NUM_SYMBOLS] = {1,6,5,5,4,3,3,3,3,2,2,2};
const int data4 [NUM_SYMBOLS] = {1,6,6,5,3,3,4,2,3,2,2,2};
const int data5 [NUM_SYMBOLS] = {1,4,4,5,4,3,4,4,4,0,3,3};

// paylines

const int payline[NUM_PAYLINES][NUM_REELS] = {{1,1,1,1,1},
    {0,0,0,0,0},
    {2,2,2,2,2},
    {0,1,2,1,0},
    {2,1,0,1,2},
    {0,0,1,0,0},
    {2,2,1,2,2},
    {0,0,1,2,2},
    {2,2,1,0,0},
    {1,0,0,0,1},
    {1,2,2,2,1},
    {1,1,0,1,1},
    {1,1,2,1,1},
    {1,0,1,0,1},
    {1,2,1,2,1}
};

// symbols

const string symbols[NUM_SYMBOLS] = {"Wild", "9", "10",
                                     "J", "Q", "K", "A",
                                     "Pumpkin", "Eyes",
                                     "Jackpot", "FreeSpins", "WW"
                                    };

#endif // PARAMETERS_H
