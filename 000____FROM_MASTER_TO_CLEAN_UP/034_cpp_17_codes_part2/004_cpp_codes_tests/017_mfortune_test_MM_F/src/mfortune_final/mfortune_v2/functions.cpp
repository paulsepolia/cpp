
#include "parameters.h"
#include "functions.h"

#include <algorithm>
#include <iostream>
#include <iomanip>

using std::rotate;
using std::random_shuffle;
using std::count;
using std::endl;
using std::cout;
using std::left;
using std::setw;

// # 1 --> build_reel

void build_reel(vector<string> & reel_loc)
{
    bool iflag = true;
    vector<string>::iterator it;

    while(iflag) {
        random_shuffle(reel_loc.begin(), reel_loc.end());

        it = adjacent_find(reel_loc.begin(), reel_loc.end());
        if (it == reel_loc.end()) {
            iflag = false;
        }
    }
}

// # 2 --> random_rotate_reel

void random_rotate_reel(vector<string> & reel_loc)
{
    const unsigned int SIZE_REEL = reel_loc.size();
    const unsigned long DIVISOR = (static_cast<unsigned long>(RAND_MAX) + 1UL) / SIZE_REEL;
    const unsigned int kRot = rand() / DIVISOR;

    rotate(reel_loc.begin(), reel_loc.begin()+kRot, reel_loc.end());
}

// # 3 --> create paylines

void create_payline(const int * payline_loc,
                    vector<string> & vec_payline,
                    const vector<vector<string>> & reels_all)
{
    for (int i = 0; i != DIM_HORIZONTAL; i++) {
        int j = payline_loc[i];

        vec_payline.push_back(reels_all[i][j]);
    }
}

// # 4 --> find possible winning paylines

void possible_win_paylines(const vector<string> * vec_paylines_loc,
                           const string * symbols_loc,
                           vector<int> & poss_win_loc)
{
    for (int i = 0; i != NUM_PAYLINES; i++) {
        for (int j = 0; j != NUM_SYMBOLS; j++) {
            int count_symbols =
                count(vec_paylines_loc[i].begin(),
                      vec_paylines_loc[i].end(),
                      symbols_loc[j]);

            if (count_symbols >= 2) {
                poss_win_loc.push_back(i);
                poss_win_loc.push_back(j);
                poss_win_loc.push_back(count_symbols);
            }
        }
    }
}

// # 5 --> display reels layout

void display_reels_layout(const vector<string> * reel)
{
    cout << endl;
    cout << "-------------------->> reels layout" << endl;
    cout << endl;

    for (int i = 0; i != DIM_VERTICAL; i++) {
        cout << setw(DELIM_LOC_A) << left << reel[0][i] << " "
             << setw(DELIM_LOC_A) << left << reel[1][i] << " "
             << setw(DELIM_LOC_A) << left << reel[2][i] << " "
             << setw(DELIM_LOC_A) << left << reel[3][i] << " "
             << setw(DELIM_LOC_A) << left << reel[4][i] << endl;
    }
}

// # 6 --> put all reels in a vector of vector<string> records

void create_reels_all(const vector<string> * reel,
                      vector<vector<string>> * reels_all)
{
    for (int i = 0; i != NUM_REELS; i++) {
        reels_all[0].push_back(reel[i]);
    }
}

// # 7 --> display all paylines

void display_all_paylines(const vector<string> * vec_paylines)
{
    cout << endl;
    cout << "-------------------->> paylines" << endl;
    cout << endl;

    for (int i = 0; i != NUM_PAYLINES; i++) {
        cout << setw(DELIM_LOC_A) << left << i;
        for (int j = 0; j != DIM_HORIZONTAL; j++) {
            cout << setw(DELIM_LOC_A) << left << vec_paylines[i][j] << " ";
        }
        cout << endl;
    }

    cout << endl;
}

// # 8 --> calculate and display results

void calculate_and_display_results(const vector<string> * win_symbol,
                                   const int & spin_index,
                                   int & total_win,
                                   int & win2_loc,
                                   int & win3_loc,
                                   int & win4_loc,
                                   int & win5_loc)
{
    static int total2 = 0;
    static int total3 = 0;
    static int total4 = 0;
    static int total5 = 0;

    int total2_tmp = 0;
    int total3_tmp = 0;
    int total4_tmp = 0;
    int total5_tmp = 0;

    // results

    cout << "-------------------->> results in pounds"  << endl;
    cout << endl;

    // two consecutive same elements

    int counter2 = 0;

    if (!win_symbol[0].empty()) {

        for (unsigned int i = 0; i != win_symbol[0].size(); i++) {
            if ((win_symbol[0][i] == "Jackpot")  ||
                    (win_symbol[0][i] == "Pumpkin")  ||
                    (win_symbol[0][i] == "Eyes")) {
                counter2++;
            }
        }

        if (counter2 != 0) {

            int c_jackpot =
                count(win_symbol[0].begin(), win_symbol[0].end(), "Jackpot");

            int c_pumpkin =
                count(win_symbol[0].begin(), win_symbol[0].end(), "Pumpkin");

            int c_eyes =
                count(win_symbol[0].begin(), win_symbol[0].end(), "Eyes");

            total2_tmp = c_jackpot*10 + c_pumpkin*5 + c_eyes*8;

            total2 = total2 + c_jackpot*10 + c_pumpkin*5 + c_eyes*8;

            cout << " --> total pounds for same-two consecutive = "
                 << (c_jackpot*10 + c_pumpkin*5 + c_eyes*8) << endl;
        }
    }

    if ((win_symbol[0].empty()) || (counter2 == 0)) {
        cout << " --> NO SAME-TWO WINNING CONSECUTIVE ELEMENTS" << endl;

    }

    // three consecutive same elements

    if (!win_symbol[1].empty()) {

        int c_Wild =
            count(win_symbol[1].begin(), win_symbol[1].end(), "Wild");

        int c_9 =
            count(win_symbol[1].begin(), win_symbol[1].end(), "9");

        int c_10 =
            count(win_symbol[1].begin(), win_symbol[1].end(), "10");

        int c_J =
            count(win_symbol[1].begin(), win_symbol[1].end(), "J");

        int c_Q =
            count(win_symbol[1].begin(), win_symbol[1].end(), "Q");

        int c_K =
            count(win_symbol[1].begin(), win_symbol[1].end(), "K");

        int c_A =
            count(win_symbol[1].begin(), win_symbol[1].end(), "A");

        int c_Pumpkin =
            count(win_symbol[1].begin(), win_symbol[1].end(), "Pumpkin");

        int c_Eyes =
            count(win_symbol[1].begin(), win_symbol[1].end(), "Eyes");

        int c_Jackpot =
            count(win_symbol[1].begin(), win_symbol[1].end(), "Jackpot");

        int c_FreeSpins =
            count(win_symbol[1].begin(), win_symbol[1].end(), "FreeSpins");

        int c_WW =
            count(win_symbol[1].begin(), win_symbol[1].end(), "WW");

        total3_tmp = c_Wild * 120 + c_9 * 10 + c_10 * 10 +
                     c_J    * 20  + c_Q * 35 + c_K  * 15 +
                     c_A    * 20  + c_Pumpkin * 60 + c_Eyes * 20  +
                     c_Jackpot * 20 + c_FreeSpins * 5 + c_WW * 0 ;

        total3 = total3 + total3_tmp;

        cout << " --> total pounds for same-three consecutive = "
             << total3_tmp << endl;
    }

    if (win_symbol[1].empty()) {
        cout << " --> NO SAME-THREE WINNING CONSECUTIVE ELEMENTS" << endl;

    }

    // four consecutive same elements

    if (!win_symbol[2].empty()) {

        int c_Wild =
            count(win_symbol[2].begin(), win_symbol[2].end(), "Wild");

        int c_9 =
            count(win_symbol[2].begin(), win_symbol[2].end(), "9");

        int c_10 =
            count(win_symbol[2].begin(), win_symbol[2].end(), "10");

        int c_J =
            count(win_symbol[2].begin(), win_symbol[2].end(), "J");

        int c_Q =
            count(win_symbol[2].begin(), win_symbol[2].end(), "Q");

        int c_K =
            count(win_symbol[2].begin(), win_symbol[2].end(), "K");

        int c_A =
            count(win_symbol[2].begin(), win_symbol[2].end(), "A");

        int c_Pumpkin =
            count(win_symbol[2].begin(), win_symbol[2].end(), "Pumpkin");

        int c_Eyes =
            count(win_symbol[2].begin(), win_symbol[2].end(), "Eyes");

        int c_Jackpot =
            count(win_symbol[2].begin(), win_symbol[2].end(), "Jackpot");

        int c_FreeSpins =
            count(win_symbol[2].begin(), win_symbol[2].end(), "FreeSpins");

        int c_WW =
            count(win_symbol[2].begin(), win_symbol[2].end(), "WW");

        total4_tmp = c_Wild * 550 + c_9 * 15 + c_10 * 25 +
                     c_J    * 65  + c_Q * 85 + c_K  * 55 +
                     c_A    * 130  + c_Pumpkin * 140 + c_Eyes * 95  +
                     c_Jackpot * 140 + c_FreeSpins * 10 + c_WW * 0 ;

        total4 = total4 + total4_tmp;

        cout << " --> total pounds for same-four consecutive = "
             << total4_tmp << endl;
    }

    if (win_symbol[2].empty()) {
        cout << " --> NO SAME-FOUR WINNING CONSECUTIVE ELEMENTS" << endl;

    }

    // five consecutive same elements

    if (!win_symbol[3].empty()) {

        int c_Wild =
            count(win_symbol[3].begin(), win_symbol[3].end(), "Wild");

        int c_9 =
            count(win_symbol[3].begin(), win_symbol[3].end(), "9");

        int c_10 =
            count(win_symbol[3].begin(), win_symbol[3].end(), "10");

        int c_J =
            count(win_symbol[3].begin(), win_symbol[3].end(), "J");

        int c_Q =
            count(win_symbol[3].begin(), win_symbol[3].end(), "Q");

        int c_K =
            count(win_symbol[3].begin(), win_symbol[3].end(), "K");

        int c_A =
            count(win_symbol[3].begin(), win_symbol[3].end(), "A");

        int c_Pumpkin =
            count(win_symbol[3].begin(), win_symbol[3].end(), "Pumpkin");

        int c_Eyes =
            count(win_symbol[3].begin(), win_symbol[3].end(), "Eyes");

        int c_Jackpot =
            count(win_symbol[3].begin(), win_symbol[3].end(), "Jackpot");

        int c_FreeSpins =
            count(win_symbol[3].begin(), win_symbol[3].end(), "FreeSpins");

        int c_WW =
            count(win_symbol[3].begin(), win_symbol[3].end(), "WW");

        total5_tmp = c_Wild * 1500 + c_9 * 40 + c_10 * 70 +
                     c_J    * 90  + c_Q * 160 + c_K  * 135 +
                     c_A    * 180  + c_Pumpkin * 220 + c_Eyes * 155  +
                     c_Jackpot * 0 + c_FreeSpins * 70 + c_WW * 0 ;

        total5 = total5 + total5_tmp;

        cout << " --> total pounds for same-five consecutive = "
             << total5_tmp << endl;
    }

    if (win_symbol[3].empty()) {
        cout << " --> NO SAME-FIVE WINNING CONSECUTIVE ELEMENTS" << endl;

    }
    // totals

    win2_loc = total2_tmp;
    win3_loc = total3_tmp;
    win4_loc = total4_tmp;
    win5_loc = total5_tmp;

    cout << endl;
    cout << " --> Total amount played = £ " << spin_index << endl;

    total_win = total2 + total3 + total4 + total5;

    cout << " --> Total amount won    = £ " << total_win << endl;
}

//=====//
// end //
//=====//
