
#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include "parameters.h"

#include <vector>
#include <string>

using std::vector;
using std::string;

// 1 --> build_reel

void build_reel(vector<string> &);

// 2 --> rotate_reel

void random_rotate_reel(vector<string> &);

// 3 --> create payline

void create_payline(const int *, vector<string> &, const vector<vector<string>> &);

#endif // FUNCTIONS_H

// end
