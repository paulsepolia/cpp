
#include "parameters.h"

#include <iostream>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <string>
#include <ctime>
#include <iomanip>

using std::cout;
using std::cin;
using std::endl;
using std::vector;
using std::rotate;
using std::rand;
using std::srand;
using std::string;
using std::time;
using std::random_shuffle;
using std::setw;
using std::left;

//==============================//
// local functions declarations //
//==============================//

// # 1 --> build_reel

void build_reel(vector<string> &);

// # 2 --> rotate_reel

void random_rotate_reel(vector<string> &);

// # 3 --> create payline

void create_payline(const int *, vector<string> &, const vector<vector<string>> &);

//==================//
// the main program //
//==================//

int main()
{
    //==================//
    // local parameters //
    //==================//

    const int I_DO_MAX = 1000;

    //=================//
    // local variables //
    //=================//

    // reels declaration and definition as vectors of strings

    vector<string> * reel = new vector<string> [NUM_REELS];

    vector<vector<string>> reels_all;

    // reel # 1 --> step --> alpha

    for (int k = 0; k != NUM_REELS; k++) {
        for (int i = 0; i != DIM_REEL; i++) {
            for (int j = 0; j != data1[i]; j++) {
                reel[k].push_back(symbols[i]);
            }
        }
    }

    // play game here

    for (int iL = 0; iL != I_DO_MAX; iL++) {

        cout << "---------------------------------------------------------->> " << iL <<  endl;

        // initialize random generator

        srand(static_cast<unsigned int>(time(0)));

        // build reels --> step --> beta (final)

        for (int i = 0; i != NUM_REELS; i++) {
            build_reel(reel[i]);
        }

        // reels random rotation

        for (int i = 0; i != NUM_REELS; i++) {
            random_rotate_reel(reel[i]);
        }

        // reels output

        cout << endl;
        cout << "-------------------->> reels layout" << endl;
        cout << endl;

        for (int i = 0; i != DIM_VERTICAL; i++) {
            cout << setw(DELIM_LOC) << left << reel[0][i] << " "
                 << setw(DELIM_LOC) << left << reel[1][i] << " "
                 << setw(DELIM_LOC) << left << reel[2][i] << " "
                 << setw(DELIM_LOC) << left << reel[3][i] << " "
                 << setw(DELIM_LOC) << left << reel[4][i] << endl;
        }

        // create reels_all

        vector<vector<string>> reels_all;

        reels_all.push_back(reel[0]);
        reels_all.push_back(reel[1]);
        reels_all.push_back(reel[2]);
        reels_all.push_back(reel[3]);
        reels_all.push_back(reel[4]);

        // create paylines

        vector<string> * vec_paylines = new vector<string> [PAYLINES_NUM];

        create_payline(payline1,  vec_paylines[0],  reels_all);
        create_payline(payline2,  vec_paylines[1],  reels_all);
        create_payline(payline3,  vec_paylines[2],  reels_all);
        create_payline(payline4,  vec_paylines[3],  reels_all);
        create_payline(payline5,  vec_paylines[4],  reels_all);
        create_payline(payline6,  vec_paylines[5],  reels_all);
        create_payline(payline7,  vec_paylines[6],  reels_all);
        create_payline(payline8,  vec_paylines[7],  reels_all);
        create_payline(payline9,  vec_paylines[8],  reels_all);
        create_payline(payline10, vec_paylines[9],  reels_all);
        create_payline(payline11, vec_paylines[10], reels_all);
        create_payline(payline12, vec_paylines[11], reels_all);
        create_payline(payline13, vec_paylines[12], reels_all);
        create_payline(payline14, vec_paylines[13], reels_all);
        create_payline(payline15, vec_paylines[14], reels_all);

        // check paylines

        cout << endl;
        cout << "-------------------->> paylines" << endl;
        cout << endl;

        for (int i = 0; i != PAYLINES_NUM; i++) {
            for (int j = 0; j != DIM_HORIZONTAL; j++) {
                cout << setw(DELIM_LOC) << left << vec_paylines[i][j] << " ";
            }
            cout << endl;
        }

        // clear reels_all

        reels_all.clear();
        reels_all.shrink_to_fit();

        // delete vec_paylines

        delete [] vec_paylines;
    }

    return 0;
}

//============================//
// local functions definition //
//============================//

// # 1 --> build_reel

void build_reel(vector<string> & reel_loc)
{
    bool iflag = true;
    vector<string>::iterator it;

    while(iflag) {
        random_shuffle(reel_loc.begin(), reel_loc.end());

        it = adjacent_find(reel_loc.begin(), reel_loc.end());
        if (it == reel_loc.end()) {
            iflag = false;
        }
    }
}

// # 2 --> random_rotate_reel

void random_rotate_reel(vector<string> & reel_loc)
{
    const unsigned int SIZE_REEL = reel_loc.size();
    const unsigned long DIVISOR = (static_cast<unsigned long>(RAND_MAX) + 1UL) / SIZE_REEL;
    const unsigned int kRot = rand() / DIVISOR;

    rotate(reel_loc.begin(), reel_loc.begin()+kRot, reel_loc.end());
}

// # 3 --> create payline

void create_payline(const int * payline_loc,
                    vector<string> & vec_payline,
                    const vector<vector<string>> & reels_all)
{
    const int DIM_HORIZONTAL = 5;

    for (int i = 0; i != DIM_HORIZONTAL; i++) {
        int j = payline_loc[i];

        vec_payline.push_back(reels_all[i][j]);
    }
}

// end
