
#ifndef PARAMETERS_H
#define PARAMETERS_H

#include <string>

using std::string;

const int DIM_REEL = 12;
const int DELIM_LOC = 10;
const int DIM_VERTICAL = 3;
const int DIM_HORIZONTAL = 5;
const int NUM_REELS = 5;
const int PAYLINES_NUM = 15;

// number of elements for each reel

const int data1 [] = {1,6,5,4,4,4,3,3,2,1,2,4};
const int data2 [] = {1,6,6,5,4,2,2,3,3,2,2,3};
const int data3 [] = {1,6,5,5,4,3,3,3,3,2,2,2};
const int data4 [] = {1,6,6,5,3,3,4,2,3,2,2,2};
const int data5 [] = {1,4,4,5,4,3,4,4,4,0,3,3};

// paylines as vector

const int payline1  [] = {1,1,1,1,1};
const int payline2  [] = {0,0,0,0,0};
const int payline3  [] = {2,2,2,2,2};
const int payline4  [] = {0,1,2,1,0};
const int payline5  [] = {2,1,0,1,2};
const int payline6  [] = {0,0,1,0,0};
const int payline7  [] = {2,2,1,2,2};
const int payline8  [] = {0,0,1,2,2};
const int payline9  [] = {2,2,1,0,0};
const int payline10 [] = {1,0,0,0,1};
const int payline11 [] = {1,2,2,2,1};
const int payline12 [] = {1,1,0,1,1};
const int payline13 [] = {1,1,2,1,1};
const int payline14 [] = {1,0,1,0,1};
const int payline15 [] = {1,2,1,2,1};

// symbols

const string symbols[] = {"Wild", "9", "10",
                          "J", "Q", "K", "A",
                          "Pumpkin", "Eyes",
                          "Jakpot", "Scatter", "WW"
                         };

#endif // PARAMETERS_H
