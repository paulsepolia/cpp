
#include "parameters.h"
#include "functions.h"

#include <algorithm>
#include <iostream>
#include <iomanip>

using std::rotate;
using std::random_shuffle;
using std::count;
using std::endl;
using std::cout;
using std::left;
using std::setw;

// # 1 --> build_reel

void build_reel(vector<string> & reel_loc)
{
    bool iflag = true;
    vector<string>::iterator it;

    while(iflag) {
        random_shuffle(reel_loc.begin(), reel_loc.end());

        it = adjacent_find(reel_loc.begin(), reel_loc.end());
        if (it == reel_loc.end()) {
            iflag = false;
        }
    }
}

// # 2 --> random_rotate_reel

void random_rotate_reel(vector<string> & reel_loc)
{
    const unsigned int SIZE_REEL = reel_loc.size();
    const unsigned long DIVISOR = (static_cast<unsigned long>(RAND_MAX) + 1UL) / SIZE_REEL;
    const unsigned int kRot = rand() / DIVISOR;

    rotate(reel_loc.begin(), reel_loc.begin()+kRot, reel_loc.end());
}

// # 3 --> create payline

void create_payline(const int * payline_loc,
                    vector<string> & vec_payline,
                    const vector<vector<string>> & reels_all)
{
    for (int i = 0; i != DIM_HORIZONTAL; i++) {
        int j = payline_loc[i];

        vec_payline.push_back(reels_all[i][j]);
    }
}

// # 4 --> find possible winning paylines

void possible_win_paylines(const vector<string> * vec_paylines_loc,
                           const string * symbols_loc,
                           vector<int> & poss_win_loc)
{
    for (int i = 0; i != NUM_PAYLINES; i++) {
        for (int j = 0; j != NUM_SYMBOLS; j++) {
            int count_symbols =
                count(vec_paylines_loc[i].begin(),
                      vec_paylines_loc[i].end(),
                      symbols_loc[j]);

            if (count_symbols >= 2) {
                poss_win_loc.push_back(i);
                poss_win_loc.push_back(j);
                poss_win_loc.push_back(count_symbols);
            }
        }
    }
}

// # 5 --> display reels layout

void display_reels_layout(const vector<string> * reel)
{
    cout << endl;
    cout << "-------------------->> reels layout" << endl;
    cout << endl;

    for (int i = 0; i != DIM_VERTICAL; i++) {
        cout << setw(DELIM_LOC_A) << left << reel[0][i] << " "
             << setw(DELIM_LOC_A) << left << reel[1][i] << " "
             << setw(DELIM_LOC_A) << left << reel[2][i] << " "
             << setw(DELIM_LOC_A) << left << reel[3][i] << " "
             << setw(DELIM_LOC_A) << left << reel[4][i] << endl;
    }
}

// # 6 --> put all reels in a vector of vector<string> records

void create_reels_all(const vector<string> * reel,
                      vector<vector<string>> * reels_all)
{
    for (int i = 0; i != NUM_REELS; i++) {
        reels_all[0].push_back(reel[i]);
    }
}

//=====//
// end //
//=====//
