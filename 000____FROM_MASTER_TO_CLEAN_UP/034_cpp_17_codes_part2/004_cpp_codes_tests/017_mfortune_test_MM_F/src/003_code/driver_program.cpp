
#include <iostream>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <string>
#include <ctime>
#include <iomanip>

using std::cout;
using std::cin;
using std::endl;
using std::vector;
using std::rotate;
using std::rand;
using std::srand;
using std::string;
using std::time;
using std::random_shuffle;
using std::setw;
using std::left;

//==============================//
// local functions declarations //
//==============================//

// # 1 --> build_reel

void build_reel(vector<string> &);

// # 2 --> rotate_reel

void random_rotate_reel(vector<string> &);

// # 3 --> create payline

void create_payline(const int *, vector<string> &, const vector<vector<string>> &);

//==================//
// the main program //
//==================//

int main()
{
    //==================//
    // local parameters //
    //==================//

    const int DIM_REEL = 12;
    const int I_DO_MAX = 10000;
    const int DELIM_LOC = 10;
    const int DIM_VERTICAL = 3;
    const int DIM_HORIZONTAL = 5;
    const int PAYLINES_NUM = 15;


    // number of elements for each reel

    const int data1 [] = {1,6,5,4,4,4,3,3,2,1,2,4};
    const int data2 [] = {1,6,6,5,4,2,2,3,3,2,2,3};
    const int data3 [] = {1,6,5,5,4,3,3,3,3,2,2,2};
    const int data4 [] = {1,6,6,5,3,3,4,2,3,2,2,2};
    const int data5 [] = {1,4,4,5,4,3,4,4,4,0,3,3};

    // paylines as vector

    const int payline1  [] = {1,1,1,1,1};
    const int payline2  [] = {0,0,0,0,0};
    const int payline3  [] = {2,2,2,2,2};
    const int payline4  [] = {0,1,2,1,0};
    const int payline5  [] = {2,1,0,1,2};
    const int payline6  [] = {0,0,1,0,0};
    const int payline7  [] = {2,2,1,2,2};
    const int payline8  [] = {0,0,1,2,2};
    const int payline9  [] = {2,2,1,0,0};
    const int payline10 [] = {1,0,0,0,1};
    const int payline11 [] = {1,2,2,2,1};
    const int payline12 [] = {1,1,0,1,1};
    const int payline13 [] = {1,1,2,1,1};
    const int payline14 [] = {1,0,1,0,1};
    const int payline15 [] = {1,2,1,2,1};

    // symbols

    const string symbols[] = {"Wild", "9", "10",
                              "J", "Q", "K", "A",
                              "Pumpkin", "Eyes",
                              "Jakpot", "Scatter", "WW"
                             };

    //=================//
    // local variables //
    //=================//

    // reels declaration and definition as vectors of strings

    vector<string> reel1;
    vector<string> reel2;
    vector<string> reel3;
    vector<string> reel4;
    vector<string> reel5;

    vector<vector<string>> reels_all;

    // reel # 1 --> step --> alpha

    for (int i = 0; i != DIM_REEL; i++) {
        for (int j = 0; j != data1[i]; j++) {
            reel1.push_back(symbols[i]);
        }
    }

    // reel # 2 --> step --> alpha

    for (int i = 0; i != DIM_REEL; i++) {
        for (int j = 0; j != data2[i]; j++) {
            reel2.push_back(symbols[i]);
        }
    }

    // reel # 3 --> step --> alpha

    for (int i = 0; i != DIM_REEL; i++) {
        for (int j = 0; j != data3[i]; j++) {
            reel3.push_back(symbols[i]);
        }
    }

    // reel # 4 --> step --> alpha

    for (int i = 0; i != DIM_REEL; i++) {
        for (int j = 0; j != data4[i]; j++) {
            reel4.push_back(symbols[i]);
        }
    }

    // reel # 5 --> step --> alpha

    for (int i = 0; i != DIM_REEL; i++) {
        for (int j = 0; j != data5[i]; j++) {
            reel5.push_back(symbols[i]);
        }
    }

    // play game here

    for (int iL = 0; iL != I_DO_MAX; iL++) {

        cout << "---------------------------------------------------------->> " << iL <<  endl;

        // build reels --> step --> beta (final)

        srand(unsigned(time(0)));

        build_reel(reel1);
        build_reel(reel2);
        build_reel(reel3);
        build_reel(reel4);
        build_reel(reel5);

        // reels random rotation

        random_rotate_reel(reel1);
        random_rotate_reel(reel2);
        random_rotate_reel(reel3);
        random_rotate_reel(reel4);
        random_rotate_reel(reel5);

        // reels output

        for (int i = 0; i != DIM_VERTICAL; i++) {
            cout << setw(DELIM_LOC) << left << reel1[i] << " "
                 << setw(DELIM_LOC) << left << reel2[i] << " "
                 << setw(DELIM_LOC) << left << reel3[i] << " "
                 << setw(DELIM_LOC) << left << reel4[i] << " "
                 << setw(DELIM_LOC) << left << reel5[i] << endl;
        }

        // create reels_all

        vector<vector<string>> reels_all;

        reels_all.push_back(reel1);
        reels_all.push_back(reel2);
        reels_all.push_back(reel3);
        reels_all.push_back(reel4);
        reels_all.push_back(reel5);

        // create paylines

        vector<string> * vec_paylines = new vector<string> [PAYLINES_NUM];

        create_payline(payline1,  vec_paylines[0],  reels_all);
        create_payline(payline2,  vec_paylines[1],  reels_all);
        create_payline(payline3,  vec_paylines[2],  reels_all);
        create_payline(payline4,  vec_paylines[3],  reels_all);
        create_payline(payline5,  vec_paylines[4],  reels_all);
        create_payline(payline6,  vec_paylines[5],  reels_all);
        create_payline(payline7,  vec_paylines[6],  reels_all);
        create_payline(payline8,  vec_paylines[7],  reels_all);
        create_payline(payline9,  vec_paylines[8],  reels_all);
        create_payline(payline10, vec_paylines[9],  reels_all);
        create_payline(payline11, vec_paylines[10], reels_all);
        create_payline(payline12, vec_paylines[11], reels_all);
        create_payline(payline13, vec_paylines[12], reels_all);
        create_payline(payline14, vec_paylines[13], reels_all);
        create_payline(payline15, vec_paylines[14], reels_all);

        // check paylines

        cout << "------------------------------------------------->> paylines" << endl;

        for (int i = 0; i != PAYLINES_NUM; i++) {
            for (int j = 0; j != DIM_HORIZONTAL; j++) {
                cout << setw(DELIM_LOC) << left << vec_paylines[i][j] << " ";
            }
            cout << endl;
        }
        cout << endl;


        // clear reels_all

        reels_all.clear();
        reels_all.shrink_to_fit();

        // delete vec_paylines

        delete [] vec_paylines;

    }

    return 0;
}

//============================//
// local functions definition //
//============================//

// # 1 --> build_reel

void build_reel(vector<string> & reel_loc)
{
    bool iflag = true;
    vector<string>::iterator it;

    while(iflag) {
        random_shuffle(reel_loc.begin(), reel_loc.end());

        it = adjacent_find(reel_loc.begin(), reel_loc.end());
        if (it == reel_loc.end()) {
            iflag = false;
        }
    }
}

// # 2 --> random_rotate_reel

void random_rotate_reel(vector<string> & reel_loc)
{
    const unsigned int SIZE_REEL = reel_loc.size();
    const unsigned long DIVISOR = (static_cast<unsigned long>(RAND_MAX) + 1UL) / SIZE_REEL;
    const unsigned int kRot = rand() / DIVISOR;

    rotate(reel_loc.begin(), reel_loc.begin()+kRot, reel_loc.end());
}

// # 3 --> create payline

void create_payline(const int * payline_loc,
                    vector<string> & vec_payline,
                    const vector<vector<string>> & reels_all)
{
    const int DIM_HORIZONTAL = 5;

    for (int i = 0; i != DIM_HORIZONTAL; i++) {
        int j = payline_loc[i];

        vec_payline.push_back(reels_all[i][j]);
    }
}


// fini
