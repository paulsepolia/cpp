
#include <iostream>
#include <iomanip>
#include <string>

using std::cout;
using std::endl;
using std::cin;
using std::boolalpha;
using std::string;

// isPrime

bool isPrime(unsigned long int number)
{
    if(number < 2UL) return false;

    if(number == 2UL) return true;

    if(number % 2UL == 0UL) return false;

    for(int i = 3UL; (i*static_cast<double>(i)) <= number; i+=2UL) {
        if(number % i == 0UL) return false;
    }

    return true;
}

// countPrimes

unsigned long int countPrimes(unsigned long int n)
{
    unsigned long int count = 0UL;
    unsigned long int i;
    bool isPrime(unsigned long int);

    #pragma omp parallel default(none) \
    private(i)         \
    shared(n)          \
    reduction(+:count)
    {
        #pragma omp for schedule(dynamic,1)

        for (i = 0UL; i < n; ++i) {
            if (isPrime(i)) {
                count = count + 1UL;
            }
        }
    }

    return count;
}

// the main program

int main(int argc, char ** argv)
{
    // input variables

    string input = argv[1];
    unsigned int output = 0;
    unsigned long int value = atoi(input.c_str());

    output = countPrimes(value);

    cout << " number of primes less than " << value << " are " << output << endl;
}

