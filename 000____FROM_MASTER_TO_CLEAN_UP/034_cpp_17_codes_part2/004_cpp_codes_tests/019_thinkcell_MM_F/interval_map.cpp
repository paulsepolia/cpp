#include <assert.h>
#include <map>
#include <limits>
#include <iostream>
#include <set>

using namespace std;

template<class K, class V>
class interval_map {

    friend void IntervalMapTest();

private:

    std::map<K,V> m_map;

public:
    // constructor associates whole range of K with val by inserting (K_min, val)
    // into the map
    interval_map( V const& val)
    {
        m_map.insert(m_map.begin(),std::make_pair(std::numeric_limits<K>::min(),val));
    };

    // Assign value val to interval [keyBegin, keyEnd).
    // Overwrite previous values in this interval.
    // Do not change values outside this interval.
    // Conforming to the C++ Standard Library conventions, the interval
    // includes keyBegin, but excludes keyEnd.
    // If !( keyBegin < keyEnd ), this designates an empty interval,
    // and assign must do nothing.


    void assign(K const& keyBegin, K const& keyEnd, const V& val )
    {

        typename map<K,V>::iterator it;

        it = m_map.find(keyBegin);       // try to find the keyBegin

        if (it != m_map.end()) {           // if exists
            it--;                         // goto the previous element
            if (it->second == val) {      // if the value of the previous is the same
                it++;                    // return
                m_map.erase(it);         // and delete it
            }
            if (it->second != val) {      // if the value of the previous is not the same
                m_map[keyBegin] = val;   // make assignment
            }
        } else if (it == m_map.end()) {    // if does not exist
            m_map[keyBegin] = val;        // make assignment
        }

        // set the iterator again

        it = m_map.find(keyBegin);

        it++;

        while (it->first < keyEnd) {
            if (m_map.end()==it) { // check if it++ is the end
                break;     // if yes break
            }

            m_map.erase(it);      // delete keys less than keyEnd
            it = m_map.find(keyBegin);
            it++;
        }

        // clean duplicates

        typename map<K,V>::iterator it2 = m_map.begin();

        while (it2 != m_map.end()) {
            it = it2++;
            if (it2 != m_map.end()) {
                if (it->second == it2->second) {
                    m_map.erase(it);
                }
            }
        }

    }

    // look-up of the value associated with key
    V const& operator[]( K const& key ) const
    {
        return ( --m_map.upper_bound(key) )->second;
    }
};

// Many solutions we receive are incorrect. Consider using a randomized test
// to discover the cases that your implementation does not handle correctly.
// We recommend to implement a function IntervalMapTest() here that tests the
// functionality of the interval_map, for example using a map of unsigned int
// intervals to char.

void IntervalMapTest()
{
    interval_map<int,char> K1('A');

    map<int,char>::iterator it = K1.m_map.begin();

    K1.assign(3, 5, 'B');
    K1.assign(10, 100, 'C');
    K1.assign(7, 8, 'A');
    K1.assign(11, 12, 'D');

    for (it = K1.m_map.begin(); it != K1.m_map.end(); ++it) {
        cout << it->first << " => " << it->second << endl;
    }
}

// the main function

int main(int argc, char* argv[])
{
    IntervalMapTest();
    return 0;
}
