#include <iostream>
#include <vector>
#include <memory>
#include <functional>
#include <chrono>
#include <iomanip>
#include <array>

class benchmark_timer {
public:

    explicit benchmark_timer(const double &res) :
            _res(res),
            t1{std::chrono::steady_clock::now()},
            t2{std::chrono::steady_clock::now()} {};

    ~benchmark_timer() {
        t2 = std::chrono::steady_clock::now();
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count()};
        std::cout << " --> res = " << _res << std::endl;
        std::cout << " --> time used (secs) = " << total_time << std::endl;
    }

    auto set_res(const double &res) -> void {
        _res = res;
    }

private:

    double _res{};
    decltype(std::chrono::steady_clock::now()) t1{};
    decltype(std::chrono::steady_clock::now()) t2{};
};

class AllignmentMinSize {
public:

    std::array<double, 100> a1;
    double a2;
    uint8_t a3;
    bool a4;
};

class AllignmentNotMinSize1 {
public:

    bool a4;
    std::array<double, 100> a1;
    uint8_t a3;
    double a2;
};

class A1 {
public:

    bool a4;
    int a3;
    double a2;
};

class A2 {
public:

    double a2;
    int a3;
    bool a4;
};

int main() {

    std::cout << std::setprecision(5);
    std::cout << std::scientific;

    AllignmentMinSize a{};
    AllignmentNotMinSize1 b{};

    std::cout << sizeof(a) << std::endl;
    std::cout << sizeof(b) << std::endl;
    std::cout << sizeof(A1) << std::endl;
    std::cout << sizeof(A2) << std::endl;

    std::cout << sizeof(std::array<double, 100>) << std::endl;
    std::cout << sizeof(double) << std::endl;
    std::cout << sizeof(uint8_t) << std::endl;
    std::cout << sizeof(bool) << std::endl;
}
