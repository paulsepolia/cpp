#!/bin/bash

  # 1. compile

  icpc -O3                \
       -Wall              \
       -std=gnu++17      \
       driver_program.cpp \
       -o x_intel
