
#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::string;

// the solution

int solution(string &S)
{

    const int DIM = S.length();
    int sol = 1;
    int countL = 0;
    int countR = 0;

    if (DIM == 0) {
        sol = 1;
    }

    for (int i = 0; i != DIM; i++) {
        if (S[i]=='(') {
            countL = countL + 1;
        }

        if (S[i]==')') {
            countR = countR + 1;
        }

        if ((countL - countR) < 0) {
            sol = 0;
            break;
        }
    }

    if (countL != countR) {
        sol = 0;
    }

    return sol;
}

// the main function

int main()
{
    string s1 = "((()))";
    string s2 = "(((()))";
    string s3 = "()((()))";
    string s4 = "()((()))(";

    cout << solution(s1) << endl;
    cout << solution(s2) << endl;
    cout << solution(s3) << endl;
    cout << solution(s4) << endl;

    return 0;
}

// end
