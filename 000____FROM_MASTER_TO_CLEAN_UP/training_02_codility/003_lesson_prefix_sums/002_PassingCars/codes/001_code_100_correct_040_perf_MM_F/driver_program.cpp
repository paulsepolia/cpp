
#include <iostream>
#include <vector>

using std::endl;
using std::cout;
using std::vector;

int solution(vector<int> &A)
{

    long long int i;
    long long int counter = 0;
    const long long int DIM = A.size();
    long long ik = 0;

    for (i = DIM-1; i >= 0; i--) {
        if (A[i] == 0) {
            counter = counter + (DIM-1)-i-ik;
            ik++;
        }
    }

    return counter;
}

// the main function

int main()
{
    vector<int> A( {0,1,0,1,1});

    cout << " --> example 1 --> " << solution(A) << endl;

    return 0;
}

// end
