
#include <algorithm>
#include <vector>

using namespace std;

int solution(vector<int> &A)
{
    // sort the array

    sort(A.begin(), A.end());

    // unique the array

    vector<int>::iterator it;

    it = unique(A.begin(), A.end());

    // resize the vector

    A.resize(distance(A.begin(),it));

    return A.size();
}

// END
