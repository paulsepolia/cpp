
#include <iostream>

using std::endl;
using std::cout;

// the solution

int solution(int X, int Y, int D)
{
    int res;

    if ((Y-X)%D == 0) {
        res = (Y-X)/D;
    } else {
        res = (Y-X)/D+1;
    }

    return res;
}

// the main function

int main()
{
    int X = 10;
    int Y = 85;
    int D = 30;

    cout << solution(X, Y, D) << endl;

    return 0;
}

// END
