#include <algorithm>
#include <cmath>

using namespace std;

int solution(vector<int> &A)
{
    double sumA = 0.0;
    double sumB = 0.0;
    double min;
    double tmp;
    double init = 0.0;

    const long long int SIZE = A.size();
    long long int i;

    if (SIZE == 2) {
        min = fabs(A[0]-A[1]);
    } else {

        const double SUMR = accumulate(A.begin(), A.end(), init)- A[0];

        min = fabs(SUMR - A[0]);

        sumB = SUMR;
        sumA = A[0];

        for (i = 1; i != SIZE; ++i) {
            sumA = A[i] + sumA;
            sumB = sumB - A[i];

            tmp = fabs(sumA-sumB);

            if (tmp < min) {
                min = tmp;
            }
        }
    }

    return static_cast<int>(min);
}

// END
