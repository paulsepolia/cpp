#include <algorithm>
#include <cmath>

using namespace std;

int solution(vector<int> &A)
{
    const long long int SIZE = A.size();
    long long int sumMin = A[0];
    long long int sumMax = 0;

    for (long long int i = 1; i < SIZE; ++i) {
        sumMax = sumMax + A[i];
    }

    long long int minDif = fabs(sumMax-sumMin);

    for (int i = 1; i < SIZE; i++) {
        long long int dif = fabs(sumMax - sumMin);
        if (dif < minDif) {
            minDif = dif;
        }
        sumMin += A[i];
        sumMax -= A[i];
    }

    return minDif;
}

// END
