
#include <vector>
#include <algorithm>

using namespace std;

int solution(vector<int> &A)
{
    const long long int SIZE = A.size();

    // extreme cases

    for(long long int i = 0; i < SIZE; i++) {

        if (A[i] < 1 || A[i] > SIZE) {
            return 0;
        }
    }

    // sort vector

    sort(A.begin(), A.end());

    // new vector

    vector<int> B;

    // build the new vector

    for (long long int i = 0; i != SIZE; ++i) {
        B.push_back(A[0] + i);
    }

    // compare

    if (A == B) {
        return 1;
    } else {
        return 0;
    }
}

// END
