#include <vector>
using namespace std;

int solution(vector<int> & A)
{
    // get the size

    const long long int SIZE = A.size();

    // new array

    int * counter = new int [A.size()];

    // initialize the new array

    for (long long int i = 0; i < SIZE; ++i) {

        counter[i] = 0;
    }

    // main loop

    for(long long int i= 0; i< SIZE; i++) {
        if (A[i] < 1 || A[i] > SIZE) {
            // Out of range
            return 0;
        } else if(counter[A[i]-1] == 1) {
            // met before
            return 0;
        } else {
            // first time meet
            counter[A[i]-1] = 1;
        }
    }

    return 1;
}

// END
