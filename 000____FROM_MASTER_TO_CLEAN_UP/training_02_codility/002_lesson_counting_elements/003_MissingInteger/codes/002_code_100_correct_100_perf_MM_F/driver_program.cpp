
#include <algorithm>
#include <vector>

using namespace std;

int solution(vector<int> &A)
{
    // sort the vector

    sort(A.begin(), A.end());

    // get the size of the vector

    const long long int SIZE = A.size();

    int min_res = 1;

    for(long long int i = 0; i != SIZE; ++i) {
        if (A[i] > 0 ) {
            if (A[i] == min_res) {
                min_res++;
            }
        }
    }

    return min_res;
}

// END
