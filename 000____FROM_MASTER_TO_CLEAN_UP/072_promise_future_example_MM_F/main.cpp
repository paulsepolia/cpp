#include <iostream>
#include <future>
#include <vector>
#include <chrono>

class benchmark_timer {
public:

    explicit benchmark_timer(const double &res) :
            _res(res),
            t1{std::chrono::steady_clock::now()},
            t2{std::chrono::steady_clock::now()},
            t_prev{std::chrono::steady_clock::now()} {};

    auto print_time() const -> void {
        const auto t_loc{std::chrono::steady_clock::now()};
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t_loc - t_prev).count()};
        t_prev = t_loc;
        std::cout << " --> time used (secs) = " << total_time << std::endl;
    }

    ~benchmark_timer() {
        t2 = std::chrono::steady_clock::now();
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count()};
        std::cout << " --> res = " << _res << std::endl;
        std::cout << " --> time used (secs) = " << total_time << std::endl;
    }

    auto set_res(const double &res) -> void {
        _res = res;
    }

private:

    double _res{};
    decltype(std::chrono::steady_clock::now()) t1{};
    decltype(std::chrono::steady_clock::now()) t2{};
    mutable decltype(std::chrono::steady_clock::now()) t_prev{};
};

auto set_promise_value(std::promise<std::vector<double>> &prom, std::vector<double> &&val) -> void {

    std::cout << " --> I am about to set a value to the promise I gave but I am sleeping right now..." << std::endl;
    std::this_thread::sleep_for(std::chrono::milliseconds(10000));
    std::cout << " --> I am setting a value to my promise..." << std::endl;
    prom.set_value(std::move(val));
    std::cout << " --> My promised is fulfilled!" << std::endl;
}

auto get_future_value(std::future<std::vector<double>> &fut, std::vector<double> &val) -> void {

    std::cout << "I am waiting for my future to get its promise..." << std::endl;
    val = fut.get();
}

constexpr auto DIM_MAX{(size_t) 15'000'000};

auto main() -> int {

    {
        std::cout << "------------------------------------------->> 1" << std::endl;

        auto ot{benchmark_timer(0)};

        auto proms{std::vector<std::promise<double>>{}};
        auto futs{std::vector<std::future<double>>{}};

        std::cout << " --> building a vector of promises..." << std::endl;
        proms.resize(DIM_MAX);
        ot.print_time();


        std::cout << " --> building a vector with futures..." << std::endl;
        futs.resize(DIM_MAX);
        ot.print_time();

        std::cout << " --> associate the two vectors..." << std::endl;
        std::cout << " --> which means I associate futures with their corresponding promises..." << std::endl;

        for (size_t i = 0; i < DIM_MAX; i++) {
            futs[i] = proms[i].get_future();
        }

        ot.print_time();

        std::cout << " --> put values to all those promises..." << std::endl;

        for (size_t i = 0; i < DIM_MAX; i++) {
            proms[i].set_value((double) i);
        }

        ot.print_time();

        std::cout << " --> getting the values from the futures..." << std::endl;

        auto the_values{std::vector<double>{}};
        the_values.resize(DIM_MAX);

        for (size_t i = 0; i < DIM_MAX; i++) {
            the_values[i] = futs[i].get();
        }

        ot.print_time();

        std::cout << " --> here are some values from futures which are fulfilled" << std::endl;
        std::cout << " --> the_values[0] = " << the_values[0] << std::endl;
        std::cout << " --> the_values[1] = " << the_values[1] << std::endl;
    }
}
