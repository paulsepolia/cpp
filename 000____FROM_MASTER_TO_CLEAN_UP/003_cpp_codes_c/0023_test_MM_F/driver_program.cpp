// new_handler example
#include <iostream>     // std::cout
#include <cstdlib>      // std::exit
#include <new>          // std::set_new_handler
#include <cmath>

void no_memory ()
{
    std::cout << "Failed to allocate memory!" << std::endl;
    std::exit (1);
}

int main ()
{
    const unsigned long int DIM(std::pow(10.0, 10.0));
    std::set_new_handler(no_memory);
    std::cout << "Attempting to allocate " << (8*DIM/1024.0/1024.0/1024.0) << " GiB... ";
    double * p(new double [DIM]);
    std::cout << "Ok" << std::endl;
    delete[] p;
    return 0;
}

