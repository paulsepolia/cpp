#include <iostream>
#include <unistd.h>
#include <iomanip>
#include <cmath>

using std::cout;
using std::endl;
using std::setprecision;
using std::fixed;
using std::right;
using std::setw;
using std::pow;

// type definitions

typedef unsigned long int uli;
typedef const unsigned long int culi;

// # 1

double RAM_total()
{
    double pages_all(sysconf(_SC_PHYS_PAGES));
    double pages_size(sysconf(_SC_PAGE_SIZE));
    double total_RAM = pages_all * pages_size;

    return total_RAM;
}

// # 2

double RAM_free()
{
    double pages_free(sysconf(_SC_AVPHYS_PAGES));
    double pages_size(sysconf(_SC_PAGE_SIZE));
    double free_RAM = pages_free * pages_size;

    return free_RAM;
}

// the main function

int main()
{
    // local settings and parameters

    cout << fixed;
    cout << setprecision(10);
    const double MEGAB(1024.0 * 1024.0);
    culi TRIALS(pow(10.0, 1.0));
    culi DIMEN(pow(10.0, 4.0));

    for(uli i = 0; i != TRIALS; i++) {
        cout << "---------------------------------------------------->> " << i << endl;
        cout << "----------------------->> BEFORE" << endl;

        double totalRAM = RAM_total();
        double freeRAM = RAM_free();

        cout << " --> totalRAM = " << setw(18) << right << totalRAM/MEGAB << endl;
        cout << " --> freeRAM  = " << setw(18) << right << freeRAM/MEGAB << endl;

        double* da(new double[DIMEN]);
        for(uli k = 0; k != DIMEN; k++) {
            da[k] = static_cast<double>(k);
        }

        cout << "----------------------->> AFTER" << endl;

        totalRAM = RAM_total();
        freeRAM = RAM_free();

        cout << " --> totalRAM = " << setw(18) << right << totalRAM/MEGAB << endl;
        cout << " --> freeRAM  = " << setw(18) << right << freeRAM/MEGAB << endl;

        cout << "----------------------->> DELETE" << endl;

    }

    return 0;
}
