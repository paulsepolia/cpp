// redirecting cout's output thrrough its stream buffer
#include <iostream>     // std::streambuf, std::cout
#include <fstream>      // std::ofstream
#include <iomanip>

int main ()
{
	const unsigned long int I_MAX(10000000);

    std::streambuf *psbuf, *backup;
    std::ofstream filestr;
    filestr.open ("test.txt");

    backup = std::cout.rdbuf();     // back up cout's streambuf

    psbuf = filestr.rdbuf();        // get file's streambuf
    std::cout.rdbuf(psbuf);         // assign streambuf to cout

	for(unsigned long int i = 1; i <= I_MAX; i++)
	{
    	std::cout << "This is written to the file --> " << std::setw(16) << i << std::endl;
	}

    std::cout.rdbuf(backup);        // restore cout's original streambuf

    filestr.close();

    return 0;
}



// end
