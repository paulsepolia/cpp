//======================//
// class Vec definition //
//======================//

#include "vec_declaration.hpp"
#include "errors.hpp"
#include <utility>
#include <iostream>
#include <string>
#include <cstdlib>

using std::move;
using std::cout;
using std::endl;
using std::string;

// constructor

template<typename T>
Vec<T>::Vec() : _size(0), _p()
{}

// destructor

template<typename T>
Vec<T>::~Vec()
{
    if(_p.get()) {
        _p.reset();
    }
}

// copy constructor

template<typename T>
Vec<T>::Vec(const Vec<T> & other)
{
    if(this != &other) {
        if(other._p.get()) {
            this->deallocate();
            this->allocate(other._size);
            for(uli i = 0; i != other._size; i++) {
                this->set(i, other.get(i));
            }
        } else if(other._p.get() == 0) {
            this->deallocate();
        }
    }
}

// move constructor

template<typename T>
Vec<T>::Vec(Vec<T> && other)
{
    this->_p = move(other._p);
}

// copy assignment operator

template<typename T>
Vec<T>  Vec<T>::operator=(const Vec<T> & other)
{
    if(this != &other) {
        if(other._p.get()) {
            this->deallocate();
            this->allocate(other._size);
            for(uli i = 0; i != other._size; i++) {
                this->set(i, other.get(i));
            }
        } else if(other._p.get() == 0) {
            this->deallocate();
        }
    }

    return *this;
}

// move assignment operator

template<typename T>
Vec<T> & Vec<T>::operator=(Vec<T> && other)
{
    this->_p = move(other._p);

    return *this;
}

//==================//
// member functions //
//==================//

// allocate

template<typename T>
void Vec<T>::allocate(const uli & val_size)
{
    this->deallocate();
    this->_p.reset(new T [val_size]);
    this->_size = val_size;
}

// check_allocation

template<typename T>
bool Vec<T>::check_allocation() const
{
    bool tmp_val = false;
    if(this->_p.get()) {
        tmp_val = true;
    }

    return tmp_val;
}

// deallocate

template<typename T>
void Vec<T>::deallocate()
{
    if(this->_p) {
        this->_p.reset();
    }
}

// get

template<typename T>
T Vec<T>::get(culi & index) const throw(string)
{
    if(!this->check_allocation()) {
        throw_error_and_quit(ERROR_001);
    }

    if(((this->size()-1) < index) || (index < 0)) {
        throw_error_and_quit(ERROR_002);
    }

    return this->_p.get()[index];
}

// set

template<typename T>
void Vec<T>::set(culi & index, const T & val) throw(string)
{
    if(!this->check_allocation()) {
        throw_error_and_quit(ERROR_001);
    }

    if(((this->size()-1) < index) || (index < 0)) {
        throw_error_and_quit(ERROR_002);
    }

    this->_p.get()[index] = val;
}

// operator[]

template<typename T>
T Vec<T>::operator[](culi & index)
{
    return this->get(index);
}

template<typename T>
uli Vec<T>::size() const
{
    return this->_size;
}

//==========================//
// private member functions //
//==========================//

// throw_error_and_quit

template<typename T>
string Vec<T>::throw_error_and_quit(string my_str) const
{
    try {
        throw my_str;
    } catch(string & error_code) {
        cout << error_code << endl;
        exit(EXIT_FAILURE);
    }
}

// end
