#!/bin/bash

    g++.7.2.0  -c                  \
			   -O3                 \
               -Wall               \
               -std=c++14          \
               driver_program.cpp

	g++.7.2.0  \
    -L/opt/Fotech/helios/lib -lboost_system -lboost_thread  \
    driver_program.o \
    -o x_gnu 
