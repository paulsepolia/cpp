#include <iostream>
#include <cmath>
#include <vector>
#include <iomanip>
#include <cstdlib>
#include <memory>

using std::cout;
using std::endl;
using std::pow;
using std::vector;
using std::fixed;
using std::setprecision;
using std::shared_ptr;

// parameters

const bool FLAG_DEBUG1(true);
const bool FLAG_DEBUG2(true);
const bool FLAG_DEBUG3(true);

// type definitions

typedef unsigned long int uli;
typedef const uli culi;

// static variables

static uli indexAllocate(0);
static uli indexDelete(0);

// my deleter

template<typename T>
class my_deleter {
public:
    void operator()(T * ptr)
    {
        if (ptr) {
            ::delete ptr;

            if(FLAG_DEBUG3) {
                cout << " ptr --> 3 = " << ptr << endl;
            }
            ptr = 0;
            if(FLAG_DEBUG3) {
                cout << " ptr --> 4 = " << ptr << endl;
            }
        }
    }
};

// overload the new operator

void* operator new(size_t sz) throw(std::bad_alloc)
{
    indexAllocate++;

    if(FLAG_DEBUG1) {
        cout << " --> inside new overloaded operator, indexAllocate = " << indexAllocate << endl;
    }

    return malloc(sz);
}

// overload the delete operator

void operator delete(void* ptr)
{

    indexDelete++;

    if(FLAG_DEBUG1) {
        cout << " --> overloaded delete, indexDelete = " << indexDelete << endl;
        cout << " ptr --> 1 = " << ptr << endl;
    }

    if(ptr) {
        free(ptr);
        ptr = 0;
    }

    if(FLAG_DEBUG2) {
        cout << " ptr --> 2 = " << ptr << endl;
        if(indexDelete == indexAllocate) {
            cout << " --> NO MEMORY LEAKS UP TO NOW" << endl;
        }
    }

    ptr = 0;
}

// the main program

int main()
{
    double * pd = new double(10);

    cout << "  pd = " <<  pd << endl;
    cout << " *pd = " << *pd << endl;

    delete pd;

    cout << "  pd = " <<  pd << endl;
    cout << " *pd = " << *pd << endl;

    return 0;
}

// end
