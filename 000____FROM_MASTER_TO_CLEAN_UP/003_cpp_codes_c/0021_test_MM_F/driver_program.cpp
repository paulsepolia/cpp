//=======================================//
// get output form a linux shell command //
//=======================================//

#include <iostream>
#include <stdexcept>
#include <stdio.h>
#include <string>

std::string exec(const char *cmd) {

    // local parameters

    const uint32_t SIZE(20000);

    // local variables

    char * buffer(new char [SIZE]);

    std::string result = "";

    FILE *pipe = popen(cmd, "r");

    if (!pipe) {
        throw std::runtime_error("popen() failed!");
    }

    try {
        while (!feof(pipe)) {
            if (fgets(buffer, SIZE, pipe) != NULL)
                result += buffer;
        }
    } catch (...) {
        pclose(pipe);
        throw;
    }

    pclose(pipe);

    return result;
}

int main() {

    std::string output(exec("python get_net.py"));

    std::cout << output << std::endl;

    return 0;
}