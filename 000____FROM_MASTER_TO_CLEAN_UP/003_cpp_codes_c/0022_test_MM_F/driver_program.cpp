// terminate example
#include <iostream>       // std::cout, std::cerr
#include <exception>      // std::exception, std::terminate
#include <cmath>

int main()
{
    double * p(0);
	const unsigned long int DIMEN(std::pow(10.0, 10.0));

    std::cout << "Attempting to allocate " << (sizeof(double) * DIMEN/1024.0/1024.0/1024.0) << " GiB... ";
    try {
        p = new double [DIMEN];
    } catch (std::exception& e) {
        std::cerr << "ERROR: could not allocate storage" << std::endl;
        std::terminate();
        std::cout << "TEST PHRASE" << std::endl;
    }
    std::cout << "Ok" << std::endl;
    delete[] p;
    return 0;
}


