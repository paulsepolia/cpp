#!/bin/bash

    g++-5.3.0  -O3                 \
               -Wall               \
               -std=c++14          \
               driver_program.cpp  \
               -o x_gnu_530
