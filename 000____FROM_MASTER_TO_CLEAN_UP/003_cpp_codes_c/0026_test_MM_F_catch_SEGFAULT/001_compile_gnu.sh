#!/bin/bash

    g++   -O3                \
          -Wall              \
          -std=c++0x         \
          driver_program.cpp \
          -o x_gnu
