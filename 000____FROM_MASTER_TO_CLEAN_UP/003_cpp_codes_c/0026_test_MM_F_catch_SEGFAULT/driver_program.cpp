#include <signal.h>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
    std::cout <<  "Caught segfault at address: " <<  si->si_addr << std::endl;
    exit(0);
}

int main()
{
    struct sigaction sa;

    memset(&sa, 0, sizeof(struct sigaction));
    sigemptyset(&sa.sa_mask);
    sa.sa_sigaction = segfault_sigaction;
    sa.sa_flags   = SA_SIGINFO;

    sigaction(SIGSEGV, &sa, NULL);

    // here the seg-fault
     
    const int I_DO_MAX(100000);
    int arr[10] = {1,2,3,4};

    for(int i = 0; i != I_DO_MAX; i++) {
        std::cout << " -------------->> i = " << i << std::endl;
        std::cout << arr[i] << std::endl;
    }
}

