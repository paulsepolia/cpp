//======================//
// class Vec definition //
//======================//

#include "vec_declaration.hpp"
#include <utility>
#include <iostream>

using std::move;
using std::cout;
using std::endl;

// constructor

template<typename T>
Vec<T>::Vec() : _size(0), _p(0) {}

// destructor

template<typename T>
Vec<T>::~Vec()
{
    cout << " --> this->_p = " << this->_p << endl;
    cout << " --> destructor --> 1" << endl;
    if(_p != 0) {
        delete _p;
        _p = 0;
    }
    cout << " --> this->_p = " << this->_p << endl;
    cout << " --> destructor --> 2" << endl;
}

// copy constructor

template<typename T>
Vec<T>::Vec(const Vec<T> & other)
{
    cout << " --> copy constructor --> 1" << endl;
    cout << " --> this->_p = " << this->_p << endl;
    cout << " --> other._p = " << other._p << endl;

    if(this != &other) {
        if(other._p != 0) {
            this->deallocate();
            this->allocate(other._size);
            for(uli i = 0; i != other._size; i++) {
                this->set(i, other.get(i));
            }
        } else if(other._p == 0) {
            this->deallocate();
        }
    }

    cout << " --> copy constructor --> 2" << endl;
    cout << " --> this->_p = " << this->_p << endl;
    cout << " --> other._p = " << other._p << endl;
}

// copy assignment operator

template<typename T>
Vec<T> & Vec<T>::operator=(const Vec<T> & other)
{
    cout << " --> copy operator= --> 1" << endl;
    cout << " --> this->_p = " << this->_p << endl;
    cout << " --> other._p = " << other._p << endl;

    if(this != &other) {
        if(other._p != 0) {
            this->deallocate();
            this->allocate(other._size);
            for(uli i = 0; i != other._size; i++) {
                this->set(i, other.get(i));
            }
        } else if(other._p == 0) {
            this->deallocate();
        }
    }

    cout << " --> copy operator= --> 2" << endl;
    cout << " --> this->_p = " << this->_p << endl;
    cout << " --> other._p = " << other._p << endl;

    return *this;
}

//==================//
// member functions //
//==================//

// allocate

template<typename T>
void Vec<T>::allocate(const uli & val_size)
{
    cout << " --> allocate --> 1" << endl;
    cout << " --> this->_p = " << this->_p << endl;

    this->deallocate();
    this->_p = new T [val_size];
    this->_size = val_size;

    cout << " --> allocate --> 2" << endl;
    cout << " --> this->_p = " << this->_p << endl;
}

// deallocate

template<typename T>
void Vec<T>::deallocate()
{
    cout << " --> deallocate --> 1" << endl;
    cout << " --> this->_p = " << this->_p << endl;

    if(this->_p != 0) {
        delete [] this->_p;
        this->_p = 0;
    }

    cout << " --> deallocate --> 2" << endl;
    cout << " --> this->_p = " << this->_p << endl;
}

// get

template<typename T>
T Vec<T>::get(const uli & index) const
{
    //cout << " --> get --> 1" << endl;
    return this->_p[index];
}

// set

template<typename T>
void Vec<T>::set(const uli & index, const T & val)
{
    //cout << " --> set --> 1" << endl;
    this->_p[index] = val;
}

// operator[]

template<typename T>
T Vec<T>::operator[](const uli & index)
{
    //cout << " --> operator[] --> 1" << endl;
    return this->get(index);
}

// end
