//==================================================//
// A simple server in the internet domain using TCP //
//==================================================//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <iostream>

int main() {

    // local parameters

    const int32_t PORT_NUM(57717);
    const uint64_t BUFFER_SIZE(20000000);

    // local variables

    int32_t socket_fd(0);
    int32_t new_socket_fd(0);
    socklen_t clilen;
    char *buffer(new char[BUFFER_SIZE]);
    struct sockaddr_in serv_addr;
    struct sockaddr_in cli_addr;
    int32_t n(0);

    socket_fd = socket(AF_INET, SOCK_STREAM, 0);

    if (socket_fd < 0) {
        std::cout << "ERROR opening socket" << std::endl;
    }

    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(PORT_NUM);

    if (bind(socket_fd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
        std::cout << "ERROR on binding" << std::endl;
    }

    listen(socket_fd, 5);

    clilen = sizeof(cli_addr);

    new_socket_fd = accept(socket_fd, (struct sockaddr *) &cli_addr, &clilen);

    if (new_socket_fd < 0) {
        std::cout << "ERROR on accept" << std::endl;
    }

    bzero(buffer, BUFFER_SIZE);
    n = read(new_socket_fd, buffer, BUFFER_SIZE - 1);

    if (n < 0) {
        std::cout << "ERROR reading from socket" << std::endl;
    }

    std::cout << "Here is the message: " << std::endl;
    std::cout << std::endl;

    std::cout << buffer << std::endl;

    n = write(new_socket_fd, "I got your message", 18);

    if (n < 0) {
        std::cout << "ERROR writing to socket" << std::endl;
    }

    close(new_socket_fd);
    close(socket_fd);
    delete[] buffer;
}
