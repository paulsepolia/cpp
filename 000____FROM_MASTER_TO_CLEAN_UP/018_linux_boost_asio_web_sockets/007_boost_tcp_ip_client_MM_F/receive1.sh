#!/bin/bash

PORT_LOC=1990
NUM_TO_RECV=10000
i=0

while [ $i -lt $NUM_TO_RECV ]
do
	echo "------------------------------->> receiving -->> " $i
	nc -lp $PORT_LOC > "out_file_1_"$i
    rm "out_file_1_"$i
	i=$[$i+1]
done
