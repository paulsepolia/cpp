#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <boost/thread.hpp>
#include <iostream>
#include <vector>
#include <cmath>
#include <chrono>
#include <thread>

boost::asio::io_service m_io_service;
boost::asio::ip::tcp::socket m_socket(m_io_service);
boost::thread *m_pReceiveThread;

void Receive();

void Connect() {

    boost::asio::ip::tcp::endpoint endpoint(boost::asio::ip::address::from_string("127.0.0.1"), 1990);
    m_socket.connect(endpoint);

    m_pReceiveThread = new boost::thread(Receive);
}

void Send() {

    std::wstring strData(L"Message");
    boost::system::error_code error;
    const std::size_t byteSize = boost::asio::write(m_socket, boost::asio::buffer(strData), error);
}


void Receive() {

    for (;;) {
        boost::array<wchar_t, 128> buf = {0};
        boost::system::error_code error;

        const std::size_t byteSize = m_socket.read_some(boost::asio::buffer(buf), error);

        // Dispatch the received data through event notification.
    }
}

int main() {

    Connect();

    while (true) {
        boost::this_thread::sleep(boost::posix_time::seconds(1));
        Send();
    }
}