#pragma once

#include <cstdint>
#include <vector>
#include <string>
#include <boost/asio.hpp>

struct data_msg_fields {

    int64_t counter;
    int64_t start_shot;
    int64_t end_shot;
    uint32_t start_bin;
    uint32_t end_bin;
    uint64_t last_sync_shot;
    std::string time_ref_time_str;
    int64_t time_ref_counter;
    double tr_requested_interval_us;
    size_t number_of_shots;
    size_t number_of_samples;
    double sampling_frequency;
    double sample_size;
    std::string time_of_first_sample;
    float scaleFactor;
    float addOffset;
    double monitor_start;
    double monitor_end;
    double cable_start;
    double cable_end;
    std::string data_type;
    std::string data_size;
    std::string channel;
};

class ReceiveFdelMessage {

public:

    explicit ReceiveFdelMessage();

    virtual ~ReceiveFdelMessage();

    std::string read_header_and_date_time_lines(boost::asio::ip::tcp::socket &socket);

    std::string get_header_line(const std::string &header_date_time) const;

    std::string get_header_date_time_line(const std::string &header_date_time) const;

    std::string read_header_length_line(boost::asio::ip::tcp::socket &socket);

    uint64_t get_header_length(const std::string &header_length_line) const;

    std::string read_header_msg(boost::asio::ip::tcp::socket &socket, const uint64_t &bytes_to_read);

    std::string read_data_and_date_time_lines(boost::asio::ip::tcp::socket &socket);

    std::string get_data_line(const std::string &data_date_time) const;

    std::string get_data_date_time_line(const std::string &data_date_time) const;

    std::string read_data_length_line(boost::asio::ip::tcp::socket &socket);

    uint64_t get_data_length(const std::string &data_length_line) const;

    std::string read_data_msg(boost::asio::ip::tcp::socket &socket, const uint64_t &bytes_to_read);

    void parse_data_msg(const std::string &data_msg, data_msg_fields &dmf);

    std::string read_binary_data_length_line(boost::asio::ip::tcp::socket &socket);

    uint64_t get_binary_data_length(const std::string &binary_data_length_line) const;

    std::string read_binary_data_msg(boost::asio::ip::tcp::socket &socket, const uint64_t &bytes_to_read);

    void read_binary_data_msg(boost::asio::ip::tcp::socket &socket,
                              const uint64_t &bytes_to_read,
                              char *where_to);
};


//lengthHeader=475
//Counter=0
//StartShot = 0
//EndShot = 64
//StartBin = 0
//EndBin = 741
//SyncShot = 0
//timeReference.time = 2019-01-07T15:21:24.473906Z
//timeReference.counter = 64
//timeReference.intervaluS = 2000
//NumberOfShots=64
//NumberOfSamples=741
//SamplingFrequency_measured=500
//SampleSize_m=0.6752082387
//TimeOfFirstSample=2019-01-07T15:21:24.345906Z
//scaleFactor=1
//addOffset=-32768
//monitorStart=0
//monitorEnd=500.3293049
//cableStart=0
//cableEnd=500.3293049
//dataType=unsigned short
//dataSize=2
//ADCChannel=A

