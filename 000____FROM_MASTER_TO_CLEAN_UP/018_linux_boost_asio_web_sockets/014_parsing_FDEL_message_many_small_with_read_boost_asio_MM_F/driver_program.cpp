#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <boost/container/vector.hpp>
#include <iostream>
#include <vector>
#include <cmath>
#include <chrono>
#include <thread>
#include <string>
#include "ReceiveFdelMessage.h"

int main(int argc, char *argv[]) {

    // local parameters

    const auto NUM_MSG = static_cast<uint64_t>(std::pow(10.0, 9.0));

    // local variables

    if (argc < 2) {
        std::cout << "usage: " << argv[0] << " port" << std::endl;
        exit(-1);
    }

    // get port number

    const auto portno = static_cast<uint16_t>(atoi(argv[1]));

    // set up the socket

    boost::asio::io_context io_context;
    boost::asio::ip::tcp::acceptor
            acceptor(io_context, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), portno));
    boost::asio::ip::tcp::socket socket(io_context);
    acceptor.accept(socket);

    // read the Fdel message

    ReceiveFdelMessage RFM;

    std::cout << "--> pgg ---------->>  1" << std::endl;

    const auto header_date_time = RFM.read_header_and_date_time_lines(socket);

    std::cout << header_date_time << std::endl;

    std::cout << "--> pgg ---------->>  2" << std::endl;

    const auto header = RFM.get_header_line(header_date_time);

    std::cout << header << std::endl;

    std::cout << "--> pgg ---------->>  3" << std::endl;

    const auto date_time = RFM.get_header_date_time_line(header_date_time);

    std::cout << date_time << std::endl;

    std::cout << "--> pgg ---------->>  4" << std::endl;

    const auto header_length_line = RFM.read_header_length_line(socket);

    std::cout << header_length_line << std::endl;

    std::cout << "--> pgg ---------->>  5" << std::endl;

    const auto header_length = RFM.get_header_length(header_length_line);

    std::cout << header_length << std::endl;

    std::cout << "--> pgg ---------->>  6" << std::endl;

    const auto header_msg = RFM.read_header_msg(socket, header_length);

    std::cout << header_msg << std::endl;

    for (uint64_t i = 0; i < NUM_MSG; i++) {

        std::cout << "--> i   ---------->>  " << i << std::endl;

        const auto data_and_date_time = RFM.read_data_and_date_time_lines(socket);

        std::cout << data_and_date_time << std::endl;

        std::cout << "--> pgg ---------->>  8" << std::endl;

        const auto data_word = RFM.get_data_line(data_and_date_time);

        std::cout << data_word << std::endl;

        if (data_word != "Data") {
            std::cout << " --> ERROR HERE --> " << data_word << std::endl;
            break;
        }

        std::cout << "--> pgg ---------->>  9" << std::endl;

        const auto data_date_time = RFM.get_data_date_time_line(data_and_date_time);

        std::cout << data_date_time << std::endl;

        std::cout << "--> pgg ---------->> 10" << std::endl;

        const auto data_length_line = RFM.read_data_length_line(socket);

        std::cout << data_length_line << std::endl;

        std::cout << "--> pgg ---------->> 11" << std::endl;

        const auto data_length = RFM.get_data_length(data_length_line);

        std::cout << data_length << std::endl;

        std::cout << "--> pgg ---------->> 12" << std::endl;

        const auto data_msg = RFM.read_data_msg(socket, data_length);

        std::cout << data_msg << std::endl;

        std::cout << "--> pgg ---------->> 13" << std::endl;

        const auto binary_data_length_line = RFM.read_binary_data_length_line(socket);

        std::cout << binary_data_length_line << std::endl;

        std::cout << "--> pgg ---------->> 14" << std::endl;

        const auto binary_data_length = RFM.get_binary_data_length(binary_data_length_line);

        std::cout << binary_data_length << std::endl;

        std::cout << "--> pgg ---------->> 15" << std::endl;

        char *bin_data_msg = new char[binary_data_length];

        RFM.read_binary_data_msg(socket, binary_data_length, bin_data_msg);

        for (size_t kk = 0; kk < binary_data_length; kk++) {
            std::cout << bin_data_msg[kk];
        }

        delete[] bin_data_msg;
    }

    std::cout << " pgg --> END HERE" << std::endl;

    return 0;
}
