#include <iostream>
#include "ReceiveFdelMessage.h"


const uint64_t BYTES_HEADER_DATE_TIME = 35;
const uint64_t BYTES_DATA_DATE_TIME = 33;
const char NEW_LINE_CHAR = '\n';
const char EQUAL_CHAR = '=';

ReceiveFdelMessage::ReceiveFdelMessage() = default;

ReceiveFdelMessage::~ReceiveFdelMessage() = default;

std::string ReceiveFdelMessage::read_header_and_date_time_lines(boost::asio::ip::tcp::socket &socket) {

    std::string header_date_time;

    header_date_time.resize(BYTES_HEADER_DATE_TIME);

    boost::asio::read(socket, boost::asio::buffer(&header_date_time[0], BYTES_HEADER_DATE_TIME));

    header_date_time.resize(BYTES_HEADER_DATE_TIME - 1);

    return header_date_time;
}

std::string ReceiveFdelMessage::get_header_line(const std::string &header_date_time) const {

    std::string header;

    for (const auto &el: header_date_time) {
        header.push_back(el);
        if (el == NEW_LINE_CHAR) break;
    }

    header.resize(header.size() - 1);

    return header;
}

std::string ReceiveFdelMessage::get_header_date_time_line(const std::string &header_date_time) const {

    const std::size_t pos = header_date_time.find_first_of(NEW_LINE_CHAR);

    return header_date_time.substr(pos + 1, header_date_time.size());
}

std::string ReceiveFdelMessage::read_header_length_line(boost::asio::ip::tcp::socket &socket) {

    std::string header_length_line;
    std::string str_buffer;
    str_buffer.resize(1);

    while (str_buffer[0] != NEW_LINE_CHAR) {
        socket.receive(boost::asio::buffer(str_buffer));
        header_length_line.push_back(str_buffer[0]);
    }

    header_length_line.resize(header_length_line.size() - 1);

    return header_length_line;
}

uint64_t ReceiveFdelMessage::get_header_length(const std::string &header_length_line) const {

    const std::size_t pos = header_length_line.find_first_of(EQUAL_CHAR);

    return std::stoul(header_length_line.substr(pos + 1, header_length_line.size()));
}

std::string ReceiveFdelMessage::read_header_msg(boost::asio::ip::tcp::socket &socket,
                                                const uint64_t &bytes_to_read) {

    std::string header_msg;
    header_msg.resize(bytes_to_read + 1);
    header_msg[0] = NEW_LINE_CHAR;

    boost::asio::read(socket, boost::asio::buffer(&header_msg[1], bytes_to_read));

    header_msg.resize(bytes_to_read);

    return header_msg;
}

std::string ReceiveFdelMessage::read_data_and_date_time_lines(boost::asio::ip::tcp::socket &socket) {

    std::string data_date_time;

    data_date_time.resize(BYTES_DATA_DATE_TIME);

    boost::asio::read(socket, boost::asio::buffer(&data_date_time[0], BYTES_DATA_DATE_TIME));

    data_date_time.resize(BYTES_DATA_DATE_TIME - 1);

    return data_date_time;
}

std::string ReceiveFdelMessage::get_data_line(const std::string &data_date_time) const {

    std::string data;

    for (const auto &el: data_date_time) {
        data.push_back(el);
        if (el == NEW_LINE_CHAR) break;
    }

    data.resize(data.size() - 1);

    return data;
}

std::string ReceiveFdelMessage::get_data_date_time_line(const std::string &data_date_time) const {

    const std::size_t pos = data_date_time.find_first_of(NEW_LINE_CHAR);

    return data_date_time.substr(pos + 1, data_date_time.size());
}

std::string ReceiveFdelMessage::read_data_length_line(boost::asio::ip::tcp::socket &socket) {

    std::string data_length_line;
    std::string str_buffer;
    str_buffer.resize(1);

    while (str_buffer[0] != NEW_LINE_CHAR) {
        socket.receive(boost::asio::buffer(str_buffer));
        data_length_line.push_back(str_buffer[0]);
    }

    data_length_line.resize(data_length_line.size() - 1);

    return data_length_line;
}

uint64_t ReceiveFdelMessage::get_data_length(const std::string &data_length_line) const {

    const std::size_t pos = data_length_line.find_first_of(EQUAL_CHAR);

    return std::stoul(data_length_line.substr(pos + 1, data_length_line.size()));
}

std::string ReceiveFdelMessage::read_data_msg(boost::asio::ip::tcp::socket &socket,
                                              const uint64_t &bytes_to_read) {

    std::string data_msg;

    data_msg.resize(bytes_to_read);

    boost::asio::read(socket, boost::asio::buffer(data_msg, bytes_to_read));

    data_msg.resize(bytes_to_read - 1);

    return data_msg;
}

void ReceiveFdelMessage::parse_data_msg(const std::string &data_msg, data_msg_fields &dmf) {

    //data_msg.find()

}

std::string ReceiveFdelMessage::read_binary_data_length_line(boost::asio::ip::tcp::socket &socket) {

    std::string binary_data_length_line;
    std::string str_buffer;
    str_buffer.resize(1);

    while (str_buffer[0] != NEW_LINE_CHAR) {
        socket.receive(boost::asio::buffer(str_buffer));
        binary_data_length_line.push_back(str_buffer[0]);
    }

    binary_data_length_line.resize(binary_data_length_line.size() - 1);

    return binary_data_length_line;
}

uint64_t ReceiveFdelMessage::get_binary_data_length(const std::string &binary_data_length_line) const {

    const std::size_t pos = binary_data_length_line.find_first_of(EQUAL_CHAR);

    return std::stoul(binary_data_length_line.substr(pos + 1, binary_data_length_line.size()));
}

std::string ReceiveFdelMessage::read_binary_data_msg(boost::asio::ip::tcp::socket &socket,
                                                     const uint64_t &bytes_to_read) {

    std::string binary_data_msg;

    binary_data_msg.resize(bytes_to_read);

    boost::asio::read(socket, boost::asio::buffer(&binary_data_msg[0], bytes_to_read));

    return binary_data_msg;
}

void ReceiveFdelMessage::read_binary_data_msg(boost::asio::ip::tcp::socket &socket,
                                              const uint64_t &bytes_to_read,
                                              char *where_to) {

    boost::asio::read(socket, boost::asio::buffer(where_to, bytes_to_read));
}