#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <iostream>

int main() {

    // local parameters

    const int32_t PORT_NUM(57717);
    const int32_t BUFFER_SIZE(256);
    const std::string HOST("127.0.0.1");

    // local variables

    int32_t socket_fd(0);
    int32_t n(0);
    struct sockaddr_in serv_addr;
    struct hostent *server;

    char buffer[BUFFER_SIZE];

    socket_fd = socket(AF_INET, SOCK_STREAM, 0);

    if (socket_fd < 0) {
        std::cout << "ERROR opening socket" << std::endl;
    }

    server = gethostbyname(HOST.c_str());

    if (!server) {
        std::cout << "ERROR, no such host" << std::endl;
        exit(0);
    }

    bzero((char *) &serv_addr, sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;

    bcopy((char *) server->h_addr, (char *) &serv_addr.sin_addr.s_addr, server->h_length);

    serv_addr.sin_port = htons(PORT_NUM);

    if (connect(socket_fd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
        std::cout << "ERROR connecting" << std::endl;
    }

    std::cout << "Please enter the message: " << std::endl;

    bzero(buffer, BUFFER_SIZE);
    fgets(buffer, BUFFER_SIZE-1, stdin);

    n = write(socket_fd, buffer, strlen(buffer));

    if (n < 0) {
        std::cout << "ERROR writing to socket" << std::endl;
    }

    bzero(buffer, BUFFER_SIZE);

    n = read(socket_fd, buffer, BUFFER_SIZE-1);

    if (n < 0) {
        std::cout << "ERROR reading from socket" << std::endl;
    }

    std::cout << buffer << std::endl;

    close(socket_fd);
}
