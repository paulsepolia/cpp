#!/bin/bash

  g++-9.1  -O3           \
           -Wall         \
           -std=gnu++2a  \
           client.cpp    \
           -o x_client
