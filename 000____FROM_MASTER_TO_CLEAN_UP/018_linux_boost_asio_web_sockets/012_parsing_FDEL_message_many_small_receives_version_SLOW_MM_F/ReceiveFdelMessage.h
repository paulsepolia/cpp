#pragma once

#include <cstdint>
#include <vector>
#include <string>
#include <boost/asio.hpp>

class ReceiveFdelMessage {

public:

    explicit ReceiveFdelMessage();

    virtual ~ReceiveFdelMessage();

    std::string read_header_and_date_time_lines(boost::asio::ip::tcp::socket & socket);

    std::string get_header_line(const std::string & header_date_time) const;

    std::string get_header_date_time_line(const std::string & header_date_time) const;

    std::string read_header_length_line(boost::asio::ip::tcp::socket & socket);

    uint64_t get_header_length(const std::string & header_length_line) const;

    std::string read_header_msg(boost::asio::ip::tcp::socket & socket, const uint64_t & bytes_to_read);

    std::string read_data_and_date_time_lines(boost::asio::ip::tcp::socket &socket);

    std::string get_data_line(const std::string & data_date_time) const;

    std::string get_data_date_time_line(const std::string & data_date_time) const;

    std::string read_data_length_line(boost::asio::ip::tcp::socket & socket);

    uint64_t get_data_length(const std::string & data_length_line) const;

    std::string read_data_msg(boost::asio::ip::tcp::socket & socket, const uint64_t & bytes_to_read);

    std::string read_binary_data_length_line(boost::asio::ip::tcp::socket & socket);

    uint64_t get_binary_data_length(const std::string & binary_data_length_line) const;

    std::vector<char> read_binary_data_msg(boost::asio::ip::tcp::socket & socket, const uint64_t & bytes_to_read);
};


