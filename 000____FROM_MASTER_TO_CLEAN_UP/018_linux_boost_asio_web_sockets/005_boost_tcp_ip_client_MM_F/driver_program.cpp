#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <iostream>
#include <vector>

// avoid the copy to the buffer

void send_something(std::string host,
                    uint32_t port,
                    std::string message) {

    // set up the socket
    boost::asio::io_service ios;
    boost::asio::ip::tcp::endpoint endpoint(boost::asio::ip::address::from_string(host), port);
    boost::asio::ip::tcp::socket socket(ios);

    // connect to the socket
    socket.connect(endpoint);

    boost::system::error_code error;

    // write to the socket
    socket.write_some(boost::asio::buffer(&message[0], message.size()), error);

    // close socket
    socket.close();
}

int main() {

    const std::string host("127.0.0.1");
    const uint32_t port(1990);
    const std::string message("hello flowers team ... \n");

    send_something(host, port, message);

    return 0;
}

