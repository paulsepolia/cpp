#!/bin/bash
	
  g++-9.2.0 -O3           \
    	    -Wall         \
     	    -std=gnu++2a  \
	    -I/opt/boost/1.70.0/include \
     	    -L/opt/boost/1.70.0/lib/ -lboost_system \
     	    -L/opt/boost/1.70.0/lib/ -lboost_thread \
     	    -lpthread \
      	    sender_boost_asio.cpp \
      	    -o x_sender_asio
