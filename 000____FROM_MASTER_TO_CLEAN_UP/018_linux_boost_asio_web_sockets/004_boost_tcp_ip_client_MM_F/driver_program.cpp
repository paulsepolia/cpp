#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <iostream>
#include <vector>

void send_something(std::string host,
                    uint32_t port,
                    std::string message) {

    // set up the socket
    boost::asio::io_service ios;
    boost::asio::ip::tcp::endpoint endpoint(boost::asio::ip::address::from_string(host), port);
    boost::asio::ip::tcp::socket socket(ios);

    // connect to the socket
    socket.connect(endpoint);

    // set up a buffer to write the data to send
    std::vector<char> buf_vec;
    buf_vec.resize(message.size());

    // copy the data from message to buffer
    std::copy(message.begin(), message.end(), buf_vec.begin());

    boost::system::error_code error;

    // write to the socket
    socket.write_some(boost::asio::buffer(buf_vec, message.size()), error);

    // close socket
    socket.close();
}

int main() {

    const std::string host("127.0.0.1");
    const uint32_t port(1990);
    const std::string message("hello flowers team ... \n");

    send_something(host, port, message);

    return 0;
}

