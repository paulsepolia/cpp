#include <iostream>
#include <iomanip>
#include <memory>
#include <cmath>


int main() {

    {
        // CORRECT WAY

        std::cout << " --> example --> 1 --> start" << std::endl;

        auto *pd = new double(200);

        std::shared_ptr<double> sp1;
        sp1.reset(pd);

        std::shared_ptr<double> sp2(sp1);

        std::cout << " -->  *pd --> " << *pd << std::endl;
        std::cout << " --> *sp1 --> " << *sp1 << std::endl;
        std::cout << " --> *sp2 --> " << *sp2 << std::endl;
        std::cout << " -->   pd --> " << pd << std::endl;
        std::cout << " -->  sp1 --> " << sp1 << std::endl;
        std::cout << " -->  sp2 --> " << sp2 << std::endl;

        std::cout << " --> example --> 1 --> end" << std::endl;
    }

    {
        // DYNAMIC ARRAY WRAPPED BY SHARED POINTER

        std::cout << " --> example --> 2 --> start" << std::endl;

        const auto DIM = static_cast<size_t>(std::pow(10.0, 2.0));
        std::shared_ptr<double[]> sp1(new double[DIM]);

        for (size_t i = 0; i < DIM; i++) {
            sp1[i] = static_cast<double>(i);
        }

        std::cout << sp1[0] << std::endl;
        std::cout << sp1[1] << std::endl;
        std::cout << sp1[2] << std::endl;

        std::cout << " --> example --> 2 --> end" << std::endl;
    }

    {
        // DYNAMIC ARRAY WRAPPED BY UNIQUE POINTER

        std::cout << " --> example --> 3 --> start" << std::endl;

        const auto DIM = static_cast<size_t>(std::pow(10.0, 2.0));
        std::unique_ptr<double[]> up1(new double[DIM]);

        for (size_t i = 0; i < DIM; i++) {
            up1[i] = static_cast<double>(i);
        }

        std::cout << up1[0] << std::endl;
        std::cout << up1[1] << std::endl;
        std::cout << up1[2] << std::endl;

        std::cout << " --> example --> 3 --> end" << std::endl;
    }

    {
        // DYNAMIC ARRAY WRAPPED BY UNIQUE POINTER

        std::cout << " --> example --> 4 --> start" << std::endl;

        const auto DIM = static_cast<size_t>(std::pow(10.0, 2.0));
        std::unique_ptr<double[]> up1 = std::make_unique<double[]>(DIM);

        for (size_t i = 0; i < DIM; i++) {
            up1[i] = static_cast<double>(i);
        }

        std::cout << up1[0] << std::endl;
        std::cout << up1[1] << std::endl;
        std::cout << up1[2] << std::endl;

        std::cout << " --> example --> 4 --> end" << std::endl;
    }

    {
        // available in C++20

        // DYNAMIC ARRAY WRAPPED BY SHARED POINTER

        std::cout << " --> example --> 5 --> start" << std::endl;

        const auto DIM = static_cast<size_t>(std::pow(10.0, 2.0));
        std::shared_ptr<double[]> sp1 = std::make_shared<double[]>(DIM);

        for (size_t i = 0; i < DIM; i++) {
            sp1[i] = static_cast<double>(i);
        }

        std::cout << sp1[0] << std::endl;
        std::cout << sp1[1] << std::endl;
        std::cout << sp1[2] << std::endl;

        std::cout << " --> example --> 5 --> end" << std::endl;
    }


    {
        // ERROR
        // double free or corruption

        std::cout << " --> example --> 6 --> start" << std::endl;

        auto *pd = new double(200);

        std::shared_ptr<double> sp1;
        sp1.reset(pd);

        std::shared_ptr<double> sp2;
        sp2.reset(pd);

        std::cout << " -->  *pd --> " << *pd << std::endl;
        std::cout << " --> *sp1 --> " << *sp1 << std::endl;
        std::cout << " --> *sp2 --> " << *sp2 << std::endl;
        std::cout << " -->   pd --> " << pd << std::endl;
        std::cout << " -->  sp1 --> " << sp1 << std::endl;
        std::cout << " -->  sp2 --> " << sp2 << std::endl;

        std::cout << " --> example --> 6 --> end" << std::endl;
    }
}