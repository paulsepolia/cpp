//========================================================//
// This is the file selection_sort.cpp.                   //
// The selection sort realization of the Sorting pattern. //
//========================================================//

#include <algorithm>
using std::swap;

// 1. Template function indexOfSmallest.

template <class T1, class T2>
T2 indexOfSmallest(const T1 a[], T2 startIndex, T2 sentinelIndex)
{
    T2 min = a[startIndex];
    T2 indexOfMin = startIndex;

    for (T2 index = startIndex + 1; index < sentinelIndex; index++) {
        if (a[index] < min) {
            min = a[index];
            indexOfMin = index; // min is the smallest of a[startIndex] through a[index]
        }
    }

    return indexOfMin;

}

// 2. Template function split.

template <class T1, class T2>
T2 split(T1 a[], T2 begin, T2 end)
{
    T2 index = indexOfSmallest(a, begin, end);
    swap(a[begin], a[index]);
    return (begin+1);
}

// 3. Template function join.

template <class T1, class T2>
void join(T1 a[], T2 begin, T2 splitPt, T2 end)
{
    // Nothing to do.
}

//==============//
// End of code. //
//==============//
