//=======================================================//
// Driver program to Divide-and-Conquer Sorting pattern. //
//=======================================================//

#include <iostream>

using std::cout;
using std::cin;
using std::endl;

#include "sort_pattern.cpp"
#include "selection_sort.cpp"

// 1. Function declaration.

template <class T1, class T2>
void fillArray(T1 a[], T2 size);

// 2. Main function.

int main()
{

    long k;
    const long NUM_DO = 1E3;

    for (k = 0; k < NUM_DO; k++) {
        cout << " ----------------------------------------------------------->> " << k << endl;

        cout << " 1. --> Sorts numbers from lowest to highest." << endl;

        cout << " 2. --> Building the array." << endl;

        const long DIMEN = 1*1E5;
        long* sampleArray = new long [DIMEN];

        fillArray(sampleArray, DIMEN);

        cout << " 3. --> Sorting the array. " << endl;

        sort(sampleArray, DIMEN);

        cout << " 4. --> In sorted order the number are: " << endl;

        for (long index = 0; index < 5; index++) {
            cout << sampleArray[index] << " ";
        }

        cout << endl;

        cout << " 5. --> In sorted order the number are: " << endl;

        for (long index = DIMEN-5; index < DIMEN; index++) {
            cout << sampleArray[index] << " ";
        }

        cout << endl;

        cout << " 6. --> Deleting the array. " << endl;

        delete [] sampleArray;

        cout << endl;
    }

    return 0;
}

// 3. Function definition.

template <class T1, class T2>
void fillArray(T1 a[], T2 size)
{
    T2 i;

    for (i = 0; i < size; i++) {
        a[i] = size-i;
    }

}
//==============//
// End of code. //
//==============//
