//=======================================================//
// Driver program to Divide-and-Conquer Sorting pattern. //
//=======================================================//

#include <iostream>

using std::cout;
using std::cin;
using std::endl;

#include "sort_pattern.cpp"
#include "merge_sort.cpp"

// 1. Function declaration.

template <class T1, class T2>
void fillArray(T1 a[], T2 size, T2& numberUsed);

// 2. Main function.

int main()
{
    cout << " This program sorts numbers from lowest to highest." << endl;

    long sampleArray[10];
    long numberUsed;

    long my_num = 10;

    fillArray(sampleArray, my_num, numberUsed);

    sort(sampleArray, numberUsed);

    cout << " In sorted order the number are: " << endl;

    for (long index = 0; index < numberUsed; index++) {
        cout << sampleArray[index] << " ";
    }

    cout << endl;

    return 0;
}


// 3. Function definition.

template <class T1, class T2>
void fillArray(T1 a[], T2 size, T2& numberUsed)
{
    cout << "Enter up to " << size << " nonnegative whole numbers." << endl;
    cout << "Mark the end of the list with a negative number." << endl;

    T2 next;
    T2 index = 0;

    cin >> next;

    while ((next >= 0) && (index < size)) {
        a[index] = next;
        index++;
        cin >> next;
    }

    numberUsed = index;
}

//==============//
// End of code. //
//==============//
