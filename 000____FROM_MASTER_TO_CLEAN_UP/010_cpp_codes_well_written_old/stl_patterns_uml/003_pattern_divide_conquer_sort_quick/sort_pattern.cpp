//====================================//
// This is the file sort_pattern.cpp. //
//====================================//

// 1.
// Template function split.
// Rearranges elements [begin, end) of array a into two intervals
// [begin, splitPt) and [splitPt, end), such that the Sorting pattern works.
// Returns splitPt.

template <class T1, class T2>
T2 split(T1 a[], T2 begin, T2 end);

// 2.
// Template function join.
// Combines the elements in the two intervals [begin, split) and
// [split, end) in such a way that the Sorting pattern works.

template <class T1, class T2>
void join(T1 a[], T2 begin, T2 splitPt, T2 end);

// 3.
// Template function sort.
// Precondition: Interval [begin, end) of a has elements.
// Postcondition: The values in the interval [begin, end) have
// been rearranged so that a[0] <= a[1] <= ... <= a[(end - begin) -1] .

template <class T1, class T2>
void sort(T1 a[], T2 begin, T2 end)
{
    if ((end - begin) > 1) {
        T2 splitPt = split(a, begin, end);
        sort(a, begin, splitPt);
        sort(a, splitPt, end);
        join(a, begin, splitPt, end);
    } // else sorting one of fewer elements, so do nothing.
}

// 4.
// Template function sort.
// Precondition: numberUsed <= declared size of the array a.
// The array elements a[0] through a[numberUsed - 1] have values.
// Postcondition: The values of a[0] through a[numberUsed -1] have
// been rearranged so that a[0] <= a[1] <= ... <= a[numberUsed -1].

template <class T1, class T2>
void sort(T1 a[], T2 numberUsed)
{
    T2 my_zero = 0;
    sort(a, my_zero, numberUsed);
}

//==============//
// End of code. //
//==============//
