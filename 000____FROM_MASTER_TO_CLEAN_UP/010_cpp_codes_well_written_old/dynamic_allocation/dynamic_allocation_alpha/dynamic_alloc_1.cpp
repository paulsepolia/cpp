#include <iostream>
using namespace std;

int main()
{
    long int i;
    const long int dimen = 10E+9;
    int sentinel;

    for ( i=0; i<dimen; i++) {
        int *num = new int;
        *num = 1;
        if ( *num == dimen-1 ) cout << *num << endl;
        delete num;
    }

    cout << " enter an integer to exit: ";
    cin >> sentinel;

    return 0;
}
