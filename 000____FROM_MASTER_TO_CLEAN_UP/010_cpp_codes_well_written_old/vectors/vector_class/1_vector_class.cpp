// This program illustrates how to use a vector object

#include<iostream>
#include<vector>

using namespace std;

int main()
{
    vector<int> intList;
    unsigned int i;

    intList.push_back(13);
    intList.push_back(75);
    intList.push_back(28);
    intList.push_back(35);

    cout << " List elements are: ";

    for ( i = 0; i < intList.size(); i++ ) {
        cout << intList[i] << endl;
    }

    cout << endl;

    for ( i = 0; i < intList.size(); i++ ) {
        intList[i] = intList[i] * 2;
    }

    for ( i = 0; i < intList.size(); i++ ) {
        cout << intList[i] << endl;
    }

    cout << endl;

    return 0;
}
