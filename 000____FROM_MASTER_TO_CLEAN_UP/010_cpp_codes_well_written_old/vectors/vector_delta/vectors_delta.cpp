
#include <iostream>
using std::cout;
using std::cin;
using std::endl;

#include <vector>
using std::vector;

void createVector( int );

int main()
{
    const int dimen = 200000000;
    const int trials = 100;

    for ( int i = 1; i < trials; i++  ) {
        createVector( dimen );
    }

    return 0 ;
}

void createVector( int dimen )
{
    vector< int > integers1( dimen );  // dimen-element vector < int >.
    // cout << integers1.size() << endl;
}