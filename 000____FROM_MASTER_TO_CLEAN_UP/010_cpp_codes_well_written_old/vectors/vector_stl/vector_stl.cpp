// this is a demonstration program of the vector stl class

#include <iostream>
#include <vector>
#include <iomanip>

using namespace std;

int main()
{
    vector<long double> vecAlpha; // define a vector to hold long doubles values
    long int i;
    int k;
    const long int iLim = 50000000;
    const long int k1Lim = 10;
    const long int k2Lim = 1000;
    const int k3Lim = 10000;

    // set the format

    cout << fixed << showpoint << setprecision(10);

    for ( int k = 0; k < k3Lim ; k++ ) {

        // fill up the vecAlpha with values

        for (i = 0; i < iLim; i++) {
            vecAlpha.push_back(static_cast<long double>(i));
        }

        // get some values of the vecAlpha

        for ( i = 0; i < k1Lim; i++ ) {
            cout << " " << i << " " << vecAlpha.at(i) << endl;
        }

        // get the capacity of the vecAlpha

        cout << " the capacity of the vecAlpha is : "
             << vecAlpha.capacity() << endl;

        // get the size of the vecAplha

        cout << " the size of the vecAlpha is : "
             << vecAlpha.size() << endl;

        // removes the elements of the vecAlpha

        for (i = 0; i < iLim; i++) {
            vecAlpha.pop_back();
        }

        // get the capacity of the vecAlpha

        cout << " the capacity of the vecAlpha is : "
             << vecAlpha.capacity() << endl;

        // get the size of the vecAplha

        cout << " the size of the vecAlpha is : "
             << vecAlpha.size() << endl;

        // fill up the vecAlpha with values

        for (i = 0; i < iLim; i++) {
            vecAlpha.push_back(static_cast<long double>(i));
        }

        // reverse the vecAlpha

        vecAlpha.resize(iLim+k2Lim,static_cast<long double>(99));

        // get some values of the vecAlpha

        cout << " " << iLim+1 << " " << vecAlpha.at(iLim+1) << endl;
        cout << " " << iLim+2 << " " << vecAlpha.at(iLim+2) << endl;
        cout << " " << iLim+3 << " " << vecAlpha.at(iLim+3) << endl;

        // copy the contents of vecAlpha to vecBeta

        vector<long double> vecBeta(vecAlpha);

        // get some values of the vecBeta

        cout << " about the vecBeta " << endl;
        cout << " " << iLim+1 << " " << vecBeta.at(iLim+1) << endl;
        cout << " " << iLim+2 << " " << vecBeta.at(iLim+2) << endl;
        cout << " " << iLim+3 << " " << vecBeta.at(iLim+3) << endl;

        // modify the vecBeta;

        long int sz = vecBeta.size();

        for (i = 0; i < vecBeta.size(); i++ ) {
            vecBeta[i] = sz-i ;
        }

        // get some values of the vecBeta

        cout << " about the vecBeta " << endl;
        cout << " " << iLim+1 << " " << vecBeta.at(iLim+1) << endl;
        cout << " " << iLim+2 << " " << vecBeta.at(iLim+2) << endl;
        cout << " " << iLim+3 << " " << vecBeta.at(iLim+3) << endl;

        // swap the values of the two vectors

        vecAlpha.swap(vecBeta);

        // get some values of the vecBeta

        cout << " about the vecBeta " << endl;
        cout << " " << iLim+1 << " " << vecBeta.at(iLim+1) << endl;
        cout << " " << iLim+2 << " " << vecBeta.at(iLim+2) << endl;
        cout << " " << iLim+3 << " " << vecBeta.at(iLim+3) << endl;

        cout << " --------------------------------------->>> " << k << endl;

    }

    return 0;
}
