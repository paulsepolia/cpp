//===================================//
// Header file.                      //
// HourlyEmployeeCLMemberFunctions.h //
//===================================//

// This is the implementation file for the class HourlyEmployeeCL
// The interface for the class is HourlyEmployeeCL is in
// the header file HourlyEmployeeCL.h

#ifndef HOURLY_EMPLOYEE_CL_MF_H
#define HOURLY_EMPLOYEE_CL_MF_H

#include <string>
#include <iostream>
using std::string;
using std::cout;
using std::endl;

#include "EmployeeCL.h"
#include "HourlyEmployeeCL.h"

namespace PGG {

// 1.

HourlyEmployeeCL::HourlyEmployeeCL()
    : EmployeeCL(),
      wageRate(0),
      hours(0)
{
    // deliberately empty
}

// 2.

HourlyEmployeeCL::HourlyEmployeeCL(string theName,
                                   string theNumber,
                                   double theWageRate,
                                   double theHours)
    : EmployeeCL(theName, theNumber),
      wageRate(theWageRate),
      hours(theHours)
{
    // deliberatelly empty
}

// 3.

void HourlyEmployeeCL::setRate(double newWageRate)
{
    wageRate = newWageRate;
}

// 4.

double HourlyEmployeeCL::getRate() const
{
    return wageRate;
}

// 5.

void HourlyEmployeeCL::setHours(double hoursWorked)
{
    hours = hoursWorked;
}

// 6.

double HourlyEmployeeCL::getHours() const
{
    return hours;
}

// 7.

void HourlyEmployeeCL::printCheck()
{
    setNetPay(hours * wageRate);

    cout << endl;
    cout << "-----------------------------------------------" << endl;;
    cout << "Pay to the order of " << getName() << endl;
    cout << "The sum of " << getNetPay() << " Dollars." << endl;
    cout << "-----------------------------------------------" << endl;
    cout << "Check Stub: NOT NEGOTIABLE" << endl;
    cout << "Employee Number: " << getSsn() << endl;
    cout << "Hourly Employee. \n Hours worked: " << hours
         << " Rate: " << wageRate << " Pay: " << getNetPay() << endl;
    cout << "-----------------------------------------------" << endl;
}

} // end of namespace PGG

#endif // end of guard HOURLY_EMPLOYEE_CL_MF_H

//==============//
// End of code. //
//==============//
