//======================================================//
// Cpp file.                                            //
// Driver program to SaleCL and DiscountSaleCL classes. //
//======================================================//

#include <iostream>
#include "SaleCL.h"
#include "SaleCLMemberFunctions.h"
#include "DiscountSaleCL.h"
#include "DiscountSaleCLMemberFunctions.h"

using std::cout;
using std::endl;
using std::ios;
using namespace PGG;

int main()
{
    SaleCL simple(10.00); // one item at $10.00
    DiscountSaleCL discount(11.00, 10); // one item at $11.00 with a 10% discount

    cout.setf(ios::fixed);
    cout.setf(ios::showpoint);
    cout.precision(2);

    if (discount < simple) {
        cout << "Discounted item is cheaper." << endl;
        cout << "Savings is $" << simple.savings(discount) << endl;
    } else {
        cout << "Discounted item is not cheaper." << endl;
    }

    return 0;
}

//==============//
// End of code. //
//==============//
