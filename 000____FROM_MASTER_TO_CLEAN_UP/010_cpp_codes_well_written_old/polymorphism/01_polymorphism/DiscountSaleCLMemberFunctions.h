//===================================================//
// Header file.                                      //
// DiscountSaleCL class member functions definition. //
//===================================================//

#ifndef DISCOUNTSALE_CL_MF_H
#define DISCOUNTSALE_CL_MF_H

#include "DiscountSaleCL.h"

namespace PGG {

// 1.

DiscountSaleCL::DiscountSaleCL()
    : SaleCL(), discount(0)
{
    // Intentionally empty
}

// 2.

DiscountSaleCL::DiscountSaleCL(double thePrice, double theDiscount)
    : SaleCL(thePrice), discount(theDiscount)
{
    // Intentionally empty
}

// 3.

double DiscountSaleCL::getDiscount() const
{
    return discount;
}


// 4.

void DiscountSaleCL::setDiscount(double newDiscount)
{
    discount = newDiscount;
}

// 5.

double DiscountSaleCL::bill() const
{
    double fraction = discount / 100;
    return (1 - fraction) * getPrice();
}

} // end of namespace PGG

#endif // end of guard DISCOUNTSALE_CL_MF_H

//==============//
// End of code. //
//==============//
