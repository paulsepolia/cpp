//================================//
// Header file.                   //
// SaleCL class member functions. //
//================================//

#ifndef Sale_CL_MF_H
#define Sale_CL_MF_H

#include <iostream>
#include <cstdlib>
#include "SaleCL.h"
using std::cout;
using std::endl;

namespace PGG {

// 1.

SaleCL::SaleCL()
    : price(0)
{
    // Intentionally empty
}

// 2.

SaleCL::SaleCL(double thePrice)
{
    if (thePrice >= 0) {
        price = thePrice;
    } else {
        cout << "Error: Cannot have a negative price!" << endl;
        exit(1);
    }
}

// 3.

double SaleCL::bill() const
{
    return price;
}

// 4.

double SaleCL::getPrice() const
{
    return price;
}

// 5.

void SaleCL::setPrice(double newPrice)
{
    if (newPrice >= 0) {
        price = newPrice;
    } else {
        cout << "Error: Cannot have a negative price!" << endl;
        exit(1);
    }
}

// 6.

double SaleCL::savings(const SaleCL& other) const
{
    return (bill() - other.bill());
}

// 7.

bool operator < (const SaleCL& first, const SaleCL& second)
{
    return (first.bill() < second.bill());
}

} // end of namespace PGG

#endif // end of guard Sale_CL_MF_H

//==============//
// End of code. //
//==============//
