
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>

using namespace std;

// 1. functions prototype

void openInput(  ifstream & ); // pass a reference to an ifstream object

void openOutput( ofstream & ); // pass a reference to an ofstream object

double pollenUpdate( ifstream &, ofstream & ); // pass two references

// 2. the main function

int main()
{
    // 3. variables definitions.

    ifstream  inFile; // inFile  is an ifstream object
    ofstream outFile; // outFile is an ofstream object
    double average;

    // 4. display a user message.

    cout << endl;
    cout << endl;
    cout << " This program reads the old pollen count file, " << endl;
    cout << " creates a current pollen count file, " << endl;
    cout << " and calculates and displays " << endl;
    cout << " the latest 10-week average. " << endl;

    // 5. open the file to be read and written.

    openInput( inFile );
    openOutput( outFile );

    average = pollenUpdate( inFile, outFile );

    cout << endl;
    cout << " The new 10-week average is: " << average << endl;

    // 6. normal fini.

    return 0;
}

// 7. function definition.
// This function gets an external filename
// and opens the file for input.

void openInput( ifstream & fname )
{
    string filename;

    cout << endl;
    cout << endl;
    cout << " Enter the input pollen count filename: ";

    cin  >> filename;

    fname.open( filename.c_str() );

    if ( fname.fail() ) { // check for a successful open
        cout << endl;
        cout << " Failed to open the file named " << filename << " for input. " << endl;
        cout << " Please check that this file exists. " << endl;

        exit(1); // abnormal termination of the program.
    }

    return ;
}

// 8. function definition.
// This function gets an external filename
// and opens the file for output.

void openOutput( ofstream & fname )
{
    string filename;

    cout << endl;
    cout << endl;
    cout << " Enter the output pollen count filename: ";

    cin  >> filename;

    fname.open( filename.c_str() );

    if ( fname.fail() ) { // check for a successful open
        cout << endl;
        cout << " Failed to open the file named " << filename << " for output. " << endl;

        exit(1); // abnormal termination of the program.
    }

    return ;
}

// 9. the following function reads the pollen file,
//    writes a new file,
//    and returns the weekly average

double pollenUpdate( ifstream & inFile, ofstream & outFile )
{
    const int POLNUMS = 10; // maximum number of pollen counts
    int i, polreading;
    int oldreading, newcount;
    double sum = 0;
    double average;

    // get the latest pollen count
    cout << "Enter the latest pollen count reading: ";
    cin  >> newcount;

    // read the oldest pollen count
    inFile >> oldreading;

    // read, sum, and write out the rest of the pollen counts
    for( i=0; i < POLNUMS; i++ ) {
        inFile >> polreading;
        sum += polreading;
        outFile << polreading << endl;
    }

    // write out the latest reading
    outFile << newcount << endl;

    // compute and display the new average
    average = (sum + newcount) / double(POLNUMS);

    // closing the streams
    inFile.close();
    outFile.close();

    cout << endl;
    cout << " The output file has been written. " << endl;

    return average;
}
