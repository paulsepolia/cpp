#include <iostream>
#include <fstream>
#include <cctype>

using namespace std;

// 1
// interfaces

void initialize( int &, int list[] );

void copyText( ifstream & intext,
               ofstream & outtext,
               char & ch,
               int list[] );

void characterCount( char ch,
                     int list[] );

void writeTotal( ofstream & outtext,
                 int lc,
                 int list[] );
// 2
// main function

int main()
{
    // step 1.
    // declare variables

    int lineCount;
    int letterCount[26];
    char ch;
    ifstream infile;
    ofstream outfile;

    // step 2.

    infile.open( "textin.txt" );

    // step 3.

    if ( !infile ) {
        cout << "Cannot open the input file."
             << endl;
        return 1;
    }

    // step 4.

    outfile.open( "textout.out" );

    // step 5.

    initialize( lineCount, letterCount );

    // step 6.

    infile.get(ch);

    // step 7.

    while( infile ) {
        copyText( infile,
                  outfile,
                  ch,
                  letterCount );

        lineCount++;

        infile.get(ch);
    }

    // step 8.

    writeTotal( outfile,
                lineCount,
                letterCount );

    // step 9.

    infile.close();
    outfile.close();

    return 0;
}

// 3
// external functions

// initialiaze

void initialize( int & lc,
                 int list[] )
{
    int j;
    lc = 0;

    for ( j = 0; j < 26; j++ ) {
        list[j] = 0;
    }
} // end initialize


// copyText

void copyText( ifstream & intext,
               ofstream & outtext,
               char & ch,
               int list[] )
{
    while ( ch !='\n') { // process the entire line
        outtext << ch; // output the character

        characterCount(ch, list); // call the function
        // character count

        intext.get(ch); // read the next character
    }

    outtext << ch;  // output the new line character
} // end copyText


// characterCount

void characterCount( char ch,
                     int list[] )
{
    int index;

    ch = toupper( ch );

    index = static_cast<int>(ch) -
            static_cast<int>('A');

    if ( 0 <= index & index < 26 ) {
        list[index]++;
    }
}

// writeTotal

void writeTotal( ofstream & outtext,
                 int lc,
                 int list[] )
{
    int index;

    outtext << endl << endl;
    outtext << "The number of lines = " << lc << endl;

    for( index = 0; index < 26; index++ ) {
        outtext << static_cast<char>( index +
                                      static_cast<int>('A') )
                << " count = " << list[index] << endl;
    }

} // end writeTotal





