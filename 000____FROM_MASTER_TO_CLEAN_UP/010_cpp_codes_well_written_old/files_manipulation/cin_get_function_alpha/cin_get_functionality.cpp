// this program uses the cin.get() to pause the program

#include <iostream>

using namespace std;

int main()
{
    char ch;
    cout << " This program has paused. Press Enter to continue. ";
    cin.get(ch);
    cout << " Thank you ! " << endl;

    return 0;
}

