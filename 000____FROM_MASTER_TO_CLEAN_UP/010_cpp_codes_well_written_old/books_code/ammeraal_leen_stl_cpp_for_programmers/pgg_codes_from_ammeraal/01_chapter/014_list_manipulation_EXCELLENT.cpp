// insertions and deletions in a list
#include <iostream>
#include <cstdlib>
#include <list>

using namespace std;

// function definition

void showList(const char *str, const list<double> &L)
{
    list<double>::const_iterator i;
    const int DIMEN = 10;
    int j;

    cout << str << endl << " ";

    j = 0;
    for (i = L.begin(); i != L.end(); ++i) {
        cout << *i << " ";
        j++;
        if ( j >= DIMEN ) break;
    }

    cout << endl;
}

// main function

int main()
{
    list<double> L;
    double x;
    list<double>:: iterator i;
    const double DIMEN1 = 1*10E8;
    long int j;
    long int k;
    int sentinel;

    cout << "Enter an integer to start the process: ";
    cin >> sentinel;

    cout << endl;
    cout << " 1. creating the list --> push_back " << endl;
    for (j = 0; j < DIMEN1; j++) {
        L.push_back(rand()/10E8);
    }
    showList(" Initial list:", L);


    for (k = 0; k < DIMEN1; k++) {

        cout << " " << 2+k << ". deleting the 1st element repetedly --> erase " << endl;
        for (j = 0; j < DIMEN1; j++) {
            i = L.begin();
            L.erase(i);
        }

        cout << " " << 2+k << ". building the list again --> push_back " << endl;
        for (j = 0; j < DIMEN1; j++) {
            L.push_back(rand()/10E8);
        }
    }

    cout << endl;
    showList("After this deletion (of second element):", L);

    cin >> sentinel;

    return 0;
}

// end of code
