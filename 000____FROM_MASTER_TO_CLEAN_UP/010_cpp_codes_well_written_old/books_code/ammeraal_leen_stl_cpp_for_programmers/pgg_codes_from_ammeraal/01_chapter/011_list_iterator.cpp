// reading and writing a variable number of
// nonzero integers (followed in the input by 0).

#include <iostream>
#include <cstdlib>
#include <list>
using namespace std;

int main()
{
    list<double> myList;
    list<double>::iterator i;
    double x;
    double sum_local;
    int sentinel;
    const long int dimen = 10E8;
    long int ic;

    cout << " Enter an integer to start the process:";
    cin >> sentinel;

    for (ic = 0; ic < dimen; ic++) {
        myList.push_back( static_cast<double>(rand() / 10E9) );
    }

    cout << " v.begin() = " << *myList.begin() << endl;
    cout << " v.end()   = " << *myList.end()   << endl;

    for (i = myList.begin(); i != myList.end(); i++) {
        sum_local = sum_local + (*i);
    }

    cout << " sum_local = " << sum_local << endl;

    cout << endl;

    return 0;
}

// end of code
