// reading and writing a variable number of
// nonzero integers (followed in the input by 0).

#include <iostream>
#include <list>
using namespace std;

int main()
{
    list<int> v;
    list<int>::iterator i;
    int x;

    cout << "Enter positive integers, followed by 0:\n";

    cin >> x;
    v.push_back(x);
    while( x != 0 ) {
        cin >> x;
        v.push_back(x);
    }

    for (i = v.begin(); i != v.end(); ++i) {
        cout << *i << " ";
    }

    cout << endl;

    return 0;
}

// end of code
