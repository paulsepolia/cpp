// The find_if algorithm demonstrated.

#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>

using namespace std;

// class

class CondObject {
public:
    bool operator()(int x)
    {
        return 3 <= x && x <= 8;
    }
};

// main function

int main()
{
    vector<double> v;
    const long SIZE_VEC = 10E7;
    long j;
    vector<double>::iterator i;

    for (j = 0; j < SIZE_VEC; j++) {
        v.push_back(rand()/10E3);
    }

//  copy(v.begin(), v.end(), ostream_iterator<double>(cout, " "));

    i = find_if(v.begin(), v.end(), CondObject());

    if (i != v.end())
        cout << "Found element: " << *i << endl;

    return 0;
}

// end of code
