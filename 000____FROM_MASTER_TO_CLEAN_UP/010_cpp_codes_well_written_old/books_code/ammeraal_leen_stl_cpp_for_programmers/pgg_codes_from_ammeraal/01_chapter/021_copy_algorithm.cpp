// Copy algorithm
// Copying a vector to a list.
// Overwrite mode.
#include <iostream>
#include <vector>
#include <list>

using namespace std;

int main()
{
    int a[4] = {10, 20, 30, 40};
    vector<int> v(a, a+4);                // initializing the vector using list a

    list<int> L(4);                      // a list of 4 elements
    copy(v.begin(), v.end(), L.begin()); // overwrite mode
    list<int>:: iterator i;

    for(i =L.begin(); i != L.end(); ++i) {
        cout << *i << " ";
    }
    cout << endl;

    return 0;
}

// end of code
