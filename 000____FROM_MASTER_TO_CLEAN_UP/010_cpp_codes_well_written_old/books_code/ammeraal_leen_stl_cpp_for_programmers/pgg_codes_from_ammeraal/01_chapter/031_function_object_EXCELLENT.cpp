// a very simple function object

#include <iostream>

using namespace std;

// the function object

class compare {
public:
    int operator()(int x, int y) const
    {
        return x > y ;
    }
};

// main function

int main()
{
    compare v;
    cout << v(2, 15) << endl;
    cout << compare()(5,3) << endl;
    cout << v.operator()(10,11) << endl;
    cout << endl;

    return 0;
}

// end of code


