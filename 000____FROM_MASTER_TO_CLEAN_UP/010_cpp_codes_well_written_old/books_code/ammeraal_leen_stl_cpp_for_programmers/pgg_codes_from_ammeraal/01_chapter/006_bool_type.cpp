
#include <iostream>

using namespace std;

int main()
{
    bool b;

    b = false;
    cout << " b = " << b << endl;

    b = true;
    cout << " b = " << b << endl;

    cout << " sizeof(bool) = " << sizeof(bool) << endl;
    cout << " sizeof(int) = " << sizeof(int) << endl;

    bool BB[100];

    cout << " With BB defined as bool BB[100], we have\n";
    cout << " sizeof(BB) = " << sizeof(BB) << endl;

    return 0;
}
