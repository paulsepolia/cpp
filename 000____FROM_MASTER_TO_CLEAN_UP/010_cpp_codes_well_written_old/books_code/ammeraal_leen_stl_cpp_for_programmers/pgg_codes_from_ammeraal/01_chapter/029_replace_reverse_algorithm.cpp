// replacing sequence elements
#include <iostream>
#include <string>
#include <algorithm>
#include <cstring>

using namespace std;

int main()
{
    char str[] = "abcabcabc";
    int n = strlen(str);

    cout << str << endl;
    replace(str, str+n, 'b', 'q');
    cout << str << endl;

    reverse(str, str+n);
    cout << str << endl;

    return 0;
}

// end of code
