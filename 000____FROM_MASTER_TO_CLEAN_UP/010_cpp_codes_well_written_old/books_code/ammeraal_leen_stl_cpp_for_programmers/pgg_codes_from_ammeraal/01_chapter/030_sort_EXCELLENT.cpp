// sorting in descending order
// using a compare function.

#include <iostream>
#include <algorithm>
#include <iterator>

using namespace std;

// function definition

bool comparefun(int x, int y)
{
    return (x > y);
}

// main function

int main()
{
    const int N = 10;
    int a[N] = {1, 2, 4, 10, 2, 34, 5, 6, 3431, 3124};

    cout << "Before sorting:\n";
    copy(a, a+N, ostream_iterator<int>(cout, " "));
    cout << endl;

    sort(a, a+N, comparefun);

    cout << " After sorting in descending order:\n";
    copy(a, a+N, ostream_iterator<int>(cout, " "));
    cout << endl;

    return 0;
}

// end of code

