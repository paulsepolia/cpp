// insertions and deletions in a list
#include <iostream>
#include <list>

using namespace std;

// function definition

void showList(const char *str, const list<int> &L)
{
    list<int>::const_iterator i;

    cout << str << endl << " ";

    for (i = L.begin(); i != L.end(); ++i) {
        cout << *i << " ";
    }

    cout << endl;
}

// main function

int main()
{
    list<int> L;
    int x;
    list<int>:: iterator i;

    cout << "Enter positive integers, followed by 0:\n";

    while(cin >> x, x != 0) {
        L.push_back(x);
    }
    showList("Initial list:", L);

    L.push_front(123);
    showList("After inserting 123 at the beginning:", L);

    i = L.begin();
    L.insert(++i, 456);
    showList("After inserting 456 at the second position:", L);

    i = L.end();
    L.insert(--i, 999);
    showList("After inserting 999 just before the end:", L);

    i = L.begin();
    x = *i;
    L.pop_front();
    cout << "Deleted at the beginning: " << x << endl;
    showList("After this deletion:", L);

    i = L.end();
    x = *(--i);
    L.pop_back();
    cout << "Deleted at the end:" << x << endl;
    showList("After this deletion:", L);

    i = L.begin();
    x = *(++i);
    cout << "To be deleted: " << x << endl;
    L.erase(i);
    showList("After this deletion (of second element):", L);

    return 0;
}

// end of code
