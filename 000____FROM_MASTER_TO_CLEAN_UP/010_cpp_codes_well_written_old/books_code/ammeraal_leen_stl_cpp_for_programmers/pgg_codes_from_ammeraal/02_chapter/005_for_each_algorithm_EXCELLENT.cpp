// The for_each algorithm.

#include <iostream>
#include <algorithm>

using namespace std;

// class definition

class display {
public:
    display(): i(0) {}
    void operator()(int x)
    {
        cout << " a[" << i++ << "] = " << x << endl;
    }

private:
    int i;
};

// main function

int main()
{
    const int N = 4;
    int a[N] = {2, 3, 1, 10};

    for_each(a, a+N, display());

    return 0;
}

// end of code
