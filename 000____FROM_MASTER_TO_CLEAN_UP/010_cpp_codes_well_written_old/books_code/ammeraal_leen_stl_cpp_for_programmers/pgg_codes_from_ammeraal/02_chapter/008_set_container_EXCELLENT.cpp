// 2 identical sets, created differently

#include <iostream>
#include <set>

using namespace std;

int main()
{
    set<double, less<double> > S, T;

    const long SIZE_SET = 10E7;
    long i;
    int k = 0;


    cout << endl;
    cout << "  1. Building the S set. " << endl;
    for (i = 0; i < SIZE_SET; i++) {
        S.insert(static_cast<double>(SIZE_SET-i));
        S.insert(static_cast<double>(i+1));
    }

    cout << endl;
    cout << "  2. Building the T set. " << endl;
    for (i = 0; i < SIZE_SET; i++) {
        T.insert(static_cast<double>(i+1));
        T.insert(static_cast<double>(SIZE_SET-i));
    }

    cout << endl;
    cout << "  3. Comparison of the 2 sets. " << endl;
    if (S == T) cout << "     Equal sets. " << endl;

    cout << endl;
    cout << "  4. The first 10 elements are: " << endl;

    set<double, less<double> >::iterator j;
    for (j = T.begin(); j != T.end(); j++) {
        cout << *j << "  ";
        k++;
        if (k > 10) break;
    }

    cout << endl;
    return 0;
}

// end of code
