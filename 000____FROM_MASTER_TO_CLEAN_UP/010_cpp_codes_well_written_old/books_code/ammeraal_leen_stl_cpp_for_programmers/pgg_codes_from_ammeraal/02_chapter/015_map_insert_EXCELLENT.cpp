// fast insertion in a map
#include <iostream>
#include <map>

using namespace std;

typedef map<long, double, less<long> > maptype;

int main()
{
    maptype S;
    maptype T;
    maptype::iterator i;
    long k;
    long j;
    const long SIZE_MAP = 2*10E6;
    const long TRIALS = 10E1;

    for (j = 1; j <= TRIALS; j++) {
        i = S.begin();

        cout << endl;
        cout << " ------------------------------> " << j << endl;

        cout << endl;
        cout << "  1. Inserting - Building the S map. " << endl;
        for (k = 1; k <= SIZE_MAP; k++) {
            i = S.insert(i, maptype::value_type(k, 1.0L/k));     // VERY FAST
        }

        cout << endl;
        cout << "  2. Inserting - Building the T map. " << endl;
        for (k = 1; k <= SIZE_MAP; k++) {
            T[k] = 1.0L/k;     // VERY SLOW
        }

        cout << endl;
        cout << "  3. Comparison of S and T maps. " << endl;
        cout << bool(T == S) << endl;

        cout << endl;
        cout << "  4. Searching  the S map. " << endl;
        i = S.find(j);  // SAME SPEED

        cout << endl;
        cout << "  5. The results. " << endl;
        cout << (*i).second << endl;
        cout << (*i).first << endl;

        cout << endl;
        cout << "  6. Searching  the T map. " << endl;
        i = T.find(j);  // SMAE SPEED

        cout << endl;
        cout << "  7. The results. " << endl;
        cout << (*i).second << endl;
        cout << (*i).first << endl;

        cout << endl;
        cout << "  8. Erasing the whole S." << endl;
        S.erase(S.begin(), S.end());

        cout << endl;
        cout << "  9. Erasing the whole T." << endl;
        T.erase(T.begin(), T.end());
    }

    return 0;
}

// end of code

