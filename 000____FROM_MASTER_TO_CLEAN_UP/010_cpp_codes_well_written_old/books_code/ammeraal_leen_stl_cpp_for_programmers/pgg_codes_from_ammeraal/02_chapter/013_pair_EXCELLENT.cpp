// operations on pairs

#include <iostream>
#include <utility>

using namespace std;

int main()
{
    pair<long, double> P, Q;

    P = make_pair(123, 4.5);
    Q = P;

    cout << " P: " << P.first << " " << P.second << endl;
    cout << " Q: " << Q.first << " " << Q.second << endl;

    Q = make_pair(122, 4.5);

    if (P > Q) cout << " P > Q\n";
    ++Q.first;
    cout << "After ++Q.first: ";
    if (P == Q ) cout << "P == Q\n";

    cout << endl;
    return 0;
}

// end of code

