// First application of a map.

#include <iostream>
#include <map>

using namespace std;

// the class

class compare {
public:
    bool operator()(const double x, const double y) const
    {
        return x < y ;
    }
};

// the main function

int main()
{
    map<double, double, compare> D;
    const long MAP_DIMEN = 10E6;
    double x;
    long j;

    for (j = 0; j < MAP_DIMEN; j++) {
        D[ static_cast<double>(j) ] = static_cast<double>(j+1);
    }

    cout << " Enter a double: ";
    cin >> x;

    if (D.find(x) != D.end()) {
        cout << " The number is " << D[x];
    } else {
        cout << " Not Found.";
    }

    cout << endl;

    return 0;
}

// end of code

