// outiter.cpp: Output iterator; assignment statements
//              reading data from a file.
// From: Ammeraal, L. (1997) STL for C++ Programmers,
//       Chichester: John Wiley.
#include <iostream>
#include <vector>
#include <iterator>
using namespace std;

int main()
{  
#if defined(__BORLANDC__) &&  __BORLANDC__ <= 0x530
   ostream_iterator<int, char, char_traits<char> > i(cout, "abc\n");
#else
   ostream_iterator<int> i(cout, "abc\n");
#endif
   *i++ = 123;
   *i++ = 456;
   cout << endl;
   return 0;
}
