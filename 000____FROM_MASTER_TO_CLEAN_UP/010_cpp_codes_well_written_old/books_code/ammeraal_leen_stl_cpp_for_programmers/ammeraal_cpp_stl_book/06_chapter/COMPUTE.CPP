// compute.cpp: The transform algorithm and a 
//              function object of our own.
// From: Ammeraal, L. (1997) STL for C++ Programmers,
//       Chichester: John Wiley.
#include <iostream>
#include <algorithm>
#include <functional>
using namespace std;

struct compute: binary_function<int, int, int> {
   int operator()(int x, int y)const{return x + 2 * y;}
};

int main()
{  int a[5] = {10, 20, -18, 40, 50}, 
       b[5] = { 2,  2,   5,  3,  1}, result[5];
   transform(a, a + 5, b, result, compute());
   for (int i=0; i<5; i++) cout << result[i] << "  ";
      // Output: 14  24  -8  46  52
   cout << endl;
   return 0;
}
