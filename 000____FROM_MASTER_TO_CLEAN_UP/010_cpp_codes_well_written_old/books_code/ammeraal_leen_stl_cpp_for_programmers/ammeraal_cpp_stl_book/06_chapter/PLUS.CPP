// plus.cpp: The transform algorithm and plus<T>.
// From: Ammeraal, L. (1997) STL for C++ Programmers,
//       Chichester: John Wiley.
#include <iostream>
#include <algorithm>
#include <functional>
using namespace std;

int main()
{  int a[5] = {10, 20, -18, 40, 50}, 
       b[5] = { 2,  2,   5,  3,  1}, s[5];
   transform(a, a + 5, b, s, plus<int>());
   for (int i=0; i<5; i++) cout << s[i] << "  ";
    // Output: 12  22  -13  43  51
   cout << endl;
   return 0;
}
