// strelem.cpp: Strings as elements of list L and 
//   array a.
// From: Ammeraal, L. (1997) STL for C++ Programmers,
//       Chichester: John Wiley.
#include <iostream>
#include <string>
#include <list>
#include <algorithm>
using namespace std;

int main()
{  string s("One"), t("Two"), u("Three");
   list<string> L;
   L.push_back(s);
   L.push_back(t);
   L.push_back(u);
   string a[3]; 
   copy(L.begin(), L.end(), a);
   for (int k=0; k<3; k++) cout << a[k] << endl;
   return 0;
}
