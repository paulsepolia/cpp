// auto_ptr.cpp: Data is pointed to only once.
// From: Ammeraal, L. (1997) STL for C++ Programmers,
//       Chichester: John Wiley.
#include <iostream>
#include <memory>
using namespace std;

int main()
{  auto_ptr<int> a(new int), b;
   *a.get() = 123;
   cout << "*a.get() = " << *a.get() << endl; 
   b = a;
   cout << "The assignment b = a has been executed.\n";
   if (a.get() == NULL) 
      cout << "As a result, a.get() is NULL.\n";
   cout << "*b.get() = " << *b.get() << endl; 
   return 0;
}
