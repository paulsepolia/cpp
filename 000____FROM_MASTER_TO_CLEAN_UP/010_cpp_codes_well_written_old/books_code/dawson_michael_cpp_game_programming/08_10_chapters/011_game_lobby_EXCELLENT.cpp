// Game Lobby
// Simulates a game lobby where players wait

#include <iostream>
#include <string>

using namespace std;

//=========================//
// Player class definition //
//=========================//

class Player {
public:
    Player(const string& name="");
    string GetName() const;
    Player* GetNext() const;
    void SetNext(Player* next);

private:
    string m_Name;
    Player* m_pNext;  // pointer to next player in list
};

//==========================================//
// Player class member functions definition //
//==========================================//

//  1.

Player::Player(const string& name):
    m_Name(name),
    m_pNext(NULL)
{}

//  2.

string Player::GetName() const
{
    return m_Name;
}

//  3.

Player* Player::GetNext() const
{
    return m_pNext;
}

//  4.

void Player::SetNext(Player* next)
{
    m_pNext = next;
}

//========================//
// Lobby class definition //
//========================//

class Lobby {

    friend ostream& operator<<(ostream& os, const Lobby& aLobby);

public:
    Lobby();
    ~Lobby();
    void AddPlayer();
    void RemovePlayer();
    void Clear();

private:
    Player* m_pHead;

};

//=========================================//
// Lobby class member functions definition //
//=========================================//

//  1.

Lobby::Lobby():
    m_pHead(NULL)
{}

//  2.

Lobby::~Lobby()
{
    Clear();
}

//  3.

void Lobby::AddPlayer()
{
    // create a new player none
    cout << " Please enter the name of the new player: ";
    string name;
    cin >> name;
    Player* pNewPlayer = new Player(name);

    // if list is empty, make head of list this player
    if (m_pHead == 0) {
        m_pHead = pNewPlayer;
    }
    //otherwise find the end of the list and add the player there
    else {
        Player* pIter = m_pHead;
        while (pIter -> GetNext() != 0) {
            pIter = pIter -> GetNext();
        }
        pIter -> SetNext(pNewPlayer);
    }
}

//  4.

void Lobby::RemovePlayer()
{
    if (m_pHead == 0) {
        cout << "The game lobby is empty. No one to remove!" << endl;
    } else {
        Player* pTemp = m_pHead;
        m_pHead = m_pHead -> GetNext();
        delete pTemp;
    }
}

//  5.

void Lobby::Clear()
{
    while (m_pHead != 0) {
        RemovePlayer();
    }
}

//  6.

ostream& operator<<(ostream& os, const Lobby& aLobby)
{
    Player* pIter = aLobby.m_pHead;

    os << "\n Here is who is in the game Lobby:\n";

    if (pIter == 0) {
        os << "The lobby is empty.\n";
    } else {
        while (pIter !=0) {
            os << pIter -> GetName() << endl;
            pIter = pIter ->GetNext();
        }
    }

    return os;
}

//===================//
// the main function //
//===================//

int main()
{
    Lobby myLobby;
    int choice;

    do {
        cout << myLobby;
        cout << "\nGAME LOBBY\n";
        cout << "0 - Exit the program.\n";
        cout << "1 - Add a player to the lobby.\n";
        cout << "2 - Remove aplayer from the lobby.\n";
        cout << "3 - Clear the lobby.\n";
        cout << endl << "Enter a choice: ";

        cin >> choice;

        switch(choice) {
        case 0:
            cout << "Good-bye.\n";
            break;
        case 1:
            myLobby.AddPlayer();
            break;
        case 2:
            myLobby.RemovePlayer();
            break;
        case 3:
            myLobby.Clear();
            break;
        default:
            cout << "That was not a valid choice.\n";
        }
    } while(choice != 0);

    return 0;
}

// end of code
