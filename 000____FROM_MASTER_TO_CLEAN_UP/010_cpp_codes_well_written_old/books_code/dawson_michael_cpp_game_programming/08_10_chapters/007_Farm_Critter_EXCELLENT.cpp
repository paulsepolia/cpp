// Critter Farm
// Demonstrates objects containment

#include <iostream>
#include <string>
#include <vector>

using namespace std;

// class definition

class Critter {
public:
    Critter(const string& name=""); // constructor declaration
    string GetName() const; // public constant member function

private:
    string m_Name; // private member data
};

// constructor definition

Critter::Critter(const string& name):
    m_Name(name)
{}

// public constant member function definition

inline string Critter::GetName() const
{
    return m_Name;
}

//===================================================================
// class definition

class Farm {
public:
    Farm(int spaces = 1); // constructor declaration
    void Add(const Critter& aCritter); // public member function
    void RollCall() const; // public member function

private:
    vector<Critter> m_Critters; // private member data
};

// constructor definition

Farm::Farm(int spaces)
{
    m_Critters.reserve(spaces);
}

// function definition

void Farm::Add(const Critter& aCritter)
{
    m_Critters.push_back(aCritter);
}

// function definition

void Farm::RollCall() const
{
    vector<Critter>::const_iterator iter;
    int i=0;
    const int DD = 10;
    for (iter = m_Critters.begin(); iter != m_Critters.end(); ++iter) {
        cout << (*iter).GetName() << " here." << endl;
        i++;
        if (i > DD) break;
    }
}

// main function

int main()
{
    Critter crit("Poochie");
    cout << "My critter's name is " << crit.GetName() << endl;

    cout << "\nGreating critter farm.\n";
    Farm myFarm(3);

    cout << "\nAdding three critters to the farm.\n";
    myFarm.Add(Critter("Moe"));
    myFarm.Add(Critter("Larry"));
    myFarm.Add(Critter("Curly"));

    long i;
    const long DIM = 2*10E6;
    for(i = 0; i < DIM; i++) {
        myFarm.Add(Critter("MOEEEE"));
    }

    cout << "\nCalling Roll...\n";
    myFarm.RollCall();

    return 0;
}

// end of code
