// Demonstrates vectors

#include <iostream>
#include <string>
#include <vector>
#include <iterator>

using namespace std;

int main()
{

    vector<string> inventory;

    inventory.push_back("sword");
    inventory.push_back("armor");
    inventory.push_back("shield");

    vector<string>::iterator vi;

    const long SIZE_VEC = 3*10E6;
    long j;

    cout << endl;
    cout << "  1. The size of the vector 'inventory' is ";
    cout << inventory.size() << endl;

    cout << endl;
    cout << "  2. The contents of the vector 'inventory' are ";
    copy (inventory.begin(), inventory.end(), ostream_iterator<string>(cout, ", ") );
    cout << endl;

    cout << endl;
    cout << "  3. The contents again are: " << endl;
    for (vi = inventory.begin(); vi != inventory.end(); ++vi) {
        cout << *vi << endl;
    }

    cout << endl;
    cout << "  4. Building a large inventory vector. " << endl;
    for (j = 0; j < SIZE_VEC; j++) {
        inventory.push_back("AAAAAAAAA");
    }

    cout << endl;
    cout << "  5. The size of the vector 'inventory' is ";
    cout << inventory.size() << endl;

    cout << endl;
    cout << "  6. Deleting the last element. " << endl;
    inventory.pop_back();
    cout << inventory.size() << endl;

    cout << endl;
    cout << "  7. Deleting using pop_back repeatedly. " << endl;
    for (;;) {
        inventory.pop_back();
        if (inventory.size() == 0) break;
    }
    cout << inventory.size() << endl;

    cout << endl;
    cout << "  8. Building a large inventory vector. " << endl;
    for (j = 0; j < SIZE_VEC; j++) {
        inventory.push_back("AAAAAAAAA");
    }

    cout << endl;
    cout << "  9. Deleting using the member function: clear() " << endl;
    inventory.clear();
    cout << inventory.size() << endl;

    cout << endl;
    return 0;
}

// end of code

