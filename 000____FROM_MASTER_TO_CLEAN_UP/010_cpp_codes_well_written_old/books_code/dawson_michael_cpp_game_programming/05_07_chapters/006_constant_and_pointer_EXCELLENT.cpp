// constant pointer
// pointer to a constant

#include <iostream>

using namespace std;

int main()
{
//=============================================================================
// constant pointer ==> constant address that points
// den mporo na allajw thn address tou pointer
//=============================================================================

    int score = 100;
    int* const pScore = &score; // a constant pointer ==> address is fixed

    cout << " score   = " << score   << endl;
    cout << " *pScore = " << *pScore << endl;
    cout << " pScore  = " << pScore  << endl;
    cout << " &score  = " << &score  << endl;

    // pScore = &score; // error --> pScore is not modifiable

    score = 200;

    cout << " score   = " << score   << endl;
    cout << " *pScore = " << *pScore << endl;
    cout << " pScore  = " << pScore  << endl;
    cout << " &score  = " << &score  << endl;

    // &score = &score + 100; // error --> the address is fixed by definition

    int newScore = 11;

    // pScore = &newScore; // error --> pScore is fixed

    *pScore = 11;

    cout << " score   = " << score   << endl;
    cout << " *pScore = " << *pScore << endl;
    cout << " pScore  = " << pScore  << endl;
    cout << " &score  = " << &score  << endl;

//=============================================================================
// pointer to a constant
// den mporo na allajw thn timi pou blepei o pointer mesw autou
//=============================================================================

    cout << endl << endl << endl << endl;

    const int* pNumber; // declares apointer to a constant
    int number = 100;

    pNumber = &number;

    cout << " number   = " << number   << endl;
    cout << " *pNumber = " << *pNumber << endl;
    cout << " pNumber  = " << pNumber  << endl;
    cout << " &number  = " << &number  << endl;

    // *pNumber = 100; // error --> *pNumber is not modifiable

    number = 122;

    cout << " number   = " << number   << endl;
    cout << " *pNumber = " << *pNumber << endl;
    cout << " pNumber  = " << pNumber  << endl;
    cout << " &number  = " << &number  << endl;

//=============================================================================
// constant pointer to a constant
// den mporw na allajw thn address tou pointer
// alla oute kai thn time pou blepei mesw autou
//=============================================================================

    cout << endl << endl << endl << endl;

    int A = 123;
    const int* const pA = &A; // declares a constant pointer to a constant

    cout << " A   = " << A   << endl;
    cout << " *pA = " << *pA << endl;
    cout << " pA  = " << pA  << endl;
    cout << " &A  = " << &A  << endl;

    A = 200;

    cout << " A   = " << A   << endl;
    cout << " *pA = " << *pA << endl;
    cout << " pA  = " << pA  << endl;
    cout << " &A  = " << &A  << endl;

    return 0;
}

// end of code

