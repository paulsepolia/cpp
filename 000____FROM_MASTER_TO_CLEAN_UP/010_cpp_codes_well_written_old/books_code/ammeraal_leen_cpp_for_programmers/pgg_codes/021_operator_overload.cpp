//====================//
// Operator overload. //
//====================//

#include <iostream>

using std::endl;
using std::cout;
using std::ostream;

// 1.

class vec {
public:

    // a.

    vec(double xx = 0, double yy = 0)
        : x(xx), y(yy)
    {}

    // b.

    friend ostream &operator<<(ostream &os, const vec &v);

    // c.

    vec operator+(const vec &b) const
    {
        return vec(x + b.x, y + b.y);
    }

private:
    double x;
    double y;
};

// 2.

ostream &operator<<(ostream &os, const vec &v)
{
    os << "(" << v.x << ", " << v.y << ")";

    return os;
}

// 3. The main function
int main()
{
    vec u(3, 1);
    vec v(1, 2);
    vec s;

    s = u + v; // this is the overloaded + operator

    cout << " The sum of (3, 1) and (1, 2) is "
         << s  // this is the overloaded << operator
         << endl;

    return 0;
}

//==============//
// End of code. //
//==============//
