//========================================//
// Conversion from derived to base class. //
//========================================//

#include <iostream>

using std::cout;
using std::endl;

// 1. Class B declaration and definition.

class B {
public:
    int i;

    B(int ii = 0)
        : i(ii)
    {}
};

// 2. Class D declaration and definition.

class D: public B {
public:
    double x;
    D(double xx)
        : B(1), x(xx)
    {}
};

// 3. The main function.

int main()
{
    B b(2);
    B *pb = &b;
    D d(3.4);
    D *pd = &d;

    // 1. Case alpha.

    cout << endl;

    cout << " 1 --> Without any converion. " << endl;

    cout << endl;

    cout << " b --> b.i = " << b.i << endl;
    cout << " d --> d.x = " << d.x << endl;

    cout << endl;

    cout << " 2 --> Executing the assignment b = d. " << endl;

    b = d; // from derived to base class. OKAY.

    cout << endl;

    cout << " b --> b.i     = " << b.i     << endl;
    cout << " d --> d.x     = " << d.x     << endl;
    cout << " d --> pd -> i = " << pd -> i << endl;
    cout << " d --> pd -> x = " << pd -> x << endl;

    cout << " 3 --> Executing the assignment pb = pd. " << endl;

    pb = pd; // from derived to base class. OKAY.

    // d = b   // Would be an error; nor is a cast possible.
    // pd = pb // Would be an error; but using cast is possible.

    cout << " 4 --> Using cast to go from the base class to derived. " << endl;

    pd = static_cast<D*>(pb); // with cast: okay if pb points to a D object

    cout << " After pb = pd; pd = static_cast<D*>(pb); " << endl;
    cout << " we have " << endl;
    cout << " d (reached via pd): " << endl;
    cout << " pd -> i = " << pd -> i << endl;
    cout << " pd -> x = " << pd -> x << endl;

    return 0;
}

//==============//
// End of code. //
//==============//
