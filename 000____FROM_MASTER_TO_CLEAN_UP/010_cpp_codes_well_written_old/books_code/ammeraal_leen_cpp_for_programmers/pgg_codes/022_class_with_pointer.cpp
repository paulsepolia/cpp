//======================//
// Class with pointers. //
//======================//

#include <iostream>
#include <string>

using std::cout;
using std::string;
using std::endl;

// 1.

class row {
public:

    // a.

    row(int n = 3)
    {
        len = n;
        ptr = new int[n];

        for (int i = 0; i < n; i++) {
            ptr[i] = 10 * i;
        }
    }

    // b.

    ~row()
    {
        cout << " I am deleting now. " << endl;
        delete[] ptr;
    }

    // c.

    void printrow(const string &str) const
    {
        cout << endl;

        for (int i = 0; i < len; i++) {
            cout << ptr[i] << ' ';
        }

        cout << endl;
    }

private:
    int *ptr;
    int len;
};

// 2.

void tworows()
{
    row r;
    row s(5);

    r.printrow("r: ");
    s.printrow("s: ");
}

// 3.

int main()
{
    tworows();

    return 0;
}

//==============//
// End of code. //
//==============//
