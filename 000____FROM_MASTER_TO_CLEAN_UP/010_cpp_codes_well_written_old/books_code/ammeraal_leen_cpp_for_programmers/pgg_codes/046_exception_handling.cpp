//===========================//
// Re-throwing an exception. //
//===========================//

#include <iostream>

using std::endl;
using std::cout;

// 1. Lowest level.

void h()
{
    throw 0;
}

// 2. Intermediate level.

void g()
{
    try {
        h();
    } catch(int) {
        cout << "Catch in g" << endl;
        throw 1; // this statement re-throws the exception.
    }
}

// 3. Main function - Highest level.

int main()
{
    try {
        g();
    } catch(int) {
        cout << "Catch in main." << endl;
    }

    return 0;
}

//==============//
// End of main. //
//==============//
