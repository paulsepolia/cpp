//==========================//
// A pure virtual function. //
//==========================//

#include <iostream>

using std::cout;
using std::endl;

// 1. 'animal' class.

class animal {
public:
    virtual void print() const = 0; // a pure virtual function
    virtual ~animal() {}

protected:
    int nlegs;
};

// 2. 'fish' class.

class fish: public animal {
public:
    fish(int n)
    {
        nlegs = n;
    }
    virtual void print() const
    {
        cout << " A fish has " << nlegs << " legs." << endl;
    }
};

// 3. 'bird' class.

class bird: public animal {
public:
    bird(int n)
    {
        nlegs = n;
    }
    virtual void print() const
    {
        cout << " A bird has " << nlegs << " legs." << endl;
    }
};

// 4. 'mammal' class.

class mammal: public animal {
public:
    mammal(int n)
    {
        nlegs = n;
    }
    virtual void print() const
    {
        cout << " A mammal has " << nlegs << " legs." << endl;
    }
};

// 5. The main function.

int main()
{
    int i;

    animal *p[3];

    p[0] = new fish(0);
    p[1] = new bird(2);
    p[2] = new mammal(4);

    for (i = 0; i < 3; i++) {
        p[i] -> print();
    }

    for (i = 0; i < 3; i++) {
        delete p[i];
    }

    return 0;
}

//==============//
// End of code. //
//==============//
