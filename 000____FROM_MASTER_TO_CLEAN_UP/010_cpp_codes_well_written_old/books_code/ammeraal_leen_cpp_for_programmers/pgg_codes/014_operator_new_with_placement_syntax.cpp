//=========================================//
// The operator new with placement syntax. //
//=========================================//

#include <iostream>

using namespace std;

// 1. Function declaration and definition.

void *operator new(size_t nbytes, char *p)
{
    // because of 'static', pFree is initialized only once.
    static char *pFree = p;

    // output to show what happens
    cout << " nbytes = " << nbytes << endl;

    void *p1 = pFree;
    pFree += nbytes;

    return p1;
}

// 2. The main function.

int main()
{
    int *pInt;
    double *pDouble;
    char a[100];

    pInt = new (a) int;
    *pInt = 123;

    pDouble = new (a) double;
    *pDouble = 4.56;

    // Use a hacker's tool to show that array a contains
    // the values 123 and 4.56

    int *pI = reinterpret_cast<int*>(a);
    double *pD = reinterpret_cast<double*>(a + sizeof(int));

    // Uncomment the followings I will override the *pI and *pDouble

    //a[0] = '0';
    //a[1] = '1';
    //a[2] = '2';
    //a[3] = '3';

    cout << "  pI      --> " <<  pI      << endl;
    cout << " *pI      --> " << *pI      << endl;
    cout << "  pD      --> " <<  pD      << endl;
    cout << " *pD      --> " << *pD      << endl;
    cout << "  pInt    --> " <<  pInt    << endl;
    cout << " *pInt    --> " << *pInt    << endl;
    cout << " *pDouble --> " << *pDouble << endl;
    cout << "  pDouble --> " <<  pDouble << endl;

    a[0] = '0';
    a[1] = '1';
    a[2] = '2';
    a[3] = '3';

    cout << " &a[0]    --> " << &a       << endl;
    cout << "  a[0]    --> " <<  a[0]    << endl;
    cout << " &a[1]    --> " << &a+1     << endl;
    cout << "  a[1]    --> " <<  a[1]    << endl;
    cout << " &a[2]    --> " << &a+2     << endl;
    cout << "  a[2]    --> " <<  a[2]    << endl;
    cout << " &a[3]    --> " << &a+3     << endl;
    cout << "  a[3]    --> " <<  a[3]    << endl;

    return 0;
}

//==============//
// End of code. //
//==============//

