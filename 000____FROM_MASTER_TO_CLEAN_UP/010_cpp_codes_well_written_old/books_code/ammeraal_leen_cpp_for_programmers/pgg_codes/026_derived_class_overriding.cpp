//==============================//
// The principle of overriding. //
//==============================//

#include <iostream>

using std::cout;
using std::endl;

// 1. Class B declaration and definition

class B {
public:

    int f() const
    {
        return 1;
    }
};

// 2. Class D declaration and definition

class D: public B {
public:

    int f(int k) const
    {
        return k;
    }
};

// 3. The main function

int main()
{
    D d;

    cout << " d.f(2) = " << d.f(2) << endl;

    cout << " d.B::f() = " << d.B::f() << endl;

    return 0;
}

//==============//
// End of code. //
//==============//
