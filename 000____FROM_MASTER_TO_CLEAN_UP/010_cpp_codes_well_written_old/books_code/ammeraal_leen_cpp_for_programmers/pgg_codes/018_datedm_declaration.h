//===========================//
// DateDM class header file. //
//===========================//

class DateDM {
public:
    DateDM(int d = 1, int m = 1);
    int difference(const DateDM &date) const;

private:
    int day;
    int month;
};

//==============//
// End of code. //
//==============//
