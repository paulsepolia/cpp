//==============================================//
// A (not completely useless) temporary object. //
//==============================================//

#include <iostream>

using std::endl;
using std::cout;

// 1. 'Example' class.

class Example {
public:
    Example()
    {
        cout << " Object created. " << endl;
    }

    void print()
    {
        cout << " Hello! " << endl;
    }
};

// 2. The main function.

int main()
{
    Example().print(); // Output: Hello

    Example();

    return 0;
}

//==============//
// End of code. //
//==============//
