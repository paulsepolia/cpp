//====================//
// Template function. //
//====================//

#include <iostream>

using std::cout;
using std::endl;

// 1. Function definition.

template <class T>
void swapObjects(T &x, T &y)
{
    T w;

    w = x;
    x = y;
    y = w;
}

// 2. Main function.

int main()
{
    int i = 1;
    int j = 2;

    swapObjects(i, j); // Now i = 2 and j = 1

    float u = 3.4F;
    float v = 5.6F;

    swapObjects(u, v); // Now u = 5.6F and v = 3.4F

    cout << " i = " << i << endl;
    cout << " j = " << j << endl;
    cout << " u = " << u << endl;
    cout << " v = " << v << endl;

    return 0;
}

//==============//
// End of code. //
//==============//
