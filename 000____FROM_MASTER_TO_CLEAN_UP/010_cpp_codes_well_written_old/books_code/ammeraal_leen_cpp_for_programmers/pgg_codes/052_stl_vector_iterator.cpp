//======================//
// Vector and Iterator. //
//======================//

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using std::endl;
using std::cout;
using std::cin;
using std::vector;
using std::string;

// 1. The main function.

int main()
{
    vector<string> v;

    cout << " Enter lines of text to be sorted." << endl;
    cout << " Folloowd by the word stop:" << endl;

    for(;;) {
        string s;

        getline(cin, s);

        if (s == "stop") {
            break;
        }

        v.push_back(s);
    }

    sort(v.begin(), v.end());

    cout << " The same lines after sorting:" << endl;

    vector<string>::iterator i;

    for (i = v.begin(); i != v.end(); i++) {
        cout << *i << endl;
    }

    cout << " The same lines after reverse sorting:" << endl;

    vector<string>::reverse_iterator ri;

    for (ri = v.rbegin(); ri != v.rend(); ri++) {
        cout << *ri << endl;
    }

    return 0;
}

//==============//
// End of code. //
//==============//

