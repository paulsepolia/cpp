//===========================//
// vector, list, array       //
// merge, sort, unique       //
//===========================//

#include <iostream>
#include <list>
#include <vector>
#include <iterator>
#include <algorithm>

using std::endl;
using std::cout;
using std::list;
using std::vector;
using std::ostream_iterator;
using std::inserter;
using std::sort;

// 1. Function definition.

void out(const char *s, const list<long> &L)
{
    cout << s;
    copy(L.begin(), L.end(), ostream_iterator<long>(cout, " "));
    cout << endl;
}

// 2. Function definition.

void out(const char *s, const vector<long> &L)
{
    cout << s;
    copy(L.begin(), L.end(), ostream_iterator<long>(cout, " "));
    cout << endl;
}

// 3. The main function.

int main()
{
    // Local variables.

    long i;
    long k;

    // Local constants.

    const long LI_DIM = 9.0 * 1E7;

    // The main code loop.

    for (k = 0; k < 1000; k++) {

        cout << "------------------------------------------------------>>> " << k << endl;

        long *pA1 = new long [LI_DIM];
        list<long>   *pL1 = new list<long>;
        vector<long> *pV1 = new vector<long>;

        cout << "  1 --> Building the array *pA1." << endl;

        for (i = 0; i < LI_DIM; i++) {
            pA1[i] = LI_DIM-i;
        }

        cout << "  2 --> Building the list *pL1." << endl;

        for (i = 0; i < LI_DIM; i++) {
            pL1 -> push_back(LI_DIM - i);
        }

        cout << "  3 --> Merging the array *pA1 and the *pL1 into vector *pV1." << endl;

        merge(pA1, pA1 + LI_DIM, pL1 -> begin(), pL1 -> end(), inserter(*pV1, pV1 -> begin()));

        cout << "  4 --> Sorting the array *pA1." << endl;

        sort(pA1, pA1 + LI_DIM);

        cout << "  5 --> Sorting the list *pL1." << endl;

        (*pL1).sort();

        cout << "  6 --> Sorting the vector *pV1." << endl;

        sort(pV1 -> begin(), pV1 -> end());

        cout << "  7 --> Eliminating the next-to identical elements --> (*pL1)" << endl;

        (*pL1).unique();

        cout << "  8 --> The size of the list *pL1 is " << endl;

        cout << pL1 -> size() << endl;

        cout << "  9 --> The size of the vector *pV1. " << endl;

        cout << pV1 -> size() << endl;

        cout << " 10 --> Deletion of the array *pA1." << endl;

        delete [] pA1;

        cout << " 11 --> Deletion of the list *pL1." << endl;

        delete pL1;

        cout << " 12 --> Deletion of the vector *pV1." << endl;

        delete pV1;
    }

    return 0;
}

//==============//
// End of code. //
//==============//