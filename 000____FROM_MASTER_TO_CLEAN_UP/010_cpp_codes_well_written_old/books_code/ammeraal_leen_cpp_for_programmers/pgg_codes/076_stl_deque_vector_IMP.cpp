//====================================//
// Copy from vector to queue and list //
// and vice-versa.                    //
//====================================//

#include <iostream>
#include <vector>
#include <list>
#include <deque>
#include <algorithm>
#include <iterator>

using std::endl;
using std::cout;
using std::vector;
using std::list;
using std::deque;

int main()
{

// 1. Local constants.

    const long DIM_CON = 1.3 * 1E8;
    const long NUM_DO = 2;
    const long JMAX = 1000;

// 2. Local variables.

    vector<long>::iterator iterVe;
    list<long>::iterator iterLi;
    deque<long>::iterator iterDeq;

    long k;
    long i;
    long j;

// 3. The main code.

    for (j = 0; j < JMAX; j++) {

        cout << "--------------------------------------------------->>> " << j << endl;

        vector<long> *pve = new vector<long>;
        list<long>   *pli = new list<long>;
        deque<long>  *pdq = new deque<long>;

        cout << " 1. Building the vector." << endl;

        for(i = 0; i < DIM_CON; i++) {
            pve -> push_back(DIM_CON-i);
        }

        cout << " 2. Some output of the vector." << endl;

        cout << " (*pve)[0] = " << (*pve)[0] << endl;
        cout << " (*pve)[1] = " << (*pve)[1] << endl;

        cout << " 3. Copy the vector to list." << endl;

        copy( pve -> begin(), pve -> end(), inserter(*pli, pli -> begin()));

        cout << " 4. Some output of the list." << endl;

        k = 0;
        for (iterLi = (pli -> begin()); iterLi != (pli -> end()); iterLi++) {
            k++;
            if (k > NUM_DO) break;
            cout << *iterLi << endl;
        }

        cout << " 5. Copy the list to deque." << endl;

        copy(pli -> begin(), pli -> end(), inserter(*pdq, pdq -> begin()));

        cout << " 6. Some output of the deque." << endl;

        k = 0;
        for (iterDeq = pdq -> begin(); iterDeq != pdq -> end(); iterDeq++) {
            k++;
            if (k > NUM_DO) break;
            cout << *iterDeq << endl;
        }

        cout << " 7. Copy the deque to vector." << endl;

//    This piece of code is very slow.
//    copy(pdq -> begin(), pdq -> end(), inserter(*pve, pve -> begin()));

        copy(pdq -> begin(), pdq -> end(), pve -> begin());

        cout << " 8. Some output of the vector." << endl;

        k = 0;
        for (iterVe = pve -> begin(); iterVe != pve -> end(); iterVe++) {
            k++;
            if (k > NUM_DO) break;
            cout << *iterVe << endl;
        }

        cout << " 9. Deleting the containers." << endl;

        delete pve;
        delete pdq;
        delete pli;
    }

    return 0;
}

//==============//
// End of code. //
//==============//
