//==============================//
// The principle of overriding. //
//==============================//

#include <iostream>

using std::cout;
using std::endl;

// 1. Class B declaration and definition

class B {
public:

    int i;

    int compute() const
    {
        return (i * i);
    }
};

// 2. Class D declaration and definition

class D: public B {
public:

    int i;

    int compute() const
    {
        return (i + B::i);
    }

};

// 3. The main function

int main()
{
    D d;
    d.i = 5;
    d.B::i = 6;

    cout << " d.compute()    = " << d.compute() << endl;
    cout << " d.B::compute() = " << d.B::compute() << endl;

    return 0;
}

//==============//
// End of code. //
//==============//
