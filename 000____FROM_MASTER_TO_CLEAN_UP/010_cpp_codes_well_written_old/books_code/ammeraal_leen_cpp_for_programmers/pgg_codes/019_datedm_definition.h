//=========================//
// DateDM definition file. //
//=========================//

#ifndef DATEDM_DEFINITION_H
#define DATEDM_DEFINITION_H

#include <iostream>
#include "019_datedm_declaration.h"

using std::cout;
using std::endl;

// 1.

namespace {
int calendar[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
}

// 2.

DateDM::DateDM(int d, int m)
{
    if (m < 1 || m > 12) {
        cout << "Invalid month number. Value 1 used instead." << endl;
        m = 1;
    }

    if (d < 1 || d > calendar[m-1]) {
        cout << "Invalid day number. Value 1 used instead." << endl;
        d = 1;
    }

    this -> day = d;
    this -> month = m;

    // or
    // day = d;
    // month = m;

    // The 'this' operator allows me to use as local parameters
    // the more comprehensive names 'day' and 'month' instead of 'd' and 'm'
    // but then in order to distinguish from the private data members 'day' and 'month'
    // I have to use the 'this' operator and to write down the followings

    // this -> day = day;
    // this -> month = month;
}

// 3.

int DateDM::difference(const DateDM &date2) const
{
    int precedingDays[12];
    precedingDays[0] = 0;

    const int TOT_MON = 12;

    for (int i = 1; i < TOT_MON; ++i) {
        precedingDays[i] = precedingDays[i-1] + calendar[i-1];
    }

    int dayNr1 = precedingDays[month-1] + day;
    int dayNr2 = precedingDays[date2.month-1] + date2.day;

    return (dayNr2 - dayNr1);
}

#endif // end of guard DATEDM_DEFINITION_H

//==============//
// End of code. //
//==============//
