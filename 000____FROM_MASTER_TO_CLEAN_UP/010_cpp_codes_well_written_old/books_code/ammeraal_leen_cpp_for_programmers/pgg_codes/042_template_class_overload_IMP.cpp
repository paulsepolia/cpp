//=================//
// Template class. //
//=================//

#include <iostream>
#include <iomanip>

using std::cout;
using std::endl;
using std::ostream;
using std::setprecision;
using std::fixed;

// 1. 'vec' class.

template <class T>
class vec {
public:
    vec(T xx = 0, T yy = 0)
        : x(xx), y(yy)
    {}

    void printvec(ostream &os) const
    {
        os << "(" << x << ", " << y << ")";
    }

    vec<T> operator+(const vec<T> &b) const
    {
        return vec<T>(x + b.x, y + b.y);
    }

private:
    T x;
    T y;
};

// 2. '<<' operator overloading.

template <class T>
ostream &operator<<(ostream &os, const vec<T> &v)
{
    v.printvec(os);

    return os;
}

// 3. Main function

int main()
{
    vec<int> uInt(1, 2);
    vec<int> vInt(3, 4);
    vec<int> sInt;

    vec<double> uDouble(1.1, 2.2);
    vec<double> vDouble(3.3, 4.4);
    vec<double> sDouble;

    sInt = uInt + vInt;

    sDouble = uDouble + vDouble;

    cout << fixed << setprecision(10) << endl;

    cout << " sInt = " << sInt << endl;

    cout << " sDouble = " << sDouble << endl;

    return 0;
}

//==============//
// End of code. //
//==============//
