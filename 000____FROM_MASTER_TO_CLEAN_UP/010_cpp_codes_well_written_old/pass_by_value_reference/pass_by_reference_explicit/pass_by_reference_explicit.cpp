#include <iostream>
using namespace std;

void swap( double *, double *); // function prototype

int main()
{
    double firstnum = 20.5;
    double secnum = 6.25;

    cout << " the value stored in firstnum is: " << firstnum << endl;
    cout << " the value stored in secnum is: " << secnum << endl;

    swap( &firstnum, &secnum ); // call swap

    cout << " the value stored in firstnum is now: " << firstnum << endl;
    cout << " the value stored in secnum is now: " << secnum << endl;

    return 0;
}

// this function illustrates paasing pointer arguments

void swap( double *nm1Addr, double *nm2Addr )
{
    double temp;

    temp = *nm1Addr;
    *nm1Addr = *nm2Addr;
    *nm2Addr = temp;

    return;
}
