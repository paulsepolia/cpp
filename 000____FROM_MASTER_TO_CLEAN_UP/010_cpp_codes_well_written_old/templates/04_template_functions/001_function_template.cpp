#include <iostream>
using namespace std;

template <class T>    // template prefix
T abs( T value )      // function header line
{
    T absnum;           // variable declaration

    if ( value < 0 ) {
        absnum = -value;
    } else {
        absnum = value;
    }

    return absnum;
}

int main()
{
    int    num1 = -4;
    float  num2 = -4.2f;
    double num3 = -4.2345678910123456789;

    cout << abs( num1 ) << endl;
    cout << abs( num2 ) << endl;
    cout << abs( num3 ) << endl;

    return 0;
}

