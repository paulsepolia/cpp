//=================================//
// Header file.                    //
// Template.                       //
// swapValues function definition. //
//=================================//

template <typename T>
void swapValues(T& variable1, T& variable2)
{
    T temp;

    temp = variable1;
    variable1 = variable2;
    variable2 = temp;
}

//==============//
// End of code. //
//==============//

