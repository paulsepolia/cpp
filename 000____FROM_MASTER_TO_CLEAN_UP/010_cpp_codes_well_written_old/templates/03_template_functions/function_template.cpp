#include <iostream>
using namespace std;

template <class T1>

void showabs( T1 number )
{
    if ( number < 0 ) {
        number = - number;
    }

    cout << " The absolute value of the number is "
         << number << endl;

    return;
}

int main()
{
    int    num1 = -4;
    float  num2 = -4.2f;
    double num3 = -4.2345678910123456789;

    showabs( num1 );
    showabs( num2 );
    showabs( num3 );

    return 0;
}

