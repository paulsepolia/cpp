//====================//
// Header file.       //
// HourlyEmployeeCL.h //
//====================//

// This is the interface for the class HourlyEmployeeCL

#ifndef HOURLY_EMPLOYEE_CL_H
#define HOURLY_EMPLOYEE_CL_H

#include <string>
using std::string;

#include "EmployeeCL.h"

namespace PGG {

class HourlyEmployeeCL : public EmployeeCL {
public:
    HourlyEmployeeCL();
    HourlyEmployeeCL(string theName, string theSsn, double theWageRate, double theHours);
    void setRate(double newWageRate);
    double getRate() const;
    void setHours(double hoursWorked);
    double getHours() const;
    void printCheck();

private:
    double wageRate;
    double hours;
};

} // end of namespace PGG

#endif // end of guard HOURLY_EMPLOYEE_CL_H

//==============//
// End of code. //
//==============//
