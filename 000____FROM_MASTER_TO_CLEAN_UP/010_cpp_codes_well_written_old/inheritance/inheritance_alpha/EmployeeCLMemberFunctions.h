//=============================//
// Header file.                //
// EmployeeCLMemberFunctions.h //
//=============================//

// This is the implementation for the class EmployeeCL.
// The interface for the class EmployeeCL is in the header file EmployeeCL.h

#ifndef EMPLOYEE_CL_MF_H
#define EMPLOYEE_CL_MF_H

#include <string>
#include <cstdlib>
#include <iostream>
using std::string;
using std::cout;

#include "EmployeeCL.h"

namespace PGG {

// 1.

EmployeeCL::EmployeeCL()
    : name("No name yet"),
      ssn("No number yet"),
      netPay(0)
{
    // deliberately empty
}

// 2.

EmployeeCL::EmployeeCL(string theName, string theNumber)
    : name(theName),
      ssn(theNumber),
      netPay(0)
{
    // deliberately empty
}

// 3.

string EmployeeCL::getName() const
{
    return name;
}

// 4.

string EmployeeCL::getSsn() const
{
    return ssn;
}

// 5.

double EmployeeCL::getNetPay() const
{
    return netPay;
}

// 6.

void EmployeeCL::setName(string newName)
{
    name = newName;
}

// 7.

void EmployeeCL::setSsn(string newSsn)
{
    ssn = newSsn;
}

// 8.

void EmployeeCL::setNetPay(double newNetPay)
{
    netPay = newNetPay;
}

// 9.

void EmployeeCL::printCheck() const
{
    cout << "\nERROR: printCheck FUNCTION CALLED FOR AN \n"
         << "UNDIFFERENTIATED EMPLOYEE. aborting the program.\n"
         << "Check with the author of the program about this bug.\n";

    exit(1);
}

} // End of namespace PGG

#endif // end of guard EMPLOYEE_CL_MF_H

//==============//
// End of code. //
//==============//
