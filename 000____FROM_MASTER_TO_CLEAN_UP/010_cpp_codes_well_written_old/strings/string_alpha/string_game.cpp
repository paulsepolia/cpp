#include <iostream>
#include <string>
using namespace std;

int main()
{
    string str1; // an empty string
    const long int dimen = 10E+3;
    string str2(1,'*');
    string str3; // an empty string
    string str4; // an empty string
    long int i;
    int sentinel;

    cout << " 1 " << endl;

    for ( i=0; i<dimen; i++) {
        str3 = str3 + str2;
    }

    cout << " 2 " << endl;

    for ( i=0; i<dimen; i++) {
        str4 = str4 + str2;
    }

    cout << " 3 " << endl;

    cout << " str1               is: " << str1               << endl;
    cout << " str2               is: " << str2               << endl;
    cout << " str3.length()      is: " << str3.length()      << endl;
    cout << " str3.size()        is: " << str3.size()        << endl;
    cout << " 4 " << endl;
    cout << " str3.compare(str4) is: " << str3.compare(str4) << endl;
    cout << " 5 " << endl;
    cout << " str3 == str4       is: " << (str3 == str4)     << endl;
    cout << " 6 " << endl;
    cout << " str3 != str4       is: " << (str3 != str4)     << endl;
    cout << " 7 " << endl;
    cout << " str3 <  str4       is: " << (str3 <  str4)     << endl;
    cout << " str3 <= str4       is: " << (str3 <= str4)     << endl;

    cout << " enter an integer to exit: ";
    cin >> sentinel;

    return 0;
}
