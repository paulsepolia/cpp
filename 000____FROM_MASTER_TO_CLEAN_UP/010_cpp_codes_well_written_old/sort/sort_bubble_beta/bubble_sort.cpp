// bubble sort

#include <iostream>

using namespace std;

void bubbleSort( int list[], int length );

int main()
{

    const int dimen = 400000;

    int *list  = new int [dimen] ;

    int i;

    for ( int i=0; i<dimen; i++ ) {
        list[i] = dimen-i;
    }

    bubbleSort( list, dimen );

    cout << "After sorting, the list elemenst are:" << endl;

    for ( i = 0; i < dimen; i++ ) {
        cout << list[i] << endl ;
    }

    cout << endl;
}

// bubbleSort function

void bubbleSort( int list[], int length )
{
    int temp;
    int iteration;
    int index;

    for ( iteration = 1; iteration < length; iteration++ ) {
        for ( index = 0; index < length - iteration; index++ ) {
            if ( list[index] > list[index+1] ) {
                temp = list[index];
                list[index] = list[index+1];
                list[index+1] = temp;
            }
        }
    }
}

