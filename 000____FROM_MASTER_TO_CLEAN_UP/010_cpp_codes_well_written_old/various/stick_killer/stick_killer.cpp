#include <iostream>
#include <fstream>
#include <cstdlib>  // needed for exit
#include <string>
#include <iomanip>  // needed for formatting

using namespace std;

int main()
{

    string filename = "file_1.txt";

    long int j;
    const long int dimenB=10E+5;

    for ( j=0; j<dimenB; j++) {  // start of big-for

        ofstream out_file ;

        out_file.open( filename.c_str() );

        // 1. check if the file exists.

        if( out_file.fail() ) {
            cout << " the file was not successfully opened" << endl;
            exit(1) ;
        }


        // 2. set output file stream formats.

        out_file << setiosflags(ios::fixed) ;
        out_file << setiosflags(ios::showpoint) ;
        out_file << setprecision(5) ;

        // 3. send data to the file.

        long int i;
        const long int dimen = 3*10E+7;  // 3*10E+7 ==> 6 GBytes to be written
        // 1*10E+7 ==> 2 GBytes to be written, etc...

        for ( i=0; i<dimen; i++) {
            out_file << " line " << i+1 << "  " << static_cast<double>(i+1) << endl;
        }

        // 4. closing the file.

        out_file.close();

        cout << j << endl;

    }  // end of big-for

    cout << " the file " << filename << " has been successfully written. " << endl;

    return 0;

}



