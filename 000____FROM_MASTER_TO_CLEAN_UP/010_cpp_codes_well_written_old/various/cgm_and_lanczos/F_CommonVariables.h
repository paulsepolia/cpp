
//=========================================================
//
//  The Lancos Algorithm.
//
//  Author :: Pavlos G. Galiatsatos.
//
//  Date :: 2008, November.
//
//=========================================================

// 1. Declaretion of the number of OpenMP threads.

int NT ;  // Global OpenMP variables.

// 2. Declaration of the OpenMP flags.

bool ompA ; // Global OpenMP variable.
bool ompB ; // Global OpenMP variable.

// 3. Declaratiion of other variables.

bool conjuFlag ;  // Global variable.
bool conjuTibor ; // Global variable.
int matrixFlag ;  // Global variable.


/* FINI. */
