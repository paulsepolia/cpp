
//=========================================================
//
//  The Lancos Algorithm.
//
//  Author :: Pavlos G. Galiatsatos.
//
//  Date :: 2008, November.
//
//=========================================================


# include "F_LanczosAlgorithm.h"

using namespace std ;

void Input_Screen_F( void )
{

//=========================================================
// 4. All the inputs from the keyboard.

    cout << " =========================================================== " << endl ;
    cout << "  " << endl ;
    cout << " The Lanczos' Iterative Diagonalizer. " << endl ;
    cout << "  " << endl ;
    cout << " C++ && OpenMP, non-sparse syntax use version. " << endl ;
    cout << "  " << endl ;
    cout << " =========================================================== " << endl ;
    cout << "  "  << endl ;


    cout << "  1. Give the number of elements of each Lanczos' vector    ----->> " ;
    int n_LVE ;
    cin >> n_LVE ; // The number of elements of each Lanczos' vector.

    cout << "  2. Give how many eigenvalues do you want to take back     ----->> " ;
    int n_EigVa ; // The number of eigenvalues to take back.
    cin >> n_EigVa ;

    cout << "  3. Give the module number                                 ----->> " ;
    int modNu ; // The starting dimension of tthe tridiagonal matrix.
    cin >> modNu ; // and current step in each iteration.

    cout << "  4. Give the 'error' convergence constant                  ----->> " ;
    double the_error ;  // The maximum difference for convergence.
    cin >> the_error ;

    cout << "  5. To use the OpenMP directives 'ompA'       (yes/1,no/0) ----->> " ;
    cin >> ompA ; // 'ompA' Global OpenMP variable.

    cout << "  6. To use the OpenMP directives 'ompB'       (yes/1,no/0) ----->> " ;
    cin >> ompB ; // 'ompB' Global OpenMP variable.

    cout << "  7. Give the number of OpenMP threads                      ----->> " ;
// 'NT' Global OpenMP variable.
    cin >> NT ; // Number of threads for parallel execution.

    cout << "  8. Choose the matrix                            (1/2/3/4) ----->> " ;
// 'matrixFlag' Global variable.
    cin >> matrixFlag ;

    cout << "  9. To use the Conjugate Gradient Method      (yes/1,no/0) ----->> " ;
// 'conjuFlag' Global  variable.
    cin >> conjuFlag ;

    cout << " 10. To use the 'Quick' or the 'Slow' CGM         (Q/1,S/0) ----->> " ;
// 'conjuTibor' Global  variable.
    cin >> conjuTibor ;

    cout << "  " << endl ;
    cout << " Please Wait ................. " << endl ;
    cout << "  " << endl ;


//=========================================================
// 5. Declaration of the problem's matrix.

    double** MatrixMP = new double* [n_LVE] ;
    for ( int i = 0 ; i <  n_LVE ; i++ ) {
        MatrixMP[i] = new double [n_LVE] ;
    }

    Matrix_F ( MatrixMP , n_LVE ) ;

//=========================================================
// 6. Declaration of the first 2 Lanczos' vectors.

    double** LaVe = new double* [ n_LVE + 1 ] ;

    for ( int i = 0 ; i < 2 ; i++ ) {    // Only 2 Lanczos vectors !
        LaVe[i] = new double [ n_LVE ] ;    // The rest are builded dynamically.
    }

    Make_LV_F( LaVe, n_LVE  ) ;

//=========================================================
// 7. Declaration the ram space for eigenvalues.

    double* alphaMP = new double [ n_LVE ] ;

//=========================================================
// 8. Initializing and starting the Lanczos' Algorithm.

    time_t t1 ;
    t1 = clock() ;


    Lanczos_Algorithm  ( MatrixMP ,        // The system's matrix
                         LaVe ,            // The Lanczos' vectors
                         alphaMP ,         // The eigenvalues
                         n_LVE ,           // The dimension of the matrix
                         n_EigVa ,         // Number of eigenvalues to take back
                         modNu ,           // the step
                         the_error )   ;   // the error



//=========================================================
// 9. Timing.


    time_t t2 ;
    t2 = clock() ;

    cout << " ================================================= " << endl ;
    cout << " The Cpu time used is -->> " << (t2-t1)/CLOCKS_PER_SEC << endl ;
    cout << " ================================================= " << endl ;


}
