#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

// function prototypes

void bisection( long double, long double, long double, long int); // function prototype
long double f(long double); // function prototype

// main function

int main()
{
    long int imax; // maximum number of iterations
    long double a, b; // left and right ends of the original interval
    long double epsilon;  // convergence criterion

    // obtain input data
    cout << " Enter the limits of the original search interval, a and b: ";
    cin >> a >> b;
    cout << " Enter the convergence criteria: ";
    cin >> epsilon;
    cout << " Enter the maximum number of iterations allowed: ";
    cin >> imax;

    bisection( a, b, epsilon, imax );

    return 0;
}

// A bisection function that finds roots of a function
// The interval a < x < b is know to contain a root of f(x).
// The esimate of the root is improved by finding in which half of the
// interval the root lies and then replacing the original interval by
// that half-interval.

void bisection( long double a, long double b, long double epsilon, long int imax )
{
    long int i;             // current iteration counter
    long double x1, x2, x3; // left, right, and midpoint of current interval
    long double f1, f2, f3; // function evalueated at thes points
    long double width;      // width of original interval = (b-a)
    long double curwidth;   // width of current intervla = (x3-x1)
    long double xtmp, x4;
    const long double fines = 10.0E+5;
    const long double ep2 = 10E-5;
    long int indexA = 0;

    // echo back the passed input data
    cout << endl;
    cout << " The original search interval is from "
         << a << " to " << b << endl;
    cout << " The convergence criterion is interval < " << epsilon << endl;
    cout << " The maximum number of iterations allowed is " << imax << endl;
    cout << endl;
    cout << endl;

    // calculate the root

    for ( x4=a; x4<=b; x4=x4+b/fines ) {
        x1=x4;
        x3=x4+b/fines;

        f1=f(x1);
        f3=f(x3);

        for (i=1; i<=imax; i++) {
            // find which half of the interval contains the root
            x2 = (x1+x3) / 2.0;
            f2 = f(x2);
            if ( f1*f2 <= 0.0 ) { // root is in the left half-interval
                curwidth = ( x2-x1 ) / 2.0;
                f3 = f2;
                x3 = x2;
            } else { // root is in the right half-interval
                curwidth = ( x3-x2 ) / 2.0;
                f1 = f2;
                x1 = x2;
            }
            if ( (curwidth < epsilon) && abs(f2) < ep2 ) {
                cout << setprecision(20);

                if ( (abs(x2)-abs(xtmp)) > 1.0/fines ) {
                    indexA = indexA + 1;

                    cout << setiosflags(ios::right);
                    cout << setiosflags(ios::showpos);
                    cout << fixed;
                    cout << " ";
                    cout << setw(5);
                    cout << indexA;
                    cout << " ";
                    cout << setw(5);
                    cout << i;
                    cout << " ";
                    cout << setw(25);
                    cout << x2;
                    cout << " ";
                    cout << setw(25);
                    cout << f2;
                    cout << endl;
                    xtmp = x2;
                }
            }
        }
    }

    cout << endl;
    cout << " After " << imax << " iterations, no root was found "
         << " within the convergence criterion " << endl;

// return;
}

// function to evaluate f(x)

long double f( long double x )
{
    const long double PI = 2*asin(1.0); // value of pi

    return ( exp(-x) - sin(0.5 * PI * x) );
}
