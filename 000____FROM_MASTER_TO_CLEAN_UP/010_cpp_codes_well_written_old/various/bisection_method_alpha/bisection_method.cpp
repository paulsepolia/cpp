#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

// function prototypes

void bisection( long double, long double, long double, long int); // function prototype
long double f(long double); // function prototype

// main function

int main()
{
    long int imax; // maximum number of iterations
    long double a, b; // left and right ends of the original interval
    long double epsilon;  // convergence criterion

    // obtain input data
    cout << " Enter the limits of the original search interval, a and b: ";
    cin >> a >> b;
    cout << " Enter the convergence criteria: ";
    cin >> epsilon;
    cout << " Enter the maximum number of iterations allowed: ";
    cin >> imax;

    bisection( a, b, epsilon, imax );

    return 0;
}

// A bisection function that finds roots of a function
// The interval a < x < b is know to contain a root of f(x).
// The esimate of the root is improved by finding in which half of the
// interval the root lies and then replacing the original interval by
// that half-interval.

void bisection( long double a, long double b, long double epsilon, long int imax )
{
    long int i;             // current iteration counter
    long double x1, x2, x3; // left, right, and midpoint of current interval
    long double f1, f2, f3; // function evalueated at thes points
    long double width;      // width of original interval = (b-a)
    long double curwidth;   // width of current intervla = (x3-x1)

    // echo back the passed input data
    cout << endl;
    cout << " The original search intervla is from "
         << a << " to " << b << endl;
    cout << " The convergence criterion is: interval < " << epsilon << endl;
    cout << " The maximum number of iterations allowd is " << imax << endl;

    // calculate the root
    x1=a;
    x3=b;
    f1=f(x1);
    f3=f(x3);
    width = (b-a);

    // verify there is a root in the interval
    if( f1*f3 > 0.0 ) {
        cout << endl;
        cout << " No root in the interval exists " << endl ;
    } else {
        for (i=1; i<=imax; i++) {
            // find which half of the interval contains the root
            x2 = (x1+x3) / 2.0;
            f2 = f(x2);
            if ( f1*f2 <= 0.0 ) { // root is in the left half-interval
                curwidth = ( x2-x1 ) / 2.0;
                f3 = f2;
                x3 = x2;
            } else { // root is in the right half-interval
                curwidth = ( x3-x2 ) / 2.0;
                f1 = f2;
                x1 = x2;
            }
            if ( curwidth < epsilon ) {
                cout << endl;
                cout << setprecision(20);
                cout << " A root at x = " << x2 << " was found "
                     << " in " << i << " iterations " << endl;
                cout << " The value of the function is " << f2 << endl;

                return;
            }
        }
    }

    cout << endl;
    cout << " After " << imax << " iterations, no root was found "
         << " within the convergence criterion " << endl;

    return;
}

// function to evaluate f(x)

long double f( long double x )
{
    const long double PI = 2*asin(1.0); // value of pi

    return ( exp(-x) - sin(0.5 * PI * x) );
}
