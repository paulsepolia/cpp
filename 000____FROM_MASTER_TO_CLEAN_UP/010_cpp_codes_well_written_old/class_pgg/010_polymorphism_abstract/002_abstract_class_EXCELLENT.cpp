//Abstract Creature
//Demonstrates abstact classes

#include <iostream>

using namespace std;

//======================================//
// Creature abstract class declaration. //
//======================================//

class Creature {                        // abstract class
public:
    Creature(int health = 100);
    virtual void Greet() const = 0;       // pure virtual member function
    virtual void DisplayHealth() const;

protected:
    int m_Health;
};

//=============================================//
// Creature class member functions definition. //
//=============================================//

// 1. Constructor

Creature::Creature(int health):
    m_Health(health)
{}

// 2. DisplayHealth

void Creature::DisplayHealth() const
{
    cout << "Health: " << m_Health << endl;
}

//========================//
// Orc class declaration. //
//========================//

class Orc : public Creature {
public:
    Orc(int health = 120);
    virtual void Greet() const;
};

//========================================//
// Orc class member functions definition. //
//========================================//

// 1. Constructor

Orc::Orc(int health):
    Creature(health)
{}

// 2.

void Orc::Greet() const
{
    cout << "The orc grunts hello." << endl;
}

//====================//
// The main function. //
//====================//

int main()
{
    // 1.

    Creature* pCreature = new Orc();

    pCreature -> Greet();
    pCreature -> DisplayHealth();

    (*pCreature).Greet();          // same as the above code
    (*pCreature).DisplayHealth();  // same as the above code

    // 2.

    // The following is not allowed, since Creature is an abstact class.
    // It is an abstract class since accomodates a pure virtual function.

    //Creature oC1;
    //Creature oC2;

    // 3.

    Orc oOrc1(111);
    Orc oOrc2(222);

    oOrc1.Greet();
    oOrc1.DisplayHealth();

    oOrc2.Greet();
    oOrc2.DisplayHealth();

    // 4.

    Creature* pCreature2 = new Orc(333);

    pCreature2 -> Greet();
    pCreature2 -> DisplayHealth();

    (*pCreature2).Greet();          // same as the above code
    (*pCreature2).DisplayHealth();  // same as the above code

    return 0;
}

//==============//
// End of code. //
//==============//


