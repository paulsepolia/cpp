//========================================//
// Driver program to the ElementCL class. //
//========================================//

#include <iostream>
#include <iomanip>
#include <cmath>
#include "ElementCL.h"
#include "ElementCLMemberFunctions.h"
#include "TypeDefinitions.h"

using namespace std;

int main()
{
    //  1. local variables

    TA v1;
    TA v2;
    TA v3;
    bool bo1;
    bool bo2;

    //  2. local objects

    ElementCL<TA> oA;
    ElementCL<TA> oB;
    ElementCL<TA> oC;
    ElementCL<TA> oD;

    //  3. set the output format

    cout << setprecision(20) << fixed << endl;

    //  4. test --> 1 --> AddF

    v1 = 11.11111111111111111111;
    oA.SetElementF(v1);

    v2 = 22.22222222222222222222;
    oB.SetElementF(v2);

    oD.AddF(oA, oB);
    v3 = oD.GetElementF();

    cout << " -->  1. Must be " << v1 + v2 << endl;
    cout << " -->  2. Is      " << v3 << endl;

    //  5. test --> 2 --> SubtractF

    v1 = 11.11111111111111111111;
    oA.SetElementF(v1);

    v2 = 22.22222222222222222222;
    oB.SetElementF(v2);

    oD.SubtractF(oA, oB);
    v3 = oD.GetElementF();

    cout << " -->  3. Must be " << v1 - v2 << endl;
    cout << " -->  4. Is      " << v3 << endl;

    //  6. test --> 3 --> TimesF

    v1 = 11.11111111111111111111;
    oA.SetElementF(v1);

    v2 = 22.22222222222222222222;
    oB.SetElementF(v2);

    oD.TimesF(oA, oB);
    v3 = oD.GetElementF();

    cout << " -->  5. Must be " << v1 * v2 << endl;
    cout << " -->  6. Is      " << v3 << endl;

    //  7. test --> 4 --> DivideF

    v1 = 11.11111111111111111111;
    oA.SetElementF(v1);

    v2 = 22.22222222222222222222;
    oB.SetElementF(v2);

    oD.DivideF(oA, oB);
    v3 = oD.GetElementF();

    cout << " -->  7. Must be " << v1 / v2 << endl;
    cout << " -->  8. Is      " << v3 << endl;

    //  8. test --> 5 --> AbsF

    v1 = -1.0;
    oB.SetElementF(v1);

    oC.AbsF(oA);
    v2 = oC.GetElementF();

    cout << " -->  9. Must be " << abs(v1) << endl;
    cout << " --> 10. Is      " << v2 << endl;

    //  9. test --> 6 --> SetEqual

    v1 = 3.0;
    oA.SetElementF(v1);

    oC.SetEqualF(oA);
    v2 = oC.GetElementF();

    cout << " --> 11. Must be " << v1 << endl;
    cout << " --> 12. Is      " << v2 << endl;

    // 10. test --> 7 --> Overloading operators --> arithmetic type

    v1 = 2.0;
    v2 = 3.0;
    v3 = 7.0;

    cout << " --> 13. Before --> v1 is " << v1 << endl;
    cout << " --> 14. Before --> v2 is " << v2 << endl;
    cout << " --> 15. Before --> v3 is " << v3 << endl;

    oA.SetElementF(v1);
    oB.SetElementF(v2);
    oC.SetElementF(v3);

    //oC = oA + oB;
    //oC = oA - oB;
    oC = oA * oB;
    //oC = oA / oB;
    //oB += oA;
    //oB -= oA;
    //oB *= oA;
    //oB /= oA;

    v1 = oA.GetElementF();
    v2 = oB.GetElementF();
    v3 = oC.GetElementF();

    cout << " --> 16. After  --> v1 is " << v1 << endl;
    cout << " --> 17. After  --> v2 is " << v2 << endl;
    cout << " --> 18. After  --> v3 is " << v3 << endl;

    // 11a. test 8 --> Overloading operators --> bool type
    // 11b. test 8 --> Only Member functions

    v1 = 2.0;
    v2 = 2.0;

    oA.SetElementF(v1);
    oB.SetElementF(v2);

    //bo1 = (oA >= oB);
    //bo2 = (v1 >= v2);

    //bo1 = (oA <= oB);
    //bo2 = (v1 <= v2);

    //bo1 = (oA > oB);
    //bo2 = (v1 > v2);

    //bo1 = (oA < oB);
    //bo2 = (v1 < v2);

    //bo1 = (oA != oB);
    //bo2 = (v1 != v2);

    bo1 = (oA == oB);
    bo2 = (v1 == v2);

    cout << " --> 19. Must be  " << bo2 << endl;
    cout << " --> 20.      Is  " << bo1 << endl;

    // XX. Exiting.

    cout << endl;
    return 0;
}

//==============//
// End of code. //
//==============//
