//=============================//
// Header file.                //
// IntPairCLMemberFunctions.h  //
//=============================//

// 1.

IntPairCL::IntPairCL(int firstValue, int secondValue)
    : first(firstValue), second(secondValue)
{
// Body intentionally empty.
}

// 2.

IntPairCL IntPairCL::operator++(int ignoreMe) // Postfix version
{
    int temp1 = first;
    int temp2 = second;
    first++;
    second++;

    return IntPairCL(temp1, temp2);
}

// 3.

IntPairCL IntPairCL::operator++() // Prefix version
{
    first++;
    second++;
    return IntPairCL(first, second);
}

// 4.

void IntPairCL::setFirst(int newValue)
{
    first = newValue;
}

// 5.

void IntPairCL::setSecond(int newValue)
{
    second = newValue;
}

// 6.

int IntPairCL::getFirst() const
{
    return first;
}

// 7.

int IntPairCL::getSecond() const
{
    return second;
}

//==============//
// End of code. //
//==============//
