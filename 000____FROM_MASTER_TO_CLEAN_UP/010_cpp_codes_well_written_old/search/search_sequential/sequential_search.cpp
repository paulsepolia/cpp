// sequential search

#include <iostream>

using namespace std;

const int ARRAY_SIZE = 10;

int seqSearch( const int list[],
               int listLength,
               int searchItem );

int main()
{
    int intList[ARRAY_SIZE];
    int number;
    int index;

    cout << " Enter " << ARRAY_SIZE << " integers." << endl;

    for ( index = 0; index < ARRAY_SIZE; index++ ) {
        cin >> intList[index];
    }

    cout << endl;

    cout << " Enter the number to be searched: ";
    cin >> number;

    cout << endl;

    index = seqSearch( intList, ARRAY_SIZE, number );

    if ( index != -1 ) {
        cout << " AA: " << number << " is found at position "
             << index << endl;
    } else {
        cout << " BB: " << number << "is not in the list." << endl;
    }

    return 0;
}

// exteranl function

int seqSearch( const int list[],
               int listLength,
               int searchItem )
{
    int loc;

    for ( loc=0; loc < listLength; loc++ ) {
        if ( list[loc] == searchItem ) {
            return loc;
        }
    }

    return -1;
}


