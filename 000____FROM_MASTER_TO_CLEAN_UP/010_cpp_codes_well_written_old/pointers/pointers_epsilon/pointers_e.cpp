#include <iostream>

using namespace std;

int main()
{
    int *p;
    long int i;
    const long int dimen = 1000000000;

    for ( i = 0; i < dimen ; i++ ) {
        p = new int;
        *p = 54;

        delete p;
        p = NULL;

        p = new int;
        *p = 73;

        delete p;
        p = NULL;
    }

    cout << "  p = " <<  p << endl;
    cout << " &p = " << &p << endl;

    return 0;
}

