
#include <iostream>

using namespace std;

int main()
{
    int x;
    int *p;

    p = &x;

    x = 10;

    cout << endl;
    cout << "   p = " <<  p << endl;
    cout << "  *p = " << *p << endl;
    cout << "  &p = " << &p << endl;
    cout << endl;
    cout << "   x = " <<  x << endl;
    cout << "  &x = " << &x << endl;
    cout << endl;

    return 0;
}

