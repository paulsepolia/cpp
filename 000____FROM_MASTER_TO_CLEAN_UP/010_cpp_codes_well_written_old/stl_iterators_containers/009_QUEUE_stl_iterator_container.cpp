//==================================================================//
// Program to demonstrate use of queue template class from the STL. //
//==================================================================//

#include <iostream>
#include <queue>

using std::cin;
using std::cout;
using std::endl;
using std::queue;

int main()
{
    const long QUE_SIZE = 2*1E8;
    const long NUM_DO = 1E2;

    queue<double> que1;
    queue<double> que2;

    long i;
    long k;
    long tmp1 = 0;

    for (k = 0; k < NUM_DO; k++) {

        cout << " 1 --> Building the queue. " << endl;

        for (i = 0; i < QUE_SIZE; i++) {
            que1.push(static_cast<double>(i));
        }

        cout << " 2 --> Equating queue 1 and queue 2. " << endl;

        que2 = que1;

        cout << " 3 --> The front and back elements of queues. " << endl;

        cout << que1.front() << endl;
        cout << que2.front() << endl;

        cout << que1.back() << endl;
        cout << que2.back() << endl;

        cout << " 4 --> Size of queues. " << endl;

        cout << que1.size() << endl;
        cout << que2.size() << endl;

        cout << " 5 --> Remove an element from the top of the queue 1. " << endl;

        que1.pop();

        cout << " 6 --> Remove an element from the top of the queue 1. " << endl;

        que1.pop();

        cout << " 7 --> Size of the queues. " << endl;

        cout << que1.size() << endl;
        cout << que2.size() << endl;

        cout << " 8 --> The front and back elements of queues. " << endl;

        cout << que1.front() << endl;
        cout << que2.front() << endl;

        cout << que1.back() << endl;
        cout << que2.back() << endl;

        cout << " 9 --> Removing all the elements from queue 1. " << endl;

        tmp1 = que1.size();
        for (i = 0; i < tmp1; i++) {
            que1.pop();
        }

        cout << " 10 --> Size of queues. " << endl;

        cout << que1.size() << endl;
        cout << que2.size() << endl;

        cout << " 11 --> Equating the empty que 1 to que 2." << endl;

        que2 = que1;

        cout << " 12 --> Size of the queues. " << endl;

        cout << que1.size() << endl;
        cout << que2.size() << endl;
    }

    return 0;
}

//==============//
// End of code. //
//==============//
