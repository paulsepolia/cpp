//===================//
// A priority queue. //
//===================//

#include <iostream>
#include <vector>
#include <queue>

using std::endl;
using std::cout;
using std::priority_queue;
using std::vector;

// 1. Class definition.

class CompareLastDigits {
public:
    bool operator()(long x, long y)
    {
        return (x % 10 > y % 10);
    }
};

// 2. The main function.

int main()
{
    priority_queue<long, vector<long>, CompareLastDigits > PQ;

    long x;
    long i;
    long k;

    const long DIMEN_PQ = 1E8;

    for (i = 0; i < DIMEN_PQ; i++) {
        PQ.push(DIMEN_PQ-i);
    }

    k = 0;

    while(!PQ.empty()) {
        k++;
        x = PQ.top();
        if ( k < 10 ) {
            cout << "Retrieved element: " << x << endl;
        }
        PQ.pop();
    }

    cout << " Size --> Must be empty ==> 0" << endl;
    cout << PQ.size() << endl;

    cout << " Filling up the priority queue again." << endl;

    for (i = 0; i < DIMEN_PQ; i++) {
        PQ.push(i);
    }

    cout << " size --> Must be DIMEN_PQ = " << DIMEN_PQ << endl;
    cout << PQ.size() << endl;

    k = 0;

    while(!PQ.empty()) {
        k++;
        x = PQ.top();

        if ( k < 10 ) {
            cout << "Retrieved element: " << x << endl;
        }
        PQ.pop();
    }

    return 0;
}

//==============//
// End of code. //
//==============//
