//===================//
// Set container.    //
// insert, less, ==  //
//===================//

#include <iostream>
#include <set>

using std::cout;
using std::endl;
using std::set;
using std::less;

int main()
{
    // 1. Local constants.

    const long DIM_SE_A = 1.0 * 1E7;
    const long DIM_SE_B = 1.0 * 1E7;
    const long NUM_DO_A = 1000;
    const long NUM_DO_B = 10;

    // 2. Local variables.

    long k;
    long i;

    // 3. the main code.

    for (k = 1; k <= NUM_DO_A; k++) {
        cout << "------------------------------------------------------>>> " << k << endl;

        set<long, less<long> > *ps1    = new set<long>;
        set<long, less<long> > *ps2    = new set<long>;

        cout << "  1 --> Building the *ps1." << endl;

        for (i = 0; i < DIM_SE_A; i++) {
            ps1 -> insert(DIM_SE_A-i);
            ps1 -> insert(DIM_SE_A-i);
            ps1 -> insert(DIM_SE_A-i);
            ps1 -> insert(DIM_SE_A-i);
            ps1 -> insert(DIM_SE_A-i);
            ps1 -> insert(DIM_SE_A-i);
            ps1 -> insert(DIM_SE_A-i);
            ps1 -> insert(DIM_SE_A-i);
        }

        cout << "  2 --> Building the *ps2." << endl;

        for (i = 0; i < DIM_SE_A; i++) {
            ps2 -> insert(DIM_SE_A-i);
            ps2 -> insert(DIM_SE_A-i);
            ps2 -> insert(DIM_SE_A-i);
            ps2 -> insert(DIM_SE_A-i);
            ps2 -> insert(DIM_SE_A-i);
            ps2 -> insert(DIM_SE_A-i);
            ps2 -> insert(DIM_SE_A-i);
            ps2 -> insert(DIM_SE_A-i);
        }

        cout << "  3 --> Test for equality *ps1 and *ps2." << endl;

        for (i=1; i <= NUM_DO_B; i++) {
            if (*ps1 == *ps2) {
                cout << " Equal. --> " << i <<  endl;
            } else {
                cout << " Not equal." << endl;
            }
        }

        cout << "  4 --> Deleting *ps1." << endl;

        delete ps1;

        cout << "  5 --> Deleting *ps2." << endl;

        delete ps2;

        set<double, less<double> > *ps3  = new set<double, less<double> >;
        set<double, less<double> > *ps4  = new set<double, less<double> >;

        cout << "  6 --> Building the *ps3." << endl;

        for (i = 0; i < DIM_SE_B; i++) {
            ps3 -> insert(static_cast<double>(DIM_SE_B-i));
            ps3 -> insert(static_cast<double>(DIM_SE_B-i));
            ps3 -> insert(static_cast<double>(DIM_SE_B-i));
            ps3 -> insert(static_cast<double>(DIM_SE_B-i));
            ps3 -> insert(static_cast<double>(DIM_SE_B-i));
            ps3 -> insert(static_cast<double>(DIM_SE_B-i));
            ps3 -> insert(static_cast<double>(DIM_SE_B-i));
            ps3 -> insert(static_cast<double>(DIM_SE_B-i));
        }

        cout << "  7 --> Building the *ps4." << endl;

        for (i = 0; i < DIM_SE_B; i++) {
            ps4 -> insert(static_cast<double>(DIM_SE_B-i));
            ps4 -> insert(static_cast<double>(DIM_SE_B-i));
            ps4 -> insert(static_cast<double>(DIM_SE_B-i));
            ps4 -> insert(static_cast<double>(DIM_SE_B-i));
            ps4 -> insert(static_cast<double>(DIM_SE_B-i));
            ps4 -> insert(static_cast<double>(DIM_SE_B-i));
            ps4 -> insert(static_cast<double>(DIM_SE_B-i));
            ps4 -> insert(static_cast<double>(DIM_SE_B-i));
        }

        cout << "  8 --> Test for equality *ps3 and *ps4." << endl;

        for (i=1; i <= NUM_DO_B; i++) {
            if (*ps3 == *ps4) {
                cout << " Equal. --> " << i <<  endl;
            } else {
                cout << " Not equal." << endl;
            }
        }

        cout << "  9. Deleting *ps3." << endl;

        delete ps3;

        cout << " 10. Deleting *ps4." << endl;

        delete ps4;

    }

    return 0;
}

//==============//
// End of code. //
//==============//