//=======================================================//
// Program to demonstrate use of the set template class. //
//=======================================================//

#include <iostream>
#include <set>

using std::cin;
using std::cout;
using std::endl;
using std::set;

int main()
{
    // STL containers

    set<double> s1;
    set<double> s2;
    set<double> s3;
    set<double> s4;

    // local variables

    double tmpa;
    long tmpb;
    long i;
    long k;
    int sentinel;

    // local constants

    const long NUM_DO_A = 2*1E7;
    const long NUM_DO_C = 1*1E6;
    const long NUM_DO_B = 1E2;

    // main code

    cout << "  0. Building the s4 set. " << endl;

    for (i = 0; i < NUM_DO_C; i++) {
        tmpa = static_cast<double>(i);
        s4.insert(i);
    }

    for (k = 1; k <= NUM_DO_B; k++) {
        cout << "  1. Building the s1 set. " << endl;

        for (i = 0; i < NUM_DO_A; i++) {
            tmpa = static_cast<double>(i);
            s1.insert(tmpa);
        }

        cout << "  2. Equating s1, s2, and s3. " << endl;

        s2 = s1;
        cout << " s2 = s1 --> OKAY " << endl;
        s3 = s2;
        cout << " s3 = s2 --> OKAY " << endl;
        s1 = s1;
        cout << " s1 = s1 --> OKAY " << endl;

        cout << "  3. Size of s1, s2 and s3 sets. " << endl;

        cout << s1.size() << endl;
        cout << s2.size() << endl;
        cout << s3.size() << endl;

        cout << "  4. Comparison of the s1, s2 and s3 sets. " << endl;

        cout << (s1 == s2) << endl;
        cout << (s1 == s3) << endl;
        cout << (s2 == s3) << endl;

        cout << "  5. Erasing all the elements of s1 set. " << endl;

        tmpb = s1.size();
        for (i = 0; i < tmpb; i++) {
            tmpa = static_cast<double>(i);
            s1.erase(tmpa);
        }

        cout << "  6. Size of s1, s2 and s3 sets. " << endl;

        cout << s1.size() << endl;
        cout << s2.size() << endl;
        cout << s3.size() << endl;

        cout << "  7. Erasing all the elements of s2 set. " << endl;

        tmpb = s2.size();
        for (i = 0; i < tmpb; i++) {
            tmpa = static_cast<double>(i);
            s2.erase(tmpa);
        }

        cout << "  8. Size of s1, s2 and s3 sets. " << endl;

        cout << s1.size() << endl;
        cout << s2.size() << endl;
        cout << s3.size() << endl;

        cout << "  9. Erasing all the elements of s3 set. " << endl;

        tmpb = s3.size();
        for (i = 0; i < tmpb; i++) {
            tmpa = static_cast<double>(i);
            s3.erase(tmpa);
        }

        cout << " 10. Size of s1, s2 and s3 sets. " << endl;

        cout << s1.size() << endl;
        cout << s2.size() << endl;
        cout << s3.size() << endl;

        cout << " 11. Equating s1, s2, and s3 to s4. " << endl;

        s1 = s4;
        cout << " s1 = s4 --> OKAY " << endl;
        s2 = s4;
        cout << " s2 = s4 --> OKAY " << endl;
        s3 = s4;
        cout << " s3 = s4 --> OKAY " << endl;

        cout << " 12. Size of s1, s2 and s3 sets. " << endl;

        cout << s1.size() << endl;
        cout << s2.size() << endl;
        cout << s3.size() << endl;

    }

    cin >> sentinel;

    return 0;
}

//==============//
// End of code. //
//==============//

