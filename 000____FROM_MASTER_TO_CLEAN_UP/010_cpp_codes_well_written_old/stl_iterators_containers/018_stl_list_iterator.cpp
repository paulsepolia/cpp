//=====================================//
// Insertion and deletion from a list. //
//=====================================//

#include <string>
#include <iostream>
#include <list>

using std::string;
using std::cout;
using std::endl;
using std::cin;
using std::list;

// 1. Function definition

void showlist(const string &str, const list<long> &L)
{
    list<long>::const_iterator i;

    cout << str << endl << " ";

    for (i = L.begin(); i != L.end(); ++i) {
        cout << *i << " ";
    }

    cout << endl;
}

// 2. The main function.

int main()
{
    list<long> L;
    long x;

    cout << "Enter positive integers, followed by 0:" << endl;

    while (cin >> x, x != 0) {
        L.push_back(x);
    }

    showlist("Initial list: ", L);

    L.push_front(123);

    showlist("After inserting 123 at the beginning:", L);

    list<long>::iterator i = L.begin();

    L.insert(++i, 456);

    showlist("After inserting 456 at the second position:", L);

    i = L.end();

    L.insert(--i, 999);

    showlist("After inserting 999 just before the end:", L);

    i = L.begin();

    x = *i;

    L.pop_front();

    cout << "Deleted at the beggining: " << x << endl;

    showlist("After this deletion:", L);

    i = L.end();

    x = *(--i);

    L.pop_back();

    cout << "Deleted at the end: " << x << endl;

    showlist("After this deletion:", L);

    i = L.begin();

    x = *++i;

    cout << "To be deleted: " << x << endl;

    L.erase(i);

    showlist("After this deletion (of second element):", L);

    return 0;
}

//==============//
// End of code. //
//==============//
