// recursive factorial

#include <iostream>

using namespace std;

// declaration of the recursive factorial function

long double fact(int num);

// the main function

int main()
{
    int n;
    long double res;

    cout << " the integer : ";

    cin >> n;

    res = fact(n);

    cout << " the factorial of " << n << " is :";
    cout << res;

    cout << endl;
}

// the definition of the recursive factorial function

long double fact(int num)
{
    if (num==0) {
        return 1;
    } else {
        return num * fact(num-1);
    }
}

// this is the end of the code
