
// Fig. 7.29: Fibonacci.cpp
// Testing the recursive fibonacci function.

#include <iostream>
using std::cout;
using std::endl;
using std::cin;

unsigned long int fibonacci ( unsigned long int ); // function prototype

int main()
{
    int const trials = 50 ;

    // calculate the fibonacci values of 0 through 10
    for ( int counter = 0; counter <= trials ; counter++ ) {
        cout << "fibonacci( " << counter << " ) = "
             << fibonacci( counter ) << endl;
    }

    return 0;
}

// recursive methood fibonacci.
unsigned long int fibonacci ( unsigned long int number )
{
    if ( ( number == 0 ) || ( number == 1 ) ) { // base cases
        return number ;
    } else {
        return ( fibonacci( number - 1) + fibonacci( number - 2 ) ) ;
    }
} // end function fibonacci.


