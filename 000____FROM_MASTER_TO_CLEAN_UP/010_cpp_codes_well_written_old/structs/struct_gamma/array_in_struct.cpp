// array inside a struct

#include <iostream>

using namespace std;

const int ARRAY_SIZE = 10000;
const int iLim = 10000;

struct listType {
    int listElem[ARRAY_SIZE];
    int listLength;
};

int main()
{

    listType struct_a;
    int i;

    cout << " The array size is " << ARRAY_SIZE << endl;

    cout << " The contents of the array inside the struct are " << endl;

    for ( i = ARRAY_SIZE-iLim; i < ARRAY_SIZE; i++ ) {
        cout << i << " -- " << struct_a.listElem[i] << endl;
    }

    cout << " The contents of the array after initialization are " << endl;

    for ( i = 0; i < ARRAY_SIZE; i++ ) {
        struct_a.listElem[i] = 0;
    }

    for ( i = ARRAY_SIZE-iLim; i < ARRAY_SIZE; i++ ) {
        cout << i << " -- " << struct_a.listElem[i] << endl;
    }

    for ( i = 0; i < ARRAY_SIZE; i++ ) {
        struct_a.listElem[i] = i ;
    }

    for ( i = ARRAY_SIZE-iLim; i < ARRAY_SIZE; i++ ) {
        cout << i << " -- " << struct_a.listElem[i] << endl;
    }

    return 0;
}



