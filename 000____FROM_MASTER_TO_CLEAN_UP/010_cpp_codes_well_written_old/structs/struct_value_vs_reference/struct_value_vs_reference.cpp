#include <iostream>
#include <iomanip>
using namespace std;

// 1. struct declaration

const long int dimenAlpha = 10E+4;

struct Employee { // this is a global declaration
    int idNum;
    double payRate;
    double hours[ dimenAlpha ];
};

// 2. function prototype

double calcNetVa ( Employee );

double calcNetRe ( Employee & );

// 3. the main function

int main()
{
    Employee emp;
    emp.idNum = 6782;
    emp.payRate = 8.93;
    emp.hours[1] = 40.5;

    long int i;
    const long int dimenBeta = 10E+4;
    double netPayVa;
    double netPayRe;

    for ( i=0; i<dimenBeta; i++) {
        netPayVa = calcNetVa( emp );    // pass copies of the values in emp
    }

    cout << " pass by value is over " << endl;

    for ( i=0; i<dimenBeta; i++) {
        netPayRe = calcNetRe( emp );    // pass by reference
    }

    cout << " pass by reference is over " << endl;

    // set output formats

    cout << setw(10)
         << setiosflags(ios::fixed)
         << setiosflags(ios::showpoint)
         << setprecision(3);

    cout << " The net pay for employee " << emp.idNum
         << " is $ " << netPayVa << endl;

    cout << " The net pay for employee " << emp.idNum
         << " is $ " << netPayRe << endl;

    return 0;
}

// 4. function definition

double calcNetVa( Employee temp )
{
    return temp.payRate * temp.hours[1] ;
}

double calcNetRe( Employee & temp )
{
    return temp.payRate * temp.hours[1] ;
}

