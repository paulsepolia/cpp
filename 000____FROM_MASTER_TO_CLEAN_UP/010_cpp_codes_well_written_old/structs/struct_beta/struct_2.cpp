#include <iostream>
using namespace std;

// 1. struct declaration

struct Date { // this is a global declaration
    int month;
    int day;
    int year;
};

// 2. the main function

int main()
{
    long int i;
    const long int dimen = 10E+9;

    for ( i=0; i<dimen; i++ ) {
        Date birth;
        birth.month = 12+i;
        birth.day   = 28+i;
        birth.year  = 86+i;

        if ( i==dimen-1)  cout << " My birth day is " << birth.month << '/'
                                   << birth.day << '/' << birth.year << endl;
    }

    return 0;

}


