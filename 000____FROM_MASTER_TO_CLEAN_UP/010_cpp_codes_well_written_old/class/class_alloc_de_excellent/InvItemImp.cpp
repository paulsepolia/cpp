// this is the implementation file of the class: InvItem

#include <iostream>
#include <cstring>
#include "InvItem.h"

using namespace std;

// 1.

InvItem::InvItem(char *descr, int number)
{
    description = new char[strlen(descr)];
    strcpy(description, descr);
    units = number;
}

// 2.

InvItem::~InvItem()
{
    cout << " deleting using the destructor " << endl;
    delete [] description;
}

// 3.

char * InvItem::getDesc()
{
    return description;
}

// 4.

int InvItem::getUnits()
{
    return units;
}
