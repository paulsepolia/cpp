// this is the implementation file of the class: cashRegister

#include <iostream>
#include "cash_register_header.h"

using namespace std;

int cashRegister::getCurrentBalance() const
{
    return cashOnHand;
}

void cashRegister::acceptAmount( int amountIn )
{
    cashOnHand = cashOnHand + amountIn;
}

cashRegister::cashRegister( int cashIn )
{
    if ( cashIn >= 0 ) {
        cashOnHand = cashIn;
    } else {
        cashOnHand = 500;
    }
}

