// this is the implementation file of the class: demoConstructor

#include <iostream>
#include "demoConstructor.h"

using namespace std;

demoConstructor::demoConstructor()
{
    cout << " Now the default constructor is running.\n";
}

// this is the end of the implementation file
