// this is the driver program

#include <iostream>
#include "demoConstructor.h"

using namespace std;

int main()
{
    cout << " This is displayed before the object is created.\n";

    demoConstructor demoObj;  // define the demoConstructor object

    cout << " This is displayed after the object is created.\n";

    return 0;
}

// this is the end of the driver program
