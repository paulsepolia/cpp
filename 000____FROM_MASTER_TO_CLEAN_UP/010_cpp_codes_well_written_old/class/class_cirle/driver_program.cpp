// this is the driver program for the class: classCircle

#include <iostream>
#include <cmath>
#include "classCircle.h"

using namespace std;

int main()
{
    // define two circle objects
    classCircle sphere1;
    classCircle sphere2;

    // call the setRadius for each object

    sphere1.setRadius(1);
    sphere2.setRadius(2.5);

    // call the getArea function for each sphere and
    // display the returned result

    cout << " the area of sphere1 is " << sphere1.getArea() << endl;
    cout << " the area of sphere2 is " << sphere2.getArea() << endl;

    return 0;
}
