// this is the implemenation file fo the class: classCircle

#include <iostream>
#include <cmath>
#include "classCircle.h"


// 1.

void classCircle::setRadius(double r)
{
    radius = r;
}

// 2.

double classCircle::getArea()
{
    return 3.14159 * pow(radius, 2);
}

// this is the end of the implementation file
