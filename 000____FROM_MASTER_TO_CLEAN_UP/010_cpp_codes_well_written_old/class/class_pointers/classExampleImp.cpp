// this is the implementation file of the class: classExample

#include <iostream>
#include "classExample.h"

using namespace std;

// 1.

void classExample::setX(int a)
{
    x = a;
}

// 2.

void classExample::print() const
{
    cout << " x = " << x << endl;
}

// this is te end of the implementation file

