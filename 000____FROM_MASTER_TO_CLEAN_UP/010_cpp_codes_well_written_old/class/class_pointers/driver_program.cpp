
#include <iostream>
#include "classExample.h"

using namespace std;

int main()
{
    classExample *cExpPtr;
    classExample cExpObject;

    cExpPtr = &cExpObject;

    cExpPtr -> setX(5);
    cExpPtr -> print();

    (*cExpPtr).setX(11);
    (*cExpPtr).print();

    cExpObject.setX(12);
    cExpObject.print();

    return 0;
}


