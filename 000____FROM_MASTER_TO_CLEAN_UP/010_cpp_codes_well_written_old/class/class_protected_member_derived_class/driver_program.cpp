// this is the driver program
// accessing protected members of a base class in the derived class.
//

#include <iostream>
#include "base_class_header.h"
#include "derived_class_header.h"

using namespace std;

int main()
{
    bClass bObject;
    dClass dObject;

    bObject.print();
    cout << endl;

    cout << " *** Derived class object *** " << endl;

    dObject.setData('&', 2.5, 7);

    dObject.print();

    return 0;
}
