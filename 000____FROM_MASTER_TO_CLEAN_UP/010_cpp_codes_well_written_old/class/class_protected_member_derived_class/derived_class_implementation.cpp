// this is the implementation file of the class: dClass

#include <iostream>
#include "base_class_header.h"
#include "derived_class_header.h"

using namespace std;

void dClass::setData(char ch, double v, int a)
{
    bClass::setData(v);

    bCh = ch; // initialize bCh using the assignment statement
    // access of the protected to base Class variable bCh.
    // The bCh variable is still protected in the derived class

    dA = a;   // Access of the dA private variable
}

void dClass::print() const
{
    bClass::print(); // access of the bCh protected through the use of the print()
    // bClass function.

    // access of the dA private variable
    cout << " Derived class dA = " << dA << endl;
}


