// this is the header file of the class: dateType

#ifndef H_dateType
#define H_dateType

class dateType {
public:
    void setDate(int month, int day, int year);
    // function to set the date.
    // the member variable dMonth, dDay, and dYear
    // are set according to the parameters.
    // Postcondition: dMonth = month; dDay = day; dYear = year

    int getDay() const;
    // function to return the day.
    // postcondition: the value of the dDay is returned.

    int getMonth() const;
    // function to return the month.
    // postcondition: the value of the dMonth is returned.

    int getYear() const;
    // function to return the year.
    // postcondition: the value of the dYear is returned.

    void printDate() const;
    // function to output the date in the form mm-dd-yyyy.

    dateType(int month = 1, int day = 1,int year = 1900);
    // constructor to the set date
    // the member variables dMonth, dDay, and dYear are set
    // according to the parameters.
    // postcondition: dMonth = month; dDay = day; dYear = year;
    // if no values are specified, the default
    // values are used to initialize the member variables.

private:
    int dMonth; // varibale to store the month
    int dDay;   // variable to store the day
    int dYear;  // variable to store the year
};
i

#endif

// this is the end of the header file
