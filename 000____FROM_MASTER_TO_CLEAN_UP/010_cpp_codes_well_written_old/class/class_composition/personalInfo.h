// this is the header file of the class: personalInfo

#ifndef H_personalInfo
#define H_personalInfo


#include <string>
#include "person_type_header.h"
#include "date_type_header.h"

class personalInfo {
public:
    void setpersonalInfo( string first,
                          string last,
                          int month,
                          int day,
                          int year,
                          int ID);
    // Function to set the personal information,
    // the member variables are set according to the parameters.
    // postcondition: firstName = first;
    //                lastName = last;
    //                dMonth = month;
    //                dDay = day;
    //                dYear = year;
    //                personID = ID;

    void printpersonalInfo() const;
    // function to print the personal information.

    personalInfo( string first = "",
                  string last = "",
                  int month = 1,
                  int day = 1,
                  int year = 1900,
                  int ID = 0 );
    // constructor
    // The member variable are set according to the parameters.
    // postconditin: firstName = first;
    //               lastName = last;
    //               dMonth = month;
    //               dDay = day;
    //               dYear = year;
    //               personID = ID;
    // if no values are specified, the default
    // values are used to initialize the member variables.

private:
    personType name;
    dateType dDay;
    int personID;
};

#endif

// this is the end of the header file.
