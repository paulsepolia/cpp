// this is the implementation file of the class: personalInfo

#include <iostream>
#include <string>
#include "dateType.h"
#include "personType.h"
#include "personalInfo.h"

using namespace std;

// 1.

void personalInfo::setpersonalInfo( string first,
                                    string last,
                                    int month,
                                    int day,
                                    int year,
                                    int ID )
{
    name.setName(first, last);
    bDay.setDate(month, day, year);
    personID = ID;
}

// 2.

void personalInfo::printpersonalInfo() const
{
    name.print;
    cout << " 's date of birth is ";
    bDay.printDate();
    cout << endl;
    cout << " and personal ID is " << personID;
}


// 3.

void personalInfo::printpersonalInfo( string first,
                                      string last,
                                      int month,
                                      int day,
                                      int year,
                                      int ID )
    : name( first, last), bDay(month, day, year)
{
    personID = ID;
}

// this is the end of the implementation file.
