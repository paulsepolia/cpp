// this is the header file of the class: rectangleType

class rectangleType {
public:
    void setDimension(double l, double w);
    // function to set the length and width of the rectangle.
    // postcondition: length = l; width = w;

    double getLength() const;
    // function to return the length of the rectangle.
    // postcondition: the value of length is returned.

    double getWidth() const;
    // function to return the width of the rectangle.
    // postcondition: the value of width is returned.

    double area() const;
    // function to return the area of the rectangle.
    // postcondition: the area of the rectangle is
    // calculated and returned.

    double perimeter() const;
    // function to return the perimeter of the rectangle.
    // postcondition: the perimeter of the rectangle is
    // calculated and returned.

    void print() const;
    // function to output the length and width of the rectangle.

    rectangleType();
    // default constructor
    // postcondition: length = 0; width = 0;

    rectangleType( double l, double w);
    // constructor with parameters
    // postcondition: length = l, width = w;

private:
    double length;
    double width;
};

// end of the header file of the class: rectangleType
