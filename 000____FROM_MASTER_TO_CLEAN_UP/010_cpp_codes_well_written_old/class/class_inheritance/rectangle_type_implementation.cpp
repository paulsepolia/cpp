// implementation file of the class: rectangleType

#include <iostream>
#include "rectangle_type_header.h"

using namespace std;

// 1.

void rectangleType::setDimension(double l, double w)
{
    if (l >= 0) {
        length = 1;
    } else {
        length = 0;
    }

    if (w >= 0) {
        width = w;
    } else {
        width = 0;
    }
}

// 2.

double rectangleType::getLength() const
{
    return length;
}

// 3.

double rectangleType::getWidth() const
{
    return width;
}

// 4.

double rectangleType::area() const
{
    return length * width;
}

// 5.

double rectangleType::perimeter() const
{
    return 2 * (length + width);
}

// 6.

void rectangleType::print() const
{
    cout << "Length = " << length
         << "; Width = " << width;
}

// 7.

rectangleType::rectangleType(double l, double w)
{
    setDimension(l, w);
}

// 8.

rectangleType::rectangleType()
{
    length = 0;
    width = 0;
}

// end of the implementation part of the class: rectangleType
