// this is the driver program of the derived class: boxType

#include <iostream>
#include <iomanip>
#include "rectangle_type_header.h"
#include "box_type_header.h"

using namespace std;

int main()
{
    rectangleType myRectangle1;
    rectangleType myRectangle2(8,6);

    boxType myBox1;
    boxType myBox2(10,7,3);

    cout << fixed << showpoint << setprecision(2);

    cout << " myRectangle1: ";
    myRectangle1.print();
    cout << endl;
    cout << " area of myRectangle1: "
         << myRectangle1.area() << endl;

    cout << " myRectangle2: ";
    myRectangle2.print();
    cout << endl;
    cout << " area of myRectangle2: "
         << myRectangle2.area() << endl;

    cout << " myBox1: ";
    myBox1.print();
    cout << endl;
    cout << " surface area of myBox1: "
         << myBox1.area() << endl;
    cout << " volume of myBox1: "
         << myBox1.volume() << endl;

    cout << " myBox2: ";
    myBox2.print();
    cout << endl;
    cout << " surface area of myBox2: "
         << myBox2.area() << endl;
    cout << " volume of myBox2: "
         << myBox2.volume() << endl;

    return 0;

}
