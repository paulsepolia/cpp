// this is the header file of the class: boxType
// the class boxType inherits some features of the class: rectangleType
// so it is not a stand alone class, it is a derived class
// The base class is the: rectangleType.

class boxType: public rectangleType {
public:
    void setDimension(double l, double w, double h);
    // function to set the length, width, and height
    // of the box.
    // postcondition: length = l; width = w; height = h;

    double getHeight() const;
    // function to return the height of the box.
    // postcondition: the value of height is returned.

    double area() const;
    // function to return the surface area of the box.
    // postcondition: the surface area of the box is
    // calculated and returned.

    double volume() const;
    // function to return the volume of the box.
    // postcondition: the volume of the box is
    // calculated and returned.

    void print() const;
    // function to output the length, width and height of the box.

    boxType();
    // default constructor
    // postcondition: length = 0; width = 0; height = 0;

    boxType(double l, double w, double h);
    // constructor with parameters
    // postcondition: length = l; width = w; height = h;

private:
    double height;
};


