// this is the implementation file of the class: demo

#include <iostream>
#include "demo.h"

using namespace std;

// 1.
// constructor function definition

demo::demo()
{
    cout << " an object of class 'demo' has just been defined,"
         << " so the constructor is running.\n";
}

// 2.
// destructor function definition

demo::~demo()
{
    cout << " now the destructor is running.\n";
}

// this is the end of the implementation file
