// this is the driver program of the class: demo
// it demonstrates the use of the destructor.

#include <iostream>
#include "demo.h"

using namespace std;

int main()
{
    demo demoObj;

    cout << " the object now exists, but is about to be destroyed.\n";

    return 0;
}
