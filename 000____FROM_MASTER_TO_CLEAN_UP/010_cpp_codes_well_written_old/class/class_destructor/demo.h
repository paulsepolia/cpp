// this is the header file of the class: demo

#ifndef H_demo
#define H_demo

class demo {
public:
    demo(); // constructor prototype
    ~demo(); // destructor prototype
};

#endif

// this is the end of the header file
