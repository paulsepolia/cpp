// implementation of the class: clockType

#include <iostream>

using namespace std;

class clockType {
public:
    void setTime( int, int, int );
    void getTime( int&, int&, int&) const;
    void printTime() const;
    void incrementSeconds();
    void incrementMinutes();
    void incrementHours();
    bool equalTime( const clockType& ) const;
    // constructor with parameters and default values
    clockType( int = 0, int = 0, int = 0);

private:
    int hr;
    int min;
    int sec;
};

// the main function

int main()
{
    clockType myClock;
    clockType yourClock;

    int hours;
    int minutes;
    int seconds;

    // set the time of object: myClock

    myClock.setTime(5, 4, 30);

    // print the time of object: myClock

    cout << " myClock: ";
    myClock.printTime();
    cout << endl;

    // print the time of object: yourClock

    cout << " yourClock: ";
    yourClock.printTime();
    cout << endl;

    // compare myClock and yourClock

    if ( myClock.equalTime(yourClock) ) {
        cout << " Both times are equal. " << endl;
    } else {
        cout << " The two times are not equal. " << endl;
    }

    // entering the hours, minutes and seconds

    cout << " Enter the hours, minutes and seconds: ";
    cin >> hours >> minutes >> seconds;
    cout << endl;

    // setting the time of myClock

    myClock.setTime(hours, minutes, seconds);

    // print the time of myClock

    cout << " New myClock: ";
    myClock.printTime();
    cout << endl;

    // increment the time of myClock by one second

    myClock.incrementSeconds();

    // print the time of myClock

    cout << " After incrementing myClock by one second, myClock: ";
    myClock.printTime();
    cout << endl;

    // retrieve the hours, minutes, and seconds of the object myClock

    myClock.getTime(hours, minutes, seconds);

    // output the value of hours, minutes, and seconds

    cout << " hours = " << hours
         << ", minutes = " << minutes
         << ", seconds = " << seconds << endl;


    return 0;
}

// definition of the class member functions

// setTime

void clockType::setTime( int hours, int minutes, int seconds )
{
    if ( 0 <= hours && hours < 24 ) {
        hr = hours;
    } else {
        hr = 0;
    }

    if( 0 <= minutes && minutes < 60 ) {
        min = minutes;
    } else {
        min = 0;
    }

    if ( 0 <= seconds && seconds < 60 ) {
        sec = seconds;
    } else {
        sec = 0;
    }
}


// getTime

void clockType::getTime( int& hours, int& minutes, int& seconds ) const
{
    hours = hr;
    minutes = min;
    seconds = sec;
}

// incrementHours

void clockType::incrementHours()
{
    hr++;

    if ( hr > 23 ) {
        hr = 0 ;
    }
}

// incrementMinutes

void clockType::incrementMinutes()
{
    min++;

    if ( min > 59 ) {
        min = 0;
        incrementHours();
    }
}


// incrementSeconds

void clockType::incrementSeconds()
{
    sec++;

    if ( sec > 59 ) {
        sec = 0;
        incrementMinutes();
    }
}

// printTime

void clockType::printTime() const
{
    if ( hr < 10 ) {
        cout << "0";
    }

    cout << hr << ":";

    if ( min < 10 ) {
        cout << "0";
    }

    cout << min << ":";

    if (sec < 10 ) {
        cout << "0";
    }

    cout << sec;
}

// equalTime

bool clockType::equalTime( const clockType& otherClock ) const
{
    return ( hr == otherClock.hr &&
             min == otherClock.min &&
             sec == otherClock.sec ) ;

}

// constructor with parameters and defaults values

clockType::clockType( int hours, int minutes, int seconds )
{
    setTime( hours, minutes, seconds );
}
