// this is the driver program

#include <iostream>
#include "pointerDataClass.h"

using namespace std;

void testCopyConst(pointerDataClass temp);

// the main program

int main()
{
    pointerDataClass listOne;

    int num;
    int index;

    cout << " Enter 5 integers. " << endl;

    for (index = 0; index < 5; index++) {
        cin >> num;
        listOne.insertAt(index, num);
    }

    cout << " listOne: ";
    listOne.print();
    cout << endl;

    // declare listTwo and initialize it using listOne

    pointerDataClass listTwo(listOne);

    cout << " listTwo: ";
    listTwo.print();
    cout << endl;

    listTwo.insertAt(5,34);
    listTwo.insertAt(2,-76);

    cout << " After modifying listTwo: ";
    listTwo.print();
    cout << endl;

    cout << " After modifying listTwo, listOne: ";
    listOne.print();
    cout << endl;

    cout << " Calling the function testCopyConst" << endl;

    testCopyConst(listOne);

    cout << " After a call to the function testCopyConst, " << endl
         << "   listOne is: ";

    listOne.print();
    cout << endl;

    return 0;
}

// definition

void testCopyConst(pointerDataClass temp)
{
    cout << " *** Inside the function testCopyConst ***" << endl;

    cout << " Object temp data: ";
    temp.print();
    cout << endl;

    temp.insertAt(3,-100);
    cout << " after changing temp: ";
    temp.print();
    cout << endl;

    cout << " *** Exiting the function testCopyConst ***" << endl;
}
