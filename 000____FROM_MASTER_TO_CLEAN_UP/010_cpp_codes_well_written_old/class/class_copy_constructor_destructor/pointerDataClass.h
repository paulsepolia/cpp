// this is the header file of the class : pointerDataClass

#ifndef H_pointerDataClass
#define H_pointerDataClass

class pointerDataClass {
public:
    void print() const;
    // function to output the data stored in the array p.

    void insertAt(int index, int num);
    // function to insert num into the array p at the
    // position specified by index.
    // if index is out of bounds, the program is terminated.
    // if index is within bounds, but greater than the index
    // of the last item in the list, num is added at the
    // end of the list

    pointerDataClass(int size = 10);
    // constructor
    // creates an array of the size specified by the
    // parameter size; the default array size is 10.

    ~pointerDataClass();
    // destructor
    // deallocates the memory space occupied by the array p.

    pointerDataClass(const pointerDataClass& otherObject);
    // copy deep constructor

private:
    int maxSize; // variable to store the maximum size of p
    int length;  // variable to store the number of elements in p
    int *p;      // pointer to a int array
};

#endif

// this is the end of the header file



