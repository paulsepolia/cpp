#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

const int MAXRECS=3; // maximum numbre of records

struct TeleType {
    string name;
    string phoneNo;
    TeleType * nextaddr;
};

void populate( TeleType * ); // function prototype
void display( TeleType * ); // function prototype

int main()
{
    int i;
    TeleType *list, *current; // two pointers to TeleType structure

    // get a pointer to the first structure in the list
    list = new TeleType;
    current = list;

    // populate the current structure and create the remaining structures
    for ( i=0; i<MAXRECS-1; i++ ) {
        populate( current );
        current -> nextaddr = new TeleType;
        current = current -> nextaddr;
    }

    populate( current ); // populate the last structure
    current -> nextaddr = NULL; // set the last address to a NULL address
    cout << endl;
    cout << " The lists consists of the following records: " << endl;
    display(list); // display the structures

    return 0;
}

// input a name and a phone number
void populate ( TeleType * record )
{
    cout << " Enter a name: ";
    getline( cin, record -> name );
    cout << " Enter the phone number: ";
    getline( cin, record -> phoneNo );

    return;
}


void display( TeleType * contents )
{
    while( contents != NULL ) {
        cout << endl << setiosflags(ios::left)
             << setw(30) << contents -> name
             << setw(20) << contents -> phoneNo;

        contents = contents -> nextaddr;
    }

    cout << endl;

    return;
}

