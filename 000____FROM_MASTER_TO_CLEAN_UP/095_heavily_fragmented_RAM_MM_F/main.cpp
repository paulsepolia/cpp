#include <iostream>
#include <cmath>
#include <chrono>
#include <queue>
#include <random>
#include <unordered_set>
#include <algorithm>
#include <thread>
#include <mutex>

class benchmark_timer {
public:

    explicit benchmark_timer() :
            _res(0.0),
            t1{std::chrono::steady_clock::now()},
            t2{std::chrono::steady_clock::now()},
            t_prev{std::chrono::steady_clock::now()} {};

    auto print_time() const -> void {
        const auto t_loc{std::chrono::steady_clock::now()};
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t_loc - t_prev).count()};
        t_prev = t_loc;
        std::cout << " --> time used since last reset (secs) = " << total_time << std::endl;
    }

    auto reset_timer() -> void {
        const auto t_loc{std::chrono::steady_clock::now()};
        t_prev = t_loc;
        t1 = t_loc;
        t2 = t_loc;
    }

    ~benchmark_timer() {
        t2 = std::chrono::steady_clock::now();
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count()};
        std::cout << " --> res = " << _res << std::endl;
        std::cout << " --> total time used since last reset (secs) = " << total_time << std::endl;
    }

    auto set_res(const double &res) -> void {
        _res = res;
    }

private:

    double _res{};
    decltype(std::chrono::steady_clock::now()) t1{};
    decltype(std::chrono::steady_clock::now()) t2{};
    mutable decltype(std::chrono::steady_clock::now()) t_prev{};
};

const auto DO_MAX{(size_t) std::pow(10.0, 9.0)};
const auto DIM_MAX{(size_t) 120'000'000};
const auto RECORD_SIZE((size_t) 24);

auto mtx{std::mutex{}};

auto main() -> int {

    auto l1{[]() {
        for (size_t kk = 0; kk < DO_MAX; kk++) {

            mtx.lock();
            std::cout << "------------------------------------------->> kk = " << kk << std::endl;
            mtx.unlock();

            benchmark_timer ot;

            auto vec{std::vector<void *>()};
            auto cont{std::unordered_set<void *>()};

            for (size_t jj = 0; jj < DIM_MAX; jj++) {
                void *ptr = malloc(RECORD_SIZE);
                cont.emplace(ptr);
                vec.push_back(ptr);
            }

            mtx.lock();
            ot.print_time();
            mtx.unlock();

            ot.reset_timer();

            std::for_each(cont.begin(), cont.end(), [](void *ptr) { free(ptr); });

            mtx.lock();
            ot.print_time();
            mtx.unlock();

            ot.reset_timer();

            cont.clear();
            vec.clear();
        }
    }};

    auto t1{std::thread(l1)};
    auto t2{std::thread(l1)};

    t1.join();
    t2.join();
}
