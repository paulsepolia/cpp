
//=====================//
// test --> 0019 --> b //
//=====================//

// Status --> FINISHED

//==============================================================================
//
// --> the main points here are:
//
// --> 1 --> observe the behavior of operator = when it returns by value
//
//==============================================================================

#include <iostream>

using std::endl;
using std::cin;
using std::cout;

// class --> A

class A {
public:

    // constructor

    A(int n = 0) : m_n (n)
    {
        cout << " --> constructor --> A" << endl;
    }

    // copy constructor

    A(const A &a) : m_n (a.m_n)
    {
        cout << " --> copy contructor --> A" << endl;

        ++m_copy_ctor_calls;
    }

    // destructor

    ~A()
    {
        cout << " --> destructor --> ~A" << endl;

        ++m_dtor_calls;
    }

    // A operator = (const A & a)

    A  operator = (const A & a)
    {
        cout << " --> A operator = (const A & a)" << endl;

        this->m_n = a.m_n;

        return *this;
    }

private:

    int m_n;

public:

    static int m_copy_ctor_calls;
    static int m_dtor_calls;
};

// initialize static members

int A::m_copy_ctor_calls = 0;
int A::m_dtor_calls = 0;

// the main function

int main()
{
    {
        // local innermost --> 1 --> scope --> starts

        cout << " --> 1" << endl;

        A * p = NULL; // p is a pointer to an object of class A
        // DOES NOT CALL ANY CONSRTUCTOR or DESTRUCTOR

        cout << " --> 2" << endl;

        {
            // innermost --> 2 --> scope --> starts

            cout << " --> 3" << endl;

            const A a = 2; // creates const object a
            // to do so calls the constructor A,
            // since the constructor is not explicit
            // there is an implicit conversion of 2
            // to an object of class A
            // as if the statement had been written
            // const A a(2);
            // IF THE CONSTRUCTOR WAS EXPLICIT
            // THE ABOVE IS A COMPILER ERROR

            cout << " --> 4" << endl;

            p = new A[3]; // creates an array of three objects
            // to the beginning of which p points
            // to do so, calls three times the constructor A

            cout << " --> 5" << endl;

            p[0] = a; // 1 --> operator =
            // 2 --> copy constructor
            // 3 --> destructor for step 2

            cout << " --> 6" << endl;

        } // innermost --> 2 --> scope --> ends

        // at the exit of the innermost --> 2 scope
        // the local object a is being destroyed so
        // there is a call to the destructor ~A

        cout << " --> 7" << endl;

        cout << A::m_copy_ctor_calls << endl; // it is zero

        cout << " --> 8" << endl;

        cout << A::m_dtor_calls << endl; // it is one, for the object a

        cout << " --> 9" << endl;

        p[1] = A(1); // creates an object of type class A
        // 1 --> constructor
        // 2 --> operator =
        // 3 --> copy constructor
        // 4 --> destructor for step 3
        // 5 --> destructor for step 4

        cout << " --> 10" << endl;

        p[2] = 2; // 1 --> constructor
        // 2 --> operator =
        // 3 --> copy constructor
        // 4 --> destructor for step 3
        // 5 --> destructor for step 4

        cout << " --> 11" << endl;

        delete [] p; // calls 3 times the destructor A

        cout << " --> 12" << endl;

        cout << A::m_copy_ctor_calls << endl; // it is 3
        // steps 5, 9, 10

        cout << " --> 13" << endl;

        cout << A::m_dtor_calls << endl; // it is 9

        cout << " --> 14" << endl;

    } // local innermost --> 1 --> scope --> ends

    cout << " --> 15 --> exit" << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    cout << " --> 16 --> exit" << endl;

    return 0;
}

//======//
// FINI //
//======//

