#!/bin/bash

  # 1. compile

  icpc -O3                \
       -xHost             \
       -Wall              \
       -std=c++11         \
       -static            \
       -wd2012            \
       driver_program.cpp \
       -o x_intel
