
//===============//
// test --> 0011 //
//===============//

// Status --> FINISHED

//==============================================================================
//
// --> tha main points here are:
//
// --> 1 --> vector<A> a({A(1), A(2), A(3)});
//	     the above statement creates a vector of type vector<A>
//	     The vector has three elements. The three elements are
//	     created in the following way. The constructor is beign called 3 times
//	     to create the tmp elements. Then the copy constructor is being called
//	     3 times to create the elements of vector and then the TMP elements
//	     are being destroyed by the calling the destructor 3 times
//
// --> 2 --> list<A> l({A(1), A(2), A(3)});
//	     SAME AS --> 1
//
// --> 3 --> the "sort" member function to list container behaves
//           different than "sort" generic algorithm
//
//==============================================================================

#include <algorithm>
#include <iostream>
#include <list>
#include <vector>

using std::cout;
using std::cin;
using std::endl;
using std::vector;
using std::list;
using std::sort;

// the class --> Int

class Int {
public:

    // constructor

    Int(int i = 0) : m_i (i)
    {
        cout << " --> constructor --> Int" << endl;
    }

    // destructor --> ~Int

    ~Int()
    {
        cout << " --> destructor --> ~Int" << endl;
    }

    // copy constructor

    Int(const Int & a)
    {
        cout << " --> copy constructor --> Int" << endl;

        this->m_i = a.m_i;
    }

    // operator <

    bool operator < (const Int & a) const
    {
        cout << " --> bool operator < (const Int &) const" << endl;

        return (this->m_i < a.m_i);
    }

    // operator =

    Int & operator = (const Int & a)
    {
        cout << " --> Int & operator = (const Int &)" << endl;

        this->m_i = a.m_i;

        ++m_assignments;

        return *this;
    }

    // static function

    static int get_assignments()
    {
        cout << " --> function --> get_assignments" << endl;

        return m_assignments;
    }

private:

    int m_i;
    static int m_assignments;
};

// static private member initialization

int Int::m_assignments = 0;

// the main function

int main()
{
    {
        // local innermost scope --> starts

        cout << " --> 1" << endl;

        list<Int> l({ Int(3), Int(1) }); // creates a list with two elements of type Int
        // so calls two times the constructor
        // then two times the copy constructor
        // and then two times the destructor

        cout << " --> 2" << endl;

        l.sort(); // sort here is the member function of the list container
        // that version of sort DOES NOT call the operator =
        // calls only the operator <

        cout << " --> 3" << endl; // so, Int::get_assignments() is equal to 0
        // and the following statement is false and 0

        cout << (Int::get_assignments() > 0 ? 1 : 0) << endl;

        cout << " --> 4" << endl;

        vector<Int> v({ Int(2), Int() }); // creates two objects of type Int
        // so calls two times the constructor
        // then two times the copy constructor
        // and two times the destructor

        cout << " --> 5" << endl;

        sort(v.begin(), v.end()); // that version of sort, generic algorithm
        // calls the operator =
        // and the operator <

        cout << " --> 6" << endl; // so, Int::get_assignments() is NOT equal to 0
        // and the following statement is true and 2

        cout << (Int::get_assignments() > 0 ? 2 : 0) << endl;

        cout << " --> 7" << endl;

        Int(10);

        cout << " --> 8" << endl;

        Int k1;

        cout << " --> 9" << endl;

        Int();

        cout << " --> 10" << endl;

        Int k2(k1);

        cout << " --> 11 --> end" << endl;

    } // local innermost scope --> ends

    // at the exit of the innermost scope
    // the ~Int destructor is called 6 times
    // for k1
    // for k2
    // two times for elements of the l list<Int>
    // two times for elements of the v vector<Int>

    cout << " --> 12 --> exit" << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    cout << " --> 13 --> exit" << endl;

    return 0;
}

//======//
// FINI //
//======//

