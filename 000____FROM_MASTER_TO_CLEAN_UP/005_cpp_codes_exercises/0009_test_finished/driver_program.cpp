
//===============//
// test --> 0009 //
//===============//

// Status --> FINISHED

//==============================================================================
//
// the main points here are:
//
// 1 --> B(int n = 5) : A(), m_x (++m_i), m_a (new A[2])
//			results in creation of the following objects
//			while executing the B b;
//			--> m_x of type A
//			--> m_a[0] of type A
//			--> m_a[1] of type A
//			--> b of type B
//
//==============================================================================

#include <iostream>

using std::endl;
using std::cin;
using std::cout;

// class --> A

class A {
public:

    // constructor

    A(int n = 0) : m_i(n)
    {
        cout << " --> constructor --> A" << endl;
        cout << m_i << endl;
        ++m_i;
    }

    // destructor

    virtual ~A()
    {
        cout << " --> destructor --> ~A" << endl;
    }

protected:

    int m_i;
};

// class --> B

class B : public A {
public:

    // constructor

    B(int n = 5) : A(), m_x (++m_i), m_a (new A[2])
    {
        cout << " --> constructor --> B" << endl;
        cout << m_i << endl;
    }

    // destructor

    virtual	~B()
    {
        cout << " --> destructor --> ~B" << endl;
        delete [] m_a;
    }

private:

    A m_x;
    A * m_a;
};

// the main function

int main()
{
    {
        // local innermost scope --> starts

        cout << " --> 1" << endl;

        B b;

        cout << " --> 2" << endl;

        // creation of object b of type B results in the following steps
        // 1 --> call of constructor A with n = 0, so m_i = 0, so --> 0
        // 2 --> then ++m_i, so m_i == 1
        // 3 --> then calls m_x(++m_i), so m_i == 2 and constructor A is executed
        //       so --> 2
        // 4 --> then m_a(new A[2]) creates 2 objects of type A, m_i = 0
        // 5 --> finally B constructors last part is executed where m_i = 2

    } // local innermost scope --> ends

    // at the exit of the local innermost scope
    // four objects are being destroyed
    // --> b
    // --> m_x
    // --> m_a[0]
    // --> m_a[1]
    // --> so four times the ~A destructor is called
    // --> and one time the ~B destructor is called

    cout << " --> 3 --> exit" << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    cout << " --> 4 --> exit" << endl;

    return 0;
}

//======//
// FINI //
//======//

