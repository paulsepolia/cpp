
//===============//
// test --> 0013 //
//===============//

// Status --> FINISHED

//==============================================================================
//
// --> the main points here are:
//
// --> 1 --> usage of default templates
//
//	template<typename T, T t = T()>
//	void f (T a) {...}
//
//==============================================================================

#include <iostream>
#include <iomanip>

using std::endl;
using std::cout;
using std::cin;
using std::boolalpha;

// function --> f1

template<typename T, T t = T()>
void f1(T a1)
{
    cout << " --> f1 --> 1" << endl;

    cout << " --> f1 --> (t > T()) --> " << (t > T()) << endl;

    cout << " --> f1 --> 2" << endl;

    cout << " --> f1 --> (t < T()) --> " << (t < T()) << endl;

    cout << " --> f1 --> 3" << endl;

    cout << " --> f1 --> (t == T()) --> " << (t == T()) << endl;

    cout << " --> f1 --> 4" << endl;

    cout << " --> f1 --> a1 = " << a1 << endl;
}

// function --> f2

template<typename T, T t = T()>
void f2(T a1, T a2)
{
    cout << " --> f2 --> 1" << endl;

    cout << " --> f2 --> (t > T()) --> " << (t > T()) << endl;

    cout << " --> f2 --> 2" << endl;

    cout << " --> f2 --> (t < T()) --> " << (t < T()) << endl;

    cout << " --> f2 --> 3" << endl;

    cout << " --> f2 --> (t == T()) --> " << (t == T()) << endl;

    cout << " --> f2 --> 4" << endl;

    cout << " --> f2 --> a1 = " << a1 << endl;

    cout << " --> f2 --> 5" << endl;

    cout << " --> f2 --> a2 = " << a2 << endl;

    cout << " --> f2 --> 6" << endl;

    cout << " --> f2 --> T() = " << T() << endl;

    cout << " --> f2 --> 7" << endl;

    cout << " --> f2 --> int() = " << int() << endl;

    cout << " --> f2 --> 8" << endl;

    cout << " --> f2 --> double() = " << double() << endl;
}

// the main function

int main()
{
    // adjust the output format

    cout << boolalpha;

    // call the function

    cout << "---------------------------------->> 1" << endl;

    f1<int, 9>(11);

    cout << "---------------------------------->> 2" << endl;

    f1<int, -9>(12);

    cout << "---------------------------------->> 3" << endl;

    f2<int, 10>(13, 14);

    cout << "---------------------------------->> 4" << endl;

    f2<int, -10>(15, 16);

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

