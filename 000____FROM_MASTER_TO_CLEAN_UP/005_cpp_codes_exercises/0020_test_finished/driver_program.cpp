
//===============//
// test --> 0020 //
//===============//

// status --> FINISHED

//==============================================================================
//
// --> the main points here are the following:
//
// --> 1 --> use of polymorphism --> const BASE *pb = &DERIVED;
//
//   	     the above pointer calls base and derived functions
//	     if the base if virtual calls the derived equivalent
//
// --> 2 --> const BASE *pb = & BASE;
// 	     calls ONLY BASE function (of course)
//
//==============================================================================

#include <iostream>

using std::cout;
using std::cin;
using std::endl;

// class --> B

class B {
public:

    // constructor

    B()
    {
        cout << " --> constructor --> B" << endl;
    }

    // destructor

    virtual ~B()
    {
        cout << " --> destructor --> ~B" << endl;
    }

    // function

    virtual int shift(int n = 2) const
    {
        cout << " --> function --> shift --> B" << endl;

        return n << 2;
    }
};

// class --> D

class D : public B {
public:

    // constructor

    D()
    {
        cout << " --> constructor --> D" << endl;
    }

    // destructor

    virtual ~D()
    {
        cout << " --> destructor --> ~D" << endl;
    }

    // function

    int shift(int n = 3) const
    {
        cout << " --> function --> shift --> D" << endl;

        return n << 3;
    }
};

// the main function

int main()
{
    {
        // local innermost scope --> starts

        cout << " --> 1" << endl;

        const D d; // creates the object d of the type derived class D
        // 1 --> calls the constructor B
        // 2 --> calls the constructor D

        cout << " --> 2" << endl;

        const B * b = & d; // DOES NOT CALL ANYTHING
        // b is a pointer of type class b,
        // but initialized a reference of type class D
        // any virtual base class is not called by b
        // but instead is called the derived version of it

        cout << " --> 3" << endl;

        cout << " --> b->shift() = " << b->shift() << endl; // calls function of D
        // since the base 'shift'
        // function is virtual

        cout << " --> 4" << endl;

        B * pb1; // DOES NOT CALL ANYTHING

        cout << " --> 5" << endl;

        B b1; // calls the constructor B

        cout << " --> 6" << endl;

        pb1 = & b1; // DOES NOT CALL ANYTHING

        cout << " --> 7" << endl;

        cout << " --> b1.shift() = " << b1.shift() << endl; // function of B

        cout << " --> 8" << endl;

        cout << " --> pb1->shift() = " << pb1->shift() << endl; // function of B

        cout << " --> 9" << endl;

        D d1; // constructor B and D

        cout << " --> 10" << endl;

        pb1 = & d1; // DOES NOT CALL ANYTHING

        cout << " --> 11" << endl;

        cout << " --> pb1->shift() = " << pb1->shift() << endl; // function of D

        cout << " --> 11 --> end" << endl;

    } // local innermost scope --> ends

    // at the exit of the local innermost scope
    // three objects are being destroyed, b and b1, d1
    // and the related destructors are being called

    cout << " --> 12 --> exit" << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    cout << " --> 13 --> exit" << endl;

    return 0;
}

//======//
// FINI //
//======//

