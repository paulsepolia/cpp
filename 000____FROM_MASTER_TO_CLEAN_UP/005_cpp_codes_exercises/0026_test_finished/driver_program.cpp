
//===============//
// test --> 0026 //
//===============//

// Status --> FINISHED

#include <algorithm> // move, count_if
#include <iostream>
#include <memory>    // unique_ptr

using std::cin;
using std::cout;
using std::endl;
using std::unique_ptr;
using std::move;

// definition --> P

typedef unique_ptr<int> P;

// class --> is_null

class is_null {
public:

    // constructor

    is_null()
    {
        cout << " --> constructor --> is_null" << endl;
    }

    // destructor

    ~is_null()
    {
        cout << " --> destructor --> ~is_null" << endl;
    }

    // operator()

    bool operator()(const P &p) const
    {
        return !bool(p);
    }
};

// the main function

int main()
{
    {
        // local innermost scope --> starts

        cout << " --> 1" << endl;

        P p[] = { P(new int(1)), P(new int(0)), P(nullptr), P(new int(2)), P() };

        cout << " --> 2" << endl;

        auto b = begin(p);

        cout << " --> 3" << endl;

        auto e = end(p);

        cout << " --> 4" << endl;

        cout << count_if(b, e, is_null()) << endl; // there are 2 null pointers
        // P(nullptr) and P()
        // result is 2

        cout << " --> 5" << endl;

        P x = move(p[0]); // P[0] becomes a NULL pointer

        cout << " --> 6" << endl;

        P & y = p[1];

        cout << " --> 7" << endl;

        static_cast<void>(y);

        cout << " --> 8" << endl;

        p[2] = move(p[3]); // p[2] is not anymore NULL pointer
        // p[3] becomes NULL pointer

        cout << " --> 9" << endl;

        cout << count_if(b, e, is_null()) << endl; // result is 3
        // p[0], p[3], p[4]

        cout << " --> 10" << endl;

    } // local innermost scope --> ends

    cout << " --> 11 --> exit" << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    cout << " --> 12 --> exit" << endl;

    return 0;
}

//======//
// FINI //
//======//

