
//=====================//
// test --> 0017 --> c //
//=====================//

// Status --> FINISHED

//==============================================================================
//
// --> the main points here are:
//
// --> 1 --> CONVERSION OPERATORS
//
// --> 2 --> operator int () const { return m_i; }
//
//	     The above definition of the conversion operator int ()
//	     inside a class converts an object A to int
//	     so you can write e.g. int i = A;
//
// --> 3 --> operator double () const { return static_cast<double>(m_i); }
//
// 	     The above definition of the conversion operator double ()
//	     inside a class converts and object of that class to double
//	     so you can write e.g. double x = A;
//
// --> 4 --> operator A () const {...}
//
//	     YOU CAN DEFINE SUCH AN OPERATOR BUT NEVER BEING EXECUTED
//	     the job can be done by the copy constructor
//	     which is always (the copy constructor) present
//
//==============================================================================

#include <iostream>
#include <iomanip>

using std::cout;
using std::cin;
using std::endl;
using std::fixed;
using std::setprecision;
using std::showpos;
using std::showpoint;

// class --> A

class A {
public:

    // constructor with zero arguments

    A() : m_i (0)
    {
        cout << " --> constructor --> A --> zero" << endl;
    }

    // constructor with one argument

    A(int i) : m_i(i)
    {
        cout << " --> constructor --> A --> one" << endl;
    }

    // destructor

    ~A()
    {
        cout << " --> destructor --> ~A" << endl;
    }

    // copy constructor

    A(const A & a)
    {
        cout << " --> copy constructor --> A" << endl;

        this->m_i = a.m_i;
    }

    // operator int() overload

    operator int() const
    {
        cout << " --> operator int () const --> opid --> 1" << endl;
        cout << " --> m_i = " << m_i << endl;

        return m_i;
    }

    // operator double() overload

    operator double() const
    {
        cout << " --> operator double () const --> opid --> 2" << endl;
        cout << " --> m_i = " << m_i << endl;

        return static_cast<double>(m_i);
    }

    // operator
    // IT IS USELESS BUT CAN BE DEFINED
    // THE PROGRAM ALWAYS IGNORES IT
    // since the job can be done either by the copy constructor
    // or by the assignment operator which are always present
    // since they are provided always by the compiler if not by the user

    operator A() const
    {
        cout << " --> operator A() const --> opid --> 3" << endl;
        cout << " --> m_i = " << m_i << endl;

        return *this;
    }

    // operator =

    A & operator = (const A & a)
    {
        cout << " --> A & operator = (const A &) --> opid --> 4" << endl;
        cout << " --> m_i = " << m_i << endl;

        this->m_i = a.m_i;

        return * this;
    }

private:

    int m_i;
};

// the main function

int main()
{
    // adjust the output format

    cout << fixed;
    cout << showpos;
    cout << showpoint;
    cout << setprecision(10);

    {
        // local innermost scope --> starts

        cout << " --> 1" << endl;

        A a1;

        cout << " --> 2" << endl;

        int i1 = a1;

        cout << " --> 3" << endl;

        cout << " --> i1 = " << i1 << endl;

        cout << " --> 4" << endl;

        A a2(11);

        cout << " --> 5" << endl;

        int i2 = a2;

        cout << " --> 6" << endl;

        cout << " --> i2 = " << i2 << endl;

        cout << " --> 6" << endl;

        double d1 = a2;

        cout << " --> 7" << endl;

        cout << " --> d1 = " << d1 << endl;

        cout << " --> 8" << endl;

        A a3 = a1; // DOES NOT CALL THE DEFINED CONVERSION OPERATOR
        // ONLY THE COPY CONSTRUCTOR

        cout << " --> 9" << endl;

        a3 = a1; // DOES NOT CALL THE DEFINDED CONVERSION OPERATOR
        // BUT INSTEAD THE ASSIGNMENT OPERATOR
        // which is provided in any case either by the compiler
        // or by the developer

        cout << " --> 10 --> end" << endl;

    } // local innermost scope --> ends

    cout << " --> 11 --> exit" << endl;

    // at the exit of the innermost local scope
    // the three local objects a1, a2, a3 are being destroyed
    // by calling the destructor ~A

    // sentineling

    int sentinel;
    cin >> sentinel;

    cout << " --> 12 --> exit" << endl;

    return 0;
}

//======//
// FINI //
//======//

