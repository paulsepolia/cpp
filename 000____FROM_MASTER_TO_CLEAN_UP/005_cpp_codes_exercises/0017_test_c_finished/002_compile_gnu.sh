#!/bin/bash

  # 1. compile

  g++-4.8 -O3                \
          -Wall              \
          -std=c++0x         \
          -static            \
          driver_program.cpp \
          -o x_gnu
