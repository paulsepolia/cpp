#!/bin/bash

  g++-5 	-O3                \
      	-Wall              \
      	-std=c++14         \
		-ftemplate-depth=100000 \
      	driver_program.cpp \
          -o x_gnu_521
