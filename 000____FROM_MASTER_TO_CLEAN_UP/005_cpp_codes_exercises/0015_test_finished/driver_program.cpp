
//===============//
// test --> 0015 //
//===============//

// Status --> FINISHED

//==============================================================================
//
// --> the main points are:
//
// --> 1 --> LL list<int>
//       --> You can use the "remove_if" generic algorithm
//	     remove_if(LL.begin(), LL.end(), P());
//	     does not modify the size and rest properties
//	     but removes AND shifts left the elements, the last elements
//           remain the same
//
// --> 2 --> LL list<int>
// 	 --> The "remove_if" member function works exaclty as expected
//	     LL.remove_if(P()), removes, shifts left and resizes
//	     so DOES modify the properties of the container
//
// --> 3 --> So, if an algorithm exits as generic and as member function
//	     to a container is MUCH MUCH BETTER TO USE THE MEMBER FUNCTION version
//	     since it does exaclty what it has to be done,
//	     modifies values and properties of the container
//	     The equivalent generic algorithm DOES NOT modify the properties
//	     of the container ONLY THE VALUES
//
//==============================================================================

#include <algorithm>
#include <iostream>
#include <list>

using std::list;
using std::remove_if;
using std::cout;
using std::cin;
using std::endl;

// class --> P

class P {
public:

    // constructor

    P()
    {
        cout << " --> constructor --> P" << endl;
    }

    // destructor

    ~P()
    {
        cout << " --> destructor --> ~P" << endl;
    }

    // operator()

    bool operator() (const int &n) const
    {
        cout << " --> bool operator() (const int &) const" << endl;

        return n % 3 == 0;
    }

};

// the main function

int main()
{

    {
        // local innermost scope --> starts

        cout << " --> 1" << endl;

        list<int>::iterator it; // does not create anything

        cout << " --> 2" << endl;

        list<int> l({ 5, 2, 6, 1, 13, 9, 19 }); // creates a list of 7 integers

        cout << " --> 3" << endl;

        for (it = l.begin(); it != l.end(); it++) { // prints the ints of the list
            cout << *it << endl;
        }

        cout << " --> 4" << endl;

        cout << " --> l.size() = " << l.size() << endl; // the size is 7

        cout << " --> 5" << endl;

        remove_if(l.begin(), l.end(), P()); // general remove_if DOES NOT
        // modify the properties of the container
        // does remove the elements,
        // the last elements are remian the original

        // so --> { 5, 2, 6,  1, 13, 9, 19} --> { 5, 2, _,  1, 13, _, 19 } -->
        // so --> { 5, 2, 1, 13, 19, _,  _} --> { 5, 2, 1, 13, 19, 9, 19 }

        cout << " --> 6" << endl;

        cout << " --> l.size() = " << l.size() << endl; // the size remains the same, 7
        // since the remove_if algorithm
        // DOES NOT modify the properties
        // of the container

        cout << " --> 7" << endl;

        for (it = l.begin(); it != l.end(); it++) {
            cout << *it << endl;
        }

        cout << " --> 8" << endl;

        l.remove_if(P()); // member functions remove_if()
        // DOES MODIFY the properties
        // of the container and in any case
        // DOES the job that should do

        // so --> { 5, 2, 1, 13, 19, 9, 19 } --> { 5, 2, 1, 13, 19, _, 19} -->
        // so --> { 5, 2, 1, 13, 19, 19} --> SIZE = 6

        cout << " --> 9" << endl;

        cout << " --> l.size() = " << l.size() << endl; // it is 6

        cout << " --> 10" << endl;

        for (it = l.begin(); it != l.end(); it++) {
            cout << *it << endl;
        }

        cout << " --> 11 --> end" << endl;

    } // local innermost scope --> ends

    cout << " --> 12 --> exit" << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    cout << " --> 13 --> exit" << endl;

    return 0;
}

//======//
// FINI //
//======//

