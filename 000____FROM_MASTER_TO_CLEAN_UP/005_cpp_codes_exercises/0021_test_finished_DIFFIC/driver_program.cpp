
//===============//
// test --> 0021 //
//===============//

// Status --> FINISHED

//==============================================================================
//
// --> the main points here are:
//
// --> 1 -->
//
//
// --> 2 -->
//
//==============================================================================

#include <iostream>

using std::cout;
using std::cin;
using std::endl;

// class --> A

class A {
public:

    // constructor

    A(int n = 2) : m_n(n)
    {
        cout << " --> constructor --> A" << endl;
    }

    // destructor

    ~A()
    {
        cout << " --> destructor --> ~A" << endl;
    }

    // member function

    int get_n() const
    {
        cout << " --> function --> get_n --> A" << endl;

        return m_n;
    }

    // member function

    void set_n(int n)
    {
        cout << " --> function --> set_n --> A" << endl;

        m_n = n;
    }

private:

    int m_n;
};

// class --> B

class B {
public:

    // constructor

    B(char c = 'a') : m_c(c)
    {
        cout << " --> constructor --> B" << endl;
    }

    // destructor

    ~B()
    {
        cout << " --> destructor --> ~B" << endl;
    }

    // member function

    char get_c() const
    {
        cout << " --> function --> get_c --> B" << endl;

        return m_c;
    }

    // member function

    void set_c(char c)
    {
        cout << " --> function --> set_c --> B" << endl;

        m_c = c;
    }

private:

    char m_c;
};

// class --> C

class C : virtual  public A, public B { // any common member variables collapses to one instance
    // ONLY MEMBER VARIABLES - NOT FUNCTIONS
public:				      // THE FUNCTIONS MUST BE UNIQUE TO EACH CLASS
    // constructor

    C(): A(), B()
    {
        cout << " --> constructor --> C" << endl;
    }

    // destructor

    ~C()
    {
        cout << " --> destructor --> ~C" << endl;
    }
};

// class --> D

class D :  virtual public A, public B { // any common member variables collapses to one instance
    // ONLY MEMBER VARIABLES - NOT FUNCTIONS
public:				      // THE FUNCTION MUST BE UNIQUE TO EACH CLASS
    // constructor

    D() : A(), B()
    {
        cout << " --> constructor --> D" << endl;
    }

    // destructor

    ~D()
    {
        cout << " --> destructor --> ~D" << endl;
    }

};

// class --> E

class E : public C , public D {
public:
    // constructor

    E() : C(), D()
    {
        cout << " --> constructor --> E" << endl;
    }

    // destructor

    ~E()
    {
        cout << " --> destructor --> ~E" << endl;
    }


};

// the main function

int main()
{
    {
        // local innermost scope --> starts

        cout << " --> 1" << endl;

        E e; // creates the object e of type class E
        // class E is derived from C and D
        // C is derived from A and B
        // D is derived from A and B
        // so E --> C --> A and B
        //	and then --> E --> D --> A and B

        cout << " --> 2" << endl;

        C & c = e; // DOES NOT CALL ANYTHING

        cout << " --> 3" << endl;

        D & d = e; // DOES NOT CALL ANYTHING

        cout << " --> 4" << endl;

        cout << c.get_c() << endl; // that of class B --> a

        cout << " --> 5" << endl;

        cout << d.get_n() << endl; // that of class A --> 2

        cout << " --> 6" << endl;

        c.set_n(3); // that of class A

        cout << " --> 7" << endl;

        cout << c.get_n() << endl; // it is 3

        cout << " --> 8" << endl;

        d.set_c('b'); // that of class B

        cout << " --> 9" << endl;

        cout << d.get_c() << endl; // it is b

        cout << " --> 10" << endl;

        cout << c.get_c() << endl; // that of class B --> a

        cout << " --> 11" << endl;

        cout << d.get_n() << endl;

        cout << " --> 12 --> end" << endl;

    } // local innermost scope --> ends

    cout << " --> 13 --> exit" << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    cout << " --> 14 --> exit" << endl;

    return 0;
}

//======//
// FINI //
//======//

