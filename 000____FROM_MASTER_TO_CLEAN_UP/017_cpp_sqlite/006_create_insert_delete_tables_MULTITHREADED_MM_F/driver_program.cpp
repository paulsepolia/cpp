#include <iostream>
#include <string>
#include <vector>
#include <chrono>
#include <thread>
#include <sqlite3.h>

using namespace std::chrono;

void create_write_delete_sql_function(const std::string &thread_flag, int32_t rc, sqlite3 *db) {

    // local parameters

    const uint32_t I_MAX(100);
    const uint32_t DIV(10);

    // local variables

    const std::string flag(thread_flag);
    const char *sql(0);
    sqlite3_stmt *statement(0);
    std::string sql_str{};
    auto t1(system_clock::now());
    auto t2(system_clock::now());
    duration<double> time_span{};

    std::cout << "---------------------------------->> thread_flag = " << thread_flag << std::endl;

    // open database

    if (rc) {
        std::cout << "Can't open database: " << sqlite3_errmsg(db) << std::endl;
        return;
    } else {
        std::cout << "Opened database successfully" << std::endl;
    }

    //==========//
    // put data //
    //==========//

    t1 = system_clock::now();

    for (uint32_t i(1); i <= I_MAX; i++) {

        // time monitoring

        if (i % DIV == 0) {

            std::cout << "----------------------------------->> i = " << i << std::endl;
            t2 = system_clock::now();
            time_span = duration_cast<duration<double>>(t2 - t1);
            std::cout << " --> time used = " << time_span.count() << std::endl;
            t1 = t2;
        }

        //==============//
        // CREATE TABLE //
        //==============//

        // create sql statement

        sql_str = std::string(" CREATE TABLE COMPANY") +
                  std::string(flag) +
                  std::string("S") +
                  std::to_string(i) +
                  std::string(" (ID INT PRIMARY KEY     NOT NULL,") +
                  std::string(" NAME           TEXT    NOT NULL,") +
                  std::string(" AGE            INT     NOT NULL,") +
                  std::string(" ADDRESS        CHAR(50),") +
                  std::string(" SALARY         REAL)");

        sql = sql_str.c_str();

        // prepare sql statement

        rc = sqlite3_prepare_v2(db, sql, -1, &statement, NULL);

        if (rc != SQLITE_OK) {
            std::cout << ("SQL error: " + std::string(thread_flag)) << " : " << sqlite3_errmsg(db) << std::endl;
        }

        // execute sql statement

        rc = sqlite3_step(statement);

        //==========================//
        // INSERT DATA TO THE TABLE //
        //==========================//

        // create sql statement

        sql_str = std::string(" INSERT INTO COMPANY") +
                  std::string(flag) +
                  std::string("S") +
                  std::to_string(i) +
                  std::string(" (ID,NAME,AGE,ADDRESS,SALARY) ") +
                  std::string(" VALUES (1, 'Paul', 32, 'California', 20000.00 ); ") +
                  std::string(" INSERT INTO COMPANY") +
                  std::string(flag) +
                  std::to_string(i) +
                  std::string(" (ID,NAME,AGE,ADDRESS,SALARY) ") +
                  std::string(" VALUES (2, 'Allen', 25, 'Texas', 15000.00 ); ") +
                  std::string(" INSERT INTO COMPANY") +
                  std::string(flag) +
                  std::to_string(i) +
                  std::string(" (ID,NAME,AGE,ADDRESS,SALARY) ") +
                  std::string(" VALUES (3, 'Teddy', 23, 'Norway', 20000.00 );") +
                  std::string(" INSERT INTO COMPANY") +
                  std::string(flag) +
                  std::to_string(i) +
                  std::string(" (ID,NAME,AGE,ADDRESS,SALARY) ") +
                  std::string(" VALUES (4, 'Mark', 25, 'RichMond', 65000.00 );");

        sql = sql_str.c_str();

        // prepare sql statement

        sqlite3_stmt *statement(0);

        rc = sqlite3_prepare_v2(db, sql, -1, &statement, NULL);

        if (rc != SQLITE_OK) {
            std::cout << ("SQL error: " + std::string(thread_flag)) << " : " << sqlite3_errmsg(db) << std::endl;
        }

        // execute sql statement

        rc = sqlite3_step(statement);
    }

    //=============//
    // delete data //
    //=============//

    t1 = system_clock::now();
    t2 = system_clock::now();

    for (uint32_t i(1); i <= I_MAX; i++) {

        // timing

        if (i % DIV == 0) {

            std::cout << "----------------------------------->> i = " << i << std::endl;
            t2 = system_clock::now();
            time_span = duration_cast<duration<double>>(t2 - t1);
            std::cout << " --> time used = " << time_span.count() << std::endl;
            t1 = t2;
        }

        //==============//
        // DELETE TABLE //
        //==============//

        // create sql statement

        sql_str = std::string(" DROP TABLE COMPANY") +
                  std::string(flag) +
                  std::string("S") +
                  std::to_string(i) +
                  std::string(";");

        sql = sql_str.c_str();

        // prepare sql statement

        rc = sqlite3_prepare_v2(db, sql, -1, &statement, NULL);

        if (rc != SQLITE_OK) {
            std::cout << ("SQL error: " + std::string(thread_flag)) << " : " << sqlite3_errmsg(db) << std::endl;
        }

        // execute sql statement

        rc = sqlite3_step(statement);
    }

    sqlite3_close(db);
}

// the main function

int main() {

    // local parameters and variables

    const std::string database_path_name("test.db");
    const uint32_t NUM_THREADS(100);
    sqlite3 *db(0);

    // open the database

    int32_t rc(sqlite3_open(database_path_name.c_str(), &db));

    // launch several threads

    std::vector<std::thread> vec_threads{};

    for (uint32_t i(1); i <= NUM_THREADS; i++) {

        std::string flag(std::string("thread") + std::to_string(i));
        std::thread thread_loc(create_write_delete_sql_function, flag, rc, db);
        vec_threads.push_back(std::move(thread_loc));
    }

    // join the threads

    uint32_t sentinel(0);
    std::cout << "-->> Input an integer to continue : " << std::endl;
    std::cin >> sentinel;

    for (uint32_t i(1); i <= NUM_THREADS; i++) {

        vec_threads[i].join();
    }

    return 0;
}