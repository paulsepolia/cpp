// packagedTask.cpp

#include <utility>
#include <future>
#include <iostream>
#include <thread>
#include <deque>
#include <cmath>

static const auto DO_MAX = static_cast<uint64_t>(std::pow(10.0, 3.0));

class SumUp {
public:
    long double operator()(long double beg, long double end) {

        long double sum{0};

        for(uint64_t k = 0; k < DO_MAX; k++) {
            for (auto i = static_cast<uint64_t>(beg); i < end; ++i) {
                sum += static_cast<long double>(i);
            }
        }

        return sum;
    }
};

int main() {

    std::cout << std::endl;

    SumUp sumUp1;
    SumUp sumUp2;
    SumUp sumUp3;
    SumUp sumUp4;

    // define the tasks
    std::packaged_task<long double(long double, long double)> sumTask1(sumUp1);
    std::packaged_task<long double(long double, long double)> sumTask2(sumUp2);
    std::packaged_task<long double(long double, long double)> sumTask3(sumUp3);
    std::packaged_task<long double(long double, long double)> sumTask4(sumUp4);

    // get the futures
    std::future<long double> sumResult1 = sumTask1.get_future();
    std::future<long double> sumResult2 = sumTask2.get_future();
    std::future<long double> sumResult3 = sumTask3.get_future();
    std::future<long double> sumResult4 = sumTask4.get_future();

    // push the tasks on the container
    std::deque<std::packaged_task<long double(long double, long double)> > allTasks;

    allTasks.push_back(std::move(sumTask1));
    allTasks.push_back(std::move(sumTask2));
    allTasks.push_back(std::move(sumTask3));
    allTasks.push_back(std::move(sumTask4));

    long double begin{1};
    long double increment{10000000};
    long double end = begin + increment;

    // execute each task in a separate thread
    while (not allTasks.empty()) {

        std::packaged_task<long double(long double, long double)> myTask = std::move(allTasks.front());
        allTasks.pop_front();
        std::thread sumThread(std::move(myTask), begin, end);
        begin = end;
        end += increment;
        sumThread.detach();
    }

    // get the results

    const auto sum = sumResult1.get() + sumResult2.get() + sumResult3.get() + sumResult4.get();

    std::cout << " --> sum = " << sum << std::endl;

    std::cout << std::endl;

}