// fireAndForgetFutures.cpp

#include <chrono>
#include <future>
#include <iostream>
#include <thread>

int main() {

    {
        std::cout << std::endl;

        // these are NOT a fire and forget futures

        const auto f1 = std::async(std::launch::async, [] {
            std::cout << " --> waiting for the first future to finish ..." << std::endl;
            std::this_thread::sleep_for(std::chrono::seconds(10));
            std::cout << "first thread" << std::endl;
        });

        const auto f2 = std::async(std::launch::async, [] {
            std::cout << " --> waiting for the second future to finish ..." << std::endl;
            std::this_thread::sleep_for(std::chrono::seconds(1));
            std::cout << "second thread" << std::endl;
        });

        std::cout << "main thread" << std::endl;
    }

    {
        std::cout << std::endl;

        // these are a fire and forget futures (not given name)

        std::async(std::launch::async, [] {
            std::cout << " --> waiting for the first future to finish ..." << std::endl;
            std::this_thread::sleep_for(std::chrono::seconds(10));
            std::cout << "first thread" << std::endl;
        });

        std::async(std::launch::async, [] {
            std::cout << " --> waiting for the second future to finish ..." << std::endl;
            std::this_thread::sleep_for(std::chrono::seconds(1));
            std::cout << "second thread" << std::endl;
        });

        std::cout << "main thread" << std::endl;
    }

    std::cout << std::endl;
}