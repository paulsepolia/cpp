#include <iostream>
#include <cstdint>

template<typename T>
class Base {
public:

    Base() {
        std::cout << " --> base --> constructor" << std::endl;
    }

    void print_function() {
        static_cast<T *>(this)->implementation();
    }

    virtual ~Base() {
        std::cout << " --> base --> destructor" << std::endl;
    };
};

class Derived1 : public Base<Derived1> {
public:

    Derived1() : Base<Derived1>() {
        std::cout << " --> derived1 --> constructor" << std::endl;
    }

    ~Derived1() override {
        std::cout << " --> derived1 --> destructor" << std::endl;
    }

    void implementation() {
        std::cout << " --> hello from --> Derived1" << std::endl;
    }
};

class Derived2 : public Derived1 {
public:

    Derived2() : Derived1() {
        std::cout << " --> derived2 --> constructor" << std::endl;
    }

    ~Derived2() final {
        std::cout << " --> derived2 --> destructor" << std::endl;
    }

    void implementation() {
        std::cout << " --> hello from --> Derived2" << std::endl;
    }
};

int main() {

    {
        std::cout << " --> example --> 1 -------------------> start" << std::endl;

        Base<Derived1> b;
        b.print_function();

        std::cout << " --> example --> 1 -------------------> end" << std::endl;
    }

    {
        std::cout << " --> example --> 2 -------------------> start" << std::endl;

        Base<Derived1> *pb = new Derived1;
        pb->print_function();

        delete pb;

        std::cout << " --> example --> 2 -------------------> end" << std::endl;
    }

    {
        std::cout << " --> example --> 3 -------------------> start" << std::endl;

        // STATIC POLYMORPHISM STOPS HERE

        Base<Derived1> *pb = new Derived2;
        pb->print_function();

        delete pb;

        std::cout << " --> example --> 3 -------------------> end" << std::endl;
    }
}