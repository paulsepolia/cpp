#!/bin/bash

# compile

nasm -f elf64 driver_program.asm

# link

ld -m elf_x86_64 -s -o x_64bit *.o

# clean

rm *.o
