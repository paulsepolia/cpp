#!/bin/bash

# compile

yasm -f elf64 -g dwarf2 -l driver_program.lst driver_program.asm

# link

ld -m elf_x86_64 -s -o x_yasm_64bit *.o

# clean

rm *.o
