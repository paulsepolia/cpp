#!/bin/bash

# compile

nasm -f elf32 driver_program.asm

# link

ld -m elf_i386 -s -o x_32bit *.o

# clean

rm *.o
