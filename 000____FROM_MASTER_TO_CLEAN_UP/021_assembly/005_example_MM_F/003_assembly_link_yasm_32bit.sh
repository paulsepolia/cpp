#!/bin/bash

# compile

yasm -f elf32 -g dwarf2 -l driver_program.lst driver_program.asm

# link

ld -m elf_i386 -s -o x_yasm_32bit *.o

# clean

rm *.o
