#!/bin/bash

  # 1. compile

  g++        -Wall              \
             -O3                \
             -std=gnu++14       \
             driver_program.cpp \
             -o x_gnu
