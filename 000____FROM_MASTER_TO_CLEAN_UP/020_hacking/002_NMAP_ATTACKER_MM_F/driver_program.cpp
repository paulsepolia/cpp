#include <iostream>
#include <cstdlib>
#include <string>
#include <vector>
#include <algorithm>
#include <fstream>
#include <chrono>
#include <thread>

// get the number of nmap processes running on Linux

uint32_t read_num_nmap(const std::string &file_name) {

    std::string line;

    while (true) {

        std::ifstream my_file;
        my_file.open(file_name, std::ios::in);

        if (my_file.good()) {

            getline(my_file, line);
            my_file.close();
            if(line.size() != 0) break;

        } else {
            std::cout << "Unable to open file" << std::endl;
        }

        std::this_thread::sleep_for(std::chrono::seconds(1));
    }

    return std::stoul(line);
}

// read the black-listed ips

std::vector<std::string> read_ips(const std::string &file_name) {

    std::vector<std::string> ips;

    while (true) {

        std::ifstream my_file;
        my_file.open(file_name, std::ios::in);

        if (my_file.good()) {

            std::string line;

            while (getline(my_file, line)) {
                ips.push_back(line);
            }

            my_file.close();
            break;

        } else {
            std::cout << "Unable to open file" << std::endl;
        }

        std::this_thread::sleep_for(std::chrono::seconds(1));
    }

    return ips;
}

int main() {

    // local parameters
    const std::string INDEX("index");
    const std::string IP_HERE("IP_HERE");
    const uint32_t NMAP_MAX(2);
    const std::string NMAP_FILE_NUM("NMAP_NUM");
    const uint32_t SLEEP_NMAP_SECS(10);
    const uint32_t SLEEP_DIST_SECS(1);
    const std::string cmd_nmap_number("ps -A --no-headers | grep nmap | wc -l > NMAP_NUM &");
    const std::string cmd_main("nmap -sS -sU -T4 -A -v IP_HERE > OUT_index &");
    const std::string BLACK_IPS("BLACK_LISTED_IPs.txt");

    // black-listed ips
    std::vector<std::string> ips(read_ips(BLACK_IPS));

    // remove duplicates
    auto it(std::unique(ips.begin(), ips.end()));
    ips.resize(std::distance(ips.begin(), it));

    // shuffle
    std::random_shuffle(ips.begin(), ips.end());

    uint32_t i(0);
    uint32_t nums(0);

    while (true) {

        for (const auto &ip: ips) {

            std::cout << "---->> nmap processes started = " << ++i << std::endl;

            // main command
            std::string cmd_main_local(cmd_main);

            // put the index
            uint64_t pos_found(cmd_main_local.find(INDEX));
            cmd_main_local.replace(pos_found, std::string(INDEX).length(), std::to_string(i));

            // put the ip
            pos_found = cmd_main_local.find(IP_HERE);
            cmd_main_local.replace(pos_found, std::string(IP_HERE).length(), ip);

            // nmap scan here
            int res1(system(cmd_main_local.c_str()));

            if (res1 != 0) {
                std::cout << "--> error while executing the command: ";
                std::cout << cmd_main_local << std::endl;
                exit(-1);
            }

            // count how many nmap commands are running
            // if more than a max value wait

            int32_t res2(system(cmd_nmap_number.c_str()));

            if (res2 != 0) {
                std::cout << "--> error while executing the command: ";
                std::cout << cmd_nmap_number << std::endl;
                exit(-2);
            }

            std::this_thread::sleep_for(std::chrono::seconds(SLEEP_DIST_SECS));

            nums = read_num_nmap(NMAP_FILE_NUM);

            while (nums >= NMAP_MAX) {

                int32_t res3(system(cmd_nmap_number.c_str()));

                std::cout << "--> sleeping for " << SLEEP_NMAP_SECS << " seconds" << std::endl;
                std::this_thread::sleep_for(std::chrono::seconds(SLEEP_NMAP_SECS));

                if (res3 != 0) {
                    std::cout << "--> error while executing the command: ";
                    std::cout << cmd_nmap_number << std::endl;
                    exit(-3);
                }

                nums = read_num_nmap(NMAP_FILE_NUM);
                std::cout << "---->> nmap processes running = " << nums << std::endl;
            }
        }
    }
}
