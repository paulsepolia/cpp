#include <iostream>
#include <vector>
#include <any>
#include <cmath>
#include <chrono>

const auto DIM_MAX = static_cast<uint64_t>(std::pow(10.0, 8.0));
const auto DO_MAX = static_cast<uint64_t>(std::pow(10.0, 1.0));

int main() {

    {
        const auto t1 = std::chrono::steady_clock::now();

        double sum_d{0.0};

        for (uint64_t k = 0; k < DO_MAX; k++) {

            std::vector<std::any> va;

            for (uint64_t i = 0; i < DIM_MAX; i++) {
                va.emplace_back(static_cast<double>(i));
            }

            for (const auto &el: va) {
                sum_d += std::any_cast<double>(el);
            }
        }

        std::cout << sum_d << std::endl;
        const auto t2 = std::chrono::steady_clock::now();
        const auto td = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count();
        std::cout << " --> time used = " << td << std::endl;

    }

    {
        const auto t1 = std::chrono::steady_clock::now();
        double sum_d{0.0};

        for (uint64_t k = 0; k < DO_MAX; k++) {

            std::vector<double> va;

            for (uint64_t i = 0; i < DIM_MAX; i++) {
                va.emplace_back(static_cast<double>(i));
            }

            for (const auto &el: va) {
                sum_d += el;
            }
        }

        std::cout << sum_d << std::endl;
        const auto t2 = std::chrono::steady_clock::now();
        const auto td = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count();
        std::cout << " --> time used = " << td << std::endl;
    }
}
