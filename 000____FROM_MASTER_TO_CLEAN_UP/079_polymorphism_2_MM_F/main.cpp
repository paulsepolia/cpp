#include <iostream>

// erroneous example

class A1 {
public:
    virtual auto f1() const -> void {
        std::cout << "A1" << std::endl;
    }
};

class A2 : public A1 {
public:
    auto f1() const -> void final {
        std::cout << "A2" << std::endl;
    }
};

auto main() -> int {

    {
        std::cout << "--->> 1" << std::endl;
        A1 *a{new A1()};
        a->f1();
        static_cast<A2 *>(a)->f1();
        dynamic_cast<A2 *>(a)->f1();
    }

    {
        std::cout << "--->> 2" << std::endl;
        A1 *a{new A2()};
        a->f1();
        static_cast<A2 *>(a)->f1();
        dynamic_cast<A2 *>(a)->f1();
    }
}

