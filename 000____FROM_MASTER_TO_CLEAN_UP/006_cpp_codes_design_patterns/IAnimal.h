#ifndef IANIMAL_H
#define IANIMAL_H

class IAnimal {
public:
    virtual int GetNumberOfLegs() const = 0;
    virtual void Speak() = 0;
    virtual void Free() = 0;
}

#endif // IANIMAL_H
