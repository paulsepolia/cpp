//================================//
// Author: Pavlos G. Galiatsatos  //
// Date: 2013/10/12               //
// Functions: pass by value       //
//================================//

#include <iostream>

using namespace std;

// 1. functions declaration

void funcValueParam(int num);

// 2. main function

int main()
{

    int number = 6;

    cout << "Before calling the function, number = " << number << endl;

    funcValueParam(number);

    cout << "After calling the function, number = " << number << endl;

    cout << "Conclusion: The original value is unchanged" << endl;

    return 0;
}

// 3. functions definition

void funcValueParam(int num)
{
    cout << "Inside function: before changing, num = " << num << endl;

    num = 15;

    cout << "Inside function: after changing, num = " << num << endl;
}

//======//
// FINI //
//======//
