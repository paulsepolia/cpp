
//===================//
// Constructs: while //
//===================//

///==============//
// code --> 0012 //
//===============//

// keywords: while, clock_t, clock(), CLOCKS_PER_SEC

#include <iostream>
#include <iomanip>
#include <cmath>
#include <ctime>

using std::cout;
using std::cin;
using std::endl;
using std::pow;
using std::setprecision;
using std::fixed;
using std::showpos;
using std::showpoint;

// the main function

int main()
{
    // local variables and parameters

    cout << " --> 1" << endl;

    const long long int I_MAX  = static_cast<long long>(pow(10.0, 7.0));
    const long long int I_STEP = 1;
    long long int i;
    clock_t t1;
    clock_t t2;

    cout << " --> 2" << endl;

    cout << fixed;
    cout << showpos;
    cout << showpoint;
    cout << setprecision(10);

    // while loop

    cout << " --> 3" << endl;

    cout << " --> time and counter initialization" << endl;

    t1 = clock();
    i = 0;

    while (i <= I_MAX) {
        cout << i << " ->$$<- ";
        i = i + I_STEP;
    }

    t2 = clock();

    cout << endl;

    cout << " --> 4 --> end" << endl;

    cout << " --> the value reached is --> " << i << endl;
    cout << " --> the time used is --> " << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    cout << " --> 5 --> exit" << endl;

    return 0;
}

//======//
// FINI //
//======//

