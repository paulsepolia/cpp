//=======================================//
// Author: Pavlos G. Galiatsatos         //
// Date: 2013/10/30                      //
// Functions: array, vector, deque, list //
// Algorithms: remove, erase             //
//=======================================//

#include <iostream>
#include <algorithm>
#include <iterator>
#include <vector>
#include <list>
#include <deque>

using namespace std;

// global consts

const long int I_MAX = 5 * static_cast<long int>(10000000);
const int THE_ELEM = 1;

// hand made condition

template <class T>
bool cond(T x)
{
    return (x < THE_ELEM);
}

// main

int main ()
{
    // variables declaration

    int i;
    vector<int> aVector;
    list<int> aList;
    deque<int> aDeque;
    int j;
    const int J_MAX = 100;
    vector<int>::iterator new_end_itV;
    list<int>::iterator new_end_itL;
    deque<int>::iterator new_end_itD;

    // build the array

    for (j = 0; j < J_MAX; j++) {
        cout << "------------------------------------------------>> " << j+1 << endl;

        cout << " 1 --> build the array" << endl;

        int *aArray = new int [I_MAX];

        for (i = 0;  i < I_MAX; i++) {
            aArray[i] = THE_ELEM;
        }

        // copy the array to a vector

        cout << " 2 --> build the vector" << endl;

        aVector.resize(I_MAX);

        copy(aArray, aArray+I_MAX, aVector.begin());

        // copy the array to the list

        cout << " 3 --> build the list" << endl;

        aList.resize(I_MAX);

        copy(aArray, aArray+I_MAX, aList.begin());

        // copy the array to the deque

        cout << " 4 --> build the deque" << endl;

        aDeque.resize(I_MAX);

        copy(aList.begin(), aList.end(), aDeque.begin());

        // remove

        cout << " 5 --> remove from the vector all the elements" << endl;

        new_end_itV = remove_if(aVector.begin(), aVector.end(), cond<int>);

        cout << " 6 --> remove from the list all the elements" << endl;

        new_end_itL = remove_if(aList.begin(), aList.end(), cond<int>);

        cout << " 7 --> remove from the deque all the elements" << endl;

        new_end_itD = remove_if(aDeque.begin(), aDeque.end(), cond<int>);

        // free up RAM

        cout << " Free up RAM" << endl;

        aVector.erase(new_end_itV, aVector.end());
        aList.erase(new_end_itL, aList.end());
        aDeque.erase(new_end_itD, aDeque.end());

        aVector.clear();
        aList.clear();
        aDeque.clear();
        delete [] aArray;
    }

    return 0;
}

// end
