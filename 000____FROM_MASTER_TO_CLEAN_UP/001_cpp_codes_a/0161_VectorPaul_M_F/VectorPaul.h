//==================//
// VectorPaul class //
//==================//

#include "IncludeLibs.h"

// a class

template <typename T>
class VectorPaul {
private:

    // 1.

    long dim;
    T * p1;
    static int RandomNumber();

    // 2.

    struct c_unique {
        int current;
        c_unique() : current (0) {} // default constructor
        explicit c_unique(int numStart) : current (numStart) {} // a non-default constructor
        int operator()()
        {
            return ++current;
        }
    } UniqueNumber;

    // 3.

    static int compareFun(const void * a, const void * b);

public:

    // constructors here

    // 1. the one and only one allowed default constructor

    VectorPaul<T>() : dim (0), p1 (NULL), UniqueNumber(10) {}; // default constructor

    // 2. a non-default constructor

    explicit VectorPaul<T>(long dimLoc) : dim (dimLoc), p1 (NULL), UniqueNumber(1000) {};

    // member functions here

    void addVector(const VectorPaul<T>&, const VectorPaul<T>&);
    void allocateVector();
    void deleteVector();
    void fillVector(T&);
    void generateVectorRandom();
    void generateVectorUnique();
    T getVector(long) const;
    long getDim();
    void initVector();
    bool isSortedVector();
    T maxElementVector();
    T minElementVector();
    void randomShuffleVector();
    void reverseVector();
    void setDim(long);
    void setEqualTo(const VectorPaul<T>&);
    void setVector(long, T);
    void sortVector();
    void sortVectorQSort();
    void subtractVector(const VectorPaul<T>&, const VectorPaul<T>&);

    // overloaded operators here

    VectorPaul<T> & operator = (const VectorPaul<T>&);
    VectorPaul<T>   operator + (const VectorPaul<T>&) const;
    VectorPaul<T>   operator - (const VectorPaul<T>&) const; // THIS IS WRONG!
};

//======//
// FINI //
//======//
