//======================//
// VectorPaul Functions //
//======================//

#include "IncludeLibs.h"

//===============================//
// private member functions here //
//===============================//

// private member function

template <typename T>
int VectorPaul<T>::compareFun (const void * a, const void * b)
{
    if ((*(T*)a > *(T*)b)) {
        return  1;
    };
    if ((*(T*)a < *(T*)b)) {
        return -1;
    };

    return 0;
}

// private member function

template <typename T>
int VectorPaul<T>::RandomNumber()
{
    return (rand()%100);
}

//==============================//
// public member functions here //
//==============================//

// public member function

template <typename T>
void VectorPaul<T>::addVector(const VectorPaul<T> & objA, const VectorPaul<T> & objB)
{
    long i;

    // add here

    for (i = 0; i < dim; i++) {
        p1[i] = objA.p1[i] + objB.p1[i];
    }
}

// public member function

template <typename T>
void VectorPaul<T>::allocateVector()
{
    p1 = new T [dim];
}

// public member function

template <typename T>
void VectorPaul<T>::deleteVector()
{
    delete [] p1;
}

// public member function

template <typename T>
void VectorPaul<T>::fillVector(T & elem)
{
    long i;

    for (i = 0; i < dim; i++) {
        p1[i] = elem;
    }
}

// public member function

template <typename T>
void VectorPaul<T>::generateVectorRandom()
{
    return generate(p1, p1+dim, RandomNumber);
}

// public member function

template <typename T>
void VectorPaul<T>::generateVectorUnique()
{
    return generate(p1, p1+dim, UniqueNumber);
}

// public member function

template <typename T>
long VectorPaul<T>::getDim()
{
    return dim;
}

// public member function

template <typename T>
T VectorPaul<T>::getVector(long i) const
{
    return p1[i];
}

// public memebr function

template <typename T>
void VectorPaul<T>::initVector()
{
    T val = static_cast<T>(0);
    this->fillVector(val);
}

// public member function

template <typename T>
bool VectorPaul<T>::isSortedVector()
{
    return is_sorted(p1, p1+dim);
}

// public member function

template <typename T>
T VectorPaul<T>::maxElementVector()
{
    return *max_element(p1, p1+dim);
}

// public member function

template <typename T>
T VectorPaul<T>::minElementVector()
{
    return *min_element(p1, p1+dim);
}

// public member function

template <typename T>
void VectorPaul<T>::randomShuffleVector()
{
    random_shuffle(p1, p1+dim);
}

// public member function

template <typename T>
void VectorPaul<T>::reverseVector()
{
    reverse(p1, p1+dim);
}

// public member function

template <typename T>
void VectorPaul<T>::setDim(long i)
{
    dim = i;
}

// public member function

template <typename T>
void VectorPaul<T>::setEqualTo(const VectorPaul<T> & objA)
{
    long i;

    // set elements

    for (i = 0; i < this->dim ; i++) {
        this->setVector(i, objA.getVector(i));
    }
}

// public member function

template <typename T>
void VectorPaul<T>::setVector(long i, T val)
{
    p1[i] = val;
}

// public member function

template <typename T>
void VectorPaul<T>::sortVector()
{
    sort(p1, p1+dim);
}

// public member function

template <typename T>
void VectorPaul<T>::sortVectorQSort()
{
    qsort(p1, dim, sizeof(T), compareFun);
}

// public member function

template <typename T>
void VectorPaul<T>::subtractVector(const VectorPaul<T> & objA, const VectorPaul<T> & objB)
{
    long i;

    // subtract here

    for (i = 0; i < dim; i++) {
        p1[i] = objA.p1[i] - objB.p1[i];
    }
}

//===========================//
// overloaded operators here //
//===========================//

// operator=()

template <typename T>
VectorPaul<T> & VectorPaul<T>::operator=(const VectorPaul<T> & objA)
{
    if(this == &objA) {
        return *this;
    }

    this->setEqualTo(objA);

    return *this;
}

// operator+()

template <typename T>
VectorPaul<T> VectorPaul<T>::operator+(const VectorPaul<T> &objA) const
{
    long i;

    for (i = 0; i < dim; i++) {
        p1[i] = p1[i] + objA.p1[i];
    }

    return *this;
}

// operator-()  THIS IS WRONG!

template <typename T>
VectorPaul<T> VectorPaul<T>::operator-(const VectorPaul<T> &objA) const
{
    long i;

    for (i = 0; i < dim; i++) {
        p1[i] = p1[i] - objA.p1[i];
    }

    return *this;
}

//======//
// FINI //
//======//
