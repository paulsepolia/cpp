
//=========================================================//
// Handle exceptions by fixing the errors.                 //
// The program continues to prompt the user until a valid  //
// input is entered                                        //
//=========================================================//

#include <iostream>
#include <string>
#include <limits>

using std::endl;
using std::cin;
using std::cout;
using std::string;
using std::numeric_limits;
using std::streamsize;

int main()
{
    int number;
    bool done = false;

    string str = "The input stream is in the fail state.";

    do {
        try {
            cout << "Line 8: Enter an integer: ";
            cin >> number;
            cout << endl;

            if (cin.fail()) {
                throw str;
            }

            done = true;
            cout << "Line 14: Number = " << number << endl;
        } catch (string messageStr) {
            cout << "Line 18: " << messageStr << endl;
            cout << "Line 19: Restoring the input stream." << endl;

            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
        }
    } while(!done);

    return 0;
}

//======//
// FINI //
//======//
