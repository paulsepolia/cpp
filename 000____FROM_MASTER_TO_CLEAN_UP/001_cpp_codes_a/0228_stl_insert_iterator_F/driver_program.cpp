
//=========================//
// insert_iterator example //
//=========================//

#include <iostream>
#include <iterator>
#include <vector>
#include <algorithm>

using std::endl;
using std::cout;
using std::cin;
using std::insert_iterator;
using std::vector;
using std::copy;

// the main function

int main()
{
    vector<int> foo;
    vector<int> bar;
    vector<int>::iterator it;

    // build foo

    for (int i = 1; i <= 5; i++) {
        foo.push_back(i);
    }

    // build bar

    for (int i = 1; i <= 5; i++) {
        bar.push_back(i*10);
    }

    // get the beggining of the foo list

    it = foo.begin();

    // advance the iterator 3+1 positions

    advance(it, 3);
    it++;

    // define insert_iterator type variable

    insert_iterator<vector<int> > insert_it(foo, it);

    // copy bar list to 3rd position and beyond of foo list
    // that is an example use of insertion without to overwrite anything

    copy(bar.begin(), bar.end(), insert_it);

    // some outputs

    cout << " --> foo: ";

    for (it = foo.begin(); it!= foo.end(); it++) {
        cout << " " << *it;
    }

    cout << endl;

    // some outputs

    cout << " --> bar: ";

    for (it = bar.begin(); it != bar.end(); it++) {
        cout << " " << *it;
    }

    cout << endl;

    // create another insert type iterator

    it = bar.begin();

    insert_iterator<vector<int> > insert_it_b(bar, it);

    **insert_it_b = 100;
    **insert_it_b++ = 200;

    // some outputs

    cout << " --> bar: ";

    for (it = bar.begin(); it != bar.end(); it++) {
        cout << " " << *it;
    }

    cout << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

