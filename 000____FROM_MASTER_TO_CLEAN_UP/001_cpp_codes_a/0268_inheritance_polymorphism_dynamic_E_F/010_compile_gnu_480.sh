#!/bin/bash

  # 1. compile

  g++-4.8   -O3                  \
            -Wall                \
            -std=c++0x           \
            -static              \
            base.cpp             \
            derivedA.cpp         \
            driver_program_2.cpp \
            -o x_gnu_480
