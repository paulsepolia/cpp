
//=======================================//
// implementation of the class queue_pgg //
//=======================================//

#include "queue_pgg.h"

// default constructor

template <typename T, typename CONT>
queue_pgg<T, CONT>::queue_pgg() {}

// destructor

template <typename T, typename CONT>
queue_pgg<T, CONT>::~queue_pgg() {}

// assignment operator

template <typename T, typename CONT>
queue_pgg<T, CONT> & queue_pgg<T, CONT>::operator = (const queue_pgg<T, CONT> & s1)
{
    this->elems = s1.elems;

    return *this;
}

// copy constructor

template <typename T, typename CONT>
queue_pgg<T, CONT>::queue_pgg(const queue_pgg<T, CONT> & s1)
{
    *this = s1;
}

// empty --> public member function

template <typename T, typename CONT>
bool queue_pgg<T, CONT>::empty() const
{
    return elems.empty();
}

// size --> public member function

template <typename T, typename CONT>
size_t queue_pgg<T, CONT>::size() const
{
    return elems.size();
}

// front --> public member function

template <typename T, typename CONT>
T & queue_pgg<T, CONT>::front()
{
    return elems.front();
}

// back --> public member function

template <typename T, typename CONT>
T & queue_pgg<T, CONT>::back()
{
    return elems.back();
}

// push --> public member function

template <typename T, typename CONT>
void queue_pgg<T, CONT>::push(const T & arg)
{
    elems.push_back(arg);
}

// pop --> public member function

template <typename T, typename CONT>
void queue_pgg<T, CONT>::pop()
{
    elems.pop_front();
}

// swap --> public member function

template <typename T, typename CONT>
void queue_pgg<T, CONT>::swap(queue_pgg<T, CONT> & s1)
{
    queue_pgg<T, CONT> stmp;

    stmp = s1;
    s1 = *this;
    *this = stmp;
}

// emplace --> public member function

template <typename T, typename CONT>
void queue_pgg<T, CONT>::emplace(const T & arg)
{
    elems.emplace_back(arg);
}

//======//
// FINI //
//======//
