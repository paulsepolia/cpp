//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/10/17              //
// Functions: Class              //
//===============================//

#include <ctime>
#include <cstdlib>
#include "dieHeader.h"

using std::srand;
using std::rand;
using std::time;

// class definition

// 1.

die::die()
{
    num = 1;
    srand(time(0));
}

// 2.

int die::roll()
{
    num = rand() % 6 + 1;

    return num;
}

// 3.

int die::getNum() const
{
    return num;
}

//======//
// FINI //
//======//
