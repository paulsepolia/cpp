#include <iostream>
#include <string>
#include <pthread.h>
#include <iomanip>

using namespace std;

// function declaration

void *aFun(void *);

// main function

int main()
{
    // call the function

    pthread_t f1_thread;
    pthread_t f2_thread;
    pthread_t f3_thread;
    pthread_t f4_thread;

    string s1;
    string s2;
    string s3;
    string s4;

    s1 = "1";
    s2 = "2";
    s3 = "3";
    s4 = "4";

    // set the output format

    cout << setprecision(20);
    cout << showpoint;
    cout << showpos;

    // Create independent threads each of which
    // will execute the function and call the function

    pthread_create(&f1_thread, NULL, aFun, (void*) (&s1));
    pthread_create(&f2_thread, NULL, aFun, (void*) (&s2));
    pthread_create(&f3_thread, NULL, aFun, (void*) (&s3));
    pthread_create(&f4_thread, NULL, aFun, (void*) (&s4));

    // join

    pthread_join(f1_thread, NULL);
    pthread_join(f2_thread, NULL);
    pthread_join(f3_thread, NULL);
    pthread_join(f4_thread, NULL);

    return 0;
}

// function definition

void *aFun(void *argument)
{
    // variables declaration

    string sA;
    double sum;
    const long int I_MAX = 20 * static_cast<long int>(1000000000);
    long i;

    // give value

    sA = *((string *) argument);

    // a message

    cout << " Hello from thread --> " << sA << endl;

    // local waste of CPU time
    // to notice the parallel execution

    sum = 0.0;

    for(i = 0; i < I_MAX; i++) {
        sum = sum + static_cast<double>(i);
    }

    cout << " Sum is --> " << sum << " and thread is " << sA << endl;

    return NULL;
}

// end
