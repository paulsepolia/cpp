
//===================================//
// stl, find, find_if, adjacent_find //
//===================================//

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <cassert>
#include <cmath>
#include <iterator>
#include <cstdlib>

using std::cout;
using std::cin;
using std::endl;
using std::setprecision;
using std::fixed;
using std::showpoint;
using std::showpos;
using std::find;
using std::find_if;
using std::adjacent_find;
using std::pow;
using std::iterator;
using std::exit;

// object function

template <class T>
class greaterThan2 {
public:
    bool operator() (const T& x) const
    {
        return (x > -2);
    }
};

// object function

template <class T>
class lessThan2 {
public:
    bool operator() (const T& x) const
    {
        return (x < 2);
    }
};

// object function

template <class T>
class equalLocal {
private:

    long long iLoc = -1;

public:

    bool operator() (const T& x)
    {
        iLoc++;
        return (x == cos(static_cast<double>(iLoc)));
    }
};

// the main function

int main()
{
    // local variables and parameters

    const long long DIM_MAX = static_cast<long long>(pow(10.0, 5.0));
    const long long K_MAX = static_cast<long long>(pow(10.0, 5.0));
    double * ptr;
    int sentinel;

    // main for loop

    for (long long k = 0; k < K_MAX; k++) {
        cout << "-------------------------------------------------->> " << k << endl;

        // allocate the RAM

        cout << "  1 --> allocate RAM" << endl;

        double * a = new double [DIM_MAX];

        // build the array

        cout << "  2 --> build the array" << endl;

        for (long long i = 0; i < DIM_MAX; i++) {
            a[i] = cos(static_cast<double>(i));
        }

        // find all the elements

        cout << "  3 --> find all elements" << endl;

        for (long long i = 0; i != DIM_MAX; i++) {
            ptr = find(&a[0], &a[DIM_MAX], cos(static_cast<double>(i)));

            if (ptr == &a[DIM_MAX]) {
                cout << "ERROR! Enter an integer to exit: ";
                cin >> sentinel;
                exit(-1);
            }
        }

        // find_if all the elements

        cout << "  4 --> find-if all elements greater than -2" << endl;

        for (long long i = 0; i != DIM_MAX; i++) {
            ptr = find_if(&a[0], &a[DIM_MAX], greaterThan2<double>());

            if (ptr == &a[DIM_MAX]) {
                cout << "ERROR! Enter an integer to exit: ";
                cin >> sentinel;
                exit(-1);
            }
        }

        // find_if all the elements

        cout << "  5 --> find-if all elements less than 2" << endl;

        for (long long i = 0; i != DIM_MAX; i++) {
            ptr = find_if(&a[0], &a[DIM_MAX], lessThan2<double>());

            if (ptr == &a[DIM_MAX]) {
                cout << "ERROR! Enter an integer to exit: ";
                cin >> sentinel;
                exit(-1);
            }
        }

        // find_if all the elements

        cout << "  6 --> find-if all elements equal to cos(i)" << endl;

        for (long long i = 0; i != DIM_MAX; i++) {
            ptr = find_if(&a[0], &a[DIM_MAX], equalLocal<double>());

            if (ptr == &a[DIM_MAX]) {
                cout << "ERROR! Enter an integer to exit: ";
                cin >> sentinel;
                exit(-1);
            }
        }
    }

    // sentineling

    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
