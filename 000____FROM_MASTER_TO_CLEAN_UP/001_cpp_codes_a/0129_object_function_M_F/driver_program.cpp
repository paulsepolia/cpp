//=================//
// object function //
//=================//

#include <iostream>

using std::cout;
using std::cin;
using std::endl;

// a less-than template class

template <class T>
class less_than {
public:

    int operator()(const T &x, const T &y)
    {
        return x < y;
    }
};

// the main function

int main()
{
    less_than<int>    ob1;
    less_than<double> ob2;

    cout << "ob1(2, 3)     = " << ob1(2, 3) << endl;
    cout << "ob1(3, 2)     = " << ob1(3, 2) << endl;
    cout << "ob1(3, 3)     = " << ob1(3, 3) << endl;
    cout << "ob2(3.0, 3.1) = " << ob2(3.0, 3.1) << endl;

    cout << "less_than<double>()(2.1, 2.2) = " << less_than<double>()(2.1, 2.2) << endl;
    cout << "less_than<double>()(2.3, 2.2) = " << less_than<double>()(2.3, 2.2) << endl;

    int sentinel;
    cin >> sentinel;

    return 0;
}

// less_than<double> IS TYPE
// less_than<double>() IS AN OBJECT
// less_than<double>()(1.2, 1.3) IS A CALL TO THE MEMBER FUNCTION operator() OF THIS OBJECT

//======//
// FINI //
//======//

