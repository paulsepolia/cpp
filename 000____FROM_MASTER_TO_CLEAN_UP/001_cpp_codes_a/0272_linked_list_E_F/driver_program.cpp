
//=========================================//
// linked list with a vector as data member//
//=========================================//

#include <iostream>
#include <cmath>
#include <cstdlib>

using std::endl;
using std::cout;
using std::cin;
using std::pow;

// the linkList definition

class linkList {
public:

    linkList(): data(0), next(NULL), vec(NULL) {};
    ~linkList() {}; // { delete [] vec; };
    // the destructor does not contains the delete statement
    // because i delete manualy the vector
    // I prefer that way, so to have absolute control.

    int data;
    double * vec;
    linkList * next;
};

// the main function

int main()
{
    // local parameters and variables

    const long DIM_MAX = pow(10.0, 7.3);  // dimension of nodes vector
    const long NODES_MAX = 10;            // number of nodes
    const long J_MAX = pow(10.0, 5.0);    // number of trials

    // main for test loop

    for (long j = 0; j < J_MAX; j++) {
        cout << "------------------------------------------>> " << j << endl;

        linkList * head;
        linkList * node;
        head = new linkList;
        node = new linkList;

        // initailize the first node
        // it is next to the head

        head->next = node;

        // create NODE_MAX number of nodes

        for (long i = 0; i < NODES_MAX; i++) {
            node->data = i;
            node->vec = new double [DIM_MAX];

            for (long j = 0; j < DIM_MAX; j++) {
                node->vec[j] = j;
            }

            node->next = new linkList;
            node = node->next;
        }

        // set the end of nodes as null pointer
        // it is a mark to detect the end node

        node->next = NULL;

        // start again

        // initialize the first node

        node = head->next;

        // delete vec in each node
        // i can do that implicitly if i put the deletion
        // statement in the destructor
        // Warning: Can nto do that two times : here and in the destructor
        // only here OR in the destructor

        for (long i = 0; i < NODES_MAX; i++) {
            delete [] node->vec;
            node = node->next;
        }

        // read the data in each node
        // just for testing purposes

        node = head->next;

        while (node->next != NULL) {
            //cout << node->data << endl; // testing purposes
            node = node->next;
        }

        // delete nodes
        // i delete always the next to next to the head node

        node = head->next;
        linkList * q;

        while (node->next != NULL) {
            q = node->next;
            node->next = q->next;
            //cout << q->data << endl; // testing purposes

            delete q;
        }
    }

    int sentinel;
    cin >> sentinel;

}

//======//
// FINI //
//======//

