
//=========================//
// class Money declaration //
//=========================//

#include <iostream>

using std::cout;
using std::endl;

// declaration

class Money {
public:

    // constructors

    Money();
    Money(double); // supports automatic type conversion

    // member functions

    void input(const double &);
    void output() const;
    double get() const;

    // overloading operators as member functions

    const Money & operator += (const Money &);
    const Money & operator -= (const Money &);
    const Money & operator *= (const Money &);
    const Money & operator /= (const Money &);

    const Money operator + (const Money &) const;
    const Money operator - (const Money &) const;
    const Money operator * (const Money &) const;
    const Money operator / (const Money &) const;

    bool operator == (const Money &) const;
    bool operator <  (const Money &) const;
    bool operator >  (const Money &) const;
    bool operator <= (const Money &) const;
    bool operator >= (const Money &) const;

    const Money operator -  () const;
    const Money operator ++ ();
    const Money operator ++ (int);
    const Money operator -- ();
    const Money operator -- (int);

private:

    double valLoc;
};

//======//
// FINI //
//======//
