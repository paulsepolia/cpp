
#include <iostream>
#include <iomanip>
#include "money_definition.h"

using std::endl;
using std::cout;
using std::cin;
using std::fixed;
using std::setprecision;
using std::showpoint;
using std::showpos;
using std::boolalpha;
using std::noboolalpha;

// the main function

int main()
{
    // 1

    cout << fixed;
    cout << setprecision(2);
    cout << showpoint;
    cout << showpos;
    cout << boolalpha;

    // 2

    Money monA;
    Money monB;
    Money monC;

    // 3

    monA.output();
    monB.output();

    // 4

    double valA = 10.0;
    double valB = 11.0;

    monA.input(valA);
    monB.input(valB);

    monA.output();
    monB.output();

    // 5

    monC = monA + monB;

    monC.output();

    // 6

    monC = monA - monB;

    monC.output();

    // 7

    cout << (monA == monB) << endl;
    cout << (monA == monA) << endl;
    cout << (monB == monB) << endl;
    cout << (monC == monB) << endl;

    // 8

    monA = -monA;

    cout << monA.get() << endl;

    monA = -monB;

    cout << monA.get() << endl;

    // 9

    ++monA;

    cout << "-------------------------------->> ++monA" << endl;
    cout << monA.get() << endl;

    ++monA;

    cout << "-------------------------------->> ++monA" << endl;
    cout << monA.get() << endl;

    // 10

    monA++;

    cout << "-------------------------------->> monA++" << endl;
    cout << monA.get() << endl;

    monA++;

    cout << "-------------------------------->> monA++" << endl;
    cout << monA.get() << endl;

    // 11

    --monA;

    cout << "-------------------------------->> --monA" << endl;
    cout << monA.get() << endl;

    --monA;

    cout << "-------------------------------->> --monA" << endl;
    cout << monA.get() << endl;

    // 12

    monA--;

    cout << "-------------------------------->> monA--" << endl;
    cout << monA.get() << endl;

    monA--;

    cout << "-------------------------------->> monA--" << endl;
    cout << monA.get() << endl;

    // 13

    monC = monA * monB;

    cout << monC.get() << endl;

    // 14

    monC = monA / monB;

    cout << monC.get() << endl;

    // 15

    cout << (monA > monB) << endl;
    cout << (monA < monB) << endl;
    cout << (monA >= monB) << endl;
    cout << (monA <= monB) << endl;
    cout << (monA <= monA) << endl;
    cout << (monA < monA) << endl;

    // 16

    monA.input(20.0);
    monB.input(30.0);
    monC.input(40.0);

    monC += monA;

    cout << monA.get() << endl;
    cout << monC.get() << endl;

    // 17

    monA.input(20.0);
    monB.input(30.0);
    monC.input(40.0);

    monC -= monA;

    cout << monA.get() << endl;
    cout << monC.get() << endl;

    // 18

    monA.input(20.0);
    monB.input(30.0);
    monC.input(40.0);

    monC *= monA;

    cout << monA.get() << endl;
    cout << monC.get() << endl;

    // 19

    monA.input(20.0);
    monB.input(30.0);
    monC.input(40.0);

    monC /= monA;

    cout << monA.get() << endl;
    cout << monC.get() << endl;

    // 20

    monA.input(20.0);
    monB.input(30.0);
    monC.input(40.0);

    monB = monA + 15; // supported becuase of the constructor
    //monC = 20 + monA; // does not supported because the overloading is
    // done using member operators and the symmetry
    // of the two arguments has been lost

    cout << monA.get() << endl;
    cout << monB.get() << endl;
    cout << monC.get() << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
