
//========================//
// class Money definition //
//========================//

#include <iostream>
#include "money_declaration.h"

using std::cout;
using std::endl;

// default constructor provided by the programmer

Money::Money() : valLoc (0.0) {};

// constructor with an argument

Money::Money(double val) : valLoc (val) {};

// member function

void Money::input(const double & val)
{
    valLoc = val;
}

// member function

void Money::output() const
{
    cout << valLoc << endl;
}

// member function

double Money::get() const
{
    return valLoc;
}

// member function

// += operator overload

const Money & Money::operator += (const Money & rhs)
{
    this->valLoc = this->get() + rhs.get();
    return *this;
}

// member function

// -= operator overload

const Money & Money::operator -= (const Money & rhs)
{
    this->valLoc = this->get() - rhs.get();
    return *this;
}

// member function

// *= operator overload

const Money & Money::operator *= (const Money & rhs)
{
    this->valLoc = this->get() * rhs.get();
    return *this;
}

// member function

// /= operator overload

const Money & Money::operator /= (const Money & rhs)
{
    this->valLoc = this->get() / rhs.get();
    return *this;
}

// member function

// + operator overload

const Money Money::operator + (const Money & mon2) const
{
    double valLoc;

    valLoc = this->get() + mon2.get();

    return Money(valLoc);
}

// member function

// - operator overload

const Money Money::operator - (const Money & mon2) const
{
    double valLoc;

    valLoc = this->get() - mon2.get();

    return Money(valLoc);
}

// member function

// * operator overload

const Money Money::operator * (const Money & mon2) const
{
    double valLoc;

    valLoc = this->get() * mon2.get();

    return Money(valLoc);
}

// member function

// / operator overload

const Money Money::operator / (const Money & mon2) const
{
    double valLoc;

    valLoc = this->get() / mon2.get();

    return Money(valLoc);
}

// member function

// == operator overload

// declaration

bool Money::operator == (const Money & mon2) const
{
    return (this->get() == mon2.get());
}

// member function

// <= operator overload

// declaration

bool Money::operator <= (const Money & mon2) const
{
    return (this->get() <= mon2.get());
}

// member function

// >= operator overload

// declaration

bool Money::operator >= (const Money & mon2) const
{
    return (this->get() >= mon2.get());
}

// member function

// < operator overload

// declaration

bool Money::operator < (const Money & mon2) const
{
    return (this->get() < mon2.get());
}

// member function

// > operator overload

// declaration

bool Money::operator > (const Money & mon2) const
{
    return (this->get() > mon2.get());
}

// member function

// - operator overload

const Money Money::operator - () const
{
    return (Money(this->get()));
}

// member function

// ++ operator overload (prefix)

const Money  Money::operator ++ ()
{
    double tmp = 0;

    tmp = this->get();

    ++tmp;

    this->input(tmp);

    return (Money(this->get()));
}

// member function

// ++ operator overload (postfix)

const Money  Money::operator ++ (int)
{
    ++(*this);

    return (Money(this->get()));
}

// member function

// -- operator overload (prefix)

const Money  Money::operator -- ()
{
    double tmp = 0;

    tmp = this->get();

    --tmp;

    this->input(tmp);

    return (Money(this->get()));
}

// member function

// -- operator overload (postfix)

const Money  Money::operator -- (int)
{
    --(*this);

    return (Money(this->get()));
}

//======//
// FINI //
//======//

