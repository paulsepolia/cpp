
//==============================================//
// manipulators: setprecision, fixed, showpoint //
//==============================================//

//===============//
// code --> 0003 //
//===============//

// keywords: manipulators, fixed, showpoint, setprecision, showpos

#include <iostream>
#include <iomanip>

using std::cout;
using std::cin;
using std::endl;
using std::fixed;
using std::setprecision;
using std::showpos;
using std::showpoint;

const double PI = 3.14159265;

// the main function

int main()
{
    double radius = 12.67;
    double height = 12.00;

    cout << fixed;
    cout << showpoint;
    cout << showpos;

    cout << " --> 1" << endl;

    cout << setprecision(2);

    cout << " --> setprecision(2)" << endl;
    cout << " --> radius = " << radius << endl;
    cout << " --> height = " << height << endl;
    cout << " --> volume = " << PI * radius * radius * height << endl;
    cout << " --> PI = " << PI << endl;

    cout << " --> 2" << endl;

    cout << setprecision(3);

    cout << " --> setprecision(3)" << endl;
    cout << " --> radius = " << radius << endl;
    cout << " --> height = " << height << endl;
    cout << " --> volume = " << PI * radius * radius * height << endl;
    cout << " --> PI = " << PI << endl;

    cout << " --> 3" << endl;

    cout << setprecision(4);

    cout << " --> setprecision(4)" << endl;
    cout << " --> radius = " << radius << endl;
    cout << " --> height = " << height << endl;
    cout << " --> volume = " << PI * radius * radius * height << endl;
    cout << " --> PI = " << PI << endl;

    cout << " --> 4" << endl;

    cout << " --> various precisions" << endl;

    cout << " --> " << setprecision(3) << radius << endl;
    cout << " --> " << setprecision(6) << height << endl;
    cout << " --> " << setprecision(16) << PI << endl;

    cout << " --> 5" << endl;

    cout << " --> various precisions" << endl;

    cout << " --> " << setprecision(1) << radius << endl;
    cout << " --> " << setprecision(5) << height << endl;
    cout << " --> " << setprecision(10) << PI << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

