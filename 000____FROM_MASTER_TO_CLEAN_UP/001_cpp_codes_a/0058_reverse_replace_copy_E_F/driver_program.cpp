//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/10/30              //
// Functions: vector             //
// Algorithms: reverse_copy      //
//===============================//

#include <iostream>
#include <algorithm>
#include <vector>
#include <iomanip>

using namespace std;

int main ()
{
    // variables declaration

    const long int I_MAX = 20 * static_cast<long int>(10000000);
    vector<double> aVector;
    long int i;
    int j;
    const int J_MAX = 200;

    // set the ouput format

    cout << fixed;
    cout << showpos;
    cout << showpoint;
    cout << setprecision(20);

    for (j = 0; j < J_MAX; j++) {
        cout << "-------------------------------------------------------> " << j+1 << endl;

        // build the array

        cout << " 1 --> build the array" << endl;

        double *aArray = new double [I_MAX];

        for (i = 0; i < I_MAX; i++) {
            aArray[i] = static_cast<double>(i);
        }

        // vector resize and allocate space

        cout << " 2 --> resize the vector" << endl;

        aVector.resize(I_MAX);

        // do the reverse copy

        cout << " 3 --> reverse_copy the array to the vector x 4" << endl;

        reverse_copy(aArray, aArray+I_MAX, aVector.begin());
        copy(aArray, aArray+I_MAX, aVector.begin());
        reverse_copy(aArray, aArray+I_MAX, aVector.begin());
        copy(aArray, aArray+I_MAX, aVector.begin());
        reverse_copy(aArray, aArray+I_MAX, aVector.begin());

        // some output

        cout << " 4 --> some output" << endl;

        cout << " aArray[0]  --> " << aArray[0] << endl;
        cout << " aArray[1]  --> " << aArray[1] << endl;
        cout << " aVector[0] --> " << aVector[0] << endl;
        cout << " aVector[1] --> " << aVector[1] << endl;

        // free RAM

        cout << " 5 --> free RAM" << endl;

        delete [] aArray;
        aVector.clear();
    }

    return 0;
}

// end
