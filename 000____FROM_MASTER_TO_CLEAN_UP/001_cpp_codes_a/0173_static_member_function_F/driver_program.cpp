//============================//
// Base and derived class     //
// Public, Protected, Private //
//============================//

#include <iostream>

using std::cout;
using std::cin;
using std::endl;

// a class

class user {
private:

    int id;
    static int next_id;

public:

    static int next_user_id()
    {
        next_id++;
        return next_id;
    }

    user() : id (10)   // my default constructor
    {
        id = user::next_id++;
        cout << " --> inside constructor --> id = " << id << endl;  //or, id = user.next_user_id();
        cout << " --> inside constructor --> next_id = " << next_id << endl;
    }
};

// static variable definition

int user::next_id = 0;

// the main function

int main()
{
    user A;
    user B;
    user C;

    cout << " --> A.next_user_id() = " << A.next_user_id() << endl;
    cout << " --> B.next_user_id() = " << B.next_user_id() << endl;
    cout << " --> C.next_user_id() = " << C.next_user_id() << endl;

    // a static member function is not bind to any object
    // so you can call it form the class directly

    cout << " --> user::next_user_id() = " << user::next_user_id() << endl;
    cout << " --> user::next_user_id() = " << user::next_user_id() << endl;
    cout << " --> user::next_user_id() = " << user::next_user_id() << endl;

    return 0;
}

//======//
// FINI //
//======//
