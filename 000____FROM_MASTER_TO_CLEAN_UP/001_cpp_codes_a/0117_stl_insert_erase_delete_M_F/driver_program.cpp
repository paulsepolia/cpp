//=====================================//
// STL Build, Push, Pop, Remove, Clear //
// Vector, Deque, List                 //
//=====================================//

#include <iostream>
#include <algorithm>
#include <vector>
#include <list>
#include <deque>
#include <cmath>
#include <iomanip>
#include <ctime>

// A. the main program

int main()
{
    // 1. variables and parameters

    const long int I_MAX = static_cast<long int>(pow(10.0, 7.7));
    const long int K_MAX = static_cast<long int>(pow(10.0, 6.0));
    const long int J_MAX = static_cast<long int>(pow(10.0, 2.5));
    long int i;
    long int k;
    long int j;
    clock_t t1;
    clock_t t2;
    std::list<double>::iterator itL1;
    std::list<double>::iterator itL2;
    std::vector<double>::iterator itV1;
    std::vector<double>::iterator itV2;
    std::deque<double>::iterator itD1;
    std::deque<double>::iterator itD2;
    const double ELEMA = 10.0;

    // set the output

    std::cout << std::fixed;
    std::cout << std::setprecision(5);
    std::cout << std::showpoint;

    // 2. main bench loop

    for (k = 0; k < K_MAX; k++) {
        std::list<double>* listA  = new std::list<double>   [1];
        std::vector<double>* vecA = new std::vector<double> [1];
        std::deque<double>* deqA  = new std::deque<double>  [1];

        std::cout << std::endl;
        std::cout << "------------------------------------------------------------>>> " << k << std::endl;
        std::cout << std::endl;

        //=======================//
        // BUILD using push_back //
        //=======================//

        std::cout<< std::endl;
        std::cout << " BUILD using push_back()" << std::endl;
        std::cout<< std::endl;

        #pragma omp parallel sections default(none)                        \
        shared(vecA, deqA, listA, std::cout) \
        private(t1, t2, i)
        {
            #pragma omp section
            {
                // 3. building the vector

                t1 = clock();
                std::cout << "  1 --> building the vector --> (*vecA)" << std::endl;
                for (i = 0; i < I_MAX; i++)
                {
                    (*vecA).push_back(static_cast<double>(cos(double(i))));
                }
                t2 = clock();
                std::cout << "    --> done with vector --> (*vecA) --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 4. building the list

                t1 = clock();
                std::cout << "  2 --> building the list --> (*listA)" << std::endl;
                for (i = 0; i < I_MAX; i++)
                {
                    (*listA).push_back(static_cast<double>(cos(double(i))));
                }
                t2 = clock();
                std::cout << "    --> done with list --> (*listA) --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 5. building the deque

                t1 = clock();
                std::cout << "  3 --> building the deque --> (*dequeA)" << std::endl;
                for (i = 0; i < I_MAX; i++)
                {
                    (*deqA).push_back(static_cast<double>(cos(double(i))));
                }
                t2 = clock();
                std::cout << "    --> done with deque --> (*dequeA) --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
        }

        //================================//
        // get the size of the containers //
        //================================//

        std::cout << " (*vecA).size()  --> " << (*vecA).size()  << std::endl;
        std::cout << " (*listA).size() --> " << (*listA).size() << std::endl;
        std::cout << " (*deqA).size()  --> " << (*deqA).size()  << std::endl;

        //=======================//
        // INSERT using insert() //
        //=======================//

        std::cout<< std::endl;
        std::cout << " INSERT using insert()" << std::endl;
        std::cout<< std::endl;

        #pragma omp parallel sections default(none)                        \
        shared(vecA, deqA, listA, std::cout) \
        shared(itV1, itL1, itD1)             \
        private(t1, t2)
        {
            #pragma omp section
            {
                // 6. insert in to the vector at position 2

                t1 = clock();
                std::cout << "  4 --> vector --> insert at pos 2" << std::endl;
                itV1 = (*vecA).begin(); // points at the beginning of the vector
                ++itV1; // points to the location 2
                itV1 = (*vecA).insert(itV1, ELEMA);
                t2 = clock();
                std::cout << "    --> done with vector --> (*vecA) --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 7. insert in to the list at position 2

                t1 = clock();
                std::cout << "  5 --> list --> insert at pos 2" << std::endl;
                itL1 = (*listA).begin();
                ++itL1;
                itL1 = (*listA).insert(itL1, ELEMA);
                t2 = clock();
                std::cout << "    --> done with list --> (*listA) --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 8. insert in to the deque at position 2

                t1 = clock();
                std::cout << "  6 --> deque --> insert at pos 2" << std::endl;
                itD1 = (*deqA).begin();
                ++itD1;
                itD1 = (*deqA).insert(itD1, ELEMA);
                t2 = clock();
                std::cout << "    --> done with deque --> (*deqA) --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
        }

        //================================//
        // get the size of the containers //
        //================================//

        std::cout << " (*vecA).size()  --> " << (*vecA).size()  << std::endl;
        std::cout << " (*listA).size() --> " << (*listA).size() << std::endl;
        std::cout << " (*deqA).size()  --> " << (*deqA).size()  << std::endl;

        //======================//
        // ERASE using pop_back //
        //======================//

        std::cout<< std::endl;
        std::cout << " ERASE using pop_back()" << std::endl;
        std::cout<< std::endl;

        #pragma omp parallel sections default(none)                        \
        shared(vecA, deqA, listA, std::cout) \
        shared(itV1, itL1, itD1)             \
        private(t1, t2, i)
        {
            #pragma omp section
            {
                // 9. pop_back the vector

                t1 = clock();
                std::cout << "  7 --> pop_back() the vector --> (*vecA)" << std::endl;
                for (i = 0; i < I_MAX+1; i++)
                {
                    (*vecA).pop_back();
                }
                (*vecA).shrink_to_fit();
                t2 = clock();
                std::cout << "    --> done with vector --> (*vecA) --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 10. pop_back the list

                t1 = clock();
                std::cout << "  8 --> pop_back() the list --> (*listA)" << std::endl;
                for (i = 0; i < I_MAX+1; i++)
                {
                    (*listA).pop_back();
                }
                t2 = clock();
                std::cout << "    --> done with list --> (*listA) --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 11. pop_back the deque

                t1 = clock();
                std::cout << "  9 --> pop_back() the deque --> (*deqA)" << std::endl;
                for (i = 0; i < I_MAX+1; i++)
                {
                    (*deqA).pop_back();
                }
                (*deqA).shrink_to_fit();
                t2 = clock();
                std::cout << "    --> done with deque --> (*deqA) --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
        }

        //================================//
        // get the size of the containers //
        //================================//

        std::cout << " (*vecA).size()  --> " << (*vecA).size()  << std::endl;
        std::cout << " (*listA).size() --> " << (*listA).size() << std::endl;
        std::cout << " (*deqA).size()  --> " << (*deqA).size()  << std::endl;

        //=======================//
        // BUILD using push_back //
        //=======================//

        std::cout<< std::endl;
        std::cout << " BUILD using push_back()" << std::endl;
        std::cout<< std::endl;

        #pragma omp parallel sections default(none)                        \
        shared(vecA, deqA, listA, std::cout) \
        private(t1, t2, i)
        {
            #pragma omp section
            {
                // 12. building the vector

                t1 = clock();
                std::cout << " 10 --> building the vector --> (*vecA)" << std::endl;
                for (i = 0; i < I_MAX; i++)
                {
                    (*vecA).push_back(static_cast<double>(cos(double(i))));
                }
                t2 = clock();
                std::cout << "    --> done with vector --> (*vecA) --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 13. building the list

                t1 = clock();
                std::cout << " 11 --> building the list --> (*listA)" << std::endl;
                for (i = 0; i < I_MAX; i++)
                {
                    (*listA).push_back(static_cast<double>(cos(double(i))));
                }
                t2 = clock();
                std::cout << "    --> done with list --> (*listA) --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 14. building the deque

                t1 = clock();
                std::cout << " 12 --> building the deque --> (*deqA)" << std::endl;
                for (i = 0; i < I_MAX; i++)
                {
                    (*deqA).push_back(static_cast<double>(cos(double(i))));
                }
                t2 = clock();
                std::cout << "    --> done with deque --> (*deqA) --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
        }

        //=======================//
        // INSERT using insert() //
        //=======================//

        std::cout<< std::endl;
        std::cout << " INSERT using insert()" << std::endl;
        std::cout<< std::endl;

        #pragma omp parallel sections default(none)                        \
        shared(vecA, deqA, listA, std::cout) \
        shared(itV1, itL1, itD1)             \
        private(t1, t2, j)
        {
            #pragma omp section
            {
                // 15. insert in to the vector at position 2

                t1 = clock();
                std::cout << " 13 --> vector --> insert at pos 2 --> " << J_MAX << " times" << std::endl;
                itV1 = (*vecA).begin(); // points at the beginning of the vector
                ++itV1; // points to the location 2
                for (j = 0; j < J_MAX; j++)
                {
                    itV1 = (*vecA).insert(itV1, ELEMA);
                }
                t2 = clock();
                std::cout << "    --> done with vector --> (*vecA) --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 16. insert in to the list at position 2

                t1 = clock();
                std::cout << " 14 --> list --> insert at pos 2 --> " << J_MAX << " times" << std::endl;
                itL1 = (*listA).begin();
                ++itL1;
                for (j = 0; j < J_MAX; j++)
                {
                    itL1 = (*listA).insert(itL1, ELEMA);
                }
                t2 = clock();
                std::cout << "    --> done with list --> (*listA) --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 17. insert in to the deque at position 2

                t1 = clock();
                std::cout << " 15 --> deque --> insert at pos 2 --> " << J_MAX << " times" << std::endl;
                itD1 = (*deqA).begin();
                ++itD1;
                for (j = 0; j < J_MAX; j++)
                {
                    itD1 = (*deqA).insert(itD1, ELEMA);
                }
                t2 = clock();
                std::cout << "    --> done with deque --> (*deqA) --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
        }

        //================================//
        // get the size of the containers //
        //================================//

        std::cout << " (*vecA).size()  --> " << (*vecA).size()  << std::endl;
        std::cout << " (*listA).size() --> " << (*listA).size() << std::endl;
        std::cout << " (*deqA).size()  --> " << (*deqA).size()  << std::endl;

        //===================//
        // FREE THE RAM FAST //
        //===================//

        std::cout << "=====================================" << std::endl;

        t1 = clock();
        std::cout << " deleting vecA .... " << std::endl;
        delete [] vecA;
        t2 = clock();
        std::cout << "    --> done with vector --> vecA --> "
                  << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;

        t1 = clock();
        std::cout << " delete the listA .... " << std::endl;
        delete [] listA;
        t2 = clock();
        std::cout << "    --> done with list --> listA --> "
                  << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;

        t1 = clock();
        std::cout << " delete the deqA .... " << std::endl;
        delete [] deqA;
        t2 = clock();
        std::cout << "    --> done with deque --> deqA --> "
                  << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;

        std::cout << "=====================================" << std::endl;
    }


    int sentinel;
    std::cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
