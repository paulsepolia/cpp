
//========================================//
// sorting a vector into descending order //
//========================================//

#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>

using std::cout;
using std::cin;
using std::endl;
using std::vector;
using std::greater;
using std::sort;
using std::less;

// the main function

int main()
{
    // local variables and parameters

    vector<char> vA(26);
    less<char> funObjLessChar;

    // build the vector

    for(unsigned int i = 0; i < vA.size(); i++) {
        vA[i] = 'A' + i;
    }

    // display original vector

    cout << endl;
    cout << " --> original ordering of vA: " << endl << endl;

    for (unsigned int i = 0; i < vA.size(); i++) {
        cout << vA[i] << " ";
    }

    cout << endl << endl;

    // sort vector into desceding order

    sort(vA.begin(), vA.end(), greater<char>());

    // display re-ordered vector

    cout << " --> re-ordering of vA: " << endl << endl;

    for (unsigned int i = 0; i < vA.size(); i++) {
        cout << vA[i] << " ";
    }

    cout << endl << endl;

    // sort vector into asceding order

    sort(vA.begin(), vA.end(), funObjLessChar);

    // display re-ordered vector

    cout << " --> re-ordering of vA: " << endl << endl;

    for (unsigned int i = 0; i < vA.size(); i++) {
        cout << vA[i] << " ";
    }

    cout << endl << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
