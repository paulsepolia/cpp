//=============================//
// Header file                 //
// ElementCL Class Declaration //
//=============================//

template <typename T>
class ElementCL {
public:
    ElementCL(const T& elem=0); // constructor declaration
    ~ElementCL();               // destructor declaration

    //=======================//
    // Member functions here //
    //=======================//

    void SetElementF(const T& =0);
    T GetElementF() const;
    void SetEqualF(const ElementCL<T>&);
    void SetEqualZeroF();
    void AddF(const ElementCL<T>&, const ElementCL<T>&);
    void SubtractF(const ElementCL<T>&, const ElementCL<T>&);
    void TimesF(const ElementCL<T>&, const ElementCL<T>&);
    void DivideF(const ElementCL<T>&, const ElementCL<T>&);
    void AbsF(const ElementCL<T>&);

    //=======================//
    // Friend functions here //
    //=======================//

    // EMPTY //

private:

    T m_Elem;

public:

    //===========================//
    // Overloaded operators here //
    //===========================//


    // 1. '='

    template <typename T2>
    ElementCL<T2>& operator=(const ElementCL<T2>& elem)
    {
        if (this != &elem) { // 'this' points to object which calls the
            // function 'operator=()'.
            m_Elem = elem.GetElementF();
        }

        return *this;
    }

    // 2. '+'

    template <typename T2>
    ElementCL<T2> operator+(ElementCL<T2>& elem)
    {
        ElementCL<T2> tmp;

        tmp.SetElementF(m_Elem + elem.GetElementF());

        return tmp;
    }

    // 3. '-'

    template <typename T2>
    ElementCL<T2> operator-(ElementCL<T2>& elem)
    {
        ElementCL<T2> tmp;

        tmp.SetElementF(m_Elem - elem.GetElementF());

        return tmp;
    }

    // 4. '*'

    template <typename T2>
    ElementCL<T2> operator*(ElementCL<T2> elem)
    {
        elem.SetElementF(m_Elem * elem.GetElementF());

        return elem;
    }

    // 5. '/'

    template <typename T2>
    ElementCL<T2> operator/(ElementCL<T2> elem)
    {
        elem.SetElementF(m_Elem / elem.GetElementF());

        return elem;
    }

    // 6. '+='

    template <typename T2>
    ElementCL<T2> operator+=(ElementCL<T2> elem)
    {
        m_Elem = m_Elem + elem.GetElementF();

        elem.SetElementF(m_Elem);

        return elem;
    }

    // 7. '-='

    template <typename T2>
    ElementCL<T2> operator-=(ElementCL<T2> elem)
    {
        m_Elem = m_Elem - elem.GetElementF();

        elem.SetElementF(m_Elem);

        return elem;
    }

    // 8. '*='

    template <typename T2>
    ElementCL<T2> operator*=(ElementCL<T2> elem)
    {
        m_Elem = m_Elem * elem.GetElementF();

        elem.SetElementF(m_Elem);

        return elem;
    }

    // 9. '/='

    template <typename T2>
    ElementCL<T2> operator/=(ElementCL<T2> elem)
    {
        m_Elem = m_Elem / elem.GetElementF();

        elem.SetElementF(m_Elem);

        return elem;
    }

    // 10. '>='

    template <typename T2>
    bool operator>=(ElementCL<T2> elem)
    {
        bool val;

        val = (m_Elem >= elem.GetElementF());

        return val;
    }

    // 11. '<='

    template <typename T2>
    bool operator<=(ElementCL<T2> elem)
    {
        bool val;

        val = (m_Elem <= elem.GetElementF());

        return val;
    }

    // 12. '>'

    template <typename T2>
    bool operator>(ElementCL<T2> elem)
    {
        bool val;

        val = (m_Elem > elem.GetElementF());

        return val;
    }

    // 13. '<'

    template <typename T2>
    bool operator<(ElementCL<T2> elem)
    {
        bool val;

        val = (m_Elem < elem.GetElementF());

        return val;
    }

    // 14. '=='

    template <typename T2>
    bool operator==(ElementCL<T2> elem)
    {
        bool val;

        val = (m_Elem == elem.GetElementF());

        return val;
    }

    // 15. '!='

    template <typename T2>
    bool operator!=(ElementCL<T2> elem)
    {
        bool val;

        val = (m_Elem != elem.GetElementF());

        return val;
    }


};

//======//
// FINI //
//======//
