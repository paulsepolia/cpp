//=======================================//
// Author: Pavlos G. Galiatsatos         //
// Date: 2013/10/23                      //
// Functions: vector, deque, array, list //
// Algorithms: initialization            //
//=======================================//

#include <iostream>
#include <vector>
#include <deque>
#include <list>
#include <iomanip>
#include <algorithm>

using namespace std;

// the main function

int main ()
{
    // some variables declaration

    const long int MAX_ELEM = 5 * static_cast<long int>(10000000);
    const long int J_MAX_DO = 10;
    const long int FIND_MAX = 100;
    vector<double> aVector;
    deque<double> aDeque;
    list<double> aList;
    vector<double>::iterator itV;
    deque<double>::iterator itD;
    list<double>::iterator itL;
    long int i;
    long int j;
    double *pD;

    for (j = 0; j < J_MAX_DO; j++) {
        cout << "--------------------------------------------------> " << j+1 << endl;

        double *aArray = new double [MAX_ELEM];

        // build the array

        cout << " 1 --> build array" << endl;
        for (i = 0; i < MAX_ELEM; i++) {
            aArray[i] = static_cast<double>(MAX_ELEM-i);
        }

        // copy in insert mode the array to the vector, deque, list

        cout << " 2 --> copy array to vector in insert mode" << endl;
        itV = aVector.begin();
        copy(aArray, aArray+MAX_ELEM, inserter(aVector, itV));

        cout << " 3 --> copy array to deque in insert mode" << endl;
        itD = aDeque.begin();
        copy(aArray, aArray+MAX_ELEM, inserter(aDeque, itD));

        cout << " 4 --> copy array to list in insert mode" << endl;
        itL = aList.begin();
        copy(aArray, aArray+MAX_ELEM, inserter(aList, itL));

        // set up the output style

        cout << fixed;
        cout << setprecision(10);
        cout << showpos;
        cout << showpoint;

        // find elements

        cout << " 4 --> find element in a vector --> ";
        for(i = 0; i < FIND_MAX; i ++) {
            itV = find(aVector.begin(), aVector.end(), aArray[i]);
        }
        cout << *itV << endl;

        cout << " 5 --> find element in a deque --> ";
        for(i = 0; i < FIND_MAX; i++) {
            itD = find(aDeque.begin(), aDeque.end(), aArray[i]);
        }
        cout << *itD << endl;

        cout << " 6 --> find element in a array --> ";
        for(i = 0; i < FIND_MAX; i++) {
            pD = find(aArray, aArray+MAX_ELEM, aArray[i]);
        }
        cout << *pD << endl;

        // free RAM

        aVector.clear();
        aDeque.clear();
        aList.clear();
        delete [] aArray;
    }

    return 0;
}

// end
