//========================//
// base and derived class //
//========================//

#include <iostream>
#include "employee.h"
#include "hourlyemployee.h"
#include "salariedemployee.h"

// namespaces

using std::cout;
using std::endl;
using std::cin;
using pgg::Employee;
using pgg::HourlyEmployee;
using pgg::SalariedEmployee;

// functions

void showEmployeeData(const Employee object);

// the main function

int main()
{
    HourlyEmployee joe("Pavlos G. Galiatsatos", "123-45-6789", 20.50, 40);

    SalariedEmployee boss("Mr Big Shot", "987-65-4321", 10500.50);

    showEmployeeData(joe);
    showEmployeeData(boss);

    int sentinel;
    cin >> sentinel;
    return 0;
}

// function definition

void showEmployeeData(const Employee object)
{
    cout << "Name: " << object.getName() <<endl;
    cout << "Social Security Number: " << object.getSsn() << endl;
}

//======//
// FINI //
//======//
