
//============//
// STL; stack //
//============//

#include <iostream>
#include <iomanip>
#include <stack>
#include <ctime>
#include <cmath>

int main ()
{
    // 1. variables and parameters

    const long int I_MAX = static_cast<long int>(pow(10.0, 7.8));
    const long int J_MAX = static_cast<long int>(pow(10.0, 3.0)) + 1;
    const long int K_MAX = static_cast<long int>(pow(10.0, 7.0));
    long int i;
    long int j;
    long int k;
    clock_t t1;
    clock_t t2;

    for (k = 0; k < K_MAX; k++) {
        std::stack<double> *staD1 = new std::stack<double> [1];
        std::stack<double> *staD2 = new std::stack<double> [1];
        std::stack<double> *staD3 = new std::stack<double> [1];
        std::stack<long double> *staL1 = new std::stack<long double> [1];
        std::stack<long double> *staL2 = new std::stack<long double> [1];
        std::stack<long double> *staL3 = new std::stack<long double> [1];

        std::cout << std::endl;
        std::cout << "------------------------------------------------------------------>> " << k << std::endl;
        std::cout << std::endl;

        //=========================//
        // PARALLEL SECTIONS --> A //
        //=========================//

        #pragma omp parallel sections default(none)\
        shared(staD1, staL1, std::cout)\
        private(t1, t2, i)
        {
            #pragma omp section
            {
                // 2. push doubles into stack

                t1 = clock();
                std::cout << " -->  1 --> push elements in to stack --> (*staD1)" << std::endl;
                for (i = 0; i < I_MAX; ++i)
                {
                    (*staD1).push(cos(static_cast<double>(i)));
                }
                t2 = clock();
                std::cout << " --> done with --> (*staD1) --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;

                // 3. pop the doubles until the stack is empty

                t1 = clock();
                std::cout << " -->  2 --> pop elements from the stack --> (*staD1)" << std::endl;
                while (!(*staD1).empty())
                {
                    (*staD1).pop();
                }
                t2 = clock();
                std::cout << " --> done with --> (*staD1) --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 4. push long ints into stack

                t1 = clock();
                std::cout << " -->  3 --> push elements in to stack --> (*staL1)" << std::endl;
                for (i = 0; i < I_MAX; ++i)
                {
                    (*staL1).push(i);
                }
                t2 = clock();
                std::cout << " --> done with --> (*staL1) --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;

                // 5. pop the long ints until the stack is empty

                t1 = clock();
                std::cout << " -->  4 --> pop elements from the stack --> (*staL1)" << std::endl;
                while (!(*staL1).empty())
                {
                    (*staL1).pop();
                }
                t2 = clock();
                std::cout << " --> done with --> (*staL1) --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
        }

        //=========================//
        // PARALLEL SECTIONS --> B //
        //=========================//

        #pragma omp parallel sections default(none)\
        shared(staD1, staL1, std::cout)\
        private(t1, t2, i)
        {
            #pragma omp section
            {
                // 6. push doubles into stack

                t1 = clock();
                std::cout << " -->  5 --> push elements in to stack --> (*staD1)" << std::endl;
                for (i = 0; i < I_MAX; ++i)
                {
                    (*staD1).push(cos(static_cast<double>(i)));
                }
                t2 = clock();
                std::cout << " --> done with --> (*staD1) --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;

                // 7. delete stack

                t1 = clock();
                std::cout << " -->  6 --> delete the stack --> (*staD1)" << std::endl;
                delete [] staD1;
                t2 = clock();
                std::cout << " --> done with --> (*staD1) --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 8. push long ints into stack

                t1 = clock();
                std::cout << " -->  7 --> push elements in to stack --> (*staL1)" << std::endl;
                for (i = 0; i < I_MAX; ++i)
                {
                    (*staL1).push(i);
                }
                t2 = clock();
                std::cout << " --> done with --> (*staL1) --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;

                // 9. delete stack

                t1 = clock();
                std::cout << " -->  8 --> delete the stack --> (*staL1)" << std::endl;
                delete [] staL1;
                t2 = clock();
                std::cout << " --> done with --> (*staL1) --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
        }

        //=========================//
        // PARALLEL SECTIONS --> C //
        //=========================//

        #pragma omp parallel sections default(none)\
        shared(staD3, staL3, staD2, staL2, std::cout)\
        private(t1, t2, i)
        {
            #pragma omp section
            {
                // 10. push doubles into the stacks

                t1 = clock();
                std::cout << " -->  9 --> push elements in to stack --> (*staD3)" << std::endl;
                for (i = 0; i < I_MAX; ++i)
                {
                    (*staD3).push(cos(static_cast<double>(i)));
                }
                t2 = clock();
                std::cout << " --> done with --> (*staD3) --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 11. push long ints into stack

                t1 = clock();
                std::cout << " --> 10 --> push elements in to stack --> (*staL3)" << std::endl;
                for (i = 0; i < I_MAX; ++i)
                {
                    (*staL3).push(i);
                }
                t2 = clock();
                std::cout << " --> done with --> (*staL3) --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 12. push doubles into the stacks

                t1 = clock();
                std::cout << " --> 11 --> push elements in to stack --> (*staD2)" << std::endl;
                for (i = 0; i < I_MAX; ++i)
                {
                    (*staD2).push(sin(static_cast<double>(i)));
                }
                t2 = clock();
                std::cout << " --> done with --> (*staD2) --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 13. push long ints into stack

                t1 = clock();
                std::cout << " --> 12 --> push elements in to stack --> (*staL2)" << std::endl;
                for (i = 0; i < I_MAX; ++i)
                {
                    (*staL2).push(I_MAX-i);
                }
                t2 = clock();
                std::cout << " --> done with --> (*staL2) --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
        }

        //=========================//
        // PARALLEL SECTIONS --> D //
        //=========================//

        #pragma omp parallel sections default(none)\
        shared(staD3, staL3, staD2, staL2, std::cout)\
        private(t1, t2, i, j)
        {
            #pragma omp section
            {
                t1 = clock();
                std::cout << " --> 13 --> swap --> swap((*staD2),(*staD3))" << std::endl;
                for (j = 0; j < J_MAX; j++)
                {
                    std::swap((*staD2),(*staD3));
                }
                t2 = clock();
                std::cout << " --> done with --> swap((*staD2),(*staD3)); --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;

            }
            #pragma omp section
            {
                t1 = clock();
                std::cout << " --> 14 --> swap --> swap((*staL2),(*staL3))" << std::endl;
                for (j = 0; j < J_MAX; j++)
                {
                    std::swap((*staL2),(*staL3));
                }
                t2 = clock();
                std::cout << " --> done with --> swap((*staL2),(*staL3)); --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;

            }
        }

        #pragma omp parallel sections default(none)\
        shared(staD3, staL3, staD2, staL2, std::cout)\
        private(t1, t2, i)
        {
            #pragma omp section
            {
                t1 = clock();
                std::cout << " --> 15 --> delete [] staD2; " << std::endl;
                delete [] staD2;
                t2 = clock();
                std::cout << " --> done with --> delete [] staD2; --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                t1 = clock();
                std::cout << " --> 16 --> delete [] staD3; " << std::endl;
                delete [] staD3;
                t2 = clock();
                std::cout << " --> done with --> delete [] staD3; --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                t1 = clock();
                std::cout << " --> 17 --> delete [] staL2; " << std::endl;
                delete [] staL2;
                t2 = clock();
                std::cout << " --> done with --> delete [] staL2; --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                t1 = clock();
                std::cout << " --> 18 --> delete [] staL3; " << std::endl;
                delete [] staL3;
                t2 = clock();
                std::cout << " --> done with --> delete [] staL3; --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
        }
    }

    int sentinel;
    std::cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
