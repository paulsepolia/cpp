
//====================//
// vector<bool>::flip //
//====================//

#include <iostream>
#include <vector>

using std::endl;
using std::cout;
using std::cin;
using std::vector;
using std::boolalpha;
using std::noboolalpha;

// the main function

int main ()
{
    // local variables and parameters

    vector<bool> mask;

    mask.push_back(2);
    mask.push_back(0);
    mask.push_back(1);
    mask.push_back(0);

    cout << boolalpha;

    // output

    cout << " --> mask contains: " << endl;

    for (unsigned int i = 0; i < mask.size(); i++) {
        cout <<  " --> " << mask[i] << endl;
    }

    for (unsigned int i = 0; i < mask.size(); i++) {
        cout << " --> " << mask[i]
             << " + " << i << " = " << mask[i]+i << endl;
    }

    // flip the bits

    mask.flip();

    // output

    cout << " --> mask fliped contains: " << endl;

    for (unsigned int i = 0; i < mask.size(); i++) {
        cout <<  " --> " << mask[i] << endl;
    }

    for (unsigned int i = 0; i < mask.size(); i++) {
        cout << " --> " << mask[i]
             << " + " << i << " = " << mask[i]+i << endl;
    }

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

