
//====================//
// STL, set, multiset //
//====================//

#include <iostream>
#include <set>
#include <vector>
#include <list>
#include <deque>
#include <algorithm>
#include <iomanip>
#include <numeric>
#include <ctime>
#include <cmath>
#include <iterator>

using std::cout;
using std::cin;
using std::endl;

using std::set;
using std::multiset;

using std::vector;
using std::list;
using std::deque;

using std::find;
using std::sort;
using std::binary_search;

using std::inner_product;

using std::fixed;
using std::showpoint;
using std::showpos;
using std::setprecision;

using std::pow;

using std::iterator;

// the main function

int main()
{
    // local variables and parameters

    long long DIM_MAX = static_cast<long long>(pow(10.0, 7.8));
    long long K_MAX = static_cast<long long>(pow(10.0, 5.0));
    long long FIND_MAX = static_cast<long long>(pow(10.0, 5.0));
    long long SORT_MAX = 5LL;

    double * dp;
    vector<double>::iterator itv;
    deque<double>::iterator itd;
    list<double>::iterator itl1, itl2;
    set<double>::iterator its1, its2;
    multiset<double>::iterator itms1, itms2;
    clock_t t1;
    clock_t t2;

    // output format

    cout << fixed;
    cout << setprecision(20);
    cout << showpos;
    cout << showpoint;

    // for loop

    for (long long k = 0; k != K_MAX; k++) {
        // counter

        cout << "-------------------------------------------------------->> " << k << endl;

        // the containers

        double * a1 = new double [DIM_MAX];
        vector<double> * V1 = new vector<double>;
        deque<double> * D1 = new deque<double>;
        list<double> * L1 = new list<double>;
        set<double> * S1 = new set<double>;
        multiset<double> * MS1 = new multiset<double>;

        //
        // build the array
        //

        cout << endl << endl;

        cout << " --> build the array" << endl;

        t1 = clock();

        for (long long i = 0; i != DIM_MAX; i++) {
            a1[i] = static_cast<double>(DIM_MAX-i);
        }

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // find an element in the array

        cout << " --> find an element in the array" << endl;

        t1 = clock();

        for (long long i = 0; i != FIND_MAX; i++) {
            dp = find(&a1[0], &a1[DIM_MAX], a1[i]);
        }

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // sort the array

        cout << " --> sort the array" << endl;

        t1 = clock();

        for (long long i = 0; i != SORT_MAX; i++) {
            sort(&a1[0], &a1[DIM_MAX]);
        }

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // find an element in the sorted array

        cout << " --> find the same element in the sorted array" << endl;

        t1 = clock();

        for (long long i = 0; i != FIND_MAX; i++) {
            dp = find(&a1[0], &a1[DIM_MAX], a1[i]);
        }

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // delete the array

        delete [] a1;

        //
        // build the vector
        //

        cout << endl << endl;

        cout << " --> build the vector" << endl;

        t1 = clock();

        for (long long i = 0; i != DIM_MAX; i++) {
            V1->push_back(static_cast<double>(DIM_MAX-i));
        }

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // find an element in the vector

        cout << " --> find an element in the vector" << endl;

        t1 = clock();

        for (long long i = 0; i != FIND_MAX; i++) {
            itv = find(V1->begin(), V1->end(), (*V1)[i]);
        }

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

        // sort the vector

        cout << " --> sort the vector" << endl;

        t1 = clock();

        for (long long i = 0; i != SORT_MAX; i++) {
            sort(V1->begin(), V1->end());
        }

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // find an element in the sorted vector

        cout << " --> find the same element in the sorted vector" << endl;

        t1 = clock();

        for (long long i = 0; i != FIND_MAX; i++) {
            itv = find(V1->begin(), V1->end(), (*V1)[i]);
        }

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // delete the vector

        delete V1;

        //
        // build the deque
        //

        cout << endl << endl;

        cout << " --> build the deque" << endl;

        t1 = clock();

        for (long long i = 0; i != DIM_MAX; i++) {
            D1->push_back(static_cast<double>(DIM_MAX-i));
        }

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // find an element in the deque

        cout << " --> find an element in the deque" << endl;

        t1 = clock();

        for (long long i = 0; i != FIND_MAX; i++) {
            itd = find(D1->begin(), D1->end(), (*D1)[i]);
        }

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

        // sort the deque

        cout << " --> sort the deque" << endl;

        t1 = clock();

        for (long long i = 0; i != SORT_MAX; i++) {
            sort(D1->begin(), D1->end());
        }

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // find an element in the sorted deque

        cout << " --> find the same element in the sorted deque" << endl;

        t1 = clock();

        for (long long i = 0; i != FIND_MAX; i++) {
            itd = find(D1->begin(), D1->end(), (*D1)[i]);
        }

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // delete the deque

        delete D1;

        //
        // build the list
        //

        cout << endl << endl;

        cout << " --> build the list" << endl;

        t1 = clock();

        for (long long i = 0; i != DIM_MAX; i++) {
            L1->push_back(static_cast<double>(DIM_MAX-i));
        }

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // find an element in the list

        cout << " --> find an element in the list" << endl;

        t1 = clock();

        itl1 = L1->begin();

        for (long long i = 0; i != FIND_MAX; i++) {
            itl2 = find(L1->begin(), L1->end(), *itl1);
            itl1++;
        }

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

        // sort the list

        cout << " --> sort the list" << endl;

        t1 = clock();

        for (long long i = 0; i != SORT_MAX; i++) {
            L1->sort();
        }

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // find an element in the sorted list

        cout << " --> find the same element in the sorted list" << endl;

        t1 = clock();

        itl1 = L1->begin();

        for (long long i = 0; i != FIND_MAX; i++) {
            itl2 = find(L1->begin(), L1->end(), *itl1);
            itl1++;
        }

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // delete the list

        delete L1;

        //
        // build the set
        //

        cout << endl << endl;

        cout << " --> build the set" << endl;

        t1 = clock();

        for (long long i = 0; i != DIM_MAX; i++) {
            S1->insert(static_cast<double>(DIM_MAX-i));
        }

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // find an element in the set

        cout << " --> find an element in the set" << endl;

        t1 = clock();

        its1 = S1->begin();

        for (long long i = 0; i != FIND_MAX; i++) {
            its2 = find(S1->begin(), S1->end(), *its1);
            its1++;
        }

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

        // delete the set

        delete S1;

        //
        // build the multiset
        //

        cout << endl << endl;

        cout << " --> build the multiset" << endl;

        t1 = clock();

        for (long long i = 0; i != DIM_MAX; i++) {
            MS1->insert(static_cast<double>(DIM_MAX-i));
        }

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // find an element in the multiset

        cout << " --> find an element in the multiset" << endl;

        t1 = clock();

        itms1 = MS1->begin();

        for (long long i = 0; i != FIND_MAX; i++) {
            itms2 = find(MS1->begin(), MS1->end(), *itms1);
            itms1++;
        }

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

        cout << endl << endl;

        // delete the multiset

        delete MS1;
    }

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
