//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/10/30              //
// Functions: auto_ptr           //
//===============================//

#include <iostream>
#include <memory>

using namespace std;

// main

int main ()
{
    auto_ptr<int> a(new int);
    auto_ptr<int> b;

    cout << " 1 --> not any assignment yet" << endl;

    cout << "*a.get() = " << *a.get() << endl;

    cout << " 2 --> an assignment is done" << endl;

    *a.get() = 123;

    cout << "*a.get() --> " << *a.get() << endl;

    b = a;

    cout << "The assignment b = a has been executed." << endl;

    if (a.get() == NULL) {
        cout << "a.get() is NULL" << endl;
    }

    cout << "*b.get() = " << *b.get() << endl;

    return 0;
}

// end
