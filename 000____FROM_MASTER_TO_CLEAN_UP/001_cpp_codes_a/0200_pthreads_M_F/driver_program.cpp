
//=========================//
// pthreads and stack size //
//=========================//

#include <iostream>
#include <string>
#include <pthread.h>
#include <cstdlib>
#include <cassert>
#include <unistd.h>
#include <climits>
#include <cmath>

using std::cin;
using std::cout;
using std::endl;

// 1. size of the arrays

const long MAX_DIM = static_cast<long>(pow(10.0, 8.4));

// 2. function

static void *threadFunc1(void*)
{
    double arrayA[MAX_DIM];

    for (long i = 0; i < MAX_DIM; i++) {
        arrayA[i] = cos(static_cast<double>(i));
    }

    cout << " --> Hello from threadFunc1" << endl;
    cout << " --> threadFunc1 --> arrayA[1] = " << arrayA[1] << endl;

    return NULL;
}

// 3. function

static void *threadFunc2(void*)
{
    double arrayB[MAX_DIM];

    for (long i = 0; i < MAX_DIM; i++) {
        arrayB[i] = cos(static_cast<double>(i));
    }

    cout << " --> Hello from threadFunc2" << endl;
    cout << " --> threadFunc2 --> arrayB[1] = " << arrayB[1] << endl;

    return NULL;
}

// 4. the main function

int main()
{
    pthread_t t1;
    pthread_t t2;
    pthread_attr_t thread_attr;
    const double EXTRA_SIZE = 10000000000.0;

    int s = 0;
    size_t tmp_size = 0;

    s = pthread_attr_init(&thread_attr);
    assert(s==0);

    cout << " --> PTHREADS_STACK_MIN = " << PTHREAD_STACK_MIN << endl;

    s = pthread_attr_setstacksize(&thread_attr , PTHREAD_STACK_MIN + EXTRA_SIZE);

    assert(s == 0);

    s = pthread_attr_getstacksize(&thread_attr , &tmp_size );

    assert(s == 0);

    cout << " --> forced stack size of pthread is:" << tmp_size << endl;

    // launch thread 1

    s = pthread_create(&t1, &thread_attr, threadFunc1, NULL);

    assert(s == 0);

    // launch thread 2

    s = pthread_create(&t2, &thread_attr, threadFunc2, NULL);

    assert(s == 0);

    cout << " --> Main done()" << endl;

    pthread_exit(NULL);

    return 0;
}

//======//
// FINI //
//======//
