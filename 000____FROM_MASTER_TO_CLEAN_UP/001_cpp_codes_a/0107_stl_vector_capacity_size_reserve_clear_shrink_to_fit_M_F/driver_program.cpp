//================================//
// STL: vector                    //
// resize, shrink_to_fit, reserve //
// capacity                       //
//================================//

#include <iostream>
#include <vector>
#include <cmath>
#include <ctime>
#include <iomanip>

using namespace std;

int main()
{
    vector<double> vecA;
    long int i;
    const long int VEC_DIM = static_cast<long int>(pow(10.0, 8.1));
    const int COEF = 2;
    const double ZERO = static_cast<double>(0);
    int k;
    const int K_MAX = 1000000;
    clock_t t1;
    clock_t t2;

    cout << fixed;
    cout << setprecision(5);
    cout << showpoint;

    // 1. build the vector -- MAIN CODE

    for (k = 1; k <= K_MAX; k++) {
        cout << "-------------------------------------------------------------->>> " << k << endl;
        cout << endl;
        cout << "  1 --> Build the vector " << endl;

        t1 = clock();

        for (i = 1; i <= VEC_DIM; i++) {
            vecA.push_back(static_cast<double>(i));
        }

        t2 = clock();

        cout << "  2 --> Time used to build the vector ---------------------------->> "
             << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

        // 2. get the size and capacity of the vector

        cout << "  3 --> The size of the vector is         vecA.size() = " << vecA.size() << endl;
        cout << "  4 --> The capacity of the vector is vecA.capacity() = " << vecA.capacity() << endl;

        // 3. shrink to fit the size

        cout << "  5 --> Shrink to fit the size of the vector " << endl;

        vecA.shrink_to_fit();

        // 4. get the size and capacity of the vector

        cout << "  6 --> The size of the vector is         vecA.size() = " << vecA.size() << endl;
        cout << "  7 --> The capacity of the vector is vecA.capacity() = " << vecA.capacity() << endl;

        // 5. resize the vector

        cout << "  8 --> Resize the vector to be " << COEF << " times the old size" << endl;

        vecA.resize(COEF * VEC_DIM, ZERO);

        // 6. get the size and capacity of the vector

        cout << "  9 --> The size of the vector is         vecA.size() = " << vecA.size() << endl;
        cout << " 10 --> The capacity of the vector is vecA.capacity() = " << vecA.capacity() << endl;

        // 7. clear the vector

        cout << " 11 --> clear the vector" << endl;

        vecA.clear();

        // 8. get the size and capacity of the vector

        cout << " 12 --> The size of the vector is         vecA.size() = " << vecA.size() << endl;
        cout << " 13 --> The capacity of the vector is vecA.capacity() = " << vecA.capacity() << endl;

        // 9. build the vector again when the capacity is enough to hold the elements

        cout << " 14 --> Build the vector again and notice the speed" << endl;

        t1 = clock();

        for (i = 1; i <= VEC_DIM; i++) {
            vecA.push_back(static_cast<double>(i));
        }

        t2 = clock();

        cout << " 15 --> Time used to build the vector ---------------------------->> "
             << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

        // 10. clear the vector

        cout << " 16 --> clear the vector" << endl;

        vecA.clear();

        // 11. shrink to fit the size

        cout << " 17 --> Shrink to fit the size of the vector" << endl;

        vecA.shrink_to_fit();

        // 12. get the size and capacity of the vector

        cout << " 18 --> The size of the vector is         vecA.size() = " << vecA.size() << endl;
        cout << " 19 --> The capacity of the vector is vecA.capacity() = " << vecA.capacity() << endl;

        // 13. reserve space for the vector

        cout << " 20 --> Reserve space for the vector" << endl;

        vecA.reserve(VEC_DIM);

        // 14. get the size and capacity of the vector

        cout << " 21 --> The size of the vector is         vecA.size() = " << vecA.size() << endl;
        cout << " 22 --> The capacity of the vector is vecA.capacity() = " << vecA.capacity() << endl;

        // 15. build the vector again when the capacity is enough to hold the elements

        cout << " 23 --> Build the vector again and notice the speed" << endl;

        t1 = clock();

        for (i = 1; i <= VEC_DIM; i++) {
            vecA.push_back(static_cast<double>(i));
        }

        t2 = clock();

        cout << " 24 --> Time used to build the vector ---------------------------->> "
             << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

        // 16. get the size and capacity of the vector

        cout << " 25 --> The size of the vector is         vecA.size() = " << vecA.size() << endl;
        cout << " 26 --> The capacity of the vector is vecA.capacity() = " << vecA.capacity() << endl;

        // 17. clear the vector

        cout << " 27 --> clear the vector" << endl;

        vecA.clear();

        // 18. shrink to fit the size

        cout << " 28 --> Shrink to fit the size of the vector" << endl;

        vecA.shrink_to_fit();

        // 19. get the size and capacity of the vector

        cout << " 29 --> The size of the vector is         vecA.size() = " << vecA.size() << endl;
        cout << " 30 --> The capacity of the vector is vecA.capacity() = " << vecA.capacity() << endl;

        cout << endl;
    }
    // x. sentinel

    int sentinel;
    cin >> sentinel;

    return 0;
}

// FINI

