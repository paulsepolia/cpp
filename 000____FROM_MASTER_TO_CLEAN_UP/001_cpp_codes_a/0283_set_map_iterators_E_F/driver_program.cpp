
///=======================//
// set, map and iterators //
//========================//

#include <iostream>
#include <iomanip>
#include <set>
#include <map>
#include <cmath>
#include <iterator>
#include <vector>

using std::endl;
using std::cin;
using std::cout;
using std::fixed;
using std::showpos;
using std::showpoint;
using std::setprecision;
using std::set;
using std::map;
using std::pow;
using std::vector;
using std::pair;

// the main function

int main()
{
    // local variables and paramaters

    const long long DIM_MAX = static_cast<long long>(pow(10.0, 1.0));
    vector<double> vA;
    map<double,long long> mA;
    set<double> sA;

    // output

    cout << fixed;
    cout << showpoint;
    cout << showpos;
    cout << setprecision(20);

    // build the vector

    for (long long i = 0; i < DIM_MAX; i++) {
        vA.push_back(cos(static_cast<double>(i)));
    }

    // build the set

    for (long long i = 0; i < DIM_MAX; i++) {
        sA.insert(vA[i]);
    }

    // build the map

    for (long long i = 0; i < DIM_MAX; i++) {
        mA.insert(pair<double, long long>(vA[i], i));
    }

    map<double, long long>::iterator itm;

    cout << mA.begin()->first << endl;
    cout << mA.begin()->second << endl;
    cout << mA.begin()->first << endl;
    cout << mA.begin()->second << endl;
    cout << *sA.begin() << endl;
    cout << *sA.begin() << endl;

    itm = mA.begin();

    cout << itm->first << endl;
    cout << itm->second << endl;

    cout << endl << endl;

    for (itm = mA.begin(); itm != mA.end(); itm++) {
        cout << itm->first << " --> " << itm->second << endl;
    }

    cout << endl << endl;

    for (auto & x : mA) {
        cout << x.first << " --> " << x.second << endl;
    }

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

