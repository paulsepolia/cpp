//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/11/04              //
// Functions: array              //
// Algorithms: sort, size        //
//===============================//

#include <iostream>
#include <iomanip>
#include <array>
#include <cmath>
#include <algorithm>

using namespace std;

int main ()
{
    // variables declaration

    const long int DIM_AR = 20 * static_cast<long int>(10000000);
    const long int J_MAX = 100;
    array<double, DIM_AR> arrayA;
    array<double, DIM_AR> arrayB;
    long int i;
    long int j;

    // set the output style

    cout << fixed;
    cout << showpos;
    cout << showpoint;
    cout << setprecision(4);

    for (j = 0; j < J_MAX; j++) {
        cout << "------------------------------------------------------------------>> " << j+1 << endl;

        // build the array

        for (i = 0; i < DIM_AR; i++) {
            arrayA.at(i) = static_cast<double>(i+j);
            arrayB.at(i) = static_cast<double>(i+j+1);
        }

        // some outpout

        cout << " arrayA.at(0) = " << arrayA.at(0) << endl;
        cout << " arrayA.at(1) = " << arrayA.at(1) << endl;
        cout << " arrayB.at(0) = " << arrayB.at(0) << endl;
        cout << " arrayB.at(1) = " << arrayB.at(1) << endl;
        cout << " the size is: arrayA.size()  = " << arrayA.size() << endl;
        cout << " is empty is: arrayA.empty() = " << arrayA.empty() << endl;
        cout << " the size is: arrayB.size()  = " << arrayB.size() << endl;
        cout << " is empty is: arrayB.empty() = " << arrayB.empty() << endl;

        // build the array again

        for (i = 0; i < DIM_AR; i++) {
            arrayA.at(i) = cos(static_cast<double>(i+j));
            arrayB.at(i) = cos(static_cast<double>(i+j+1));
        }

        // some outpout

        cout << " arrayA.at(0) = " << arrayA.at(0) << endl;
        cout << " arrayA.at(1) = " << arrayA.at(1) << endl;
        cout << " arrayB.at(0) = " << arrayB.at(0) << endl;
        cout << " arrayB.at(1) = " << arrayB.at(1) << endl;
        cout << " the size is: arrayA.size()  = " << arrayA.size() << endl;
        cout << " is empty is: arrayA.empty() = " << arrayA.empty() << endl;
        cout << " the size is: arrayB.size()  = " << arrayB.size() << endl;
        cout << " is empty is: arrayB.empty() = " << arrayB.empty() << endl;

        // some output

        cout << " arrayA.max_size() = " << arrayA.max_size() << endl;
        cout << " arrayB.max_size() = " << arrayB.max_size() << endl;

        // fill the array with an element

        cout << " fill the array with a specific element: arrayA.fill(20.0); " << endl;
        cout << " fill the array with a specific element: arrayB.fill(30.0); " << endl;

        arrayA.fill(static_cast<double>(20.0));
        arrayB.fill(static_cast<double>(30.0));
        cout << " arrayA.at(0) = " << arrayA.at(0) << endl;
        cout << " arrayA.at(1) = " << arrayA.at(1) << endl;
        cout << " arrayB.at(0) = " << arrayB.at(0) << endl;
        cout << " arrayB.at(1) = " << arrayB.at(1) << endl;

        // swap the two arrays

        cout << " swap arrayA and arrayB" << endl;

        arrayA.swap(arrayB);

        cout << " arrayA.at(0) = " << arrayA.at(0) << endl;
        cout << " arrayA.at(1) = " << arrayA.at(1) << endl;
        cout << " arrayB.at(0) = " << arrayB.at(0) << endl;
        cout << " arrayB.at(1) = " << arrayB.at(1) << endl;

        // sorting the arrays

        cout << " sorting the two arrays" << endl;

        sort(arrayA.begin(), arrayA.end());
        sort(arrayB.begin(), arrayB.end());

        cout << " arrayA.at(0) = " << arrayA.at(0) << endl;
        cout << " arrayA.at(1) = " << arrayA.at(1) << endl;
        cout << " arrayB.at(0) = " << arrayB.at(0) << endl;
        cout << " arrayB.at(1) = " << arrayB.at(1) << endl;
    }

    return 0;
}

// end
