//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/10/23              //
// Functions: vector             //
//===============================//

#include <iostream>
#include <vector>

using namespace std;

// main function
int main()
{
    vector<int> v;
    int x;
    cout << "Enter positive integers, followed ny 0: " << endl;
    while(x != 0) {
        cin >> x;
        v.push_back(x);
    }

    vector<int>::iterator i;
    vector<int>::reverse_iterator j;

    cout << " forward going " << endl;

    for (i = v.begin(); i != v.end(); i++) {
        cout << *i << " --> ";
    }

    cout << endl;

    cout << " backward going " << endl;

    for (j = v.rbegin(); j != v.rend(); j++) {
        cout << *j << " <-- ";
    }

    cout << endl;

    return 0;
}
// end
