
//=========================//
// Headers : cmath, cctype //
//=========================//

//===============//
// code --> 0017 //
//===============//

// keywords: ceil, floor, toupper, tolower, right, isupper, islower

#include <iostream>
#include <iomanip>
#include <cmath>

using std::cout;
using std::cin;
using std::endl;
using std::fixed;
using std::setprecision;
using std::right;
using std::showpoint;
using std::pow;
using std::boolalpha;

// the main function

int main()
{

    cout << setprecision(10);
    cout << showpoint;
    cout << fixed;
    cout << right;
    cout << boolalpha;

    double x = 12.345678;
    int ix = 12;
    char ch1 = 'a';
    char ch2 = 'B';

    cout << " --> x         = " << x         << endl;
    cout << " --> ix        = " << ix        << endl;
    cout << " --> exp(x)    = " << exp(x)    << endl;
    cout << " --> cos(x)    = " << cos(x)    << endl;
    cout << " --> fabs(-x)  = " << fabs(-x)  << endl;
    cout << " --> abs(-ix)  = " << abs(-ix)  << endl;
    cout << " --> ceil(x)   = " << ceil(x)   << endl;
    cout << " --> ceil(-x)  = " << ceil(-x)  << endl;
    cout << " --> pow(x,ix) = " << pow(x,ix) << endl;
    cout << " --> floor(x)  = " << floor(x)  << endl;
    cout << " --> floor(-x) = " << floor(-x) << endl;
    cout << " --> sqrt(x)   = " << sqrt(x)   << endl;
    cout << " --> sqrt(ix)  = " << sqrt(ix)  << endl;
    cout << endl;
    cout << " --> static_cast<char>(toupper(ch1)) = " << static_cast<char>(toupper(ch1)) << endl;
    cout << " --> static_cast<char>(tolower(ch2)) = " << static_cast<char>(tolower(ch2)) << endl;
    cout << " --> isupper(ch1) = " << isupper(ch1) << endl;
    cout << " --> islower(ch1) = " << islower(ch1) << endl;
    cout << " --> isupper(ch2) = " << isupper(ch2) << endl;
    cout << " --> islower(ch2) = " << islower(ch2) << endl;

    cout << endl;

    cout << " --> end" << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    cout << " --> exit" << endl;

    return 0;
}

//======//
// FINI //
//======//

