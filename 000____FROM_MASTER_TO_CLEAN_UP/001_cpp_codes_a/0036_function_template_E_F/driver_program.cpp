//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/10/23              //
// Functions: Templates          //
//===============================//

#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

// function definition

template <class T>
T f(T& x);

// main function

int main()
{
    // variables

    int x1 = 10;
    double x2 = 10.0;
    long int x3 = 10;
    long long int x4 = 10;
    long double x5 = 10.0;

    // set the output form

    cout << fixed;
    cout << setprecision(30);
    cout << showpoint;
    cout << showpos;

    // outputs

    cout << " x1 = " << x1 << " --> f(x1) = " << f(x1) << endl;
    cout << " x2 = " << x2 << " --> f(x2) = " << f(x2) << endl;
    cout << " x3 = " << x3 << " --> f(x3) = " << f(x3) << endl;
    cout << " x4 = " << x4 << " --> f(x4) = " << f(x4) << endl;
    cout << " x5 = " << x5 << " --> f(x5) = " << f(x5) << endl;

    return 0;
}

// function declaration

template <class T>
T f(T& x)
{
    T y = 2 * x;

    T tmpVal;

    tmpVal = (y + (x*x + 1)/y) + sin((y + (x*x + 1)/y));

    return tmpVal;
}

// end
