//=======================================================//
// pass member function as argument to external function //
//=======================================================//

#include <iostream>

using std::cout;
using std::cin;
using std::endl;

// a class

class aClass {
public:
    void test(int a, int b)
    {
        cout << a << " , " << b << " , " << a+b << endl;
    }
};

// an external function

void functionExt(void (aClass::*function)(int, int), aClass & a)
{
    (a.*function)(2, 3);
}

// the main function

int main()
{
    aClass a; // note: no parentheses; with parentheses it is a function declaration

    functionExt(&aClass::test, a);

    return 0;
}

//======//
// FINI //
//======//
