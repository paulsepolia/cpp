//===========================================//
// Header file.                              //
// ListCL class member functions definition. //
//===========================================//

#include <iostream>

// 1. Constructor definition.

template <typename T, typename P>
ListCL<T,P>::ListCL():
    m_pHead(NULL)
{}

// 2. Destructor definition.

template <typename T, typename P>
ListCL<T,P>::~ListCL()
{
    ClearList();
}

// 3. AddElement.

template <typename T, typename P>
void ListCL<T,P>::AddElement(const T& elem)
{
    ElementCL<T>* pNewElement = new ElementCL<T>(elem);

    // if list is empty, make head of list this element
    if (m_pHead == NULL) {
        m_pHead = pNewElement;
    }
    // othewise fine the end of the list and add the element there
    else {
        ElementCL<T>* pIter = m_pHead;

        while ((pIter -> GetElementNext()) != NULL) {
            pIter = pIter -> GetElementNext();
        }

        pIter -> SetElementNext(pNewElement);
    }
}

// 4. LengthList.

template <typename T, typename P>
P ListCL<T,P>::LengthList()
{
    P len = 0;

    // if list is empty, make head of list this element
    if (m_pHead == NULL) {
        len = 0;
        return len;
    }
    // othewise find the end of the list and add the element there
    else {
        ElementCL<T>* pIter = m_pHead;

        len = 1; // there is at least one element

        while (pIter -> GetElementNext() != NULL) {
            len += 1;
            pIter = pIter -> GetElementNext();
        }

        return len;
    }
}

// 5. RemoveElement.

template <typename T, typename P>
void ListCL<T,P>::RemoveElement()
{
    if (m_pHead == NULL) {
        std::cout << " List is empty !" << endl;
    } else {
        ElementCL<T>* pElemTemp = m_pHead;
        m_pHead = m_pHead -> GetElementNext();
        pElemTemp = NULL;
    }
}

// 6. ClearList.

template <typename T, typename P>
void ListCL<T,P>::ClearList()
{
    while (m_pHead != NULL) {
        RemoveElement();
    }
}

// 7. GetListElement.

template <typename T, typename P>
T ListCL<T,P>::GetListElement(P index)
{
    T elem;
    P iL;

    // if list is empty return.
    if (m_pHead == NULL) {
        std::cout << " List is empty. Returned -1." << endl;
        elem = -1;
        return elem;
    }
    // othewise return the element at position index
    else {
        ElementCL<T>* pIter = m_pHead;

        iL = 1; // there is at least one element

        while (pIter -> GetElementNext() != NULL) {
            iL += 1;
            pIter = pIter -> GetElementNext();
            if (iL == index) {
                elem = (*pIter).GetElement();
                break;
            }
        }

        if (iL < index) {
            std::cout << " List is sorter than " << index << ". Returned -1." << endl;
            elem = -1;
            return elem;
        }
    }

    return elem;
}

// 8. SetListElement.

template <typename T, typename P>
T ListCL<T,P>::SetListElement(T elem, P index)
{
    P iL;

    // if list is empty return
    if (m_pHead == NULL) {
        std::cout << " List is empty. Returned -1." << endl;
        elem = -1;
        return elem;
    }
    // othewise return the element at position index
    else {
        ElementCL<T>* pIter = m_pHead;

        iL = 1; // there is at least one element

        while (pIter -> GetElementNext() != NULL) {
            iL += 1;
            pIter = pIter -> GetElementNext();
            if (iL == index) {
                (*pIter).SetElement(elem);
                break;
            }
        }

        if (iL < index) {
            std::cout << " List is sorter than " << index << ". Returned -1." << endl;
            elem = -1;
            return elem;
        }
    }
    return elem;
}

// 9. AddElement.

template <typename T, typename P>
void ListCL<T,P>::AddElement(const T& elem, P loc)
{
    ElementCL<T>* pNewElement = new ElementCL<T>(elem);
    ElementCL<T>* pIter = m_pHead;

    P i;

    for (i = 1; i < loc-1; i++) {
        pIter = pIter -> GetElementNext();
    }

    pIter -> SetElementNext(pNewElement);
}

//==============//
// End of code. //
//==============//
