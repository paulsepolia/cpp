//================================//
// Author: Pavlos G. Galiatsatos  //
// Date: 2013/10/23               //
// Functions: vector, deque       //
// Algorithms: sort               //
//================================//

#include <iostream>
#include <vector>
#include <deque>
#include <iomanip>
#include <ctime>
#include <algorithm>

using namespace std;

int main ()
{
    // variables declaration

    vector<double> aVector;
    deque<double> aDeque;
    double *p;
    long int i;
    long int j;
    const long int J_MAX_DO = 0.1 * static_cast<long int>(100);
    const long int MAX_ELEM = 4 * static_cast<long int>(100000000);
    clock_t t1;
    clock_t t2;

    // set up the output style

    cout << fixed;
    cout << setprecision(10);
    cout << showpos;
    cout << showpoint;

    // allocate an array with space
    // for MAX_LEM elements using vector's allocator

    cout << " vector --> allocate, construct, sort, destroy, deallocate " << endl;

    t1 = clock();

    for (j = 0; j < J_MAX_DO; j++) {
        p = aVector.get_allocator().allocate(MAX_ELEM);

        // construct values in-place on the array

        for (i = 0; i < MAX_ELEM; i++) {
            aVector.get_allocator().construct(&p[i], static_cast<double>(MAX_ELEM-i));
        }

        // some output

        cout << "The allocated vector contains (0,3):" << endl;

        for (i = 0; i < 3; i++) {
            cout << " --> " << i << " --> " << p[i] << endl;
        }

        // sort the vector

        sort(aVector.begin(), aVector.end());

        // some output after sort

        cout << "The sorted allocated vector contains (0,3):" << endl;

        for (i = 0; i < 3; i++) {
            cout << " --> " << i << " --> " << p[i] << endl;
        }

        // destroy element by element

        for (i = 0; i < MAX_ELEM; i++) {
            aVector.get_allocator().destroy(&p[i]);
        }

        // deallocate the vector RAM

        aVector.get_allocator().deallocate(p, MAX_ELEM);
    }

    t2 = clock();

    cout << " time for vector --> " << (t2-t1)/CLOCKS_PER_SEC << endl;

    cout << " deque --> allocate, construct, sort, destroy, deallocate " << endl;

    t1 = clock();

    for (j = 0; j < J_MAX_DO; j++) {
        p = aDeque.get_allocator().allocate(MAX_ELEM);

        // construct values in-place on the array

        for (i = 0; i < MAX_ELEM; i++) {
            aDeque.get_allocator().construct(&p[i], static_cast<double>(MAX_ELEM-i));
        }

        // some output

        cout << "The allocated deque contains (0,3):" << endl;

        for (i = 0; i < 3; i++) {
            cout << " --> " << i << " --> " << p[i] << endl;
        }

        // sort the deque

        sort(aDeque.begin(), aDeque.end());

        // some output after sort

        cout << "The sorted allocated deque contains (0,3):" << endl;

        for (i = 0; i < 3; i++) {
            cout << " --> " << i << " --> " << p[i] << endl;
        }

        // destroy element by element

        for (i = 0; i < MAX_ELEM; i++) {
            aDeque.get_allocator().destroy(&p[i]);
        }

        // deallocate the vector RAM

        aDeque.get_allocator().deallocate(p, MAX_ELEM);
    }

    t2 = clock();

    cout << " time for deque --> " << (t2-t1)/CLOCKS_PER_SEC << endl;

    return 0;
}

// end
