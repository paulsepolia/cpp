//=====================//
// define preprocessor //
//=====================//

#include <iostream>

using std::cout;
using std::endl;

#define SWAP(a, b)  { a ^= b; b ^= a; a ^= b; }

int main()
{
    int x = 10;
    int y = 5;
    int z = 4;

    // What happens now?

    if(x > 0) {
        cout << " before --> x = " << x << endl;
        cout << " before --> y = " << y << endl;

        SWAP(x, y);

        cout << " after --> x = " << x << endl;
        cout << " after --> y = " << y << endl;
    }

    if(x >= 0) {
        cout << " before --> x = " << x << endl;
        cout << " before --> z = " << z << endl;

        SWAP(x, z);

        cout << " after --> x = " << x << endl;
        cout << " after --> z = " << z << endl;
    }

    // end

    return 0;
}

// FINI
