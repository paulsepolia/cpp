
//==============================//
// definition of class PFArrayD //
//==============================//

#include <iostream>
#include <cstdlib>
#include "pfarrayd.h"

using std::cout;
using std::cin;
using std::endl;
using std::exit;

namespace pgg {
// default constructor

PFArrayD::PFArrayD() : capacity(50), used(0) //, a(NULL)
{
    a = new double [capacity];
}

// constructor with one argument

PFArrayD::PFArrayD(int size) : capacity(size), used(0) //, a(NULL)
{
    a = new double [capacity];
}

// member function

int PFArrayD::getCapacity() const
{
    return capacity;
}

// member function

bool PFArrayD::full() const
{
    return (capacity == used);
}

// member function

int PFArrayD::getNumberUsed() const
{
    return used;
}

// member function

void PFArrayD::emptyArray()
{
    used = 0;
}

// member function

void PFArrayD::addElement(double element)
{
    if (used >= capacity) {
        cout << "Attempt to exceed capacity in PFArrayD.";
        cout << " Enter an integer to terminate the program: " << endl;
        int sentinel;
        cin >> sentinel;
        exit(0);
    }

    a[used] = element;
    used++;
}

// copy constructor

PFArrayD::PFArrayD(const PFArrayD& pfaObject)
    : capacity(pfaObject.getCapacity()), used(pfaObject.getNumberUsed()) //, a(NULL)
{
    a = new double [capacity];

    for (int i = 0; i < used; i++) {
        a[i] = pfaObject.a[i];
    }
}

// operator [] overload

double & PFArrayD::operator[](int index)
{
    if (index >= used) {
        cout << "Illegal index in PFArrayD. Enter an integer to exit" << endl;
        int sentinel;
        cin >> sentinel;
        exit(1);
    }

    return a[index];
}

// operator = overload

PFArrayD& PFArrayD::operator=(const PFArrayD& rightSide)
{
    if (capacity != rightSide.capacity) {
        delete [] a;
        a = new double [rightSide.capacity];
    }

    capacity = rightSide.capacity;
    used = rightSide.used;

    for (int i = 0; i < used; i++) {
        a[i] = rightSide.a[i];
    }

    return *this;
}

// default destructor

PFArrayD::~PFArrayD()
{
    delete [] a;
}

} // pgg

//======//
// FINI //
//======//

