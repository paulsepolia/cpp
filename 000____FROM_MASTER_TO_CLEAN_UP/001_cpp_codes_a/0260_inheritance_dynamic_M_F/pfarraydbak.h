
//======================================//
// declaration of the class PFArrayDBak //
//======================================//

#ifndef PFARRAYDBAK_H
#define PFARRAYDBAK_H

#include "pfarrayd.h"

namespace pgg {
class PFArrayDBak : public PFArrayD {
public:

    // default constructor

    PFArrayDBak(); // initializes with a capacity of 50

    // constructor with one argument

    explicit PFArrayDBak(int);

    // copy constructor

    PFArrayDBak(const PFArrayDBak&);

    // member functions

    void backup(); // makes a backup copy of the partially filled array
    void restore(); // restores the partially filled array to the last saved version.
    // if backup has never been invoked, this empties the partially filled array.

    // = operator overload

    PFArrayDBak & operator =(const PFArrayDBak &);

    // destructor

    virtual ~PFArrayDBak();

private:
    double *b; // for a backup of main array
    int usedB; // backup for inherited variable used
};

} // pgg

#endif // PFARRAYDBAK_H

//======//
// FINI //
//======//

