
#include <iostream>
#include <cmath>
#include "pfarraydbak.h"

using std::cin;
using std::cout;
using std::endl;
using std::pow;
using pgg::PFArrayD;
using pgg::PFArrayDBak;

// the main function

int main()
{
    const int CAPA_A = static_cast<int>(pow(10.0, 7.9));
    const int K_MAX = 1000000;

    for (int k = 0; k < K_MAX; k++) {
        cout << "------------------------------------------------->> " << k << endl;

        cout << " --> define the object" << endl;

        PFArrayDBak a(CAPA_A);
        PFArrayDBak b;

        cout << " --> empty the object" << endl;

        a.emptyArray();

        // build the array

        cout << " --> build the object" << endl;

        for (int i = 0; i < CAPA_A; i++) {
            a.addElement(i);
        }

        cout << " a.getNumberUsed() = " << a.getNumberUsed() << endl;
        cout << " a.getCapacity() = " << a.getCapacity() << endl;

        // backup array

        cout << " --> backup the object" << endl;

        a.backup();

        // empty the array.

        a.emptyArray();

        cout << " a.getNumberUsed() = " << a.getNumberUsed() << endl;
        cout << " a.getCapacity() = " << a.getCapacity() << endl;

        // restore the array

        cout << " --> restore the object" << endl;

        a.restore();

        cout << " a.getNumberUsed() = " << a.getNumberUsed() << endl;
        cout << " a.getCapacity() = " << a.getCapacity() << endl;

        cout << " --> a[1] = " << a[1] << endl;
        cout << " --> a[2] = " << a[2] << endl;

        cout << " --> set b = a" << endl;

        b = a;

        b.emptyArray();

    }

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

