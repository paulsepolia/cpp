
//=======================//
// CLAPTR driver program //
//=======================//

#include <iostream>
#include <iomanip>
#include <cmath>
#include "CLAPTR_DECL.h"
#include "CLAPTR_DEF.h"

using std::cout;
using std::cin;
using std::endl;
using std::fixed;
using std::showpos;
using std::showpoint;
using std::setprecision;
using std::boolalpha;
using std::pow;
using std::cos;

// the main function

int main()
{
    // local variables and parameters

    const long DIM_MAX = static_cast<long>(pow(10.0, 8.0));

    // adjust the output format

    cout << fixed;
    cout << setprecision(20);
    cout << showpos;
    cout << showpoint;
    cout << boolalpha;

    // 1

    cout << "-------------------------------------->> 1" << endl;

    CLAPTR pA;
    CLAPTR pB;

    cout << pA.getSize() << endl;
    pA.printSize();
    pA.reset();
    cout << pA.getSize() << endl;
    pA.printSize();
    cout << endl;

    // 2

    cout << "-------------------------------------->> 2" << endl;

    pA.setSize(1000000);
    pA.printSize();
    cout << pA.getSize() << endl;
    pA.reset();
    pA.printSize();
    cout << pA.getSize() << endl;

    // 3

    for (int j = 0; j < 10; j++) {
        cout << "-------------------------------------->> " << j << endl;

        pA.setSize(DIM_MAX);

        for (long i = 0; i < DIM_MAX; i++) {
            pA.setElement(i, cos(i));
        }

        pB = pA; // test the = operator
        CLAPTR pC(pA); // test the copy constructor

        pA.printElem(0);
        pB.printElem(0);
        pC.printElem(0);

        pA.printElem(1);
        pB.printElem(1);
        pC.printElem(1);

        pA.printElem(2);
        pB.printElem(2);
        pC.printElem(2);

        pA.printElem(3);
        pB.printElem(3);
        pC.printElem(3);

        pA.printElem(4);
        pB.printElem(4);
        pC.printElem(4);

        pA.reset();
        pB.reset();
        pC.reset();
    }
    // sentineling

    int sentinel;
    cin >> sentinel;

    // at the end I test the default destructor ! it wokrs

    return 0;
}

//======//
// FINI //
//======//

