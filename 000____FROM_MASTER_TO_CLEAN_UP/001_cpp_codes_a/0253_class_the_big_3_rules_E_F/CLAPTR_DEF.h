
#ifndef CLAPTR_DEF_H
#define CLAPTR_DEF_H

#include <iostream>
#include "CLAPTR_DECL.h"

using std::cout;
using std::endl;

//===================//
// CLAPTR definition //
//===================//

// default constructor

CLAPTR::CLAPTR() : a (NULL), dim (0) {}

// one argument constructor

CLAPTR::CLAPTR(long size) : a (NULL), dim (size)
{
    a = new double [dim];
}

// two argument constructor

CLAPTR::CLAPTR(long size, double elem) : a (NULL), dim (size)
{
    a = new double [dim];

    for (long i = 0; i != dim; i++) {
        a[i] = elem;
    }
}

// member function

void CLAPTR::setElement(long index, double elem)
{
    a[index] = elem;
}

// member function

double CLAPTR::getElem(long index) const
{
    return a[index];
}

// member function

void CLAPTR::printElem(long index) const
{
    cout << a[index] << endl;
}

// member function

long CLAPTR::getSize() const
{
    return dim;
}

// member function

void CLAPTR::printSize() const
{
    cout << dim << endl;
}

// member function

void CLAPTR::setSize(long size)
{
    if (a == NULL) {
        a = new double [size];
    } else {
        delete a;
        a = new double [size];
    }

    dim = size;
}

// member function

void CLAPTR::reset()
{
    delete [] a;
    a = NULL;
    dim = 0;
    a = new double;
}

// copy constructor

CLAPTR::CLAPTR(const CLAPTR & obj) : a (NULL), dim (obj.getSize())
{
    a = new double [dim];

    for (long i = 0; i < dim; i++) {
        a[i] = obj.getElem(i);
    }
}

// = operator overload

CLAPTR & CLAPTR::operator = (const CLAPTR & obj)
{
    cout << " --> dim = " << dim << endl;
    cout << " --> obj.dim = " << obj.dim << endl;

    if (dim != obj.dim) {
        delete [] a;
        a = new double [obj.dim];
    }

    dim = obj.dim;

    for (long i = 0; i < obj.dim; i++) {
        a[i] = obj.a[i];
    }

    return *this;

}

// default destructor

CLAPTR::~CLAPTR()
{
    delete [] a;
    a = NULL;
}

//======//
// FINI //
//======//

#endif // CLAPTR_DEF_H

