
//======================//
// pet class definition //
//======================//

#include <iostream>
#include "pet.h"

using std::cout;
using std::endl;

namespace pgg {
// constructor

template <class T>
Pet<T>::Pet() : name("No name yet"), price(0) {}

// member function

template <class T>
void Pet<T>::print() const
{
    cout << "name: " << name << endl;
    cout << "price: " << price << endl;
}

// destructor

template <class T>
Pet<T>::~Pet() {}

} // pgg

//======//
// FINI //
//======//

