//==========================//
// STL, nth_element example //
//==========================//

#include <iostream>
#include <algorithm>
#include <vector>
#include <deque>
#include <iomanip>
#include <cmath>
#include <ctime>
#include <cassert>
#include <iterator>

using std::cout;
using std::cin;
using std::endl;

using std::binary_search;
using std::find;

using std::vector;
using std::deque;

using std::fixed;
using std::showpoint;
using std::showpos;
using std::setprecision;

using std::pow;

// the main function

int main()
{
    // local variables and parameters

    const long long DIM_MAX = static_cast<long long>(pow(10.0, 8.0));
    const long long K_MAX = static_cast<long long>(pow(10.0, 5.0));
    const long long FIND_MAX = static_cast<long long>(pow(10.0, 5.0));
    clock_t t1;
    clock_t t2;
    bool found;
    vector<double>::iterator itv;
    deque<double>::iterator itd;
    double * pd;

    // adjust the output

    cout << fixed;
    cout << setprecision(20);
    cout << showpos;
    cout << showpoint;

    // for loop

    for (long long k = 0; k != K_MAX; k++) {
        // the counter

        cout << "------------------------------------------------->> " << k << endl;

        vector<double> * vA = new vector<double>;
        deque<double> * dA = new deque<double>;
        double * aA = new double [DIM_MAX];

        //
        //
        // build the vector

        cout << " --> build the vector" << endl;

        t1 = clock();

        for (long long i = 0; i != DIM_MAX; i++) {
            vA->push_back(cos(static_cast<double>(i)));
        }

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // print out some contents

        cout << " --> (*vA)[0]        = " << (*vA)[0] << endl;
        cout << " --> (*vA)[1]        = " << (*vA)[1] << endl;
        cout << " --> (*vA)[2]        = " << (*vA)[2] << endl;
        cout << " --> (*vA)[3]        = " << (*vA)[3] << endl;
        cout << " --> (*vA)[4]        = " << (*vA)[4] << endl;

        // binary search

        cout << " --> binary_search --> vector" << endl;

        t1 = clock();

        for (long long i = 0; i != DIM_MAX; i++) {
            found = binary_search(vA->begin(), vA->end(), cos(static_cast<double>(i)));
            assert(found == true);
        }

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // find

        cout << " --> find --> vector" << endl;

        t1 = clock();

        for (long long i = 0; i != FIND_MAX; i++) {
            itv = find(vA->begin(), vA->end(), cos(static_cast<double>(i)));
            assert(itv != vA->end());
        }

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        //
        //
        // build the deque

        cout << " --> build the deque" << endl;

        t1 = clock();

        for (long long i = 0; i != DIM_MAX; i++) {
            dA->push_back(cos(static_cast<double>(i)));
        }

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // print out some contents

        cout << " --> (*dA)[0]        = " << (*dA)[0] << endl;
        cout << " --> (*dA)[1]        = " << (*dA)[1] << endl;
        cout << " --> (*dA)[2]        = " << (*dA)[2] << endl;
        cout << " --> (*dA)[3]        = " << (*dA)[3] << endl;
        cout << " --> (*dA)[4]        = " << (*dA)[4] << endl;

        // binary search

        cout << " --> binary_search --> deque" << endl;

        t1 = clock();

        for (long long i = 0; i != DIM_MAX; i++) {
            found = binary_search(dA->begin(), dA->end(), static_cast<double>(i));
            assert(found == true);
        }

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // find

        cout << " --> find --> deque" << endl;

        t1 = clock();

        for (long long i = 0; i != FIND_MAX; i++) {
            itd = find(dA->begin(), dA->end(), cos(static_cast<double>(i)));
            assert(itd != dA->end());
        }

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;


        //
        //
        // build the array

        cout << " --> build the array" << endl;

        t1 = clock();

        for (long long i = 0; i != DIM_MAX; i++) {
            aA[i] = cos(static_cast<double>(i));
        }

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // print out some contents

        cout << " --> aA[0]        = " << aA[0] << endl;
        cout << " --> aA[1]        = " << aA[1] << endl;
        cout << " --> aA[2]        = " << aA[2] << endl;
        cout << " --> aA[3]        = " << aA[3] << endl;
        cout << " --> aA[4]        = " << aA[4] << endl;

        // binary search

        cout << " --> binary_search --> array" << endl;

        t1 = clock();

        for (long long i = 0; i != DIM_MAX; i++) {
            found = binary_search(&aA[0], &aA[DIM_MAX], cos(static_cast<double>(i)));
            assert(found == true);
        }

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        // find

        cout << " --> find --> array" << endl;

        t1 = clock();

        for (long long i = 0; i != FIND_MAX; i++) {
            pd = find(&aA[0], &aA[DIM_MAX], cos(static_cast<double>(i)));
            assert(pd != &aA[DIM_MAX]);
        }

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;


        // delete the containers

        cout << " --> delete the array" << endl;

        t1 = clock();

        delete [] aA;

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        cout << " --> delete the vector" << endl;

        t1 = clock();

        delete vA;

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;

        cout << " --> delete the deque" << endl;

        t1 = clock();

        delete dA;

        t2 = clock();

        cout << " --> time used = " << (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC) << endl;
    }

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

