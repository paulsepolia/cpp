//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/10/13              //
// Functions: array              //
//===============================//

#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

int main()
{
    const int DIM_MAX = 100;
    const int K_MAX = 10000000;
    int i;
    int k;
    double* array_a = new double [DIM_MAX];

    cout << fixed;
    cout << showpoint;
    cout << setprecision(20);
    cout << showpos;

    for (i = 0; i < DIM_MAX; i++) {
        array_a[i] = cos(i);
    }

    cout << " 1 --> " << array_a[0] << endl;
    cout << " 2 --> " << array_a[1] << endl;
    cout << " 3 --> " << array_a[2] << endl;
    cout << " 4 --> " << array_a[3] << endl;
    cout << " 5 --> " << array_a[4] << endl;
    cout << " 6 --> " << array_a[5] << endl;
    cout << " x --> " << array_a[DIM_MAX-1] << endl;
    cout << endl;

    for (i = 0; i < DIM_MAX; i++) {
        array_a[i] = cos(array_a[i]);
    }

    cout << " 1 --> " << array_a[0] << endl;
    cout << " 2 --> " << array_a[1] << endl;
    cout << " 3 --> " << array_a[2] << endl;
    cout << " 4 --> " << array_a[3] << endl;
    cout << " 5 --> " << array_a[4] << endl;
    cout << " 6 --> " << array_a[5] << endl;
    cout << " x --> " << array_a[DIM_MAX-1] << endl;
    cout << endl;

    for (i = 0; i < DIM_MAX; i++) {
        array_a[i] = cos(array_a[i]);
    }

    cout << " 1 --> " << array_a[0] << endl;
    cout << " 2 --> " << array_a[1] << endl;
    cout << " 3 --> " << array_a[2] << endl;
    cout << " 4 --> " << array_a[3] << endl;
    cout << " 5 --> " << array_a[4] << endl;
    cout << " 6 --> " << array_a[5] << endl;
    cout << " x --> " << array_a[DIM_MAX-1] << endl;
    cout << endl;

    for (i = 0; i < DIM_MAX; i++) {
        array_a[i] = sin(sin(sin(sin(array_a[i]))));
    }

    cout << " 1 --> " << array_a[0] << endl;
    cout << " 2 --> " << array_a[1] << endl;
    cout << " 3 --> " << array_a[2] << endl;
    cout << " 4 --> " << array_a[3] << endl;
    cout << " 5 --> " << array_a[4] << endl;
    cout << " 6 --> " << array_a[5] << endl;
    cout << " x --> " << array_a[DIM_MAX-1] << endl;
    cout << endl;

    for (k = 0; k < K_MAX; k++) {
        for (i = 0; i < DIM_MAX; i++) {
            array_a[i] = sin(sin(sin(sin(array_a[i]))));
        }
    }

    cout << " 1 --> " << array_a[0] << endl;
    cout << " 2 --> " << array_a[1] << endl;
    cout << " 3 --> " << array_a[2] << endl;
    cout << " 4 --> " << array_a[3] << endl;
    cout << " 5 --> " << array_a[4] << endl;
    cout << " 6 --> " << array_a[5] << endl;
    cout << " x --> " << array_a[DIM_MAX-1] << endl;
    cout << endl;

    for (k = 0; k < K_MAX; k++) {
        for (i = 0; i < DIM_MAX; i++) {
            array_a[i] = sin(sin(sin(sin(array_a[i]))));
        }
    }

    cout << " 1 --> " << array_a[0] << endl;
    cout << " 2 --> " << array_a[1] << endl;
    cout << " 3 --> " << array_a[2] << endl;
    cout << " 4 --> " << array_a[3] << endl;
    cout << " 5 --> " << array_a[4] << endl;
    cout << " 6 --> " << array_a[5] << endl;
    cout << " x --> " << array_a[DIM_MAX-1] << endl;
    cout << endl;

    return 0;
}

//======//
// FINI //
//======//
