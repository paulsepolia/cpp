//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/10/13              //
// Functions: Class              //
//===============================//

#include <iostream>

using namespace std;

// A. classes declaration

// 1. clockTypoe declaration

class clockType {
public:

    void setTime(int, int, int);
    void getTime(int&, int&, int&) const;
    void printTime() const;
    void incrementSeconds();
    void incrementMinutes();
    void incrementHours();
    bool equalTime(const clockType&) const;

private:

    int hr;
    int min;
    int sec;
};

// B. The main function

int main()
{
    // 1. varaible declaration

    clockType myClock;
    clockType yourClock;
    int hours;
    int minutes;
    int seconds;

    // 2. get the time from the clock

    cout << " --> myClock.printTime() = ";
    myClock.printTime();
    cout << endl;
    cout << " --> yourClock.printTime() = ";
    yourClock.printTime();
    cout << endl;

    // 3. set the time of myClock and of yourClock

    myClock.setTime(5, 4, 30);
    yourClock.setTime(5, 45, 16);

    cout << " --> myClock.printTime() = ";
    myClock.printTime();
    cout << endl;
    cout << " --> yourClock.printTime() = ";
    yourClock.printTime();
    cout << endl;

    // 4. compare myClock and yourClock

    if (myClock.equalTime(yourClock)) {
        cout << "Both times are equal." << endl;
    } else {
        cout << "The two times are not equal." << endl;
    }

    // 5. enter new values

    cout << "Enter the hours, minutes, and seconds: ";
    cin >> hours >> minutes >> seconds;
    cout << endl;

    // 6. set the timr of myClock again

    myClock.setTime(hours, minutes, seconds);

    cout << "New myClock: ";
    myClock.printTime();
    cout << endl;

    myClock.incrementSeconds();

    myClock.printTime();
    cout << endl;

    myClock.getTime(hours, minutes, seconds);

    cout << " hours   = " << hours << endl;
    cout << " minutes = " << minutes << endl;
    cout << " seconds = " << seconds << endl;

    return 0;
}

// C. Class member functions definition

// 1.

void clockType::setTime(int hours, int minutes, int seconds)
{
    if (0 <= hours && hours < 24) {
        hr = hours;
    } else {
        hr = 0;
    }

    if (0 <= minutes && minutes < 60) {
        min = minutes;
    } else {
        min = 0;
    }

    if (0 <= seconds && seconds < 60) {
        sec = seconds;
    } else {
        sec = 0;
    }
}

// 2.

void clockType::getTime(int& hours, int& minutes, int& seconds) const
{
    hours = hr;
    minutes = min;
    seconds = sec;
}

// 3.

void clockType::printTime() const
{
    if (hr < 10) {
        cout << "0";
    }
    cout << hr << ":";

    if (min < 10) {
        cout << "0";
    }
    cout << min << ":";

    if (sec < 10) {
        cout << "0";
    }
    cout << sec;
}

// 4.

void clockType::incrementHours()
{
    hr++;
    if (hr > 23) {
        hr = 0;
    }
}

// 5.

void clockType::incrementMinutes()
{
    min++;
    if (min > 59) {
        min = 0;
        incrementHours();
    }
}

// 6.

void clockType::incrementSeconds()
{
    sec++;

    if (sec > 59) {
        sec = 0;
        incrementMinutes();
    }
}

// 7.

bool clockType::equalTime(const clockType& otherClock) const
{
    return (hr  == otherClock.hr  &&
            min == otherClock.min &&
            sec == otherClock.sec);
}

//======//
// FINI //
//======//
