//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/10/13              //
// Functions: c string           //
//===============================//

#include <iostream>
#include <cstring>
#include <fstream>

using namespace std;

int main()
{
    const int FN_LEN = 100;
    char fileNameOut[FN_LEN];
    char fileNameIn[FN_LEN];
    char discard;

    cout << "Enter the file name to read from: ";
    cin.get(fileNameIn, FN_LEN);
    cin.get(discard);
    cout << "I am reading from: " << fileNameIn << endl;

    cout << "Enter the file name to write to: ";
    cin.get(fileNameOut, FN_LEN);
    cin.get(discard);
    cout << "I am writting to: " << fileNameOut << endl;

    return 0;
}

//======//
// FINI //
//======//
