
//===========================//
// using standard exceptions //
//===========================//

#include <iostream>
#include <exception>
#include <cmath>
#include <vector>
#include <thread>

using std::exception;
using std::endl;
using std::cout;
using std::pow;
using std::vector;
using std::thread;

// hand made exception class
// I use the base class: exception
// and I redefine the virtual function what
// as you can see I have put many new stuff there

class myexception: public exception {
    virtual const char* what() const throw()
    {
        const long DIM_MAX = static_cast<long>(pow(10.0, 8.2));

        const long I_MAX = 10;

        for (long i = 0; i < I_MAX; i++) {
            double * a = new double [DIM_MAX];

            for (long j = 0; j < DIM_MAX; j++) {
                a[j] = cos(static_cast<double>(j));
                //if (a[j] == 0.1) cout << "MOU" << endl;
            }

            delete [] a;
        }

        return "My exception happened";
    }
};

// the bench function

void benchFun ()
{
    myexception myex1;
    myexception myex2;

    const int I_MAX = 10000000;

    for (int i = 0; i < I_MAX; i++) {
        try {
            throw myex1;
        } catch (exception& e) {
            cout << " --> " << i << " --> " << e.what() << endl;
        }
    }

    try {
        throw myex2;
    } catch (exception& e) {
        cout << " --> 2 --> " << e.what() << endl;
    }
}

// the main function

int main()
{
    vector<thread> threadsVec;
    const int NUM_THREADS = 4;

    for (int i = 0; i < NUM_THREADS; i++) {
        threadsVec.push_back(thread(&benchFun));
    }

    for ( auto & t : threadsVec) {
        t.join();
    }

    threadsVec.clear();
}

//======//
// FINI //
//======//
