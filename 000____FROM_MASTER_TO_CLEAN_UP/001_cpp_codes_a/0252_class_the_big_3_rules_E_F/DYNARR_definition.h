
#ifndef DYNARR_DEF_H
#define DYNARR_DEF_H

#include <cstdlib>
#include "DYNARR_declaration.h"

using std::cout;
using std::endl;
using std::exit;

// default constructor definition

DYNARR::DYNARR() : a (NULL), capacity (100), used (0)
{
    a = new double [capacity];
}

// constructor with one argument definition

DYNARR::DYNARR(int size) : a (NULL), capacity (size), used (0)
{
    a = new double [capacity];
}

// member function definition

int DYNARR::getCapacity() const
{
    return capacity;
}

// member function definition

int DYNARR::getNumberUsed() const
{
    return used;
}

// member function definition

bool DYNARR::full() const
{
    return (capacity == used);
}
// member function definition

void DYNARR::emptyArray()
{
    used = 0;
}

// copy constructor definition

DYNARR::DYNARR(const DYNARR & objA) : a (NULL),
    capacity (objA.getCapacity()),
    used (objA.getNumberUsed())
{
    a = new double [capacity];

    for (int i = 0; i < used; i++) {
        a[i] = objA.a[i];
    }
}

// member function definition

void DYNARR::addElement(double elem)
{
    if (used >= capacity) {
        cout << "Attempt to exceed capacity in DYNARR." << endl;
        exit(1);
    }

    a[used] = elem;
    used++;
}

// [] operator overload as member function

double & DYNARR::operator[] (int index)
{
    if (index >= used) {
        cout << "Illegal index in DYNARR." << endl;
        exit(1);
    }

    return a[index];
}

// = operator overload as member function

DYNARR & DYNARR::operator = (const DYNARR & rhs)
{
    if (capacity != rhs.capacity) {
        delete [] a;
        a = new double [rhs.capacity];
    }

    capacity = rhs.capacity;

    used = rhs.used;

    for (int i = 0; i < used; i++) {
        a[i] = rhs.a[i];
    }

    return *this;
}

// default destructor

DYNARR::~DYNARR()
{
    delete [] a;
}

//======//
// FINI //
//======//

#endif // DYNARR_DEF_H
