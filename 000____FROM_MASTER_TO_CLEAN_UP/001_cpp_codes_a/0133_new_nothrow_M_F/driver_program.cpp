//==============//
// new operator //
//==============//

#include <iostream>
#include <new>

// it is supposed that if the memory allocation goes wrong
// then the pointer pa is set to nullptr and the program continues normally

int main()
{
    long double * pa;
    long dimen;
    std::cin >> dimen;
    pa = new (std::nothrow) long double [dimen];

    if (pa == NULL) {
        std::cout << "Error assigning memory!" << std::endl;
    }

    int sentinel;
    std::cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

