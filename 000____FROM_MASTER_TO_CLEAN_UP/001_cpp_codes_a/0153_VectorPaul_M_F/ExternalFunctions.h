//====================//
// External Functions //
//====================//

#include "IncludeLibs.h"

template <typename T>
void benchFun(VectorPaul<T> & obj, long dim)
{
    long i;

    // 1.

    cout << " --> obj.setDim(dim);" << endl;
    obj.setDim(dim);

    // 2.

    cout << " --> obj.allocateVector();" << endl;
    obj.allocateVector();

    // 3.

    cout << " --> obj.setVector(j, val);" << endl;

    for (i = 0; i < dim; i++) {
        double val = static_cast<double>(i);
        obj.setVector(i, val);
    }

    // 4.

    cout << " --> obj.getVector(0L) = " << obj.getVector(0L)  << endl;
    cout << " --> obj.getVector(1L) = " << obj.getVector(1L)  << endl;
    cout << " --> obj.getVector(2L) = " << obj.getVector(2L)  << endl;

    // 5.

    cout << " --> obj.reverseVector();" << endl;
    obj.reverseVector();

    // 6.

    cout << " --> obj.getVector(0L) = " << obj.getVector(0L)  << endl;
    cout << " --> obj.getVector(1L) = " << obj.getVector(1L)  << endl;
    cout << " --> obj.getVector(2L) = " << obj.getVector(2L)  << endl;

    // 7.

    cout << " --> obj.setVector(j, val);" << endl;

    for (i = 0; i < dim; i++) {
        double val = cos(static_cast<double>(i));
        obj.setVector(i, val);
    }

    // 8.

    cout << " --> obj.getVector(0L) = " << obj.getVector(0L)  << endl;
    cout << " --> obj.getVector(1L) = " << obj.getVector(1L)  << endl;
    cout << " --> obj.getVector(2L) = " << obj.getVector(2L)  << endl;

    // 10.

    cout << " --> obj.sortVector();" << endl;
    obj.sortVector();

    // 11.

    cout << " --> obj.IsSortedVector() = " << obj.IsSortedVector() << endl;

    // 12.

    for (i = 0 ; i < 10L; i++)
        cout << " --> obj.getVector(" << i << ") = " << obj.getVector(i) << endl;

    // 13.

    cout << " --> obj.randomShuffleVector();" << endl;
    obj.randomShuffleVector();

    // 14.

    for (i = 0 ; i < 10L; i++)
        cout << " --> obj.getVector(" << i << ") = " << obj.getVector(i) << endl;

    // 15.

    cout << " --> obj.maxElementVector() = " << obj.maxElementVector() << endl;

    // 16.

    cout << " --> obj.minElementVector() = " << obj.minElementVector() << endl;

    // 17.

    cout << " --> obj.getDim() = " << obj.getDim() << endl;

    // 18.

    cout << " --> obj.fillVector(10);" << endl;
    double val = 10.0;
    obj.fillVector(val);

    // 19.

    cout << " --> obj.maxElementVector() = " << obj.maxElementVector() << endl;

    // 20.

    cout << " --> obj.minElementVector() = " << obj.minElementVector() << endl;

    // 21.

    cout << " --> obj.deleteVector();" << endl;
    obj.deleteVector();
}

//======//
// FINI //
//======//