
//===================//
// Constructs: while //
//===================//

//===============//
// code --> 0013 //
//===============//

// keywords: while, clock(), clock_t, CLOCKS_PER_SEC

#include <iostream>
#include <iomanip>
#include <cmath>
#include <ctime>

using std::cout;
using std::cin;
using std::endl;
using std::fixed;
using std::showpos;
using std::showpoint;
using std::setprecision;

// the main function

int main()
{
    // local variables and parameters

    cout << " --> 1" << endl;

    const long long int I_MAX  = static_cast<long long int>(pow(10.0, 10.0));
    const long long int I_STEP = 1LL;
    const long double A_SUM = static_cast<long double>(1.0);
    long int i;
    long double sum;
    clock_t t1;
    clock_t t2;

    // adjust the output format

    cout << " --> 2" << endl;

    cout << fixed;
    cout << showpoint;
    cout << setprecision(5);
    cout << showpos;

    // initialize clock and counters

    cout << " --> 3" << endl;

    cout << " --> while construct execution..." << endl;

    i = 0LL;
    sum = 0.0;
    t1 = clock();

    while (i <= I_MAX) {
        i = i + I_STEP;
        sum = sum + A_SUM;
    }

    t2 = clock();

    cout << " --> 4 --> end" << endl;

    cout << " --> sum       = " << sum << endl;
    cout << " --> i         = " << i << endl;
    cout << " --> time used = " << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    cout << " --> 5 --> exit" << endl;

    return 0;
}

//======//
// FINI //
//======//

