//=================================//
// Header file.                    //
// DoublePairCL class declaration. //
//=================================//

class DoublePairCL {
public:
    DoublePairCL()
    {
        /* Body intentionally empty. */
    }
    DoublePairCL(double firstValue, double secondValue)
        : first(firstValue), second(secondValue)
    {
        /* Body intentionally empty. */
    }

    double& operator[](int index);

private:
    double first;
    double second;
};

//==============//
// End of code. //
//==============//
