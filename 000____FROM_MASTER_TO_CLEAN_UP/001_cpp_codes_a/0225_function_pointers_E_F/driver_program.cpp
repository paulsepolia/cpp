
//===========================//
// function pointers example //
//===========================//

#include <iostream>
#include <cmath>
#include <cstdlib>
#include <iomanip>

using std::endl;
using std::cout;
using std::cin;
using std::pow;
using std::rand;
using std::qsort;
using std::cos;
using std::fixed;
using std::showpoint;
using std::setprecision;
using std::showpos;

// function --> 1

// we use void * arguments so as to be able to use any type of agruments
// that is the C-way to pass any type of arguments
// C++ uses templates, but also the above syntax,
// so it is perfectly legal to use that in a C++ pure code

int mySorter(const void * first_arg, const void * second_arg)
{
    double first = * (double*) first_arg;
    double second = * (double*) second_arg;

    // sort here

    if (first < second) {
        return -1;
    } else if (first == second) {
        return 0;
    } else {
        return 1;
    }
}

// the main function

int main()
{
    // local variables and parameters

    const int DIMEN_MAX = static_cast<int>(pow(10.0, 7.0));
    double * myArray = new double [DIMEN_MAX];
    int i;

    // set the output format

    cout << fixed;
    cout << setprecision(20);
    cout << showpos;
    cout << showpoint;

    // build the array

    for (i = 0; i < DIMEN_MAX; i++) {
        myArray[i] = cos(static_cast<double>(i));
    }

    // sort the array

    qsort(myArray, DIMEN_MAX, sizeof(double), mySorter);

    // display some results

    for (i = 0; i < 10000; i++) {
        cout << " --> " << i << " --> " << myArray[i] << endl;
    }

    // delete the array

    delete [] myArray;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

