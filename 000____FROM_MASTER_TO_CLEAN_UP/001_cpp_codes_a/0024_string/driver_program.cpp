//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/10/13              //
// Functions: string             //
//===============================//

#include <iostream>
#include <string>

using namespace std;

int main()
{
    string str1;
    string str2;
    char ch;
    long int i;
    const long int I_MAX_DO = 1000000;

    cout << "Enter a string with no blanks: ";
    cin >> str1; // read the string
    cin.get(ch); // read new line character
    cout << endl;
    cout << "The string you entered = " << str1 << endl;
    cout << endl;

    cout << "Enter a sentence: ";
    getline(cin, str2);
    cout << endl;
    cout << "The sentence you entered is: " << str2 << endl;

    str2 = str2 + str1;
    cout << "str2 is: " << str2 << endl;

    for (i = 0; i < I_MAX_DO; i++) {
        str2 = str2 + str1;
    }

    cout << "str2 is: " << str2 << endl;

    return 0;
}

//======//
// FINI //
//======//
