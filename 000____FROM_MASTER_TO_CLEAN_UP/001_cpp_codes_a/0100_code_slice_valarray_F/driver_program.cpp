//========================//
// slice example //
//========================//

#include <iostream>
#include <cstddef>
#include <valarray>

using namespace std;

// the main code

int main()
{
    // 1. local variables and parameters

    const int DIMEN = 100000;
    valarray<int> foo(DIMEN);
    int i;
    size_t n;

    for (i = 0; i < DIMEN; i++) {
        foo[i] = i;
    }

    slice mySlice = slice(1, 10, 100);

    valarray<int> bar = foo[mySlice];

    cout << "slice(2,100,4): " << endl;

    for (n = 0; n < bar.size(); n++) {
        cout << " " << bar[n];
    }

    cout << endl;

    return 0;
}

//======//
// FINI //
//======//
