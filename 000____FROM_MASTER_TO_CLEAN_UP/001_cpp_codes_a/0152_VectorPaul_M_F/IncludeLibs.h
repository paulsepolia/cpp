//================//
// common headers //
//================//

#include <iostream>
#include <cmath>
#include <iomanip>
#include <algorithm>

using std::cin;
using std::cout;
using std::endl;
using std::showpoint;
using std::setprecision;
using std::showpos;
using std::fixed;
using std::sort;
using std::random_shuffle;
using std::max_element;
using std::min_element;

//======//
// FINI //
//======//