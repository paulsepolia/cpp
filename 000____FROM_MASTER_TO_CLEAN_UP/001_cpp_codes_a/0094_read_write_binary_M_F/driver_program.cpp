//=======================================//
// Writing a binary file and then        //
// Reading the entire binary file in RAM //
//=======================================//

#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>
#include <cmath>

using namespace std;

int main ()
{
    // 1. local variables and parameters

    const long int I_MAX = pow(10, 4.0);
    const string fileName = "my_file.bin";
    ifstream fileIn;
    ofstream fileOut;
    double tmpA;
    long int i;
    double* valueArray = new double [I_MAX];

    // 2. adjust the output format

    cout << fixed;
    cout << setprecision(10);
    cout << showpos;
    cout << showpoint;

    // 3. open the stream to write the data

    fileOut.open(fileName.c_str(), ios::out | ios::binary);

    // 4. test if the file stream is openend

    if(!fileOut.is_open()) {
        cout << " Error! The file to write to is not opened. Exit." << endl;
        return -1;
    }

    // 5. write to the file

    cout << " 1 --> Write data to the file" << endl;

    for (i = 0; i < I_MAX; i++) {
        tmpA = static_cast<double>(i+1+0.2);
        fileOut.write(reinterpret_cast<char*>(&tmpA), sizeof(tmpA));
    }

    // 6. close the file

    fileOut.close();

    // 7. open the file stream to read

    fileIn.open(fileName.c_str(), ios::in | ios::binary );

    // 8. test if the file stream is opened

    if (!fileIn.is_open()) {
        cout << " Error! The file to read from is not opened. Exit." << endl;
        return -1;
    }

    // 9. rewind the file

    cout << " 2 --> Rewind the file" << endl;

    fileIn.seekg(0, ios::beg);

    // 10. read element by element and store in RAM

    cout << " 3 --> Read element by element and store in RAM" << endl;

    for (i = 0; i < I_MAX; i++) {
        fileIn.read(reinterpret_cast<char*>(&tmpA), sizeof(tmpA));
        valueArray[i] = tmpA;
    }

    // 11. close the file

    fileIn.close();

    // 12. some output

    cout << " 4 --> Some output to make a test" << endl;

    for (i = 90; i < 93; i++) {
        cout << i << " --> " << valueArray[i] << endl;
    }

    // 13. free up the RAM

    cout << " 5 --> Free up RAM" << endl;

    delete [] valueArray;

    return 0;
}

//======//
// FINI //
//======//
