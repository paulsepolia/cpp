//========================//
// STL: deque             //
// resize, shrink_to_fit  //
//========================//

#include <iostream>
#include <deque>
#include <cmath>
#include <ctime>
#include <iomanip>

using namespace std;

int main()
{
    deque<double> deqA;
    long int i;
    const long int DEQ_DIM = static_cast<long int>(pow(10.0, 8.0));
    const int COEF = 2;
    const double ZERO = static_cast<double>(0);
    int k;
    const int K_MAX = 1000000;
    clock_t t1;
    clock_t t2;

    cout << fixed;
    cout << setprecision(5);
    cout << showpoint;

    // 1. build the deque -- MAIN CODE

    for (k = 1; k <= K_MAX; k++) {
        cout << "--------------------------------------------------------------------------------->>> " << k << endl;
        cout << endl;
        cout << "  1 --> Build the deque " << endl;

        t1 = clock();

        for (i = 1; i <= DEQ_DIM; i++) {
            deqA.push_back(static_cast<double>(i));
        }

        t2 = clock();

        cout << "  2 --> Time used to build the deque ------------------------------------------->>> "
             << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

        // 2. get the size of the deque

        cout << "  3 --> The size of the deque is deqA.size() = " << deqA.size() << endl;

        // 3. shrink to fit the size

        cout << "  4 --> Shrink to fit the size of the deque " << endl;

        deqA.shrink_to_fit();

        // 4. get the size of the deque

        cout << "  5 --> The size of the deque is deqA.size() = " << deqA.size() << endl;

        // 5. resize the deque

        cout << "  6 --> Resize the deque to be " << COEF << " times the old size" << endl;

        deqA.resize(COEF * DEQ_DIM, ZERO);

        // 6. get the size of the deque

        cout << "  7 --> The size of the deque is deqA.size() = " << deqA.size() << endl;

        // 7. clear the deque

        cout << "  8 --> clear the deque" << endl;

        deqA.clear();

        // 8. get the size of the deque

        cout << "  9 --> The size of the deque is deqA.size() = " << deqA.size() << endl;

        // 9. build the deque again when the capacity is enough to hold the elements

        cout << " 10 --> Build the deque again and notice the speed" << endl;

        t1 = clock();

        for (i = 1; i <= DEQ_DIM; i++) {
            deqA.push_back(static_cast<double>(i));
        }

        t2 = clock();

        cout << " 11 --> Time used to build the deque ------------------------------------------->>> "
             << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

        // 10. clear the deque

        cout << " 12 --> clear the deque" << endl;

        deqA.clear();

        // 11. shrink to fit the size

        cout << " 13 --> Shrink to fit the size of the deque" << endl;

        deqA.shrink_to_fit();

        // 12. get the size of the deque

        cout << " 14 --> The size of the deque is deqA.size() = " << deqA.size() << endl;

        // 13. get the size of the deque

        cout << " 15 --> The size of the deque is deqA.size() = " << deqA.size() << endl;

        // 14. build the deque again when the capacity is enough to hold the elements

        cout << " 16 --> Build the deque again and notice the speed" << endl;

        t1 = clock();

        for (i = 1; i <= DEQ_DIM; i++) {
            deqA.push_back(static_cast<double>(i));
        }

        t2 = clock();

        cout << " 17 --> Time used to build the deque ------------------------------------------->>> "
             << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

        // 15. get the size of the deque

        cout << " 18 --> The size of the deque is deqA.size() = " << deqA.size() << endl;

        // 16. clear the deque

        cout << " 19 --> clear the deque" << endl;

        deqA.clear();

        // 17. shrink to fit the size

        cout << " 20 --> Shrink to fit the size of the deque" << endl;

        deqA.shrink_to_fit();

        // 18. get the size of the deque

        cout << " 21 --> The size of the deque is deqA.size() = " << deqA.size() << endl;

        cout << endl;
    }
    // x. sentinel

    int sentinel;
    cin >> sentinel;

    return 0;
}

// FINI
