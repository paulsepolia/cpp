//======================//
// STL clear and delete //
//======================//

#include "includes.h"
#include "bench_fun.h"

// the main program

int main()
{
    // variables and parameters

    const long I_MAX = static_cast<long>(pow(10.0, 7.0));
    const long K_MAX = static_cast<long>(pow(10.0, 6.0));
    long k;
    clock_t t1;
    clock_t t2;
    double tall;
    double tallFun;

    // main bench loop

    for (k = 0; k < K_MAX; k++) {
        cout << endl;
        cout << "------------------------------------------------------------>>> " << k << endl;
        cout << endl;

        t1 = clock();

        tallFun = bench_fun(I_MAX);

        t2 = clock();

        tall = (t2-t1)/ static_cast<double>(CLOCKS_PER_SEC);

        cout << " xx --> total time for benchmark function in main --> " << tall << endl;
        cout << " yy --> total deallocation time on stack          --> " << tall - tallFun << endl;
    }

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
