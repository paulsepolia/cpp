
//================//
// driver program //
//================//

#include <iostream>
#include <cmath>
#include <iomanip>
#include <cstdlib>
#include "base.h"
#include "derivedA.h"

using std::cout;
using std::endl;
using std::cin;
using std::pow;
using std::setprecision;
using std::fixed;
using std::showpos;
using std::showpoint;
using std::boolalpha;
using std::exit;

using pgg::Base;
using pgg::DerivedA;

// main function

int main()
{
    // set the output format

    cout << fixed;
    cout << setprecision(18);
    cout << showpos;
    cout << showpoint;
    cout << boolalpha;

    // local parameters and variables

    const long K_MAX = static_cast<long>(pow(10.0, 7.0));
    const long DIM_MAX = static_cast<long>(pow(10.0, 7.0));

    for (long k = 0; k < K_MAX; k++) {

        cout << "---------------------------------------------------------------->> " << k << endl;

        // A1 object

        Base A1;

        A1 = DIM_MAX;

        A1 = 11.11;

        cout << " A1.getElement(10) = " << A1.getElement(10) << endl;

        // D1 object

        DerivedA D1;

        D1 = DIM_MAX;

        D1 = 22.22;

        // D2 object

        cout << " D1.getElement(10) = " << D1.getElement(10) << endl;

        DerivedA D2;

        D2 = DIM_MAX;

        cout << " D2.getElement(10) = " << D2.getElement(10) << endl;

        A1.allocate(DIM_MAX);
        D1.allocate(DIM_MAX);
        D2.allocate(DIM_MAX);

        A1 = 10.0;
        D1 = 20.0;
        D2 = 30.0;

        DerivedA D3;

        D3 = D1 + D2 + A1;

        cout << " D3.getElement(10) = " << D3.getElement(10) << endl;

        A1 = D1 + D2 + A1;

        cout << " A1.getElement(10) = " << A1.getElement(10) << endl;

        cout << " D3.createBackup();" << endl;

        D3.createBackup();

        D3 = 111.111;

        cout << " D3.getElement(10) = " << D3.getElement(10) << endl;

        D3.getBackup();

        cout << " D3.getBackup();" << endl;

        cout << " D3.getElement(10) = " << D3.getElement(10) << endl;

        //

        cout << " D3 = A1 + 11; " << endl;

        D3 = A1 + 11;

        cout << " D3.getElement(10) = " << D3.getElement(10) << endl;

        cout << " D3 = D3 + 200; " << endl;

        D3 = D3 + 200;

        cout << " D3.getElement(10) = " << D3.getElement(10) << endl;

        //

        cout << " D3 = A1 - 11; " << endl;

        D3 = A1 - 11;

        cout << " D3.getElement(10) = " << D3.getElement(10) << endl;

        cout << " D3 = D3 - 200; " << endl;

        D3 = D3 - 200;

        cout << " D3.getElement(10) = " << D3.getElement(10) << endl;

        //

        cout << " D3 = A1 * 11; " << endl;

        D3 = A1 * 11;

        cout << " D3.getElement(10) = " << D3.getElement(10) << endl;

        cout << " D3 = D3 * 200; " << endl;

        D3 = D3 * 200;

        cout << " D3.getElement(10) = " << D3.getElement(10) << endl;

        //

        cout << " D3 = A1 / 11; " << endl;

        D3 = A1 / 11;

        cout << " D3.getElement(10) = " << D3.getElement(10) << endl;

        cout << " D3 = D3 / 200; " << endl;

        D3 = D3 / 200;

        cout << " D3.getElement(10) = " << D3.getElement(10) << endl;

        //

        cout << " D3 += 11; " << endl;

        D3 += 11;

        cout << " D3.getElement(10) = " << D3.getElement(10) << endl;

        //

        cout << " D3 *= 200; " << endl;

        D3 *= 200;

        cout << " D3.getElement(10) = " << D3.getElement(10) << endl;

        //

        cout << " D3 /= 200; " << endl;

        D3 /= 200;

        cout << " D3.getElement(10) = " << D3.getElement(10) << endl;

        //

        cout << " D3 -= 200; " << endl;

        D3 -= 200;

        cout << " D3.getElement(10) = " << D3.getElement(10) << endl;

        //

        D1.destroy();
        D2.destroy();
        D3.destroyBackup();
        A1.destroy();
        D3.destroy();
        D3.destroyBackup();
    }

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
