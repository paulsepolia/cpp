
//===========================//
// using standard exceptions //
//===========================//

#include <iostream>
#include <exception>

using std::exception;
using std::endl;
using std::cout;

// a inherited class from the exception C++ class
// here I re-define the virtual function what()
// of the base class exception

class myexception: public exception {
    virtual const char* what() const throw()
    {
        return "My exception happened";
    }
};

// the main function

int main ()
{
    myexception myex1;
    myexception myex2;

    // 1.

    try {
        throw myex1;
    } catch (exception& e) {
        cout << " --> 1 --> " << e.what() << endl;
    }

    // 2.

    try {
        throw myex2;
    } catch (exception& e) {
        cout << " --> 2 --> " << e.what() << endl;
    }

    // 3.

    int sentinel;
    std::cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
