
//===========================//
// derived1 class definition //
//===========================//

#include <cstdlib>
#include <cmath>
#include "base.h"
#include "derived1.h"

using std::rand;
using std::srand;

namespace pgg {
// default constructor

Derived1::Derived1() : Base(), dim1(0), a1(NULL), b1(NULL) {}

// virtual destructor

Derived1::~Derived1()
{
    delete [] a1;
    delete [] b1;

    a1 = NULL;
    b1 = NULL;
}

// member function

void Derived1::setDim(long theDim)
{
    Base::setDim(theDim);
    dim1 = theDim;
}

// member function

long Derived1::getDim() const
{
    return dim1;
}

// member function

void Derived1::reserve()
{
    Base::reserve();
    a1 = new double [dim1];
    b1 = new double [dim1];
}

// member function

void Derived1::build(double theElem)
{
    Base::build(theElem);

    for (long i = 0; i < dim1; i++) {
        a1[i] = theElem;
        b1[i] = theElem;
    }
}

// member function

void Derived1::buildRandom(int theSeed)
{
    Base::buildRandom(theSeed);

    srand(theSeed);

    double theElem = static_cast<double>(rand());

    for (long i = 0; i < dim1; i++) {
        a1[i] = theElem;
        b1[i] = theElem;
    }
}

// member function

void Derived1::destroy()
{
    Base::destroy();

    delete [] a1;
    delete [] b1;

    a1 = NULL;
    b1 = NULL;

    dim1 = 0;
}

// member function

void Derived1::setEqual(const Derived1& objA)
{
    Base::setEqual(objA);

    this->dim1 = objA.dim1;
    this->a1 = new double [objA.dim1];
    this->b1 = new double [objA.dim1];

    for (long i = 0; i < objA.dim1; i++) {
        this->a1[i] = objA.a1[i];
        this->b1[i] = objA.b1[i];
    }
}

// member function

double Derived1::getElementAlpha(long index) const
{
    return Base::getElementAlpha(index);
}

// member function

double Derived1::getElementBeta(long index) const
{
    return Base::getElementBeta(index);
}

// member function

double Derived1::getElementAlpha1(long index) const
{
    return a1[index];
}

// member function

double Derived1::getElementBeta1(long index) const
{
    return b1[index];
}

// member function

void Derived1::setElement(double theElem, long index)
{
    Base::setElement(theElem, index);

    a1[index] = theElem;
    b1[index] = -theElem;
}

// member function

bool Derived1::isEqual(const Derived1& objA) const
{
    if (!Base::isEqual(objA)) {
        return false;
    }

    if (objA.dim1 != this->dim1) {
        return false;
    }

    for (long i = 0; i != objA.dim1; i++) {
        if (objA.a1[i] != this->a1[i]) {
            return false;
        }

        if (objA.b1[i] != this->b1[i]) {
            return false;
        }
    }

    return true;
}


// = operator overload

Derived1 & Derived1::operator = (const Derived1 & objA)
{
    this->setEqual(objA);

    return *this;
}


// copy constructor

Derived1::Derived1(const Derived1& objA) : Base(objA), dim1(objA.dim1), a1(NULL), b1(NULL)
{
    a1 = new double [dim1];
    b1 = new double [dim1];

    for (long i = 0; i < dim1; i++) {
        a1[i] = objA.a1[i];
        b1[i] = objA.b1[i];
    }
}

// friend function

bool operator == (const Derived1& objA1, const Derived1& objA2)
{
    return (objA1.isEqual(objA2));
}

// friend function
// operator overload +

const Derived1 operator + (const Derived1& objA1, const Derived1& objA2)
{
    Derived1 objA3;

    objA3.setDim(objA1.dim1);

    objA3.reserve();

    for (long i = 0; i < objA1.dim1; i++) {
        objA3.setElement(objA1.a1[i] + objA2.a1[i], i);
    }

    return Derived1(objA3);
}

// friend function
// operator overload -

const Derived1 operator - (const Derived1& objA1, const Derived1& objA2)
{
    Derived1 objA3;

    objA3.setDim(objA1.dim1);

    objA3.reserve();

    for (long i = 0; i < objA1.dim1; i++) {
        objA3.setElement(objA1.a1[i] - objA2.a1[i], i);
    }

    return Derived1(objA3);
}

// friend function
// operator overload *

const Derived1 operator * (const Derived1& objA1, const Derived1& objA2)
{
    Derived1 objA3;

    objA3.setDim(objA1.dim1);

    objA3.reserve();

    for (long i = 0; i < objA1.dim1; i++) {
        objA3.setElement(objA1.a1[i] * objA2.a1[i], i);
    }

    return Derived1(objA3);
}

// friend function
// operator overload /

const Derived1 operator / (const Derived1& objA1, const Derived1& objA2)
{
    Derived1 objA3;

    objA3.setDim(objA1.dim1);

    objA3.reserve();

    for (long i = 0; i < objA1.dim1; i++) {
        objA3.setElement(objA1.a1[i] / objA2.a1[i], i);
    }

    return Derived1(objA3);
}

// friend function
// operator overload +=

const Derived1& operator += (Derived1& objA1, const Derived1& objA2)
{
    objA1 = objA1 + objA2;

    return objA1;
}

// friend function
// operator overload -=

const Derived1& operator -= (Derived1& objA1, const Derived1& objA2)
{
    objA1 = objA1 - objA2;

    return objA1;
}

// friend function
// operator overload *=

const Derived1& operator *= (Derived1& objA1, const Derived1& objA2)
{
    objA1 = objA1 * objA2;

    return objA1;
}

// friend function
// operator overload /=

const Derived1& operator /= (Derived1& objA1, const Derived1& objA2)
{
    objA1 = objA1 / objA2;

    return objA1;
}

// friend function
// unary operator overload -

const Derived1 operator - (const Derived1& objA1)
{
    Derived1 objA3;

    objA3.setDim(objA1.dim1);

    objA3.reserve();

    for (long i = 0; i < objA1.dim1; i++) {
        objA3.setElement(-objA1.a1[i], i);
    }

    return Derived1(objA3);
}

// friend function
// operator overload ++ (prefix)

const Derived1 operator ++ (Derived1& objA1)
{
    Derived1 objA3;

    objA3.setDim(objA1.dim1);

    objA3.reserve();

    for (long i = 0; i < objA1.dim1; i++) {
        objA3.setElement(++objA1.a1[i], i);
    }

    return Derived1(objA3);
}

// friend function
// operator overload ++ (postfix)

const Derived1 operator ++ (Derived1& objA1, int)
{
    Derived1 objA3;

    objA3.setDim(objA1.dim1);

    objA3.reserve();

    for (long i = 0; i < objA1.dim1; i++) {
        objA3.setElement(++objA1.a1[i], i);
    }

    return Derived1(objA3);
}

// friend function
// operator overload -- (prefix)

const Derived1 operator -- (Derived1& objA1)
{
    Derived1 objA3;

    objA3.setDim(objA1.dim1);

    objA3.reserve();

    for (long i = 0; i < objA1.dim1; i++) {
        objA3.setElement(--objA1.a1[i], i);
    }

    return Derived1(objA3);
}

// friend function
// operator overload -- (postfix)

const Derived1 operator -- (Derived1& objA1, int)
{
    Derived1 objA3;

    objA3.setDim(objA1.dim1);

    objA3.reserve();

    for (long i = 0; i < objA1.dim1; i++) {
        objA3.setElement(--objA1.a1[i], i);
    }

    return Derived1(objA3);
}

} // pgg

//======//
// FINI //
//======//
