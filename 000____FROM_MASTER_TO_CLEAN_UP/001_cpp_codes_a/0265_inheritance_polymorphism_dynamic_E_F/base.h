
//========================//
// base class declaration //
//========================//

#ifndef BASE_H
#define BASE_H

namespace pgg {
class Base {
public:

    Base();
    virtual ~Base();
    void setDim(long);
    virtual long getDim() const;
    virtual void reserve();
    virtual void build(double);
    virtual void buildRandom(int);
    virtual void destroy();
    virtual void setEqual(const Base&);
    virtual double getElementAlpha(long) const;
    virtual double getElementBeta(long) const;
    virtual void setElement(double, long);
    virtual bool isEqual(const Base&) const;
    virtual Base & operator = (const Base&); // = operator overload
    Base(const Base&); // copy constructor

    // friend functions

    // ==

    friend bool operator == (const Base&, const Base&);

    // +, -, *, /

    friend const Base operator + (const Base&, const Base&);
    friend const Base operator - (const Base&, const Base&);
    friend const Base operator * (const Base&, const Base&);
    friend const Base operator / (const Base&, const Base &);

    // +=, -=, *=, /=

    friend const Base& operator += (Base&, const Base&);
    friend const Base& operator -= (Base&, const Base&);
    friend const Base& operator *= (Base&, const Base&);
    friend const Base& operator /= (Base&, const Base&);

    // -, ++, --

    friend const Base operator -  (const Base&);
    friend const Base operator ++ (Base&);
    friend const Base operator ++ (Base&, int);
    friend const Base operator -- (Base&);
    friend const Base operator -- (Base&, int);

private:

    long dim;
    double * a;
    double * b;
};

} // pgg

#endif // !BASE_H

//======//
// FINI //
//======//
