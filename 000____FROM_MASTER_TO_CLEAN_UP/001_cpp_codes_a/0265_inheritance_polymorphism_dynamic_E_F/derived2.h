
//============================//
// Derived2 class declaration //
//============================//

#ifndef DERIVED_2_H
#define DERIVED_2_H

#include "base.h"
#include "derived1.h"

namespace pgg {
class Derived2 : public Derived1 {
public:

    Derived2();
    virtual ~Derived2();
    virtual void setDim(long);
    virtual long getDim() const;
    virtual void reserve();
    virtual void build(double);
    virtual void buildRandom(int);
    virtual void destroy();
    virtual void setEqual(const Derived2&);
    virtual double getElementAlpha(long) const;
    virtual double getElementBeta(long) const;
    virtual double getElementAlpha1(long) const;
    virtual double getElementBeta1(long) const;
    virtual double getElementAlpha2(long) const;
    virtual double getElementBeta2(long) const;
    virtual void setElement(double, long);
    virtual bool isEqual(const Derived2&) const;
    virtual Derived2 & operator = (const Derived2&); // = operator overload
    Derived2(const Derived2&); // copy constructor

    // friend functions

    friend bool operator == (const Derived2&, const Derived2&);

    // +, -, *, /

    friend const Derived2 operator + (const Derived2&, const Derived2&);
    friend const Derived2 operator - (const Derived2&, const Derived2&);
    friend const Derived2 operator * (const Derived2&, const Derived2&);
    friend const Derived2 operator / (const Derived2&, const Derived2&);

    // +=, -=, *=, /=

    friend const Derived2& operator += (Derived2&, const Derived2&);
    friend const Derived2& operator -= (Derived2&, const Derived2&);
    friend const Derived2& operator *= (Derived2&, const Derived2&);
    friend const Derived2& operator /= (Derived2&, const Derived2&);

    // -, ++, --

    friend const Derived2 operator -  (const Derived2&);
    friend const Derived2 operator ++ (Derived2&);
    friend const Derived2 operator ++ (Derived2&, int);
    friend const Derived2 operator -- (Derived2&);
    friend const Derived2 operator -- (Derived2&, int);

private:

    long dim2;
    double * a2;
    double * b2;
};

} // pgg

#endif // DERIVED_2_H

//======//
// FINI //
//======//
