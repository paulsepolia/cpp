//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/10/30              //
// Functions:  vector            //
// Algorithms: for_each          //
//===============================//

// for_each example
#include <iostream>     // std::cout
#include <algorithm>    // std::for_each
#include <vector>       // std::vector
#include <typeinfo>

int myfunction (int i)    // function:
{
    return 2*i;
}

class myclass {           // function object type:
public:
    int operator() (int i)
    {
        return 2*i;
    }
} myobject;

int main ()
{
    std::vector<int> myvector;
    myvector.push_back(10);
    myvector.push_back(20);
    myvector.push_back(30);

    std::cout << "myvector contains:";
    for_each (myvector.begin(), myvector.end(), myfunction);
    for (int m = 0; m < 3; m++) {
        std::cout << myvector[m] << std::endl;
    }

    // or:
    std::cout << "myvector contains:";
    for_each (myvector.begin(), myvector.end(), myobject);
    for (int m = 0; m < 3; m++) {
        std::cout << myvector[m] << std::endl;
    }

    return 0;
}

