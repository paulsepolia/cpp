//=======================================//
// Author: Pavlos G. Galiatsatos         //
// Date: 2013/10/23                      //
// Functions: vector, deque, array, list //
// Algorithms: initialization            //
//=======================================//

#include <iostream>
#include <vector>
#include <deque>
#include <list>
#include <iomanip>
#include <algorithm>

using namespace std;

// the main function

int main ()
{
    // some variables declaration

    const long int MAX_ELEM = 4 * static_cast<long int>(10000000);
    const long int J_MAX_DO = 10;
    long int i;
    long int j;

    for (j = 0; j < J_MAX_DO; j++) {
        cout << "--------------------------------------------------> " << j+1 << endl;

        double *aArray = new double [MAX_ELEM];

        // build the array

        for (i = 0; i < MAX_ELEM; i++) {
            aArray[i] = static_cast<double>(MAX_ELEM-i);
        }

        // initialize the vector, deque, list with array

        vector<double> aVector(aArray, aArray+MAX_ELEM);
        deque<double> aDeque(aArray, aArray+MAX_ELEM);
        list<double> aList(aArray, aArray+MAX_ELEM);

        // set up the output style

        cout << fixed;
        cout << setprecision(10);
        cout << showpos;
        cout << showpoint;

        // some output

        cout << "aArray[0]     = " << aArray[0]     << endl;
        cout << "aArray[1]     = " << aArray[1]     << endl;
        cout << "aVector[0]    = " << aVector[0]    << endl;
        cout << "aVector[1]    = " << aVector[1]    << endl;
        cout << "aDeque[0]     = " << aDeque[0]     << endl;
        cout << "aDeque[1]     = " << aDeque[1]     << endl;
        cout << "aList.front() = " << aList.front() << endl;
        cout << "aList.back()  = " << aList.back()  << endl;

        // sort now

        cout << " sort vector " << endl;
        sort(aVector.begin(), aVector.end());
        cout << " sort deque " << endl;
        sort(aDeque.begin(), aDeque.end());
        cout << " sort list " << endl;
        aList.sort();

        // some output afet shorting

        cout << "aArray[0]     = " << aArray[0]     << endl;
        cout << "aArray[1]     = " << aArray[1]     << endl;
        cout << "aVector[0]    = " << aVector[0]    << endl;
        cout << "aVector[1]    = " << aVector[1]    << endl;
        cout << "aDeque[0]     = " << aDeque[0]     << endl;
        cout << "aDeque[1]     = " << aDeque[1]     << endl;
        cout << "aList.front() = " << aList.front() << endl;
        cout << "aList.back()  = " << aList.back()  << endl;

        // free RAM

        aVector.clear();
        aDeque.clear();
        aList.clear();
        delete [] aArray;
    }

    return 0;
}

// end
