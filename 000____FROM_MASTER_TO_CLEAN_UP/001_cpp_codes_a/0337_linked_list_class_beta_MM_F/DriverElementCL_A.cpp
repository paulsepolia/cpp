//========================================//
// Driver program to the ElementCL class. //
//========================================//

#include <iostream>

#include "ElementCL.h"
#include "ElementCLMemberFunctions.h"
#include "TypeDefinitions.h"

using namespace std;

int main()
{

    // local variables

    const TA vA = 2.0;
    const TA vB = 3.0;
    TA sA, sB, sC;

    // local objects

    ElementCL<TA> oA(10);
    ElementCL<TA> oB(11);
    ElementCL<TA> oC;

    // code rest

    // test 1.
    // testing the constructor

    sA = oA.GetElement();
    sB = oB.GetElement();
    sC = oC.GetElement();

    cout << "----> test 1." << endl;
    cout << sA << endl;
    cout << sB << endl;
    cout << sC << endl;

    // test 2.
    // testing the member function: SetElement(T&)

    oA.SetElement(vA);
    oB.SetElement(vB);

    sA = oA.GetElement();
    sB = oB.GetElement();

    cout << "----> test 2." << endl;
    cout << sA << endl;
    cout << sB << endl;

    // test 3.
    // testing the overloaded operator '='.

    oC = oA;

    sA = oA.GetElement();
    sC = oC.GetElement();

    cout << "----> test 3." << endl;
    cout << sA << endl;
    cout << sC << endl;

    // test 4.
    // testing the overloaded operators:
    // '+'  , '-'  , '*'  , '/'
    // '+=' , '-=' , '*=' , '/*'

    oA.SetElement(vA);
    oB.SetElement(vB);
    oC.SetElement();

    sA = oA.GetElement();
    sB = oB.GetElement();
    sC = oC.GetElement();

    cout << "----> test 4." << endl;
    cout << sA << endl;
    cout << sB << endl;
    cout << sC << endl;

    oC = oA + oB;
//  oC = oA - oB;
//  oC = oA * oB;
//  oC = oA / oB;
//  oB += oA;
//  oB -= oA;
//  oB *= oA;
//  oB /= oA;

    sA = oA.GetElement();
    sB = oB.GetElement();
    sC = oC.GetElement();

    cout << sA << endl;
    cout << sB << endl;
    cout << sC << endl;

    return 0;
}

//==============//
// End of code. //
//==============//
