//===========================================//
// Header file.                              //
// ListCL class friend functions definition. //
//===========================================//

#include <cstdlib>

// 1. CreateList

template <typename T, typename P>
void CreateList(ListCL<T,P>& list, P len)
{
    P i;
    T ZERO = 0.0;

    for (i = 0; i < len; i++) {
        list.AddElement(ZERO);
    }
}

// 2. SetEqual.

template <typename T, typename P>
void SetEqual(ListCL<T,P>& listB, ListCL<T,P>& listA)
{
    P iL, lenA;
    T elemA;

    lenA = listA.LengthList();

    for (iL =0; iL < lenA; iL++) {
        elemA = listA.GetListElement(iL+1);
        listB.SetListElement(elemA, iL+1);
    }
}

// 3. AddList

template <typename T, typename P>
void AddList(ListCL<T,P>& listC, ListCL<T,P>& listA, ListCL<T,P>& listB)
{
    P iL, len;
    T elemA, elemB, elemC;

    len = listA.LengthList();

    for (iL =0; iL < len; iL++) {
        elemA = listA.GetListElement(iL+1);
        elemB = listB.GetListElement(iL+1);
        elemC = elemA + elemB;
        listC.SetListElement(elemC, iL+1);
    }
}

// 4. SubtractList

template <typename T, typename P>
void SubtractList(ListCL<T,P>& listC, ListCL<T,P>& listA, ListCL<T,P>& listB)
{
    P iL, len;
    T elemA, elemB, elemC;

    len = listA.LengthList();

    for (iL =0; iL < len; iL++) {
        elemA = listA.GetListElement(iL+1);
        elemB = listB.GetListElement(iL+1);
        elemC = elemA - elemB;
        listC.SetListElement(elemC, iL+1);
    }
}

// 5. MultiplyList

template <typename T, typename P>
void MultiplyList(ListCL<T,P>& listC, ListCL<T,P>& listA, ListCL<T,P>& listB)
{
    P iL, len;
    T elemA, elemB, elemC;

    len = listA.LengthList();

    for (iL =0; iL < len; iL++) {
        elemA = listA.GetListElement(iL+1);
        elemB = listB.GetListElement(iL+1);
        elemC = elemA * elemB;
        listC.SetListElement(elemC, iL+1);
    }
}

// 6. DivideList

template <typename T, typename P>
void DivideList(ListCL<T,P>& listC, ListCL<T,P>& listA, ListCL<T,P>& listB)
{
    P iL, len;
    T elemA, elemB, elemC;

    len = listA.LengthList();

    for (iL =0; iL < len; iL++) {
        elemA = listA.GetListElement(iL+1);
        elemB = listB.GetListElement(iL+1);
        elemC = elemA / elemB;
        listC.SetListElement(elemC, iL+1);
    }
}

//==============//
// End of code. //
//==============//
