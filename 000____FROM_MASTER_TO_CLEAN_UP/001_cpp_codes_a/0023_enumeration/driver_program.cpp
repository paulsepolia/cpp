//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/10/13              //
// Functions: enumeration        //
//===============================//

#include <iostream>

using namespace std;

// ROCK, PAPER SCISSORS are default integer type
// so when you do long int arithemtic there is a possibility
// to have an overflow and to get back rubbish

enum objectType {ROCK, PAPER, SCISSORS};

int main()
{
    // 1.

    const long I_DO_MAX = 50 * static_cast<long>(1000000000);
    objectType ot1;
    objectType ot2;

    // 2.

    cout << "Default vaules of ot1 and ot2" << endl;
    cout << " ot1 = " << ot1 << endl;
    cout << " ot2 = " << ot2 << endl;
    cout << "The default values are rubbish" << endl;

    // 3.

    cout << "New values for ot1 and ot2" << endl;
    ot1 = PAPER;
    ot2 = SCISSORS;
    cout << " ot1 = " << ot1 << endl;
    cout << " ot2 = " << ot2 << endl;

    // 4.

    cout << "New values for ot1 and ot2" << endl;
    ot2 = ot1;
    cout << " ot1 = " << ot1 << endl;
    cout << " ot2 = " << ot2 << endl;

    // 5.

    cout << "New values for ot1 and ot2" << endl;
    ot2 = static_cast<objectType>(ot2 + 1);
    cout << " ot1 = " << ot1 << endl;
    cout << " ot2 = " << ot2 << endl;

    // 6.

    cout << "New values for ot1 and ot2" << endl;

    for (long i = 0; i < I_DO_MAX; i++) {
        ot2 = static_cast<objectType>(ot2 + 1);
        ot1 = static_cast<objectType>(ot2 + 1);
    }

    cout << " ot1 = " << ot1 << endl;
    cout << " ot2 = " << ot2 << endl;

    return 0;
}

//======//
// FINI //
//======//
