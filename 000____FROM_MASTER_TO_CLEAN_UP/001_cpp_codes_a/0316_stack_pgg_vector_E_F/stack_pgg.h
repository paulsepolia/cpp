
//====================================//
// declaration of the stack_pgg class //
//====================================//

#ifndef STACK_PGG_H
#define STACK_PGG_H

#include <vector>
#include <cstdlib>

using std::vector;
using std::size_t;

template <typename T>
class stack_pgg {
public:

    stack_pgg<T>(); // default constructor
    ~stack_pgg<T>(); // destructor
    stack_pgg<T>(const stack_pgg<T> &); // copy constructor

    // member functions

    bool empty() const;
    size_t size() const;
    T & top();
    void push(const T &);
    void pop();
    void swap(stack_pgg<T> &);
    void emplace(const T &);

    // operators overload

    stack_pgg<T> & operator = (const stack_pgg<T> &); // assignment operator

private:

    vector<T> elems;
};

#endif // STACK_PGG_H

//======//
// FINI //
//======//

