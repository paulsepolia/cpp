
//=======================================//
// implementation of the stack_pgg class //
//=======================================//

#include "stack_pgg.h"

// default constructor

template <typename T>
stack_pgg<T>::stack_pgg() {}

// destructor

template <typename T>
stack_pgg<T>::~stack_pgg() {}

// assignment operator

template <typename T>
stack_pgg<T> & stack_pgg<T>::operator = (const stack_pgg<T> & s1)
{
    this->elems = s1.elems;

    return *this;
}

// copy constructor

template <typename T>
stack_pgg<T>::stack_pgg(const stack_pgg<T> & s1)
{
    *this = s1;
}

// empty --> public member function

template <typename T>
bool stack_pgg<T>::empty() const
{
    return elems.empty();
}

// size --> public member function

template <typename T>
size_t stack_pgg<T>::size() const
{
    return elems.size();
}

// top --> public member function

template <typename T>
T & stack_pgg<T>::top()
{
    return elems.back();
}

// push --> public member function

template <typename T>
void stack_pgg<T>::push(const T & arg)
{
    elems.push_back(arg);
}

// pop --> public member function

template <typename T>
void stack_pgg<T>::pop()
{
    elems.pop_back();
}

// swap --> public member function

template <typename T>
void stack_pgg<T>::swap(stack_pgg<T> & s1)
{
    stack_pgg<T> stmp;

    stmp = s1;
    s1 = *this;
    *this = stmp;
}

// emplace --> public member function

template <typename T>
void stack_pgg<T>::emplace(const T & arg)
{
    elems.emplace_back(arg);
}


//======//
// FINI //
//======//

