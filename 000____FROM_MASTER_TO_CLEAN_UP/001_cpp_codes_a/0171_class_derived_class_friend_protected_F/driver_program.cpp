//============================//
// Base and derived class     //
// Public, Protected, Private //
//============================//

#include <iostream>

using std::cout;
using std::cin;
using std::endl;

// the base class

class Base {
private:

    int MyPrivateInt;

protected:

    int MyProtectedInt;

public:

    int MyPublicInt;
    Base() : MyPrivateInt (1),
        MyProtectedInt (2),
        MyPublicInt(3) {}; // my default constructor

    Base(int pri, int pro, int pub) :
        MyPrivateInt(pri), MyProtectedInt(pro), MyPublicInt(pub) {};

};

// the derived class

class Derived : public Base { // public inheritance so I can cast Derived as Base
public:

// int foo1() { return MyPrivateInt; }
// it is not okay, a derived class does not have access to private vars

    int foo2()
    {
        return MyProtectedInt;     // okay
    }
    int foo3()
    {
        return MyPublicInt;    // okay
    }

    Derived() {}; // the default constructor
};

// an unrelated class to the rest classes

class Unrelated {
private:

    Base B1;
    Base B2;

public:

    // it is not okay. Unrelated class has not access to private variables of Base and B
    //int foo1()  { return B1.MyPrivateInt; }

    // it is not okay. Unrelated class has not access to protected variables of Base and B
    //int foo2()  { return B1.MyProtectedInt; }

    int foo3()
    {
        return B1.MyPublicInt;     // okay
    }
    int foo4()
    {
        return B2.MyPublicInt;     // okay
    }

    Unrelated() : B1(), B2() {};
};

int main()
{
    // Base class object

    Base BaseObj1;
    Base BaseObj2(10, 11, 12);

    cout << " --> BaseObj1.MyPublicInt = " << BaseObj1.MyPublicInt << endl;
    cout << " --> BaseObj2.MyPublicInt = " << BaseObj2.MyPublicInt << endl;

    // Derived class object

    Derived DerivedObj;

    cout << " --> DerivedObj.foo2() = " << DerivedObj.foo2() << endl;

    cout << " --> DerivedObj.foo3() = " << DerivedObj.foo3() << endl;

    cout << " --> the following is possible because of public inheritance" << endl;
    cout << " --> DerivedObj.MyPublicInt = " << DerivedObj.MyPublicInt << endl;

    // Unrelated class object

    Unrelated UnrelatedObj;

    cout << " --> UnrelatedObj.foo3() = " << UnrelatedObj.foo3() << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;
}

//======//
// FINI //
//======//
