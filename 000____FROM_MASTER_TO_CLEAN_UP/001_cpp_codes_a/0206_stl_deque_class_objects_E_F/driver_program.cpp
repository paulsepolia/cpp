
//=========================//
// Using a deque to        //
// reassemble out-of-order //
// information packets     //
//=========================//

#include <iostream>
#include <deque>
#include <iterator>
#include <algorithm>
#include "packet.h"
#include "packet_functions.h"

using std::cout;
using std::cin;
using std::endl;
using std::deque;

// the main function

int main()
{
    deque<packet> pack;
    deque<packet>::iterator p;
    packet pkt;

    getpacket(pkt); // get first packet
    pack.push_back(pkt); // put first packet in deque

    // read and store remaining packets

    while(getpacket(pkt)) {
        if(pkt.getpnum() < pack.front().getpnum()) {
            pack.push_front(pkt);
        } else if (pkt.getpnum() >= pack.back().getpnum()) {
            pack.push_back(pkt);
        } else {
            p = lower_bound(pack.begin(), pack.end(), pkt);
            pack.insert(p, pkt);
        }
    }

    cout << endl;
    cout << "Packets reassembled: " << endl;

    for (unsigned int i = 0; i < pack.size(); i++) {
        cout << pack[i].getinfo() << " ";
    }

    cout << endl;

    return 0;
}

//======//
// FINI //
//======//

