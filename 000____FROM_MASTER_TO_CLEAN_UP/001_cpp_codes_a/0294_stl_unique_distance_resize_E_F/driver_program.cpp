
//===========================================//
// STL,  remove_if, unique, resize, distance //
//===========================================//

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <vector>
#include <cmath>

using std::endl;
using std::cout;
using std::cin;
using std::fixed;
using std::setprecision;
using std::showpos;
using std::showpoint;

using std::sort;
using std::unique;
using std::distance;

using std::vector;
using std::pow;
using std::iterator;

// function as predicate

bool myPredA(const double& x, const double& y)
{
    const double DETA = 0.001;
    return ((abs(x)-abs(y)) < DETA);
}

// function object as predicate

class myPredB {
private:

    static const constexpr double  DETA = 0.001;

public:

    bool operator()(const double& x, const double& y) const
    {
        return  ((x-y) < DETA);
    }
};

// the main function

int main()
{
    // local variables and parameters

    const long long DIM_MAX = static_cast<long long>(pow(10.0, 8.0));
    const long long K_MAX = static_cast<long long>(pow(10.0, 5.0));
    vector<double>::iterator itv;

    cout << fixed;
    cout << showpoint;
    cout << showpos;
    cout << setprecision(20);

    for( long long k = 0; k != K_MAX; k++) {
        // the counter

        cout << "------------------------------------------------>> " << k << endl;

        // the vector

        vector<double> * VA = new vector<double>;

        // build the vector again

        for (long long i = 0; i != DIM_MAX; i++) {
            VA->push_back(cos(static_cast<double>(i)));
        }

        cout << " --> VA->size() = " << VA->size() << endl;

        // sort the vector

        cout << " --> sort the vector using the general algorithm" << endl;

        sort(VA->begin(), VA->end());

        // unique the sorted vector

        cout << " --> apply unique to the sorted vector and then resize" << endl;

        itv = unique(VA->begin(), VA->end(), myPredB());

        // apply resize

        VA->resize(distance(VA->begin(), itv));

        // get the size --> must be 3

        cout << " --> VA->size() = " << VA->size() << endl;

        // some outputs

        cout << " (*VA)[0] = " << (*VA)[0] << endl;
        cout << " (*VA)[1] = " << (*VA)[1] << endl;
        cout << " (*VA)[2] = " << (*VA)[2] << endl;

        //
        // delete the vector

        cout << " --> delete the vector" << endl;

        delete VA;
    }

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

