//=======================================//
// Author: Pavlos G. Galiatsatos         //
// Date: 2013/10/30                      //
// Functions: array, vector, deque, list //
// Algorithms: accumulate, count         //
//=======================================//

#include <iostream>
#include <algorithm>
#include <iterator>
#include <vector>
#include <list>
#include <deque>
#include <cmath>
#include <numeric>
#include <iomanip>

using namespace std;

bool gotIt(double x)
{
    return (x == 1.0);
}

// global consts

const long int I_MAX = 5 * static_cast<long int>(10000000);

// main

int main ()
{
    // variables declaration

    int i;
    vector<double> aVector;
    list<double> aList;
    deque<double> aDeque;
    int j;
    int k;
    const int J_MAX = 100;
    double sumA = 1.0;
    double sumV = 1.0;
    double sumL = 1.0;
    double sumD = 1.0;
    const int K_MAX = 10;

    cout << fixed;
    cout << showpos;
    cout << showpoint;
    cout << setprecision(20);

    // build the array

    for (j = 0; j < J_MAX; j++) {
        cout << "------------------------------------------------>> " << j+1 << endl;

        cout << " 1 --> build the array" << endl;

        double *aArray = new double [I_MAX];

        for (i = 0;  i < I_MAX; i++) {
            aArray[i] = 1.0;
        }

        // copy the array to a vector

        cout << " 2 --> build the vector" << endl;

        aVector.resize(I_MAX);

        copy(aArray, aArray+I_MAX, aVector.begin());

        // copy the array to the list

        cout << " 3 --> build the list" << endl;

        aList.resize(I_MAX);

        copy(aArray, aArray+I_MAX, aList.begin());

        // copy the array to the deque

        cout << " 4 --> build the deque" << endl;

        aDeque.resize(I_MAX);

        copy(aList.begin(), aList.end(), aDeque.begin());

        // accumulate

        for (k = 0; k < K_MAX; k++) {
            sumA = accumulate(aArray, aArray+I_MAX, sumA, multiplies<double>());
            sumV = accumulate(aVector.begin(), aVector.end(), sumV, multiplies<double>());
            sumL = accumulate(aList.begin(), aList.end(), sumL, multiplies<double>());
            sumD = accumulate(aDeque.begin(), aDeque.end(), sumD, multiplies<double>());
        }

        cout << " sumA = " << sumA << endl;
        cout << " sumV = " << sumV << endl;
        cout << " sumL = " << sumL << endl;
        cout << " sumD = " << sumD << endl;

        // count

        for (k = 0; k < K_MAX; k++) {
            sumA = count_if(aArray, aArray+I_MAX, gotIt);
            sumV = count_if(aVector.begin(), aVector.end(), gotIt);
            sumL = count_if(aList.begin(), aList.end(), gotIt);
            sumD = count_if(aDeque.begin(), aDeque.end(), gotIt);
        }

        cout << " sumA = " << sumA << endl;
        cout << " sumV = " << sumV << endl;
        cout << " sumL = " << sumL << endl;
        cout << " sumD = " << sumD << endl;

        // free up RAM

        cout << " Free up RAM" << endl;

        aVector.clear();
        aList.clear();
        aDeque.clear();
        delete [] aArray;

        sumA = sumV = sumL = sumD = 1.0;
    }

    return 0;
}

// end
