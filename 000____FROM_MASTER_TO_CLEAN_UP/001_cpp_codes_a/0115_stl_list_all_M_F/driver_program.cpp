//==========//
// List All //
//==========//

#include <iostream>
#include <list>
#include <cmath>
#include <ctime>
#include <iomanip>

// A. a binary predicate implemented as a function

bool same_integral_part(double first, double second)
{
    return (int(first) == int(second));
}

// B. a binary predicate implemented as a class

class is_near {
public:
    bool operator() (double first, double second)
    {
        return (fabs(first-second) < 0.5);
    }
};

// C. a binary predicate implemented as a class

struct myComClass {
    bool operator() (double i,double j)
    {
        return (i < j);
    }
} myComObj;


// D. main program

int main()
{
    // 1. variable an parameters declaration

    const long int I_MAX = static_cast<long int>(pow(10.0, 7.9));
    const long int J_MAX = static_cast<long int>(pow(10.0, 6.0));
    const double EL_A = 7.0;
    const double EL_B = 8.0;
    std::list<double> listA;
    std::list<double> listB;
    long int i;
    long int j;
    std::list<double>::iterator it1;
    std::list<double>::iterator it2;
    std::list<double>::iterator it3;
    std::list<double>::iterator it4;

    // 2. main bench loop

    for (j = 1; j <= J_MAX; j++) {
        std::cout << std::endl;
        std::cout << "------------------------------------------------------------------>> " << j << std::endl;

        // 3. build the two lists in parallel

        std::cout << std::endl;
        std::cout << "  1 --> build the lists in parallel, using assign" << std::endl;
        std::cout << std::endl;

        #pragma omp parallel sections \
        shared(listA, listB, std::cout)
        {
            #pragma omp section
            {
                listA.assign(I_MAX, EL_A);
                std::cout << "--> done with listA" << std::endl;
            }
            #pragma omp section
            {
                listB.assign(I_MAX, EL_B);
                std::cout << "--> done with listB" << std::endl;
            }
        }

        // 4. unique the two lists in parallel

        std::cout << std::endl;
        std::cout << "  2 --> unique the lists in parallel " << std::endl;
        std::cout << std::endl;

        #pragma omp parallel sections \
        shared(listA, listB, std::cout)
        {
            #pragma omp section
            {
                listA.unique();
                std::cout << "--> done with listA" << std::endl;
            }
            #pragma omp section
            {
                listB.unique();
                std::cout << "--> done with listB" << std::endl;
            }
        }

        std::cout << " size of listA = " << listA.size() << std::endl;
        std::cout << " size of listB = " << listB.size() << std::endl;

        // 5. build the two lists in parallel

        std::cout << std::endl;
        std::cout << "  3 --> build the lists in parallel again, using assign" << std::endl;
        std::cout << std::endl;

        #pragma omp parallel sections \
        shared(listA, listB, std::cout)
        {
            #pragma omp section
            {
                listA.assign(I_MAX, EL_A);
                std::cout << "--> done with listA" << std::endl;
            }
            #pragma omp section
            {
                listB.assign(I_MAX, EL_B);
                std::cout << "--> done with listB" << std::endl;
            }
        }

        std::cout << " size of listA = " << listA.size() << std::endl;
        std::cout << " size of listB = " << listB.size() << std::endl;

        // 6. clear the two lists in parallel

        std::cout << std::endl;
        std::cout << "  4 --> clear the lists in parallel, using clear()" << std::endl;
        std::cout << std::endl;

        #pragma omp parallel sections \
        shared(listA, listB, std::cout)
        {
            #pragma omp section
            {
                listA.clear();
                std::cout << "--> done with listA" << std::endl;
            }
            #pragma omp section
            {
                listB.clear();
                std::cout << "--> done with listB" << std::endl;
            }
        }

        std::cout << " size of listA = " << listA.size() << std::endl;
        std::cout << " size of listB = " << listB.size() << std::endl;

        // 7. build the two lists in parallel

        std::cout << std::endl;
        std::cout << "  5 --> build the lists in parallel again, using assign" << std::endl;
        std::cout << std::endl;

        #pragma omp parallel sections \
        shared(listA, listB, std::cout)
        {
            #pragma omp section
            {
                listA.assign(I_MAX, EL_A);
                std::cout << "--> done with listA" << std::endl;
            }
            #pragma omp section
            {
                listB.assign(I_MAX, EL_B);
                std::cout << "--> done with listB" << std::endl;
            }
        }

        std::cout << " size of listA = " << listA.size() << std::endl;
        std::cout << " size of listB = " << listB.size() << std::endl;

        // 8. erase the two lists in parallel

        std::cout << std::endl;
        std::cout << "  6 --> erase the lists in parallel again, using erase(begin, end)" << std::endl;
        std::cout << std::endl;

        it1 = listA.begin();
        it2 = listA.end();
        it3 = listB.begin();
        it4 = listB.end();

        #pragma omp parallel sections \
        shared(listA, listB, std::cout)\
        shared(it1, it2, it3, it4)
        {
            #pragma omp section
            {
                listA.erase(it1, it2);
                std::cout << "--> done with listA" << std::endl;
            }
            #pragma omp section
            {
                listB.erase(it3, it4);
                std::cout << "--> done with listB" << std::endl;
            }
        }

        std::cout << " size of listA = " << listA.size() << std::endl;
        std::cout << " size of listB = " << listB.size() << std::endl;

        // 9. build the two lists in parallel

        std::cout << std::endl;
        std::cout << "  7 --> build the lists in parallel again, using assign" << std::endl;
        std::cout << std::endl;

        #pragma omp parallel sections \
        shared(listA, listB, std::cout)
        {
            #pragma omp section
            {
                listA.assign(I_MAX, EL_A);
                std::cout << "--> done with listA" << std::endl;
            }
            #pragma omp section
            {
                listB.assign(I_MAX, EL_B);
                std::cout << "--> done with listB" << std::endl;
            }
        }

        std::cout << " size of listA = " << listA.size() << std::endl;
        std::cout << " size of listB = " << listB.size() << std::endl;

        // 10. erase the two lists in parallel

        std::cout << std::endl;
        std::cout << "  8 --> erase the lists in parallel again, using erase(begin) for loop" << std::endl;
        std::cout << std::endl;

        it1 = listA.begin();
        it3 = listB.begin();

        #pragma omp parallel sections \
        shared(listA, listB, std::cout)\
        shared(it1, it3)\
        private(i)
        {
            #pragma omp section
            {
                for (i = 0; i < I_MAX; i++)
                {
                    it1 = listA.erase(it1);
                }
                std::cout << "--> done with listA" << std::endl;
            }
            #pragma omp section
            {
                for (i = 0; i < I_MAX; i++)
                {
                    it3 = listB.erase(it3);
                }
                std::cout << "--> done with listB" << std::endl;
            }
        }

        std::cout << " size of listA = " << listA.size() << std::endl;
        std::cout << " size of listB = " << listB.size() << std::endl;

        // 11. build the two lists in parallel

        std::cout << std::endl;
        std::cout << "  9 --> build the lists in parallel again, using assign" << std::endl;
        std::cout << std::endl;

        #pragma omp parallel sections \
        shared(listA, listB, std::cout)
        {
            #pragma omp section
            {
                listA.assign(I_MAX, EL_A);
                std::cout << "--> done with listA" << std::endl;
            }
            #pragma omp section
            {
                listB.assign(I_MAX, EL_B);
                std::cout << "--> done with listB" << std::endl;
            }
        }

        std::cout << " size of listA = " << listA.size() << std::endl;
        std::cout << " size of listB = " << listB.size() << std::endl;

        // 12. erase the two lists in parallel

        std::cout << std::endl;
        std::cout << " 10 --> remove all the elements from the lists in parallel, using remove(element)"
                  << std::endl;
        std::cout << std::endl;

        #pragma omp parallel sections \
        shared(listA, listB, std::cout)
        {
            #pragma omp section
            {
                listA.remove(EL_A);
                std::cout << "--> done with listA" << std::endl;
            }
            #pragma omp section
            {
                listB.remove(EL_B);
                std::cout << "--> done with listB" << std::endl;
            }
        }

        std::cout << " size of listA = " << listA.size() << std::endl;
        std::cout << " size of listB = " << listB.size() << std::endl;

        // 13. build the two lists in parallel

        std::cout << std::endl;
        std::cout << " 11 --> build the lists in parallel again, using assign" << std::endl;
        std::cout << std::endl;

        #pragma omp parallel sections \
        shared(listA, listB, std::cout)
        {
            #pragma omp section
            {
                listA.assign(I_MAX, EL_A);
                std::cout << "--> done with listA" << std::endl;
            }
            #pragma omp section
            {
                listB.assign(I_MAX, EL_B);
                std::cout << "--> done with listB" << std::endl;
            }
        }

        std::cout << " size of listA = " << listA.size() << std::endl;
        std::cout << " size of listB = " << listB.size() << std::endl;

        // 14. sort the lists in parallel

        std::cout << std::endl;
        std::cout << " 12 --> sort the lists in parallel, using sort" << std::endl;
        std::cout << std::endl;

        #pragma omp parallel sections \
        shared(listA, listB, std::cout, myComObj)
        {
            #pragma omp section
            {
                listA.sort(myComObj);
                std::cout << "--> done with listA" << std::endl;
            }
            #pragma omp section
            {
                listB.sort(myComObj);
                std::cout << "--> done with listB" << std::endl;
            }
        }

        std::cout << " size of listA = " << listA.size() << std::endl;
        std::cout << " size of listB = " << listB.size() << std::endl;

        // 15. merge the lists

        std::cout << std::endl;
        std::cout << " 13 --> merge the lists" << std::endl;
        std::cout << std::endl;

        listA.merge(listB);

        std::cout << "--> done with --> listA.merge(listB)" << std::endl;
        std::cout << " size of listA = " << listA.size() << std::endl;
        std::cout << " size of listB = " << listB.size() << std::endl;

        // 16. clear the two lists in parallel

        std::cout << std::endl;
        std::cout << " 14 --> clear the lists in parallel, after merging --> VERY SLOW" << std::endl;
        std::cout << std::endl;

        #pragma omp parallel sections \
        shared(listA, listB, std::cout)
        {
            #pragma omp section
            {
                listA.clear();
                std::cout << "--> done with listA" << std::endl;
            }
            #pragma omp section
            {
                listB.clear();
                std::cout << "--> done with listB" << std::endl;
            }
        }

        std::cout << " size of listA = " << listA.size() << std::endl;
        std::cout << " size of listB = " << listB.size() << std::endl;

    }

    return 0;
}

//======//
// FINI //
//======//
