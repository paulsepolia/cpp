
///===============//
// driver program //
///===============//

#include <iostream>
#include <iomanip>
#include <string>
#include "BaseHeader.h"
#include "BaseDefinition.h"
#include "DerivedHeader.h"
#include "DerivedDefinition.h"

using std::cin;
using std::cout;
using std::endl;
using std::fixed;
using std::setprecision;
using std::showpoint;
using std::showpos;
using std::string;

using pgg::base;
using pgg::derived;

int main()
{
    cout << fixed;
    cout << showpos;
    cout << showpoint;
    cout << setprecision(2);

    // base

    base ticketA;

    cout << " -->  1 --> " << ticketA.getTicketNumber() << endl;
    cout << " -->  2 --> " << ticketA.getTicketPrice() << endl;

    ticketA.setTicketNumber(10);
    ticketA.setTicketPrice(12.13);

    cout << " -->  3 --> " << ticketA.getTicketNumber() << endl;
    cout << " -->  4 --> " << ticketA.getTicketPrice() << endl;

    ticketA.resetTicket();

    cout << " -->  5 --> " << ticketA.getTicketNumber() << endl;
    cout << " -->  6 --> " << ticketA.getTicketPrice() << endl;

    // base

    base ticketB(11, 13.14);

    cout << " -->  7 --> " << ticketB.getTicketNumber() << endl;
    cout << " -->  8 --> " << ticketB.getTicketPrice() << endl;

    ticketA.setTicketNumber(12);
    ticketA.setTicketPrice(14.15);

    cout << " -->  9 --> " << ticketB.getTicketNumber() << endl;
    cout << " --> 10 --> " << ticketB.getTicketPrice() << endl;

    ticketB.resetTicket();

    cout << " --> 11 --> " << ticketB.getTicketNumber() << endl;
    cout << " --> 12 --> " << ticketB.getTicketPrice() << endl;

    // derived

    derived ticketC;

    cout << " --> 13 --> " << ticketC.getTicketNumber() << endl;
    cout << " --> 14 --> " << ticketC.getTicketPrice() << endl;
    cout << " --> 15 --> " << ticketC.getTicketSeason() << endl;

    ticketC.setTicketNumber(13);
    ticketC.setTicketPrice(21.22);
    ticketC.setTicketSeason("Summer");

    cout << " --> 16 --> " << ticketC.getTicketNumber() << endl;
    cout << " --> 17 --> " << ticketC.getTicketPrice() << endl;
    cout << " --> 18 --> " << ticketC.getTicketSeason() << endl;

    ticketC.resetTicket();

    cout << " --> 19 --> " << ticketC.getTicketNumber() << endl;
    cout << " --> 20 --> " << ticketC.getTicketPrice() << endl;
    cout << " --> 21 --> " << ticketC.getTicketSeason() << endl;

    // derived

    derived ticketD(14, 23.24, "WINTER");

    cout << " --> 22 --> " << ticketD.getTicketNumber() << endl;
    cout << " --> 23 --> " << ticketD.getTicketPrice() << endl;
    cout << " --> 24 --> " << ticketD.getTicketSeason() << endl;

    ticketD.setTicketNumber(15);
    ticketD.setTicketPrice(31.45);
    ticketD.setTicketSeason("AUTUM");

    cout << " --> 25 --> " << ticketD.getTicketNumber() << endl;
    cout << " --> 26 --> " << ticketD.getTicketPrice() << endl;
    cout << " --> 27 --> " << ticketD.getTicketSeason() << endl;

    ticketD.resetTicket();

    cout << " --> 28 --> " << ticketD.getTicketNumber() << endl;
    cout << " --> 29 --> " << ticketD.getTicketPrice() << endl;
    cout << " --> 30 --> " << ticketD.getTicketSeason() << endl;

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
