
//=====================//
// DerivedDefinition.h //
//=====================//

#ifndef DERIVEDDEFINITION_H
#define DERIVEDDEFINITION_H

#include <string>
#include "BaseHeader.h"
#include "BaseDefinition.h"
#include "DerivedHeader.h"

using std::string;
using pgg::base;
using pgg::derived;

namespace pgg {
// default constructor

derived::derived() : base(), ticketSeason ("NONE") {}

// non-default constructor

derived::derived(int num, double price, string season) : base(num, price), ticketSeason (season) {}

// member function

string derived::getTicketSeason() const
{
    return ticketSeason;
}

// member function

void derived::setTicketSeason(string season)
{
    ticketSeason = season;
}

// member function

void derived::resetTicket()
{
    base::resetTicket();
    ticketSeason = "NONE";
}
} // pgg

#endif // DERIVEDDEFINITION_H

//======//
// FINI //
//======//
