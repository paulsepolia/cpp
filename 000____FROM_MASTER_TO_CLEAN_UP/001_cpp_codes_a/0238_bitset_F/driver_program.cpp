
//================//
// bitset example //
//================//

#include <iostream>
#include <bitset>

using std::cout;
using std::cin;
using std::endl;
using std::bitset;

// the main function

int main()
{
    std::bitset<4> foo;    // foo: 0000....
    foo.set();              // foo: 1111....

    std::cout << foo << " as an integer is: " << foo.to_ulong() << endl;

    return 0;
}

//======//
// FINI //
//======//
