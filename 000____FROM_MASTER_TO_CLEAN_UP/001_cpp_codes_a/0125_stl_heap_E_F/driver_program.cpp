// alg_push_heap.cpp

#include <deque>
#include <vector>
#include <algorithm>
#include <functional>
#include <iostream>
#include <cmath>
#include <iomanip>

int main()
{
    long int k;
    long int i;
    const long int I_MAX = static_cast<long int>(pow(10.0, 8.0));
    const long int K_MAX = static_cast<long int>(pow(10.0, 7.0));

    // push_back double into the deque

    for (k = 0; k < K_MAX; k++) {
        std::cout << "------------------------------------------------------>> " << k << std::endl;

        //=======//
        // deque //
        //=======//

        std::deque <double>* deq1 = new std::deque<double> [1];

        // build the deque

        std::cout << " --> building the deque..." << std::endl;

        for (i = 1; i <= I_MAX; i++) {
            (*deq1).push_back(cos(static_cast<double>(i)));
        }

        // random_shuffle the deque doubles

        std::cout << " --> random shuffle the deque..." << std::endl;

        random_shuffle((*deq1).begin(), (*deq1).end());

        // make (*deq1) a heap with default less than ordering

        std::cout << " --> make heaped the deque..." << std::endl;

        make_heap((*deq1).begin(), (*deq1).end());

        // add an element to the heap

        std::cout << " --> push back -1 to the deque..." << std::endl;

        (*deq1).push_back(-1);

        // reheap the deque

        std::cout << " --> apply push_heap to the deque..." << std::endl;

        push_heap((*deq1).begin(), (*deq1).end());

        // make (*deq1) a heap with greater than ordering

        std::cout << " --> make heaped the deque again..." << std::endl;

        make_heap((*deq1).begin(), (*deq1).end(), std::greater<double>());

        // add an element to the heap

        std::cout << " --> push back 11 to the deque..." << std::endl;

        (*deq1).push_back(11);

        // push_heap

        std::cout << " --> apply push_heap to the deque..." << std::endl;

        push_heap((*deq1).begin(), (*deq1).end(), std::greater<double>());

        // sort the heap

        std::cout << " --> sorting the heap of the deque..." << std::endl;

        sort_heap((*deq1).begin(), (*deq1).end());

        // clear deque

        std::cout << " --> clear the deque..." << std::endl;

        (*deq1).clear();

        // detele the deque

        std::cout << " --> delete the deque..." << std::endl;

        delete [] deq1;

        //========//
        // vector //
        //========//

        std::vector <double>* vec1 = new std::vector<double> [1];

        // build the vector

        std::cout << " --> building the vector..." << std::endl;

        for (i = 1; i <= I_MAX; i++) {
            (*vec1).push_back(cos(static_cast<double>(i)));
        }

        // random_shuffle the vector doubles

        std::cout << " --> random shuffle the vector..." << std::endl;

        random_shuffle((*vec1).begin(), (*vec1).end());

        // make (*vec1) a heap with default less than ordering

        std::cout << " --> make heaped the vector..." << std::endl;

        make_heap((*vec1).begin(), (*vec1).end());

        // add an element to the heap

        std::cout << " --> push back -1 to the vector..." << std::endl;

        (*vec1).push_back(-1);

        // reheap the vector

        std::cout << " --> apply push_heap to the vector..." << std::endl;

        push_heap((*vec1).begin(), (*vec1).end());

        // make (*vec1) a heap with greater than ordering

        std::cout << " --> make heaped the vector again..." << std::endl;

        make_heap((*vec1).begin(), (*vec1).end(), std::greater<double>());

        // add an element to the heap

        std::cout << " --> push back 11 to the vector..." << std::endl;

        (*vec1).push_back(11);

        // push_heap

        std::cout << " --> apply push_heap to the vector..." << std::endl;

        push_heap((*vec1).begin(), (*vec1).end(), std::greater<double>());

        // sort the heap

        std::cout << " --> sorting the heap of the vector..." << std::endl;

        sort_heap((*vec1).begin(), (*vec1).end());

        // clear vector

        std::cout << " --> clear the vector..." << std::endl;

        (*vec1).clear();

        // detele the vector

        std::cout << " --> delete the vector..." << std::endl;

        delete [] vec1;
    }

    int sentinel;
    std::cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
