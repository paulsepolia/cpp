//===============//
// qsort example //
//===============//

#include <cstdlib>
#include <iostream>

using std::cin;
using std::cout;
using std::endl;

// the array

int values[] = { 40, 10, 100, 90, 20, 25 };

// the comparing function

int compareA (const void * a, const void * b)
{
    return ( *(int*)a - *(int*)b );
}

// the comparing function

int compareB (const void * a, const void * b)
{
    return ( *(double*)a - *(double*)b );
}

// the main function

int main ()
{
    // the array

    double *p = new double [100];

    // build the array

    for (int i = 0; i < 100; i++) {
        p[i] = 100-i;
    }

    // some output

    cout << "p[1] = " << p[1] << endl;

    // sort the array

    int n;

    qsort (values, 6, sizeof(int), compareA);

    // some output

    for (n = 0; n < 6; n++) {
        cout << values[n] << endl;
    };

    // sort the array

    qsort (&p[0], 100, sizeof(double), compareB);

    // some output

    for (n=0; n < 100; n++) {
        cout << p[n] << endl;
    }

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
