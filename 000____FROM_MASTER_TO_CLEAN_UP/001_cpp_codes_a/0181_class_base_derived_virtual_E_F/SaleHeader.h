//==============//
// SaleHeader.h //
//==============//

#ifndef SALE_HEADER
#define SALE_HEADER

namespace pgg {
class Sale {
public:
    Sale(); // default constructor
    explicit Sale(double); // non-default constructor
    double getPrice() const;
    void setPrice(double);
    virtual double bill() const;
    double savings(const Sale&) const;
private:
    double price;
};
} // pgg

#endif // SALE_HEADER
