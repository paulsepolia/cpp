
//===================//
// ClassA definition //
//===================//

#include <iostream>
#include "classa.h"

using std::cout;
using std::endl;
using pgg::ClassA;

// constructor

ClassA::ClassA() : a(0.0), b(0.0)
{
    c = new double;
    *c = 0.0;
}

// constructor with two arguments

ClassA::ClassA(double aLoc, double bLoc) : a(aLoc), b(bLoc)
{
    c = new double;
    *c = 0.0;
}

// constructor with three arguments

ClassA::ClassA(double aLoc, double bLoc, double cLoc) : a(aLoc), b(bLoc)
{
    c  = new double;
    *c = cLoc;
}

// copy constructor

ClassA::ClassA(const ClassA& obj)
{
    a = obj.a;
    b = obj.b;
    c = new double;
    *c = *(obj.c);
}


// destructor

ClassA::~ClassA()
{
    delete c;
    c = NULL;
}

// member function

void ClassA::print() const
{
    cout << "I am the virtual void print() in ClassA : a  = " << a  << endl;
    cout << "I am the virtual void print() in ClassA : b  = " << b  << endl;
    cout << "I am the virtual void print() in ClassA : *c = " << *c << endl;
}

// member function

double ClassA::add() const
{
    cout << "I am the virtual double add() in ClassA: a+b+(*c) = ";

    return (a+b+(*c));
}

// member function

double ClassA::multiply() const
{
    cout << "I am the virtual double multiply() in ClassA : a*b*(*c) = ";

    return (a*b*(*c));
}

// member function

void ClassA::locA() const
{
    cout << "I am the locA function." << endl;
}

// member function

double ClassA::value() const
{
    return a;
}

// < operator overload
// member function function

bool ClassA::operator < (const InterfaceA& obj) const
{
    return (this->value() < obj.value());
}

// <= operator overload
// member function function

bool ClassA::operator <= (const InterfaceA& obj) const
{
    return (this->value() <= obj.value());
}

// > operator overload
// member function function

bool ClassA::operator > (const InterfaceA& obj) const
{
    return (this->value() > obj.value());
}

// >= operator overload
// member function function

bool ClassA::operator >= (const InterfaceA& obj) const
{
    return (this->value() >= obj.value());
}

// == operator overload
// member function function

bool ClassA::operator == (const InterfaceA& obj) const
{
    bool tmp;

    tmp = (this->value() == obj.value());

    return tmp;
}

// == operator overload
// member function function

bool ClassA::operator == (const ClassA& obj) const
{
    bool tmp;

    tmp = (this->value() == obj.value()) && ((this->b) == obj.b) && (*(this->c) == *(obj.c));

    return tmp;
}

// + operator overload
// member function function

ClassA ClassA::operator + (const ClassA& obj) const
{
    return ClassA(this->value() + obj.value(), this->b + obj.b);
}

// - operator overload
// member function function

ClassA ClassA::operator - (const ClassA& obj) const
{
    return ClassA(this->value() - obj.value(), this->b - obj.b);
}

// * operator overload
// member function function

ClassA ClassA::operator * (const ClassA& obj) const
{
    return ClassA(this->value() * obj.value(), this->b * obj.b);
}

// / operator overload
// member function function

ClassA ClassA::operator / (const ClassA& obj) const
{
    return ClassA(this->value() / obj.value(), this->b / obj.b);
}

// ++ operator overload - prefix
// member function

const ClassA ClassA::operator++()
{
    return ClassA(++a, b);
}

// ++ operator overload - postfix
// member function

const ClassA ClassA::operator++(int)
{
    return ClassA(++a, b);
}

// -- operator overload - prefix
// member function

const ClassA ClassA::operator--()
{
    return ClassA(--a, b);
}

// -- operator overload - postfix
// member function

const ClassA ClassA::operator--(int)
{
    return ClassA(--a, b);
}

// - operator overload
// member function

const ClassA ClassA::operator-() const
{
    return ClassA(-a, -b);
}

// += operator overload
// member function

const ClassA ClassA::operator += (const ClassA& obj)
{
    return ClassA(a + obj.value(), b);
}

// -= operator overload
// member function

const ClassA ClassA::operator -= (const ClassA& obj)
{
    return ClassA(a - obj.value(), b);
}

// *= operator overload
// member function

const ClassA ClassA::operator *= (const ClassA& obj)
{
    return ClassA(a * obj.value(), b);
}

// /= operator overload
// member function

const ClassA  ClassA::operator /= (const ClassA& obj)
{
    return ClassA(a / obj.value(), b);
}

// = operator overload
// member function

const ClassA ClassA::operator = (const ClassA& obj)
{
    this->a = obj.a;
    this->b = obj.b;
    this->c = new double;
    *(this->c) = *(obj.c);

    return *this;
}

//======//
// FINI //
//======//
