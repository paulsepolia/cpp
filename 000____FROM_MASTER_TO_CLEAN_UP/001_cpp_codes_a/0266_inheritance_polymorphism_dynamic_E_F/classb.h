
//====================//
// ClassB declaration //
//====================//

#ifndef CLASS_B
#define CLASS_B

#include "ainterface.h"

namespace pgg {
class ClassB : public InterfaceA {
public:

    ClassB();
    ClassB(double, double);
    ClassB(double, double, double);
    ClassB(const ClassB&);
    virtual ~ClassB();
    virtual void print() const;
    virtual double add() const;
    virtual double multiply() const;
    void locB() const;
    virtual double value() const;

    // operator overload
    // <, <=, >, >=, ==

    virtual bool operator <  (const InterfaceA&) const;
    virtual bool operator <= (const InterfaceA&) const;
    virtual bool operator >  (const InterfaceA&) const;
    virtual bool operator >= (const InterfaceA&) const;
    virtual bool operator == (const InterfaceA&) const;
    virtual bool operator == (const ClassB&) const;

    // operator overload
    // +, -, *, /

    virtual ClassB operator +  (const ClassB&) const;
    virtual ClassB operator -  (const ClassB&) const;
    virtual ClassB operator *  (const ClassB&) const;
    virtual ClassB operator /  (const ClassB&) const;

    // operator overload
    // ++, --, -

    virtual const ClassB operator ++ ();
    virtual const ClassB operator ++ (int);
    virtual const ClassB operator -- ();
    virtual const ClassB operator -- (int);
    virtual const ClassB operator -  () const;

    //operator overload
    // +=, -=, *=, /=

    const ClassB operator += (const ClassB&);
    const ClassB operator -= (const ClassB&);
    const ClassB operator *= (const ClassB&);
    const ClassB operator /= (const ClassB&);

    // operator overload
    // =

    virtual const ClassB operator = (const ClassB&);

private:

    double a;
    double b;
    double *c;
};

}
#endif // CLASS_A

//======//
// FINI //
//======//
