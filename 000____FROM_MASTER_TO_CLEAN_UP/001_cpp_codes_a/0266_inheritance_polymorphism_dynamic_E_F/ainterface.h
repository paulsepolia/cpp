
//====================//
// an interface class //
//====================//

// contains pure virtual functions
// and no member variables

#ifndef INTERFACE_A_H
#define INTERFACE_A_H

namespace pgg {
class InterfaceA {
public:

    InterfaceA() {};
    virtual ~InterfaceA() {};
    virtual void print() const = 0;
    virtual double add() const = 0;
    virtual double multiply() const = 0;
    virtual double value() const = 0;

    // operator overload
    // <, <=, >, >=, ==

    virtual bool operator <  (const InterfaceA&) const = 0;
    virtual bool operator <= (const InterfaceA&) const = 0;
    virtual bool operator >  (const InterfaceA&) const = 0;
    virtual bool operator >= (const InterfaceA&) const = 0;
    virtual bool operator == (const InterfaceA&) const = 0;

};
}
#endif // INTERFACE_A_H

//======//
// FINI //
//======//
