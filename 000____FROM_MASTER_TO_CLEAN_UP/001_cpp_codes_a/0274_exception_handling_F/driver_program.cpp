
#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int main()
{
    int a1;
    int a2;

    try {
        a2 = 10;
        cout << " a2 = " << a2 << endl;
        cout << " Enter a1:";
        cin >> a1;
        cout << " a1 = " << a1 << endl;

        if (a1 <=0) {
            throw a2;
        } else {
            cout << "All are okay!" << endl;
        }
    } catch(int e) {
        cout << "a1 = " << a1 << endl;
        cout << "a2 = " << a2 << endl;
        cout << "I catch an expeption" << endl;
    }

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

