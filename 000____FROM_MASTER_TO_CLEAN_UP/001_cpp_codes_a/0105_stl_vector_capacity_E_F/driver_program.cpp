
// vector: capacity
// The capacity member function for vectors

#include <iostream>
#include <iomanip>
#include <vector>

using namespace std;

int main()
{
    vector<int> vecA;
    vector<int>::size_type n0 = 12345;
    vector<int>::size_type n1;
    const long int SIZE_MAX_VECA = 400000000L;
    const int K_MAX_RUN = 1000000L;

    cout << "vecA.size()       vecA.capacity() " << endl;

    for (long int k = 0L; k < K_MAX_RUN; k++) {
        cout << " -------------------------------------------------->>> " << k << endl;

        for (long int i = 0L; i < SIZE_MAX_VECA; i++) {
            n1 = vecA.capacity();

            if (n1 != n0) {
                cout << setw(2) << " size = " << setw(20)
                     << vecA.size() << "  capacity =   " << setw(20) << n1 << endl;
                n0 = n1;
            }

            vecA.push_back(123); // vecA.size() increases by 1
        }

        cout << " ------------------->> Clear the vector " << endl;

        vecA.clear();

        cout << " -------------------->> Shrink the capacity to fit the size " << endl;

        vecA.shrink_to_fit();
    }

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

