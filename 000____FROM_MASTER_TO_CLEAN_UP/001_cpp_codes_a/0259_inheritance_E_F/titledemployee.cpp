//========================================//
// definition of the class TitledEmployee //
//========================================//

#include <string>
#include <iostream>
#include "titledemployee.h"

using std::string;
using std::cout;
using std::endl;

namespace pgg {
// default constructor

TitledEmployee::TitledEmployee() : SalariedEmployee(), title("no title yet") {}

// constructor with three arguments

TitledEmployee::TitledEmployee(string theName, string theTitle, string theNumber, double theWeeklyPay)
    : SalariedEmployee(theName + " " + theTitle, theNumber, theWeeklyPay), title (theTitle)  {}

// member function

string TitledEmployee::getTitle() const
{
    return title;
}

// member function

void TitledEmployee::setTitle(string newTitle)
{
    title = newTitle;
}

// member function

void TitledEmployee::setName(string newName)
{
    Employee::setName(newName + " " + title);
}

} // pgg

