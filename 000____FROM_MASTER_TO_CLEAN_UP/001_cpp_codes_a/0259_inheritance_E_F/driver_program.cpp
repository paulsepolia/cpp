
//=============//
// inheritance //
//=============//

#include <iostream>
#include <iomanip>
#include "hourlyemployee.h"
#include "salariedemployee.h"
#include "titledemployee.h"

using std::cout;
using std::cin;
using std::endl;
using std::fixed;
using std::setprecision;
using std::showpos;
using std::showpoint;
using pgg::Employee;
using pgg::HourlyEmployee;
using pgg::SalariedEmployee;
using pgg::TitledEmployee;

// the main function

int main()

{
    // adjust the output format

    cout << fixed;
    cout << setprecision(10);
    cout << showpoint;
    cout << showpos;

    // local variable and parameters

    // 1

    HourlyEmployee joe;

    joe.setName("Pavlos Joe");
    joe.setSsn("123-45-6789");
    joe.setRate(20.50);
    joe.setHours(40);

    cout << "Check for " << joe.getName()
         << " for " << joe.getHours() << " hours." << endl;

    joe.printCheck();

    cout << endl;

    // 2

    HourlyEmployee paul;

    cout << "Check for " << paul.getName()
         << " for " << paul.getHours() << " hours." << endl;

    paul.printCheck();

    cout << endl;

    // 3

    SalariedEmployee boss("Mr Big", "897-65-4321", 10500.50);

    cout << "Check for " << boss.getName() << endl;

    cout << boss.getName() << endl;
    cout << boss.getNetPay() << endl;
    cout << boss.getSalary() << endl;
    cout << boss.getSsn() << endl;

    boss.printCheck();

    // 4

    TitledEmployee andy2("ANDY", "MALAKAS", "123", 100);

    cout << "Check for " << andy2.getName()
         << " for " << andy2.getNetPay() << " Dollars." << endl;

    andy2.printCheck();

    cout << endl;

    // 5

    TitledEmployee andy3;

    string name = "ANDY";
    andy3.setTitle("MALAKAS");
    andy3.setName(name);
    andy3.setSalary(1111111);
    andy3.setSsn("123-100-200");

    cout << "Check for " << andy3.getName()
         << " for " << andy2.getNetPay() << " Dollars." << endl;

    andy3.printCheck();

    cout << endl;

    // 6

    Employee andy;

    cout << "Check for " << andy.getName()
         << " for " << andy.getNetPay() << " Dollars." << endl;

    andy.printCheck();

    cout << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
