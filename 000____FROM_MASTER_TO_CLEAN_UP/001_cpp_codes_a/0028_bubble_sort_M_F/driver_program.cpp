//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/10/13              //
// Functions: Bubble Sort        //
//===============================//

#include <iostream>
#include <cmath>
#include <iomanip>
#include <ctime>

using namespace std;

// 1. function declaration

void bubbleSort(long double* list, long int length);

// 2. the main function

int main()
{
    const long int DIM_ARR = 100000;
    const int K_MAX = 10;
    long double* arrayA = new long double [DIM_ARR];
    long int i;
    int k;
    clock_t t;

    cout << fixed;
    cout << showpoint;
    cout << setprecision(15);
    cout << showpos;

    for(i = 0; i < DIM_ARR; i++) {
        arrayA[i] = cos(static_cast<double>(i));
    }

    for (k = 1; k < K_MAX; k++) {
        cout << " --------------------------------------------> " << k << endl;

        t = clock();

        bubbleSort(arrayA, DIM_ARR);

        t = clock() - t;

        cout << " It took (seconds) = " << static_cast<double>(t)/CLOCKS_PER_SEC << endl;
        cout << " array[0]          = " << arrayA[0] << endl;
        cout << " array[1]          = " << arrayA[1] << endl;
        cout << " array[2]          = " << arrayA[2] << endl;
        cout << " array[DIM_ARR-3]  = " << arrayA[DIM_ARR-3] << endl;
        cout << " array[DIM_ARR-2]  = " << arrayA[DIM_ARR-2] << endl;
        cout << " array[DIM_ARR-1]  = " << arrayA[DIM_ARR-1] << endl;
    }

    return 0;
}

// 3. function declaration

void bubbleSort(long double* list, long int length)
{
    long double temp;
    long int iteration;
    long int index;

    for (iteration = 1; iteration < length; iteration++) {
        for (index = 0; index < length - iteration; index++) {
            if (list[index] > list[index+1]) {
                temp = list[index];
                list[index] = list[index + 1];
                list[index + 1] = temp;
            }
        }
    }

}
//======//
// FINI //
//======//
