//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/10/31              //
// Functions: map                //
// Algorithms: erase             //
//===============================//

#include <iostream>
#include <map>

using namespace std;

int main ()
{
    // variables declaration

    map<long int, double> mapA;
    map<long int, double> mapB;
    map<long int, double> mapC;
    map<long int, double> mapD;
    long int i;
    const long int I_MAX = 10000000;
    const long int I_MAX_B = 10000;
    int j;
    const int J_MAX = 1000;
    map<long int, double>::iterator itA;
    map<long int, double>::iterator itB;

    // main code

    for (j = 0; j < J_MAX; j++) {
        cout << "---------------------------------------------------------------> " << j+1 << endl;

        // insert using the pair<long int, double> mechanism

        cout << " 1 --> Build the 1st map" << endl;

        for (i = 0; i < I_MAX; i++) {
            mapA.insert(pair<long int, double>(i, static_cast<double>(i)));
        }

        cout << " 2 --> Build the 2nd map" << endl;

        for (i = I_MAX-1; i >= 0; i--) {
            mapB.insert(pair<long int, double>(i, static_cast<double>(i)));
        }

        cout << " 3 --> Build the 3rd map, through the 1st map" << endl;

        for (i = 0; i < I_MAX_B; i++) {
            mapC.insert(mapA.begin(), mapA.find(i));
        }

        cout << " 4 --> Build the 4th map, through the 2nd map" << endl;

        for (i = 0; i < I_MAX_B; i++) {
            mapD.insert(mapB.begin(), mapB.find(i));
        }

        // compare the maps

        cout << " 5 --> Compare the 1st and the 2nd maps" << endl;

        if (mapA == mapB) {
            cout << " --> Maps are equal" << endl;
        } else {
            cout << " --> Maps are not equal" << endl;
        }


        cout << " 6 --> Compare the 3rd and the 4th map" << endl;

        if (mapC == mapD) {
            cout << " --> Maps are equal" << endl;
        } else {
            cout << " --> Maps are not equal" << endl;
        }

        // erase by key

        cout << " 7 --> Erase key by key the 1st map" << endl;

        for (i = 0; i < I_MAX; i++) {
            itA = mapA.find(i);
            mapA.erase(itA);
        }

        cout << " 8 --> Erase key by key the 2nd map" << endl;

        for (i = 0; i < I_MAX; i++) {
            itB = mapB.find(i);
            mapB.erase(itB);
        }

        cout << " 9 --> Compare again the maps" << endl;

        if (mapA == mapB) {
            cout << " --> Maps are equal" << endl;
        } else {
            cout << " --> Maps are not equal" << endl;
        }

        if (mapC == mapD) {
            cout << " --> Maps are equal" << endl;
        } else {
            cout << " --> Maps are not equal" << endl;
        }

        // Free up the RAM

        cout << "10 --> Free up the RAM" << endl;

        mapA.clear();
        mapB.clear();
        mapC.clear();
        mapD.clear();
    }

    return 0;
}

// end
