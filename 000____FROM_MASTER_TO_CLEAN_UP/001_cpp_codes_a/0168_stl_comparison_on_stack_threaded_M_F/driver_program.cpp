//======================//
// STL clear and delete //
//======================//

#include "includes.h"
#include "bench_fun.h"

// the main program

int main()
{
    // variables and parameters

    const long I_MAX = static_cast<long>(pow(10.0, 7.0));
    const long K_MAX = static_cast<long>(pow(10.0, 6.0));
    long k;
    int i;
    clock_t t1;
    clock_t t2;
    double tMainAll = 0.0;
    const int NUM_THREADS = 2;
    double tMain[NUM_THREADS];
    double tFun[NUM_THREADS];
    double tFunAll = 0.0;
    vector<thread> vecThreads;

    // main bench loop

    for (k = 0; k < K_MAX; k++) {
        cout << endl;
        cout << "------------------------------------------------------------>>> " << k << endl;
        cout << endl;

        // set to zero the time counters

        for (i = 0; i < NUM_THREADS; i++) {
            tMain[i] = 0.0;
            tFun[i] = 0.0;
        }

        // spawn threads

        for (i = 0; i < NUM_THREADS; i++) {
            vecThreads.push_back(thread(&bench_fun, I_MAX, ref(tFun[i])));
        }

        // join threads

        for (i = 0; i < NUM_THREADS; i++) {
            t1 = clock();
            vecThreads[i].join();
            t2 = clock();
            tMain[i] = (t2-t1)/static_cast<double>(CLOCKS_PER_SEC);
        }

        // the time in main and in function

        for (i = 0; i < NUM_THREADS; i++) {
            tMainAll = tMainAll + tMain[i];
            tFunAll = tFunAll + tFun[i];
        }

        // clear vector

        cout << "xx1 --> clear the threads vector ..." << endl;

        vecThreads.clear();

        cout << "xx2 --> total time for benchmark function in main --> " << tMainAll << endl;
        cout << "xx3 --> total time for benchmark function         --> " << tFunAll << endl;
        cout << "xx4 --> total deallocation time on stack          --> " << tMainAll - tFunAll << endl;

    }

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
