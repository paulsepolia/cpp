//=====================//
// unique_lock example //
//=====================//

#include <iostream>       // cout
#include <thread>         // thread
#include <mutex>          // mutex, nique_lock

using std::cout;
using std::cin;
using std::endl;
using std::thread;
using std::mutex;
using std::unique_lock;

// a mutex

mutex mtx;           // mutex for critical section

// function

void print_block (int n, char c)
{
    // critical section (exclusive access to std::cout signaled by lifetime of lck):

    unique_lock<mutex> lck (mtx);

    for (int i=0; i < n; ++i) {
        cout << c;
    }

    cout << endl;
}

int main ()
{
    thread th1 (print_block,50,'*');
    thread th2 (print_block,50,'$');

    th1.join();
    th2.join();

    return 0;
}

// end
