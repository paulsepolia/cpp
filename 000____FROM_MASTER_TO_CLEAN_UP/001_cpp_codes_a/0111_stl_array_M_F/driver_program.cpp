//=======//
// array //
//=======//

#include <iostream>
#include <array>
#include <cmath>
#include <ctime>
#include <iomanip>

using namespace std;

int main()
{
    long int k;
    const long int K_MAX = 2L * static_cast<long int>(pow(10.0, 1.0));
    const long int ARR_DIM = 200000000L;
    long int j;
    const long int J_MAX = 2000000L;
    clock_t t1;
    clock_t t2;

    cout << fixed;
    cout << setprecision(5);
    cout << noshowpos;

    for (j = 1; j <= J_MAX; j++) {
        cout << "---------------------------------------------------------------->>> " << j << endl;

        cout << endl;
        cout << "  1 --> Declare the array '*parrayA' of size " << ARR_DIM << endl;

        array<double, ARR_DIM> *parrayA = new array<double, ARR_DIM>;

        cout << "  2 --> Fill the array '*parrayA' " << K_MAX << " times with various elements" << endl;

        t1 = clock();

        for (k = 1; k <= K_MAX; k++) {
            (*parrayA).fill(static_cast<double>(k+j));
        }

        t2 = clock();

        cout << endl;
        cout << "  ------------------------------------------------> time used = "
             << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;
        cout << endl;

        cout << "  3 --> Some '*parrayA' elements" << endl;
        cout << endl;
        cout << "  --->> (*parrayA)[0] = " << (*parrayA)[0] << endl;
        cout << "  --->> (*parrayA)[1] = " << (*parrayA)[1] << endl;
        cout << "  --->> (*parrayA)[2] = " << (*parrayA)[2] << endl;
        cout << endl;

        cout << "  4 --> Declare the array '*parrayB' of size " << ARR_DIM << endl;

        array<double, ARR_DIM> *parrayB = new array<double, ARR_DIM>;

        cout << "  5 --> Fill the array '*parrayB' " << K_MAX << " times with various elements" << endl;

        t1 = clock();

        for (k = 1; k <= K_MAX; k++) {
            (*parrayB).fill(static_cast<double>(k+j+1));
        }

        t2 = clock();

        cout << endl;
        cout << "  ------------------------------------------------> time used = "
             << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;
        cout << endl;

        cout << "  6 --> Some '*parrayB' elements" << endl;
        cout << endl;
        cout << "  --->> (*parrayB)[0] = " << (*parrayB)[0] << endl;
        cout << "  --->> (*parrayB)[1] = " << (*parrayB)[1] << endl;
        cout << "  --->> (*parrayB)[2] = " << (*parrayB)[2] << endl;
        cout << endl;

        cout << "  7 --> Swap the two arrays " << K_MAX + 1 + (K_MAX%2)*(-1) << " times " << endl;

        t1 = clock();

        for (k = 1; k <= K_MAX + 1 + (K_MAX%2)*(-1); k++) {
            (*parrayA).swap(*parrayB);
        }

        t2 = clock();

        cout << endl;
        cout << "  ------------------------------------------------> time used = "
             << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;
        cout << endl;

        cout << "  8 --> Some '*parrayA' elements" << endl;
        cout << endl;
        cout << "  --->> (*parrayA)[0] = " << (*parrayA)[0] << endl;
        cout << "  --->> (*parrayA)[1] = " << (*parrayA)[1] << endl;
        cout << "  --->> (*parrayA)[2] = " << (*parrayA)[2] << endl;
        cout << endl;

        cout << "  9 --> Some '*parrayB' elements" << endl;
        cout << endl;
        cout << "  --->> (*parrayB)[0] = " << (*parrayB)[0] << endl;
        cout << "  --->> (*parrayB)[1] = " << (*parrayB)[1] << endl;
        cout << "  --->> (*parrayB)[2] = " << (*parrayB)[2] << endl;
        cout << endl;

        cout << " 10 --> Delete the arrays" << endl;
        cout << endl;

        delete parrayA;
        delete parrayB;
    }

    int sentinel;
    cin >> sentinel;

    return 0;
}

// FINI
