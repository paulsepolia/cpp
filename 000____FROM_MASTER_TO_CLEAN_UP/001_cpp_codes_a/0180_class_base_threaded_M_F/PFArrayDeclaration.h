//======================//
// PFArrayDeclaration.h //
//======================//

// objects of this class are partially filled arrays of P

#ifndef PFARRAYD_H
#define PFARRAYD_H

namespace pgg {
template <typename T, typename P>
class PFArrayD {
public:

    PFArrayD(); // default constructor
    explicit PFArrayD(T); // a non-default constructor
    PFArrayD(const PFArrayD&); // copy constructor

    void addElement(P);
    bool full() const;
    T getCapacity() const;
    T getNumberUsed() const;
    void emptyArray()
    {
        used = 0;
    };

    P& operator [] (T); // overload [] operator
    PFArrayD& operator = (const PFArrayD&); // overload assignment

    ~PFArrayD(); // destructor

private:

    P *a;
    T capacity;
    T used;
};
} // pgg

#endif // PFARRAYD_H

//======//
// FINI //
//======//
