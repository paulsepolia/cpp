
//==============================//
// definition of the class Sale //
//==============================//

#include <iostream>
#include <cstdlib>
#include "sale.h"

using std::cout;
using std::endl;
using std::cin;

namespace pgg {
// default constructor

Sale::Sale() : price (0) {}

// constructor with one argument

Sale::Sale(double thePrice)
{
    if (thePrice >= 0) {
        price = thePrice;
    } else {
        cout << "Error: Cannot have a negative price!" << endl;
        cout << "Enter an integer to exit: ";
        int sentinel;
        cin >> sentinel;
        exit(1);
    }
}

// virtual member function

double Sale::bill() const
{
    return price;
}

// member function

double Sale::getPrice() const
{
    return price;
}

// member function

void Sale::setPrice(double newPrice)
{
    if (newPrice >= 0) {
        price = newPrice;
    } else {
        cout << "Error: Cannot have a negative price!" << endl;
        cout << "Enter an integer to exit: ";
        int sentinel;
        cin >> sentinel;
        exit(1);
    }
}

// member function

double Sale::savings(const Sale& other) const
{
    return (bill() - other.bill());
}

// < operator overload as external function

bool operator < (const Sale& first, const Sale& second)
{
    return (first.bill() < second.bill());
}

} // pgg

//======//
// FINI //
//======//
