
//===============================//
// declaration of the class Sale //
//===============================//

#ifndef SALE_H
#define SALE_H

namespace pgg {
class Sale {
public:

    Sale();
    explicit Sale(double);
    double getPrice() const;
    void setPrice(double);
    virtual double bill() const;
    double savings(const Sale&) const;

private:

    double price;
};

bool operator < (const Sale&, const Sale&);

} // pgg

#endif // SALE_H

//======//
// FINI //
//======//
