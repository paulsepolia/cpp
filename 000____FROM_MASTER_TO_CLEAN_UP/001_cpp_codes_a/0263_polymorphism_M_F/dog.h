
//=======================//
// Dog class declaration //
//=======================//

#ifndef DOG_H
#define DOG_H

#include <string>
#include "pet.h"

using std::string;

namespace pgg {
class Dog : public Pet {
public:
    Dog();
    string breed;
    virtual void print() const;
    virtual ~Dog();
};

} // pgg

#endif // DOG_H

//======//
// FINI //
//======//

