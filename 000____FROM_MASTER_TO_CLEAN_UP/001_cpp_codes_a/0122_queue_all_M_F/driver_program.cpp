//============//
// STL; queue //
//============//

#include <iostream>
#include <iomanip>
#include <queue>
#include <ctime>
#include <cmath>

int main ()
{
    // 1. variables and parameters

    const long int I_MAX = static_cast<long int>(pow(10.0, 7.9));
    const long int J_MAX = static_cast<long int>(pow(10.0, 3.0)) + 1;
    const long int K_MAX = static_cast<long int>(pow(10.0, 7.0));
    long int i;
    long int j;
    long int k;
    clock_t t1;
    clock_t t2;

    for (k = 0; k < K_MAX; k++) {
        std::queue<double> *queD1 = new std::queue<double> [1];
        std::queue<double> *queD2 = new std::queue<double> [1];
        std::queue<double> *queD3 = new std::queue<double> [1];
        std::queue<double> *queD4 = new std::queue<double> [1];
        std::queue<double> *queD5 = new std::queue<double> [1];
        std::queue<long double> *queL1 = new std::queue<long double> [1];
        std::queue<long double> *queL2 = new std::queue<long double> [1];
        std::queue<long double> *queL3 = new std::queue<long double> [1];
        std::queue<long double> *queL4 = new std::queue<long double> [1];
        std::queue<long double> *queL5 = new std::queue<long double> [1];

        std::cout << std::endl;
        std::cout << "------------------------------------------------------------------>> " << k << std::endl;
        std::cout << std::endl;

        //=========================//
        // PARALLEL SECTIONS --> A //
        //=========================//

        #pragma omp parallel sections default(none)\
        shared(queD1, queL1, std::cout)\
        private(t1, t2, i)
        {
            #pragma omp section
            {
                // 2. push doubles into queue

                t1 = clock();
                std::cout << " -->  1 --> push elements in to queue --> (*queD1)" << std::endl;
                for (i = 0; i < I_MAX; ++i)
                {
                    (*queD1).push(cos(static_cast<double>(i)));
                }
                t2 = clock();
                std::cout << " --> done with --> (*queD1) --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;

                // 3. pop the doubles until the queue is empty

                t1 = clock();
                std::cout << " -->  2 --> pop elements from the queue --> (*queD1)" << std::endl;
                while (!(*queD1).empty())
                {
                    (*queD1).pop();
                }
                t2 = clock();
                std::cout << " --> done with --> (*queD1) --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 4. push long ints into queue

                t1 = clock();
                std::cout << " -->  3 --> push elements in to queue --> (*queL1)" << std::endl;
                for (i = 0; i < I_MAX; ++i)
                {
                    (*queL1).push(i);
                }
                t2 = clock();
                std::cout << " --> done with --> (*queL1) --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;

                // 5. pop the long ints until the queue is empty

                t1 = clock();
                std::cout << " -->  4 --> pop elements from the queue --> (*queL1)" << std::endl;
                while (!(*queL1).empty())
                {
                    (*queL1).pop();
                }
                t2 = clock();
                std::cout << " --> done with --> (*queL1) --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
        }

        //=========================//
        // PARALLEL SECTIONS --> B //
        //=========================//

        #pragma omp parallel sections default(none)\
        shared(queD1, queL1, std::cout)\
        private(t1, t2, i)
        {
            #pragma omp section
            {
                // 6. push doubles into queue

                t1 = clock();
                std::cout << " -->  5 --> push elements in to queue --> (*queD1)" << std::endl;
                for (i = 0; i < I_MAX; ++i)
                {
                    (*queD1).push(cos(static_cast<double>(i)));
                }
                t2 = clock();
                std::cout << " --> done with --> (*queD1) --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;

                // 7. delete queue

                t1 = clock();
                std::cout << " -->  6 --> delete the queue --> (*queD1)" << std::endl;
                delete [] queD1;
                t2 = clock();
                std::cout << " --> done with --> (*queD1) --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 8. push long ints into queue

                t1 = clock();
                std::cout << " -->  7 --> push elements in to queue --> (*queL1)" << std::endl;
                for (i = 0; i < I_MAX; ++i)
                {
                    (*queL1).push(i);
                }
                t2 = clock();
                std::cout << " --> done with --> (*queL1) --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;

                // 9. delete queue

                t1 = clock();
                std::cout << " -->  8 --> delete the queue --> (*queL1)" << std::endl;
                delete [] queL1;
                t2 = clock();
                std::cout << " --> done with --> (*queL1) --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
        }

        //=========================//
        // PARALLEL SECTIONS --> C //
        //=========================//

        #pragma omp parallel sections default(none)\
        shared(queD3, queL3, queD2, queL2, std::cout)\
        private(t1, t2, i)
        {
            #pragma omp section
            {
                // 10. push doubles into the queues

                t1 = clock();
                std::cout << " -->  9 --> push elements in to queue --> (*queD3)" << std::endl;
                for (i = 0; i < I_MAX; ++i)
                {
                    (*queD3).push(cos(static_cast<double>(i)));
                }
                t2 = clock();
                std::cout << " --> done with --> (*queD3) --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 11. push long ints into queue

                t1 = clock();
                std::cout << " --> 10 --> push elements in to queue --> (*queL3)" << std::endl;
                for (i = 0; i < I_MAX; ++i)
                {
                    (*queL3).push(i);
                }
                t2 = clock();
                std::cout << " --> done with --> (*queL3) --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 12. push doubles into the queues

                t1 = clock();
                std::cout << " --> 11 --> push elements in to queue --> (*queD2)" << std::endl;
                for (i = 0; i < I_MAX; ++i)
                {
                    (*queD2).push(sin(static_cast<double>(i)));
                }
                t2 = clock();
                std::cout << " --> done with --> (*queD2) --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 13. push long ints into queue

                t1 = clock();
                std::cout << " --> 12 --> push elements in to queue --> (*queL2)" << std::endl;
                for (i = 0; i < I_MAX; ++i)
                {
                    (*queL2).push(I_MAX-i);
                }
                t2 = clock();
                std::cout << " --> done with --> (*queL2) --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
        }

        //=========================//
        // PARALLEL SECTIONS --> D //
        //=========================//

        #pragma omp parallel sections shared(queD3, queL3, queD2, queL2, std::cout)\
        private(t1, t2, j)
        {
            #pragma omp section
            {
                t1 = clock();
                std::cout << " --> 13 --> swap --> swap((*queD2),(*queD3))" << std::endl;
                for (j = 0; j < J_MAX; j++)
                {
                    std::swap((*queD2),(*queD3));
                }
                t2 = clock();
                std::cout << " --> done with --> swap((*queD2),(*queD3)); --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                t1 = clock();
                std::cout << " --> 14 --> swap --> swap((*queL2),(*queL3))" << std::endl;
                for (j = 0; j < J_MAX; j++)
                {
                    std::swap((*queL2),(*queL3));
                }
                t2 = clock();
                std::cout << " --> done with --> swap((*queL2),(*queL3)); --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
        }

        //=========================//
        // PARALLEL SECTIONS --> E //
        //=========================//

        #pragma omp parallel sections default(none)\
        shared(queD3, queL3, queD2, queL2, std::cout)\
        private(t1, t2)
        {
            #pragma omp section
            {
                t1 = clock();
                std::cout << " --> 15 --> delete [] queD2; " << std::endl;
                delete [] queD2;
                t2 = clock();
                std::cout << " --> done with --> delete [] queD2; --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                t1 = clock();
                std::cout << " --> 16 --> delete [] queD3; " << std::endl;
                delete [] queD3;
                t2 = clock();
                std::cout << " --> done with --> delete [] queD3; --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                t1 = clock();
                std::cout << " --> 17 --> delete [] queL2; " << std::endl;
                delete [] queL2;
                t2 = clock();
                std::cout << " --> done with --> delete [] queL2; --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                t1 = clock();
                std::cout << " --> 18 --> delete [] queL3; " << std::endl;
                delete [] queL3;
                t2 = clock();
                std::cout << " --> done with --> delete [] queL3; --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
        }

        //=========================//
        // PARALLEL SECTIONS --> F //
        //=========================//

        #pragma omp parallel sections default(none)\
        shared(queD4, queL4, queD5, queL5, std::cout)\
        private(t1, t2, i)
        {
            #pragma omp section
            {
                // 14. emplace doubles into the queues

                t1 = clock();
                std::cout << " --> 19 --> emplace elements in to queue --> (*queD4)" << std::endl;
                for (i = 0; i < I_MAX; ++i)
                {
                    (*queD4).emplace(cos(static_cast<double>(i)));
                }
                t2 = clock();
                std::cout << " --> done with --> (*queD4) --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 15. emplace long ints into queue

                t1 = clock();
                std::cout << " --> 20 --> emplace elements in to queue --> (*queL4)" << std::endl;
                for (i = 0; i < I_MAX; ++i)
                {
                    (*queL4).emplace(i);
                }
                t2 = clock();
                std::cout << " --> done with --> (*queL4) --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 16. emplace doubles into the queues

                t1 = clock();
                std::cout << " --> 21 --> emplace elements in to queue --> (*queD5)" << std::endl;
                for (i = 0; i < I_MAX; ++i)
                {
                    (*queD5).emplace(cos(static_cast<double>(i)));
                }
                t2 = clock();
                std::cout << " --> done with --> (*queD5) --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                // 17. emplace long ints into queue

                t1 = clock();
                std::cout << " --> 22 --> emplace elements in to queue --> (*queL5)" << std::endl;
                for (i = 0; i < I_MAX; ++i)
                {
                    (*queL5).emplace(i);
                }
                t2 = clock();
                std::cout << " --> done with --> (*queL5) --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
        }

        //=========================//
        // PARALLEL SECTIONS --> G //
        //=========================//

        // swapping here to fragment the RAM!

        std::swap((*queD4),(*queD5));
        std::swap((*queL4),(*queL5));

        #pragma omp parallel sections default(none)\
        shared(queD4, queL4, queD5, queL5, std::cout)\
        private(t1, t2)
        {
            #pragma omp section
            {
                t1 = clock();
                std::cout << " --> 23 --> delete [] queD4; " << std::endl;
                delete [] queD4;
                t2 = clock();
                std::cout << " --> done with --> delete [] queD4; --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                t1 = clock();
                std::cout << " --> 24 --> delete [] queL4; " << std::endl;
                delete [] queL4;
                t2 = clock();
                std::cout << " --> done with --> delete [] queL4; --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                t1 = clock();
                std::cout << " --> 25 --> delete [] queD5; " << std::endl;
                delete [] queD5;
                t2 = clock();
                std::cout << " --> done with --> delete [] queD5; --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
            #pragma omp section
            {
                t1 = clock();
                std::cout << " --> 26 --> delete [] queL5; " << std::endl;
                delete [] queL5;
                t2 = clock();
                std::cout << " --> done with --> delete [] queL5; --> "
                << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << std::endl;
            }
        }
    }

    int sentinel;
    std::cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
