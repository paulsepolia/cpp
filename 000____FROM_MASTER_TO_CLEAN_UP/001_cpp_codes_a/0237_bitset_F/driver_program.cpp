
//================//
// bitset example //
//================//

#include <iostream>
#include <bitset>

using std::cout;
using std::cin;
using std::endl;
using std::boolalpha;
using std::noboolalpha;
using std::bitset;

// the main function

int main ()
{
    bitset<8> foo;

    cout << " --> Please, enter an 8-bit binary number: ";

    cin >> foo;

    cout << boolalpha;

    cout << "foo.all()  = " << foo.all()  << endl;
    cout << "foo.any()  = " << foo.any()  << endl;
    cout << "foo.none() = " << foo.none() << endl;

    cout << noboolalpha;

    cout << "foo.all()  = " << foo.all()  << endl;
    cout << "foo.any()  = " << foo.any()  << endl;
    cout << "foo.none() = " << foo.none() << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
