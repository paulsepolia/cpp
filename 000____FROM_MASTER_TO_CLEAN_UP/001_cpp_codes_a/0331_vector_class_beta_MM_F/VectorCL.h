//=============================//
// Header file.                //
// VectorCL Class Declaration. //
//=============================//

template <typename T, typename P>
class VectorCL {
public:

    VectorCL();                                    // constructor
    ~VectorCL();                                   // destructor

    //========================//
    // Member functions here. //
    //========================//

    void CreateF(P);                                            // create manually the vector
    void DeleteF();                                             // delete manually the vector
    T GetElementF(P) const;                                     // get the ith element of the vector
    void SetElementF(P, const T&);                              // set the vector element by element
    void SetEqualF(const VectorCL<T,P>&);                       // assignment operator
    void SetEqualZeroF();                                       // set to zero
    void SetEqualNumberF(const T&);                             // set to a specific number
    void AddF(const VectorCL<T,P>&, const VectorCL<T,P>&);      // add vectors
    void SubtractF(const VectorCL<T,P>&, const VectorCL<T,P>&); // subtract vectors
    void TimesF(const VectorCL<T,P>&, const VectorCL<T,P>&);    // multiply two vectors
    void DivideF(const VectorCL<T,P>&, const VectorCL<T,P>&);   // divide two vectors
    void NormalizeF(const VectorCL<T,P>&);                      // normalize the vector
    void ProjectF(const VectorCL<T,P>&, const VectorCL<T,P>&);  // projects the 1st to the 2nd
    void MGSF(VectorCL<T,P>*, const VectorCL<T,P>&, P, P);      // Modified Gram-Schmidt orthogonalizer
    void GSF(VectorCL<T,P>*, const VectorCL<T,P>&, P, P);       // Gram-Schmidt orthogonalizer
    void NegationF(const VectorCL<T,P>&);                       // Negation
    void InverseF(const VectorCL<T,P>&);                        // Inversion
    void AbsF(const VectorCL<T,P>&);                            // Absolute value

    //========================//
    // Friend functions here. //
    //========================//

    template <typename T2, typename P2>
    friend T2 DotF(const VectorCL<T2,P2>&, const VectorCL<T2,P2>&);  // Dot function

    template <typename T2, typename P2>
    friend T2 TotalF(const VectorCL<T2,P2>&);                      // TotalF function

    template <typename T2, typename P2>
    friend T2 MaxF(const VectorCL<T2,P2>&);                        // Max function

    template <typename T2, typename P2>
    friend T2 MinF(const VectorCL<T2,P2>&);                        // Min function

    template <typename T2, typename P2>
    friend T2 MaxAbsF(const VectorCL<T2,P2>&);                     // MaxAbs function

    template <typename T2, typename P2>
    friend T2 MinAbsF(const VectorCL<T2,P2>&);                     // MinAbs function

private:

    P m_Dim;
    T* m_Vec;      // pointer to the 1st element of the vector

public:

    //============================//
    // Overloaded operators here. //
    //============================//

    VectorCL<T,P>  operator=(const VectorCL<T,P>);  // the '=' operator
    VectorCL<T,P>  operator+(VectorCL<T,P>);        // the '+' operator
    VectorCL<T,P>& operator+=(VectorCL<T,P>&);      // the '+=' operator
};

//==============//
// End of code. //
//==============//
