//=======================================//
// Main function.                        //
// Driver program to the VectorCL class. //
//=======================================//

#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cmath>

#include "VectorCL.h"
#include "VectorCLMemberFunctions.h"
#include "VectorCLOverloadedOperators.h"
#include "VectorCLFriendFunctions.h"

#include "FunctionsFA.h"

#include "TypeDefinitions.h"

using namespace std;

int main()
{
    // local constants

    const TB  DIMVEC  = 1E3;
    const TB  TOTVEC  = 1E3;
    const TB  MAXTEST = 2;
    const TA  DLOW    = 0.0;
    const TA  DHIGH   = 1.0;
    const TA  ZERO    = 0.0;
    const TA  ONE     = 1.0;

    // local variables

    TB i, j, k;
    TA atmp;

    // array of objects

    VectorCL<TA, TB> *vectorArray;
    vectorArray = new VectorCL<TA, TB>[TOTVEC];

    // seeding the random generator

    srand(static_cast<unsigned>(20));

    // setting the precision of the outputs

    cout << setprecision(20) << fixed << endl;

    // main code

    // 1. Create the vectors

    cout << "  1. ----> Create the vectors." << endl;
    cout << "  2. ----> Dimension is " << DIMVEC << endl;
    cout << "  3. ----> Number of vector is " << TOTVEC << endl;

    for (i = 0; i < TOTVEC; i++) {
        vectorArray[i].CreateF(DIMVEC);
    }

    // 2. Initialize the vectors

    cout << "  4. ----> Initialize the vectors." << endl;
    cout << "  5. ----> The vectors are random." << endl;

    for (i = 0; i < TOTVEC; i++) {
        for (j = 0; j < DIMVEC; j++) {
            atmp = random_number(DLOW, DHIGH);
            vectorArray[i].SetElementF(j, atmp);
        }
    }

    // 3. Reorthogonalize the vectors array

    cout << "  6. ----> Reorthogonalize the vectors." << endl;

    for (i = 0; i < TOTVEC-1; i++) {
        vectorArray[i+1].MGSF(vectorArray, vectorArray[i+1], i+1, DIMVEC);
    }

    cout << "  7. ----> Normalize the vectors." << endl;

    for (i = 0; i < TOTVEC; i++) {
        vectorArray[i].NormalizeF(vectorArray[i]);
    }

    // 4. Dot product some orthogonalized vectors

    cout << "  8. ----> Dot products some orthogonalized vectors." << endl;

    atmp = DotF(vectorArray[0], vectorArray[1]);

    cout << "  9. ----> Must be " << ZERO << endl;
    cout << " 10. ---->      Is " << pow(abs(atmp), 0.5) << endl;

    atmp = DotF(vectorArray[2], vectorArray[2]);

    cout << " 11. ----> Must be " << ONE << endl;
    cout << " 12. ---->      Is " << pow(abs(atmp), 0.5) << endl;

    atmp = DotF(vectorArray[TOTVEC-1], vectorArray[TOTVEC-1]);

    cout << " 13. ----> Must be " << ONE << endl;
    cout << " 14. ---->      Is " << pow(abs(atmp), 0.5) << endl;

    atmp = DotF(vectorArray[1], vectorArray[TOTVEC-1]);

    cout << " 15. ----> Must be " << ZERO << endl;
    cout << " 16. ---->      Is " << pow(abs(atmp), 0.5) << endl;

    // 5. Use of overloaded '=' operator.

    cout << " 17. ----> Overloaded '=' operator." << endl;

    for (i = 1; i < TOTVEC; i++) {
        vectorArray[i] = vectorArray[0];
    }

    cout << " 18. ----> Test of the overloaded '=' operator." << endl;

    atmp = DotF(vectorArray[0], vectorArray[1]);
    cout << " 19. ----> Must be " << ONE << endl;
    cout << " 20. ---->      Is " << pow(abs(atmp), 0.5) << endl;

    atmp = DotF(vectorArray[2], vectorArray[3]);
    cout << " 21. ----> Must be " << ONE << endl;
    cout << " 22. ---->      Is " << pow(abs(atmp), 0.5) << endl;

    // 6. Use of overloaded '+' operator.

    cout << " 23. ----> Test of the overloaded '+' operator." << endl;

    cout << " 24. ----> Before." << endl;

    cout << vectorArray[0].GetElementF(1) << endl;
    cout << vectorArray[1].GetElementF(1) << endl;
    cout << vectorArray[2].GetElementF(1) << endl;

    vectorArray[2] = vectorArray[1] + vectorArray[0];

    cout << " 25. ----> After." << endl;

    cout << vectorArray[0].GetElementF(1) << endl;
    cout << vectorArray[1].GetElementF(1) << endl;
    cout << vectorArray[2].GetElementF(1) << endl;

    // 7. Use of the TotalF, MaxF, MinF, MaxAbsF, MinAbsF functions.

    cout << " 26. ----> Use of the TotalF, MaxF, MinF, MaxAbsF, MinAbsF functions." << endl;

    cout << " 27. ----> TotalF = " << TotalF(vectorArray[1]) << endl;
    cout << " 28. ----> Max    = " << MaxF(vectorArray[1])    << endl;
    cout << " 29. ----> MaxAbs = " << MaxAbsF(vectorArray[1]) << endl;
    cout << " 30. ----> Min    = " << MinF(vectorArray[1])    << endl;
    cout << " 31. ----> MinAbs = " << MinAbsF(vectorArray[1]) << endl;

    // 8. Use of Negation and Inverse functions.

    cout << " 32. ----> " << vectorArray[0].GetElementF(1) << endl;

    vectorArray[0].NegationF(vectorArray[0]);
    vectorArray[0].InverseF(vectorArray[0]);

    cout << " 33. ----> " << vectorArray[0].GetElementF(1) << endl;
    cout << " 34. ----> " << vectorArray[1].GetElementF(1) << endl;

    vectorArray[1].NegationF(vectorArray[1]);
    vectorArray[1].InverseF(vectorArray[1]);

    cout << " 35. ----> " << vectorArray[1].GetElementF(1) << endl;

    //  X. RAM is given back to system

    cout << endl;
    cout << " 36. ----> RAM is given back to system." << endl;
    cout << " 37. ----> Vectors deletion." << endl;
    cout << endl;

    for (k = 0; k < TOTVEC; k++) {
        vectorArray[k].DeleteF();
    }

    return 0;
}

//==============//
// End of code. //
//==============//
