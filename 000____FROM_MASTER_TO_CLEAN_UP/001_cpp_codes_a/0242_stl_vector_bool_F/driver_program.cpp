
//====================//
// vector<bool>::flip //
//====================//

#include <iostream>
#include <vector>
#include <cmath>

using std::endl;
using std::cout;
using std::cin;
using std::vector;
using std::boolalpha;
using std::pow;
using std::rand;
using std::srand;

// the main function

int main ()
{
    // local variables and parameters

    const long long int DIMEN_MAX = static_cast<long long int>(pow(10.0, 10.0));
    const long long int DISPLAY_SIZE = 10;
    vector<bool> * mask = new vector<bool>;

    // build the vector of bools

    srand(time(NULL));

    for (long long int i = 0; i != DIMEN_MAX; i++) {
        //mask->push_back(rand()%2); // very slow to build huge vectors (calls a function)
        mask->push_back(i%2);
    }

    cout << boolalpha;

    // output

    cout << " --> mask contains: " << endl;

    for (long long int i = 0; i != DISPLAY_SIZE; i++) {
        cout <<  " --> " << (*mask)[i] << endl;
    }

    // output

    for (long long int i = 0; i != DISPLAY_SIZE; i++) {
        cout << " --> " << (*mask)[i]
             << " + " << i << " = " << (*mask)[i] + i << endl;
    }

    // flip the bits

    mask->flip();

    // output

    cout << " --> mask fliped contains: " << endl;

    for (long long int i = 0; i < DISPLAY_SIZE; i++) {
        cout <<  " --> " << (*mask)[i] << endl;
    }

    // output

    for (long long int i = 0; i < DISPLAY_SIZE; i++) {
        cout << " --> " << (*mask)[i]
             << " + " << i << " = " << (*mask)[i] + i << endl;
    }

    // delete the bool vector

    delete mask;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

