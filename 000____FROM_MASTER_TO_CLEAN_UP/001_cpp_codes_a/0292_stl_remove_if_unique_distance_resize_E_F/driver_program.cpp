
//===========================================//
// STL,  remove_if, unique, resize, distance //
//===========================================//

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <vector>
#include <list>
#include <deque>
#include <cmath>

using std::endl;
using std::cout;
using std::cin;
using std::fixed;
using std::setprecision;
using std::showpos;
using std::showpoint;

using std::remove;
using std::remove_copy;
using std::remove_if;
using std::remove_copy_if;
using std::sort;
using std::unique;
using std::distance;

using std::vector;
using std::list;
using std::deque;
using std::pow;
using std::iterator;

// function object

template <class T>
class larger05 {
public:
    bool operator()(const T & i) const
    {
        return (i > 0.5);
    }
};

// function object

template <class T>
class larger04 {
public:
    bool operator()(const T & i) const
    {
        return (i > 0.4);
    }
};

// function object

template <class T>
class larger00 {
public:
    bool operator()(const T & i) const
    {
        return (i > 0.0);
    }
};

// function object

template <class T>
class largerm1 {
public:
    bool operator()(const T & i) const
    {
        return (i > -1);
    }
};

// the main function

int main()
{
    // local variables and parameters

    const long long DIM_MAX = static_cast<long long>(pow(10.0, 7.8));
    const long long K_MAX = static_cast<long long>(pow(10.0, 5.0));
    vector<double>::iterator new_endV;
    list<double>::iterator new_endL;
    deque<double>::iterator new_endD;

    for( long long k = 0; k != K_MAX; k++) {
        // the counter

        cout << "------------------------------------------------>> " << k << endl;

        // the vector, the list, the deque

        vector<double> * VA = new vector<double>;
        list<double> * LA = new list<double>;
        deque<double> * DA = new deque<double>;

        // build the vector

        for (long long i = 0; i != DIM_MAX; i++) {
            VA->push_back(cos(static_cast<double>(i)));
        }

        // use the remove_if algorithm

        new_endV = remove_if(VA->begin(), VA->end(), larger05<double>());

        // use erase member function

        VA->erase(new_endV, VA->end());

        // output the size

        cout << "  > +0.5 --> VA->size() = " << VA->size() << endl;

        // use the remove_if algorithm

        new_endV = remove_if(VA->begin(), VA->end(), larger04<double>());

        // use erase member function

        VA->erase(new_endV, VA->end());

        // output the size

        cout << "  > +0.4 --> VA->size() = " << VA->size() << endl;

        // use the remove_if algorithm

        new_endV = remove_if(VA->begin(), VA->end(), larger00<double>());

        // use erase member function

        VA->erase(new_endV, VA->end());

        // output the size

        cout << "  > +0.0 --> VA->size() = " << VA->size() << endl;

        // use the remove_if algorithm

        new_endV = remove_if(VA->begin(), VA->end(), largerm1<double>());

        // use erase member function

        VA->erase(new_endV, VA->end());

        // output the size

        cout << "  > -1.0 --> VA->size() = " << VA->size() << endl;

        //
        //
        // build the list

        for (long long i = 0; i != DIM_MAX; i++) {
            LA->push_back(cos(static_cast<double>(i)));
        }

        // use the remove_if algorithm

        new_endL = remove_if(LA->begin(), LA->end(), larger05<double>());

        // use erase member function

        LA->erase(new_endL, LA->end());

        // output the size

        cout << "  > +0.5 --> LA->size() = " << LA->size() << endl;

        // use the remove_if algorithm

        new_endL = remove_if(LA->begin(), LA->end(), larger04<double>());

        // use erase member function

        LA->erase(new_endL, LA->end());

        // output the size

        cout << "  > +0.4 --> LA->size() = " << LA->size() << endl;

        // use the remove_if algorithm

        new_endL = remove_if(LA->begin(), LA->end(), larger00<double>());

        // use erase member function

        LA->erase(new_endL, LA->end());

        // output the size

        cout << "  > +0.0 --> LA->size() = " << LA->size() << endl;

        // use the remove_if algorithm

        new_endL = remove_if(LA->begin(), LA->end(), largerm1<double>());

        // use erase member function

        LA->erase(new_endL, LA->end());

        // output the size

        cout << "  > -1.0 --> LA->size() = " << LA->size() << endl;

        //
        //
        // build the deque

        for (long long i = 0; i != DIM_MAX; i++) {
            DA->push_back(cos(static_cast<double>(i)));
        }

        // use the remove_if algorithm

        new_endD = remove_if(DA->begin(), DA->end(), larger05<double>());

        // use erase member function

        DA->erase(new_endD, DA->end());

        // output the size

        cout << "  > +0.5 --> DA->size() = " << DA->size() << endl;

        // use the remove_if algorithm

        new_endD = remove_if(DA->begin(), DA->end(), larger04<double>());

        // use erase member function

        DA->erase(new_endD, DA->end());

        // output the size

        cout << "  > +0.4 --> DA->size() = " << DA->size() << endl;

        // use the remove_if algorithm

        new_endD = remove_if(DA->begin(), DA->end(), larger00<double>());

        // use erase member function

        DA->erase(new_endD, DA->end());

        // output the size

        cout << "  > +0.0 --> DA->size() = " << DA->size() << endl;

        // use the remove_if algorithm

        new_endD = remove_if(DA->begin(), DA->end(), largerm1<double>());

        // use erase member function

        DA->erase(new_endD, DA->end());

        // output the size

        cout << "  > -1.0 --> DA->size() = " << DA->size() << endl;

        //
        //
        // build the vector again

        for (long long i = 0; i != DIM_MAX; i++) {
            VA->push_back(0.0);
        }

        cout << " --> VA->size() = " << VA->size() << endl;

        // sort the vector

        cout << " --> sort the vector using the general algorithm" << endl;

        sort(VA->begin(), VA->end());

        // unique the sortded vector

        cout << " --> apply unique to the sorted vector and then resize" << endl;

        new_endV = unique(VA->begin(), VA->end());

        // apply resize

        VA->resize(distance(VA->begin(), new_endV));

        // get the size --> must be 1

        cout << " --> VA->size() = " << VA->size() << endl;

        //
        // delete the vector

        cout << " --> delete the vector" << endl;

        delete VA;

        //
        // delete the list

        cout << " --> delete the list" << endl;

        delete LA;

        //
        // delete the deque

        cout << " --> delete the deque" << endl;

        delete DA;

    }

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
