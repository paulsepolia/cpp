
//====================//
// exception handling //
//====================//

#include <iostream>
#include <cstdlib>

using std::cin;
using std::cout;
using std::endl;

// a class to be thrown

class DivideByZero {};

// a function declaration which
// contains a throw statement

double safeDivide(int, int) throw(DivideByZero);

// the main function

int main()
{
    int numerator;
    int denominator;
    double quotient;

    cout << "Enter numerator: " << endl;
    cin >> numerator;
    cout << "Enter denominator: " << endl;
    cin >> denominator;

    //

    try {
        quotient = safeDivide(numerator, denominator);
    } catch(DivideByZero) {
        cout << "ERROR: Division by zero!" << endl;
        cout << "Program aborting." << endl;
        int sentinel;
        cin >> sentinel;
        exit(-1);
    }

    cout << numerator << "/" << denominator << " = " << quotient << endl;

    cout << "End of program!" << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

// function definition

double safeDivide(int top, int bottom) throw(DivideByZero)
{
    if (bottom == 0) {
        throw DivideByZero();
    }

    return top / static_cast<double>(bottom);
}

//======//
// FINI //
//======//
