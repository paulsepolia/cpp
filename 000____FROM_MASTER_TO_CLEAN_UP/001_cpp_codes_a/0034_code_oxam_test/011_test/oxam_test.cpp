//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/10/20              //
//===============================//

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <limits>
#include <ctime>

using namespace std;

// A. class declaration

class standingOrders {
public:

    standingOrders();  // constructor
    ~standingOrders(); // destructor

    // Member functions here

    string getFileName();
    void openFileStream(string);
    void readData(string);
    double getHighestValue(double& aveVal);
    void insertOrder(int& intId, double& ordVal);
    void eraseOrder(int& intId);

private:

    vector<int> vecTime, vecId;
    vector<double> vecVal;
    vector<char> vecOp;
    bool goodFile;
    fstream dataStream;
    clock_t startTime;
};

// B. main function

int main()
{
    // 1. variables declaration
    standingOrders staOrd1;
    string fileName;
    double highestVal, aveVal;

    // 2. get the file name
    fileName = staOrd1.getFileName();

    // 3. open the stream
    staOrd1.openFileStream(fileName);

    // 4. read data
    staOrd1.readData(fileName);

    // 5. get the highest value and the time weighted average value
    highestVal = staOrd1.getHighestValue(aveVal);

    cout << " The time weigthed average value is: " << aveVal << endl;
    cout << " The highest value is: " << highestVal << endl;

    // 6. put new orders
    int newID = 110;
    double newVal = 15.0;
// staOrd1.insertOrder(newID, newVal);
    staOrd1.eraseOrder(newID);

    return 0;
}

// C. class member functions definition

// 1. default constructor
standingOrders::standingOrders()
{
    vecId.clear();
    vecOp.clear();
    vecVal.clear();
    vecTime.clear();
    goodFile = false;
    startTime = clock();
}

// 2. destructor
standingOrders::~standingOrders() {}

// 3. string getFileName() // member function
string standingOrders::getFileName()
{
    string fileName;
    cout << " Enter the data file name: ";
    getline(cin, fileName);

    return fileName;
}

// 4. fstream openFileStream(string) // member function
void standingOrders::openFileStream(string fileName)
{
    // 1. open the i/o stream
    dataStream.open(fileName.c_str(), ios::in | ios::out | ios::app);

    // 2. test if the i/o stream is ready
    if(!dataStream.is_open()) {
        cout << " The file is not opened. Abort." << endl;
        goodFile = false;
        return;
    } else {
        goodFile = true;
    }

    // 3. test if the file is empty

    dataStream.get(); // try to read a char
    if(dataStream.eof()) { // test if the char is EOF
        cout << " The file is empty. Abort." << endl;
        goodFile = false;
        return;
    } else {
        goodFile = true;     // rewind the file
        dataStream.seekg(0, ios::beg);
    }
}

// 5. void readData(string) // member function
void standingOrders::readData(string fileName)
{
    // 1. variable declarations
    int intTime, intId, numLines, i;
    char charOp;
    double doubleVal;

    // 2. test
    if (!goodFile) {
        cout << " Can not read. Abort." << endl;
        return;
    }

    // 3. read lines
    numLines = 0; // set the initial value to zero
    while(dataStream >> intTime >> charOp >> intId) {
        numLines = numLines + 1; // number of lines in the file
        vecTime.push_back(intTime); // time of order
        vecOp.push_back(charOp); // type of order
        vecId.push_back(intId); // order ID

        if(charOp == 'I') { // insert case
            dataStream >> doubleVal;
            vecVal.push_back(doubleVal);
        } else if (charOp == 'E') { // erase case
            for(i = 0; i < numLines; i++) {
                if(vecId[i] == vecId[numLines-1]) {
                    doubleVal = vecVal[i];     // a safe value
                    break;
                }
            }
            vecVal.push_back(doubleVal);
        }
    }
}

// 6. double getHighestValue(double&) // member function
double standingOrders::getHighestValue(double& aveVal)
{
    // 1. variable declarations
    vector<int>  vecTimePeriod;
    vector<double> vecValPeriod;
    int  numLines, i, tableSize;
    double  tmpValA, tmpValB, sumLoc, highestVal;
    vector<double>::iterator highestValueIter;
    string fileName;

    // 2. test
    if (!goodFile) {
        aveVal = numeric_limits<double>::quiet_NaN();
        return aveVal;
    }

    // 3. get the time-weighted average highest price
    // 3-a. algorithm to create two new vectors:
    //      vecValPeriod, which holds the valid price
    //      vecTimePeriod, which holds the corresponding time
    //      The two new vectors is the table of the valid orders

    numLines = vecId.size();

    for (i = 0; i < numLines-1; i++) {
        if (vecId[i] != vecId[i+1]) {
            if (vecOp[i] == 'I') {
                tmpValA = vecVal[i];
            } else if (vecOp[i] == 'E') {
                tmpValA = vecVal[i+1];
            }

            tmpValB = vecTime[i+1] - vecTime[i];
            vecValPeriod.push_back(tmpValA);
            vecTimePeriod.push_back(tmpValB);
        } else if (vecId[i] == vecId[i+1]) {
            tmpValA = vecVal[i];
            tmpValB = (vecTime[i+1] - vecTime[i]);
            vecValPeriod.push_back(tmpValA);
            vecTimePeriod.push_back(tmpValB);
        }
    }

    // 3-b. get the time weigthed average price
    tableSize = vecValPeriod.size();
    sumLoc = 0.0;

    for (i = 0; i < tableSize; i++) {
        sumLoc = sumLoc + vecValPeriod[i] * vecTimePeriod[i];
    }

    aveVal = sumLoc / (vecTime[numLines-1]-vecTime[0]);

    // 4. highest valid price
    highestValueIter = max_element(vecValPeriod.begin(), vecValPeriod.end());
    highestVal = *highestValueIter; // the highest value is here

    return highestVal;
}

// void insertOrder(int&, int&, double&) // member function
void standingOrders::insertOrder(int& intId, double& ordVal)
{
    clock_t newTime;
    newTime = clock();
    clock_t ordTime;
    ordTime = newTime - startTime;

    cout << ordTime << endl;
    dataStream << ordTime << "I" << intId << ordVal;
}

// void eraseOrder(int&, double&) // member function
void standingOrders::eraseOrder(int& intId)
{
    clock_t newTime;
    newTime = clock();
    clock_t ordTime;
    ordTime = newTime - startTime;

    dataStream << ordTime << "E" << intId;
}


//======//
// FINI //
//======//
