
//======//
// Bind //
//======//

//===============//
// code --> 0340 //
//===============//

// keywords: bind

#include <iostream>
#include <cmath>
#include <iomanip>
#include <functional>

using std::cout;
using std::cin;
using std::endl;
using std::pow;
using std::fixed;
using std::showpoint;
using std::showpos;
using std::setprecision;
using std::boolalpha;
using std::bind;

using namespace std::placeholders;

// test function --> f1

double f1(const double & x, const double & y)
{
    return x+y;
}

// the main function

int main()
{
    // local variables and parameters

    const long long K_MAX = static_cast<long long>(pow(10.0, 5.0));

    // adjust the output format

    cout << fixed;
    cout << showpos;
    cout << showpoint;
    cout << setprecision(15);
    cout << boolalpha;

    // the benchmark loop

    for (long long k = 0; k != K_MAX; k++) {
        // the counter

        cout << "-------------------------------------------------------------->> " << k << endl;

        // test --> 1

        double tmp_double;

        tmp_double = f1(10.0, 20.0);

        cout << " --> " << tmp_double << endl;

        // test --> 2

        auto f1_b1 = bind(f1, _1, 20.0);

        tmp_double = f1_b1(10.0);

        cout << " --> " << tmp_double << endl;

        // test --> 3

        auto f1_b2 = bind(f1, _1, _1);

        tmp_double = f1_b2(20.0);

        cout << " --> " << tmp_double << endl;

        // test --> 4

        auto f1_b3 = bind(f1, _2, _2);

        tmp_double = f1_b3(10.0, 30.0);
    }

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
