
//===========================================//
// function object and                       //
// function class used as template arguments //
// a template class                          //
//===========================================//

#include <iostream>

using std::endl;
using std::cout;
using std::cin;

// function object and function class

template <class T>
class square {
public:
    T operator () (T x) const
    {
        return x * x;
    }
};

// function object and function class

template <class T>
class cube {
public:
    T operator () (T x) const
    {
        return x * x * x;
    }
};

// a template class to get
// template class functions as arguments

template <class TA, class TB>
class cont {
public:
    // defualt constructor

    cont() : j (0) {}

    // explicit constructor

    explicit cont(TA i) : j (i) {}

    // member function

    void print() const
    {
        cout << TB()(j) << endl;
    }

    // member function

    void set(TA i)
    {
        j = i;
    }

private:
    TA j;
};

// the main function

int main()
{
    // square<int> is a function class
    // and serves as an argument to cont template class

    cont<int, square<int> > numA(2);

    numA.print();

    // square<double> is a function class
    // serves as an argument to cont template class

    cont<double, square<double> > numB(3);

    numB.print();

    //

    cont<double, square<double> > numC;

    numC.set(4);

    numC.print();

    // cube<double> is a function class
    // and serves as an argument to cont template class

    cont<double, cube<double> > numD;

    numD.set(5);

    numD.print();

    // sentineling

    int sentinel;
    cin >> sentinel;
}

//======//
// FINI //
//======//

