//==============================//
// objects and object functions //
//==============================//

#include <iostream>

using std::cout;
using std::cin;
using std::endl;

// a class

class T {
public:
    T()
    {
        i = 123;     // default constructor
    }
    int i;
};

// a class

class U {
public:
    char operator()()
    {
        return 'Q';
    }

    int operator()(int a, int b, int c)
    {
        return a + b + c;
    }
};

// the main function

int main()
{
    // type object

    int j;
    j = T().i; // T is a type, T() is object
    cout << " j = " << j << endl;

    // function object

    U u; // u is an object

    cout << " --> u()      = " << u() << endl;
    cout << " --> u(1,2,3) = " << u(1,2,3) << endl;

    // sentinel;

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
