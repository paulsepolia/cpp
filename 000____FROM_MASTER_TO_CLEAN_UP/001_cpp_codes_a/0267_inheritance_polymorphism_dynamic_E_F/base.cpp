
//=======================//
// base class definition //
//=======================//

#include <iostream>
#include <cstdlib>
#include <cmath>
#include "base.h"

using std::cout;
using std::cin;
using std::endl;
using std::exit;
using std::sqrt;

namespace pgg {
// default constructor

Base::Base() : dim(0)
{
    vec = new double;
}

// constructor with two arguments

Base::Base(double val, long theDim) : dim(theDim)
{
    vec = new double [theDim];

    for (long i = 0; i != dim; i++) {
        vec[i] = val;
    }
}

// copy constructor

Base::Base(const Base & obj) : dim(obj.dim)
{
    vec = new double [dim];

    for (long i = 0; i != dim; i++) {
        vec[i] = obj.vec[i];
    }
}

// member function

void Base::allocate(long theDim)
{
    delete vec;
    dim = theDim;
    vec = new double [theDim];
}

// member function

long Base::getDim() const
{
    return dim;
}

// member function

double Base::getElement(long index) const
{
    return vec[index];
}

// member function

void Base::setDim(long theDim)
{
    delete [] this->vec;
    this->dim = theDim;
    this->vec = new double [theDim];
}

// member function

void Base::setElement(double elem, long index)
{
    vec[index] = elem;
}

// member function

void Base::setEqual(const Base& obj)
{
    delete [] this->vec;

    this->vec = new double [obj.dim];

    for (long i = 0; i < obj.dim; i++) {
        this->vec[i] = obj.vec[i];
    }
}

// member function

bool Base::isEqual(const Base& obj) const
{
    if (this->getDim() != obj.getDim()) {
        return false;
    }

    for (long i = 0; i != obj.dim; i++) {
        if (this->vec[i] != obj.vec[i]) {
            return false;
        }
    }

    return true;
}

// member function

void Base::add(const Base& objA, const Base& objB)
{
    if (objA.dim != objB.dim) {
        cout << "Objects of unequal length! Enter an integer to exit: ";
        int sentinel;
        cin >> sentinel;
        exit(-1);
    }

    delete [] this->vec;
    this->dim = objA.dim;
    this->vec = new double [ objA.dim];

    for (long i = 0; i != objA.dim; i++) {
        this->vec[i] = objA.vec[i] + objB.vec[i];
    }
}

// member function

void Base::subtract(const Base& objA, const Base& objB)
{
    if (objA.dim != objB.dim) {
        cout << "Objects of unequal length! Enter an integer to exit: ";
        int sentinel;
        cin >> sentinel;
        exit(-1);
    }

    delete [] this->vec;
    this->dim = objA.dim;
    this->vec = new double [ objA.dim];

    for (long i = 0; i != objA.dim; i++) {
        this->vec[i] = objA.vec[i] - objB.vec[i];
    }
}

// member function

void Base::multiply(const Base& objA, const Base& objB)
{
    if (objA.dim != objB.dim) {
        cout << "Objects of unequal length! Enter an integer to exit: ";
        int sentinel;
        cin >> sentinel;
        exit(-1);
    }

    delete [] this->vec;
    this->dim = objA.dim;
    this->vec = new double [ objA.dim];

    for (long i = 0; i != objA.dim; i++) {
        this->vec[i] = objA.vec[i] * objB.vec[i];
    }
}

// member function

void Base::divide(const Base& objA, const Base& objB)
{
    if (objA.dim != objB.dim) {
        cout << "Objects of unequal length! Enter an integer to exit: ";
        int sentinel;
        cin >> sentinel;
        exit(-1);
    }

    delete [] this->vec;
    this->dim = objA.dim;
    this->vec = new double [objA.dim];

    for (long i = 0; i != objA.dim; i++) {
        this->vec[i] = objA.vec[i] / objB.vec[i];
    }
}

// member function

double Base::magnitude() const
{
    double tmp = 0;

    for (long i = 0; i != dim; i++) {
        tmp = tmp + ((this->vec[i]) * (this->vec[i]));
    }

    return sqrt(tmp);
}

// member function

void Base::destroy()
{
    delete [] vec;
    vec = NULL;
}

// destructor

Base::~Base()
{
    delete [] vec;
}

// == operator overload

bool Base::operator == (const Base& obj) const
{
    if (this->dim != obj.dim) {
        cout << "Objects of unequal length! Enter an integer to exit: ";
        int sentinel;
        cin >> sentinel;
        exit(-1);
    }

    for (long i = 0; i != obj.dim; i++) {
        if (this->vec[i] != obj.vec[i]) {
            return false;
        }
    }

    return true;
}

// != operator overload

bool Base::operator != (const Base& obj) const
{
    if (this->dim != obj.dim) {
        cout << "Objects of unequal length! Enter an integer to exit: ";
        int sentinel;
        cin >> sentinel;
        exit(-1);
    }

    return !(*this == obj);
}

// < operator overload

bool Base::operator < (const Base& obj) const
{
    if (this->dim != obj.dim) {
        cout << "Objects of unequal length! Enter an integer to exit: ";
        int sentinel;
        cin >> sentinel;
        exit(-1);
    }

    return (this->magnitude() < obj.magnitude());

}

// <= operator overload

bool Base::operator <= (const Base& obj) const
{
    if (this->dim != obj.dim) {
        cout << "Objects of unequal length! Enter an integer to exit: ";
        int sentinel;
        cin >> sentinel;
        exit(-1);
    }

    return (this->magnitude() <= obj.magnitude());

}

// > operator overload

bool Base::operator > (const Base& obj) const
{
    if (this->dim != obj.dim) {
        cout << "Objects of unequal length! Enter an integer to exit: ";
        int sentinel;
        cin >> sentinel;
        exit(-1);
    }

    return (this->magnitude() > obj.magnitude());
}

// >= operator overload

bool Base::operator >= (const Base& obj) const
{
    if (this->dim != obj.dim) {
        cout << "Objects of unequal length! Enter an integer to exit: ";
        int sentinel;
        cin >> sentinel;
        exit(-1);
    }

    return (this->magnitude() >= obj.magnitude());
}

// + operator overload

Base Base::operator + (const Base& obj) const
{
    if (this->dim != obj.dim) {
        cout << "Objects of unequal length! Enter an integer to exit: ";
        int sentinel;
        cin >> sentinel;
        exit(-1);
    }

    Base tmp;

    tmp.setDim(obj.dim);

    tmp.add(*this, obj);

    return tmp;
}

// - operator overload

Base Base::operator - (const Base& obj) const
{
    if (this->dim != obj.dim) {
        cout << "Objects of unequal length! Enter an integer to exit: ";
        int sentinel;
        cin >> sentinel;
        exit(-1);
    }

    Base tmp;

    tmp.setDim(obj.dim);

    tmp.subtract(*this, obj);

    return Base(tmp);
}

// * operator overload

Base Base::operator * (const Base& obj) const
{
    if (this->dim != obj.dim) {
        cout << "Objects of unequal length! Enter an integer to exit: ";
        int sentinel;
        cin >> sentinel;
        exit(-1);
    }

    Base tmp;

    tmp.setDim(obj.dim);

    tmp.multiply(*this, obj);

    return Base(tmp);
}

// / operator overload

Base Base::operator / (const Base& obj) const
{
    if (this->dim != obj.dim) {
        cout << "Objects of unequal length! Enter an integer to exit: ";
        int sentinel;
        cin >> sentinel;
        exit(-1);
    }

    Base tmp;

    tmp.setDim(obj.dim);

    tmp.divide(*this, obj);

    return Base(tmp);
}

// ++ operator overload (prefix)

const Base Base::operator ++ ()
{
    for (long i = 0; i != this->dim; i++) {
        ++(this->vec[i]);
    }

    return *this;
}

// ++ operator overload (postfix)

const Base Base::operator ++ (int)
{
    for (long i = 0; i != this->dim; i++) {
        ++(this->vec[i]);
    }

    return *this;
}

// -- operator overload (prefix)

const Base Base::operator -- ()
{
    for (long i = 0; i != this->dim; i++) {
        --(this->vec[i]);
    }

    return *this;
}

// -- operator overload (postfix)

const Base Base::operator -- (int)
{
    for (long i = 0; i != this->dim; i++) {
        --(this->vec[i]);
    }

    return *this;
}

// - unary operator overload

const Base Base::operator - ()
{
    for (long i = 0; i != this->dim; i++) {
        this->vec[i] = -(this->vec[i]);
    }

    return *this;
}

// += operator overload

const Base Base::operator += (const Base& obj)
{
    if (this->dim != obj.dim) {
        cout << "Objects of unequal length! Enter an integer to exit: ";
        int sentinel;
        cin >> sentinel;
        exit(-1);
    }

    for (long i = 0; i < obj.dim; i++) {
        this->vec[i] = this->vec[i] + obj.vec[i];
    }

    return *this;
}

// -= operator overload

const Base Base::operator -= (const Base& obj)
{
    if (this->dim != obj.dim) {
        cout << "Objects of unequal length! Enter an integer to exit: ";
        int sentinel;
        cin >> sentinel;
        exit(-1);
    }

    for (long i = 0; i < obj.dim; i++) {
        this->vec[i] = this->vec[i] - obj.vec[i];
    }

    return *this;
}

// *= operator overload

const Base Base::operator *= (const Base& obj)
{
    if (this->dim != obj.dim) {
        cout << "Objects of unequal length! Enter an integer to exit: ";
        int sentinel;
        cin >> sentinel;
        exit(-1);
    }

    for (long i = 0; i < obj.dim; i++) {
        this->vec[i] = this->vec[i] * obj.vec[i];
    }

    return *this;
}

// /= operator overload

const Base Base::operator /= (const Base& obj)
{
    if (this->dim != obj.dim) {
        cout << "Objects of unequal length! Enter an integer to exit: ";
        int sentinel;
        cin >> sentinel;
        exit(-1);
    }

    for (long i = 0; i < obj.dim; i++) {
        this->vec[i] = this->vec[i] / obj.vec[i];
    }

    return *this;
}

// = operator overload

const Base Base::operator = (const Base& obj)
{
    this->destroy();
    this->setDim(obj.dim);

    for (long i = 0; i != obj.dim; i++) {
        this->vec[i] = obj.vec[i];
    }

    return *this;
}

// [] operator overload

double Base::operator [] (long index)
{
    if ( (index < 0) || (index >= this->dim)) {
        cout << "Illegal index! Enter an integer to exit: ";
        int sentinel;
        cin >> sentinel;
        exit(-1);
    }

    return this->vec[index];
}

} //pgg

//======//
// FINI //
//======//
