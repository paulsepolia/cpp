//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/10/17              //
// Functions: Class              //
//===============================//

#include <iostream>
#include <string>

using namespace std;

// class petType declaration

class petType {
public:
    virtual void print(); // virtual function
    petType(string n = "");
private:
    string name;
};

// class dogType declaration

class dogType: public petType {
public:
    void print();
    dogType(string n = "", string b = "");

private:
    string breed;
};

// other functions declaration

void callPrint(petType&);

// main program

int main()
{
    petType pet("Lucky");
//  dogType dog("Tommy", "German Shepherd");

    pet.print();
    cout << endl;
//  dog.print();
    cout << "**** Calling the function callPrint ****" << endl;
    callPrint(pet);
    cout << endl;
//  callPrint(dog);
}

// class petType functions definition

void petType::print()
{
    cout << "Name: " << name;
}

petType::petType(string n)
{
    name = n;
}

// class dogType functions definition

void dogType::print()
{
    petType::print();
    cout << ", Breed: " << breed << endl;
}

// other functions

void callPrint(petType& p)
{
    p.print();
}
