
//========================//
// class Money definition //
//========================//

#include <iostream>
#include "money_declaration.h"

using std::cout;
using std::endl;

// default constructor provided by the programmer

Money::Money() : valLoc (0.0) {};

// constructor with an argument

Money::Money(double val) : valLoc (val) {};

// member function

void Money::input(const double & val)
{
    valLoc = val;
}

// member function

void Money::output() const
{
    cout << valLoc << endl;
}

// member function

double Money::get() const
{
    return valLoc;
}

// friend function

// += operator overload

const Money & operator += (Money & mon1, const Money & mon2)
{
    mon1.valLoc = mon1.valLoc + mon2.valLoc;

    return mon1;
}

// friend function

// -= operator overload

const Money & operator -= (Money & mon1, const Money & mon2)
{
    mon1.valLoc = mon1.valLoc - mon2.valLoc;

    return mon1;
}

// friend function

// *= operator overload

const Money & operator *= (Money & mon1, const Money & mon2)
{
    mon1.valLoc = mon1.valLoc * mon2.valLoc;

    return mon1;
}

// friend function

// /= operator overload

const Money & operator /= (Money & mon1, const Money & mon2)
{
    mon1.valLoc = mon1.valLoc / mon2.valLoc;

    return mon1;
}

// friend function

// + operator overload

const Money operator + (const Money & mon1, const Money & mon2 )
{
    return Money(mon1.valLoc + mon2.valLoc);
}

// friend function

// - operator overload

const Money operator - (const Money & mon1, const Money & mon2 )
{
    return Money(mon1.valLoc - mon2.valLoc);
}

// friend function

// * operator overload

const Money operator * (const Money & mon1, const Money & mon2 )
{
    return Money(mon1.valLoc * mon2.valLoc);
}

// friend function

// / operator overload

const Money operator / (const Money & mon1, const Money & mon2 )
{
    return Money(mon1.valLoc / mon2.valLoc);
}

// friend function

// == operator overload

// declaration

bool operator == (const Money & mon1, const Money & mon2)
{
    return (mon1.valLoc == mon2.valLoc);
}

// friend function

// <= operator overload

// declaration

bool operator <= (const Money & mon1, const Money & mon2)
{
    return (mon1.valLoc <= mon2.valLoc);
}

// friend function

// >= operator overload

// definition

bool operator >= (const Money & mon1, const Money & mon2)
{
    return (mon1.valLoc >= mon2.valLoc);
}

// friend function

// < operator overload

// definition

bool operator < (const Money & mon1, const Money & mon2)
{
    return (mon1.valLoc < mon2.valLoc);
}

// friend function

// > operator overload

// definition

bool operator > (const Money & mon1, const Money & mon2)
{
    return (mon1.valLoc > mon2.valLoc);
}

// friend function

// - operator overload

const Money operator - (const Money & mon1)
{
    return (Money(-mon1.valLoc));
}

// friend function

// ++ operator overload (prefix)

const Money operator ++ (Money & mon)
{
    return (Money(++mon.valLoc));
}

// friend function

// ++ operator overload (postfix)

const Money operator ++ (Money & mon, int)
{
    return (Money(++mon.valLoc));
}

// friend function

// -- operator overload (prefix)

const Money operator -- (Money & mon)
{
    return (Money(--mon.valLoc));
}

// friend function

// -- operator overload (postfix)

const Money operator -- (Money & mon, int)
{
    return (Money(--mon.valLoc));
}

//======//
// FINI //
//======//

