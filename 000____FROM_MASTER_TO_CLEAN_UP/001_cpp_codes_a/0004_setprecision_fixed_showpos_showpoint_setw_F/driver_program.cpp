
//======================================//
// manipulators: setw, fixed, showpoint //
//======================================//

//===============//
// code --> 0004 //
//===============//

// keywords: setw, showpos, showpoint, setprecision, manipulators, fixed

#include <iostream>
#include <iomanip>

using std::endl;
using std::cout;
using std::cin;
using std::fixed;
using std::showpoint;
using std::showpos;
using std::setprecision;
using std::setw;

// the main function

int main()
{
    int x = 19;
    int a = 345;
    double y = 76.384;

    cout << " --> 1" << endl;

    cout << fixed << showpoint;

    cout << "12345678901234567890" << endl;

    cout << " --> 2" << endl;

    cout << setw(5) << x << endl;

    cout << " --> 3" << endl;

    cout << setw(5) << a << setw(5) << "Hi"
         << setw(5) << x << endl << endl;

    cout << " --> 4" << endl;

    cout << setprecision(2);

    cout << " --> 5" << endl;

    cout << setw(6) << a << setw(6) << y
         << setw(6) << x << endl;

    cout << " --> 6" << endl;

    cout << setw(6) << x << setw(6) << a
         << setw(6) << y << endl << endl;

    cout << " --> 7" << endl;

    cout << setw(5) << a << x << endl;

    cout << " --> 8" << endl;

    cout << setw(2) << a << setw(4) << x << endl;

    cout << " --> 9 --> end" << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

