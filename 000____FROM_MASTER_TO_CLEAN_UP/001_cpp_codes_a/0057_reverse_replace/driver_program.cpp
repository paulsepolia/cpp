//=======================================//
// Author: Pavlos G. Galiatsatos         //
// Date: 2013/10/23                      //
// Functions: vector, deque, array, list //
// Algorithms: reverse, replace          //
//=======================================//

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

// the main function

int main ()
{
    // variables

    long int i;
    long int j;
    const long int I_MAX = 1 * static_cast<long int>(100000);
    vector<long int> aVector;
    const long int THE_VAL_A = 10;
    const long int THE_VAL_B = 20;
    const int J_MAX = 10;

    for (j = 0; j < J_MAX; j++) {
        // write data to the vector
        cout << "-------------------------------------------------------> " << j+1 << endl;
        cout << endl;
        cout << " 1 --> build the vector" << endl;

        for (i = 0; i < I_MAX; i++) {
            aVector.push_back(THE_VAL_A);
        }

        // replace all the data
        cout << " 2 --> replace all the data" << endl;
        replace(aVector.begin(), aVector.end(), THE_VAL_A, THE_VAL_B);

        // clear the vector
        cout << " 3 --> clear the vector" << endl;
        aVector.clear();

        // write data to the vector
        cout << " 4 --> build the vector again" << endl;

        for (i = 0; i < I_MAX; i++) {
            aVector.push_back(i);
        }

        // reverse data to the vector
        cout << " 5 --> reverse data to the vector x 4" << endl;

        reverse(aVector.begin(), aVector.end());

        reverse(aVector.begin(), aVector.end());

        reverse(aVector.begin(), aVector.end());

        reverse(aVector.begin(), aVector.end());

        // replace all the data again
        cout << " 5 --> replace all the data again" << endl;

        for (i = 0; i < I_MAX; i++) {
            replace(aVector.begin(), aVector.end(), i, THE_VAL_B);
        }

        cout << endl;
    }

    return 0;
}

// end
