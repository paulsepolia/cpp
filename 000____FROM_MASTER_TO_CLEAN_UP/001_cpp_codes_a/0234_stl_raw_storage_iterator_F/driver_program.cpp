
//===================================//
// demonstrate raw storage iterators //
//===================================//

#include <iostream>
#include <deque>
#include <memory>
#include <algorithm>

using std::endl;
using std::cout;
using std::cin;
using std::deque;
using std::raw_storage_iterator;
using std::copy;

// class declaration and definition

class X {
public:

    // default constructor

    X() : a (0), b (0), sum (0) {}

    // two arguments coinstructor

    X(int x, int y) : a (x), b (y), sum (0) {}

    // copy constructor

    X(const X &o) : a (o.a), b (o.b), sum (o.sum) {}

    // overload assignment operator

    X operator = (const X &o)
    {
        a = o.a;
        b = o.b;
        // do not assign sum
        // sum = o.sum; // if uncomment that line the code works fine
        return *this;
    }

    // member functions

    void setsum()
    {
        sum = a+b;
    }

    void show()
    {
        cout << a << "," << b;
        cout << " Sum is: " << sum << endl;
    }

private:

    int a;
    int b;
    int sum;
};

// the main function

int main()
{
    // local variables and parameters

    double raw1[100];
    double raw2[100];

    X * p; // a pointer to X class
    deque<X> q(5); // a deque container of 5 objects of type X

    // set the deque

    for (int i = 0; i < 5; i++) {
        q[i] = X(i, i);
    }

    // set the sum of each X class type q[i] objects

    for (int i = 0; i < 5; i++) {
        q[i].setsum();
    }

    // store deque in uninitialized memory the wrong way
    // since the assignment operator is called
    // but there is no sum assignment there

    copy(q.begin(), q.end(), (X*)raw1);

    p = (X*)raw1;

    p[0].show();
    p[1].show();
    p[2].show();
    p[3].show();
    p[4].show();

    // store deque in uninitialized memory the right way
    // here the copy constructor is called
    // and sum variable gets values

    copy(q.begin(), q.end(), raw_storage_iterator<X*,X>((X*)raw2));

    p = (X*) raw2;

    p[0].show();
    p[1].show();
    p[2].show();
    p[3].show();
    p[4].show();

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

