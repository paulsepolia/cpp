
//====================//
// demonstrate bitset //
//====================//

#include <iostream>
#include <bitset>

using std::cout;
using std::cin;
using std::endl;
using std::bitset;

// the main function

int main()
{
    bitset<16> bA(32);

    // original bits

    cout << " --> original bits -------------------> " << bA << endl;

    // assign some bits

    cout << " --> assign some bits" << endl;

    bA[0] = 1;
    bA[2] = 1;
    bA[10] = 1;
    bA[12] = 1;

    cout << " --> bits after assignment -----------> " << bA << endl;

    // rotate bits

    bA <<= 2;

    cout << " --> bits after left rotate ----------> " << bA << endl;

    // flip bits

    bA.flip();

    cout << " --> bits after flipping bits --------> " << bA << endl;

    // count bits

    cout << " --> bA has " << bA.count() << " bits set" << endl;

    // test bits threee different ways

    if (bA[0] == 1) {
        cout << " bit 0 is on" << endl;
    }

    if (bA.test(1) == 1) {
        cout << " bit 1 is on" << endl;
    }

    // can add bits to integers

    cout << "Add 11 to bit 0: " << bA[0] + 11 << endl;

    // convert to int

    cout << " --> bA as integer is ---> " << bA.to_ullong() << endl;
    cout << " --> bA as integer is ---> " << bA.to_ulong() << endl;
    cout << " --> bA as integer is ---> " << bA.to_string() << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
