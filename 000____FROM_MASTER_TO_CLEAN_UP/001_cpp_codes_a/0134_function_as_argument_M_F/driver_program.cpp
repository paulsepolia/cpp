//============================//
// function name as parameter //
//============================//

#include <iostream>

using std::cout;
using std::cin;
using std::endl;

// --> function --> myFunA

void myFunA()
{
    cout << "--> myFunA here!";
}

// --> function--> myFunB

void myFunB(void (*fun)())
{
    fun();

    cout << " ... via myFunB!" << endl;
}

// --> function --> myFunC

void myFunC(void fun())
{
    fun();

    cout << " ... via myFunC!" << endl;
}


// the main function

int main()
{
    // 1.

    myFunA();

    cout << endl;

    // 2.

    myFunB(myFunA);

    cout << endl;

    // 3.

    myFunB(&myFunA);

    cout << endl;

    // 4.

    myFunC(&myFunA);

    // 5. sentineling

    int sentinel;
    cin >> sentinel;
}

//======//
// FINI //
//======//

