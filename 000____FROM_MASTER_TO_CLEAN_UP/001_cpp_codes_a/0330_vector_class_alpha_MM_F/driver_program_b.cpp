//=======================================//
// Main function.                        //
// Driver program to the VectorCL class. //
//=======================================//

#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cmath>

#include "VectorCL.h"
#include "VectorCLMemberFunctions.h"
#include "VectorCLOverloadedOperators.h"
#include "VectorCLFriendFunctions.h"

#include "FunctionsFA.h"

#include "TypeDefinitions.h"

using namespace std;

int main()
{
    // local constants

    const TB  DIMVEC  = 1E2;
    const TB  TOTVEC  = 1E2;
    const TB  MAXTEST = 5;
    const TA  DLOW    = 0.0;
    const TA  DHIGH   = 1.0;
    const TB  MYSEED  = 20;

    // local variables

    TB i, j, k;
    TA atmp;

    // array of objects

    VectorCL<TA, TB> *vectorArray;
    vectorArray = new VectorCL<TA, TB>[TOTVEC];

    // seeding the random generator

    srand(static_cast<unsigned>(MYSEED));

    // setting the precision of the outputs

    cout << setprecision(20) << fixed << endl;

    // main code

    // 1. Create the vectors

    cout << "----> Create the vectors." << endl;

    for (i = 0; i < TOTVEC; i++) {
        vectorArray[i].CreateF(DIMVEC);
    }

    // 2. Initialize the vectors

    cout << "----> Initialize the vectors." << endl;

    for (i = 0; i < TOTVEC; i++) {
        for (j = 0; j < DIMVEC; j++) {
            atmp = random_number(DLOW, DHIGH);
            vectorArray[i].SetElementF(j, atmp);
        }
    }

    // 3. Reorthogonalize the vectors array

    cout << "----> Reorthogonalize the vectors." << endl;

    for (i = 0; i < TOTVEC-1; i++) {
        vectorArray[i+1].MGSF(vectorArray, vectorArray[i+1], i+1, DIMVEC);
    }

    cout << "----> Normalize the vectors." << endl;

    for (i = 0; i < TOTVEC; i++) {
        vectorArray[i].NormalizeF(vectorArray[i]);
    }

    // 4. Dot product some orthogonalized vectors

    cout << "----> Dot products some orthogonalized vectors." << endl;

    for(i = 0; i < MAXTEST; i++) {
        atmp = DotF(vectorArray[i], vectorArray[i+1]);
        cout << "---->  " << pow(abs(atmp), 0.5) << endl;
        atmp = DotF(vectorArray[i], vectorArray[TOTVEC-1]);
        cout << "---->  " << pow(abs(atmp), 0.5) << endl;
        atmp = DotF(vectorArray[i], vectorArray[i]);
        cout << "---->  " << pow(abs(atmp), 0.5) << endl;
    }

    // 5. Use of overloaded '=' operator.

    cout << "----> Overloaded '=' operator." << endl;

    for (i = 1; i < TOTVEC; i++) {
        vectorArray[i] = vectorArray[0];
    }

    cout << "----> Test of the oveloaded '=' operator." << endl;

    for(k = 0; k < MAXTEST; k++) {
        atmp = DotF(vectorArray[k], vectorArray[k+1]);
        cout << "---->  " << pow(abs(atmp), 0.5) << endl;
    }

    // 6. Use of overloaded '+' operator.

    cout << "----> Test the overloaded '+' operator." << endl;

    cout << "----> Before." << endl;

    cout << vectorArray[0].GetElementF(1) << endl;
    cout << vectorArray[1].GetElementF(1) << endl;
    cout << vectorArray[2].GetElementF(1) << endl;

    vectorArray[2] = vectorArray[1] + vectorArray[0];

    cout << "----> After." << endl;

    cout << vectorArray[0].GetElementF(1) << endl;
    cout << vectorArray[1].GetElementF(1) << endl;
    cout << vectorArray[2].GetElementF(1) << endl;

    // 7. Use of the TotalF, MaxF, MinF, MaxAbsF, MinAbsF functions.

    cout << "----> Use of the TotalF, MaxF, MinF, MaxAbsF, MinAbsF functions." << endl;

    cout << "----> TotalF = " << TotalF(vectorArray[k]) << endl;
    cout << "----> Max    = " << MaxF(vectorArray[k])    << endl;
    cout << "----> MaxAbs = " << MaxAbsF(vectorArray[k]) << endl;
    cout << "----> Min    = " << MinF(vectorArray[k])    << endl;
    cout << "----> MinAbs = " << MinAbsF(vectorArray[k]) << endl;

    // 8. Use of Negation and Inverse functions.

    cout << "----> " << vectorArray[0].GetElementF(1) << endl;

    vectorArray[0].NegationF(vectorArray[0]);
    vectorArray[0].InverseF(vectorArray[0]);

    cout << "----> " << vectorArray[0].GetElementF(1) << endl;
    cout << "----> " << vectorArray[1].GetElementF(1) << endl;

    vectorArray[1].NegationF(vectorArray[1]);
    vectorArray[1].InverseF(vectorArray[1]);

    cout << "----> " << vectorArray[1].GetElementF(1) << endl;

    //  X. RAM is given back to system

    cout << endl;
    cout << "----> RAM is given back to system." << endl;
    cout << "----> Vectors deletion." << endl;
    cout << endl;

    for (k = 0; k < TOTVEC; k++) {
        vectorArray[k].DeleteF();
    }

    return 0;
}

//==============//
// End of code. //
//==============//
