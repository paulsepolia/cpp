
//====================================================//
// Functions: open, close                             //
// Classes: ifstream, ofstream, string                //
// Manipulators: setw, fixed, setprecision, showpoint //
//====================================================//

//===============//
// code --> 0007 //
//===============//

// keywords : open, close, ifstream, ofstream, string, setw, fixed
//	      setprecision, showpoint, showpos

#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>

using std::cout;
using std::endl;
using std::cin;
using std::fixed;
using std::setprecision;
using std::setw;
using std::showpoint;
using std::showpos;
using std::string;
using std::ifstream;
using std::ofstream;

// the main function

int main()
{
    // --> 1 --> declare variables

    ifstream inFile;
    ofstream outFile;
    double test1;
    double test2;
    double test3;
    double test4;
    double test5;
    double average;
    string firstName;
    string lastName;

    // --> 2 --> open units

    cout << " --> 1" << endl;

    cout << " --> open all the files" << endl;

    inFile.open("test.txt");
    outFile.open("testavg.out");

    cout << " --> 2" << endl;

    cout << " --> set the output format" << endl;

    outFile << fixed;
    outFile << showpoint;
    outFile << setprecision(3);
    outFile << showpos;

    // --> 3 --> execution body

    cout << " --> 3" << endl;

    cout << " --> processing data" << endl;

    cout << " --> read the first name" << endl;

    inFile >> firstName;

    cout << " --> read the last name" << endl;

    inFile >> lastName;

    cout << " --> 4" << endl;

    cout << " --> write the name to the file" << endl;

    outFile << " --> Student name: " << firstName;
    outFile << " ";
    outFile << lastName << endl;

    cout << " --> 5" << endl;

    cout << " --> read the all the scores" << endl;

    inFile >> test1;
    inFile >> test2;
    inFile >> test3;
    inFile >> test4;
    inFile >> test5;

    cout << " --> 6" << endl;

    cout << " --> write to the file the scores" << endl;

    outFile << " --> testscores: " << setw(9) << test1;

    outFile << setw(9) << test2 << setw(9) << test3;

    outFile << setw(9) << test4 << setw(9) << test5 << endl;

    average = (test1 + test2 + test3 + test4 + test5) / 5.0;

    outFile << " --> average test score: " << setw(9) << average << endl;

    // --> 4 --> close units

    cout << " --> 7" << endl;

    cout << " --> close the input and output units" << endl;

    inFile.close();
    outFile.close();

    cout << " --> 8 --> end" << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

