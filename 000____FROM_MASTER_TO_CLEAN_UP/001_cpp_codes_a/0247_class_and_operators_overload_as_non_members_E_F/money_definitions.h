
//=========================//
// class Money definitions //
//=========================//

#include <iostream>
#include "money_declarations.h"

using std::cout;
using std::endl;
using std::istream;
using std::ostream;

// definitions

// default constructor provided by the programmer

Money::Money() : valLoc (0.0) {};

// constructor with an argument

Money::Money(double val) : valLoc (val) {};

// member function

void Money::input(const double & val)
{
    valLoc = val;
}

// member function

void Money::output() const
{
    cout << valLoc << endl;
}

// member function

double Money::get() const
{
    return valLoc;
}

// + operator overload

// declaration

const Money operator + (const Money &, const Money &);

// definition

const Money operator + (const Money & mon1, const Money & mon2)
{
    double valLoc;

    valLoc = mon1.get() + mon2.get();

    return Money(valLoc);
}

// - operator overload

// declaration

const Money operator - (const Money &, const Money &);

// definition

const Money operator - (const Money & mon1, const Money & mon2)
{
    double valLoc;

    valLoc = mon1.get() - mon2.get();

    return Money(valLoc);
}

// * operator overload

// declaration

const Money operator * (const Money &, const Money &);

// definition

const Money operator * (const Money & mon1, const Money & mon2)
{
    double valLoc;

    valLoc = mon1.get() * mon2.get();

    return Money(valLoc);
}

// / operator overload

// declaration

const Money operator / (const Money &, const Money &);

// definition

const Money operator / (const Money & mon1, const Money & mon2)
{
    double valLoc;

    valLoc = mon1.get() / mon2.get();

    return Money(valLoc);
}

// == operator overload

// declaration

bool operator == (const Money &, const Money &);

// definition

bool operator == (const Money & mon1, const Money & mon2)
{
    return (mon1.get() == mon2.get());
}

// <= operator overload

// declaration

bool operator <= (const Money &, const Money &);

// definition

bool operator <= (const Money & mon1, const Money & mon2)
{
    return (mon1.get() <= mon2.get());
}

// >= operator overload

// declaration

bool operator >= (const Money &, const Money &);

// definition

bool operator >= (const Money & mon1, const Money & mon2)
{
    return (mon1.get() >= mon2.get());
}

// < operator overload

// declaration

bool operator < (const Money &, const Money &);

// definition

bool operator < (const Money & mon1, const Money & mon2)
{
    return (mon1.get() < mon2.get());
}

// > operator overload

// declaration

bool operator > (const Money &, const Money &);

// definition

bool operator > (const Money & mon1, const Money & mon2)
{
    return (mon1.get() > mon2.get());
}

// - operator overload

// declaration

const Money operator - (const Money &);

// definition

const Money operator - (const Money & mon1)
{
    return (Money(-mon1.get()));
}

// ++ operator overload (prefix)

// declaration

const Money operator ++ (Money &);

// definition

const Money operator ++ (Money & mon1)
{
    double tmp = 0;

    tmp = mon1.get();

    ++tmp;

    mon1.input(tmp);

    return  (Money(mon1.get()));
}

// ++ operator overload (postfix)

// declaration

const Money operator ++ (Money &, int);

// definition

const Money operator ++ (Money & mon1, int)
{
    ++mon1;

    return (Money(mon1.get()));
}

// -- operator overload (prefix)

// declaration

const Money operator -- (Money &);

// definition

const Money operator -- (Money & mon1)
{
    double tmp = 0;

    tmp = mon1.get();

    --tmp;

    mon1.input(tmp);

    return (Money(mon1.get()));
}

// -- operator overload (postfix)

// declaration

const Money operator -- (Money &, int);

// definition

const Money operator -- (Money & mon1, int)
{
    --mon1;

    return (Money(mon1.get()));
}

// += operator overload

// declaration

const Money & operator += (Money &, const Money &);

// definition

const Money & operator += (Money & mon1, const Money & mon2)
{
    double tmp;
    tmp = mon1.get() + mon2.get();
    mon1.input(tmp);

    return mon1;
}

// -= operator overload

// declaration

const Money & operator -= (Money &, const Money &);

// definition

const Money & operator -= (Money & mon1, const Money & mon2)
{
    double tmp;
    tmp = mon1.get() - mon2.get();
    mon1.input(tmp);

    return mon1;
}

// *= operator overload

// declaration

const Money & operator *= (Money &, const Money &);

// definition

const Money & operator *= (Money & mon1, const Money & mon2)
{
    double tmp;
    tmp = mon1.get() * mon2.get();
    mon1.input(tmp);

    return mon1;
}


// /= operator overload

// declaration

const Money & operator /= (Money &, const Money &);

// definition

const Money & operator /= (Money & mon1, const Money & mon2)
{
    double tmp;
    tmp = mon1.get() / mon2.get();
    mon1.input(tmp);

    return mon1;
}

// << operator overload

// declaration

ostream & operator << (ostream &, const Money &);

// definition

ostream & operator << (ostream & outStream, const Money & mon2)
{
    outStream << mon2.get();

    return outStream;
}

// >> operator overload

// declaration

istream & operator >> (istream &, Money &);

// definition

istream & operator >> (istream & inStream, Money & mon2)
{
    double tmp;

    inStream >> tmp;

    mon2.input(tmp);

    return inStream;
}

//======//
// FINI //
//======//

