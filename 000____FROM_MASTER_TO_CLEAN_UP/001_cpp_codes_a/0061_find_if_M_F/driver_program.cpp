//=======================================//
// Author: Pavlos G. Galiatsatos         //
// Date: 2013/10/30                      //
// Functions: array, vector, deque, list //
// Algorithms: find_if                   //
//=======================================//

#include <iostream>
#include <algorithm>
#include <fstream>
#include <iterator>
#include <vector>
#include <list>
#include <deque>

using namespace std;

// global consts

const long int I_MAX = 5 * static_cast<long int>(10000000);

// function definition

template<class T>
bool condition(T x)
{
    return (x >= I_MAX);
}

// main

int main ()
{
    // variables declaration

    int i;
    vector<int> aVector;
    list<int> aList;
    deque<int> aDeque;
    vector<int>::iterator itV;
    list<int>::iterator itL;
    deque<int>::iterator itD;
    int j;
    const int J_MAX = 100;

    // build the array

    for (j = 0; j < J_MAX; j++) {
        cout << "------------------------------------------------>> " << j+1 << endl;

        cout << " 1 --> build the array" << endl;

        int *aArray = new int [I_MAX];

        for (i = 0;  i < I_MAX; i++) {
            aArray[i] = static_cast<int>(I_MAX - i);
        }

        // copy the array to a vector

        cout << " 2 --> build the vector" << endl;

        aVector.resize(I_MAX);

        copy(aArray, aArray+I_MAX, aVector.begin());

        // copy the array to the list

        cout << " 3 --> build the list" << endl;

        aList.resize(I_MAX);

        copy(aArray, aArray+I_MAX, aList.begin());

        // copy the array to the deque

        cout << " 4 --> build the deque" << endl;

        aDeque.resize(I_MAX);

        copy(aList.begin(), aList.end(), aDeque.begin());

        // find_if for vector

        cout << " find_if for vector" << endl;
        itV = find_if(aVector.begin(), aVector.end(), condition<int>);

        cout << " find_if for list" << endl;
        itL = find_if(aList.begin(), aList.end(), condition<int>);

        cout << " find_if for deque" << endl;
        itD = find_if(aDeque.begin(), aDeque.end(), condition<int>);

        if (itV != aVector.end()) {
            cout << " Found element: " << *itV << endl;
        }

        if (itL != aList.end()) {
            cout << " Found element: " << *itL << endl;
        }

        if (itD != aDeque.end()) {
            cout << " Found element: " << *itD << endl;
        }

        // free up RAM

        cout << " Free up RAM" << endl;

        aVector.clear();
        aList.clear();
        aDeque.clear();
        delete [] aArray;
    }

    return 0;
}

// end
