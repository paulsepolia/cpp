
//=================//
// equal, mismatch //
//=================//

#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <list>
#include <vector>
#include <deque>
#include <cmath>
#include <cassert>
#include <algorithm>

using std::cout;
using std::cin;
using std::endl;

using std::exit;

using std::fixed;
using std::showpos;
using std::showpoint;
using std::setprecision;

using std::list;
using std::vector;
using std::deque;

using std::pow;

using std::equal;
using std::mismatch;

using std::pair;

// the main function

int main()
{
    // local variables and parameters

    const long long DIM_MAX = static_cast<long long>(pow(10.0, 7.5));
    const long long K_MAX = static_cast<long long>(pow(10.0, 5.0));
    pair< deque<double>::iterator, list<double>::iterator >  pairItDL;
    pair< vector<double>::iterator, list<double>::iterator > pairItVL;
    pair< double*, list<double>::iterator > pairItdL;

    // output format

    cout << fixed;
    cout << setprecision(20);
    cout << showpos;
    cout << showpoint;

    // main for loop

    for (long long k = 0; k < K_MAX; k++) {
        // output

        cout << "------------------------------------------------------------>> " << k << endl;

        list<double> * L1 = new list<double>;
        vector<double> * V1 = new vector<double>;;
        deque<double> * D1 = new deque<double>;
        double * a = new double [DIM_MAX];

        // build the array

        cout << " 0 --> build the array" << endl;

        for (long long i = 0; i != DIM_MAX; i++) {
            a[i] = cos(static_cast<double>(i));
        }

        // build the list

        cout << " 1 --> build the list" << endl;

        for (long long i = 0; i != DIM_MAX; i++) {
            L1->push_back(cos(static_cast<double>(i)));
        }

        // build the vector

        cout << " 2 --> build the vector" << endl;

        for (long long i = 0; i != DIM_MAX; i++) {
            V1->push_back(cos(static_cast<double>(i)));
        }

        // build the deque

        cout << " 3 --> build the deque" << endl;

        for (long long i = 0; i != DIM_MAX; i++) {
            D1->push_back(cos(static_cast<double>(i)));
        }

        cout << " 4 --> assert(equal(D1->begin(), D1->end(), L1->begin()));" << endl;

        assert(equal(D1->begin(), D1->end(), L1->begin()));

        cout << " 5 --> assert(equal(V1->begin(), V1->end(), L1->begin()));" << endl;

        assert(equal(V1->begin(), V1->end(), L1->begin()));

        cout << " 6 --> assert(equal(&a[0], &a[DIM_MAX], L1->begin()));" << endl;

        assert(equal(&a[0], &a[DIM_MAX], L1->begin()));

        cout << endl;

        cout << " 7 --> pairItVL = mismatch(V1->begin(), V1->end(), L1->begin());" << endl;

        pairItVL = mismatch(V1->begin(), V1->end(), L1->begin());

        cout << endl;
        cout << " *(pairItVL.first)  = " << *(pairItVL.first) << endl;
        cout << " *(pairItVL.second) = " << *(pairItVL.second) << endl;
        cout << endl;

        cout << " 8 --> pairItDL = mismatch(D1->begin(), D1->end(), L1->begin());" << endl;

        pairItDL = mismatch(D1->begin(), D1->end(), L1->begin());

        cout << endl;
        cout << " *(pairItDL.first)  = " << *(pairItDL.first) << endl;
        cout << " *(pairItDL.second) = " << *(pairItDL.second) << endl;
        cout << endl;

        cout << " 9 --> pairItdL = mismatch(&a[0], &a[DIM_MAX], L1->begin());" << endl;

        pairItdL = mismatch(&a[0], &a[DIM_MAX], L1->begin());

        cout << endl;
        cout << " *(pairItdL.first)  = " << *(pairItdL.first) << endl;
        cout << " *(pairItdL.second) = " << *(pairItdL.second) << endl;
        cout << endl;

        // delete the containers

        cout << "10 --> delete [] a;" << endl;

        delete [] a;

        cout << "11 --> delete D1;" << endl;

        delete D1;

        cout << "12 --> delete L1;" << endl;

        delete L1;

        cout << "13 --> delete V1;" << endl;

        delete V1;
    }

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

