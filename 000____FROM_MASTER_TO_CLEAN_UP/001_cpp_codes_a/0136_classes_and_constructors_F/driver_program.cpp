//===================//
// class and threads //
//===================//

#include <iostream>

// a class

class Rectangle {
public:

    Rectangle(); // default constructor
    Rectangle(int, int); // a constructor - not the default one
    void set_values(int, int);
    int area();

private:

    int width;
    int height;
};

// default constructor

Rectangle::Rectangle()
{
    std::cout << " default constructor is being called..." << std::endl;
    width = 1;
    height = 9;
};

// constructor - not the default one

Rectangle::Rectangle(int a, int b)
{
    std::cout << " the non-default constructor is being called..." << std::endl;

    width = a;
    height = b;
}

// member function definition

int Rectangle::area()
{
    return width * height;
}

// member function definition

void Rectangle::set_values(int x, int y)
{
    width = x;
    height = y;
}

// the main function

int main()
{
    // type definition

    std::cout << " 1 --> " << std::endl;
    Rectangle rect1;           // the default constructor is being called
    std::cout << " 2 --> " << std::endl;
    Rectangle rect2;           // the default constructor is being called
    std::cout << " 3 --> " << std::endl;
    Rectangle rect3 (1, 10);   // functional type for assignment
    std::cout << " 4 --> " << std::endl;
    Rectangle rect4 {1, 11};   // uniform type for assignment
    std::cout << " 5 --> " << std::endl;
    Rectangle rect5 = {1, 12}; // POD type for assignment
    std::cout << " 6 --> " << std::endl;
    Rectangle rect6 = {};      // the default constructor is being called

    // some work

    rect1.set_values(1, 1); // overides the default constructor
    std::cout << " --> rect1.area() = " << rect1.area() << std::endl;
    std::cout << " --> rect2.area() = " << rect2.area() << std::endl;
    std::cout << " --> rect3.area() = " << rect3.area() << std::endl;
    std::cout << " --> rect4.area() = " << rect4.area() << std::endl;
    std::cout << " --> rect5.area() = " << rect5.area() << std::endl;
    std::cout << " --> rect6.area() = " << rect6.area() << std::endl;

    // some work

    rect1.set_values(1, 2);
    std::cout << " --> rect1.area() = " << rect1.area() << std::endl;
    std::cout << " --> rect2.area() = " << rect2.area() << std::endl;
    std::cout << " --> rect3.area() = " << rect3.area() << std::endl;
    std::cout << " --> rect4.area() = " << rect4.area() << std::endl;
    std::cout << " --> rect5.area() = " << rect5.area() << std::endl;
    std::cout << " --> rect6.area() = " << rect6.area() << std::endl;

    // some work

    rect1.set_values(1, 3);
    std::cout << " --> rect1.area() = " << rect1.area() << std::endl;
    std::cout << " --> rect2.area() = " << rect2.area() << std::endl;
    std::cout << " --> rect3.area() = " << rect3.area() << std::endl;
    std::cout << " --> rect4.area() = " << rect4.area() << std::endl;
    std::cout << " --> rect5.area() = " << rect5.area() << std::endl;
    std::cout << " --> rect6.area() = " << rect6.area() << std::endl;

    // sentineling

    int sentinel;
    std::cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
