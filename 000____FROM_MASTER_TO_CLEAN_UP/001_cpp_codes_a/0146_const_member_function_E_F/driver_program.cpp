
// Member functions can be overloaded on their constness:
// i.e., a class may have two member functions with identical signatures
// except that one is const and the other is not:
// in this case, the const version is called only when the object is itself const,
// and the non-const version is called when the object is itself non-const.

//==================================//
// overloading members on constness //
//==================================//

#include <iostream>

// a class

class MyClass {
public :

    MyClass(int val) : x (val) { } // constructor

    const int& get() const   // public meber function
    {
        return x;
    }

    int& get()
    {
        return x;     // overloaded public meber function
    }

private :

    int x;
};

// the main function

int main()
{
    MyClass foo (10);
    const MyClass bar (20);

    foo.get() = 15;         // ok: get() returns int&
// bar.get() = 25;        // not valid: get() returns const int&

    std::cout << foo.get() << std::endl;
    std::cout << bar.get() << std::endl;

    // sentineling

    int sentinel;
    std::cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
