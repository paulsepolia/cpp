//================================//
// PFArrayDefinition.h definition //
//================================//

#ifndef PFARRAYD_DEF
#define PFARRAYD_DEF

#include <iostream>
#include "PFArrayDeclaration.h"

using std::cout;
using std::endl;

namespace pgg {
// default constructor

template<typename T, typename P>
PFArrayD<T,P>::PFArrayD() : a(NULL), capacity(T(50)), used(T(0))
{
    a = new P [capacity];
}

// non-default constructor

template<typename T, typename P>
PFArrayD<T,P>::PFArrayD(T size) : a(NULL), capacity(T(size)), used(T(0))
{
    a = new P [capacity];
}

// member function

template<typename T, typename P>
T PFArrayD<T,P>::getCapacity() const
{
    return capacity;
}

// member function

template<typename T, typename P>
T PFArrayD<T,P>::getNumberUsed() const
{
    return used;
}

// copy constructor

template<typename T, typename P>
PFArrayD<T,P>::PFArrayD(const PFArrayD& pfaObject) :
    a(NULL), capacity(pfaObject.getCapacity()), used(pfaObject.getNumberUsed())
{
    a = new P [capacity];
    for (T i = 0; i < used; i++) {
        a[i] = pfaObject.a[i];
    }
}

// member function

template<typename T, typename P>
void PFArrayD<T,P>::addElement(P element)
{
    if (used >= capacity) {
        cout << "Attempt to exceed capacity in PFArrayD." << endl;
        exit(0);
    }

    a[used] = element;
    used++;
}

// member function

template<typename T, typename P>
P& PFArrayD<T,P>::operator[](T index)
{
    if (index >= used) {
        cout << "Illegal index in PFArrayD." << endl;
        exit(0);
    }

    return a[index];
}

// member function. operator=
template<typename T, typename P>
PFArrayD<T,P>& PFArrayD<T,P>::operator=(const PFArrayD& rightSide)
{
    if (capacity != rightSide.capacity) {
        delete [] a;
        a = new P [rightSide.capacity];
    }

    capacity = rightSide.capacity;
    used = rightSide.used;

    for (T i = 0; i < used; i++) {
        a[i] = rightSide.a[i];
    }

    return *this;
}

// destructor

template<typename T, typename P>
PFArrayD<T,P>::~PFArrayD()
{
    delete [] a;
}

} // pgg

#endif // PFARRAYD_DEF

//======//
// FINI //
//======//
