
// Sieve of Eratosthenes to generate
// all prime numbers below a given limit.

#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

int main()
{
    cout << "To generate all prime numbers < N, enter N: ";

    double N;
    long int i;
    long int sqrtN;
    long int count = 0;
    long int j;

    cin >> N;

    sqrtN = pow(N, 0.5) + 1;

    cout << sqrtN << endl;

    vector<bool> S(static_cast<long int>(N), true);

    // Initially, all S[i] are true.
    // S[i] = false if and when we find i is not a prime number.
    // Some prime number

    // --> 1

    for (i = 2; i < sqrtN; i++) {
        if (S[i]) {
            for (long int k = i*i; k < N; k=k+i) {
                S[k] = false;
            }
        }
    }

    // --> 2

    for (i = 2; i < N; i++) {
        if (S[i]) {
            j = i;
            count++;
        }
    }

    // --> 3

    cout << "There are " << count
         << " prime numbers less than N." << endl;
    cout << "Largest prime number less than N is " << j << "." << endl;

    return 0;
}

//======//
// FINI //
//======//

