// alg_push_heap.cpp

#include <vector>
#include <algorithm>
#include <functional>
#include <iostream>

int main()
{
    using namespace std;
    vector <int> v1;
    vector <int> v2;
    vector <int>::iterator Iter1;
    vector <int>::iterator Iter2;

    int i;

    for (i = 1; i <= 13; i++) {
        v1.push_back(i);
    }

    random_shuffle(v1.begin(), v1.end());

    cout << "Vector v1 is ( ";
    for (Iter1 = v1.begin(); Iter1 != v1.end(); Iter1++) {
        cout << *Iter1 << " ";
    }
    cout << ")." << endl;

    // Make v1 a heap with default less than ordering
    make_heap(v1.begin(), v1.end());
    cout << "The heaped version of vector v1 is ( ";
    for (Iter1 = v1.begin(); Iter1 != v1.end(); Iter1++) {
        cout << *Iter1 << " ";
    }
    cout << ")." << endl;

    // Add an element to the heap
    v1.push_back(-1);
    cout << "The heap v1 with -1 pushed back is ( ";
    for (Iter1 = v1.begin(); Iter1 != v1.end(); Iter1++) {
        cout << *Iter1 << " ";
    }
    cout << ")." << endl;

    push_heap(v1.begin(), v1.end());
    cout << "The reheaped v1 with -1 added is ( ";
    for (Iter1 = v1.begin(); Iter1 != v1.end(); Iter1++) {
        cout << *Iter1 << " ";
    }
    cout << ")." << endl << endl;

    // Make v1 a heap with greater than ordering
    make_heap(v1.begin(), v1.end(), greater<int>());
    cout << "The greater-than heaped version of v1 is\n ( ";
    for (Iter1 = v1.begin(); Iter1 != v1.end(); Iter1++) {
        cout << *Iter1 << " ";
    }
    cout << ")." << endl;

    v1.push_back(11);
    cout << "The greater-than heap v1 with 11 pushed back is\n ( ";
    for (Iter1 = v1.begin(); Iter1 != v1.end(); Iter1++) {
        cout << *Iter1 << " ";
    }
    cout << ")." << endl;

    push_heap(v1.begin(), v1.end(), greater<int>());
    cout << "The greater than reheaped v1 with 11 added is\n ( ";
    for (Iter1 = v1.begin(); Iter1 != v1.end(); Iter1++) {
        cout << *Iter1 << " ";
    }
    cout << ")." << endl;

    int sentinel;
    std::cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
