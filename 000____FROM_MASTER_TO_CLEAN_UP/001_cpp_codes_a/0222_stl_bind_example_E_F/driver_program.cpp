
//==============//
// bind example //
//==============//

#include <iostream>
#include <functional>

using std::endl;
using std::cin;
using std::cout;
using std::bind;

// a function

double my_divide (double x, double y)
{
    return x/y;
}

// a struct

struct MyPair {
public:

    double a,b;
    double multiply()
    {
        return a*b;
    }
};

// the placeholders namespace

using namespace std::placeholders;  // adds visibility of _1, _2, _3,...

// the main function

int main ()
{
    // binding function

    auto fn_five = bind(my_divide, 10, 2);  // returns 10/2

    cout << fn_five() << endl;              // 5

    // binding function

    auto fn_half = std::bind (my_divide, _1, 2);  // returns x/2

    cout << fn_half(10) << endl;                  // 5

    // binding function

    auto fn_invert = std::bind (my_divide,_2,_1);  // returns y/x

    std::cout << fn_invert(10,2) << endl;          // 0.2

    auto fn_rounding = std::bind<int> (my_divide,_1,_2);  // returns int(x/y)

    std::cout << fn_rounding(10,3) << endl;               // 3

    // using the struct

    MyPair ten_two {10, 2};

    // binding members:

    auto bound_member_fn = std::bind (&MyPair::multiply,_1); // returns x.multiply()

    cout << bound_member_fn(ten_two) << endl;  // 20

    auto bound_member_data = std::bind (&MyPair::a,ten_two); // returns ten_two.a

    cout << bound_member_data() << endl;  // 10

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

