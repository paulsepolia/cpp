//================================//
// Author: Pavlos G. Galiatsatos  //
// Date: 2013/10/12               //
// Functions: roll dice           //
//================================//

#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

// 1. functions declaration

long int rollDice(long int num, const long int I_MAX_DO);

// 2. main function

int main()
{
    const long int I_MAX_DO = 100 * static_cast<long int>(1000000);

    cout << "Roll the dice for " << I_MAX_DO << " times" << endl;

    cout << "The number of times the dice "
         << "get the sum  2 = " << rollDice(2, I_MAX_DO) << endl;

    cout << "The number of times the dice "
         << "get the sum  3 = " << rollDice(3, I_MAX_DO) << endl;

    cout << "The number of times the dice "
         << "get the sum  4 = " << rollDice(4, I_MAX_DO) << endl;

    cout << "The number of times the dice "
         << "get the sum  5 = " << rollDice(5, I_MAX_DO) << endl;

    cout << "The number of times the dice "
         << "get the sum  6 = " << rollDice(6, I_MAX_DO) << endl;

    cout << "The number of times the dice "
         << "get the sum  7 = " << rollDice(7, I_MAX_DO) << endl;

    cout << "The number of times the dice "
         << "get the sum  8 = " << rollDice(8, I_MAX_DO) << endl;

    cout << "The number of times the dice "
         << "get the sum  9 = " << rollDice(9, I_MAX_DO) << endl;

    cout << "The number of times the dice "
         << "get the sum 10 = " << rollDice(10, I_MAX_DO) << endl;

    cout << "The number of times the dice "
         << "get the sum 11 = " << rollDice(11, I_MAX_DO) << endl;

    cout << "The number of times the dice "
         << "get the sum 12 = " << rollDice(12, I_MAX_DO) << endl;

    return 0;
}

// 3. functions definition

long int rollDice(long int num, const long int I_MAX_DO)
{
    int die1;
    int die2;
    int sum;
    long int rollCount = 0;
    long int i;

    srand(time(0));

    for (i = 0 ; i < I_MAX_DO; i++) {
        do {
            die1 = rand() % 6 + 1;
            die2 = rand() % 6 + 1;
            sum = die1 + die2;
            rollCount++;
        } while(sum != num);
    }

    return rollCount;
}

//======//
// FINI //
//======//
