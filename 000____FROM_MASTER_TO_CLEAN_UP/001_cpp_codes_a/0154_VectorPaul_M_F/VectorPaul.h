//==================//
// VectorPaul class //
//==================//

#include "IncludeLibs.h"

// a class

template <typename T>
class VectorPaul {
private:

    long dim_max;
    long dim;
    T * p1;

public:

    void setDim(long);
    long getDim();
    void allocateVector();
    void setVector(long, T);
    T getVector(long);
    void sortVector();
    void randomShuffleVector();
    T maxElementVector();
    T minElementVector();
    void reverseVector();
    void fillVector(T&);
    bool isSortedVector();
    void generateVector();
    void deleteVector();
};

//======//
// FINI //
//======//
