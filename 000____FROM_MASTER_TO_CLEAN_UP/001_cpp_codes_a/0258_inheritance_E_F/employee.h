
//===================================//
// declaration of the class Employee //
//===================================//

#ifndef EMPLOYEE_H
#define EMPLOYEE_H

#include <string>

using std::string;

namespace pgg {
class Employee {
public:

    // constructors

    Employee();
    Employee(string, string);

    // member functions

    string getName() const;
    string getSsn() const;
    double getNetPay() const;
    void setName(string);
    void setSsn(string);
    void setNetPay(double);
    void printCheck() const;

    // virtual destructor

    virtual ~Employee();

private:

    string name;
    string ssn;
    double netPay;
};

} // pgg

#endif // EMPLOYEE_H

//======//
// FINI //
//======//
