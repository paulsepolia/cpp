
///===============//
// driver program //
///===============//

#include <iostream>
#include <iomanip>
#include <string>
#include "BaseHeader.h"
#include "BaseDefinition.h"
#include "DerivedHeader.h"
#include "DerivedDefinition.h"
#include "DerivedHeader2.h"
#include "DerivedDefinition2.h"

using std::cin;
using std::cout;
using std::endl;
using std::fixed;
using std::setprecision;
using std::showpoint;
using std::showpos;
using std::string;

using pgg::base;
using pgg::derived;
using pgg::derived2;

int main()
{
    cout << fixed;
    cout << showpos;
    cout << showpoint;
    cout << setprecision(2);

    // base

    cout << "------------------------------------>> TICKET A" << endl;

    base ticketA;

    cout << " -->  1 --> " << ticketA.getTicketNumber() << endl;
    cout << " -->  2 --> " << ticketA.getTicketPrice() << endl;

    ticketA.setTicketNumber(10);
    ticketA.setTicketPrice(12.13);

    cout << " -->  3 --> " << ticketA.getTicketNumber() << endl;
    cout << " -->  4 --> " << ticketA.getTicketPrice() << endl;

    ticketA.resetTicket();

    cout << " -->  5 --> " << ticketA.getTicketNumber() << endl;
    cout << " -->  6 --> " << ticketA.getTicketPrice() << endl;

    // base

    cout << "------------------------------------>> TICKET B" << endl;

    base ticketB(11, 13.14);

    cout << " -->  7 --> " << ticketB.getTicketNumber() << endl;
    cout << " -->  8 --> " << ticketB.getTicketPrice() << endl;

    ticketA.setTicketNumber(12);
    ticketA.setTicketPrice(14.15);

    cout << " -->  9 --> " << ticketB.getTicketNumber() << endl;
    cout << " --> 10 --> " << ticketB.getTicketPrice() << endl;

    ticketB.resetTicket();

    cout << " --> 11 --> " << ticketB.getTicketNumber() << endl;
    cout << " --> 12 --> " << ticketB.getTicketPrice() << endl;

    // derived

    cout << "------------------------------------>> TICKET C" << endl;

    derived ticketC;

    cout << " --> 13 --> " << ticketC.getTicketNumber() << endl;
    cout << " --> 14 --> " << ticketC.getTicketPrice() << endl;
    cout << " --> 15 --> " << ticketC.getTicketSeason() << endl;

    ticketC.setTicketNumber(13);
    ticketC.setTicketPrice(21.22);
    ticketC.setTicketSeason("Summer");

    cout << " --> 16 --> " << ticketC.getTicketNumber() << endl;
    cout << " --> 17 --> " << ticketC.getTicketPrice() << endl;
    cout << " --> 18 --> " << ticketC.getTicketSeason() << endl;

    ticketC.resetTicket();

    cout << " --> 19 --> " << ticketC.getTicketNumber() << endl;
    cout << " --> 20 --> " << ticketC.getTicketPrice() << endl;
    cout << " --> 21 --> " << ticketC.getTicketSeason() << endl;

    // derived

    cout << "------------------------------------>> TICKET D" << endl;

    derived ticketD(14, 23.24, "WINTER");

    cout << " --> 22 --> " << ticketD.getTicketNumber() << endl;
    cout << " --> 23 --> " << ticketD.getTicketPrice() << endl;
    cout << " --> 24 --> " << ticketD.getTicketSeason() << endl;

    ticketD.setTicketNumber(15);
    ticketD.setTicketPrice(31.45);
    ticketD.setTicketSeason("AUTUM");

    cout << " --> 25 --> " << ticketD.getTicketNumber() << endl;
    cout << " --> 26 --> " << ticketD.getTicketPrice() << endl;
    cout << " --> 27 --> " << ticketD.getTicketSeason() << endl;

    ticketD.resetTicket();

    cout << " --> 28 --> " << ticketD.getTicketNumber() << endl;
    cout << " --> 29 --> " << ticketD.getTicketPrice() << endl;
    cout << " --> 30 --> " << ticketD.getTicketSeason() << endl;

    // derived2

    cout << "------------------------------------>> TICKET E" << endl;

    derived2 ticketE;

    cout << " --> 31 --> " << ticketE.getTicketNumber() << endl;
    cout << " --> 32 --> " << ticketE.getTicketPrice() << endl;
    cout << " --> 33 --> " << ticketE.getTicketSeason() << endl;
    cout << " --> 34 --> " << ticketE.getTicketType() << endl;

    ticketE.setTicketNumber(13);
    ticketE.setTicketPrice(21.22);
    ticketE.setTicketSeason("Summer");
    ticketE.setTicketType("Football");

    cout << " --> 35 --> " << ticketE.getTicketNumber() << endl;
    cout << " --> 36 --> " << ticketE.getTicketPrice() << endl;
    cout << " --> 37 --> " << ticketE.getTicketSeason() << endl;
    cout << " --> 38 --> " << ticketE.getTicketType() << endl;

    ticketE.resetTicket();

    cout << " --> 39 --> " << ticketE.getTicketNumber() << endl;
    cout << " --> 40 --> " << ticketE.getTicketPrice() << endl;
    cout << " --> 41 --> " << ticketE.getTicketSeason() << endl;
    cout << " --> 42 --> " << ticketE.getTicketType() << endl;

    // derived2

    cout << "------------------------------------>> TICKET F" << endl;

    derived2 ticketF(30, 41.11, "COLD", "EXPENSIVE");

    cout << " --> 43 --> " << ticketF.getTicketNumber() << endl;
    cout << " --> 44 --> " << ticketF.getTicketPrice() << endl;
    cout << " --> 45 --> " << ticketF.getTicketSeason() << endl;
    cout << " --> 46 --> " << ticketF.getTicketType() << endl;

    ticketF.setTicketNumber(18);
    ticketF.setTicketPrice(28.29);
    ticketF.setTicketSeason("Summer2");
    ticketF.setTicketType("Football2");

    cout << " --> 47 --> " << ticketF.getTicketNumber() << endl;
    cout << " --> 48 --> " << ticketF.getTicketPrice() << endl;
    cout << " --> 49 --> " << ticketF.getTicketSeason() << endl;
    cout << " --> 50 --> " << ticketF.getTicketType() << endl;

    ticketF.resetTicket();

    cout << " --> 51 --> " << ticketF.getTicketNumber() << endl;
    cout << " --> 52 --> " << ticketF.getTicketPrice() << endl;
    cout << " --> 53 --> " << ticketF.getTicketSeason() << endl;
    cout << " --> 54 --> " << ticketF.getTicketType() << endl;

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
