
//===================================//
// Functions: getline, clear, ignore //
// Classes: string    		     //
//===================================//

//===============//
// code --> 0006 //
//===============//

// keywords: string, getline, clear, ignore, input buffer

#include <iostream>
#include <string>

using std::cin;
using std::cout;
using std::endl;
using std::string;

// the main function

int main()
{
    string name;

    // --> 1

    cout << " --> 1 --> Read string using the 'cin >> name;' command line " << endl;

    cout << " --> 2 --> Input any string: " << endl;

    cin >> name;

    cout << name << endl;

    // --> 2 --> clean the buffer

    cin.clear();           // restore input stream

    cin.ignore(500, '\n'); // clear the buffer

    // --> 3

    cout << " --> 3 --> read string using the 'getline(cin, name);' command line " << endl;

    cout << " --> 4 --> input the same string" << endl;

    getline(cin, name);

    cout << name << endl;

    cout << " --> 5 --> end" << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//

