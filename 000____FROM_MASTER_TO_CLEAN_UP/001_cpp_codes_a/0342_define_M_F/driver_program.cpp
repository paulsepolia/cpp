//=====================//
// define preprocessor //
//=====================//

//===============//
// code --> 0342 //
//===============//

#include <iostream>
#include <iomanip>
#include <string>

using std::cout;
using std::cin;
using std::endl;
using std::fixed;
using std::showpoint;
using std::showpos;
using std::setprecision;
using std::boolalpha;
using std::string;

// define preprocessor

#define str(x) #x
#define glue(a,b) a ## b

// the main function

int main()
{
    // 1

    cout << str(test1) << endl;
    cout << str(test2) << endl;
    cout << str(test3) << endl;
    cout << str(test4) << endl;

    // 2

    string s1;
    s1 = str(test5);

    cout << s1 << endl;
    cout << s1.length() << endl;

    // 3

    glue(, cout) << "test10" << endl;
    glue(c, out) << "test11" << endl;
    glue(co, ut) << "test12" << endl;
    glue(cou, t) << "test13" << endl;
    glue(cout, ) << "test14" << endl;

    // end

    return 0;
}

// FINI
