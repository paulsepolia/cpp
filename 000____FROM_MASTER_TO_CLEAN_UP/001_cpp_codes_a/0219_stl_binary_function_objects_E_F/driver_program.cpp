
//===================================//
// creating a binary function object //
//===================================//

#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>
#include <iomanip>

using std::endl;
using std::cout;
using std::cin;
using std::vector;
using std::transform;
using std::binary_function;
using std::fixed;
using std::setprecision;
using std::showpos;
using std::showpoint;

// another template function object
// no inheritance
// EQUALLY GOOD

template<class T>
class midpointA {
public:
    T operator() (T a, T b)
    {
        return static_cast<T>((a-b)/2 + b);
    }
};

// the main function

int main()
{
    // local variables and parameters

    const int MAX_DIMEN = 10;
    vector<double> v1(MAX_DIMEN);
    vector<double> v2(MAX_DIMEN);
    vector<double> v3(MAX_DIMEN);

    // set the output format

    cout << fixed;
    cout << setprecision(20);
    cout << showpos;
    cout << showpoint;

    // build the vector v1

    for (unsigned int i = 0; i < v1.size(); i++) {
        v1[i] = static_cast<double>(i);
    }

    // build the vector v2

    for (unsigned int i = 0; i < v2.size(); i++) {
        v2[i] = 10+static_cast<double>(i);
    }

    // build the vector v3

    for (unsigned int i = 0; i < v3.size(); i++) {
        v3[i] = static_cast<double>(0.0);
    }

    // display the v1

    cout << endl;

    for (unsigned int i = 0; i < v1.size(); i++) {
        cout << i << " --> " << v1[i] << endl;
    }

    // display the v2

    cout << endl;

    for (unsigned int i = 0; i < v2.size(); i++) {
        cout << i << " --> " << v2[i] << endl;
    }

    // apply the binary function object

    transform(v1.begin(), v1.end(), v2.begin(), v3.begin(), midpointA<double>());

    // display the midpoints

    cout << endl;

    for (unsigned int i = 0; i < v3.size(); i++) {
        cout << i << " --> " << v3[i] << endl;
    }

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
