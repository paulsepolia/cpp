
//=================================//
// Functions: assert               //
// Preprocessor directives: NDEBUG //
//=================================//

//===============//
// code --> 0010 //
//===============//

// keywords: NDEBUG, preprocessor, assert, disable assert


// the following line disables 'assert' function

#define NDEBUG

#include <iostream>
#include <cassert>

using std::cout;
using std::cin;
using std::endl;

// the main function

int main()
{
    // --> 1 --> declare variables

    int numerator;
    int denominator;
    int quotient;

    cout << " --> 1" << endl;

    cout << " --> Enter the numerator:" << endl;

    cin >> numerator;

    cout << " --> 2 --> test the state of the cin input stream" << endl;

    if(cin) {
        cout << " --> All is okay with the given input" << endl;
    }

    cout << " --> Enter the denominator:" << endl;

    cin >> denominator;

    cout << " --> 3 --> test the state of the cin input stream" << endl;

    if(cin) {
        cout << " --> All is okay with the given input" << endl;
    }

    cout << " --> 4 --> assert(denominator)" << endl;

    assert(denominator);

    cout << " --> 5" << endl;

    quotient = numerator / denominator;

    cout << " --> numerator = " << numerator << endl;
    cout << " --> denominator = " << denominator << endl;
    cout << " --> quotient is = " << quotient << endl;

    cout << " --> 6 --> end" << endl;

    // sentineling

    int sentinel;
    cin >> sentinel;

    cout << " --> 7 --> exit" << endl;

    return 0;
}

//======//
// FINI //
//======//

