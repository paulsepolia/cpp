#include <iostream>
#include <iomanip>

class Singleton {
private:

    static bool instanceFlag;
    static Singleton *single;

    Singleton() {

        std::cout << std::boolalpha << " --> private constructor --> instance flag = " << instanceFlag << std::endl;
        std::cout << " --> private constructor" << std::endl;
    }

public:
    static Singleton *getInstance();

    void method();

private:
    ~Singleton() {
        std::cout << std::boolalpha << " --> private destructor --> instance flag = " << instanceFlag << std::endl;
        std::cout << "private destructor" << std::endl;
        instanceFlag = false;
    }
};

bool Singleton::instanceFlag = false;
Singleton *Singleton::single = NULL;

Singleton *Singleton::getInstance() {

    std::cout << std::boolalpha << " --> getInstance --> instance flag = " << instanceFlag << std::endl;

    if (!instanceFlag) {
        single = new Singleton();
        instanceFlag = true;
        return single;
    } else {
        return single;
    }
}

void Singleton::method() {
    std::cout << "Method of the singleton class" << std::endl;
}

int main() {

    Singleton *sc1;
    Singleton *sc2;

    sc1 = Singleton::getInstance();
    sc1->method();
    sc2 = Singleton::getInstance();
    sc2->method();

    return 0;
}