#include <iostream>
#include <vector>
#include <cmath>
#include <execution>
#include <algorithm>
#include <iomanip>

int main() {

    const auto DIM_MAX = static_cast<uint64_t>(2 * std::pow(10.0, 8.0));
    const auto DO_MAX = static_cast<uint64_t>(std::pow(10.0, 6.0));
    const auto MOD_VAL = static_cast<uint64_t>(100);
    const double VAL1 = 0.99;
    const double VAL2 = 1.0 / VAL1;

    std::cout << std::fixed;
    std::cout << std::setprecision(10);

    std::vector<double> vec(DIM_MAX, 2.5);

    for (uint64_t k = 0; k < DO_MAX; k++) {

        std::transform(std::execution::par, vec.begin(), vec.end(), vec.begin(),
                       [&](const auto &v) {
                           return v * VAL1;
                       });

        std::transform(std::execution::par, vec.begin(), vec.end(), vec.begin(),
                       [&](const auto &v) {
                           return v * VAL2;
                       });

        if (k % MOD_VAL == 0) {
            std::cout << "----------------------------------->> k = " << k << std::endl;
            std::cout << vec[0] << std::endl;
            std::cout << vec[1] << std::endl;
        }
    }
}