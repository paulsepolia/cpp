#!/bin/bash

  g++-9.1   -O3         \
            -Wall       \
            -std=gnu++2a\
            -pthread    \
            -fopenmp    \
            -pedantic   \
            -I/opt/tbb/2019/include/  \
            -L/opt/tbb/2019/lib/ -ltbb \
            main.cpp   \
            -o x_gnu

