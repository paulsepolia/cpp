#include <vector>
#include <random>
#include <iostream>
#include <numeric>
#include <execution>
#include <chrono>

class benchmark_timer {
public:

    explicit benchmark_timer(const double &res) :
            _res(res),
            t1{std::chrono::steady_clock::now()},
            t2{std::chrono::steady_clock::now()} {};

    ~benchmark_timer() {
        t2 = std::chrono::steady_clock::now();
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count()};
        std::cout << " --> res = " << _res << std::endl;
        std::cout << " --> time used (secs) = " << total_time << std::endl;
    }

    auto set_res(const double &res) -> void {
        _res = res;
    }

private:

    double _res{};
    decltype(std::chrono::steady_clock::now()) t1{};
    decltype(std::chrono::steady_clock::now()) t2{};
};


auto random_number_generator() -> double {

    // usage of thread local random engines
    // allows running the generator in concurrent mode

    thread_local static std::default_random_engine rd;

    auto dist{std::uniform_real_distribution<double>(0, 1)};

    return dist(rd);
}

constexpr auto DO_MAX{(size_t) 1000000};

auto main() -> int {

    constexpr auto size_loc{(size_t) 500000000};

    for (size_t kk = 0; kk < DO_MAX; kk++) {

        benchmark_timer ot(0.0);

        std::cout << "-------------------------------------->> kk = " << kk << std::endl;

        auto v1{std::vector<double>(size_loc)};
        auto v2{std::vector<double>(size_loc)};

        // initialize vectors with random numbers

        std::generate(std::execution::par, v1.begin(), v1.end(), random_number_generator);
        std::generate(std::execution::par, v2.begin(), v2.end(), random_number_generator);

        // the dot product calculation

        double res = std::transform_reduce(std::execution::par_unseq,
                                           v1.cbegin(), v1.cend(),
                                           v2.cbegin(),
                                           .0,
                                           std::plus<>(),
                                           std::multiplies<>());

        std::cout << " --> The dot product is: " << res << std::endl;
        ot.set_res(res);
    }
}
