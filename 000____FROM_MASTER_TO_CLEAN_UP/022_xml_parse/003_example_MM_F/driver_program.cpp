#include <set>
#include <iostream>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/foreach.hpp>

using std::set;
using std::cout;
using std::endl;


struct threats_to_resolve {
    std::string           helios_id;
    std::set<std::string> uuids;
};


threats_to_resolve load(const std::string &file)
{
    boost::property_tree::ptree pt;
    read_xml(file, pt);

    threats_to_resolve co;

    for(boost::property_tree::ptree::value_type &v: pt.get_child("resolve_threat.uuids")){

		std::cout << v.second.data() << std::endl;
	}

    return co;
}

void save(const threats_to_resolve &co, const std::string &file)
{
    boost::property_tree::ptree pt;

    pt.put("resolve_threat.helios_id", co.helios_id);

    BOOST_FOREACH(
        const std::string &name, co.uuids)
    pt.add("resolve_threat.uuids.uuid", name);

    write_xml(file, pt);
}

int main()
{
	try
    {
        threats_to_resolve ds;
		const std::string input("example.xml");
		const std::string output("example_out.xml");

        load(input);
        save(ds, output);
        std::cout << "Success\n";
    }
    catch (std::exception &e)
    {
        std::cout << "Error: " << e.what() << std::endl;
    }
    return 0;
}

