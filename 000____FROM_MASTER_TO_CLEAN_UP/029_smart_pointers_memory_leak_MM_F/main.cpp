#include <iostream>
#include <iomanip>
#include <memory>
#include <cmath>

class A {
public:

    std::shared_ptr<A> sp;
};

int main() {

    const auto DIM = static_cast<size_t>(std::pow(10.0, 5.0));

    for (size_t i = 0; i < DIM; i++) {

        std::shared_ptr<A> sp1 = std::make_shared<A>();
        std::shared_ptr<A> sp2 = std::make_shared<A>();

        sp1->sp = sp2;
        sp2->sp = sp1;
    }
}
