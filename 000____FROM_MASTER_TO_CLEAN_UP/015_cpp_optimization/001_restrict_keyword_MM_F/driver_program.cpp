#include <iostream>
#include <chrono>

using namespace std::chrono;

const uint64_t DIM(50*1024);

void example2a(float *a, float *b, float *c) {

    for (uint64_t i = 0; i < DIM; i++) {

        a[i] = 0.0f;
        b[i] = 0.0f;

        for (uint64_t j = 0; j < DIM; j++) {
            a[i] = a[i] + c[i * DIM + j];
            b[i] = b[i] + c[i * DIM + j] * c[i * DIM + j];
        }
    }
}


void example2b(float *__restrict__ a, float *__restrict__ b, float *__restrict__ c) {

    for (uint64_t i = 0; i < DIM; i++) {

        a[i] = 0.0f;
        b[i] = 0.0f;

        for (uint64_t j = 0; j < DIM; j++) {
            a[i] = a[i] + c[i * DIM + j];
            b[i] = b[i] + c[i * DIM + j] * c[i * DIM + j];
        }
    }
}


int main() {

    float *a(new float[DIM]);
    float *b(new float[DIM]);
    float *c(new float[DIM * DIM]);

    auto t1(system_clock::now());
    auto t2(system_clock::now());
    duration<double> time_span;

    // 1

    for (uint64_t i = 0; i < DIM; i++) {
        a[i] = static_cast<float>(i);
        b[i] = static_cast<float>(i + 1);
    }

    for (uint64_t i = 0; i < DIM * DIM; i++) {
        c[i] = static_cast<float>(i);
    }

    t1 = system_clock::now();
    example2a(a, b, c);
    t2 = system_clock::now();

    time_span = duration_cast<duration<double>>(t2-t1);
    std::cout << " --> time used --> 1 --> " << time_span.count() << std::endl;
    std::cout << " --> a[0] --> 1 --> " << a[0] << std::endl;
    std::cout << " --> b[0] --> 1 --> " << b[0] << std::endl;

    // 2

    for (uint64_t i = 0; i < DIM; i++) {
        a[i] = static_cast<float>(i);
        b[i] = static_cast<float>(i + 1);
    }

    for (uint64_t i = 0; i < DIM * DIM; i++) {
        c[i] = static_cast<float>(i);
    }

    t1 = system_clock::now();
    example2b(a, b, c);
    t2 = system_clock::now();

    time_span = duration_cast<duration<double>>(t2-t1);
    std::cout << " --> time used --> 2 --> " << time_span.count() << std::endl;
    std::cout << " --> a[0] --> 2 --> " << a[0] << std::endl;
    std::cout << " --> b[0] --> 2 --> " << b[0] << std::endl;

    return 0;
}
