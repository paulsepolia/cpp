#include <iostream>

// You save a lot of RAM space if you adopt a "is-a"
// relationship instead a "has-a" relationship

class A1 {
public:
    auto f1() const -> void {
        std::cout << "-------------->> f1 from A1" << std::endl;
    }
};

// private inheritance ("is-a" relationship)
class A2 : private A1 {
private:
    double x1{0.0};
};

// private inheritance ("is-a" relationship)
class A3 : private A1 {
private:
    double x1{0.0};
    double x2{0.0};
};

// composition ("has-a" relationship)
class B2 {
private:
    double x1{0.0};
    A1 a1;
};

// composition ("has-a" relationship)
class B3 {
private:
    double x1{0.0};
    double x2{0.0};
    A1 a1;
};

class C2 {
private:
    A1 a1;
    double x1{0.0};
};

class C3 {
private:
    A1 a1;
    double x1{0.0};
    double x2{0.0};
};

auto main() -> int {

    const auto DIM_MAX{(size_t) 1'000};

    {
        std::cout << "--------------------------->> 1" << std::endl;

        std::cout << "--> sizeof(A1) --> " << sizeof(A1) << std::endl;
        std::cout << "--> sizeof(A2) --> " << sizeof(A2) << std::endl;
        std::cout << "--> sizeof(A3) --> " << sizeof(A3) << std::endl;

        A1 a1;
        A2 a2;
        A3 a3;

        std::cout << "--> sizeof(a1) --> " << sizeof(a1) << std::endl;
        std::cout << "--> sizeof(a2) --> " << sizeof(a2) << std::endl;
        std::cout << "--> sizeof(a3) --> " << sizeof(a3) << std::endl;

        std::cout << "--> sizeof(A1[DIM_MAX]) --> " << sizeof(A1[DIM_MAX]) << std::endl;
        std::cout << "--> sizeof(A2[DIM_MAX]) --> " << sizeof(A2[DIM_MAX]) << std::endl;
        std::cout << "--> sizeof(A3[DIM_MAX]) --> " << sizeof(A3[DIM_MAX]) << std::endl;
    }

    {
        std::cout << "--------------------------->> 2" << std::endl;

        std::cout << "--> sizeof(A1) --> " << sizeof(A1) << std::endl;
        std::cout << "--> sizeof(B2) --> " << sizeof(B2) << std::endl;
        std::cout << "--> sizeof(B3) --> " << sizeof(B3) << std::endl;

        A1 a1;
        B2 b2;
        B3 b3;

        std::cout << "--> sizeof(a1) --> " << sizeof(a1) << std::endl;
        std::cout << "--> sizeof(b2) --> " << sizeof(b2) << std::endl;
        std::cout << "--> sizeof(b3) --> " << sizeof(b3) << std::endl;

        std::cout << "--> sizeof(A1[DIM_MAX]) --> " << sizeof(A1[DIM_MAX]) << std::endl;
        std::cout << "--> sizeof(B2[DIM_MAX]) --> " << sizeof(B2[DIM_MAX]) << std::endl;
        std::cout << "--> sizeof(B3[DIM_MAX]) --> " << sizeof(B3[DIM_MAX]) << std::endl;
    }

    {
        std::cout << "--------------------------->> 3" << std::endl;

        std::cout << "--> sizeof(A1) --> " << sizeof(A1) << std::endl;
        std::cout << "--> sizeof(C2) --> " << sizeof(C2) << std::endl;
        std::cout << "--> sizeof(C3) --> " << sizeof(C3) << std::endl;

        A1 a1;
        C2 c2;
        C3 c3;

        std::cout << "--> sizeof(a1) --> " << sizeof(a1) << std::endl;
        std::cout << "--> sizeof(c2) --> " << sizeof(c2) << std::endl;
        std::cout << "--> sizeof(c3) --> " << sizeof(c3) << std::endl;

        std::cout << "--> sizeof(A1[DIM_MAX]) --> " << sizeof(A1[DIM_MAX]) << std::endl;
        std::cout << "--> sizeof(C2[DIM_MAX]) --> " << sizeof(C2[DIM_MAX]) << std::endl;
        std::cout << "--> sizeof(C3[DIM_MAX]) --> " << sizeof(C3[DIM_MAX]) << std::endl;
    }
}