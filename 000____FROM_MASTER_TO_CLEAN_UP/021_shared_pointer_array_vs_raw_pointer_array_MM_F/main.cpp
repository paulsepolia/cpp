#include <iostream>
#include <memory>
#include <cmath>
#include <chrono>
#include <numeric>
#include <iomanip>

const auto DIM_MAX = static_cast<uint64_t>(std::pow(10.0, 7.0));
const auto DO_MAX = static_cast<uint64_t>(std::pow(10.0, 3.0));

int main() {

    std::cout << std::fixed;
    std::cout << std::setprecision(10);

    {
        const auto t1 = std::chrono::steady_clock::now();
        double sum_loc = 0;

        for (uint64_t i = 0; i < DO_MAX; i++) {

            std::shared_ptr<double[]> sp_a;
            sp_a.reset(new double[DIM_MAX]);

            for (uint64_t j = 0; j < DIM_MAX; j++) {
                sp_a[j] = static_cast<double>(j);
            }

            sum_loc = std::accumulate(sp_a.get(), sp_a.get() + DIM_MAX, sum_loc);
        }

        const auto t2 = std::chrono::steady_clock::now();
        const auto td = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count();
        std::cout << " --> td-shared-pointer-array = " << td << std::endl;
        std::cout << " --> sum_loc = " << sum_loc << std::endl;
    }

    {
        const auto t1 = std::chrono::steady_clock::now();
        double sum_loc = 0;

        for (uint64_t i = 0; i < DO_MAX; i++) {

            double * p_a = nullptr;
            p_a = new double[DIM_MAX];

            for (uint64_t j = 0; j < DIM_MAX; j++) {
                p_a[j] = static_cast<double>(j);
            }

            sum_loc = std::accumulate(p_a, p_a + DIM_MAX, sum_loc);

            delete [] p_a;
        }

        const auto t2 = std::chrono::steady_clock::now();
        const auto td = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count();
        std::cout << " --> td-raw-pointer-array = " << td << std::endl;
        std::cout << " --> sum_loc = " << sum_loc << std::endl;
    }

    return 0;
}
