#
# Copyright 2017 Intel Corporation.
# 
# The source code, information and material ("Material") contained herein is owned by Intel
# Corporation or its suppliers or licensors, and title to such Material remains with Intel
# Corporation or its suppliers or licensors. The Material contains proprietary information of Intel
# or its suppliers and licensors. The Material is protected by worldwide copyright laws and treaty
# provisions. No part of the Material may be used, copied, reproduced, modified, published,
# uploaded, posted, transmitted, distributed or disclosed in any way without Intel's prior express
# written permission. No license under any patent, copyright or other intellectual property rights
# in the Material is granted to or conferred upon you, either expressly, by implication, inducement,
# estoppel or otherwise. Any license under such intellectual property rights must be express and
# approved by Intel in writing.
# 
# Include any supplier copyright notices as supplier requires Intel to use.
# 
# Include supplier trademarks or logos as supplier requires Intel to use, preceded by an asterisk.
# An asterisked footnote can be added as follows: *Third Party trademarks are the property of their
# respective owners.
# 
# Unless otherwise agreed by Intel in writing, you may not remove or alter this notice or any other
# notice embedded in Materials by Intel or Intel's suppliers or licensors in any way.
# This file is part of Threading Building Blocks. Threading Building Blocks is free software;
# you can redistribute it and/or modify it under the terms of the GNU General Public License
# version 2  as  published  by  the  Free Software Foundation.  Threading Building Blocks is
# distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See  the GNU General Public License for more details.   You should have received a copy of
# the  GNU General Public License along with Threading Building Blocks; if not, write to the
# Free Software Foundation, Inc.,  51 Franklin St,  Fifth Floor,  Boston,  MA 02110-1301 USA
#
# As a special exception,  you may use this file  as part of a free software library without
# restriction.  Specifically,  if other files instantiate templates  or use macros or inline
# functions from this file, or you compile this file and link it with other files to produce
# an executable,  this file does not by itself cause the resulting executable to be covered
# by the GNU General Public License. This exception does not however invalidate any other
# reasons why the executable file might be covered by the GNU General Public License.

# The C++ compiler
#CXX=g++

# detecting MS Windows (for MinGW support)
ifeq ($(OS), Windows_NT)
RM = cmd /C del /Q /F
RD = cmd /C rmdir
UI = con
EXE = $(NAME)$(SUFFIX).exe

else
RM = rm -f
RD = rmdir -r

# detecting 64-bit platform
arch ?= $(shell uname -m)
# Possible values of interest: intel64 x86_64 amd64 ia64 ppc64 sparc sparc64
x64 ?= $(findstring 64,$(subst sparc,sparc64,$(arch)))

# see https://wiki.debian.org/Multiarch/Tuples
MULTIARCH = $(arch)
ifeq ($(arch),ia32)
MULTIARCH = i386
endif
ifeq ($(arch),intel64)
MULTIARCH = x86_64
endif
ifeq ($(arch),ppc32)
MULTIARCH = powerpc
endif
ifeq ($(arch),sparc)
MULTIARCH = sparc64
endif
MULTIARCHTUPLE ?= $(MULTIARCH)-linux-gnu

# detecting UI ("mac", "x" or "con")
ifeq ($(shell uname),Darwin)
  UI ?= mac
else
  UI ?= $(shell sh -c "[ -f /usr/X11R6/lib$(x64)/libX11.so -o -f /usr/lib$(x64)/libX11.so -o -f /usr/lib/$(MULTIARCHTUPLE)/libX11.so ] && echo x")
endif

ifeq ($(UI),x)
  EXE = $(NAME)$(SUFFIX)
  UI_CXXFLAGS += -I/usr/X11R6/include
  LIBS += -lpthread -L/usr/X11R6/lib$(x64) -lX11
  # detect if libXext can be found
  ifeq ($(shell sh -c "[ -f /usr/X11R6/lib$(x64)/libXext.so -o -f /usr/lib$(x64)/libXext.so -o -f /usr/lib/$(MULTIARCHTUPLE)/libXext.so ] && echo 0"),0)
    LIBS += -lXext
  else  # no libXext
    UI_CXXFLAGS += -DX_NOSHMEM
  endif # libXext

else # ! X
  ifeq ($(UI),mac)
    CXX_UI?=g++
    LIBS += -framework OpenGL -framework Foundation -framework Cocoa
    MACUISOURCES = ../../common/gui/xcode/tbbExample/OpenGLView.m ../../common/gui/xcode/tbbExample/main.m ../../common/gui/xcode/tbbExample/tbbAppDelegate.m 
    MACUIOBJS = OpenGLView.o main.o tbbAppDelegate.o
    APPRES = $(NAME)$(SUFFIX).app/Contents/Resources
    EXE = $(NAME)$(SUFFIX).app/Contents/MacOS/$(NAME)$(SUFFIX)

  else # ! OS X*

    EXE = $(NAME)$(SUFFIX)
    ifeq (,$(strip $(UI)))
       UI = con
       $(warning Note: no graphics output capability detected, building for console output.)
    endif
  endif # OS X*
endif # X
endif # Windows vs. other
