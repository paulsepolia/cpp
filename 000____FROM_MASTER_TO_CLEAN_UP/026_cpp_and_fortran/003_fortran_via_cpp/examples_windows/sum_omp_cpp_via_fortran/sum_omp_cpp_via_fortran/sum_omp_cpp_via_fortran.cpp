
 # include <iostream>
 # include <ctime>

 using namespace std ;

 extern "C" 
 { void FORTRAN_OMP_SUB( double& , int& , int&, int& ) ; }

 int main()
 {
   int n1 ; 
   int n2 ;
   int  nt ;
   double resta ;

   n1 = 5 ;
   n2 = 5 ;
   nt = 2 ;
  
   time_t t1 ;
   t1 = clock() ;

   FORTRAN_OMP_SUB( resta, n1, n2, nt ) ;

   time_t t2 ;
   t2 = clock() ;

   cout << " n1    --> " << n1    << endl ;
   cout << " n2    --> " << n2    << endl ;
   cout << " nt    --> " << nt    << endl ;
   cout << " resta --> " << resta << endl ;
   cout << " total cpp cpu time --> " << (t2-t1)/1000. << endl ;
		
   return 0;
}

