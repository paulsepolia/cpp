!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/07/11              !
!===============================!

  program c_fortran_pointer

  use m_1_parameters
  use, intrinsic :: iso_c_binding, only: c_f_pointer, C_INT, C_PTR

  implicit none

  ! 1. function interface

  interface
    function c_allocate(n) bind(C, name="c_allocate")
      import :: C_PTR, C_INT
      implicit none
      type(C_PTR)                :: c_allocate
      integer(kind=C_INT), value :: n
    end function c_allocate
  end interface

  ! 2. local variables and parameters

  integer(kind=si), parameter              :: ROWS = 3
  integer(kind=si), parameter              :: COLS = 5
  integer(kind=C_INT), pointer, contiguous :: fort_ptr(:,:)
  type(C_PTR)                              :: cptr
  integer(kind=C_INT)                      :: grid_size
  integer(kind=si)                         :: i
  integer(kind=si)                         :: j

  ! 3. local settings

  grid_size = ROWS * COLS
  cptr = c_allocate(grid_size)

  ! 4. establish pointer association

  call c_f_pointer(cptr, fort_ptr, [ROWS, COLS])

  write(*,*) " --> size(fort_ptr, dim=1) --> ", size(fort_ptr, dim=1)
  write(*,*) " --> size(fort_ptr, dim=2) --> ", size(fort_ptr, dim=2)

  write(*,*) ""

  do j = 1, COLS
    do i = 1, ROWS
      write(*,*) " --> ", i, " --> ", j, " --> fort_ptr(i,j) --> ", fort_ptr(i,j)
    end do
  end do

  end program c_fortran_pointer
