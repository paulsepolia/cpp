//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2014/10/21              //
//===============================//

#include <stdio.h>
#include <string.h>

void c_routine(char * string)
{
	printf("  --> c_routine: length = %i\n", (int) strlen(string));
  	printf("  --> c_routine: text = %s\n", string);
}
