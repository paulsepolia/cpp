//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2014/10/21              //
//===============================//

#include <iostream>
#include <cmath>
#include <iomanip>
#include <ctime>
#include <omp.h>

#include "m_2_fortran_vec_add_cpp.h"

using std::endl;
using std::cin;
using std::cout;
using std::pow;
using std::fixed;
using std::setprecision;
using std::clock;

// the main function

int main()
{
	// --> local parameters

  	const long DIM_VEC = 1 * static_cast<long>(pow(10.0, 8.0));
	const long K_MAX = static_cast<long>(pow(10.0, 4.0));

	// --> local variables	

	long i;
	long k;
	clock_t t1;
	clock_t t2;
	int nt;

	// --> main test for-loop

	for (k = 0 ; k != K_MAX; ++k)
	{
		// --> counter

		cout << "------------------------------------------------------>> " << k << endl;
 
  		// --> allocation of RAM

		cout << " --> RAM allocation" << endl;

  		double * array_a = new double [DIM_VEC];
  		double * array_b = new double [DIM_VEC];
		double * array_c = new double [DIM_VEC];

		// --> adjust the output format

		cout << fixed;
		cout << setprecision(20);

  		// --> initialization

		cout << " --> building the arrays" << endl;

		t1 = clock();

		#pragma omp parallel default(none)   \
				     shared(array_a) \
				     shared(array_b) \
				     shared(array_c) \
			     	     private(i)
		{
			#pragma omp for

  			for(i = 0; i < DIM_VEC; ++i)
  			{
    				array_a[i] = static_cast<double>(i);
    				array_b[i] = static_cast<double>(i);
				array_c[i] = 0.0;
  			}
		}

		// get the number of openmp threads

		#pragma omp parallel default(none) \
				     shared(nt)
		{
			nt = omp_get_num_threads();
		}

		t2 = clock();

		cout << " --> time used = " 
		     << (t2-t1+0.0)/(CLOCKS_PER_SEC * nt) << endl;

  		// --> call the fortran defined function

		cout << " --> fortran_dot execution via cpp" << endl;

		t1 = clock();

  		fortran_vec_add_(array_a, array_b, array_c, &DIM_VEC);

		t2 = clock();

  		cout << " --> array_c[0]         = " << array_c[0] << endl;
  		cout << " --> array_c[1]         = " << array_c[1] << endl;
  		cout << " --> array_c[2]         = " << array_c[2] << endl;
  		cout << " --> array_c[3]         = " << array_c[3] << endl;
	        cout << " --> array_c[DIM_VEC-2] = " << array_c[DIM_VEC-2] << endl;
	        cout << " --> array_c[DIM_VEC-1] = " << array_c[DIM_VEC-1] << endl;

		cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC/nt << endl;

  		// --> free up RAM

		cout << " --> delete RAM" << endl;

  		delete [] array_a;
  		delete [] array_b;
		delete [] array_c;
 
	}
 
	// XX --> sentineling

	cout << " --> end" << endl;

	int sentinel;
	cin >> sentinel;

  	return 0;
}
