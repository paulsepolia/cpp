#include <iostream>
#include <variant>
#include <string>

int main() {

    std::cout << "sizeof string: " << sizeof(std::string) << std::endl;

    std::cout << "sizeof variant<int, string>: " << sizeof(std::variant<int, std::string>) << std::endl;

    std::cout << "sizeof variant<int, float>: " << sizeof(std::variant<int, float>) << std::endl;

    std::cout << "sizeof variant<int, double>: " << sizeof(std::variant<int, double>) << std::endl;
}