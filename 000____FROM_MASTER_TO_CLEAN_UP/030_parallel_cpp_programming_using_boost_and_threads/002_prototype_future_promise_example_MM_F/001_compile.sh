#!/bin/bash

  g++   -O3          \
        -Wall        \
        -std=gnu++17 \
        -pthread     \
        main.cpp     \
        -o x_gnu
