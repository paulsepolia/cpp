#include <iostream>
#include <queue>
#include <vector>
#include <cmath>
#include <boost/thread.hpp>
#include <boost/chrono.hpp>
#include <thread>

const auto CHUNK_DIM = static_cast<size_t>(std::pow(10.0, 4));
const auto QUEUE_MAX_SIZE = static_cast<size_t>(std::pow(10.0, 3));
const auto MAX_DO_WORK = static_cast<size_t>(std::pow(10.0, 6));
const auto DEBUG_MOD = static_cast<size_t>(std::pow(10.0, 4));
const bool DEBUG_FLG = false;

class Writer {
public:

    Writer() : data{}, data_push(0), data_pop(0) {}

    virtual ~Writer() = default;

    void create_data() {

        std::cout << " --> creating data ..." << std::endl;

        while (true) {

            boost::this_thread::interruption_point();
            ++data_push;

            if (DEBUG_FLG && data_push % DEBUG_MOD == 0) {
                std::cout << " --> data push --> " << data_push << std::endl;
            }

            std::vector<double> chunk;
            for (size_t i = 0; i < CHUNK_DIM; i++) {
                chunk.push_back(i);
            }
            data.push(chunk);
        }
    }

    void get_data() {

        std::cout << " --> consuming data ..." << std::endl;

        while (true) {

            boost::this_thread::interruption_point();
            const bool flg = data.empty();

            if (!flg) {
                data_pop++;
                if (DEBUG_FLG && data_pop % DEBUG_MOD == 0) {
                    std::cout << " --> data pop --> " << data_pop << std::endl;
                }
                std::vector<double> chunk(data.front());
                data.pop();
            }
        }
    }

    size_t get_size() {
        return data.size();
    }

    size_t get_data_push_counter() {
        return data_push;
    }

private:

    std::queue<std::vector<double>> data;
    size_t data_push;
    size_t data_pop;
};


int main() {

    Writer WR;

    boost::thread th1(boost::bind(&Writer::create_data, &WR));
    boost::thread th2(boost::bind(&Writer::get_data, &WR));

    while (true) {

        if (WR.get_size() > QUEUE_MAX_SIZE) {
            std::cout << "Size of queue is of maximum size." << std::endl;
            std::cout << "Interrupting the thread which creates data." << std::endl;
            th1.interrupt();
            std::cout << "Waiting the get data thead to consume all the data..." << std::endl;
            if (WR.get_size() == 0) {
                th2.interrupt();
            }
            break;
        } else if (WR.get_data_push_counter() > MAX_DO_WORK) {
            std::cout << "Maximum allowed work for data-creation-thread is done." << std::endl;
            th1.interrupt();
            std::cout << "Waiting the get-data-thead to consume all the data..." << std::endl;
            if (WR.get_size() == 0) {
                th2.interrupt();
            }
            break;
        }

        std::this_thread::sleep_for(std::chrono::seconds(1));
    }

    th1.join();
    th2.join();
}
