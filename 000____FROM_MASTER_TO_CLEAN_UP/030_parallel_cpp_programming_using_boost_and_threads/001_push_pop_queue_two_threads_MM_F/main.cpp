#include <iostream>
#include <queue>
#include <vector>
#include <cmath>
#include <thread>
#include <future>

const auto CHUNK_DIM = static_cast<size_t>(std::pow(10.0, 4));
const auto DATA_MAX_SIZE = static_cast<size_t>(std::pow(10.0, 3));
const auto DATA_MAX = static_cast<size_t>(std::pow(10.0, 4));
const auto DEBUG_MOD = static_cast<size_t>(std::pow(10.0, 3));

std::queue<std::vector<double>> data;

size_t data_push = 0;
size_t data_pop = 0;

void create_data(std::future<void> future_obj) {

    std::cout << " --> create data --> started" << std::endl;

    while (true) {

        if (future_obj.wait_for(std::chrono::milliseconds(1)) != std::future_status::timeout) {
            break;
        }

        ++data_push;

        if (data_push % DEBUG_MOD == 0) {
            std::cout << " --> data push --> " << data_push << std::endl;
        }

        if (data_push > DATA_MAX) break;

        std::vector<double> chunk;

        for (size_t i = 0; i < CHUNK_DIM; i++) {
            chunk.push_back(i);
        }

        data.push(chunk);
    }

    std::cout << " --> create data --> ended" << std::endl;
}

void get_data(std::future<void> future_obj) {

    std::cout << " --> get data --> started" << std::endl;

    while (true) {

        if (future_obj.wait_for(std::chrono::milliseconds(1)) != std::future_status::timeout) {
            break;
        }

        const bool flg = data.empty();

        if (!flg) {

            data_pop++;

            if (data_pop % DEBUG_MOD == 0) {
                std::cout << " --> data pop --> " << data_pop << std::endl;
            }

            std::vector<double> chunk(data.front());
            data.pop();
        }

        if (data_pop >= DATA_MAX) break;
    }

    std::cout << " --> get data --> ended" << std::endl;
}


int main() {

    // create a std::promise object
    std::promise<void> exit_signal1;
    std::promise<void> exit_signal2;

    // fetch std::future object associated with promise
    std::future<void> future_obj1 = exit_signal1.get_future();
    std::future<void> future_obj2 = exit_signal2.get_future();

    // start threads
    std::thread th1(&create_data, std::move(future_obj1));
    std::thread th2(&get_data, std::move(future_obj2));

    while (true) {

        if (data.size() > DATA_MAX_SIZE) {
            std::cout << " --> asking data_create thread to stop..." << std::endl;
            exit_signal1.set_value();
            exit_signal2.set_value();
            break;
        } else if (data_push > DATA_MAX) {
            break;
        }

        std::this_thread::sleep_for(std::chrono::seconds(1));
    }

    // wait for thread to join

    th1.join();
    th2.join();
}
