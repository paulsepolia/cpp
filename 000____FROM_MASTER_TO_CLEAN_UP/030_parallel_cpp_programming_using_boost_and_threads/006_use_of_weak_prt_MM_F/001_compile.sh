#!/bin/bash

  g++-8.2.0 -O3          \
            -Wall        \
	        -pthread     \
            -std=gnu++17 \
            main.cpp     \
            -o x_gnu
