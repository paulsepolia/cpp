#include <iostream>
#include <memory>

int main() {

    // empty definition
    std::shared_ptr<double> sp;

    // takes ownership of pointer
    sp.reset(new double(222.22));

    // get pointer to data without taking ownership
    std::weak_ptr<double> weak1 = sp;

    // deletes managed object, acquires new pointer
    sp.reset(new double(333.33));

    // get pointer to new data without taking ownership
    std::weak_ptr<double> weak2 = sp;

    // weak1 is expired!
    if (auto tmp = weak1.lock()) {
        std::cout << *tmp << std::endl;
    } else {
        std::cout << "weak1 is expired" << std::endl;
    }

    // weak2 points to new data
    if (auto tmp = weak2.lock()) {
        std::cout << *tmp << std::endl;
    } else {
        std::cout << "weak2 is expired" << std::endl;
    }
}