#!/bin/bash
	
  g++-8.2.0 	-O2            \
    	        -Wall          \
     		    -std=gnu++17   \
     		    -pthread       \
                -I/opt/boost/1.69.0/include \
                -L/opt/boost/1.69.0/lib -lboost_system \
     	    	-L/opt/boost/1.69.0/lib -lboost_thread \
                -L/opt/boost/1.69.0/lib -lboost_chrono \
     		    main.cpp       \
      		    -o x_gnu
