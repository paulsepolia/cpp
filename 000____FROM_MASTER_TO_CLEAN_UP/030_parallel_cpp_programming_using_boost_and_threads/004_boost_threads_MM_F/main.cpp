#include <boost/thread.hpp>
#include <boost/chrono.hpp>
#include <iostream>

const uint64_t MAX_DO = 20;

void wait(uint64_t seconds) {
    boost::this_thread::sleep_for(boost::chrono::seconds{seconds});
}

void thread1() {
    try {
        for (uint64_t i = 0; i < MAX_DO; ++i) {
            wait(1);
            std::cout << " --> thread = 1 --> wait for --> " << i << std::endl;
        }
    }
    catch (boost::thread_interrupted &) {
        std::cout << " interrupted " << std::endl;
    }
}

void thread2() {

    boost::this_thread::disable_interruption no_interruption;

    try {
        for (uint64_t i = 0; i < MAX_DO; ++i) {
            wait(1);
            std::cout << " --> thread = 2 --> wait for --> " << i << std::endl;
        }
    }
    catch (boost::thread_interrupted &) {
        std::cout << " interrupted" << std::endl;
    }
}

int main() {

    std::cout << " --> thread 1, started" << std::endl;

    boost::thread t1{thread1};
    wait(10);
    t1.interrupt();

    std::cout << " --> thread 1, interrupted" << std::endl;
    std::cout << " --> thread 2, started" << std::endl;

    boost::thread t2{thread2};
    wait(10);
    t2.interrupt();

    std::cout << " --> thread 2, interrupted --> but has been disabled" << std::endl;

    t1.join();
    t2.join();

    return 0;
}
