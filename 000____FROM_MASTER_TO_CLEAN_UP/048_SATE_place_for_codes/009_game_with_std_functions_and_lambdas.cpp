#include <iostream>
#include <vector>
#include <functional>

auto main() -> int {

    {
        std::cout << "---------------------------------->> 1" << std::endl;
        auto lbd{[](const double &a, const double &b) -> double {
            return a + b;
        }};

        std::cout << lbd(10.0, 20.0) << std::endl;
    }
    {
        std::cout << "---------------------------------->> 2" << std::endl;

        const auto val{30.00};

        auto lbd{[=](const double &a, const double &b) -> double {
            return a + b + val;
        }};

        std::cout << lbd(10.0, 20.0) << std::endl;

        std::vector<decltype(lbd)> v;

        v.push_back(lbd);
        v.push_back(lbd);
        v.push_back(lbd);
        v.push_back(lbd);

        auto a{10.0};
        auto b{20.0};

        for (const auto &el: v) {
            a = el(a, b);
            std::cout << a << std::endl;
        }
    }
    {
        std::cout << "---------------------------------->> 3" << std::endl;

        const auto val{30.00};

        auto lbd{[val](const double &a, const double &b) -> double {
            return a + b + val;
        }};

        std::cout << lbd(10.0, 20.0) << std::endl;

        std::vector<decltype(lbd)> v;

        v.push_back(lbd);
        v.push_back(lbd);
        v.push_back(lbd);
        v.push_back(lbd);

        auto a{10.0};
        auto b{20.0};

        for (const auto &el: v) {
            a = el(a, b);
            std::cout << a << std::endl;
        }
    }
    {
        std::cout << "---------------------------------->> 3" << std::endl;

        const auto val{30.00};

        auto lbd{[val](const double &a, const double &b) -> double {
            return a + b + val;
        }};

        std::cout << lbd(10.0, 20.0) << std::endl;

        std::vector<decltype(lbd)> v;

        v.push_back(lbd);
        v.push_back(lbd);
        v.push_back(lbd);
        v.push_back(lbd);

        auto a{10.0};
        auto b{20.0};

        for (const auto &el: v) {
            a = el(a, b);
            std::cout << a << std::endl;
        }
    }
    {
        std::cout << "---------------------------------->> 4" << std::endl;

        const auto val{30.00};

        auto lbd{[val](const double &a, const double &b) -> double {
            return a + b + val;
        }};

        std::cout << lbd(10.0, 20.0) << std::endl;

        std::vector<decltype(lbd)> v;

        auto a{10.0};
        auto b{20.0};

        v.push_back(lbd);
        v.push_back(lbd);
        v.push_back(lbd);
        v.push_back(lbd);
        v.push_back(lbd);
        v.push_back(lbd);
        v.push_back(lbd);

        for (const auto &el: v) {
            a = el(a, b);
            std::cout << a << std::endl;
        }
    }
    {
        std::cout << "---------------------------------->> 5" << std::endl;

        const auto val{30.00};

        std::function<double(const double &, const double &)> f;

        auto lbd{[val](const double &a, const double &b) -> double {
            return a + b + val;
        }};

        f = lbd;

        std::cout << f(10.0, 20.0) << std::endl;

        std::vector<decltype(f)> v;

        v.push_back(f);
        v.push_back(f);
        v.push_back(f);
        v.push_back(f);
        v.push_back(f);
        v.push_back(f);
        v.push_back(f);
        v.emplace_back(lbd);

        auto a{10.0};
        auto b{20.0};

        for (const auto &el: v) {
            a = el(a, b);
            std::cout << a << std::endl;
        }
    }
    {
        std::cout << "---------------------------------->> 6" << std::endl;

        const auto val{30.00};

        std::function<double(const double &, const double &)> f;

        auto lbd{[val](const double &a, const double &b) -> double {
            return a + b + val;
        }};

        f = lbd;

        std::cout << f(10.0, 20.0) << std::endl;

        std::vector<decltype(f)> v;

        v.emplace_back(lbd);
        v.emplace_back(lbd);
        v.emplace_back(lbd);
        v.emplace_back(lbd);
        v.emplace_back(lbd);
        v.emplace_back(lbd);
        v.emplace_back(lbd);
        v.emplace_back(lbd);

        auto a{10.0};
        auto b{20.0};

        for (const auto &el: v) {
            a = el(a, b);
            std::cout << a << std::endl;
        }
    }
}