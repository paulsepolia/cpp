// promise example
#include <iostream>       // std::cout
#include <functional>     // std::ref
#include <thread>         // std::thread
#include <future>         // std::promise, std::future

void print_int(std::future<int> &fut) {

    std::cout << "--> pgg --> 1" << std::endl;
    int x = fut.get();
    std::cout << "--> pgg --> 2" << std::endl;
    std::cout << "value: " << x << '\n';
    std::cout << "--> pgg --> 3" << std::endl;
}

int main() {
    std::cout << "--> pgg --> 4" << std::endl;
    std::promise<int> prom;                      // create promise
    std::cout << "--> pgg --> 5" << std::endl;

    std::future<int> fut = prom.get_future();    // engagement with future

    std::cout << "--> pgg --> 6" << std::endl;
    std::thread th1(print_int, std::ref(fut));  // send future to new thread

    std::cout << "--> pgg --> 7" << std::endl;
    prom.set_value(10);                         // fulfill promise
    // (synchronizes with getting the future)

    std::cout << "--> pgg --> 8" << std::endl;
    th1.join();

    //prom.set_value (11);
    std::cout << "--> pgg --> 9" << std::endl;
    return 0;
}