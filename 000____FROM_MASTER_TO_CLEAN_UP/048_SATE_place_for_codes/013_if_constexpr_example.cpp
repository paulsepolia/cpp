#include <iostream>
#include <vector>
#include <memory>
#include <functional>
#include <chrono>
#include <iomanip>
#include <cmath>
#include <cassert>

class benchmark_timer {
public:

    explicit benchmark_timer(const double &res) :
            _res(res),
            t1{std::chrono::steady_clock::now()},
            t2{std::chrono::steady_clock::now()} {};

    ~benchmark_timer() {
        t2 = std::chrono::steady_clock::now();
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count()};
        std::cout << " --> res = " << _res << std::endl;
        std::cout << " --> time used (secs) = " << total_time << std::endl;
    }

    auto set_res(const double &res) -> void {
        _res = res;
    }

private:

    double _res{};
    decltype(std::chrono::steady_clock::now()) t1{};
    decltype(std::chrono::steady_clock::now()) t2{};
};

class A {
public:

    explicit A(std::string name) : _name(std::move(name)) {}

    [[nodiscard]] auto get_name() const -> std::string {
        return _name;
    }

    auto set_name(std::string name) -> void {
        _name = std::move(name);
    }

    virtual ~A() = default;

private:
    std::string _name;
};

template<typename T>
auto generic_mod(const T &v, const T &n) -> T {
    assert(n != 0);
    if constexpr (std::is_floating_point_v<T>) {
        return std::fmod(v, n);
    } else {
        return v % n;
    }
}


int main() {

    {
        std::cout << "--------------------------------------->> 1" << std::endl;
        std::cout << generic_mod(10.0, 4.0) << std::endl;
    }
    {
        std::cout << "--------------------------------------->> 1" << std::endl;
        std::cout << generic_mod(10.0, 4.1) << std::endl;
    }
    {
        std::cout << "--------------------------------------->> 1" << std::endl;
        std::cout << generic_mod(10.0, 4.2) << std::endl;
    }
    {
        std::cout << "--------------------------------------->> 1" << std::endl;
        std::cout << generic_mod(10.0, 4.3) << std::endl;
    }
    {
        std::cout << "--------------------------------------->> 2" << std::endl;
        std::cout << generic_mod(10, 4) << std::endl;
    }
}