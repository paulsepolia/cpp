
#include <iostream>
#include <cmath>
#include <chrono>
#include <iomanip>
#include <vector>
#include <memory>
#include <thread>
#include <mutex>
#include <array>

const auto DIM1 = static_cast<uint64_t>(std::pow(10.0, 3.0));
const auto DIM2 = static_cast<uint64_t>(std::pow(10.0, 8.0));
const auto MOD1 = 10 * static_cast<uint64_t>(std::pow(10.0, 2.0));
const auto NUM_T = static_cast<uint64_t>(2);

std::mutex mtx;

void fun() {

    std::cout << std::fixed;
    std::cout << std::setprecision(5);

    std::vector<std::shared_ptr<int>> v1;

    auto t1 = std::chrono::steady_clock::now();

    for (uint64_t k = 1; k <= DIM2; k++) {
        for (uint64_t i = 0; i < DIM1; i++) {
            std::shared_ptr<int> p_tmp(new int[DIM1]);
            v1.push_back(p_tmp);
        }

        if (k % MOD1 == 0) {
            v1.clear();
            const auto t2 = std::chrono::steady_clock::now();
            const auto tp = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count();

            std::unique_lock<std::mutex> lck(mtx, std::defer_lock);
            lck.lock();

            std::cout << " -->> t-id = " << std::setw(8) << std::right << std::this_thread::get_id()
                      << " -->> k = " << std::setw(8) << std::right << k
                      << " --> t-used = " << tp << std::endl;

            lck.unlock();

            t1 = t2;
        }
    }
}

int main() {

    std::array<std::thread, NUM_T> threads_ar;

    for (auto &el : threads_ar) {
        el = std::thread(fun);
    }

    for (auto &el : threads_ar) {
        el.join();
    }

    int sentinel;
    std::cout << "Enter integer to exit local scope: ";
    std::cin >> sentinel;
    std::cout << "pgg --> after the exit" << std::endl;
}