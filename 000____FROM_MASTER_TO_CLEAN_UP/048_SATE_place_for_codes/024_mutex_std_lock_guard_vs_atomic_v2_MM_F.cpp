#include <iostream>
#include <cmath>
#include <thread>
#include <iomanip>
#include <vector>
#include <atomic>
#include <mutex>

const auto DO_MAX{(size_t) (std::pow(10.0, 8.0))};

constexpr auto NUM_TH{uint32_t(8)};
constexpr auto ZERO_UINT64{uint64_t(0.0)};
constexpr auto ONE_UINT64{uint64_t(1.0)};

auto sum_mtx{std::mutex{}};
auto sum_res_atomic{std::atomic<uint64_t>(ZERO_UINT64)};
auto sum_res_mtx{ZERO_UINT64};

class benchmark_timer {
public:

    explicit benchmark_timer(const double &res) :
            _res(res),
            t1{std::chrono::steady_clock::now()},
            t2{std::chrono::steady_clock::now()} {};

    ~benchmark_timer() {
        t2 = std::chrono::steady_clock::now();
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count()};
        std::cout << "--> res = " << _res << std::endl;
        std::cout << "--> time used (secs) = " << total_time << std::endl;
    }

    auto set_res(const double &res_local) -> void {
        _res = res_local;
    }

private:

    double _res{};
    decltype(std::chrono::steady_clock::now()) t1{};
    decltype(std::chrono::steady_clock::now()) t2{};
};

auto sum_atomic_v1(size_t do_times) -> uint64_t {

    for (size_t kk{0}; kk < do_times; kk++) {
        sum_res_atomic.fetch_add(ONE_UINT64, std::memory_order_seq_cst);
    }

    return sum_res_atomic;
}

auto sum_atomic_v2(size_t do_times) -> uint64_t {

    for (size_t kk{0}; kk < do_times; kk++) {
        sum_res_atomic.fetch_add(ONE_UINT64, std::memory_order_relaxed);
    }

    return sum_res_atomic;
}

auto sum_atomic_v3(size_t do_times) -> uint64_t {

    for (size_t kk{0}; kk < do_times; kk++) {
        sum_res_atomic.fetch_add(ONE_UINT64, std::memory_order_release);
    }

    return sum_res_atomic;
}

auto sum_atomic_v4(size_t do_times) -> uint64_t {

    for (size_t kk{0}; kk < do_times; kk++) {
        sum_res_atomic.fetch_add(ONE_UINT64, std::memory_order_acq_rel);
    }

    return sum_res_atomic;
}

auto sum_atomic_v5(size_t do_times) -> uint64_t {

    for (size_t kk{0}; kk < do_times; kk++) {
        sum_res_atomic.fetch_add(ONE_UINT64, std::memory_order_acquire);
    }

    return sum_res_atomic;
}

auto sum_atomic_v6(size_t do_times) -> uint64_t {

    for (size_t kk{0}; kk < do_times; kk++) {
        sum_res_atomic.fetch_add(ONE_UINT64, std::memory_order_consume);
    }

    return sum_res_atomic;
}

auto sum_mtx_fast(size_t do_times) -> uint64_t {

    auto lock{std::lock_guard<std::mutex>{sum_mtx}};

    for (size_t kk{0}; kk < do_times; kk++) {
        sum_res_mtx += ONE_UINT64;
    }
    return sum_res_mtx;
}

int main() {

    std::cout << std::boolalpha;
    std::cout << std::setprecision(5);
    std::cout << std::fixed;

    {
        std::cout << "--------------------------->> 0 --> using mutexes" << std::endl;
        std::cout << "--> create threads" << std::endl;

        auto fun = sum_mtx_fast;

        auto vec{std::vector<std::thread>()};

        benchmark_timer ot(0);

        for (uint32_t i{0}; i < NUM_TH; i++) {
            vec.emplace_back(std::thread(fun, DO_MAX));
        }

        std::cout << "--> join threads" << std::endl;

        for (auto &el : vec) {
            el.join();
        }

        std::cout << "--> is it okay = " << (sum_res_mtx == DO_MAX * NUM_TH) << std::endl;
        ot.set_res(sum_res_mtx);
    }

    {
        std::cout << "--------------------------->> 1 --> using atomic operations --> v1" << std::endl;
        std::cout << "--> create threads" << std::endl;

        sum_res_atomic = ZERO_UINT64;

        auto fun = sum_atomic_v1;

        auto vec{std::vector<std::thread>()};

        benchmark_timer ot(0);

        for (uint32_t i{0}; i < NUM_TH; i++) {
            vec.emplace_back(std::thread(fun, DO_MAX));
        }

        std::cout << "--> join threads" << std::endl;

        for (auto &el : vec) {
            el.join();
        }

        std::cout << "--> is it okay = " << (sum_res_atomic == DO_MAX * NUM_TH) << std::endl;
        ot.set_res(sum_res_atomic);
    }

    {
        std::cout << "--------------------------->> 2 --> using atomic operations --> v2" << std::endl;
        std::cout << "--> create threads" << std::endl;

        sum_res_atomic = ZERO_UINT64;

        auto fun = sum_atomic_v2;

        auto vec{std::vector<std::thread>()};

        benchmark_timer ot(0);

        for (uint32_t i{0}; i < NUM_TH; i++) {
            vec.emplace_back(std::thread(fun, DO_MAX));
        }

        std::cout << "--> join threads" << std::endl;

        for (auto &el : vec) {
            el.join();
        }

        std::cout << "--> is it okay = " << (sum_res_atomic == DO_MAX * NUM_TH) << std::endl;
        ot.set_res(sum_res_atomic);
    }

    {
        std::cout << "--------------------------->> 3 --> using atomic operations --> v3" << std::endl;
        std::cout << "--> create threads" << std::endl;

        sum_res_atomic = ZERO_UINT64;

        auto fun = sum_atomic_v3;

        auto vec{std::vector<std::thread>()};

        benchmark_timer ot(0);

        for (uint32_t i{0}; i < NUM_TH; i++) {
            vec.emplace_back(std::thread(fun, DO_MAX));
        }

        std::cout << "--> join threads" << std::endl;

        for (auto &el : vec) {
            el.join();
        }

        std::cout << "--> is it okay = " << (sum_res_atomic == DO_MAX * NUM_TH) << std::endl;
        ot.set_res(sum_res_atomic);
    }

    {
        std::cout << "--------------------------->> 4 --> using atomic operations --> v4" << std::endl;
        std::cout << "--> create threads" << std::endl;

        sum_res_atomic = ZERO_UINT64;

        auto fun = sum_atomic_v4;

        auto vec{std::vector<std::thread>()};

        benchmark_timer ot(0);

        for (uint32_t i{0}; i < NUM_TH; i++) {
            vec.emplace_back(std::thread(fun, DO_MAX));
        }

        std::cout << "--> join threads" << std::endl;

        for (auto &el : vec) {
            el.join();
        }

        std::cout << "--> is it okay = " << (sum_res_atomic == DO_MAX * NUM_TH) << std::endl;
        ot.set_res(sum_res_atomic);
    }

    {
        std::cout << "--------------------------->> 5 --> using atomic operations --> v5" << std::endl;
        std::cout << "--> create threads" << std::endl;

        sum_res_atomic = ZERO_UINT64;

        auto fun = sum_atomic_v5;

        auto vec{std::vector<std::thread>()};

        benchmark_timer ot(0);

        for (uint32_t i{0}; i < NUM_TH; i++) {
            vec.emplace_back(std::thread(fun, DO_MAX));
        }

        std::cout << "--> join threads" << std::endl;

        for (auto &el : vec) {
            el.join();
        }

        std::cout << "--> is it okay = " << (sum_res_atomic == DO_MAX * NUM_TH) << std::endl;
        ot.set_res(sum_res_atomic);
    }

    {
        std::cout << "--------------------------->> 6 --> using atomic operations --> v6" << std::endl;
        std::cout << "--> create threads" << std::endl;

        sum_res_atomic = ZERO_UINT64;

        auto fun = sum_atomic_v6;

        auto vec{std::vector<std::thread>()};

        benchmark_timer ot(0);

        for (uint32_t i{0}; i < NUM_TH; i++) {
            vec.emplace_back(std::thread(fun, DO_MAX));
        }

        std::cout << "--> join threads" << std::endl;

        for (auto &el : vec) {
            el.join();
        }

        std::cout << "--> is it okay = " << (sum_res_atomic == DO_MAX * NUM_TH) << std::endl;
        ot.set_res(sum_res_atomic);
    }
}
