#include <iostream>
#include <iomanip>
#include <memory>
#include <cmath>

class A {
public:
    virtual void fun() {
        std::cout << " --> A" << std::endl;
    }

    virtual ~A() {
        std::cout << " --> ~A" << std::endl;
    }
};

class B : public A {
public:
    void fun() final {
        std::cout << " --> B" << std::endl;
    }

    ~B() final {
        std::cout << " --> ~B" << std::endl;
    }
};


class A1 {
public:
    virtual void fun() {
        std::cout << " --> A1" << std::endl;
    }

    ~A1() {
        std::cout << " --> ~A1" << std::endl;
    }
};

class B1 : public A1 {
public:
    void fun() final {
        std::cout << " --> B" << std::endl;
    }

    ~B1() {
        std::cout << " --> ~B" << std::endl;
    }
};

int main() {

    {
        std::cout << " --> example --> 1 --> start" << std::endl;

        B b;
        A &a = b;
        B *pb = dynamic_cast<B *>(&a);
        if (pb) {
            std::cout << " --> down-casting is successful" << std::endl;
        }

        std::cout << " --> example --> 1 --> end" << std::endl;
    }

    {
        std::cout << " --> example --> 2 --> start" << std::endl;

        B b;
        A &a = b;

        try {
            B &pb = dynamic_cast<B &>(a);
            std::cout << " --> down-casting is successful" << std::endl;
        }
        catch (std::exception &e) {
            std::cout << " --> something happened --> " << e.what() << std::endl;
        }

        std::cout << " --> example --> 2 --> end" << std::endl;
    }

    {
        std::cout << " --> example --> 3 --> start" << std::endl;

        A b;
        A *a = &b;
        B *pb = dynamic_cast<B *>(a);
        if (pb) {
            std::cout << " --> down-casting is successful" << std::endl;
        }

        std::cout << " --> example --> 3 --> end" << std::endl;
    }

    {
        std::cout << " --> example --> 4 --> start" << std::endl;

        A b;
        A &a = b;
        try {
            B &pb = dynamic_cast<B &>(a);
            std::cout << " --> down-casting is successful" << std::endl;
        }
        catch (std::exception &e) {
            std::cout << " --> something happened --> " << e.what() << std::endl;
        }

        std::cout << " --> example --> 4 --> end" << std::endl;
    }

    {
        std::cout << " --> example --> 5 --> start" << std::endl;
        A * pa = new B;
        delete pa;
        std::cout << " --> example --> 5 --> end" << std::endl;
    }

    {
        std::cout << " --> example --> 6 --> start" << std::endl;
        A1 * pa = new B1;
        delete pa;
        std::cout << " --> example --> 6 --> end" << std::endl;
    }
}