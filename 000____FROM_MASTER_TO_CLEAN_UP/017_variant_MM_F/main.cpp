#include <iostream>
#include <variant>

enum class ErrorCode {
    Ok,
    SystemError,
    IoError,
    NetworkError
};

std::variant<std::string, ErrorCode> FetchNameFromNetwork(int i) {

    if (i == 0) {
        return ErrorCode::SystemError;
    } else if (i == 1) {
        return ErrorCode::NetworkError;
    } else if (i == 2) {
        return ErrorCode::Ok;
    } else if (i == 3) {
        return ErrorCode::IoError;
    }

    return std::string("Hello World!");
}

int main() {

    {
        const auto response = FetchNameFromNetwork(0);

        if (std::holds_alternative<std::string>(response)) {
            std::cout << std::get<std::string>(response) << std::endl;
        } else {
            std::cout << "Error!" << std::endl;
        }
    }

    {
        const auto response = FetchNameFromNetwork(1);

        if (std::holds_alternative<std::string>(response)) {
            std::cout << std::get<std::string>(response) << std::endl;
        } else {
            std::cout << "Error!" << std::endl;
        }
    }

    {
        const auto response = FetchNameFromNetwork(10);

        if (std::holds_alternative<std::string>(response)) {
            std::cout << std::get<std::string>(response) << std::endl;
        } else {
            std::cout << "Error!" << std::endl;
        }
    }

    return 0;
}