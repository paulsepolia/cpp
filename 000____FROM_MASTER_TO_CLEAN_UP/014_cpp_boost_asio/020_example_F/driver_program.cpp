#include <iostream>
#include <functional>
#include <memory>
#include <mutex>
#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <thread>
#include <chrono>

std::mutex global_stream_lock{};

void worker_thread(std::shared_ptr<boost::asio::io_service> io_svc, const uint32_t &counter) {

    global_stream_lock.lock();
    std::cout << "Thread " << counter << " --> Start" << std::endl;
    global_stream_lock.unlock();

    io_svc->run();

    global_stream_lock.lock();
    std::cout << "Thread " << counter << " --> End" << std::endl;
    global_stream_lock.unlock();
}

void print_function(const uint32_t &index) {

    std::cout << "--> Index = " << index << std::endl;
}

int main() {

    const uint32_t NUM_THREADS(4);
    const uint32_t NUM_JOBS(10);

    // create a shared pointer to the io_service object

    std::cout << "--> 1" << std::endl;

    std::shared_ptr<boost::asio::io_service> io_svc(new boost::asio::io_service);

    std::cout << "--> 2" << std::endl;

    // create a shared pointer to the io_service::work object

    std::shared_ptr<boost::asio::io_service::work> worker(new boost::asio::io_service::work(*io_svc));

    std::cout << "--> 3" << std::endl;

    // lock here and inform the user

    global_stream_lock.lock();
    std::cout << "The program will exit once all the work has finished" << std::endl;
    global_stream_lock.unlock();

    // create a group of boost threads

    boost::thread_group threads{};

    for (uint32_t i(1); i <= NUM_THREADS; i++) {

        threads.create_thread(std::bind(&worker_thread, io_svc, i));
    }

    std::cout << "--> 4" << std::endl;

    // sleep for a while

    std::this_thread::sleep_for(std::chrono::milliseconds(1000));

    std::cout << "--> 5" << std::endl;

    // post jobs here in order
    // BUT THEY ARE NOT EXECUTED IN ORDER

    for(uint32_t i = 1; i <= NUM_JOBS; i++) {

        io_svc->post(std::bind(&print_function, i));
        std::cout << " --> i = " << i << std::endl;
    }

    worker.reset();

    std::cout << "--> 11" << std::endl;

    threads.join_all();

    std::cout << "--> 12" << std::endl;

    return 0;
}
