#include <boost/asio.hpp>
#include <iostream>

int main() {

    boost::asio::io_service io_svc{};

    // no work has been given to io_svc so there is no blocking
    io_svc.run();

    std::cout << " We will see this line" << std::endl;

    return 0;
}