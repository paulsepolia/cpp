#!/bin/bash

  g++       -O3                \
            -Wall              \
            -std=c++0x         \
            -pthread           \
            driver_program.cpp \
            -I /opt/Fotech/helios/include \
            /opt/Fotech/helios/lib/*.so  \
            -o x_gnu
