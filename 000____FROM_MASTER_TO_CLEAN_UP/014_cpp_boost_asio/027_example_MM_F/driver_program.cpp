#include <iostream>
#include <chrono>
#include <iomanip>
#include </opt/Fotech/helios/include/boost/thread.hpp>

int main()
{
    auto t1(std::chrono::system_clock::now());
    auto t2(std::chrono::system_clock::now());
    std::chrono::duration<double> time_span;
    const double TIME_TO_SLEEP(10);

    std::cout << std::setprecision(5); 

    for(int i = 0; i != 10; i++) {

        std::cout << " --> i = " << i << std::endl;

        t1 = std::chrono::system_clock::now();

        boost::this_thread::sleep(boost::posix_time::seconds(TIME_TO_SLEEP));

        t2 = std::chrono::system_clock::now();

        time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2-t1);

        std::cout << " --> time passed = " << std::fixed << time_span.count() << " seconds" << std::endl;
        std::cout << " --> time passed should be = " << std::fixed << TIME_TO_SLEEP  << " seconds" << std::endl;
    }

    return 0;
}
