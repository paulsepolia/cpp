#include <boost/process.hpp>
#include <string>
#include <iostream>

int main()
{
    boost::process::ipstream pipe_stream;
    boost::process::child c("gcc --version", boost::process::std_out > pipe_stream);

    std::string line;

    while (pipe_stream && std::getline(pipe_stream, line) && !line.empty())
	{
        std::cout << line << std::endl;
	}

    c.wait();
}

