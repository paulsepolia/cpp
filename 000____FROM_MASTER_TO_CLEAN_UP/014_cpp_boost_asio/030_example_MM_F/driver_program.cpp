#include <boost/process.hpp>
#include <string>
#include <iostream>

int main()
{
    // input

    //std::string cmd("df -h"); 
	//std::string cmd("tail -F driver_program.cpp");
    std::string cmd("find /home/fotech/ -name *");

    boost::process::ipstream pipe_stream;
    boost::process::child c(cmd, boost::process::std_out > pipe_stream);

    std::string line;

    while (pipe_stream && std::getline(pipe_stream, line) && !line.empty())
	{
        std::cout << line << std::endl;
	}

    c.wait();
}

