#!/bin/bash

  g++-9.2.0  -c           \
             -O3          \
             -Wall        \
             -std=gnu++17 \
	     -I/mnt/shared/opt/boost/1.70.0/include/ \
             -I/usr/include/python2.7 \
	     main.cpp

  g++-9.2.0  -pthread \
             /mnt/shared/opt/boost/1.70.0/lib/*.so \
             main.o \
	     -o x_gnu

  rm *.o
