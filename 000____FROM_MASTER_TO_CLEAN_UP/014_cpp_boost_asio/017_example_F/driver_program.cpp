#include <iostream>
#include <functional>
#include <memory>
#include <mutex>
#include <boost/asio.hpp>
#include <boost/thread.hpp>

std::mutex global_stream_lock{};
const uint32_t K_MAX(100000);

// this function is only to provide an idea which thread works and when

void worker_thread(std::shared_ptr<boost::asio::io_service> io_svc,
                   const uint32_t &counter) {

    // initial greetings

    global_stream_lock.lock();
    std::cout << " --> Hello from counter --> " << counter << std::endl;
    global_stream_lock.unlock();

    // blocks here and execute any given work
    // until the is_svc->stop() is called

    io_svc->poll();

    // final greetings

    global_stream_lock.lock();
    std::cout << " --> Bye from counter --> " << counter << std::endl;
    global_stream_lock.unlock();
}

//=================================================//
// this function has the main (the only) work load //
//=================================================//

void sum_fun(const uint32_t &index) {

    global_stream_lock.lock();
    std::cout << " --> Executing thread with index --> " << index << std::endl;
    global_stream_lock.unlock();

    double_t sum(0);

    for (uint32_t i(1); i <= K_MAX; i++) {
        for (uint32_t j(1); j <= K_MAX; j++) {
            sum = sum + static_cast<double_t>(i) * j;
        }
    }

    global_stream_lock.lock();
    std::cout << " --> Done thread with index --> " << index << ", sum --> " << sum << std::endl;
    global_stream_lock.unlock();
}

int main() {

    const uint32_t THREADS_MAX(4);
    const uint32_t NUM_JOBS_TOTAL(1000);

    // create a pointer to the io_service object

    std::shared_ptr<boost::asio::io_service> io_svc(new boost::asio::io_service);

    // create a pointer to the io_service::work object

    std::shared_ptr<boost::asio::io_service::work> worker(new boost::asio::io_service::work(*io_svc));

    // create a group of boost threads
    // Do not create more threads than the actual available cores
    // unless you know what you are doing

    boost::thread_group threads{};

    for (uint32_t i(1); i <= THREADS_MAX; i++) {

        std::cout << " --> create thread with index --> " << i << std::endl;
        threads.create_thread(std::bind(&worker_thread, io_svc, i));
    }

    // give a lot of job here to the io_svc object
    // as soon as any thread is available it get some

    for (uint32_t i(1); i <= NUM_JOBS_TOTAL; i++) {

        std::cout << "--> post the job number --> " << i << std::endl;
        io_svc->post(std::bind(&sum_fun, i));
        //io_svc->dispatch(std::bind(&sum_fun, i));
    }

    // unblocks the io_svc->run();
    // and execution ends before has been completed
    // so i need to block the execution here using a read statement as

    //std::cout << "--> Enter a number here to reach the io_svc->stop(), to stop any execution";
    std::cin.get();

    // the statement below stops any execution by the io_svc pointer

    io_svc->stop();

    std::cout << "--> Enter a number here to reach the threads.join_all()";
    std::cin.get();

    threads.join_all();

    return 0;
}
