#include <vector>
#include <iostream>
#include <cmath>
#include <chrono>

const auto OBJS_NUM = static_cast<uint64_t>(std::pow(10.0, 7.0));
const auto DO_MAX = static_cast<uint64_t>(std::pow(10.0, 3.0));

class alignas(4) vec4 {
public:

    vec4() : x1{0}, x2{0}, x3{0}, x4{0} {}

    float x1;
    float x2;
    float x3;
    float x4;
};

class alignas(16) vec16 {
public:

    vec16() : x1{0}, x2{0}, x3{0}, x4{0} {}

    float x1;
    float x2;
    float x3;
    float x4;
};

class alignas(256) vec32 {
public:

    vec32() : x1{0}, x2{0}, x3{0}, x4{0} {}

    float x1;
    float x2;
    float x3;
    float x4;
};

int main() {

    {
        const auto t1 = std::chrono::steady_clock::now();

        auto pv4 = new vec4[OBJS_NUM];

        for (uint64_t k1 = 1; k1 <= DO_MAX; k1++) {

            for (uint64_t i = 0; i < OBJS_NUM; i++) {

                pv4[i].x1 = static_cast<float>(i + 0);
                pv4[i].x2 = static_cast<float>(i + 1);
                pv4[i].x3 = static_cast<float>(i + 2);
                pv4[i].x4 = static_cast<float>(i + 3);
            }
        }

        std::cout << pv4[0].x1 << std::endl;
        std::cout << pv4[0].x2 << std::endl;
        std::cout << pv4[0].x3 << std::endl;
        std::cout << pv4[0].x4 << std::endl;

        std::cout << pv4[10].x1 << std::endl;
        std::cout << pv4[10].x2 << std::endl;
        std::cout << pv4[10].x3 << std::endl;
        std::cout << pv4[10].x4 << std::endl;

        delete[] pv4;
        const auto t2 = std::chrono::steady_clock::now();
        const auto td4 = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count();

        std::cout << " --> td4 = " << td4 << std::endl;
    }

    {
        const auto t1 = std::chrono::steady_clock::now();

        auto pv16 = new vec16[OBJS_NUM];

        for (uint64_t k1 = 1; k1 <= DO_MAX; k1++) {

            for (uint64_t i = 0; i < OBJS_NUM; i++) {

                pv16[i].x1 = static_cast<float>(i + 0);
                pv16[i].x2 = static_cast<float>(i + 1);
                pv16[i].x3 = static_cast<float>(i + 2);
                pv16[i].x4 = static_cast<float>(i + 3);
            }
        }

        std::cout << pv16[0].x1 << std::endl;
        std::cout << pv16[0].x2 << std::endl;
        std::cout << pv16[0].x3 << std::endl;
        std::cout << pv16[0].x4 << std::endl;

        std::cout << pv16[10].x1 << std::endl;
        std::cout << pv16[10].x2 << std::endl;
        std::cout << pv16[10].x3 << std::endl;
        std::cout << pv16[10].x4 << std::endl;

        delete[] pv16;
        const auto t2 = std::chrono::steady_clock::now();
        const auto td16 = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count();

        std::cout << " --> td16 = " << td16 << std::endl;
    }

    {
        const auto t1 = std::chrono::steady_clock::now();

        auto pv32 = new vec32[OBJS_NUM];

        for (uint64_t k1 = 1; k1 <= DO_MAX; k1++) {

            for (uint64_t i = 0; i < OBJS_NUM; i++) {

                pv32[i].x1 = static_cast<float>(i + 0);
                pv32[i].x2 = static_cast<float>(i + 1);
                pv32[i].x3 = static_cast<float>(i + 2);
                pv32[i].x4 = static_cast<float>(i + 3);
            }
        }

        std::cout << pv32[0].x1 << std::endl;
        std::cout << pv32[0].x2 << std::endl;
        std::cout << pv32[0].x3 << std::endl;
        std::cout << pv32[0].x4 << std::endl;

        std::cout << pv32[10].x1 << std::endl;
        std::cout << pv32[10].x2 << std::endl;
        std::cout << pv32[10].x3 << std::endl;
        std::cout << pv32[10].x4 << std::endl;

        delete[] pv32;
        const auto t2 = std::chrono::steady_clock::now();
        const auto td32 = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count();

        std::cout << " --> td32 = " << td32 << std::endl;
    }
}