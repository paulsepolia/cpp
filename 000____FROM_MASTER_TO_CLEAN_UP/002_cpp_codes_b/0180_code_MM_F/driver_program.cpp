//================//
// memcpy example //
//================//

#include <iostream>
#include <vector>
#include <cmath>
#include <cstring>

using std::cout;
using std::endl;
using std::vector;
using std::pow;

// the main function

int main()
{
    // local parameters

    const unsigned int DIM(static_cast<unsigned int>(pow(10.0, 4.0)));

    vector<int> v1 {};
    vector<int> v2 {};

    cout << "------------------------------------------------->> 1" << endl;
    cout << " v1.size()     = " << v1.size() << endl;
    cout << " v2.size()     = " << v2.size() << endl;
    cout << " v1.capacity() = " << v1.capacity() << endl;
    cout << " v2.capacity() = " << v2.capacity() << endl;

    // build the vector

    for(unsigned int i = 0; i != DIM; i++) {
        v1.push_back(i);
    }

    cout << "------------------------------------------------->> 2" << endl;

    cout << " --> before" << endl;
    cout << " v1.size()     = " << v1.size() << endl;
    cout << " v2.size()     = " << v2.size() << endl;
    cout << " v1.capacity() = " << v1.capacity() << endl;
    cout << " v2.capacity() = " << v2.capacity() << endl;

    v2 = v1;

    cout << " --> after" << endl;
    cout << " v1.size()     = " << v1.size() << endl;
    cout << " v2.size()     = " << v2.size() << endl;
    cout << " v1.capacity() = " << v1.capacity() << endl;
    cout << " v2.capacity() = " << v2.capacity() << endl;

    v1 = vector<int>(); // reset the vector

    cout << "------------------------------------------------->> 4" << endl;

    cout << " v1.size()     = " << v1.size() << endl;
    cout << " v2.size()     = " << v2.size() << endl;
    cout << " v1.capacity() = " << v1.capacity() << endl;
    cout << " v2.capacity() = " << v2.capacity() << endl;

    return 0;
}

// end
