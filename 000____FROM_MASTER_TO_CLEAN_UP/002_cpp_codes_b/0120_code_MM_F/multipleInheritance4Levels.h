//===============================//
// MULTIPLE INHERITANCE 4 LEVELS //
//===============================//

#ifndef MULTIPLE_INHERITANCE_4LEVELS_H
#define MULTIPLE_INHERITANCE_4LEVELS_H

#include <iostream>

using std::endl;
using std::cout;

//=========//
// class A //
//=========//

class A {
public:

    // # 1 --> constructor

    A()
    {
        cout << " --> constructor --> A" << endl;
    }

    // # 2 --> copy constructor

    A(const A & other)
    {
        cout << " --> copy constructor --> A" << endl;
    }

    // # 3 --> fun

    virtual void fun()
    {
        cout << " --> fun --> A" << endl;
    }

    // # 4 --> destructor

    virtual ~A()
    {
        cout << " --> destructor --> ~A" << endl;
    }
};

//=========//
// class B //
//=========//

class B: public virtual A {
public:

    // # 1 --> constructor

    B()
    {
        cout << " --> constructor --> B" << endl;
    }

    // # 2 --> copy constructor

    B(const B & other)
    {
        cout << " --> copy constructor --> B" << endl;
    }

    // # 3 --> fun

    virtual void fun()
    {
        cout << " --> fun --> B" << endl;
    }

    // # 4 --> destructor

    virtual ~B()
    {
        cout << " --> destructor --> ~B" << endl;
    }
};

//=========//
// class C //
//=========//

class C : public virtual B {
public:

    // # 1 --> constructor

    C()
    {
        cout << " --> constructor --> C" << endl;
    }

    // # 2 --> copy constructor

    C(const C & other)
    {
        cout << " --> copy constructor --> C" << endl;
    }

    // # 3 --> fun

    void fun()
    {
        cout << " --> fun --> C" << endl;
    }

    // # 4 --> destructor

    virtual ~C()
    {
        cout << " --> destructor --> ~C" << endl;
    }
};

//=========//
// class D //
//=========//

class D : public virtual A {
public:

    // # 1 --> constructor

    D()
    {
        cout << " --> constructor --> D" << endl;
    }

    // # 2 --> copy constructor

    D(const D & other)
    {
        cout << " --> copy constructor --> D" << endl;
    }

    // # 3 --> fun

    void fun()
    {
        cout << " --> fun --> D" << endl;
    }

    // # 4 --> destructor

    virtual ~D()
    {
        cout << " --> destructor --> ~D" << endl;
    }
};

//=========//
// class E //
//=========//

class E : public virtual D {
public:

    // # 1 --> constructor

    E()
    {
        cout << " --> constructor --> E" << endl;
    }

    // # 2 --> copy constructor

    E(const E & other)
    {
        cout << " --> copy constructor --> E" << endl;
    }

    // # 3 --> fun

    void fun()
    {
        cout << " --> fun --> E" << endl;
    }

    // # 4 --> destructor

    virtual ~E()
    {
        cout << " --> destructor --> ~E" << endl;
    }
};

//=========//
// class F //
//=========//

class F : public C, public E {
public:

    // # 1 --> constructor

    F()
    {
        cout << " --> constructor --> F" << endl;
    }

    // # 2 --> copy constructor

    F(const F & other)
    {
        cout << " --> copy constructor --> F" << endl;
    }

    // # 3 --> fun

    void fun()
    {
        cout << " --> fun --> F" << endl;
    }

    // # 4 --> destructor

    virtual ~F()
    {
        cout << " --> destructor --> ~F" << endl;
    }
};

#endif // MULTIPLE_INHERITANCE_4LEVELS_H

// end
