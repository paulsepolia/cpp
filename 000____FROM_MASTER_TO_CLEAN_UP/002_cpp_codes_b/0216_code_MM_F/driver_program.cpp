//=========================//
// references and pointers //
//=========================//

#include <iostream>
#include <typeinfo>

using std::cout;
using std::endl;

// the main function

int main()
{
    // # 1

    int x1(10);
    int & rx1(x1);
    int x2(20);

    cout << " &x1  = " <<  &x1 << endl;
    cout << " &rx1 = " << &rx1 << endl;
    cout << "  x1  = " <<   x1 << endl;
    cout << " rx1  = " <<  rx1 << endl;

    x1 = x2;

    cout << "  x1  = " <<   x1 << endl;
    cout << "  x2  = " <<   x2 << endl;
    cout << " rx1  = " <<  rx1 << endl;

    cout << " &x2  = " <<  &x2 << endl;
    cout << " &x1  = " <<  &x1 << endl;

    rx1 = 30;

    cout << "  x1  = " <<   x1 << endl;
    cout << "  x2  = " <<   x2 << endl;
    cout << " rx1  = " <<  rx1 << endl;

    // # 2

    const int & rx2(rx1);

    cout << " &rx1 = " << &rx1 << endl;
    cout << " &rx2 = " << &rx2 << endl;
    cout << "  rx1 = " <<  rx1 << endl;
    cout << "  rx2 = " <<  rx2 << endl;

    rx1 = 40;

    cout << " &rx1 = " << &rx1 << endl;
    cout << " &rx2 = " << &rx2 << endl;
    cout << "  rx1 = " <<  rx1 << endl;
    cout << "  rx2 = " <<  rx2 << endl;

    cout << " typeid(rx1).name() = " << typeid(rx1).name() << endl;
    cout << " typeid( x1).name() = " << typeid(x1).name() << endl;
    cout << " typeid(rx2).name() = " << typeid(rx2).name() << endl;
    cout << " typeid( x2).name() = " << typeid(x2).name() << endl;
    cout << " typeid(int&).name() = " << typeid(int&).name() << endl;

    // # 3

    const int & rx3(100);

    cout << "  rx3 = " <<  rx3 << endl;
    cout << " &rx3 = " << &rx3 << endl;

    const int * p1(&rx3);
    int * p2(nullptr);

    p2 = const_cast<int*>(p1);
    *p2 = 200;

    cout << " *p1 = " << *p1 << endl;
    cout << " *p2 = " << *p1 << endl;
    cout << " rx3 = " << rx3 << endl;

    // # 4

    int * & px4(p2);

    cout << "  px4 = " <<  px4 << endl;
    cout << "   p2 = " <<  p2  << endl;
    cout << " *px4 = " << *px4 << endl;
    cout << "  *p2 = " << *p2  << endl;

    *p2 = 300;

    cout << " *px4 = " << *px4 << endl;
    cout << "  *p2 = " << *p2  << endl;


    return 0;
}

// end
