//================//
// move symantics //
//================//

#include <iostream>
#include <utility>
#include <cmath>
#include <vector>

using std::cout;
using std::endl;
using std::move;
using std::vector;

// type definitions

typedef unsigned long int uli;

// class with move symantics

class my_string {
public:

    // constructor

    explicit my_string(const vector<double> & other) : s(other)
    {
        cout << " --> 1 --> constructor" << endl;
    }

    // copy constructor

    my_string(const my_string & other)
    {
        cout << " --> 2 --> copy constructor" << endl;
        this->s = other.s;
    }

    // copy assignment operator

    const my_string & operator = (const my_string & other)
    {
        cout << " --> 3 --> copy assignment operator" << endl;
        this->s = other.s;
        return *this;
    }

    // move constructor

    my_string(const my_string && other) :
        s(move(other.s))
    {
        cout << " --> 4 --> move constructor" << endl;
    }

    // move assignment operator

    my_string & operator = (const my_string && other)
    {
        cout << " --> 5 --> move assignment operator" << endl;

        if (this != &other) {
            this->s = move(other.s);
        }
        return *this;
    }

    // set string

    void set(const vector<double> & s_loc)
    {
        this->s = s_loc;
    }

    // get string

    const vector<double> & get() const
    {
        return this->s;
    }

    // destructor

    virtual ~my_string()
    {
        cout << " --> 6 --> destructor" << endl;
    }

private:

    vector<double> s;
};

// the main function

int main()
{
    // local parameters

    const vector<double> SS = {1.2345};
    const vector<double> v1 = {1.1, 2.2};
    const vector<double> v2 = {2.2, 3.3};

    // local variables

    my_string S1(v1);
    my_string S2(v2);
    my_string S3(SS);

    cout << " --> &S1 = " << &S1 << endl;
    cout << " --> &S2 = " << &S2 << endl;
    cout << " --> &S3 = " << &S3 << endl;

    cout << " --> S1.get() = " << S1.get()[0] << endl;
    cout << " --> S2.get() = " << S2.get()[0] << endl;
    cout << " --> S3.get() = " << S3.get()[0] << endl;

    cout << " --> execute: S1 = S2" << endl;

    S1 = S2;

    cout << " --> &S1 = " << &S1 << endl;
    cout << " --> &S2 = " << &S2 << endl;
    cout << " --> S1.get() = " << S1.get()[0] << endl;
    cout << " --> S2.get() = " << S2.get()[0] << endl;

    cout << " --> execute: S1.set(v1)" << endl;

    S1.set(v1);

    cout << " --> execute: S2.set(v2)" << endl;

    S2.set(v2);

    cout << " --> execute: S1 = move(S2)" << endl;

    S1 = move(S2);

    cout << " --> S1.get() = " << S1.get()[0] << endl;
    cout << " --> S2.get() = " << S2.get()[0] << endl;

    cout << " --> execute: S1 = move(S3)" << endl;

    S1 = move(S3);

    cout << " --> S1.get()[0] = " << S1.get()[0] << endl;
    cout << " --> S2.get()[0] = " << S2.get()[0] << endl;
    cout << " --> S3.get()[0] = " << S3.get()[0] << endl;

    return 0;
}

// END
