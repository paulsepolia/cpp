//========//
// Vector //
//========//

#include <iostream>
#include <vector>
#include <cmath>
#include <random>
#include <iomanip>
#include <algorithm>
#include <functional>

using std::cout;
using std::endl;
using std::vector;
using std::pow;
using std::random_device;
using std::mt19937;
using std::uniform_real_distribution;
using std::fixed;
using std::setprecision;
using std::sort;
using std::multiplies;

// type definitions

typedef unsigned long int uli;

// a predicate implemented as a class

struct is_less_than_ten {
    const double TEN = 10.0;
    bool operator() (const double & value)
    {
        return (value <= TEN);
    }
};

// class: List

template <typename T>
class Vector : public vector<T> {
};

// the main function

int main()
{
    // local parameters

    const uli DIM1 = 3 * uli(pow(10.0, 6.0));
    const uli TRIALS1 = 10;

    // local parameters

    Vector<double> L1;

    // fill with random numbers

    cout << fixed;
    cout << setprecision(5);

    for (uli ik = 0; ik != TRIALS1; ik++) {

        cout << "----------------------------------------------------------->> " << ik << endl;

        cout << " --> declare random device" << endl;

        random_device rd;
        mt19937 gen(rd());
        uniform_real_distribution<> dis(0, 11);

        cout << " --> build vector" << endl;

        for (uli i = 0; i < DIM1; i++) {
            L1.push_back(dis(gen));
        }

        cout << " --> L1.size() = " << L1.size() << endl;

        cout << " --> sort the vector" << endl;

        sort(L1.begin(), L1.end());

        cout << " --> sum the elements" << endl;

        double sum = accumulate(L1.begin(), L1.end(), 0.0);

        cout << " sum/DIM = " << sum/DIM1 << endl;

        double product = accumulate(L1.begin(), L1.end(), 1.0, multiplies<double>());

        cout << " product/DIM = " << product/DIM1 << endl;

        cout << " --> clear vector" << endl;

        L1.clear();

        cout << " --> reset the internal state of the random generator" << endl;

        dis.reset();
    }

    return 0;
}

// end
