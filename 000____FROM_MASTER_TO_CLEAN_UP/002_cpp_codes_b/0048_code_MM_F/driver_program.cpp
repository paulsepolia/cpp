//======//
// list //
//======//

#include <iostream>
#include <list>
#include <cmath>
#include <ctime>
#include <iomanip>

using std::endl;
using std::cout;
using std::cin;
using std::list;
using std::pow;
using std::clock;
using std::fixed;
using std::setprecision;

// type definitions

typedef unsigned long int uli;

// the main function

int main ()
{
    // local parameters

    uli DIM1 = 3 * uli(pow(10.0, 7.0));
    uli DIM2 = uli(pow(10.0, 2.0));

    // adjust the output

    cout << fixed;
    cout << setprecision(10);

    // local variables

    time_t t1;
    time_t t2;
    list<double> L1;
    list<double> L2;
    list<double> L3;
    list<double>::iterator it1;
    list<double>::iterator it2;

    // build list L1

    cout << " --> L1.assign(DIM1, 123.4);" << endl;

    t1 = clock();

    for (uli i = 0; i != DIM2; i++) {
        L1.assign(DIM1, 123.4);
    }

    t2 = clock();
    cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

    // build list L2

    cout << " --> L2.assign(L1.begin(), L1.end());" << endl;

    t1 = clock();

    for (uli i = 0; i != DIM2; i++) {
        L2.assign(L1.begin(), L1.end());
    }

    t2 = clock();
    cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

    // build list L3

    cout << " --> L3.assign(L1.begin(), L1.end());" << endl;

    t1 = clock();

    for (uli i = 0; i != DIM2; i++) {
        L3.assign(L2.begin(), L2.end());
    }

    t2 = clock();
    cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

    // build using an AR1

    cout << " --> build the array DIM2 times" << endl;

    double * AR1 = new double [DIM1];

    t1 = clock();

    for (uli j = 0; j != DIM2; j++) {
        for (uli i = 0; i != DIM1; i++) {
            AR1[i] = 123.4;
        }
    }

    t2 = clock();
    cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

    cout << " --> build the list using the array" << endl;

    t1 = clock();

    for (uli i = 0; i != DIM2; i++) {
        L1.assign(AR1, AR1+DIM1);
    }

    t2 = clock();
    cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

    cout << " --> size of L1 = " << uli(L1.size()) << endl;
    cout << " --> size of L2 = " << uli(L2.size()) << endl;

    cout << " --> erase L1" << endl;

    t1 = clock();

    it1 = L1.begin();
    it2 = L1.end();

    L1.erase(it1, it2);

    t2 = clock();
    cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

    cout << " --> erase L2" << endl;

    t1 = clock();

    it1 = L2.begin();
    it2 = L2.end();

    L2.erase(it1, it2);

    t2 = clock();
    cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

    cout << " --> erase L3" << endl;

    t1 = clock();

    it1 = L3.begin();
    it2 = L3.end();

    L3.erase(it1, it2);

    t2 = clock();
    cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

    cout << " --> size of L1 = " << uli(L1.size()) << endl;
    cout << " --> size of L2 = " << uli(L2.size()) << endl;
    cout << " --> size of L3 = " << uli(L3.size()) << endl;

    return 0;
}

// end
