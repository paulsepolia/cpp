//==================//
// add two integers //
//==================//

#include <iostream>
using std::endl;
using std::cout;

typedef unsigned long int uli;

// the main function

int main()
{
    int x(0);

    // # 1

    cout << "------------------------------------>> 1" << endl;
    x = 1;
    cout << " x = " << x << endl;
    cout << " x = x << 1;" << endl;
    for(int i = 0 ; i < 10; i++) {
        x = x << 1;
        cout << x << endl;
    }

    // # 2

    cout << "------------------------------------>> 2" << endl;
    x = 2;
    cout << " x = " << x << endl;
    cout << " x = x << 1;" << endl;
    for(int i = 0 ; i < 10; i++) {
        x = x << 1;
        cout << x << endl;
    }

    // # 3

    cout << "------------------------------------>> 3" << endl;
    x = 3;
    cout << " x = " << x << endl;
    cout << " x = x << 1;" << endl;
    for(int i = 0 ; i < 10; i++) {
        x = x << 1;
        cout << x << endl;
    }

    // # 4

    cout << "------------------------------------>> 4" << endl;
    x = 1;
    cout << " x = " << x << endl;
    cout << " x = x << 2;" << endl;
    for(int i = 0 ; i < 10; i++) {
        x = x << 2;
        cout << x << endl;
    }

    // # 5

    cout << "------------------------------------>> 5" << endl;
    x = 1;
    cout << " x = " << x << endl;
    cout << " x = x << 3;" << endl;
    for(int i = 0 ; i < 10; i++) {
        x = x << 3;
        cout << x << endl;
    }

    return 0;
}

// end
