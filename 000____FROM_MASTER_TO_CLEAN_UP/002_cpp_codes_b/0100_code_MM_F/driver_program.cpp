//=======================//
// operators overloading //
//=======================//

#include <iostream>
#include <utility>

using std::endl;
using std::cout;
using std::move;

// class A

class A {
public:

    // # 1 --> constructor

    A() : m_val1(-1)
    {
        cout << " --> constructor --> A --> m_val1 = " << m_val1 << endl;
    }

    // # 2 --> destructor

    virtual ~A()
    {
        cout << " --> destructor --> A --> m_val1 = " << m_val1 << endl;
    }

private:

    int m_val1;
};

// class B

class B: public A {

public:

    // # 1 --> constructor

    B() : A(), m_val2(-2)
    {
        cout << " --> constructor --> B --> m_val2 = " << m_val2 << endl;
    }

    // # 2 --> destructor

    virtual ~B()
    {
        cout << " --> destructor --> B --> m_val2 = " << m_val2 << endl;
    }

private:

    int m_val2;
};

// class C

class C: public B {

public:

    // # 1 --> constructor

    C() : B(), m_val3(-3)
    {
        cout << " --> constructor --> C --> m_val3 = " << m_val3 << endl;
    }

    // # 2 --> destructor

    virtual ~C()
    {
        cout << " --> destructor --> C --> m_val3 = " << m_val3 << endl;
    }

private:

    int m_val3;
};

// the main function

int main()
{
    //==============================================//
    // about calling of constructors and destructor //
    // detect the order of calling them             //
    //==============================================//

    // CALL:
    // # 1 --> constructor
    // # 2 --> destructor

    {
        cout << "  0 --> start ----------------------------------------------> 1" << endl;
        cout << "  1 --> A a1;" << endl;
        A a1;
        cout << "  2 --> B b1;" << endl;
        B b1;
        cout << "  3 --> C c1;" << endl;
        C c1;
        cout << "  4 --> exit -----------------------------------------------> 1" << endl;
    }

    cout << " --> MIDDLE --> 1" << endl;

    // CALL:
    // # 1 --> constructor
    // # 2 --> destructor
    // # 3 --> default copy constructor

    {
        cout << "  0 --> start ----------------------------------------------> 2" << endl;
        cout << "  1 --> A a1;" << endl;
        A a1;
        cout << "  2 --> A a2(a1);" << endl;
        A a2(a1);
        cout << "  3 --> B b1;" << endl;
        B b1;
        cout << "  4 --> B b2(b1);" << endl;
        B b2(b1);
        cout << "  5 --> C c1;" << endl;
        C c1;
        cout << "  6 --> C c2(c1);" << endl;
        C c2(c1);
        cout << "  7 --> exit -----------------------------------------------> 2" << endl;
    }

    cout << " --> MIDDLE --> 2" << endl;

    // CALL:
    // # 1 --> constructor
    // # 2 --> destructor
    // # 3 --> default copy constructor

    {
        cout << "  0 --> start ----------------------------------------------> 3" << endl;
        cout << "  1 --> C c1;" << endl;
        C c1;
        cout << "  2 --> B b1(c1);" << endl;
        B b1(c1);
        cout << "  3 --> A a1(b1);" << endl;
        A a1(b1);
        cout << "  4 --> A a2(c1);" << endl;
        A a2(c1);
        cout << "  5 --> exit -----------------------------------------------> 3" << endl;
    }

    cout << " --> MIDDLE --> 3" << endl;

    // CALL:
    // # 1 --> constructor
    // # 2 --> destructor
    // # 3 --> move

    {
        cout << "  0 --> start ----------------------------------------------> 4" << endl;
        cout << "  1 --> C c1;" << endl;
        C c1;
        cout << "  2 --> C c2;" << endl;
        C c2;
        cout << "  3 --> c1 = move(c2);" << endl;
        c1 = move(c2);
        cout << "  4 --> A a1;" << endl;
        A a1;
        cout << "  5 --> a1 = move(c1);" << endl;
        a1 = move(c1);
    }

    cout << " --> MIDDLE --> 4" << endl;

    // CALL:
    // # 1 --> constructor
    // # 2 --> destructor
    // # 3 --> default move constructor

    {
        cout << "  0 --> start ----------------------------------------------> 4" << endl;
        cout << "  1 --> C c1;" << endl;
        C c1;
        cout << "  2 --> C c2(move(c1));" << endl;
        C c2(move(c1));
        cout << "  3 --> A a1(move(c2));" << endl;
        A a1(move(c2));
    }

    cout << " --> MIDDLE --> 5" << endl;

    return 0;
}

// end
