//=====================================//
// overloaded new and delete operators //
// garbage collector 			    //
//=====================================//

#include <iostream>
#include <cmath>
#include <list>
#include <memory>

using std::cout;
using std::endl;
using std::pow;
using std::list;
using std::free;
using std::shared_ptr;

// type definitions

typedef unsigned long int uli;
typedef const uli culi;

// static variables

static uli indexAllocate(0);
static uli indexDelete(0);

// overload the new operator

void* operator new(size_t sz) throw(std::bad_alloc)
{
    indexAllocate++;
    cout << " --> inside new overloaded operator, indexAllocate = " << indexAllocate << endl;
    return malloc(sz);
}

// overload the delete operator

void operator delete(void* ptr) noexcept
{
    indexDelete++;
    cout << " --> overloaded delete, indexDelete = " << indexDelete << endl;
    free(ptr);
    ptr = 0;

    if(indexDelete == indexAllocate) {
        cout << " --> NO MEMORY LEAKS UP TO NOW" << endl;
    }
}

// deleter

template<typename T>
class my_deleter {
public:
    void operator()(T * ptr)
    {
        delete [] ptr;
        ptr = 0;
    }
};

// static vector

static list<shared_ptr<double>> lsp {};

// the main function

int main()
{
    culi	DIMEN(1*static_cast<uli>(pow(10.0, 1.0)));

    cout << "----------------------------------------------------------------->>  1" << endl;

    lsp.resize(0);

    cout << "----------------------------------------------------------------->>  2" << endl;

    for(uli i = 0; i != DIMEN; i++) {
        cout << "------------------------------------------------------------>>  3, " << i << endl;
        double * p = new double(i);
        shared_ptr<double> sp(p, my_deleter<double>());
        lsp.push_back(sp);
    }

    cout << "----------------------------------------------------------------->>  4" << endl;

    lsp.clear();

    cout << "----------------------------------------------------------------->>  5" << endl;

    if(indexAllocate != indexDelete) {
        cout << " --> indexAllocate = " << indexAllocate << endl;
        cout << " --> indexDelete   = " << indexDelete << endl;
        cout << " --> WARNING: Possible memory leak" << endl;
    }

    cout << "----------------------------------------------------------------->>  END" << endl;

    return 0;
}

// end
