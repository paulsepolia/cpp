//===================//
// function pointers //
//===================//

#include <iostream>

using std::cout;
using std::cin;
using std::endl;

// the main function

int main()
{
    int i1       = 11;
    int i2       = 12;
    int i3       = 13;
    const int i4 = 14;
    const int * p1(nullptr);
    const int * p2(nullptr);
    int       * p3(nullptr);
    const int * p4(nullptr);

    // assing a new address to the constant pointer

    p1 = &i1;
    p2 = &i2;
    p3 = &i3;
    p4 = &i4;

    cout << " --> i1 = " << i1 << endl;
    cout << " --> i2 = " << i2 << endl;
    cout << " --> i3 = " << i3 << endl;
    cout << " --> i4 = " << i4 << endl;
    cout << " --> p1 = " << p1 << endl;
    cout << " --> p2 = " << p2 << endl;
    cout << " --> p3 = " << p3 << endl;
    cout << " --> p4 = " << p4 << endl;

    // pointers are constants means that
    // we cannot alter the value they pointing to using them

    //*p1 = 21; // ERROR: expression *p1 is not a modifiable lvalue
    //*p2 = 22; // ERROR: expression *p2 is not a modifiable lvalue

    *p3 = 23;

    cout << " -->  i3 = " <<  i3 << endl;
    cout << " -->  p3 = " <<  p3 << endl;
    cout << " --> *p3 = " << *p3 << endl;

    return 0;
}
