//====================//
// single linked list //
//====================//

#include <iostream>
using std::endl;
using std::cout;

// singh linked list definition

struct node {
    int x;
    node *next;
};

// the main function

int main()
{
    node * root;      // This will be the unchanging first node
    root = new node;  // Now root points to a node struct
    root->next = 0;   // The node root points to has its next pointer
    // set equal to a null pointer
    root->x = 5;      // By using the -> operator, you can modify the node
    //  a pointer (root in this case) points to.

    cout << " &root       = " << &root << endl;
    cout << " &root->next = " << &root->next << endl;
    cout << " &root->x    = " << &root->x << endl;
    cout << "  root->next = " << root->next << endl;
    cout << "  root->x    = " << root->x << endl;

    // delete the pointer

    delete root;
    root = 0;
    if(root) {
        delete root;
    }

    return 0;
}

// end
