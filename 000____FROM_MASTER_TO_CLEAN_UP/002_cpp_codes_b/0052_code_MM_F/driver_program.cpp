//======//
// list //
//======//

#include <iostream>
#include <list>

using std::endl;
using std::cout;
using std::list;

// a predicate implemented as a function:

bool single_digit(const int & value)
{
    return (value < 10);
}

// a predicate implemented as a class:

struct is_odd {
    bool operator() (const int & value)
    {
        return ((value%2) == 1);
    }
};

// the main function

int main ()
{
    int myints[] = {15,36,7,17,20,39,4,1};

    list<int> mylist(myints,myints+8);   // 15 36 7 17 20 39 4 1

    mylist.remove_if(single_digit);     // 15 36 17 20 39

    mylist.remove_if(is_odd());         // 36 20

    cout << "mylist contains:";

    for (auto it = mylist.begin(); it != mylist.end(); ++it) {
        cout << ' ' << *it;
    }

    cout << endl;

    return 0;
}

// end
