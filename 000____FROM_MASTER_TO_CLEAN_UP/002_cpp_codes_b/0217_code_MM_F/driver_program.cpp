//=====================//
// upcast and downcast //
//=====================//

#include <iostream>
#include <typeinfo>

using std::cout;
using std::endl;

// class --> A

class A {
public:

    A(): p1(new int(-1)), x1(*p1), x2(*p1) {}

    int * p1;
    int & x1;
    int & x2;

    virtual ~A()
    {
        cout << " ~A() --> 1" << endl;
        if(p1) {
            cout << " ~A() --> 2" << endl;
            delete p1;
            p1 = 0;
        }
    }
};

// the main function

int main()
{
    // # 1

    cout << " --> A a1; --> start" << endl;

    A a1;

    cout << " --> A a1; --> end" << endl;

    cout << "  a1.p1 = " <<  a1.p1 << endl;
    cout << " *a1.p1 = " << *a1.p1 << endl;
    cout << " &a1.p1 = " << &a1.p1 << endl;

    cout << "  a1.x1 = " <<  a1.x1 << endl;
    cout << "  a1.x2 = " <<  a1.x2 << endl;

    cout << "  a1.x1 = 100;" << endl;

    a1.x1 = 100;

    cout << " *a1.p1 = " << *a1.p1 << endl;
    cout << "  a1.x1 = " << a1.x1 << endl;
    cout << "  a1.x2 = " << a1.x2 << endl;

    cout << "  a1.x1 = 200;" << endl;

    a1.x2 = 200;

    cout << " *a1.p1 = " << *a1.p1 << endl;
    cout << "  a1.x1 = " << a1.x1 << endl;
    cout << "  a1.x2 = " << a1.x2 << endl;

    // # 2

    delete a1.p1;
    a1.p1 = 0;

    // # 3

    a1.p1 = new int(111);

    cout << " --> fini" << endl;

    return 0;
}

// end
