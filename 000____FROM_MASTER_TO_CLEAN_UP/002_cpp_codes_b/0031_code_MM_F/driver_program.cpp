//================//
// move symantics //
//================//

#include <iostream>
#include <utility>
#include <cmath>
#include <string>

using std::cout;
using std::endl;
using std::move;
using std::pow;
using std::string;

// type definitions

typedef unsigned long int uli;

// class with move symantics

class my_string {
public:

    // constructor

    explicit my_string(const string & s_loc) : s(s_loc)
    {
        cout << " --> 1 --> constructor" << endl;
    }

    // copy constructor

    my_string(const my_string & other)
    {
        cout << " --> 2 --> copy constructor" << endl;
        this->s = other.s;
    }

    // copy assignment operator

    const my_string & operator = (const my_string & other)
    {
        cout << " --> 3 --> copy assignment operator" << endl;
        this->s = other.s;
        return *this;
    }

    // move constructor

    my_string(my_string && other) :
        s(move(other.s))
    {
        cout << " --> 4 --> move constructor" << endl;
    }

    // move assignment operator

    my_string & operator = (my_string && other)
    {
        cout << " --> 5 --> move assignment operator" << endl;

        if (this != &other) {
            this->s = move(other.s);
        }
        return *this;
    }

    // set string

    void set(const string & s_loc)
    {
        this->s = s_loc;
    }

    // get string

    const string & get() const
    {
        return this->s;
    }

    // destructor

    virtual ~my_string()
    {
        cout << " --> 6 --> destructor" << endl;
    }

private:

    string s;
};

// the main function

int main()
{
    // local parameters

    const string SS = "test string";

    // local variables

    my_string S1("123456");
    my_string S2("ABCDEF");
    my_string S3(SS);

    cout << " --> &S1 = " << &S1 << endl;
    cout << " --> &S2 = " << &S2 << endl;
    cout << " --> &S3 = " << &S3 << endl;

    cout << " --> S1.get() = " << S1.get() << endl;
    cout << " --> S1.get() = " << S1.get() << endl;
    cout << " --> S2.get() = " << S2.get() << endl;
    cout << " --> S3.get() = " << S3.get() << endl;

    cout << " --> execute: S1 = S2" << endl;

    S1 = S2;

    cout << " --> &S1 = " << &S1 << endl;
    cout << " --> &S2 = " << &S2 << endl;
    cout << " --> S1.get() = " << S1.get() << endl;
    cout << " --> S2.get() = " << S2.get() << endl;

    cout << " --> execute: S1.set(\"KKKKK1\")" << endl;

    S1.set("KKKKK1");

    cout << " --> execute: S2.set(\"KKKKK2\")" << endl;

    S2.set("KKKKK2");

    cout << " --> execute: S1 = move(S2)" << endl;

    S1 = move(S2);

    cout << " --> S1.get() = " << S1.get() << endl;
    cout << " --> S2.get() = " << S2.get() << endl;

    cout << " --> execute: S1 = move(S3)" << endl;

    S1 = move(S3);

    cout << " --> S1.get() = " << S1.get() << endl;
    cout << " --> S2.get() = " << S2.get() << endl;
    cout << " --> S3.get() = " << S3.get() << endl;

    return 0;
}

// END
