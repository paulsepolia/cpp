//========================//
// NONPOLYMORPHIC CLASSES //
//========================//

#ifndef CLASSES_NON_POLYMORPHIC_H
#define CLASSES_NON_POLYMORPHIC_H

#include <iostream>

using std::endl;
using std::cout;

//=========//
// class A //
//=========//

class A {
public:

    // # 1 --> constructor

    A()
    {
        cout << " --> constructor --> A" << endl;
    }

    // # 2 --> copy constructor

    A(const A & other)
    {
        cout << " --> copy constructor --> A" << endl;
    }

    // # 3 --> fun

    void fun()
    {
        cout << " --> fun --> A" << endl;
    }

    // # 4 --> destructor

    virtual ~A()
    {
        cout << " --> destructor --> ~A" << endl;
    }
};

//=========//
// class B //
//=========//

class B : virtual public A {
public:

    // # 1 --> constructor

    B()
    {
        cout << " --> constructor --> B" << endl;
    }

    // # 2 --> copy constructor

    B(const B & other)
    {
        cout << " --> copy constructor --> B" << endl;
    }

    // # 3 --> fun

    void fun()
    {
        cout << " --> fun --> B" << endl;
    }

    // # 4 --> destructor

    virtual ~B()
    {
        cout << " --> destructor --> ~B" << endl;
    }
};

//=========//
// class C //
//=========//

class C : virtual public B {
public:

    // # 1 --> constructor

    C()
    {
        cout << " --> constructor --> C" << endl;
    }

    // # 2 --> copy constructor

    C(const C & other)
    {
        cout << " --> copy constructor --> C" << endl;
    }

    // # 3 --> fun

    void fun()
    {
        cout << " --> fun --> C" << endl;
    }

    // # 4 --> destructor

    virtual ~C()
    {
        cout << " --> destructor --> ~C" << endl;
    }
};

//=========//
// class D //
//=========//

class D : virtual public C {
public:

    // # 1 --> constructor

    D()
    {
        cout << " --> constructor --> D" << endl;
    }

    // # 2 --> copy constructor

    D(const D & other)
    {
        cout << " --> copy constructor --> D" << endl;
    }

    // # 3 --> fun

    void fun()
    {
        cout << " --> fun --> D" << endl;
    }

    // # 4 --> destructor

    virtual ~D()
    {
        cout << " --> destructor --> ~D" << endl;
    }
};

#endif // CLASSES_NON_POLYMORPHIC_H

// end
