//=============//
// linked list //
//=============//

#include <cmath>
#include <iostream>
using std::endl;
using std::cout;

#define DEBUG
//#undef DEBUG

// list definition

class list {
public:
    int * data1;
    list * next;
};

typedef unsigned long int uli;

// the main function

int main()
{
    const uli DIM(std::pow(10.0, 2.0));
    const uli DIM2(std::pow(10.0, 3.0));
    const uli TR(std::pow(10.0, 5.0));

    for(int k = 0; k != TR; k++) {

        // # 1 create nodes

        list * head(new list);
        list * node(new list);

        // initialize the head

        head->next = node;

        // build the nodes

        for(uli i = 0; i != DIM; i++) {
            node->data1 = new int [DIM2];
            node->next = new list;
            node = node->next;
        }

        // finalize the nodes

        node->next = 0;
        node->data1 = new int [DIM2];

        // delete the nodes
        node = head;
        while(node->next) {
            list * q;
            q = node->next;
            node->next = node->next->next;
            delete [] q->data1;
            delete q;
        }

        // delete the head
        delete head;
    }

    return 0;
}

// END
