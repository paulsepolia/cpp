
//==================//
// move constructor //
//==================//

#include <string>
#include <iostream>
#include <iomanip>

using std::string;
using std::cout;
using std::endl;

using std::move;
using std::quoted;

// struct --> A

struct A {

    string s;

    A() : s("test") {}

    A(const A& o) : s(o.s)
    {
        cout << "move failed!" << endl;
    }

    A(A&& o) noexcept :
        s(move(o.s)) {}
};

// function A

A f(A a)
{
    return a;
}

// function B

struct B : A {
    string s2;
    int n;
    // implicit move contructor B::(B&&)
    // calls A's move constructor
    // calls s2's move constructor
    // and makes a bitwise copy of n
};

// function B

struct C : B {
    ~C() {} // destructor prevents implicit move ctor C::(C&&)
};

// function D

struct D : B {
    D() {}
    ~D() {} // destructor would prevent implicit move ctor D::(D&&)
    D(D&&) = default; // force a move ctor anyway
};

// the main function

int main()
{
    cout << "Trying to move A" << endl;

    A a1 = f(A()); // move-construct from rvalue temporary

    A a2 = move(a1); // move-construct from xvalue

    cout << "Trying to move B" << endl;

    B b1;

    cout << "Before move, b1.s = " << quoted(b1.s) << endl;

    B b2 = move(b1); // calls implicit move ctor

    cout << "After move, b1.s = " << quoted(b1.s) << endl;

    cout << "Trying to move C" << endl;

    C c1;

    C c2 = move(c1); // calls the copy constructor

    cout << "Trying to move D" << endl;

    D d1;

    D d2 = move(d1);

    return 0;
}

// END
