//==========================//
// set_symmetric_difference //
//==========================//

#include <iostream>
#include <algorithm>
#include <vector>

using std::cout;
using std:: endl;
using std::set_symmetric_difference;
using std::sort;
using std::vector;

// the main function

int main()
{
    int first[] = {5,10,15,20,25};
    int second[] = {50,40,30,20,10};

    vector<int> v(10);                      // 0  0  0  0  0  0  0  0  0  0
    vector<int>::iterator it;

    sort(first,first+5);     //  5 10 15 20 25
    sort(second,second+5);   // 10 20 30 40 50

    it = set_symmetric_difference (first, first+5, second, second+5, v.begin());

    //  5 15 25 30 40 50  0  0  0  0

    v.resize(it-v.begin());                      //  5 15 25 30 40 50

    cout << " --> The symmetric difference has " << (v.size()) << " elements: " << endl;;

    for (it = v.begin(); it != v.end(); ++it) {
        cout << ' ' << *it;
    }

    cout << endl;;

    return 0;
}

// end
