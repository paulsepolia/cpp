//==============//
// move example //
//==============//

#include <utility>   // std::move
#include <iostream>  // std::cout
#include <vector>    // std::vector
#include <string>    // std::string

using std::endl;
using std::cout;
using std::vector;
using std::string;
using std::move;

// function --> 1

string get_name()
{
    cout << " --> 1f" << endl;
    return "Alex";
}

// function --> 2

string get_name(string & str)
{
    cout << " --> 2f" << endl;
    return str;
}

// function --> 3

const string & get_name(string && str)
{
    cout << " --> 3f" << endl;
    return str;
}

// function --> 4

void print_ref(const string & str)
{
    cout << " --> 1 --> const reference --> " << str << endl;
}

// function --> 5

void print_ref(string && str)
{
    cout << " --> 2 --> rvalue reference --> " << str << endl;;
}

// the main function

int main()
{
    const string & name1 = get_name(); // ok

    cout << " --> name1 = " << name1 << endl;

    //string & name2 = getName(); // NOT ok

    const string && name3 = get_name(); // ok

    cout << " --> name3 = " << name3 << endl;

    string && name4 = get_name(); // also ok

    cout << " --> name4 = " << name4 << endl;

    cout << " --> uses the const reference argument function" << endl;

    print_ref(name1); // calls the first print_ref function, taking an lvalue reference
    print_ref(name3); // calls the first print_ref function, taking an lvalue reference
    print_ref(name4); // calls the first print_ref function, taking an lvalue reference

    cout << " --> uses the rvalue reference argument function" << endl;

    print_ref(get_name()); // calls the second print_ref function, taking a mutable rvalue reference

    print_ref(get_name(name4)); // calls the second print_ref function, taking a mutable rvalue reference

    string name5("10");

    print_ref(get_name(name5)); //

    print_ref(get_name(move(name5))); //

    cout << " name5 = " << name5 << endl;

    return 0;
}

// END
