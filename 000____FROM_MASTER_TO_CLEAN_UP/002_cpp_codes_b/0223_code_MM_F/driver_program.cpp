//=================//
// dynamic binding //
//=================//

#include <iostream>
#include <typeinfo>

using std::cout;
using std::endl;

// class --> Base

class Base {
public:
    virtual void DoOperation()
    {
        cout << "Base class here" << endl;
    }
};

// class --> Derived

class Derived: public Base {
public:
    virtual void DoOperation()
    {
        cout << "Derived class here" << endl;
    }
};

// the main function

int main()
{
    // # 1

    Base * pB1 = new Derived();
    pB1->DoOperation(); //dynamic binding, uses virtual table lookup at run time..
    cout << typeid(pB1).name() << endl;

    // # 2

    Base * pB2(new Base());
    pB2->DoOperation();
    cout << typeid(pB2).name() << endl;

    // # 3

    Derived * pD1(new Derived());
    pD1->DoOperation();
    cout << typeid(pD1).name() << endl;

    return 0;
}

// end
