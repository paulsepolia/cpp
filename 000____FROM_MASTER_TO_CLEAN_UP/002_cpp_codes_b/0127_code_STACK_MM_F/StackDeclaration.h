//=========================//
// Stack class declaration //
//=========================//

#ifndef STACK_DECLARATION_H
#define STACK_DECLARATION_H

#include <stack>
using std::stack;

// declaration

template <class T>
class Stack {
public:

    // constructor

    Stack();

    // copy constructor

    Stack(const Stack<T> &);

    // move constructor

    Stack(Stack<T> &&);

    // destructor

    virtual ~Stack();

    // member functions

    inline bool Empty() const;        // returns whether the stack is empty
    inline unsigned int Size() const; // get the size of the stack
    inline T Top() const;             // get back the value of the top item
    inline void Push(const T &);      // adds item to the top

    template <class ... Args>
    inline void Emplace(Args && ... args);  // create and push

    inline void Pop();                       // delete item from the top
    inline void Swap(Stack<T> &) noexcept;   // swap contents

    // copy assignment operator

    inline void operator = (const Stack<T> &);

    // move assignment operator

    inline void operator = (Stack<T> &&);

private:

    stack<T> m_data; // the actual data array
};

#endif // STACK_DECLARATION_H
