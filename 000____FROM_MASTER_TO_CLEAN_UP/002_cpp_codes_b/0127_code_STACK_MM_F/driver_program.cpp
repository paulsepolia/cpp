//===============================//
// driver program to Stack class //
//===============================//

#include "Stack.h"
#include <iostream>
#include <iomanip>

using std::cout;
using std::cin;
using std::endl;
using std::fixed;
using std::setprecision;
using std::boolalpha;

// the main function

int main()
{
    Stack<int> s1;

    // # 0

    cout << fixed;
    cout << setprecision(5);
    cout << boolalpha;

    // # 1

    cout << " --> push some integers" << endl;

    s1.Push(10);
    s1.Push(11);
    s1.Push(12);
    s1.Push(13);
    s1.Push(14);

    // # 2

    cout << " --> get the top element" << endl;

    cout << " --> s1.Top() = " << s1.Top() << endl;
    cout << " --> s1.Top() = " << s1.Top() << endl;
    cout << " --> s1.Top() = " << s1.Top() << endl;

    // # 3

    cout << " --> pop some elements" << endl;

    cout << " --> s1.Pop();" << endl;
    s1.Pop();
    cout << " --> s1.Pop();" << endl;
    s1.Pop();
    cout << " --> s1.Pop();" << endl;
    s1.Pop();

    // # 4

    cout << " --> get the top element" << endl;

    cout << " --> s1.Top() = " << s1.Top() << endl;
    cout << " --> s1.Top() = " << s1.Top() << endl;
    cout << " --> s1.Top() = " << s1.Top() << endl;

    // # 5

    cout << " --> emplace some elements" << endl;

    s1.Emplace(20);
    s1.Emplace(21);
    s1.Emplace(22);
    s1.Emplace(23);
    s1.Emplace(24);

    // # 6

    cout << " --> get the top element" << endl;

    cout << " --> s1.Top() = " << s1.Top() << endl;
    cout << " --> s1.Top() = " << s1.Top() << endl;
    cout << " --> s1.Top() = " << s1.Top() << endl;

    // # 7

    cout << " --> Stack<int> s2(s1);" << endl;

    Stack<int> s2(s1);

    // # 8

    cout << " -> s1.Size() = " << s1.Size() << endl;
    cout << " -> s2.Size() = " << s2.Size() << endl;

    // # 9

    cout << " --> get the top element" << endl;

    cout << " --> s1.Top() = " << s1.Top() << endl;
    cout << " --> s2.Top() = " << s2.Top() << endl;

    // # 10

    cout << " --> s1.Empty() = " << s1.Empty() << endl;
    cout << " --> s2.Empty() = " << s2.Empty() << endl;

    // # 11

    cout << " --> pop s1 until is empty" << endl;

    while(!s1.Empty()) {
        s1.Pop();
    }

    // # 12

    cout << " --> pop s2 until is empty" << endl;

    while(!s2.Empty()) {
        s2.Pop();
    }

    // # 13

    cout << " --> s1.Empty() = " << s1.Empty() << endl;
    cout << " --> s2.Empty() = " << s2.Empty() << endl;

    // # 14

    s1.Push(100);
    s1.Push(200);
    s2.Emplace(300);

    s1.Swap(s2);

    cout << " --> s1.Top() = " << s1.Top() << endl;
    cout << " --> s2.Top() = " << s2.Top() << endl;

    return 0;
}

// end
