//==================//
// reinterpret_cast //
//==================//

#include <cassert>
#include <iostream>

using std::cout;
using std::endl;

// function

int f()
{
    return 42;
}

//===================//
// the main function //
//===================//

int main()
{
    int i = 7;

    //=============================//
    // pointer to integer and back //
    //=============================//

    // assign the address of an int object to pointers of various types

    unsigned long int* ip1 = reinterpret_cast<unsigned long int*>(&i);
    unsigned int* ip2 = reinterpret_cast<unsigned int*>(&i);
    double *ip3 = reinterpret_cast<double*>(&i); // it is an error

    // get the address - must be the same

    cout << " --> &i  = " << &i << endl;
    cout << " --> ip1 = " << ip1 << endl;
    cout << " --> ip2 = " << ip2 << endl;
    cout << " --> ip3 = " << ip3 << endl;

    // get the value

    cout << " --> i    = " << i << endl;
    cout << " --> *ip1 = " << *ip1 << endl;
    cout << " --> *ip2 = " << *ip2 << endl;
    cout << " --> *ip3 = " << *ip3 << endl;

    // pointer to function to another and back

    void(*fp1)() = reinterpret_cast<void(*)()>(f);

    //fp1(); // undefined behavior

    int(*fp2)() = reinterpret_cast<int(*)()>(fp1);

    cout << " --> fp2() = " << fp2() << endl;

    // type aliasing through pointer

    char* p2 = reinterpret_cast<char*>(&i);

    if(p2[0] == '\x7') {
        cout << "This system is little-endian" << endl;
    } else {
        cout << "This system is big-endian" << endl;
    }

    // type aliasing through reference

    reinterpret_cast<int&>(i) = 41;
    cout << " --> i = " << i << endl;

    reinterpret_cast<unsigned int&>(i) = 42;
    cout << " --> i = " << i << endl;

    reinterpret_cast<double&>(i) = 43.1234;
    cout << " --> i = " << i << endl;

    reinterpret_cast<unsigned long int&>(i) = 44;
    cout << " --> i = " << i << endl;

    return 0;
}
