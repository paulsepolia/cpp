//==============//
// late binding //
//==============//

#include <iostream>

using std::endl;
using std::cout;

// class --> B1

class B1 {
public:

    // constructor

    B1(): x1(1), x2(2), x3(3) {}

    int x1;
    int x2;
    int x3;
};

// class --> D1

class D1: public B1 {
public:

    // constructor

    D1(): B1(), x4(4) {}

    int x4;
};

// class --> D2

class D2: public D1 {
public:

    // constructor

    D2(): D1(), x5(5) {}

    int x5;
};

// the main function

int main()
{
    // local variables

    B1 b1;
    D1 d1;
    D2 d2;

    // size

    cout << " --> sizeof(b1) = " << sizeof(b1) << endl;
    cout << " --> sizeof(d1) = " << sizeof(d1) << endl;
    cout << " --> sizeof(d2) = " << sizeof(d2) << endl;

    return 0;
}

// end
