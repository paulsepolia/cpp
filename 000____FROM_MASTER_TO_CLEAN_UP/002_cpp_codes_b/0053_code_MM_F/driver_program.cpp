//======//
// list //
//======//

#include <iostream>
#include <list>
#include <cmath>
#include <ctime>
#include <iomanip>

using std::endl;
using std::cout;
using std::list;
using std::pow;
using std::clock;
using std::fixed;
using std::setprecision;

// type definition

typedef unsigned long int uli;

// a predicate implemented as a function:

bool is_odd_fun(const uli & value)
{
    return ((value%2) == 1);
}

// a predicate implemented as a class:

struct is_odd {
    bool operator() (const uli & value)
    {
        return ((value%2) == 1);
    }
};

// the main function

int main()
{
    // local parameters

    const uli DIM1 = 5 * uli(pow(10.0, 7.0));
    const uli ELEM = 3UL;

    list<uli> L1;
    time_t t1;
    time_t t2;

    // output adjust

    cout << fixed;
    cout << setprecision(10);

    // # 1

    L1.assign(DIM1, ELEM);

    cout << " --> L1.remove_if(is_odd_fun);" << endl;

    t1 = clock();

    L1.remove_if(is_odd_fun);

    t2 = clock();

    cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

    // # 2

    L1.assign(DIM1, ELEM);

    cout << " --> L1.remove_if(is_odd());" << endl;

    t1 = clock();

    L1.remove_if(is_odd());

    t2 = clock();

    cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

    // # 3

    L1.assign(DIM1, ELEM);

    cout << " --> L1.unique();" << endl;

    t1 = clock();

    L1.unique();

    t2 = clock();

    cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

    cout << " --> L1 contains:";

    for (auto it = L1.begin(); it != L1.end(); ++it) {
        cout << ' ' << *it;
    }

    cout << endl;

    cout << " --> L1.size() = " << L1.size() << endl;

    return 0;
}

// end
