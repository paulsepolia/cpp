//================//
// move symantics //
//================//

#include <iostream>
#include <utility>
#include <list>
#include <cmath>

using std::cout;
using std::endl;
using std::move;
using std::list;
using std::pow;

// type definitions

typedef unsigned long int uli;

// the main function

int main()
{
    // local parameters

    const uli DIM = 5 * static_cast<uli>(pow(10.0, 7.0));
    const uli TRIALS1 = static_cast<uli>(pow(10.0, 8.0));
    const uli TRIALS2 = static_cast<uli>(pow(10.0, 1.0));

    // local variables

    list<double> l1;
    list<double> l2;
    uli x = 0;

    // build the list

    cout << " --> build the list" << endl;

    for (uli i = 0; i != DIM; i++) {
        l1.push_back(i+0.1234);
    }

    // move l1 to l2

    cout << " --> move the lists --> fast process" << endl;

    for (uli i = 0; i != TRIALS1; i++) {
        l2 = move(l1);
        l1 = move(l2);
        x = x+1;
    }

    cout << " -->  &l1 = " << &l1    << endl;
    cout << " -->  &l2 = " << &l2    << endl;
    cout << " -->    x = " << x << endl;

    // copy l1 to l2

    cout << " --> copy the lists --> slow process" << endl;

    for (uli i = 0; i != TRIALS2; i++) {
        cout << "----------------------------------->> copy --> " << i << endl;
        l2 = l1;
        l1 = l2;
    }

    cout << " --> &l1 = " << &l1    << endl;
    cout << " --> &l2 = " << &l2    << endl;

    return 0;
}

// END
