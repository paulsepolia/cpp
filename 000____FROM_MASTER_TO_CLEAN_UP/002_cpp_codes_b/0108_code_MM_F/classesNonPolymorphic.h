//=========================//
// NON POLYMORPHIC CLASSES //
//=========================//

#ifndef CLASSES_NON_POLYMORPHIC_H
#define CLASSES_NON_POLYMORPHIC_H

#include <iostream>

using std::endl;
using std::cout;

//=========//
// class A //
//=========//

namespace NONPOLYM {

class A {
public:

    // # 1 --> constructor

    A()
    {
        cout << " --> constructor --> A" << endl;
    }

    // # 2 --> fun

    void fun()
    {
        sumA = sumA + 1.0;
    }

    // # 3 --> get

    double get()
    {
        return sumA;
    }

    // # 4 --> destructor

    virtual ~A()
    {
        cout << " --> destructor --> ~A" << endl;
    }

private:

    static double sumA;
};

double A::sumA = 0;

//=========//
// class B //
//=========//

class B : virtual public A {
public:

    // # 1 --> constructor

    B(): A()
    {
        cout << " --> constructor --> B" << endl;
    }

    // # 2 --> fun

    void fun()
    {
        sumB = sumB + 1.0;
    }

    // # 3 --> get

    double get()
    {
        return sumB;
    }

    // # 4 --> destructor

    virtual ~B()
    {
        cout << " --> destructor --> ~B" << endl;
    }

private:

    static double sumB;
};

double B::sumB = 0;

//=========//
// class C //
//=========//

class C : virtual public B {
public:

    // # 1 --> constructor

    C(): A(), B()
    {
        cout << " --> constructor --> C" << endl;
    }

    // # 2 --> fun

    void fun()
    {
        sumC = sumC + 1.0;
    }

    // # 3 --> get

    double get()
    {
        return sumC;
    }

    // # 4 --> destructor

    virtual ~C()
    {
        cout << " --> destructor --> ~C" << endl;
    }

private:

    static double sumC;
};

double C::sumC = 0;

//=========//
// class D //
//=========//

class D : virtual public C {
public:

    // # 1 --> constructor

    D(): A(), B(), C()
    {
        cout << " --> constructor --> D" << endl;
    }

    // # 2 --> fun

    void fun()
    {
        sumD = sumD + 1.0;
    }

    // # 3 --> get

    double get()
    {
        return sumD;
    }

    // # 4 --> destructor

    virtual ~D()
    {
        cout << " --> destructor --> ~D" << endl;
    }

private:

    static double sumD;
};

double D::sumD = 0;

}// end namespace NONPOLYM

#endif // CLASSES_NON_POLYMORPHIC_H

// end
