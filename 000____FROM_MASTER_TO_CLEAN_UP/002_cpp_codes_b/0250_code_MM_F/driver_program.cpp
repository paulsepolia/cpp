//================//
// tail recursion //
//================//

#include <iostream>
#include <iomanip>
#include <cmath>
#include <chrono>

using std::cout;
using std::endl;
using namespace std::chrono;

unsigned int f(unsigned int a)
{
    if (a == 0 || a == 1) {
        return a;
    }

    return f(a-1) + f(a-2);  // tail recursion
}

// the main function

int main()
{
    const unsigned long int LIM(5*std::pow(10.0, 1.0));
    auto t1 = system_clock::now();
    auto t2 = system_clock::now();
    duration<double> time_span;

    cout << std::fixed;
    cout << std::setprecision(10);
    cout << std::showpos;

    // execute here

    t1 = system_clock::now();
    cout << " --> f         = " << std::setw(30) << std::right << f(LIM) << endl;
    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << std::setw(30) << std::right << time_span.count() << endl;

    return 0;
}

// end
