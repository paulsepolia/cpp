//=================//
// diamond problem //
//=================//

#include <iostream>

using std::endl;
using std::cout;

// class --> B

class B {
public:

    // default constructor

    B()
    {
        cout << " --> B()" << endl;
    }

    // default destructor

    ~B()
    {
        cout << " --> ~B()" << endl;
    }
};

// class --> D1

class D1: virtual public B {
public:

    // default constructor

    D1(): B()
    {
        cout << " --> D1()" << endl;
    }

    // default destructor

    ~D1()
    {
        cout << " --> ~D1()" << endl;
    }
};

// class --> D2

class D2: virtual public B {
public:

    // default constructor

    D2(): B()
    {
        cout << " --> D2()" << endl;
    }

    // default destructor

    ~D2()
    {
        cout << " --> ~D2()" << endl;
    }
};

// class --> E1

class E1: public D1, public D2 {
public:

    // default constructor

    E1(): D1(), D2()
    {
        cout << " --> E1()" << endl;
    }

    // default destructor

    ~E1()
    {
        cout << " --> ~E1()" << endl;
    }
};

// the main function

int main()
{
    cout << "------------------------------------------------>>  1" << endl;
    cout << "B b1;" << endl;

    B b1;

    cout << "------------------------------------------------>>  2" << endl;
    cout << "D1 d1;" << endl;

    D1 d1;

    cout << "------------------------------------------------>>  3" << endl;
    cout << "D2 d2;" << endl;

    D2 d2;

    cout << "------------------------------------------------>>  4" << endl;
    cout << "E1 e1;" << endl;

    E1 e1;

    cout << "------------------------------------------------>>  5" << endl;
    cout << "B b2 = e1;" << endl;

    B b2 = e1;

    cout << "------------------------------------------------>>  6" << endl;
    cout << "B b3 = e1;" << endl;

    B b3 = e1;

    cout << "------------------------------------------------>>  7" << endl;
    cout << "B & b4 = e1;" << endl;

    B & b4 = e1;

    cout << "------------------------------------------------>>  8" << endl;
    cout << "B & b5 = e1;" << endl;

    B & b5 = e1;

    cout << "------------------------------------------------>>  END" << endl;

    return 0;
}

// end
