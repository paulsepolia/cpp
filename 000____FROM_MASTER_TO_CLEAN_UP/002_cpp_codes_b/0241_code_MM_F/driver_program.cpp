//========================//
// strings and substrings //
//========================//

#include <iostream>
#include <string>
#include <cmath>
#include <cstdlib>
#include <iomanip>

using std::cout;
using std::endl;
using std::string;
using std::to_string;
using std::pow;
using std::rand;
using std::srand;
using std::boolalpha;

#include "Solution.hpp"

#define fun  lengthOfLongestSubstringSTL_v1

// the main function

int main()
{
    // local parameters

    const int STR_SIZE(static_cast<int>(pow(10.0, 3.0)));

    // local variables

    string s1 {};
    int x1(0);
    Solution sol1;
    int max_len(0);
    cout << boolalpha;

    // initialize the random generator

    srand(10);

    // generate random number
    // and build the string

    for(int i = 0; i != STR_SIZE; i++) {
        x1 = rand()%10;
        s1 = s1 + to_string(x1);
    }

    max_len = sol1.fun(s1);

    cout << " max_len = " << max_len << endl;

    // substrings

    s1 = "12345678901234567890";
    string sub1 {};

    sub1 = s1.substr(0,1);
    cout << " sub1 = " << sub1 << endl;
    cout << " sub1.length() = " << sub1.length() << endl;

    sub1 = s1.substr(0,2);
    cout << " sub1 = " << sub1 << endl;
    cout << " sub1.length() = " << sub1.length() << endl;

    sub1 = s1.substr(0,3);
    cout << " sub1 = " << sub1 << endl;
    cout << " sub1.length() = " << sub1.length() << endl;

    sub1 = s1.substr(0,100);
    cout << " sub1 = " << sub1 << endl;
    cout << " sub1.length() = " << sub1.length() << endl;

    sub1 = s1.substr(5,1);
    cout << " sub1 = " << sub1 << endl;
    cout << " sub1.length() = " << sub1.length() << endl;

    // build the original

    sub1.clear();
    sub1.shrink_to_fit();

    for (unsigned int i = 0; i < s1.length(); i++) {
        sub1 = sub1 + s1.substr(i,1);
        cout << " sub1 = " << sub1 << endl;
    }

    cout << " sub1 = " << sub1 << endl;
    cout << " s1   = " << s1 << endl;
    cout << " (sub1 == s1) = " << (sub1 == s1) << endl;

    return 0;
}

// end
