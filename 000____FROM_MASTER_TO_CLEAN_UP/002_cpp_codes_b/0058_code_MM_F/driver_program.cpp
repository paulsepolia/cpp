//======//
// list //
//======//

#include <iostream>
#include <list>
#include <cmath>
#include <random>

using std::cout;
using std::endl;
using std::list;
using std::pow;
using std::random_device;
using std::mt19937;
using std::uniform_real_distribution;

// type definitions

typedef unsigned long int uli;

// class: List

template <typename T>
class List : public list<T> {
};

// the main function

int main()
{
    // local parameters

    const uli DIM1 = uli(pow(10.0, 4.0));
    const double ELEM1 = 123.456;

    // local parameters

    List<double> L1;
    List<double> L2;

    // local game

    L1.assign(DIM1, ELEM1);

    cout << " --> L1.size() = " << L1.size() << endl;

    L1.merge(L1);

    cout << " --> L1.size() = " << L1.size() << endl;

    // clear list

    L1.clear();

    cout << " --> L1.size() = " << L1.size() << endl;

    // fill with random numbers

    random_device rd;
    mt19937 gen(rd());
    uniform_real_distribution<> dis(1, 2);

    for (uli i = 0; i < DIM1; i++) {
        L1.push_back(dis(gen));
    }

    cout << " --> L1.size() = " << L1.size() << endl;

    // sort list

    L1.sort();

    cout << " --> L1.size() = " << L1.size() << endl;

    return 0;
}

// end
