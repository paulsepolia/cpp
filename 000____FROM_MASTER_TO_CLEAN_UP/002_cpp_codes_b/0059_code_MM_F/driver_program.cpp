//======//
// list //
//======//

#include <iostream>
#include <list>
#include <cmath>
#include <random>
#include <iomanip>

using std::cout;
using std::endl;
using std::list;
using std::pow;
using std::random_device;
using std::mt19937;
using std::uniform_real_distribution;
using std::fixed;
using std::setprecision;

// type definitions

typedef unsigned long int uli;

// class: List

template <typename T>
class List : public list<T> {
};

// the main function

int main()
{
    // local parameters

    const uli DIM1 = 3 * uli(pow(10.0, 6.0));
    const uli TRIALS1 = 10;

    // local parameters

    List<int> L1;

    // fill with random numbers

    cout << fixed;
    cout << setprecision(5);

    for (uli ik = 0; ik != TRIALS1; ik++) {

        cout << "----------------------------------------------------------->> " << ik << endl;

        cout << " --> declare random device" << endl;

        random_device rd;
        mt19937 gen(rd());
        uniform_real_distribution<> dis(0, 11);

        cout << " --> build list" << endl;

        for (uli i = 0; i < DIM1; i++) {
            L1.push_back(int(dis(gen)));
        }

        cout << " --> L1.size() = " << L1.size() << endl;

        // sort list

        cout << " --> sort list" << endl;

        L1.sort();

        cout << " --> L1.size() = " << L1.size() << endl;

        // unique list

        cout << " --> unique list" << endl;

        L1.unique();

        cout << " --> L1.size() = " << L1.size() << endl;

        cout << " --> clear list" << endl;

        L1.clear();

        cout << " --> max of the real uniform random distribution" << endl;

        cout << " --> dis.max() = " << dis.max() << endl;

        cout << " --> min of the real uniform random distribution" << endl;

        cout << " --> dis.min() = " << dis.min() << endl;

//		cout << " --> dis.param() = " << dis.param() << endl;
//		cout << " --> dis.a = " << dis.a << endl;
//		cout << " --> dis.b = " << dis.b << endl;

        cout << " --> reset the internal state of the random generator" << endl;

        dis.reset();

        cout << " --> 1 --> dis(gen) = " << dis(gen) << endl;
        cout << " --> 2 --> dis(gen) = " << dis(gen) << endl;
        cout << " --> 3 --> dis(gen) = " << dis(gen) << endl;
        cout << " --> 4 --> dis(gen) = " << dis(gen) << endl;
    }

    return 0;
}

// end
