//=======================================//
// get path of executable and name of it //
//=======================================//

#include <iostream>

using std::cout;
using std::endl;

// the main function

int main()
{
    cout << " --> inside main executable" << endl;
    cout << " --> path = " << program_invocation_name << endl;
    return 0;
}

// end
