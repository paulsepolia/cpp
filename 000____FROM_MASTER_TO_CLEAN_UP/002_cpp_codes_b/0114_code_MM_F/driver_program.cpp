//======================//
// MULTIPLE INHERITANCE //
//======================//

#include "multipleInheritance.h"
#include <iostream>

using std::endl;
using std::cout;

//===================//
// the main function //
//===================//

int main()
{
    {
        cout << " --> A * pa1 = new A;" << endl;
        A * pa1 = new A;
        cout << " --> pa1->fun();" << endl;
        pa1->fun();
        cout << " --> B * pb1 = new B;" << endl;
        B * pb1 = new B;
        cout << " --> pb1->fun();" << endl;
        pb1->fun();
        cout << " --> C * pc1 = new C;" << endl;
        C * pc1 = new C;
        cout << " --> pc1->fun();" << endl;
        pc1->fun();
        cout << " --> D * pd1 = new D;" << endl;
        D * pd1 = new D;
        cout << " --> pd1->fun()" << endl;
        pd1->fun();
        cout << " --> delete pa1;" << endl;
        delete pa1;
        cout << " --> delete pb1;" << endl;
        delete pb1;
        cout << " --> delete pc1;" << endl;
        delete pc1;
        cout << " --> delete pd1;" << endl;
        delete pd1;
        cout << " --> exit --> 1" << endl;
    }

    cout << " --> MIDDLE ----------------------------------------------->> 1" << endl;

    {
        cout << " --> A * pa1 = new A;" << endl;
        A * pa1 = new A;
        cout << " --> pa1->fun();" << endl;
        pa1->fun();
        cout << " --> A * pb1 = new B;" << endl;
        A * pb1 = new B;
        cout << " --> pb1->fun();" << endl;
        pb1->fun();
        cout << " --> A * pc1 = new C;" << endl;
        A * pc1 = new C;
        cout << " --> pc1->fun();" << endl;
        pc1->fun();
        cout << " --> A * pd1 = new D;" << endl;
        A * pd1 = new D;
        cout << " --> pd1->fun()" << endl;
        pd1->fun();
        cout << " --> delete pa1;" << endl;
        delete pa1;
        cout << " --> delete pb1;" << endl;
        delete pb1;
        cout << " --> delete pc1;" << endl;
        delete pc1;
        cout << " --> delete pd1;" << endl;
        delete pd1;
        cout << " --> exit --> 2" << endl;
    }

    cout << " --> MIDDLE ----------------------------------------------->> 2" << endl;

    return 0;
}

// end
