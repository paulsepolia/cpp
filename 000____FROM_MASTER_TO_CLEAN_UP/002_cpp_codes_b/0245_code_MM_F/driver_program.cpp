//===========//
// recursion //
//===========//

#include <iostream>
#include <iomanip>
#include <cmath>
#include <chrono>

using std::cout;
using std::endl;
using namespace std::chrono;

// # 1

double rec_v1()
{
    static double sum(0);
    static unsigned long int counter_loc(0);
    const unsigned long int LIM(std::pow(10.0, 8.0));

    sum = sum + 1;
    counter_loc++;

    if(counter_loc >= LIM) {
        return 0;
    }

    rec_v1();

    return sum;
}

// the main function

int main()
{
    cout << " sum = " << rec_v1() << endl;
    return 0;
}

// end
