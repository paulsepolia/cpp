
#include <iostream>
#include <vector>
#include <utility>
#include <ctime>
#include <iomanip>

using std::cout;
using std::endl;
using std::vector;
using std::move;
using std::clock;
using std::fixed;
using std::setprecision;

// function --> version --> 1

const vector<int> & doubleValues (vector<int>& v)
{
    cout << "--> function --> 1 --> with l-value and r-value reference" << endl;

    int i = 0;

    for (auto itr = v.begin(), end_itr = v.end(); itr != end_itr; ++itr) {
        v[i] =  2 * (*itr);
        i = i + 1;
    }

    return v;
}

// function --> version --> 2

const vector<int> & doubleValues (vector<int> && v)
{
    cout << "--> function --> 2 --> with r-value reference" << endl;

    int i = 0;

    for (auto itr = v.begin(), end_itr = v.end(); itr != end_itr; ++itr) {
        v[i] =  2 * (*itr);
        i = i + 1;
    }

    return move(v);
}


// the main function

int main()
{
    const int DIM = 700000000;
    const int TRIALS = 10;

    vector<int> v;
    time_t t1;
    time_t t2;

    // set the output look

    cout << fixed;
    cout << setprecision(10);

    // clear and build

    cout << "--> clear and build the vector" << endl;

    t1 = clock();

    v.clear();
    v.shrink_to_fit();

    for (int i = 0; i < DIM; i++) {
        v.push_back(i);
    }

    t2 = clock();
    cout << "--> time to clear/build = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

    // without move

    cout << "--> without move" << endl;

    t1 = clock();

    for (int i = 0; i != TRIALS; i++) {
        v = doubleValues(v);
    }

    t2 = clock();
    cout << "--> time without move = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

    cout << " --> v[0] = " << v[0] << endl;
    cout << " --> v[1] = " << v[1] << endl;
    cout << " --> v[2] = " << v[2] << endl;
    cout << " --> v[3] = " << v[3] << endl;

    // clear and build the vector

    cout << "--> clear and build the vector" << endl;

    t1 = clock();

    v.clear();
    v.shrink_to_fit();

    for (int i = 0; i < DIM; i++) {
        v.push_back(i);
    }

    t2 = clock();
    cout << "--> time to clear/build = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

    // with move

    cout << "--> with move" << endl;

    t1 = clock();

    for (int i = 0; i != TRIALS; i++) {
        v = doubleValues(move(v));
    }

    t2 = clock();
    cout << "--> time with move = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

    cout << " --> v[0] = " << v[0] << endl;
    cout << " --> v[1] = " << v[1] << endl;
    cout << " --> v[2] = " << v[2] << endl;
    cout << " --> v[3] = " << v[3] << endl;

    return 0;
}

// end

