//======//
// list //
//======//

#include <iostream>
#include <list>
#include <cmath>
#include <ctime>
#include <iomanip>

using std::endl;
using std::cout;
using std::cin;
using std::list;
using std::pow;
using std::clock;
using std::fixed;
using std::setprecision;

// type definitions

typedef unsigned long int uli;

// the main function

int main ()
{
    // local parameters

    uli DIM1 = 3 * uli(pow(10.0, 4.0));
    uli DIM2 = uli(pow(10.0, 1.0));

    // adjust the output

    cout << fixed;
    cout << setprecision(10);

    // local variables

    time_t t1;
    time_t t2;
    list<double> L1;
    list<double> L2;
    list<double> L3;
    list<double>::iterator it1;
    list<double>::iterator it2;

    // build list L1, L2, L3

    cout << " --> L1.assign(DIM1, 123.4);" << endl;
    cout << " --> L2.assign(DIM1, 123.4);" << endl;
    cout << " --> L3.assign(DIM1, 123.4);" << endl;

    t1 = clock();

    for (uli i = 0; i != DIM2; i++) {

        cout << "-------------------------------------------->> 1 --> " << i << endl;

        L1.assign(DIM1, 123.4);

        while(L1.size() != 0) {
            it1 = L1.begin();
            L1.erase(it1);
        }

        cout << "-------------------------------------------->> 2 --> " << i << endl;

        L2.assign(DIM1, 123.4);

        while(L2.size() != 0) {
            it1 = L2.begin();
            L2.erase(it1);
        }

        cout << "-------------------------------------------->> 3 --> " << i << endl;

        L3.assign(DIM1, 123.4);

        while(L3.size() != 0) {
            it1 = L3.begin();
            L3.erase(it1);
        }
    }

    t2 = clock();
    cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;


    cout << " --> size of L1 = " << uli(L1.size()) << endl;
    cout << " --> size of L2 = " << uli(L2.size()) << endl;
    cout << " --> size of L3 = " << uli(L3.size()) << endl;

    return 0;
}

// end
