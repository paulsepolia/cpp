//=====================//
// erasing from vector //
//=====================//

#include <iostream>
#include <vector>

using std::cout;
using std::endl;

int main()
{
    std::vector<int> my_vec;

    // set some values (from 1 to 10)

    for (int i = 1; i <= 10; i++) {
        my_vec.push_back(i);
    }

    cout << " 1 --> my_vec.size() = " << my_vec.size() << endl;

    // erase the 6th element

    my_vec.erase(my_vec.begin()+5);

    cout << " 2 --> my_vec.size() = " << my_vec.size() << endl;

    // erase the first 3 elements

    my_vec.erase(my_vec.begin(), my_vec.begin()+3);

    cout << " 3 --> my_vec.size() = " << my_vec.size() << endl;

    std::cout << " 4 --> my_vec contains:";

    for (unsigned int i = 0; i < my_vec.size(); ++i) {
        std::cout << ' ' << my_vec[i];
    }

    std::cout << endl;

    return 0;
}

// end
