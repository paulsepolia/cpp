//================//
// throw overflow //
//================//

#include <iostream>
#include <iomanip>

using std::cout;
using std::endl;
using std::boolalpha;

#include <boost/numeric/conversion/cast.hpp>

using boost::numeric_cast;
using boost::numeric::bad_numeric_cast;
using boost::numeric::positive_overflow;
using boost::numeric::negative_overflow;

// the main function

int main()
{
    bool error_flag(false);
    cout << boolalpha;

    int x(0);
    cout << " x = " << x << endl;
    x = numeric_cast<int>(123456);
    cout << " x = " << x << endl;

    try {
        x = numeric_cast<int>(12345678901234567890);
    } catch(...) {
        error_flag = true;
    }

    cout << " x = " << x << endl;
    cout << " error detected = " << error_flag << endl;

    return 0;
}

// end
