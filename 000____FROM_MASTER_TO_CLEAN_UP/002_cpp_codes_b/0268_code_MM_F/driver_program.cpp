//=============//
// linked list //
//=============//

#include <iostream>
using std::endl;
using std::cout;

// list definition

class list {
public:
    int data;
    list * next;
};

// the main function

int main()
{
    //=============//
    // example # 1 //
    //=============//

    // # 1
    // create some nodes

    list * node01(new list);
    list * node02(new list);
    list * node03(new list);
    list * node04(new list);
    list * node05(new list);
    list * node06(new list);
    list * node07(new list);
    list * node08(new list);
    list * node09(new list);
    list * node10(new list);

    // # 2
    // put values in the data element

    node01->data = 1;
    node02->data = 2;
    node03->data = 3;
    node04->data = 4;
    node05->data = 5;
    node06->data = 6;
    node07->data = 7;
    node08->data = 8;
    node09->data = 9;
    node10->data = 10;

    // # 3
    // connect manually the nodes

    node01->next = node02;
    node02->next = node03;
    node03->next = node04;
    node04->next = node05;
    node05->next = node06;
    node06->next = node07;
    node07->next = node08;
    node08->next = node09;
    node09->next = node10;
    node10->next = 0;

    // # 4
    // traverse the nodes

    cout << " node01->next->data = " << node01->next->data << endl;
    cout << " node01->next->next->data = " << node01->next->next->data << endl;
    cout << " node01->next->next->next->data = " << node01->next->next->next->data << endl;
    cout << " node01->next->next->next->next->data = " << node01->next->next->next->next->data << endl;

    // # 5
    // delete the nodes

    delete node01;
    delete node02;
    delete node03;
    delete node04;
    delete node05;
    delete node06;
    delete node07;
    delete node08;
    delete node09;
    delete node10;

    return 0;
}

// END
