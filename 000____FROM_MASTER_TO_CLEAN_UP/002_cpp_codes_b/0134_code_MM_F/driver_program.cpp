//===========================//
// function pointers example //
//===========================//

#include <iostream>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <iomanip>
#include <algorithm>

using std::endl;
using std::cout;
using std::cin;
using std::pow;
using std::rand;
using std::qsort;
using std::time;
using std::fixed;
using std::setprecision;
using std::sort;

// function --> 1

// we use void * arguments so as to be able to use any type of agruments
// that is the C-way to pass any type of arguments
// C++ uses templates, but also the above syntax,
// so it is perfectly legal to use that in a C++ pure code

int int_sorter(const void * first_arg, const void * second_arg)
{
    int first = * (int*)(first_arg); // cast void* to int*
    int second = * (int*)(second_arg); // cast void* to int*

    // sort here

    if (first < second) {
        return -1;
    } else if (first == second) {
        return 0;
    } else {
        return 1;
    }
}

// function --> 2

// we use void * arguments so as to be able to use any type of agruments
// that is the C-way to pass any type of arguments
// C++ uses templates, but also the above syntax,
// so it is perfectly legal to use that in a C++ pure code

int dbl_sorter(const void * first_arg, const void * second_arg)
{
    double first = * (double*) (first_arg); // cast void* to double*
    double second = * (double*) (second_arg); // cast void* to double*

    // sort here

    if (first < second) {
        return -1;
    } else if (first == second) {
        return 0;
    } else {
        return 1;
    }
}

// template function object --> 3
// C++11 way

template<typename T>
class gen_sorter {
public:
    bool operator()(T first, T second)
    {
        return (first < second);
    }
};

// template function --> 4
// C++11 way

template<typename T>
bool gen_fun_sorter(T first, T second)
{
    return (first < second);
}

// the main function

int main()
{
    // local variables and parameters

    const int DIMEN_MAX = static_cast<int>(pow(10.0, 7.0));
    int * arrayInt = new int [DIMEN_MAX];
    double * arrayDbl = new double [DIMEN_MAX];
    long double * arrayLDbl1 = new long double [DIMEN_MAX];
    long double * arrayLDbl2 = new long double [DIMEN_MAX];
    int i;

    // adjust output

    cout << fixed;
    cout << setprecision(5);

    //==================//
    // build the arrays //
    //==================//

    // integers

    srand(4);

    for (i = 0; i < DIMEN_MAX; i++) {
        arrayInt[i] = rand();
    }

    // doubles

    srand(4);

    for (i = 0; i < DIMEN_MAX; i++) {
        arrayDbl[i] = static_cast<double>(rand());
    }

    // long doubles # 1

    srand(4);

    for (i = 0; i < DIMEN_MAX; i++) {
        arrayLDbl1[i] = static_cast<long double>(rand());
    }

    // long doubles # 2

    srand(4);

    for (i = 0; i < DIMEN_MAX; i++) {
        arrayLDbl2[i] = static_cast<long double>(rand());
    }

    //=================//
    // sort the arrays //
    //=================//

    // integers

    cout << " --> sort the array of integers" << endl;

    qsort(arrayInt, DIMEN_MAX, sizeof(int), int_sorter);

    // doubles

    cout << " --> sort the array of doubles" << endl;

    qsort(arrayDbl, DIMEN_MAX, sizeof(double), dbl_sorter);

    // long doubles # 1

    cout << " --> sort the array of long doubles" << endl;

    sort(arrayLDbl1, arrayLDbl1+DIMEN_MAX, gen_sorter<long double>());

    // long doubles # 2

    cout << " --> sort the array of long doubles" << endl;

    sort(arrayLDbl2, arrayLDbl2+DIMEN_MAX, gen_fun_sorter<long double>);

    // display some results

    for ( i = 0; i < 10; i++ ) {
        cout << " --> " << i
             << " --> " << arrayInt[i]
             << " --> " << arrayDbl[i]
             << " --> " << arrayLDbl1[i]
             << " --> " << arrayLDbl2[i] << endl;
    }


    // delete the array

    delete [] arrayInt;
    delete [] arrayDbl;
    delete [] arrayLDbl1;
    delete [] arrayLDbl2;

    return 0;
}
