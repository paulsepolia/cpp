//=======================//
// class Vec declaration //
//=======================//

#ifndef VEC_DECLARATION_H
#define VEC_DECLARATION_H

#include "parameters.hpp"
#include <memory>
#include <string>
#include <new>

using std::unique_ptr;
using std::string;
using std::bad_alloc;

template<typename T>
class Vec {
public:

    // construnctor

    Vec();

    // destructor

    virtual ~Vec();

    // copy constructor

    Vec(const Vec &);

    // move constructor

    Vec(Vec &&);

    // copy assignment operator

    const Vec & operator=(const Vec &);

    // move assignment operator

    const Vec & operator=(Vec &&);

    // public  member functions

    void allocate(const uli &);
    bool check_allocation() const;
    void deallocate();
    T get(const uli &) const throw(string);
    void set(const uli &, const T &);
    void add(const Vec &, const Vec &); // # 1
    void add(const Vec &, const T &);   // # 2
    void add(const T &, const Vec &);   // # 3
    void subtract(const Vec &, const Vec &); // # 1
    void subtract(const Vec &, const T &);   // # 2
    void subtract(const T &, const Vec &);   // # 3
    void multiply(const Vec &, const Vec &); // # 1
    void multiply(const Vec &, const T &);   // # 2
    void multiply(const T &, const Vec &);   // # 3
    void divide(const Vec &, const Vec &); // # 1
    void divide(const Vec &, const T &);   // # 2
    void divide(const T &, const Vec &);   // # 3
    uli get_size() const;

    // overloaded operators

    T operator[](const uli &);
    Vec operator+(const Vec &); // # 1
    Vec operator+(const T &);   // # 2

    template<typename K>
    friend Vec<K> operator+(const K &, const Vec<K> &); // # 3

    Vec operator-(const Vec &); // # 1
    Vec operator-(const T &);   // # 2

    template<typename K>
    friend Vec<K> operator-(const K &, const Vec<K> &); // # 3

    Vec operator*(const Vec &); // # 1
    Vec operator*(const T &);   // # 2

    template<typename K>
    friend Vec<K> operator*(const K &, const Vec<K> &); // # 3

    Vec operator/(const Vec &); // # 1
    Vec operator/(const T &);   // # 2

    template<typename K>
    friend Vec<K> operator/(const K &, const Vec<K> &); // 3

private:

    // private member functions

    string throw_error_and_quit(string) const;

private:

    uli _size;
    unique_ptr<T> _p;
};

#endif // VEC_DECLARATION_H
