//=============//
// statci cast //
//=============//

#include <iostream>
#include <typeinfo>

using std::cout;
using std::endl;

// class --> Base

struct Base {

    ~Base()
    {
        cout << " base --> destructor" << endl;
    }

    void name()
    {
        cout << " name --> base" << endl;
    }
};

// class --> Derived

struct Derived: Base {

    ~Derived()
    {
        cout << " derived --> destructor" << endl;
    }

    void name()
    {
        cout << " name --> derived" << endl;
    }
};

// the main function

int main()
{
    // # 1
    // trying to downcast a base class pointer

    cout << "---------------------------------------------------------->> 1" << endl;

    Base * b1 = new Base();

    cout << " b1 = " << b1 << endl;
    cout << " typeid(b1).name() = " << typeid(b1).name() << endl;

    b1->name();

    // downcasting

    Derived* d1 = static_cast<Derived*>(b1);

    cout << " d1 = " << d1 << endl;
    cout << " typeid(d1).name() = " << typeid(d1).name() << endl;

    if(d1) {
        cout << " downcast from b1 to d1 successful" << endl;
        d1->name(); // safe to call
    }

    // # 2
    // trying to downcast a base class pointer

    cout << "---------------------------------------------------------->> 2" << endl;

    Base * b2 = new Derived();

    cout << " b2 = " << b2 << endl;
    cout << " typeid(b2).name() = " << typeid(b2).name() << endl;

    b2->name();

    // downcasting

    Derived* d2 = static_cast<Derived*>(b2);

    cout << " d2 = " << d2 << endl;
    cout << " typeid(d2).name() = " << typeid(d2).name() << endl;

    if(d2) {
        cout << " downcast from b2 to d2 successful" << endl;
        d2->name(); // safe to call
    }

    // # 3 free the RAM

    cout << "---------------------------------------------------------->> 3" << endl;

    cout << " --> delete b1;" << endl;

    delete b1;

    cout << " --> delete b2;" << endl;

    delete b2;
}

// end
