//============//
// shared_ptr //
//============//

#include <memory>
#include <iostream>

using std::endl;
using std::cout;

struct Foo {
    Foo()
    {
        cout << "Foo..." << endl;
    }
    ~Foo()
    {
        cout << "~Foo..." << endl;
    }
};

struct D {
    void operator()(Foo* p) const
    {
        cout << "Call delete from function object..." << endl;
        delete p;
    }
};

int main()
{
    {
        cout << " --> 1 --> start" << endl;
        cout << "constructor with no managed object" << endl;
        std::shared_ptr<Foo> sh1;
        cout << " --> 1 --> end" << endl;
    }

    {
        cout << " --> 2 --> start" << endl;
        cout << "constructor with object" << endl;
        std::shared_ptr<Foo> sh2(new Foo);
        std::shared_ptr<Foo> sh3(sh2);
        cout << sh2.use_count() << endl;
        cout << sh3.use_count() << endl;
        cout << " --> 2 --> end" << endl;
    }

    {
        cout << "constructor with object and deleter" << endl;
        std::shared_ptr<Foo> sh4(new Foo, D());
        std::shared_ptr<Foo> sh5(new Foo, [](auto p) {
            cout << "Call delete from lambda..." << endl;
            delete p;
        });
    }
}

// end
