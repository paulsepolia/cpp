//=============//
// linked list //
//=============//

#include <cmath>
#include <iostream>
using std::endl;
using std::cout;

#define DEBUG
//#undef DEBUG

// list definition

class list {
public:
    int data;
    list * next;
};

// the main function

int main()
{
    const int DIM(std::pow(10.0, 2.0));

    //=============//
    // example # 1 //
    //=============//

    // # 1
    // create the head node and a node

    list * head(new list);
    list * node(new list);

    // # 2
    // initialize where head is pointing

    head->next = node;

    for(int i = 0; i != DIM; i++) {
        node->data = i;
        // create a new node
        node->next = new list;
        // initialize where there node is pointing
        node = node->next;
    }

    // # 3
    // set the last node to null
    node->next = 0;

    // # 4
    // print out the data elements of the nodes

    node = head;
    while(node->next) {
        cout << " node->data = " << node->data << endl;
        node = node->next;
    }

    // # 5
    // delete the nodes

    list * q;
    node = head->next;
    while(node->next) {
        q = node->next;
        node->next = q->next;
        delete q;
    }

    delete node;
    delete head;
    return 0;
}

// END
