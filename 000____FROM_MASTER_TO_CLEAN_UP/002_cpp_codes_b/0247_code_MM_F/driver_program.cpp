//===========//
// recursion //
//===========//

#include <iostream>

using std::cout;
using std::endl;

// recursive function

void number_function(int i)
{
    cout << "The number is: " << i << endl;
    i++;

    if(i < 10) {
        number_function(i);
    }
}

// the main function

int main()
{
    int i = 0;
    number_function(i);
    return 0;
}

// end
