//==============//
// move example //
//==============//

#include <utility>   // std::move
#include <iostream>  // std::cout
#include <vector>    // std::vector
#include <string>    // std::string

using std::endl;
using std::cout;
using std::vector;
using std::string;
using std::move;

// class MetaData

class MetaData {
public:

    MetaData (int size, const string & name)
        : _name(name)
        , _size(size)
    {
        cout << " --> A1 --> MetaData --> constructor" << endl;
    }

    // copy constructor

    MetaData (const MetaData & other)
        : _name(move(other._name))
        , _size(other._size)
    {
        cout << " --> A2 --> MetaData --> copy constructor" << endl;
    }

    // move constructor

    MetaData (MetaData && other)
        : _name(move(other._name))
        , _size(other._size)
    {
        cout << " --> A3 --> MetaData --> move constructor" << endl;
    }

    // copy assignment operator

    const MetaData & operator = (const MetaData & other)
    {
        cout << " --> A4 --> MetaData --> copy assignment operator" << endl;

        this->_name = other._name;
        this->_size = other._size;

        return *this;
    }

    // move assignment operator

    MetaData & operator = (MetaData && other)
    {
        cout << " --> A5 --> MetaData --> move assignment operator" << endl;

        this->_name = move(other._name);
        this->_size = move(other._size);

        return *this;
    }

    // function

    string getName () const
    {
        cout << " --> A6 --> MetaData --> getName() function" << endl;
        return _name;
    }

    // function

    int getSize () const
    {
        cout << " --> A7 --> MetaData --> getSize() function" << endl;
        return _size;
    }

    // destructor

    virtual ~MetaData()
    {
        cout << " --> A8 --> MetaData --> destructor" << endl;
    }

private:

    string _name;
    int _size;
};

// class

class ArrayWrapper {
public:

    // default constructor produces a moderately sized array

    ArrayWrapper ()
        : _p_vals(new int[64])
        , _metadata(64, "ArrayWrapper")
    {
        cout << " --> B1 --> ArrayWrapper --> constructor ALPHA" << endl;
    }

    // constructor

    ArrayWrapper (int n)
        : _p_vals(new int[n])
        , _metadata(n, "ArrayWrapper")
    {
        cout << " --> B2 --> ArrayWrapper --> constructor BETA" << endl;
    }

    // move constructor

    ArrayWrapper (ArrayWrapper && other)
        : _p_vals(other._p_vals)
        , _metadata(move(other._metadata))
    {
        cout << " --> B3 --> ArrayWrapper --> move constructor" << endl;
        other._p_vals = NULL;
    }

    // copy constructor

    ArrayWrapper (const ArrayWrapper & other)
        : _p_vals(new int[other._metadata.getSize()])
        , _metadata(other._metadata)
    {
        cout << " --> B4 --> ArrayWrapper --> copy constructor" << endl;
        for (int i = 0; i < _metadata.getSize(); ++i) {
            _p_vals[i] = other._p_vals[i];
        }
    }

    // destructor

    ~ArrayWrapper ()
    {
        cout << " --> B5 --> ArrayWrapper -->  destructor" << endl;
        delete [] _p_vals;
    }

private:

    int *_p_vals;
    MetaData _metadata;
};

// the main function

int main()
{
    MetaData a1(1000, "PAVLOS");
    MetaData a2(1000, "PAVLOS");

    cout << " --> a1.getSize() = " << endl;
    cout << a1.getSize() << endl;
    cout << " --> a1.getName() = " << endl;
    cout << a1.getName() << endl;

    cout << " a1 = a2 --> start" << endl;
    a1 = a2;
    cout << " a1 = a2 --> end" << endl;

    cout << " a1 = move(a2) --> start" << endl;
    a1 = move(a2);
    cout << " a1 = move(a2) --> end" << endl;

    ArrayWrapper b1(2);
    ArrayWrapper b2(b1);

    ArrayWrapper b3(move(b2));

    return 0;
}
