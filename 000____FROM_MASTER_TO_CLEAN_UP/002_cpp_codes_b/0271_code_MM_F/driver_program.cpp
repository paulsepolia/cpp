//=============//
// linked list //
//=============//

#include <cmath>
#include <iostream>
using std::endl;
using std::cout;

#define DEBUG
//#undef DEBUG

// list definition

class list {
public:
    int data;
    list * next;
};

// the main function

int main()
{
    const int DIM(std::pow(10.0, 1.0));

    // # 1 create nodes

    list * head(new list);
    list * node(new list);

    // initialize the head

    head->next = node;

    // build the nodes

    for(int i = 0; i != DIM; i++) {
        node->data = i;
        node->next = new list;
        node = node->next;
    }

    // finalize the nodes

    node->next = 0;

    // get back the values

    node = head->next;

    while(node->next) {
        cout << " node->data = " << node->data << endl;
        node = node->next;
    }

    // delete the nodes
    node = head;
    while(node->next) {
        list * q;
        q = node->next;
        node->next = q->next;
        delete q;
    }

    // delete the head
    delete head;

    return 0;
}

// END
