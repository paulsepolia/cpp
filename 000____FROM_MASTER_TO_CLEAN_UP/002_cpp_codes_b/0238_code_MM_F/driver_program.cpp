//=============//
// static cast //
//=============//

#include <iostream>

using std::cout;
using std::endl;

// class --> A

class A {
public:
    A(): xA(1)
    {
        cout << " --> A" << endl;
    }
public:
    int xA;
};

// class --> B

class B: public A {
public:
    B(): A(), xB(2)
    {
        cout << " --> B" << endl;
    }
public:
    int xB;
};

// function --> fun1

void fun1(const A & val)
{
    const B * pb = static_cast<const B*>(&val);
    cout << "pb->xA = " << pb->xA << endl;
    cout << "pb->xB = " << pb->xB << endl;
}

// function --> fun2

void fun2(A & val)
{
    B * pb = static_cast<B*>(&val);
    cout << "pb->xA = " << pb->xA << endl;
    cout << "pb->xB = " << pb->xB << endl;
}

// function --> fun3

void fun3(A & val)
{
    B * pb = (B*)(&val);
    cout << "pb->xA = " << pb->xA << endl;
    cout << "pb->xB = " << pb->xB << endl;
}

// the main function

int main()
{
    cout << " A a;" << endl;

    A a;

    cout << " B b;" << endl;

    B b;

    cout << " --------------------------------------> fun1" << endl;

    cout << " fun1(a);" << endl;

    fun1(a);

    cout << " fun1(b);" << endl;

    fun1(b);

    cout << " --------------------------------------> fun2" << endl;

    cout << " fun2(a);" << endl;

    fun2(a);

    cout << " fun2(b);" << endl;

    fun2(b);

    cout << " --------------------------------------> fun3" << endl;

    cout << " fun3(a);" << endl;

    fun3(a);

    cout << " fun3(b);" << endl;

    fun3(b);

    return 0;
}

// end
