//=================//
// diamond problem //
//=================//

#include <iostream>
#include <iomanip>

using std::endl;
using std::cout;
using std::boolalpha;

// the main function

int main()
{
    cout << boolalpha;

    // const int and (const int* const) types

    const int x1(10);
    const int * const p1(&x1);
    const int & r1 = x1;

    int * p2(nullptr);

    p2 = const_cast<int*>(&x1);
    *p2 = 100;

    cout << " (&x1 == p1) = " << (&x1 == p1) << endl;
    cout << " (&r1 == p1) = " << (&r1 == p1) << endl;
    cout << " ( p2 == p1) = " << ( p2 == p1) << endl;

    cout << " *p1 = " << *p1 << endl;
    cout << " *p2 = " << *p2 << endl;
    cout << "  x1 = " <<  x1 << endl; // undefined behaviour
    cout << "  r1 = " <<  r1 << endl;

    return 0;
}

// end
