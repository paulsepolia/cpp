//==============//
// late binding //
//==============//

#include <iostream>

using std::endl;
using std::cout;

// class --> Base

class Base {
public:

    virtual void function1()
    {
        cout << " Base --> fun1" << endl;
    };

    virtual void function2()
    {
        cout << " Base --> fun2" << endl;
    };
};

// class --> Base2

class Base2 {
public:
};

// class --> D1

class D1: public virtual Base {
public:

    virtual void function1()
    {
        cout << " D1 --> fun1" << endl;
    };

    virtual void function3()
    {
        cout << " D1 --> fun3" << endl;
    };
};

// class --> D2

class D2: public virtual Base {
public:
    virtual void function2()
    {
        cout << " D2 --> fun2" << endl;
    };

    virtual void function4()
    {
        cout << " D2 --> fun4" << endl;
    };
};

// class --> E1

class E1: public D1, public D2 {
public:

    virtual void function5()
    {
        cout << " E1 --> fun5" << endl;
    };
};

// class --> E2

class E2: public Base2 {
public:

};


// the main function

int main()
{
    Base a1;
    D1 d1;
    D2 d2;
    E1 e1;

    //============//
    // base class //
    //============//

    cout << " ---------------------------------->  1" << endl;

    a1.function1();
    a1.function2();

    //==================//
    // derived D1 class //
    //==================//

    cout << " ---------------------------------->  2" << endl;

    d1.function1();
    d1.function2();
    d1.function3();

    //==================//
    // derived D2 class //
    //==================//

    cout << " ---------------------------------->  3" << endl;

    d2.function1();
    d2.function2();
    d2.function4();

    //=================//
    // derived E class //
    //=================//

    cout << " ---------------------------------->  4" << endl;

    e1.function1();
    e1.function2();
    e1.function3();
    e1.function4();
    e1.function5();

    //====================//
    // base class pointer //
    //====================//

    Base * pa1(0);

    // # 1

    cout << " ---------------------------------->  4" << endl;

    // NO CASTING
    // pa1 is of type Base pointer
    // pa1 is initialized using address of a Base object
    // so we call the Base functions

    pa1 = &a1;
    pa1->function1();
    pa1->function2();

    // # 2

    cout << " ---------------------------------->  5" << endl;

    // UPCASTING
    // Treat &d1 as a base pointer

    pa1 = &d1;
    pa1->function1();
    pa1->function2();

    // # 3

    cout << " ---------------------------------->  6" << endl;

    // UPCASTING
    // Treat &d2 as a base pointer

    pa1 = &d2;
    pa1->function1();
    pa1->function2();

    //==========================//
    // derived D1 class pointer //
    //==========================//

    D1 * pd1(nullptr);

    cout << " ---------------------------------->  7" << endl;

    // NO CASTING HERE

    pd1 = &d1;
    pd1->function1();
    pd1->function2();
    pd1->function3();

    //==========================//
    // derived D2 class pointer //
    //==========================//

    D2 * pd2(nullptr);

    cout << " ---------------------------------->  8" << endl;

    // NO CASTING HERE

    pd2 = &d2;
    pd2->function1();
    pd2->function2();
    pd2->function4();

    //==========================//
    // derived E1 class pointer //
    //==========================//

    E1 * pe1(nullptr);

    cout << " ---------------------------------->  9" << endl;

    // NO CASTING HERE

    pe1 = &e1;
    pe1->function1();
    pe1->function2();
    pe1->function3();
    pe1->function4();
    pe1->function5();

    //==============//
    // dynamic cast //
    //==============//

    // # 1

    cout << " ----------------------------------> 10" << endl;

    pa1 = new D1;
    pd1 = dynamic_cast<D1*>(pa1);

    if(pd1) {
        cout << "DOWNCAST: Treat base pointer as a derived pointer" << endl;
    }

    // # 2

    cout << " ----------------------------------> 11" << endl;

    pa1 = new D2;
    pd2 = dynamic_cast<D2*>(pa1);

    if(pd2) {
        cout << "DOWNCAST: Treat base pointer as a derived pointer" << endl;
    }

    // # 3

    cout << " ----------------------------------> 12" << endl;

    pa1 = new E1;
    pe1 = dynamic_cast<E1*>(pa1);

    if(pe1) {
        cout << "DOWNCAST: Treat base pointer as a derived pointer"  << endl;
    }

    // # 4

    cout << " ----------------------------------> 13" << endl;

    pa1 = new Base;
    pe1 = dynamic_cast<E1*>(pa1);

    if(pe1) {
        cout << "DOWNCAST: Treat base pointer as a derived pointer" << endl;
    } else if(pe1 == 0) {
        cout << "DOWNCAST: FAILURE trying to treat base pointer as derived pointer" << endl;
    }

    //=============//
    // static cast //
    //=============//

    cout << " ----------------------------------> 14" << endl;

    Base2 * pa2;
    pa2 = new Base2;

    E2 * pe2;
    pe2 = static_cast<E2*>(pa2);

    if(pe2) {
        cout << "DOWNCAST::static_cast::RUN TIME ERROR" << endl;
    }

    return 0;
}

// end
