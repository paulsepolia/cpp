//=======================//
// use of move symantics //
//=======================//

#include <iostream>
#include <queue>
#include <new>
#include <cmath>
#include <iomanip>
#include <chrono>

using std::cout;
using std::endl;
using std::deque;
using std::move;
using std::pow;
using std::boolalpha;
using namespace std::chrono;

// type definitions

typedef unsigned long int uli;
typedef const uli culi;

// the main program

int main()
{
    // local parameters

    culi TRIALS(static_cast<culi>(pow(10.0, 5.0)));
    culi DIMEN(static_cast<culi>(pow(10.0, 4.0)));
    cout << boolalpha;
    auto t1 = system_clock::now();
    auto t2 = system_clock::now();
    duration<double> time_span;

    // main game

    deque<double> deq1;

    deq1.clear();
    deq1.resize(DIMEN);
    deq1.shrink_to_fit();

    cout << " deq1.size() = " << deq1.size() << endl;

    deque<double> deq2;
    deq2.clear();
    deq2.shrink_to_fit();

    cout << " deq2.size() = " << deq2.size() << endl;

    cout << " deq2 = deq1;" << endl;

    t1 = system_clock::now();

    uli i = 0;
    while(i != TRIALS) {
        deq2 = deq1;
        i++;
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    cout << " i = " << i << endl;
    cout << " deq1.size() = " << deq1.size() << endl;
    cout << " deq2.size() = " << deq2.size() << endl;
    cout << " (deq1 == deq2) = " << (deq1 == deq2) << endl;

    t1 = system_clock::now();

    i = 0;
    while(i != TRIALS) {
        deq2 = move(deq1);
        deq1 = move(deq2);
        i++;
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);
    cout << " --> time used = " << time_span.count() << endl;

    cout << " i = " << i << endl;
    cout << " deq1.size() = " << deq1.size() << endl;
    cout << " deq2.size() = " << deq2.size() << endl;
    cout << " (deq1 == deq2) = " << (deq1 == deq2) << endl;

    return 0;
}

// end
