//============================//
// MULTIPLE INHERITANCE MIXED //
//============================//

#include "multipleInheritanceMixed.h"
#include <iostream>

using std::endl;
using std::cout;

//===================//
// the main function //
//===================//

int main()
{
    {
        cout << " --> A a1;" << endl;
        A a1;
        cout << " --> B b1;" << endl;
        B b1;
        cout << " --> C c1;" << endl;
        C c1;
        cout << " --> a1.fun();" << endl;
        a1.fun();
        cout << " --> b1.fun();" << endl;
        b1.fun();
        cout << " --> c1.fun();" << endl;
        c1.fun();

        cout << " --> exit --> 1" << endl;
    }

    cout << " --> MIDDLE ----------------------------------------------->> 1" << endl;

    {
        cout << " --> A * pa1 = new C;" << endl;
        A * pa1 = new C;
        cout << " --> B * pb1 = new C;" << endl;
        B * pb1 = new C;
        cout << " --> C * pc1 = new C;" << endl;
        C * pc1 = new C;
        cout << " --> pa1->fun();" << endl;
        pa1->fun();
        cout << " --> pb1->fun();" << endl;
        pb1->fun();
        cout << " --> pc1->fun();" << endl;
        pc1->fun();
        cout << " --> delete pa1;" << endl;
        delete pa1;
        cout << " --> delete pb1;" << endl;
        delete pb1;
        cout << " --> delete pc1;" << endl;
        delete pc1;

        cout << " --> exit --> 2" << endl;
    }

    cout << " --> MIDDLE ----------------------------------------------->> 2" << endl;

    return 0;
}

// end
