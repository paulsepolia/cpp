//======================//
// FUNCTIONS DEFINITION //
//======================//

#include "functions.h"
#include <string>

using std::to_string;

// function # 1

template<>
double ac_fun(const double & x, const double & y)
{
    return x+y;
}

// function # 2

template <>
double product_fun(const double & x, const double & y)
{
    return x*y;
}

// function # 3

template <>
bool equal_fun(const double & i, const double & j)
{
    return (i == j);
}

// function # 4

template <>
bool isOdd_fun(const double & i)
{
    return ((unsigned long int)(i)%2 == 1);
}

// function # 5

template <>
bool lessThanMinusOne_fun(const double & i)
{
    return (i < -1);
}

// function # 6

template <>
double double_fun(const double & x)
{
    return 2*x;
}

// function # 7

template <>
double UniqueNumber_fun<double>()
{
    return double(123.456);
}

// function # 8

template <>
bool strComp_fun(const double & x, const double & y)
{
    return (to_string(x)[0] == to_string(y)[0]);
}

// end
