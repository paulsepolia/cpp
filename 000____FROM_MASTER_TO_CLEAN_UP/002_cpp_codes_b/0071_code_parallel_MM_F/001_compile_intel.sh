#!/bin/bash

  # 1. compile

  icpc -O3                 \
	   -ipo                \
       -Wall               \
	   -wd1202             \
       -std=c++11          \
	   -qopenmp            \
	   -D_GLIBCXX_PARALLEL \
	   functors.cpp        \
	   functions.cpp       \
       driver_program.cpp  \
       -o x_intel
