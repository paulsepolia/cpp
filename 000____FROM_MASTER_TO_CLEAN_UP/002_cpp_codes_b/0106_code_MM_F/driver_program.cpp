//========================//
// INHERITANCE SPEED GAME //
//========================//

#include <iostream>
#include <iomanip>
#include <chrono>
#include <cmath>

using std::endl;
using std::cout;
using std::setprecision;
using std::fixed;
using namespace std::chrono;
using std::pow;

// type definitions

typedef long long int lli;

//=========//
// class A //
//=========//

class A {
public:

    // # 1 --> constructor

    A()
    {
        cout << " --> constructor --> A" << endl;
    }

    // # 2 --> fun

    virtual void fun()
    {
        sumA = sumA + 1.0;
    }

    // # 3 --> get

    virtual double get()
    {
        return sumA;
    }

    // # 4 --> destructor

    virtual ~A()
    {
        cout << " --> destructor --> ~A" << endl;
    }

private:

    static double sumA;
};

double A::sumA = 0;

//=========//
// class B //
//=========//

class B : public A {
public:

    // # 1 --> constructor

    B()
    {
        cout << " --> constructor --> B" << endl;
    }

    // # 2 --> fun

    virtual void fun()
    {
        sumB = sumB + 1.0;
    }

    // # 3 --> get

    virtual double get()
    {
        return sumB;
    }

    // # 4 --> destructor

    virtual ~B()
    {
        cout << " --> destructor --> ~B" << endl;
    }

private:

    static double sumB;
};

double B::sumB = 0;

//=========//
// class C //
//=========//

class C : public B {
public:

    // # 1 --> constructor

    C()
    {
        cout << " --> constructor --> C" << endl;
    }

    // # 2 --> fun

    virtual void fun()
    {
        sumC = sumC + 1.0;
    }

    // # 3 --> get

    virtual double get()
    {
        return sumC;
    }

    // # 4 --> destructor

    virtual ~C()
    {
        cout << " --> destructor --> ~C" << endl;
    }

private:

    static double sumC;
};

double C::sumC = 0;

//=========//
// class D //
//=========//

class D : public C {
public:

    // # 1 --> constructor

    D()
    {
        cout << " --> constructor --> D" << endl;
    }

    // # 2 --> fun

    virtual void fun()
    {
        sumD = sumD + 1.0;
    }

    // # 3 --> get

    virtual double get()
    {
        return sumD;
    }

    // # 4 --> destructor

    virtual ~D()
    {
        cout << " --> destructor --> ~D" << endl;
    }

private:

    static double sumD;
};

double D::sumD = 0;

//===================//
// the main function //
//===================//

int main()
{
    // local parameters

    const lli DIM = static_cast<lli>(pow(10.0, 10.0));

    // local variables

    double x1;
    auto t1 = system_clock::now();
    auto t2 = system_clock::now();
    duration<double> time_span;
    cout << fixed;
    cout << setprecision(5);

    cout << " --> MIDDLE -------------------------------------------> 0" << endl;

    {
        // A

        cout << " --> A a1;" << endl;
        A a1;
        cout << " --> a1.fun();" << endl;
        a1.fun();
        cout << " --> x1 = a1.get();" << endl;
        x1 = a1.get();
        cout << " --> x1 = " << x1 << endl;

        // B

        cout << " --> B b1;" << endl;
        B b1;
        cout << " --> b1.fun();" << endl;
        b1.fun();
        cout << " --> x1 = b1.get();" << endl;
        x1 = b1.get();
        cout << " --> x1 = " << x1 << endl;

        // C

        cout << " --> C c1;" << endl;
        C c1;
        cout << " --> c1.fun();" << endl;
        c1.fun();
        cout << " --> x1 = c1.get();" << endl;
        x1 = c1.get();
        cout << " --> x1 = " << x1 << endl;

        // D

        cout << " --> D d1;" << endl;
        D d1;
        cout << " --> d1.fun();" << endl;
        d1.fun();
        cout << " --> x1 = d1.get();" << endl;
        x1 = d1.get();
        cout << " --> x1 = " << x1 << endl;

        cout << " --> exit --> 1" << endl;
    }

    cout << " --> MIDDLE -------------------------------------------> 1" << endl;

    {
        cout << " --> A * pa1 = new A;" << endl;
        A * pa1 = new A;
        cout << " --> A * pb1 = new B;" << endl;
        A * pb1 = new B;
        cout << " --> A * pc1 = new C;" << endl;
        A * pc1 = new C;
        cout << " --> A * pd1 = new D;" << endl;
        A * pd1 = new D;

        // timing A

        t1 = system_clock::now();

        for (lli i = 0; i != DIM; i++) {
            pa1->fun();
            x1 = pa1->get();
        }

        cout << " --> A --> x1 = " << x1 << endl;
        t2 = system_clock::now();
        time_span = duration_cast<duration<double>>(t2-t1);
        cout << " --> A --> time used = " << time_span.count() << endl;

        // timing B

        t1 = system_clock::now();

        for (lli i = 0; i != DIM; i++) {
            pb1->fun();
            x1 = pb1->get();
        }

        cout << " --> B --> x1 = " << x1 << endl;
        t2 = system_clock::now();
        time_span = duration_cast<duration<double>>(t2-t1);
        cout << " --> B --> time used = " << time_span.count() << endl;

        // timing C

        t1 = system_clock::now();

        for (lli i = 0; i != DIM; i++) {
            pc1->fun();
            x1 = pc1->get();
        }

        cout << " --> C --> x1 = " << x1 << endl;
        t2 = system_clock::now();
        time_span = duration_cast<duration<double>>(t2-t1);
        cout << " --> C --> time used = " << time_span.count() << endl;

        // timing D

        t1 = system_clock::now();

        for (lli i = 0; i != DIM; i++) {
            pd1->fun();
            x1 = pd1->get();
        }

        cout << " --> D --> x1 = " << x1 << endl;
        t2 = system_clock::now();
        time_span = duration_cast<duration<double>>(t2-t1);
        cout << " --> D --> time used = " << time_span.count() << endl;

        // deallocations

        cout << " --> delete pa1;" << endl;
        delete pa1;
        cout << " --> delete pb1;" << endl;
        delete pb1;
        cout << " --> delete pc1;" << endl;
        delete pc1;
        cout << " --> delete pd1;" << endl;
        delete pd1;

        cout << " --> exit --> 2" << endl;
    }

    return 0;
}

// end
