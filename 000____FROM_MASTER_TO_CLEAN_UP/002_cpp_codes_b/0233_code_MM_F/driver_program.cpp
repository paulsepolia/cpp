//================//
// move symantics //
//================//

#include <iostream>
#include <memory>
#include <string>

using std::cout;
using std::endl;
using std::move;
using std::string;

// function using --> lvalue

void printReference(const string & str)
{
    cout << " --> using lvalue" << endl;
    cout << str << endl;
}

// function using --> rvalue

void printReference(string && str)
{
    cout << " --> using rvalue" << endl;
    cout << str << endl;
}

// function --> getName1()

string getName1()
{
    return "Pavlos";
}

//===================//
// the main function //
//===================//

int main()
{
    // # 1

    cout << "------------------------------------------------------------>>  1" << endl;

    int x1(10);
    int x2(20);

    cout << "  x1 = " <<  x1 << endl;
    cout << "  x2 = " <<  x2 << endl;
    cout << " &x1 = " << &x1 << endl;
    cout << " &x2 = " << &x2 << endl;

    cout << " x2 = move(x1);" << endl;

    x2 = move(x1);

    cout << "  x1 = " <<  x1 << endl;
    cout << "  x2 = " <<  x2 << endl;
    cout << " &x1 = " << &x1 << endl;
    cout << " &x2 = " << &x2 << endl;

    cout << "------------------------------------------------------------>>  2" << endl;

    int * px1(new int(11));
    int * px2(new int(22));

    cout << " px1 = " <<  px1 << endl;
    cout << " px2 = " <<  px2 << endl;
    cout << "&px1 = " << &px1 << endl;
    cout << "&px2 = " << &px2 << endl;
    cout << "*px1 = " << *px1 << endl;
    cout << "*px2 = " << *px2 << endl;

    cout << " px2 = move(px1);" << endl;

    px2 = move(px1);

    cout << " px1 = " <<  px1 << endl;
    cout << " px2 = " <<  px2 << endl;
    cout << "&px1 = " << &px1 << endl;
    cout << "&px2 = " << &px2 << endl;
    cout << "*px1 = " << *px1 << endl;
    cout << "*px2 = " << *px2 << endl;

    delete px1;
    //delete px2;

    cout << "----------------------------------------------------------->> 3" << endl;

    string me("Pavlos");

    // #1

    cout << "printReference(me);" << endl;

    printReference(me);

    // #2

    cout << "printReference(\"Pavlos\");" << endl;

    printReference("Pavlos");

    // #3

    cout << "printReference(getName1());" << endl;

    printReference(getName1());

    cout << "----------------------------------------------------------->> EXIT" << endl;

    return 0;
}

// end
