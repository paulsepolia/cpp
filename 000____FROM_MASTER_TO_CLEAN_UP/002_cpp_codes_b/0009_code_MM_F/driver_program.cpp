//================//
// return schemes //
//================//

#include <iostream>

using std::cout;
using std::cin;
using std::endl;

// function --> f1

const int & f1(const int & k)
{
    cout << k << endl;

    return k;
}

// function --> f2

int & f2(int & k)
{
    cout << k << endl;

    return k;
}

// the main function

int main ()
{
    f1(1);

    int k2 = 2;

    f2(k2);

    cout << " k2 = " << k2 << endl;

    f2(k2) = 10;

    cout << " k2 = " << k2 << endl;

    const int * p1 = &f1(1);

    cout << " -->  p1 = " <<  p1 << endl;

    cout << " --> *p1 = " << *p1 << endl;

    int * p2 = &f2(k2);

    *p2 = 201;

    cout << " k2 = " << k2 << endl;

    return 0;
}

// END
