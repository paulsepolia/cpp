//====================//
// single linked list //
//====================//

#include <iostream>
using std::endl;
using std::cout;

// singh linked list definition

struct node {
    int x;
    node *next;
};

// the main function

int main()
{
    const int NUMS(100);

    node * root;      // This will be the unchanging first node
    root = new node;  // Now root points to a node struct
    node * connection;
    connection = node->next;

    for(int i = 0; i != NUMS; i++) {
        root->next = new node;
        root->x = i;
    }

    cout << " &root       = " << &root << endl;
    cout << " &root->next = " << &root->next << endl;
    cout << " &root->x    = " << &root->x << endl;
    cout << "  root->next = " << root->next << endl;
    cout << "  root->x    = " << root->x << endl;

    // delete the pointer


    return 0;
}

// end
