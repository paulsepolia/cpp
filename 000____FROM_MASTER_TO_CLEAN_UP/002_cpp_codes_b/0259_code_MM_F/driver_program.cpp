//===================//
// XOR with integers //
//===================//

#include <iostream>
using std::endl;
using std::cout;
#include <vector>

int main()
{
    std::vector<int> vec {};
    unsigned int dim(0);

    // # 1

    cout << "---------------------------->> 1" << endl;
    dim = 10;
    vec.clear();
    vec.resize(dim);
    for(unsigned int i = 0; i != vec.size(); i++) {
        vec[i] = i;
    }

    for(unsigned int i = 0; i != vec.size()-1; i++) {
        cout << " vec[i] ^ vec[i+1] = " << vec[i] << " ^ " << vec[i+1] << " = "
             << (vec[i] ^ vec[i+1]) << endl;
    }

    // # 2

    cout << "---------------------------->> 2" << endl;
    dim = 10;
    vec.clear();
    vec.resize(dim);
    for(unsigned int i = 0; i != vec.size(); i++) {
        vec[i] = dim - i;
    }

    for(unsigned int i = 0; i != vec.size()-1; i++) {
        cout << " vec[i] ^ vec[i+1] = " << vec[i] << " ^ " << vec[i+1] << " = "
             << (vec[i] ^ vec[i+1]) << endl;
    }

    // # 3

    cout << "---------------------------->> 3" << endl;
    dim = 6;
    vec.clear();
    vec.resize(dim);

    vec = {3,10,5,25,2,8};

    std::vector<int> vec2 {};

    for(unsigned int i = 0; i != vec.size()-1; i++) {
        for(unsigned int j = i+1; j != vec.size(); j++) {
            vec2.push_back(vec[i] ^ vec[j]);
        }
    }

    for(unsigned int i = 0; i != vec2.size(); i++) {
        cout << " vec2[" << i << "]= " << vec2[i] << endl;
    }

    return 0;
}

// end
