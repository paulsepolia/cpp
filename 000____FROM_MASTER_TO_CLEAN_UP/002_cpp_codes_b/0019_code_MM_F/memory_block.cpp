
#include "memory_block.h"
#include <iostream>
#include <algorithm>

using std::cout;
using std::endl;
using std::copy;

// simple constructor that initializes the resource

MemoryBlock::MemoryBlock(const unsigned long int & length)
    : m_length(length)
    , m_data(new int[length])
{
    cout << " 1 --> inside default constructor" << endl;
    cout << " 1 --> in MemoryBlock(const unsigned long int &), m_length = "
         << m_length << endl;
    cout << " 1 --> in MemoryBlock(const unsigned long int &), m_data = "
         << m_data << endl;
}

// destructor

MemoryBlock::~MemoryBlock()
{
    cout << " 2 --> inside default destructor" << endl;
    cout << " 2 --> in ~MemoryBlock(), m_length = " << m_length << endl;

    if (m_data != 0) {
        cout << " 2 --> deleting resource" << endl;
        // delete the resource
        delete [] m_data;
        m_data = 0;
    }

    cout << " 2 --> in ~MemoryBlock(), m_data = " << m_data << endl;
}

// copy constructor

MemoryBlock::MemoryBlock(const MemoryBlock & other)
    : m_length(other.m_length)
    , m_data(new int[other.m_length])
{
    cout << " 3 --> inside copy construnctor" << endl;
    cout << " 3 --> in MemoryBlock(const MemoryBlock&), m_length = "
         << m_length << endl;
    cout << " 3 --> in MemoryBlock(const MemoryBlock&), m_data = "
         << m_data << endl;
    cout << " 3 --> copying memory block now ..." << endl;
    copy(other.m_data, other.m_data + m_length, m_data);
    cout << " 3 --> end of copy" << endl;
}

// copy assignment operator

MemoryBlock & MemoryBlock::operator = (const MemoryBlock & other)
{
    cout << " 4 --> inside operator = (const MemoryBlock &)" << endl;
    cout << " 4 --> executing the operator = (const MemoryBlock&)" << endl;

    if (this != &other) {
        // Free the existing resource
        delete[] m_data;

        m_length = other.m_length;
        m_data = new int[m_length];
        cout << " 4 --> coping memory block now ..." << endl;
        copy(other.m_data, other.m_data + m_length, m_data);
        cout << " 4 --> end of copy" << endl;
    }

    cout << " 4 --> in operator=(const MemoryBlock&). m_length = " << m_length << endl;
    cout << " 4 --> in operator=(const MemoryBlock&). m_data = " << m_data << endl;

    return *this;
}

// Retrieves the length of the data resource

unsigned long int MemoryBlock::Length() const
{
    return m_length;
}

// Move constructor

MemoryBlock::MemoryBlock(MemoryBlock && other)
    : m_length(0),
      m_data(0)
{
    cout << " 5 --> inside move constructor" << endl;

    // Copy the data pointer and its length from the
    // source object

    cout << " 5 --> moving resource now" << endl;

    m_data = other.m_data;
    m_length = other.m_length;

    cout << " 5 --> in MemoryBlock(MemoryBlock&&). m_length = " << m_length << endl;
    cout << " 5 --> in MemoryBlock(MemoryBlock&&). m_data = " << m_data << endl;

    // Release the data pointer from the source object so that
    // the destructor does not free the memory multiple times

    other.m_data = 0;
    other.m_length = 0;
}

// Move assignment operator

MemoryBlock & MemoryBlock::operator = (MemoryBlock && other)
{
    cout << " 6 --> inside move assignment operator = (MemoryBlock &&)" << endl;

    if (this != &other) {
        // Free the existing resource

        delete [] m_data;

        // Copy the data pointer and its length from the
        // source object

        cout << " 6 --> move/copy data" << endl;

        m_data = other.m_data;
        m_length = other.m_length;

        cout << " 6 --> in move assignment operator = (MemoryBlock&&). m_length = " << m_length << endl;
        cout << " 6 --> in move assignment operator = (MemoryBlock&&). m_data = " << m_data << endl;

        // Release the data pointer from the source object so that
        // the destructor does not free the memory multiple times

        other.m_data = 0;
        other.m_length = 0;
    }

    return *this;
}

// END
