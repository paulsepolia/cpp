#!/bin/bash

  # 1. compile

  g++-5.3.0   -O3                \
              -Wall              \
              -std=gnu++14       \
              driver_program.cpp \
              -o x_gnu_530
