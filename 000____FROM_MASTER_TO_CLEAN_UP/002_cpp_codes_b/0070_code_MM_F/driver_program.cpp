//================//
// parallel count //
//================//

#include <iostream>
#include <vector>
#include <iterator>
#include <numeric>
#include <cmath>

#include <parallel/algorithm>
#include <parallel/settings.h>

using std::cout;
using std::endl;
using std::ostream_iterator;
using std::vector;
using std::pow;

// type definition

typedef unsigned long int uli;

// the main function

int main()
{
    // parallel settings

    __gnu_parallel::_Settings s;
    s.algorithm_strategy = __gnu_parallel::force_parallel;
    __gnu_parallel::_Settings::set(s);

    const int threads_wanted = 4;
    omp_set_dynamic(false);
    omp_set_num_threads(threads_wanted);

    // end of parallel settings

    const uli DIM1 = 2 * uli(pow(10.0, 8.0));
    const uli TRIALS1 = uli(pow(10.0, 2.0));

    vector<double> v1(DIM1);
    vector<double> v2(DIM1);

    cout << " --> build v1" << endl;

    iota(v1.begin(), v1.end(), 0.0);
    iota(v2.begin(), v2.end(), 0.0);

    int c1 = -1;
    int c2 = -1;

    cout << " --> parallel count of v1 in a parallel to v2" << endl;

    for (uli i = 0; i != TRIALS1; i++) {
        c1 = std::__parallel::count(v1.begin(), v1.end(), 1.0);
        c2 = std::__parallel::count(v2.begin(), v2.end(), 1.0);
    }

    cout << " --> some results :" << endl;

    cout << " --> v1[0] = " << v1[0] << endl;
    cout << " --> v1[1] = " << v1[1] << endl;
    cout << " --> v1[2] = " << v1[2] << endl;
    cout << " --> v1[3] = " << v1[3] << endl;

    cout << " --> v2[0] = " << v2[0] << endl;
    cout << " --> v2[1] = " << v2[1] << endl;
    cout << " --> v2[2] = " << v2[2] << endl;
    cout << " --> v2[3] = " << v2[3] << endl;

    cout << " --> v1.size() = " << v1.size() << endl;
    cout << " --> v2.size() = " << v2.size() << endl;

    cout << " --> c1 = " << c1 << endl;
    cout << " --> c2 = " << c2 << endl;

    return 0;
}

// end
