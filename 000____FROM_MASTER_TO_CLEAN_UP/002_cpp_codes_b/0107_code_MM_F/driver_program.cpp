//===========================================//
// INHERITANCE vs NON-INHERITANCE SPEED GAME //
//===========================================//

#include "classesPolymorphic.h"
#include "classesNonPolymorphic.h"

#include <iostream>
#include <iomanip>
#include <chrono>
#include <cmath>

using std::endl;
using std::cout;
using std::setprecision;
using std::fixed;
using namespace std::chrono;
using std::pow;

// type definitions

typedef long long int lli;

//===================//
// the main function //
//===================//

int main()
{
    // local parameters

    const lli DIM = static_cast<lli>(pow(10.0, 10.0));

    // local variables

    double x1;
    auto t1 = system_clock::now();
    auto t2 = system_clock::now();
    duration<double> time_span;
    cout << fixed;
    cout << setprecision(5);

    cout << " --> POLYMORPHIC CALL SPEED TEST --------------------------------> 1" << endl;

    {
        cout << " --> POLYM::A * pa1 = new POLYM::A;" << endl;
        POLYM::A * pa1 = new POLYM::A;
        cout << " --> POLYM::A * pb1 = new POLYM::B;" << endl;
        POLYM::A * pb1 = new POLYM::B;
        cout << " --> POLYM::A * pc1 = new POLYM::C;" << endl;
        POLYM::A * pc1 = new POLYM::C;
        cout << " --> POLYM::A * pd1 = new POLYM::D;" << endl;
        POLYM::A * pd1 = new POLYM::D;

        // timing A

        t1 = system_clock::now();

        for (lli i = 0; i != DIM; i++) {
            pa1->fun();
            x1 = pa1->get();
        }

        cout << " --> A --> x1 = " << x1 << endl;
        t2 = system_clock::now();
        time_span = duration_cast<duration<double>>(t2-t1);
        cout << " --> A --> time used = " << time_span.count() << endl;

        // timing B

        t1 = system_clock::now();

        for (lli i = 0; i != DIM; i++) {
            pb1->fun();
            x1 = pb1->get();
        }

        cout << " --> B --> x1 = " << x1 << endl;
        t2 = system_clock::now();
        time_span = duration_cast<duration<double>>(t2-t1);
        cout << " --> B --> time used = " << time_span.count() << endl;

        // timing C

        t1 = system_clock::now();

        for (lli i = 0; i != DIM; i++) {
            pc1->fun();
            x1 = pc1->get();
        }

        cout << " --> C --> x1 = " << x1 << endl;
        t2 = system_clock::now();
        time_span = duration_cast<duration<double>>(t2-t1);
        cout << " --> C --> time used = " << time_span.count() << endl;

        // timing D

        t1 = system_clock::now();

        for (lli i = 0; i != DIM; i++) {
            pd1->fun();
            x1 = pd1->get();
        }

        cout << " --> D --> x1 = " << x1 << endl;
        t2 = system_clock::now();
        time_span = duration_cast<duration<double>>(t2-t1);
        cout << " --> D --> time used = " << time_span.count() << endl;

        // deallocations

        cout << " --> delete pa1;" << endl;
        delete pa1;
        cout << " --> delete pb1;" << endl;
        delete pb1;
        cout << " --> delete pc1;" << endl;
        delete pc1;
        cout << " --> delete pd1;" << endl;
        delete pd1;

        cout << " --> exit --> 1" << endl;
    }

    cout << " --> NON-POLYMORPHIC CALL SPEED TEST ----------------------------> 2" << endl;

    {
        cout << " --> NONPOLYM::A * pa1 = new NONPOLYM::A;" << endl;
        NONPOLYM::A * pa1 = new NONPOLYM::A;
        cout << " --> NONPOLYM::A * pb1 = new NONPOLYM::B;" << endl;
        NONPOLYM::A * pb1 = new NONPOLYM::B;
        cout << " --> NONPOLYM::A * pc1 = new NONPOLYM::C;" << endl;
        NONPOLYM::A * pc1 = new NONPOLYM::C;
        cout << " --> NONPOLYM::A * pd1 = new NONPOLYM::D;" << endl;
        NONPOLYM::A * pd1 = new NONPOLYM::D;

        // timing A

        t1 = system_clock::now();

        for (lli i = 0; i != DIM; i++) {
            pa1->fun();
            x1 = pa1->get();
        }

        cout << " --> A --> x1 = " << x1 << endl;
        t2 = system_clock::now();
        time_span = duration_cast<duration<double>>(t2-t1);
        cout << " --> A --> time used = " << time_span.count() << endl;

        // timing B

        t1 = system_clock::now();

        for (lli i = 0; i != DIM; i++) {
            pb1->fun();
            x1 = pb1->get();
        }

        cout << " --> B --> x1 = " << x1 << endl;
        t2 = system_clock::now();
        time_span = duration_cast<duration<double>>(t2-t1);
        cout << " --> B --> time used = " << time_span.count() << endl;

        // timing C

        t1 = system_clock::now();

        for (lli i = 0; i != DIM; i++) {
            pc1->fun();
            x1 = pc1->get();
        }

        cout << " --> C --> x1 = " << x1 << endl;
        t2 = system_clock::now();
        time_span = duration_cast<duration<double>>(t2-t1);
        cout << " --> C --> time used = " << time_span.count() << endl;

        // timing D

        t1 = system_clock::now();

        for (lli i = 0; i != DIM; i++) {
            pd1->fun();
            x1 = pd1->get();
        }

        cout << " --> D --> x1 = " << x1 << endl;
        t2 = system_clock::now();
        time_span = duration_cast<duration<double>>(t2-t1);
        cout << " --> D --> time used = " << time_span.count() << endl;

        // deallocations

        cout << " --> delete pa1;" << endl;
        delete pa1;
        cout << " --> delete pb1;" << endl;
        delete pb1;
        cout << " --> delete pc1;" << endl;
        delete pc1;
        cout << " --> delete pd1;" << endl;
        delete pd1;

        cout << " --> exit --> 2" << endl;
    }

    cout << " --> NON-POLYMORPHIC CALL SPEED TEST ----------------------------> 3" << endl;

    {
        cout << " --> NONPOLYM::A * pa1 = new NONPOLYM::A;" << endl;
        NONPOLYM::A * pa1 = new NONPOLYM::A;
        cout << " --> NONPOLYM::B * pb1 = new NONPOLYM::B;" << endl;
        NONPOLYM::B * pb1 = new NONPOLYM::B;
        cout << " --> NONPOLYM::C * pc1 = new NONPOLYM::C;" << endl;
        NONPOLYM::C * pc1 = new NONPOLYM::C;
        cout << " --> NONPOLYM::B * pd1 = new NONPOLYM::D;" << endl;
        NONPOLYM::D * pd1 = new NONPOLYM::D;

        // timing A

        t1 = system_clock::now();

        for (lli i = 0; i != DIM; i++) {
            pa1->fun();
            x1 = pa1->get();
        }

        cout << " --> A --> x1 = " << x1 << endl;
        t2 = system_clock::now();
        time_span = duration_cast<duration<double>>(t2-t1);
        cout << " --> A --> time used = " << time_span.count() << endl;

        // timing B

        t1 = system_clock::now();

        for (lli i = 0; i != DIM; i++) {
            pb1->fun();
            x1 = pb1->get();
        }

        cout << " --> B --> x1 = " << x1 << endl;
        t2 = system_clock::now();
        time_span = duration_cast<duration<double>>(t2-t1);
        cout << " --> B --> time used = " << time_span.count() << endl;

        // timing C

        t1 = system_clock::now();

        for (lli i = 0; i != DIM; i++) {
            pc1->fun();
            x1 = pc1->get();
        }

        cout << " --> C --> x1 = " << x1 << endl;
        t2 = system_clock::now();
        time_span = duration_cast<duration<double>>(t2-t1);
        cout << " --> C --> time used = " << time_span.count() << endl;

        // timing D

        t1 = system_clock::now();

        for (lli i = 0; i != DIM; i++) {
            pd1->fun();
            x1 = pd1->get();
        }

        cout << " --> D --> x1 = " << x1 << endl;
        t2 = system_clock::now();
        time_span = duration_cast<duration<double>>(t2-t1);
        cout << " --> D --> time used = " << time_span.count() << endl;

        // deallocations

        cout << " --> delete pa1;" << endl;
        delete pa1;
        cout << " --> delete pb1;" << endl;
        delete pb1;
        cout << " --> delete pc1;" << endl;
        delete pc1;
        cout << " --> delete pd1;" << endl;
        delete pd1;

        cout << " --> exit --> 3" << endl;
    }

    return 0;
}

// end
