#!/bin/bash

  # 1. compile

  g++-4.9.2 -O3                \
            -Wall              \
            -std=gnu++14       \
		  memory_block.cpp   \
            driver_program.cpp \
            -o x_gnu_492
