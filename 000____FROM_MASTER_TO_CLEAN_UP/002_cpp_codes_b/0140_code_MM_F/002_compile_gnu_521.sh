#!/bin/bash

  # 1. compile

  g++-5  -O3                \
         -Wall              \
         -std=gnu++17       \
	    memory_block.cpp   \
         driver_program.cpp \
         -o x_gnu_521
