
//=========================================//
// linked list with a vector as data member//
//=========================================//

#include <iostream>
#include <cmath>
#include <cstdlib>

using std::endl;
using std::cout;

// the linkList definition

class linkList {
public:

    linkList(): data(0), next(0) {};
    ~linkList() {};
    // the destructor does not contain the delete statement
    // because i delete manualy the vector
    // it is the only way. unless it goes out of scope

    int data;
    linkList * next;
};

// the main function

int main()
{
    // local parameters and variables

    const long NODES_MAX = std::pow(10.0, 5.0);  // number of nodes
    const long J_MAX = std::pow(10.0, 5.0);      // number of trials

    // main for test loop

    linkList * head;
    head = new linkList;
    linkList * node;
    node = new linkList;

    for (long j = 0; j < J_MAX; j++) {
        //cout << "------------------------------------------>> " << j << endl;

        // initailize the first node
        // it is next to the head

        head->next = node;

        // create NODE_MAX number of nodes

        for (long i = 0; i < NODES_MAX; i++) {
            node->data = i;
            node->next = new linkList;
            node = node->next;
        }

        // set the end of nodes as null pointer
        // it is a mark to detect the end node

        node->next = 0;

        // start again

        node = head->next;
        linkList * q;

        while (node->next != 0) {
            q = node->next;
            node->next = q->next;
            delete q;
            q = 0;
        }
    }

    delete node;
    delete head;
}

// END

