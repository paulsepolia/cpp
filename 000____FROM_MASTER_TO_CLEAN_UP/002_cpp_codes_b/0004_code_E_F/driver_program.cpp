
//===========================//
// shuffle algorithm example //
//===========================//

#include <iostream>     // cout
#include <algorithm>    // shuffle, random_shuffle
#include <random>       // default_random_engine
#include <chrono>       // chrono::system_clock

using std::cout;
using std::endl;
using std::shuffle;
using std::random_shuffle;
using std::sort;
using std::default_random_engine;
using std::chrono::system_clock;

// the main function

int main ()
{
    const int DIM = 100;

    int * foo = new int [DIM];

    for (int i = 0; i != DIM; i++) {
        foo[i] = i;
    }

    // obtain a time-based seed

    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();

    // shuffle // uniform random number generator

    shuffle(foo, foo+DIM, default_random_engine(seed));
    sort(foo, foo+DIM);

    // shuffle again

    random_shuffle(foo, foo+DIM);
    sort(foo, foo+DIM);

    cout << " --> shuffled elements: ";

    for (int i = 0; i != DIM; i++) {
        cout << ' ' << foo[i];
    }

    cout << endl;

    return 0;
}

// END
