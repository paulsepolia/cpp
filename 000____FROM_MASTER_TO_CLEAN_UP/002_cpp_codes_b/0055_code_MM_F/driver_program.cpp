//======//
// list //
//======//

#include <iostream>
#include <list>
#include <cmath>
#include <ctime>
#include <iomanip>

using std::endl;
using std::cout;
using std::cin;
using std::list;
using std::pow;
using std::clock;
using std::fixed;
using std::setprecision;

// type definition

typedef unsigned long int uli;

// the main function

int main()
{
    const auto TRIALS = 10;

    for (auto ik = 0; ik != TRIALS; ik++) {

        cout << "--------------------------------------------------->> "
             << ik+1 << " / " << TRIALS << endl;

        // local parameters

        const uli DIM1 = 5 * uli(pow(10.0, 7.0));
        const uli ELEM1 = 3UL;
        const uli ELEM2 = 4UL;

        list<uli> * L1 = new list<uli> [1];
        list<uli> * L2 = new list<uli> [1];

        time_t t1;
        time_t t2;

        // output adjust

        cout << fixed;
        cout << setprecision(10);

        // # 1

        cout << " --> L1[0].assign(DIM1, ELEM1);" << endl;
        cout << " --> L2[0].assign(DIM1, ELEM2);" << endl;

        t1 = clock();

        L1[0].assign(DIM1, ELEM1);
        L2[0].assign(DIM1, ELEM2);

        t2 = clock();

        cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

        // # 2

        cout << " --> L1[0].sort();" << endl;
        cout << " --> L2[0].sort();" << endl;

        t1 = clock();

        L1[0].sort();
        L2[0].sort();

        t2 = clock();

        cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

        // # 3

        cout << " --> L1[0].size() = " << L1[0].size() << endl;
        cout << " --> L2[0].size() = " << L2[0].size() << endl;

        cout << " --> L1[0].merge(L2[0]);" << endl;

        t1 = clock();

        L1[0].merge(L2[0]);

        t2 = clock();

        cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC << endl;

        cout << " --> L1[0].size() = " << L1[0].size() << endl;
        cout << " --> L2[0].size() = " << L2[0].size() << endl;

        delete [] L1;
        delete [] L2;
    }

    int sentinel;
    cin >> sentinel;

    return 0;
}

// end
