//==================//
// INHERITANCE GAME //
//==================//

#include "classesPolymorphic.h"
#include <iostream>

using std::endl;
using std::cout;

//===================//
// the main function //
//===================//

int main()
{
    // local variables

    cout << " ----------------------------------------------------------------> 1" << endl;

    {
        cout << " --> A a1;" << endl;
        A a1;
        cout << " --> B b1;" << endl;
        B b1;
        cout << " --> C c1;" << endl;
        C c1;
        cout << " --> D d1;" << endl;
        D d1;

        cout << " --> exit --> 1" << endl;
    }

    cout << " ----------------------------------------------------------------> 2" << endl;

    {
        cout << " --> A a1;" << endl;
        A a1;
        cout << " --> a1.fun();" << endl;
        a1.fun();
        cout << " --> B b1;" << endl;
        B b1;
        cout << " --> b1.fun();" << endl;
        b1.fun();
        cout << " --> C c1;" << endl;
        C c1;
        cout << " --> c1.fun();" << endl;
        c1.fun();
        cout << " --> D d1;" << endl;
        D d1;
        cout << " --> d1.fun();" << endl;
        d1.fun();

        cout << " --> exit --> 2" << endl;
    }

    cout << " ----------------------------------------------------------------> 3" << endl;

    {
        cout << " --> A * pa1 = new D;" << endl;
        A * pa1 = new D;
        cout << " --> pa1->fun();" << endl;
        pa1->fun();
        cout << " --> B * pb1 = new D;" << endl;
        B * pb1 = new D;
        cout << " --> pb1->fun();" << endl;
        pb1->fun();
        cout << " --> C * pc1 = new D;" << endl;
        C * pc1 = new D;
        cout << " --> pc1->fun();" << endl;
        pc1->fun();
        cout << " --> D * pd1 = new D;" << endl;
        D * pd1 = new D;
        cout << " --> pd1->fun();" << endl;
        pd1->fun();

        cout << " --> delete pa1;" << endl;
        delete pa1;
        cout << " --> delete pb1;" << endl;
        delete pb1;
        cout << " --> delete pc1;" << endl;
        delete pc1;
        cout << " --> delete pd1;" << endl;
        delete pd1;
    }

    cout << " ----------------------------------------------------------------> 4" << endl;

    {
        cout << " --> A * pa1 = new B;" << endl;
        A * pa1 = new B;
        cout << " --> pa1->fun();" << endl;
        pa1->fun();
        cout << " --> B * pb1 = new C;" << endl;
        B * pb1 = new C;
        cout << " --> pb1->fun();" << endl;
        pb1->fun();
        cout << " --> C * pc1 = new D;" << endl;
        C * pc1 = new D;
        cout << " --> pc1->fun();" << endl;
        pc1->fun();
        cout << " --> D * pd1 = new D;" << endl;
        D * pd1 = new D;
        cout << " --> pd1->fun();" << endl;
        pd1->fun();

        cout << " --> delete pa1;" << endl;
        delete pa1;
        cout << " --> delete pb1;" << endl;
        delete pb1;
        cout << " --> delete pc1;" << endl;
        delete pc1;
        cout << " --> delete pd1;" << endl;
        delete pd1;
    }

    cout << " ----------------------------------------------------------------> 5" << endl;

    {
        cout << " --> A * pa1 = new A;" << endl;
        A * pa1 = new A;
        cout << " --> pa1->fun();" << endl;
        pa1->fun();
        cout << " --> B * pb1 = new B;" << endl;
        B * pb1 = new B;
        cout << " --> pb1->fun();" << endl;
        pb1->fun();
        cout << " --> C * pc1 = new C;" << endl;
        C * pc1 = new C;
        cout << " --> pc1->fun();" << endl;
        pc1->fun();
        cout << " --> D * pd1 = new D;" << endl;
        D * pd1 = new D;
        cout << " --> pd1->fun();" << endl;
        pd1->fun();

        cout << " --> delete pa1;" << endl;
        delete pa1;
        cout << " --> delete pb1;" << endl;
        delete pb1;
        cout << " --> delete pc1;" << endl;
        delete pc1;
        cout << " --> delete pd1;" << endl;
        delete pd1;
    }

    cout << " -------------------------------------------------------------------> end" << endl;

    return 0;
}

// end
