//=============//
// memory leak //
//=============//

#include <iostream>
#include <cmath>

using std::cout;
using std::cin;
using std::endl;
using std::pow;

// type definition

typedef unsigned long int uli;

// global parameters

const uli DIM = static_cast<uli>(pow(10.0, 2.0));
const uli DIM_B = 4*static_cast<uli>(pow(10.0, 6.0));

// functions

int * f1()
{
    int * p1;
    return  (p1 = new int);
}

// the main function

int main ()
{

    f1();

    // sentineling

//     int sentinel;
//     cin >> sentinel;

    return 0;
}

// END
