#include <iostream>
#include <vector>
#include <unordered_map>
#include <chrono>
#include <algorithm>
#include <map>
#include <iomanip>

class benchmark_timer {
public:

    explicit benchmark_timer(const double &res) :
            _res(res),
            t1{std::chrono::steady_clock::now()},
            t2{std::chrono::steady_clock::now()} {};

    ~benchmark_timer() {
        t2 = std::chrono::steady_clock::now();
        const auto total_time{std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count()};
        std::cout << " --> res = " << _res << std::endl;
        std::cout << " --> time used (secs) = " << total_time << std::endl;
    }

    auto set_res(const double &res) -> void {
        _res = res;
    }

private:

    double _res{};
    decltype(std::chrono::steady_clock::now()) t1{};
    decltype(std::chrono::steady_clock::now()) t2{};
};

auto main() -> int {

    std::cout << std::fixed;
    std::cout << std::setprecision(5);

    const auto DIM{(int32_t) 100};
    const auto DO_MAX{(size_t) 100'000'000};

    {
        std::cout << " --> std::vector<int32_t>" << std::endl;
        auto v{std::vector<int32_t>()};
        v.reserve(DIM);
        v.resize(DIM);

        for (int32_t i = 0; i < DIM; i++) {
            v[i] = DIM - i;
        }

        v.shrink_to_fit();
        double tot = 0;
        benchmark_timer ot(0);

        for (size_t i = 0; i < DO_MAX; i++) {
            const auto res = std::find(v.begin(), v.end(), i % DIM);
            if (res != v.end()) {
                tot++;
            }
        }

        ot.set_res(tot);
    }

    {
        std::cout << " --> std::unordered_map<int32_t, int32_t> --> search by key" << std::endl;

        auto m{std::unordered_map<int32_t, int32_t>()};
        m.reserve(DIM);

        for (int32_t i = 0; i < DIM; i++) {
            m[i] = DIM - i;
        }

        double tot = 0;
        benchmark_timer ot(0);

        for (size_t i = 0; i < DO_MAX; i++) {
            const auto res = m.find((i % DIM) + 1);
            if (res != m.end()) {
                tot++;
            }
        }

        ot.set_res(tot);
    }

    {
        std::cout << " --> std::unordered_map<int32_t, int32_t> --> search by value" << std::endl;

        auto m{std::unordered_map<int32_t, int32_t>()};
        m.reserve(DIM);

        for (int32_t i = 0; i < DIM; i++) {
            m[i] = DIM - i;
        }

        double tot = 0;
        benchmark_timer ot(0);

        for (size_t i = 0; i < DO_MAX; i++) {

            const auto val{(int32_t) i % DIM};

            const auto res = std::find_if(m.begin(), m.end(), [val](const auto &mo) {
                return mo.second == val;
            });

            if (res != m.end()) {
                tot++;
            }
        }

        ot.set_res(tot);
    }

    {
        std::cout << " --> std::map<int32_t, int32_t> --> search by key" << std::endl;

        auto m{std::map<int32_t, int32_t>()};

        for (int32_t i = 0; i < DIM; i++) {
            m[i] = DIM - i;
        }

        double tot = 0;
        benchmark_timer ot(0);

        for (size_t i = 0; i < DO_MAX; i++) {
            const auto res = m.find((i % DIM) + 1);
            if (res != m.end()) {
                tot++;
            }
        }

        ot.set_res(tot);
    }

    {
        std::cout << " --> std::map<int32_t, int32_t> --> search by value" << std::endl;

        auto m{std::map<int32_t, int32_t>()};

        for (int32_t i = 0; i < DIM; i++) {
            m[i] = DIM - i;
        }

        double tot = 0;
        benchmark_timer ot(0);

        for (size_t i = 0; i < DO_MAX; i++) {

            const auto val{(int32_t) i % DIM};

            const auto res = std::find_if(m.begin(), m.end(), [val](const auto &mo) {
                return mo.second == val;
            });

            if (res != m.end()) {
                tot++;
            }
        }

        ot.set_res(tot);
    }
}
