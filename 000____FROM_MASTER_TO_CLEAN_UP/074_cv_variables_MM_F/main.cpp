// condition_variable example

#include <iostream>
#include <thread>
#include <mutex>
#include <condition_variable>

constexpr auto NT{(size_t) 10};

auto mtx{std::mutex{}};
auto cv{std::condition_variable{}};
auto ready{(bool) false};

auto print_id(uint32_t id) -> void {

    std::unique_lock<std::mutex> lck(mtx);

    std::cout << " --> thread --> before --> thread id = " << id << std::endl;

    while (!ready) {
        std::cout << " --> I am a thread waiting here --> thread id = " << id << std::endl;
        cv.wait(lck);
        std::cout << " --> I am a thread and NOT waiting anymore --> thread id = " << id << std::endl;
    }

    std::cout << " --> thread --> after --> " << id << std::endl;
}

auto go() -> void {

    std::cout << " --> I am in go() function now..." << std::endl;

    std::unique_lock<std::mutex> lck(mtx);

    ready = true;

    std::cout << " --> I am in go() function now... below lock" << std::endl;

    std::this_thread::sleep_for(std::chrono::seconds(1));

    std::cout << " --> mark --> notify_one() --> 1" << std::endl;

    cv.notify_one();

    std::this_thread::sleep_for(std::chrono::seconds(1));

    std::cout << " --> mark --> notify_one() --> 2" << std::endl;

    cv.notify_one();
}

auto go_again() -> void {

    std::cout << " --> I am in go_again() function now..." << std::endl;

    std::unique_lock<std::mutex> lck(mtx);

    ready = true;

    std::cout << " --> waiting 20 seconds to notify all..." << std::endl;

    std::this_thread::sleep_for(std::chrono::seconds(20));

    cv.notify_all();
}

int main() {

    std::thread threads[NT];

    // spawn 10 threads:

    for (size_t i = 0; i < NT; i++) {
        threads[i] = std::thread(print_id, i);
    }

    std::cout << " --> sleep the main thread here to give time all the spawn threads to reach the 'wait'..."
              << std::endl;

    std::this_thread::sleep_for(std::chrono::seconds(2));

    std::cout << "10 threads ready to race..." << std::endl;

    std::cout << " --> go() --> 1" << std::endl;

    go();

    std::cout << " --> go() --> 2" << std::endl;

    go();

    std::cout << " --> go_all() --> 1" << std::endl;

    go_again();

    for (auto &th : threads) {
        th.join();
    }
}