//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <string>

class Solution {
public:

    // lengthOfLastWord_v1
    int lengthOfLastWord_v1(std::string);
};

#endif

// end
