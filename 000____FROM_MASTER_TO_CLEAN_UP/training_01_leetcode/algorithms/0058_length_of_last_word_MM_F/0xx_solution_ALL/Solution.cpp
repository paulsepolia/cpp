//===========================//
// Solution class definition //
//===========================//

#include <algorithm>
#include "Solution.hpp"

// lengthOfLastWord_v1
int Solution::lengthOfLastWord_v1(std::string s)
{
    int res(0);
    std::reverse(s.begin(), s.end());

    // corner case
    if(s.size() == 0) {
        res = 0;
        return res;
    }

    // get possible blank spaces
    unsigned int count_a(0);
    for(unsigned int i = 0; i != s.size(); i++) {
        if(s[i] == ' ') {
            count_a++;
        } else {
            break;
        }
    }

    // get the first word
    int count(0);
    for(unsigned int i = count_a; i != s.size(); i++) {
        if(s[i] != ' ') {
            count++;
        } else {
            break;
        }
    }

    res = count;

    return res;
}

// end
