//=================//
// valid palidrome //
//=================//

#include <iomanip>
#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// isPalindrome_v1

#define isPalindrome isPalindrome_v1

// the main function

int main()
{
    Solution sol1;
    bool res(false);
    cout << std::boolalpha;

    {
        // # 1
        cout << "------------------------------------>> 1" << endl;
        std::string str("A man, a plan, a canal: Panama");
        res = sol1.isPalindrome(str);
        cout << " res = " << res << endl;
    }

    {
        // # 2
        cout << "------------------------------------>> 2" << endl;
        std::string str("race a car");
        res = sol1.isPalindrome(str);
        cout << " res = " << res << endl;
    }
}

// end
