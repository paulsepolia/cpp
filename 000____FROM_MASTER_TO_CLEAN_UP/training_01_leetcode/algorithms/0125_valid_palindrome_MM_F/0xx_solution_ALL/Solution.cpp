//===========================//
// Solution class definition //
//===========================//

#include "Solution.hpp"
#include <vector>
#include <algorithm>

// isPalindrome_v1

bool Solution::isPalindrome_v1(std::string s)
{
    // drop spaces and non alpanumeric
    std::vector<char> vec1{};
    for(auto i: s) {
        if(isalpha(i) || isdigit(i)) {
            vec1.push_back(tolower(i));
        }
    }
    std::vector<char> vec2(vec1);
    std::reverse(vec2.begin(), vec2.end());
    return vec1 == vec2;
}

// end
