//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <string>

class Solution {
public:

    // canWinNim_v1
    bool canWinNim_v1(int);
};

#endif

// end
