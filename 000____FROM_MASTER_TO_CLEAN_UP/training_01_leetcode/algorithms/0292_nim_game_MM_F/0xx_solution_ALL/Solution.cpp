//===========================//
// Solution class definition //
//===========================//

#include "Solution.hpp"

// canWinNim_v1

bool Solution::canWinNim_v1(int n)
{
    bool res(false);

    if(n%4 == 0) {
        res = false;
    } else {
        res = true;
    }

    return res;
}

// end
