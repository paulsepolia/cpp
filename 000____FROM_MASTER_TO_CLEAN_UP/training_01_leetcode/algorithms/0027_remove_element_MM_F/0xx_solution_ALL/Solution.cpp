//===========================//
// Solution class definition //
//===========================//

#include <algorithm>
#include "Solution.hpp"

// # 1

int Solution::remove_element_v1(std::vector<int>& nums, int val)
{
    nums.erase(std::remove(nums.begin(), nums.end(), val), nums.end());

    return static_cast<int>(nums.size());
}

// # 2

int Solution::remove_element_v2(std::vector<int>& nums, int val)
{
    std::vector<int>::iterator pos;

    while(pos != nums.end()) {
        pos = std::find(nums.begin(), nums.end(), val);

        if (pos != nums.end()) {
            nums.erase(pos);
        }
    }

    return static_cast<int>(nums.size());
}

//=====//
// end //
//=====//
