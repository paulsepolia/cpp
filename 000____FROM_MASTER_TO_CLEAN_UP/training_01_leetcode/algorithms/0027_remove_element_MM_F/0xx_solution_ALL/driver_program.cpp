//================//
// remove element //
//================//

#include <iostream>

using std::cout;
using std::endl;

#include "Solution.hpp"

// macros
// allowed names are:

#define remove_element remove_element_v2

// the main function

int main()
{
    std::vector<int> my_vec {};
    Solution sol1;
    int val(0);

    // # 1

    my_vec.clear();

    for(int i = 1; i <= 20; i++) {
        my_vec.push_back(i);
    }

    val = sol1.remove_element(my_vec, 1);
    cout << " --> 1 --> val = " << val << endl;

    // # 2

    my_vec.clear();

    for(int i = 1; i <= 20; i++) {
        my_vec.push_back(20);
    }

    val = sol1.remove_element(my_vec, 20);
    cout << " --> 2 --> val = " << val << endl;

    // # 3

    my_vec.clear();

    for(int i = 1; i <= 20; i++) {
        my_vec.push_back(20);
    }

    val = sol1.remove_element(my_vec, 21);
    cout << " --> 3 --> val = " << val << endl;

    // # 4

    my_vec.clear();

    for(int i = 1; i <= 20; i++) {
        my_vec.push_back(i);
    }

    for(int i = 1; i <= 20; i++) {
        val = sol1.remove_element(my_vec, i);
        cout << " --> 4 --> " << i << " --> val = " << val << endl;
    }

    return 0;
}

//=====//
// end //
//=====//
