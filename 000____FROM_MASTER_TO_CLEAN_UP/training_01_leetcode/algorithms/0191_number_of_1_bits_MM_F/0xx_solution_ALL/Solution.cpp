//===========================//
// Solution class definition //
//===========================//

#include <bitset>
#include <string>
#include <algorithm>
#include "Solution.hpp"

// hammingWeight_v1

int Solution::hammingWeight_v1(unsigned int n)
{
    std::string binary(std::bitset<32>(n).to_string());
    int counter(0);
    for(const auto & i : binary) {
        if(i == '1') {
            counter++;
        }
    }

    return counter;
}

// end
