//===========================//
// Solution class definition //
//===========================//

#include <algorithm>

using std::transform;
using std::min_element;

#include "Solution.hpp"

// operator to get the length of a string

int op_get_length(string & str)
{
    return static_cast<int>(str.length());
}

// longestCommonPrefix definition

string Solution::longestCommonPrefix_v1(vector<string>& vec_in)
{
    // --> step # 1
    // --> check if the vector is empty or not
    // --> if it is an empty vector return an empty string

    const unsigned int VEC_SIZE(vec_in.size());
    const string STR_EMPTY("");

    if(VEC_SIZE == 0) {
        return STR_EMPTY;
    }

    // --> step # 2
    // --> check if the vector has size one

    if(VEC_SIZE == 1) {
        return vec_in[0];
    }

    // --> step # 3
    // --> get the minimum common size for each string in the vector

    vector<int> vec_help;
    vec_help.clear();
    vec_help.shrink_to_fit();
    vec_help.resize(VEC_SIZE);

    transform(vec_in.begin(), vec_in.end(), vec_help.begin(), op_get_length);

    const int MIN_LEN(*min_element(vec_help.begin(), vec_help.end()));

    // --> step # 4
    // --> check if the minimum size is zero
    // --> if it is zero return an empty string

    if(MIN_LEN == 0) {
        return STR_EMPTY;
    }

    // --> step # 5
    // --> loop over each string in the vector, compare it with the next one
    // --> and get the common prefix of each pair
    // --> and put in in the help vector

    string str_common(STR_EMPTY);
    int com_len(0);

    // clean the vec_help

    vec_help.clear();
    vec_help.shrink_to_fit();
    vec_help.resize(VEC_SIZE-1);

    for(unsigned int i = 0; i != VEC_SIZE-1; i++) {
        for(int j = 0; j != MIN_LEN; j++) {
            if(vec_in[i][j] != vec_in[i+1][j]) {
                com_len = j-1;
                vec_help[i] = com_len;
                break;
            } else {
                com_len = j;
                vec_help[i] = com_len;
            }
        }
    }

    // --> step # 6
    // --> and get the minimum element of the vec1 help vector
    // --> if it is zero return an empty string

    const int MIN_COMM_LEN(*min_element(vec_help.begin(), vec_help.end()));

    if(MIN_COMM_LEN == -1) {
        return STR_EMPTY;
    }

    // --> step # 7
    // --> get the common part

    string str_res(vec_in[0].substr(0, MIN_COMM_LEN+1));

    return str_res;
}

//=====//
// end //
//=====//
