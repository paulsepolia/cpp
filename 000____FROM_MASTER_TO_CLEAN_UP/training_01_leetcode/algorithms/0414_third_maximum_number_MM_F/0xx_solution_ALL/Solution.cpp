//===========================//
// Solution class definition //
//===========================//

#include <algorithm>
#include "Solution.hpp"

// thirdMax_v1

int Solution::thirdMax_v1(std::vector<int> & nums)
{
    // sort the array
    std::sort(nums.begin(), nums.end());

    // unique the array
    auto it = std::unique(nums.begin(), nums.end());
    nums.resize(std::distance(nums.begin(), it));

    // reverse
    std::reverse(nums.begin(), nums.end());

    // check the size
    const unsigned int DIM2(nums.size());
    int res(0);

    // return here

    if(DIM2 >= 3) {
        res = nums[2];
    } else {
        res = *std::max_element(nums.begin(), nums.end());
    }

    return res;
}

// thirdMax_v2

int Solution::thirdMax_v2(std::vector<int> & nums)
{
    int res(0);

    // get the size
    const unsigned int DIM(nums.size());

    // check the size
    if(DIM <= 2) {
        res = *std::max_element(nums.begin(), nums.end());
        return res;
    }

    // get a possible max element
    int max_loc(nums[0]);
    int tmp(0);

    // 1st round
    for(unsigned int i = 0; i != DIM-1; i++) {
        tmp = nums[i+1];

        if(tmp > max_loc) {
            max_loc = tmp;
        }
    }

    // this is the max element
    int max1(max_loc);

    // 2nd round
    // get a possible max element but not the max1
    for(unsigned int i = 0; i != DIM; i++) {
        if(max1 != nums[i]) {
            max_loc = nums[i];
            break;
        }
    }

    tmp = 0;

    // get the max element but not the max1
    for(unsigned int i = 0; i != DIM-1; i++) {
        tmp = nums[i+1];

        if(tmp > max_loc && tmp != max1) {
            max_loc = tmp;
        }
    }

    // this is the 2nd max element
    int max2(max_loc);

    // 3rd round
    // get a possible max element but not the max1 or max2
    for(unsigned int i = 0; i != DIM; i++) {
        if(max2 != nums[i] && max1 != nums[i]) {
            max_loc = nums[i];
            break;
        } else {
            max_loc = max1;
        }
    }

    tmp = 0;

    // get the max element but not max1 and max2
    for(unsigned int i = 0; i != DIM-1; i++) {
        tmp = nums[i+1];
        if(tmp > max_loc && tmp != max2 && tmp != max1 ) {
            max_loc = tmp;
        }
    }

    if(max2 != max1) {
        return max_loc;
    }

    return max1;
}

// end
