//===========================//
// Solution class definition //
//===========================//

#include <algorithm>
#include "Solution.hpp"

// titleToNumber_v1

int Solution::titleToNumber_v1(std::string s)
{
    int res(0);
    const unsigned int DIM(s.size());
    std::reverse(s.begin(), s.end());
    for(unsigned int i = 0; i != DIM; i++) {
        res = res + (s[i]-'A'+1)*pow(26.0, i);
    }

    return res;
}

// end
