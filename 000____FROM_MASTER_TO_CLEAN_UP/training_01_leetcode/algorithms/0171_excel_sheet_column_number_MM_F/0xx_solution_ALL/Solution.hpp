//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <string>

class Solution {
public:

    // titleToNumber_v1
    int titleToNumber_v1(std::string);
};

#endif

// end
