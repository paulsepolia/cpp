//===========================//
// Solution class definition //
//===========================//

#include <algorithm>
#include "Solution.hpp"

// rotate_v1

void Solution::rotate_v1(std::vector<int>& nums, int k)
{
    const int DIM(nums.size());
    if(k > DIM) {
        k = k%DIM;
    }
    std::reverse(nums.begin(), nums.end());
    std::rotate(nums.begin(), nums.begin()+k, nums.end());
    std::reverse(nums.begin(), nums.end());
}

// end
