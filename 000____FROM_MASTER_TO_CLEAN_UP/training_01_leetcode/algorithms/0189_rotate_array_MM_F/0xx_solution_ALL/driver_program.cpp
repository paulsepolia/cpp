//==============//
// rotate array //
//==============//

#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// rotate_v1

#define rotate rotate_v1

// the main function

int main()
{
    Solution sol1;
    std::vector<int> nums{1,2,3,4,5,6,7};
    int k(4);
    sol1.rotate(nums, k);
    for(const auto & i: nums) {
        cout << i ;
    }
    cout << endl;

    return 0;
}

// end
