//===========================//
// Solution class definition //
//===========================//

#include <algorithm>

using std::merge;

#include "Solution.hpp"

// medianTwoSortedArraysSTL_v1

double Solution::medianTwoSortedArraysSTL_v1(vector<int> & nums1, vector<int> & nums2)
{
    // get the sizes of the arrays

    const unsigned int SIZE1(nums1.size());
    const unsigned int SIZE2(nums2.size());

    // declare and allocate space for the merged sorted array

    vector<int> nums_all {};
    nums_all.resize(SIZE1+SIZE2);
    merge(nums1.begin(), nums1.end(), nums2.begin(), nums2.end(), nums_all.begin());

    // get the median

    double res(0);

    if((SIZE1+SIZE2)%2 == 0) {
        res = nums_all[(SIZE1+SIZE2)/2-1] + nums_all[(SIZE1+SIZE2)/2];
        res = res/2;
    } else if((SIZE1+SIZE2)%2 == 1) {
        res = nums_all[(SIZE1+SIZE2)/2];
    }

    return res;
}

//=====//
// end //
//=====//
