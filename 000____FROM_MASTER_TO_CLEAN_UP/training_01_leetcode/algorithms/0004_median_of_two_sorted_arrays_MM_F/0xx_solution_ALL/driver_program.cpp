//=========//
// two sum //
//=========//

#include <iostream>
#include <iomanip>
#include <cmath>
#include <chrono>

using std::cout;
using std::endl;
using std::setw;
using std::fixed;
using std::setprecision;
using std::right;
using std::pow;
using namespace std::chrono;

#include "Solution.hpp"

// macros
// allowed names are:
// medianTwoSortedArraysSTL_v1

#define medianFun medianTwoSortedArraysSTL_v1

// the main function

int main()
{
    // parameters

    const unsigned int SIZE_VEC1(static_cast<int>(pow(10.0, 4)));
    const unsigned int SIZE_VEC2(static_cast<int>(pow(10.0, 5)));
    const int TRIALS(static_cast<int>(pow(10.0, 5)));

    // input fixed and local variables

    cout << setprecision(10);
    Solution sol1;
    auto t1 = system_clock::now();
    auto t2 = system_clock::now();
    duration<double> time_span;
    vector<int> nums1 {};
    vector<int> nums2 {};
    double res(0);

    cout << fixed;
    cout << setprecision(5);

    // input # 1

    nums1.resize(2);
    nums2.resize(3);

    nums1 = {1, 2};
    nums2 = {3, 4, 5};

    res = sol1.medianFun(nums1, nums2);

    cout << " --> res = " << setw(10) << res << endl;

    // input # 7
    // the benchmark

    // clear the vectors

    nums1.clear();
    nums2.clear();

    nums1.shrink_to_fit();
    nums2.shrink_to_fit();

    // resize the vectors

    nums1.resize(SIZE_VEC1);
    nums2.resize(SIZE_VEC2);

    // build the vectors

    for(unsigned int i = 0; i != SIZE_VEC1; i++) {
        nums1[i] = i+1;
    }

    for(unsigned int i = 0; i != SIZE_VEC2; i++) {
        nums2[i] = SIZE_VEC1+i+1;
    }

    // start the clock

    t1 = system_clock::now();

    // benchmark here

    for(int k = 0; k != TRIALS; k++) {
        res = sol1.medianFun(nums1, nums2);
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);

    cout << " --> res = " << setw(10) << res << endl;
    cout << " --> total time used for " << TRIALS << " applications of the algorithm is "
         << fixed << time_span.count() << endl;

    return 0;
}

//=====//
// end //
//=====//
