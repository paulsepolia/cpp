//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

// ListNode definition

struct ListNode {
    int val;
    ListNode * next;
    ListNode(int x) : val(x), next(0) {}
};

// Solution declaration

class Solution {
public:

    // sortList_v1
    ListNode * sortList_v1(ListNode *);

    // sortList_v2
    ListNode * sortList_v2(ListNode *);
};

#endif

// end
