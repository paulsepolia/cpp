//===================================//
// remove element from a linked list //
//===================================//

#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// sortList_v1
// sortList_v2

#define sortList sortList_v1

// the main function

int main()
{
    ListNode * nA(0);
    ListNode * res(0);
    Solution sol1;
    res = sol1.sortList(nA);

    cout << " &res = " << &res << endl;

    return 0;
}

// end
