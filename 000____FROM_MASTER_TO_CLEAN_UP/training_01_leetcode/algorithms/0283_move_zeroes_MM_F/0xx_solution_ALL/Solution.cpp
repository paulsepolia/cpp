//===========================//
// Solution class definition //
//===========================//

#include "Solution.hpp"
#include <iostream>
using std::cout;
using std::endl;

// moveZeroes_v1

void Solution::moveZeroes_v1(std::vector<int> & nums)
{
    // get the size of the vector

    const unsigned int SIZE(nums.size());

    // iterate through the vector
    // if you find a zero erase it
    // and count it

    int counter(0);
    for(unsigned int i = 0; i != SIZE; i++) {
here:
        if(nums[i] == 0) {
            nums.erase(nums.begin()+i);
            nums.push_back(9);
            counter++;
            goto here;
        }
    }

    // put at the end zeroes

    nums.erase(nums.begin()+SIZE-counter, nums.end());
    for(int i = 0; i != counter; i++) {
        nums.push_back(0);
    }
}

// moveZeroes_v2

void Solution::moveZeroes_v2(std::vector<int> & nums)
{
    // get the size of the vector

    const unsigned int SIZE(nums.size());
    std::vector<int> tmp{};

    // iterate through the vector

    for(unsigned int i = 0; i != SIZE; i++) {
        if(nums[i] != 0) {
            tmp.push_back(nums[i]);
        }
    }

    // put at the end zeroes

    const unsigned int SIZE2(tmp.size());

    for(unsigned int i = 0; i != SIZE-SIZE2; i++) {
        tmp.push_back(0);
    }

    nums = tmp;
}

// end
