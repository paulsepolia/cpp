//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <vector>

class Solution {
public:

    // moveZeroes_v1
    void moveZeroes_v1(std::vector<int> &);

    // moveZeroes_v2
    void moveZeroes_v2(std::vector<int> &);
};

#endif

// end
