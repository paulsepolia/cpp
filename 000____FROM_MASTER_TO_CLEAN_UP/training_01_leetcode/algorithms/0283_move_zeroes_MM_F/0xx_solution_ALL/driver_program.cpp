//=============//
// Move Zeroes //
//=============//

#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// moveZeroes_v1

#define moveZeroes moveZeroes_v2

// the main function

int main()
{
    Solution sol1;

    // # 1
    {
        cout << "------------------------------------>> 1" << endl;
        std::vector<int> nums = {0,1,2,3,4,5,6,7,8,9};

        for(const auto & i: nums) {
            cout << i << " ";
        }
        cout << endl;
        sol1.moveZeroes(nums);
        for(const auto & i: nums) {
            cout << i << " ";
        }
        cout << endl;
    }

    // # 2
    {
        cout << "------------------------------------>> 2" << endl;
        std::vector<int> nums = {0,0,0,3,0,0,0,0,8,0};

        for(const auto & i: nums) {
            cout << i << " ";
        }
        cout << endl;
        sol1.moveZeroes(nums);
        for(const auto & i: nums) {
            cout << i << " ";
        }
        cout << endl;
    }

    // # 3
    {
        cout << "------------------------------------>> 2" << endl;
        std::vector<int> nums = {1,2,3,4,5,6,7,8,9,9};

        for(const auto & i: nums) {
            cout << i << " ";
        }
        cout << endl;
        sol1.moveZeroes(nums);
        for(const auto & i: nums) {
            cout << i << " ";
        }
        cout << endl;
    }



    return 0;
}

// end
