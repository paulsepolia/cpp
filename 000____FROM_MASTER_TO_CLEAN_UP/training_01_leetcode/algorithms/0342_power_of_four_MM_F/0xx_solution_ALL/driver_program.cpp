//==============//
// power of two //
//==============//

#include <iomanip>
#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// isPowerOfFour_v1

#define isPowerOfFour isPowerOfFour_v1

// the main function

int main()
{
    Solution sol1;
    bool res(false);
    int num(0);

    cout << std::fixed;
    cout << std::boolalpha;

    // # 1

    num = 3;
    res = sol1.isPowerOfFour(num);
    cout << " num = " << num << endl;
    cout << " res = " << res << endl;

    // # 2

    num = 9;
    res = sol1.isPowerOfFour(num);
    cout << " num = " << num << endl;
    cout << " res = " << res << endl;

    // # 3

    num = -16;
    res = sol1.isPowerOfFour(num);
    cout << " num = " << num << endl;
    cout << " res = " << res << endl;

    // # 4

    num = 16;
    res = sol1.isPowerOfFour(num);
    cout << " num = " << num << endl;
    cout << " res = " << res << endl;

    return 0;
}

// end
