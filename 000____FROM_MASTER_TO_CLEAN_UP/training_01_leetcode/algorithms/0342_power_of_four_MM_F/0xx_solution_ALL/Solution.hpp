//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

class Solution {
public:

    //  isPowerOfFour_v1
    bool isPowerOfFour_v1(int);
};

#endif

// end
