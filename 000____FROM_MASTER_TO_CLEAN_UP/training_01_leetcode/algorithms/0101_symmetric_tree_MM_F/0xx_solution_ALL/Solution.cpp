//===========================//
// Solution class definition //
//===========================//

#include <queue>
#include "Solution.hpp"

// isSymmetric_v1

bool Solution::isSymmetric_v1(TreeNode * root)
{
    // use single queue

    if(!root) {
        return true;
    }

    std::queue<TreeNode *> q;

    q.push(root->left);
    q.push(root->right);

    TreeNode *l;
    TreeNode *r;

    while(!q.empty()) {

        // get the left and right TreeNode pointers

        l = q.front();
        q.pop();
        r = q.front();
        q.pop();

        // if they are null recycle

        if(l == 0 && r == 0) {
            continue;
        }

        // if not both null or with different values
        // then the tree is not symmetric

        if(l == 0 || r == 0 || l->val != r->val) {
            return false;
        }

        // else

        q.push(l->left);
        q.push(r->right);
        q.push(l->right);
        q.push(r->left);
    }

    return true;
}

// isSymmetric_v2
// # a

bool isMirror(TreeNode *a, TreeNode *b)
{
    return (a && b) ?
           (a->val == b->val &&
            isMirror(a->left,b->right) &&
            isMirror(a->right,b->left)) :
           (a == b);
}

// # b

bool Solution::isSymmetric_v2(TreeNode* root)
{
    if (root == 0) {
        return true;
    }
    return isMirror(root->left, root->right);
}

// isSymmetric_v3
// # a

bool compairTwoNodes(TreeNode* leftNode, TreeNode* rightNode)
{
    if(leftNode == 0 && rightNode == 0) {
        return true;
    }
    if(leftNode == 0 || rightNode == 0) {
        return false;
    }
    if(leftNode->val != rightNode->val) {
        return false;
    }

    bool val(compairTwoNodes(leftNode->left,rightNode->right) &&
             compairTwoNodes(leftNode->right,rightNode->left));

    return val;
}

// # b

bool Solution::isSymmetric(TreeNode* root)
{
    if(root == 0) {
        return true;
    }

    bool val(compairTwoNodes(root->left,root->right));

    return val;
}

// end
