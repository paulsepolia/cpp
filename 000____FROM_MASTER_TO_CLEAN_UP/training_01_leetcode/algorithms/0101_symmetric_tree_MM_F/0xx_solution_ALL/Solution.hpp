//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

// Definition for a binary tree node

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(0), right(0) {}
};

// solution

class Solution {
public:

    // isSymmetric_v1
    bool isSymmetric_v1(TreeNode*);

    // isSymmetric_v2
    bool isSymmetric_v2(TreeNode*);

    // isSymmetric_v3
    bool isSymmetric_v3(TreeNode*);
};

#endif

//=====//
// end //
//=====//
