//=======================//
// searh insert position //
//=======================//

#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// searchInsert_v1

#define searchInsert searchInsert_v1

// the main function

int main()
{
    Solution sol1;
    int pos(-1);

    // # 1
    cout << "---------------------------------------------------------------->> 1" << endl;
    {
        std::vector<int> nums{5,7,8,10};
        int target(8);
        pos = sol1.searchInsert(nums, target);
        cout << " pos = " << pos << endl;
    }

    // # 2
    cout << "---------------------------------------------------------------->> 2" << endl;
    {
        std::vector<int> nums{5,6,7,8};
        int target(11);
        pos = sol1.searchInsert(nums, target);
        cout << " pos = " << pos << endl;
    }

    // # 3
    cout << "---------------------------------------------------------------->> 3" << endl;
    {
        std::vector<int> nums{5,6,7,8};
        int target(1);
        pos = sol1.searchInsert(nums, target);
        cout << " pos = " << pos << endl;
    }


    return 0;
}

// end
