//======================//
// permutation sequence //
//======================//

#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// getPermutation_v1
// getPermutation_v2

#define getPermutation getPermutation_v2

// the main function

int main()
{
    Solution sol1;
    int n(0);
    int k(0);
    std::string res{""};

    cout << "---------------------------------->> 1" << endl;
    n = 2;
    k = 1;
    res = sol1.getPermutation(n, k);
    cout << " res = " << res << endl;
    /*
        cout << "---------------------------------->> 2" << endl;
        n = 3;
        k = 2;
        res = sol1.getPermutation(n, k);
        cout << " res = " << res << endl;

        cout << "---------------------------------->> 3" << endl;
        n = 8;
        k = 8590;
        res = sol1.getPermutation(n, k);
        cout << " res = " << res << endl;
    */

    return 0;
}

// end
