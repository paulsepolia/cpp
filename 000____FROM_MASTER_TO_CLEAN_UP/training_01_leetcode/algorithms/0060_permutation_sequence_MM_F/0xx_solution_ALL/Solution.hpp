//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <string>

class Solution {
public:

    // getPermutaion_v1
    std::string getPermutation_v1(int, int);

    // getPermutaion_v2
    std::string getPermutation_v2(int, int);
};

#endif

// end
