//===========================//
// Solution class definition //
//===========================//

#include <algorithm>
#include "Solution.hpp"

#include <iostream>
using std::endl;
using std::cout;

// getPermutation_v1
// CORRECT BUT SLOW

std::string Solution::getPermutation_v1(int n, int k)
{
    std::vector<int> nums{};

    for(int i = 1; i <= n; i++) {
        nums.push_back(i);
    }

    int counter(0);

    do {
        counter++;
        if(counter == k) break;
    } while(std::next_permutation(nums.begin(), nums.end()));

    std::string s{""};

    for(const auto & i: nums) {
        s = s + std::to_string(i);
    }

    return s;
}

// getPermutation_v2
// FAST

std::string Solution::getPermutation_v2(int n, int k)
{
    if (n < 0 || k <= 0) {
        return std::string{""};
    }

    int fac[n+1];
    fac[0] = 1;

    for(int i = 1; i < n; i++) {
        fac[i] = fac[i-1]*i;
    }

    std::vector<char> items;

    for (int i = 1; i <= n; i++) {
        items.push_back(i+'0');
    }

    if (k > fac[n-1]*n) {
        return std::string("");
    }

    int re = k-1;
    std::string res("");

    for (int i = 1; i <= n; i++) {
        int d = re / fac[n-i];
        re %= fac[n-i];
        res.push_back(items[d]);
        items.erase(items.begin()+d);
    }

    return res;
}

// end
