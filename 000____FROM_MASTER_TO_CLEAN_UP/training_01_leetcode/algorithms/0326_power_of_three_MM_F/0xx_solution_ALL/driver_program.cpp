//==============//
// power of two //
//==============//

#include <iomanip>
#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// isPowerOfThree_v1

#define isPowerOfThree isPowerOfThree_v1

// the main function

int main()
{
    Solution sol1;
    bool res(false);
    int num(0);

    cout << std::fixed;
    cout << std::boolalpha;

    // # 1

    num = 3;
    res = sol1.isPowerOfThree(num);
    cout << " num = " << num << endl;
    cout << " res = " << res << endl;

    // # 2

    num = 9;
    res = sol1.isPowerOfThree(num);
    cout << " num = " << num << endl;
    cout << " res = " << res << endl;

    // # 3

    num = -9;
    res = sol1.isPowerOfThree(num);
    cout << " num = " << num << endl;
    cout << " res = " << res << endl;

    // # 4

    num = 27;
    res = sol1.isPowerOfThree(num);
    cout << " num = " << num << endl;
    cout << " res = " << res << endl;

    return 0;
}

// end
