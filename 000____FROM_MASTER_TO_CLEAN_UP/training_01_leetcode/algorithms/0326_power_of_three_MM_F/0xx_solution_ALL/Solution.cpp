//===========================//
// Solution class definition //
//===========================//

#include <cmath>
#include "Solution.hpp"

// isPowerOfThree_v1

bool Solution::isPowerOfThree_v1(int num)
{
    bool res(false);
    int tmp(num);

    // corner cases

    if(num == 1) {
        return true;
    }

    // main algo

    int counter(0);
    if(num%3 == 0) {
        while(num /= 3) {
            counter++;
        }
        if(static_cast<int>(std::pow(3.0, counter)) == tmp) {
            res = true;
        }
    } else {
        res = false;
    }

    return res;
}

// end
