//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <vector>

class Solution {
public:

    // singleNumber_v1
    int singleNumber_v1(std::vector<int>&);
};

#endif

//=====//
// end //
//=====//
