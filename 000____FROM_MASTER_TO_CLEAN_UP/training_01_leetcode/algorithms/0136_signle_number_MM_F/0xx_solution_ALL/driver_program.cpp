//===============//
// single number //
//===============//

#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// singleNumber_v1

#define singleNumber singleNumber_v1

// the main function

int main()
{
    std::vector<int> nums {};
    Solution sol1;
    int res(0);

    // # 1

    nums.resize(5);
    nums = {1,3,1,-1,3};
    res = sol1.singleNumber_v1(nums);
    cout << " --> 1 --> res = " << res << endl;

    // # 2

    nums.clear();
    nums.resize(21);
    nums = {17,12,5,-6,12,4,17,-5,2,-3,2,4,5,16,-3,-4,15,15,-4,-5,-6};
    res = sol1.singleNumber_v1(nums);
    cout << " --> 2 --> res = " << res << endl;

    // # 3

    nums.clear();
    nums.resize(13);
    nums = {1,2,3,4,5,6,7,1,2,3,4,5,6};
    res = sol1.singleNumber_v1(nums);
    cout << " --> 3 --> res = " << res << endl;

    return 0;
}

//=====//
// end //
//=====//
