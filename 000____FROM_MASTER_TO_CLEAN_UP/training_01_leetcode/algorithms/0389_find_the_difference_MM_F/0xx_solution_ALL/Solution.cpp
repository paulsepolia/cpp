//===========================//
// Solution class definition //
//===========================//

#include <algorithm>
#include "Solution.hpp"

// findTheDifference_v1

char Solution::findTheDifference_v1(std::string s1, std::string s2)
{
    char res('\0');

    // sort the strings

    std::sort(s1.begin(), s1.end());
    std::sort(s2.begin(), s2.end());

    // # 1
    // corner case

    if(s1.compare(s2) == 0) {
        return res;
    }

    // # 2
    // corner case

    if(s1[s1.size()-1] != s2[s2.size()-1]) {
        res = s2[s2.size()-1];
    }

    // if they are not equal

    for(unsigned int i = 0; i != s2.size()-1; i++) {
        if(s1[i] != s2[i]) {
            res = s2[i];
            break;
        } else if(i == s2.size()-2) {
            res = s2[s2.size()-1];
        }
    }

    return res;
}

// end
