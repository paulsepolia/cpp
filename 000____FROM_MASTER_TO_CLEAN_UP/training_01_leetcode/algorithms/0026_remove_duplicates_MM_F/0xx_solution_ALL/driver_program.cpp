//================//
// remove element //
//================//

#include <iostream>

using std::cout;
using std::endl;

#include "Solution.hpp"

// macros
// allowed names are:

#define remove_duplicates remove_duplicates_v2

// the main function

int main()
{
    std::vector<int> my_vec {};
    Solution sol1;
    int val(0);

    // # 1

    my_vec.clear();

    for(int i = 1; i <= 20; i++) {
        my_vec.push_back(i);
    }

    val = sol1.remove_duplicates(my_vec);
    cout << " --> 1 --> val = " << val << endl;

    // # 2

    my_vec.clear();

    for(int i = 1; i <= 20; i++) {
        my_vec.push_back(20);
    }

    val = sol1.remove_duplicates(my_vec);
    cout << " --> 2 --> val = " << val << endl;

    // # 3

    my_vec.clear();

    for(int i = 1; i <= 20; i++) {
        my_vec.push_back(20);
    }

    val = sol1.remove_duplicates(my_vec);
    cout << " --> 3 --> val = " << val << endl;

    return 0;
}

//=====//
// end //
//=====//
