//===========================//
// Solution class definition //
//===========================//

#include <set>
#include "Solution.hpp"

// # 1

int Solution::remove_duplicates_v1(std::vector<int>& nums)
{
    std::set<int> s;

    for(unsigned int i = 0; i < nums.size(); i++) {
        s.insert(nums[i]);
    }

    nums.clear();

    for (auto & item : s) {
        nums.push_back(item);
    }

    return static_cast<int>(nums.size());
}

// # 2

int Solution::remove_duplicates_v2(std::vector<int>& nums)
{
    if (nums.size() == 0) {
        return 0;
    }

    int i = 0;

    for (unsigned int j = 1; j < nums.size(); j++) {
        if (nums[j] != nums[i]) {
            i++;
            nums[i] = nums[j];
        }
    }

    nums.resize(i+1);

    return static_cast<int>(nums.size());
}

//=====//
// end //
//=====//
