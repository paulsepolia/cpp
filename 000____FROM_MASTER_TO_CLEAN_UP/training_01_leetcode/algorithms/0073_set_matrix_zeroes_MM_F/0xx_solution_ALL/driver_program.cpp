//===================//
// set matrix zeroes //
//===================//

#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// setMatrixZeroes_v1
// setMatrixZeroes_v2

#define setMatrixZeroes setMatrixZeroes_v2

// the main function

int main()
{
    Solution sol1;
    std::vector<std::vector<int>> mat {};
    unsigned int dim1(0);
    unsigned int dim2(0);

    // # 1

    dim1 = 5;
    dim2 = 2;

    mat.resize(dim1);

    for(unsigned int i = 0; i != dim1; i++) {
        mat[i].resize(dim2);
        for(unsigned int j = 0; j != dim2; j++) {
            mat[i][j] = j+i;
            cout << "mat --> " << i << ", " << j << " = "<< mat[i][j] << endl;
        }
    }

    sol1.setMatrixZeroes(mat);

    for(unsigned int i = 0; i != dim1; i++) {
        for(unsigned int j = 0; j != dim2; j++) {
            cout << "mat --> " << i << ", " << j << " = "<< mat[i][j] << endl;
        }
    }


    return 0;
}

//=====//
// end //
//=====//
