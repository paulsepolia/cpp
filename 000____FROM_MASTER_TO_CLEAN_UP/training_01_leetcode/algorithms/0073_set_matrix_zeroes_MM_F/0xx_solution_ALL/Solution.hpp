//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <vector>

class Solution {
public:

    // setMatrixZeroes_v1
    void setMatrixZeroes_v1(std::vector<std::vector<int>>&);

    // setMatrixZeroes_v2
    void setMatrixZeroes_v2(std::vector<std::vector<int>>&);
};

#endif

//=====//
// end //
//=====//
