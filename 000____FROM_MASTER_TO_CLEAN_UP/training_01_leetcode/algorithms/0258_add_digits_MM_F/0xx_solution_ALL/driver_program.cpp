//============//
// add digits //
//============//

#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// addDigits_v1
// addDigits_v2
// addDigits_v3

#define addDigits addDigits_v3

// the main function

int main()
{
    Solution sol1;
    int num(0);
    int res(0);

    // # 1

    num = 111;
    res = sol1.addDigits(num);
    cout << " res = " << res << endl;

    // # 2

    num = 222;
    res = sol1.addDigits(num);
    cout << " res = " << res << endl;

    // # 3

    num = 333;
    res = sol1.addDigits(num);
    cout << " res = " << res << endl;

    // # 4

    num = 444;
    res = sol1.addDigits(num);
    cout << " res = " << res << endl;

    return 0;
}

//=====//
// end //
//=====//
