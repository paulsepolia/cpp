//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

class Solution {
public:

    // addDigits_v1
    int addDigits_v1(int);

    // addDigits_v2
    int addDigits_v2(int);

    // addDigits_v3
    int addDigits_v3(int);
};

#endif

//=====//
// end //
//=====//
