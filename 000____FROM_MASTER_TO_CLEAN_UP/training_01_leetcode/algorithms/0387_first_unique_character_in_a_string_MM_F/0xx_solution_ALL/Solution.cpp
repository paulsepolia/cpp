//===========================//
// Solution class definition //
//===========================//

#include <algorithm>
#include "Solution.hpp"

// firstUniqChar_v1

int Solution::firstUniqChar_v1(std::string s)
{
    int res(-1);
    std::string s1(s);

    for(unsigned int i = 0; i != s.size(); i++) {
        auto it1 = std::find(s1.begin(), s1.end(), s[i]);
        auto it2 = std::find(it1+1, s1.end(), s[i]);

        if(it2 == s1.end()) {
            res = i;
            break;
        }
    }

    return res;
}

// end
