//===================//
// find peak element //
//===================//

#include <iostream>

using std::cout;
using std::endl;

#include "Solution.hpp"

// macros
// allowed names are:

#define findPeakElement findPeakElement_v1

// the main function

int main()
{
    Solution sol1;
    int res(0);
    std::vector<int> nums{1,2,3,4};
    res = sol1.findPeakElement(nums);
    cout << " res = " << res << endl;

    return 0;
}

// end
