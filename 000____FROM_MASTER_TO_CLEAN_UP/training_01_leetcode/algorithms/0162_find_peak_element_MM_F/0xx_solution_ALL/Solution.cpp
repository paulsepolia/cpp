//===========================//
// Solution class definition //
//===========================//

#include "Solution.hpp"

// findPeakElement_v1

int Solution::findPeakElement_v1(std::vector<int> & nums)
{
    // get the size of the array
    const unsigned int DIM(nums.size());

    // corner case
    if(DIM == 1) return 0;

    // corner cases further
    if(nums[0] > nums[1]) return 0;
    if(nums[DIM-1] > nums[DIM-2]) return DIM-1;

    // rest cases
    int res(0);
    for(unsigned int i = 0; i != DIM-2; i++) {
        if(nums[i+2] < nums[i+1] && nums[i] < nums[i+1]) {
            res = static_cast<int>(i+1);
            break;
        }
    }
    return res;
}

// end
