//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <vector>

class Solution {
public:

    // findPeakElement_v1
    int findPeakElement_v1(std::vector<int>&);
};

#endif

// end
