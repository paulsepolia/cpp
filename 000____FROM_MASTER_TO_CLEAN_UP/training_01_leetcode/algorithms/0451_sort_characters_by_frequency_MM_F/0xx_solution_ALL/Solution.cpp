//===========================//
// Solution class definition //
//===========================//

#include <map>
#include <algorithm>
#include "Solution.hpp"

// frequencySort_v1

std::string Solution::frequencySort_v1(std::string s)
{
    // sort the string
    std::sort(s.begin(), s.end());

    // put the substrings in a multimap with their size
    std::multimap<unsigned int, std::string> mapA{};
    std::multimap<unsigned int, std::string>::iterator it;

    // corner case
    if(s.size() == 1) {
        return s;
    }

    // main algo here
    unsigned int loc_size(0);
    for(unsigned int i = 0; i != s.size(); i++) {
        if(s[i] == s[i+1]) {
            ++loc_size;
        } else {
            mapA.insert(std::pair<unsigned int, std::string>(loc_size, s.substr(i-loc_size, loc_size+1)));
            loc_size = 0;
        }
    }

    std::string res("");
    for (auto it = mapA.rbegin(); it != mapA.rend(); ++it) {
        res = res + (it->second);
    }

    return res;
}

// end
