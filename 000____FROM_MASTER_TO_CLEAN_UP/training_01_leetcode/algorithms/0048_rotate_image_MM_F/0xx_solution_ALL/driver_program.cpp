//===============//
// rotate image //
//==============//

#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// rotate_v1

#define rotate rotate_v1

// the main function

int main()
{
    Solution sol1;
    std::vector<std::vector<int>> matrix{};
    sol1.rotate(matrix);
    return 0;
}

// end
