//===============//
// string to int //
//===============//

#include <iostream>
#include <iomanip>
#include <cmath>

using std::cout;
using std::endl;
using std::setw;
using std::right;
using std::pow;

#include "Solution.hpp"

// macros
// allowed names are:
// stringToInt_v1

#define stringToInt stringToInt_v1

// the main function

int main()
{
    // parameters

    const int SW(20);

    // input fixed and local variables

    Solution sol1;
    int res(0);
    string str {};

    // input # 1

    str = "12345";
    res = sol1.stringToInt(str);
    cout << " ---------------------------->> 1" << endl;
    cout << " str = " << setw(SW) << right << str << endl;
    cout << " res = " << setw(SW) << right << res << endl;

    // input # 2

    str = "123456";
    res = sol1.stringToInt(str);
    cout << " ---------------------------->> 2" << endl;
    cout << " str = " << setw(SW) << right << str << endl;
    cout << " res = " << setw(SW) << right << res << endl;

    // input # 3

    str = "-123456";
    res = sol1.stringToInt(str);
    cout << " ---------------------------->> 3" << endl;
    cout << " str = " << setw(SW) << right << str << endl;
    cout << " res = " << setw(SW) << right << res << endl;

    // input # 4

    str = "2222222222222222";
    res = sol1.stringToInt(str);
    cout << " ---------------------------->> 4" << endl;
    cout << " str = " << setw(SW) << right << str << endl;
    cout << " res = " << setw(SW) << right << res << endl;

    // input # 5

    str = "2147483647";
    res = sol1.stringToInt(str);
    cout << " ---------------------------->> 5" << endl;
    cout << " str = " << setw(SW) << right << str << endl;
    cout << " res = " << setw(SW) << right << res << endl;

    // input # 6

    str = "-2147483648";
    res = sol1.stringToInt(str);
    cout << " ---------------------------->> 6" << endl;
    cout << " str = " << setw(SW) << right << str << endl;
    cout << " res = " << setw(SW) << right << res << endl;

    // input # 7

    str = "2147483648";
    res = sol1.stringToInt(str);
    cout << " ---------------------------->> 7" << endl;
    cout << " str = " << setw(SW) << right << str << endl;
    cout << " res = " << setw(SW) << right << res << endl;

    // input # 8

    str = "-2147483649";
    res = sol1.stringToInt(str);
    cout << " ---------------------------->> 8" << endl;
    cout << " str = " << setw(SW) << right << str << endl;
    cout << " res = " << setw(SW) << right << res << endl;

    return 0;
}

//=====//
// end //
//=====//
