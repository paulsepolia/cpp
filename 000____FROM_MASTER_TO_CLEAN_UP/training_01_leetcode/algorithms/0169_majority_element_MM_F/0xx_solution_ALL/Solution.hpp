//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <vector>

class Solution {
public:

    // majority_v1
    int majority_v1(std::vector<int>&);
};

#endif

// end
