//===========================//
// Solution class definition //
//===========================//

#include <algorithm>
#include "Solution.hpp"

// majority_v1

int Solution::majority_v1(std::vector<int> & nums)
{
    // sort
    std::sort(nums.begin(), nums.end());

    // get the middle element

    int res(nums[nums.size()/2]);

    return res;
}

// end
