//==================//
// add two integers //
//==================//

#include <iomanip>
#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// getSum_v1
// getSum_v2

#define getSum getSum_v2

// the main function

int main()
{
    Solution sol1;
    int x(0);
    int y(0);
    int res(0);

    cout << std::fixed;
    cout << std::showpos;

    // # 1

    x = 10;
    y = 11;
    res = sol1.getSum(x,y);
    cout << "----------------------------------->> 1" << endl;
    cout << " x   = " << x << endl;
    cout << " y   = " << y << endl;
    cout << " res = " << res << endl;

    // # 2

    x = -1;
    y = -2;
    res = sol1.getSum(x,y);
    cout << "----------------------------------->> 2" << endl;
    cout << " x   = " << x << endl;
    cout << " y   = " << y << endl;
    cout << " res = " << res << endl;

    // # 3

    x = -1;
    y = 10;
    res = sol1.getSum(x,y);
    cout << "----------------------------------->> 3" << endl;
    cout << " x   = " << x << endl;
    cout << " y   = " << y << endl;
    cout << " res = " << res << endl;

    return 0;
}

//=====//
// end //
//=====//
