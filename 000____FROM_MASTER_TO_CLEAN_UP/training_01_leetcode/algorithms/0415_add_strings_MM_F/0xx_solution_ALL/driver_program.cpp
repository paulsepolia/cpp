//=============//
// add strings //
//=============//

#include <iomanip>
#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// addStrings_v1

#define addStrings addStrings_v1

// the main function

int main()
{
    Solution sol1;
    std::string s3("");
    std::string s1("");
    std::string s2("");

    // # 1

    cout << "---------------------------------------------------------------->> 1" << endl;
    s1 = "1234567";
    s2 = "23456789";
    s3 = sol1.addStrings(s1, s2);
    cout << " s1    = " << std::setw(20) << std::right << s1 << endl;
    cout << " s2    = " << std::setw(20) << std::right << s2 << endl;
    cout << " s1+s2 = " << std::setw(20) << std::right << s3 << endl;

    // # 2

    cout << "---------------------------------------------------------------->> 2" << endl;
    s1 = "1234567";
    s1 = "123456789123456789";
    s2 = "999999999999999999";
    s3 = sol1.addStrings(s1, s2);
    cout << " s1    = " << std::setw(20) << std::right << s1 << endl;
    cout << " s2    = " << std::setw(20) << std::right << s2 << endl;
    cout << " s1+s2 = " << std::setw(20) << std::right << s3 << endl;

    return 0;
}

// end
