//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <string>

class Solution {
public:

    // addStrings_v1
    std::string addStrings_v1(std::string, std::string);
};

#endif

// end
