//===========================//
// Solution class definition //
//===========================//

#include <algorithm>
#include "Solution.hpp"

// climbingStairs_v1
// recursive slow version

int Solution::climbingStairs_v1(int n)
{
    int sol(0);

    if(n == 0) {
        return 0;
    } else if(n == 1) {
        return 1;
    } else if(n == 2) {
        return 2;
    } else {
        sol = climbingStairs_v1(n-1) + climbingStairs_v1(n-2);
    }

    return sol;
}

// climbingStairs_v2
// iterative fast version

int Solution::climbingStairs_v2(int n)
{
    int sol(0);

    if(n == 0) {
        return 0;
    } else if(n == 1) {
        return 1;
    } else if(n == 2) {
        return 2;
    } else {

        int previous = 0;
        int current = 1;

        for (int i = 1; i <= n; i++) {
            int next = previous + current;
            previous = current;
            current = next;
        }
        sol = current;
    }

    return sol;
}

// end
