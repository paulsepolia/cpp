//=================//
// climbing stairs //
//=================//

#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// climbingStairs_v1
// climbingStairs_v2

#define climbingStairs climbingStairs_v2

// the main function

int main()
{
    Solution sol1;
    int res(0);
    int input(0);

    // # 1

    input = 5;
    res = sol1.climbingStairs(input);

    cout << " --> input = " << input << endl;
    cout << " --> res   = " << res << endl;

    return 0;
}

//=====//
// end //
//=====//
