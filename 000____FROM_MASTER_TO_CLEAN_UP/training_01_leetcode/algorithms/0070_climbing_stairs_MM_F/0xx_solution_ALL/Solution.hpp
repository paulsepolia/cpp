//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <vector>

class Solution {
public:

    // climbingStairs_v1
    int climbingStairs_v1(int);

    // climbingStairs_v2
    int climbingStairs_v2(int);
};

#endif

//=====//
// end //
//=====//
