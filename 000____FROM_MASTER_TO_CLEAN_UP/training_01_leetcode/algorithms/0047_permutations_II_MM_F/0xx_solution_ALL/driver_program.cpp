//==============//
// combinations //
//==============//

#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// permuteUnique_v1

#define permuteUnique permuteUnique_v1

// the main function

int main()
{
    Solution sol1;
    std::vector<std::vector<int>> res{};

    // # 1
    {
        cout << "---------------------------------->> 1" << endl;
        std::vector<int> nums{1,2,1};
        res = sol1.permuteUnique(nums);

        for(const auto & i: res) {
            for(const auto & j : i) {
                cout << j << " ";
            }
            cout << endl;
        }
    }

    // # 2
    {
        cout << "---------------------------------->> 2" << endl;
        std::vector<int> nums{1,2,3,4};
        res = sol1.permuteUnique(nums);

        for(const auto & i: res) {
            for(const auto & j : i) {
                cout << j << " ";
            }
            cout << endl;
        }
    }

    // # 3
    {
        cout << "---------------------------------->> 3" << endl;
        std::vector<int> nums{1,1,1,1};
        res = sol1.permuteUnique(nums);

        for(const auto & i: res) {
            for(const auto & j : i) {
                cout << j << " ";
            }
            cout << endl;
        }
    }

    return 0;
}

// end
