//===========================//
// Solution class definition //
//===========================//

#include <cmath>
#include "Solution.hpp"

// my_pow_v1

double Solution::my_pow_v1(double x, int n)
{
    double res(static_cast<double>(std::pow(x,n)));
    return res;
}

// my_pow_v2

double Solution::my_pow_v2(double x, int n)
{
    double res(1);

    // # 1
    // corner cases

    if((n == 0) && (x < 0.0 || x > 0.0)) {
        return 1.0;
    } else if((n != 0) && !(x < 0.0) && !(x > 0.0)) {
        return 0.0;
    } else if((n == 0) && !(x < 0.0) && !(x > 0.0)) {
        return 1.0;
    }

    // # 2
    // n positive

    if(n > 0) {
        for(int i = 0; i < n; i++) {
            res = x * res;
        }
    }

    // # 3
    // n negative

    if(n < 0) {
        for(int i = 0; i < -n; i++) {
            res = (1.0/x) * res;
        }
    }

    return res;
}

// my_pow_v3
// many instances of the recurcive makes it slower

double Solution::my_pow_v3(double x, int n)
{
    double res(1);

    // # 1
    // corner cases

    if((n == 0) && (x < 0.0 || x > 0.0)) {
        return 1.0;
    } else if((n != 0) && !(x < 0.0) && !(x > 0.0)) {
        return 0.0;
    } else if((n == 0) && !(x < 0.0) && !(x > 0.0)) {
        return 1.0;
    }

    // # 2
    // n positive

    if ((n > 0) && ((n % 2) == 0)) {
        res = my_pow_v3(x, n/2) * my_pow_v3(x, n/2);
    } else if (n > 0) {
        res = x * my_pow_v3(x, n/2) * my_pow_v3(x, n/2);
    }

    // # 3
    // n negative

    if ((n < 0) && ((-n % 2) == 0)) {
        res = my_pow_v3(x, -n/2) * my_pow_v3(x, -n/2);
        res = 1.0/res;
    } else if (n < 0) {
        res = x * my_pow_v3(x, -n/2) * my_pow_v3(x, -n/2);
        res = 1.0/res;
    }

    return res;
}

// my_pow_v4
// fastest implementation

double Solution::my_pow_v4(double x, int n)
{
    double res(1);

    // # 1
    // corner cases

    if((n == 0) && (x < 0.0 || x > 0.0)) {
        return 1.0;
    } else if((n != 0) && !(x < 0.0) && !(x > 0.0)) {
        return 0.0;
    } else if((n == 0) && !(x < 0.0) && !(x > 0.0)) {
        return 1.0;
    }

    // # 2

    double temp = my_pow_v4(x, n/2);

    if ((n % 2) == 0) {
        return temp * temp;
    } else {
        if (n > 0) {
            return x * temp * temp;
        } else {
            return (temp * temp)/x;
        }
    }
}

//=====//
// end //
//=====//
