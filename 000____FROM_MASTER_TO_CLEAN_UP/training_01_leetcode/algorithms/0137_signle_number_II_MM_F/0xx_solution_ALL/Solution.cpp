//===========================//
// Solution class definition //
//===========================//

#include <algorithm>
#include "Solution.hpp"

// singleNumberII_v1

int Solution::singleNumberII_v1(std::vector<int>& nums)
{
    int res(0);

    // corner case

    if(nums.size() == 1) {
        res = nums[0];
        return res;
    }

    // sort the vector

    std::sort(nums.begin(), nums.end());

    if(nums[0] != nums[1]) {
        res = nums[0];
        return res;
    }

    // find the unique element

    for(unsigned int i = 1; i != nums.size()-1; i++) {
        if((nums[i] != nums[i+1]) && (nums[i] != nums[i-1])) {
            res = nums[i];
            return res;
        }
    }

    res = nums[nums.size()-1];

    return res;
}

// end
