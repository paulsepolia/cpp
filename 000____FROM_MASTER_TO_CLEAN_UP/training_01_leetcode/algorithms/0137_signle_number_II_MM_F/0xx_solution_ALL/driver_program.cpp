//===============//
// single number //
//===============//

#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// singleNumberII_v1

#define singleNumberII singleNumberII_v1

// the main function

int main()
{
    std::vector<int> nums {};
    Solution sol1;
    int res(0);

    // # 1

    nums.resize(5);
    nums = {1,1,1,3,3,3,-1};
    res = sol1.singleNumberII(nums);
    cout << " --> 1 --> res = " << res << endl;

    // # 2

    nums.clear();
    nums.resize(16);
    nums = {-17,-17,-17,18,18,18,1,1,1,2,2,2,3,3,3,-1} ;
    res = sol1.singleNumberII(nums);
    cout << " --> 2 --> res = " << res << endl;

    // # 3

    nums.clear();
    nums.resize(8);
    nums = {1,2,-3,1,2,-3,1,2,-3,-1};
    res = sol1.singleNumberII(nums);
    cout << " --> 3 --> res = " << res << endl;

    // # 4

    nums.clear();
    nums.resize(8);
    nums = {1,2,3,1,2,3,-1,1,2,3};
    res = sol1.singleNumberII(nums);
    cout << " --> 4 --> res = " << res << endl;

    // # 5

    nums.clear();
    nums.resize(8);
    nums = {1,2,3,1,2,3,10,1,2,3};
    res = sol1.singleNumberII(nums);
    cout << " --> 5 --> res = " << res << endl;

    // # 6

    nums.clear();
    nums.resize(8);
    nums = {1,2,4,1,2,4,1,2,3,4};
    res = sol1.singleNumberII(nums);
    cout << " --> 6 --> res = " << res << endl;

    return 0;
}

//=====//
// end //
//=====//
