//============================//
// intersection of two arrays //
//============================//

#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// intersectionII_v1
// intersectionII_v2

#define intersectionII intersectionII_v1

// the main function

int main()
{
    Solution sol1;

    // # 1
    {
        cout << "------------------------>> 1" << endl;
        std::vector<int> nums1 = {1,1,2,2,3,3,3,4};
        std::vector<int> nums2 = {1,1,2,2,3,3,3,4};
        std::vector<int> res = sol1.intersectionII(nums1, nums2);
        for(auto & i: res) {
            cout << i << " ";
        }
        cout << endl;
    }

    // # 2
    {
        cout << "------------------------>> 2" << endl;
        std::vector<int> nums1 = {1,1,2,2,3,4};
        std::vector<int> nums2 = {1,1,2,2,3,4,5,6};
        std::vector<int> res = sol1.intersectionII(nums1, nums2);
        for(auto & i: res) {
            cout << i << " ";
        }
        cout << endl;
    }

    // # 3
    {
        cout << "------------------------>> 3" << endl;
        std::vector<int> nums1 = {1};
        std::vector<int> nums2 = {5,2,3,4};
        std::vector<int> res = sol1.intersectionII(nums1, nums2);
        for(auto & i: res) {
            cout << i << " ";
        }
        cout << endl;
    }

    // # 4
    {
        cout << "------------------------>> 4" << endl;
        std::vector<int> nums1 = {};
        std::vector<int> nums2 = {};
        std::vector<int> res = sol1.intersectionII(nums1, nums2);
        for(auto & i: res) {
            cout << i << " ";
        }
        cout << endl;
    }

    // # 5
    {
        cout << "------------------------>> 5" << endl;
        std::vector<int> nums1 = {-1,-3,-3,-3,-1};
        std::vector<int> nums2 = {-1,-2,-3};
        std::vector<int> res = sol1.intersectionII(nums1, nums2);
        for(auto & i: res) {
            cout << i << " ";
        }
        cout << endl;
    }

    return 0;
}

// end
