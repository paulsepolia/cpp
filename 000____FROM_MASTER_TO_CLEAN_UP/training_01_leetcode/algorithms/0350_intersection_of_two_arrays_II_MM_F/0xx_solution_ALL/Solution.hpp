//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <vector>

class Solution {
public:

    //  intersectionII_v1
    std::vector<int> intersectionII_v1(std::vector<int>&, std::vector<int>&);

    //  intersectionII_v2
    std::vector<int> intersectionII_v2(std::vector<int>&, std::vector<int>&);
};

#endif

// end
