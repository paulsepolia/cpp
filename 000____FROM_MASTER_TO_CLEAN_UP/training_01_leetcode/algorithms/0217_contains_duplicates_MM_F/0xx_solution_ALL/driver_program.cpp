//=====================//
// contains duplicates //
//=====================//

#include <iomanip>
#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// containsDuplicates_v1

#define containsDuplicates containsDuplicates_v1

// the main function

int main()
{
    Solution sol1;
    bool res(false);
    cout << std::fixed;
    cout << std::boolalpha;

    {
        // # 1
        cout << "---------------------------------->> 1" << endl;
        std::vector<int> vec{1,2,3,4,3,3,3};
        res = sol1.containsDuplicates(vec);
        cout << " res = " << res << endl;
    }

    {
        // # 2
        cout << "---------------------------------->> 2" << endl;
        std::vector<int> vec{1,2,3,4};
        res = sol1.containsDuplicates(vec);
        cout << " res = " << res << endl;
    }

    return 0;
}

// end
