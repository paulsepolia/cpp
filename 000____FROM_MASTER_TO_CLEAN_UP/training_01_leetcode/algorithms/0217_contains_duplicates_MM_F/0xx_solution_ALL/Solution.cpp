//===========================//
// Solution class definition //
//===========================//

#include <algorithm>
#include "Solution.hpp"

// canConstruct_v1

bool Solution::containsDuplicates_v1(std::vector<int> & nums)
{
    bool res(false);
    const unsigned int DIM1(nums.size());

    // sort
    std::sort(nums.begin(), nums.end());

    // unique
    auto it = std::unique(nums.begin(), nums.end());

    // resize
    nums.resize(std::distance(nums.begin(), it));

    // test
    const unsigned int DIM2(nums.size());

    if(DIM1 != DIM2) {
        res = true;
    } else if(DIM1 == DIM2) {
        res = false;
    }

    return res;
}

// end
