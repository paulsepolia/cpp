//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

// Definition for a binary tree node

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;

};

// Solution declaration

class Solution {
private:
    TreeNode * lastNode = 0;
public:

    // isValidBST_v1
    bool isValidBST_v1(TreeNode *);

    // isValidBST_v2
    bool isValidBST_v2(TreeNode *, long int, long int);

    // isValidBST_v2
    bool isValidBST_v2(TreeNode *);
};

#endif

// end
