//===========================//
// Solution class definition //
//===========================//

#include "Solution.hpp"
#include <climits>

// isValidBST_v1

bool Solution::isValidBST_v1(TreeNode * root)
{
    if (root == 0) {
        return true;
    }

    if (!isValidBST_v1(root->left)) {
        return false;
    }

    if (lastNode != 0 && lastNode->val >= root->val) {
        return false;
    }

    lastNode = root;

    return isValidBST_v1(root->right);
}

// isValidBST_v2

bool Solution::isValidBST_v2(TreeNode* root, long int min, long int max)
{
    if(root == 0) {
        return true;
    }

    if(root->val >= max || root->val <= min) {
        return false;
    }

    return isValidBST_v2(root->left,min,root->val) &&
           isValidBST_v2(root->right,root->val,max);
}

bool Solution::isValidBST_v2(TreeNode * root)
{
    if(root == 0) {
        return true;
    }
    return Solution::isValidBST_v2(root->left, LONG_MIN, root->val) &&
           Solution::isValidBST_v2(root->right, root->val, LONG_MAX);

}

// end
