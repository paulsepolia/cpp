//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

class Solution {
public:

    // reverseIntSTL

    int reverseIntSTL(int);

    // reverseIntNoSTL

    int reverseIntNoSTL(int);
};

#endif

//=====//
// end //
//=====//
