//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <vector>

class Solution {
public:

    // maxProfitII_v1
    int maxProfitII_v1(std::vector<int>&);
};

#endif

// end
