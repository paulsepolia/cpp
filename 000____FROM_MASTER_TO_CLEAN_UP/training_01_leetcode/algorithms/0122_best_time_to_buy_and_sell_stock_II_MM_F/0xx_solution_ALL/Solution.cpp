//===========================//
// Solution class definition //
//===========================//

#include "Solution.hpp"

#include <iostream>
using std::cout;
using std::endl;

// maxProfitII_v1

int Solution::maxProfitII_v1(std::vector<int>& prices)
{
    const unsigned int DIM(prices.size());
    // corner cases
    if(DIM == 0) return 0;
    if(DIM == 1) return 0;
    // main algo
    int sum(0);
    for(unsigned int i = 1; i != DIM; ++i) {
        if(prices[i] > prices[i-1]) {
            sum = sum + prices[i] - prices[i-1];
        }
    }
    return sum;
}

// end
