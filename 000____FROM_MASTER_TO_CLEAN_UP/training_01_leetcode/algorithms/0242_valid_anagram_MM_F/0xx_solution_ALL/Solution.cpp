//===========================//
// Solution class definition //
//===========================//

#include <algorithm>
#include "Solution.hpp"

#include <iostream>
using std::endl;
using std::cout;

// isAnagram_v1

bool Solution::isAnagram_v1(std::string s, std::string t)
{
    const unsigned int DS(s.size());
    const unsigned int DT(t.size());
    bool res(false);

    // corner case
    if(DS != DT) {
        return res;
    }

    // sort strings and compare
    std::sort(s.begin(), s.end());
    std::sort(t.begin(), t.end());

    if(s == t) {
        res = true;
    } else {
        res = false;
    }

    return res;
}

// end
