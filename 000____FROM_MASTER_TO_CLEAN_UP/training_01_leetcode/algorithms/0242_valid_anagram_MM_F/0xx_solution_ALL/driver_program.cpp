//===============//
// valid anagram //
//===============//

#include <iomanip>
#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// isAnagram_v1

#define isAnagram isAnagram_v1

// the main function

int main()
{
    Solution sol1;
    bool res(false);
    std::string s("");
    std::string t("");

    cout << std::fixed;
    cout << std::boolalpha;

    // # 1
    cout << "-------------------------------------------->> 1" << endl;
    s = "anagram";
    t = "nagaram";
    res = sol1.isAnagram(s,t);
    cout << " res = " << res << endl;

    // # 2
    cout << "-------------------------------------------->> 2" << endl;
    s = "rat";
    t = "car";
    res = sol1.isAnagram(s,t);
    cout << " res = " << res << endl;


    return 0;
}

// end
