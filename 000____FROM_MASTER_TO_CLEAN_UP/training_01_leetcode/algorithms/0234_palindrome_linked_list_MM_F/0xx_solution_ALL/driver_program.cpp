//=====================//
// reverse linked list //
//=====================//

#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// isPalindrome_v1
// isPalindrome_v2

#define isPalindrome isPalindrome_v2

// the main function

int main()
{
    ListNode * nA(0);
    bool res(false);
    Solution sol1;
    res = sol1.isPalindrome(nA);

    cout << " res = " << res << endl;

    return 0;
}

// end
