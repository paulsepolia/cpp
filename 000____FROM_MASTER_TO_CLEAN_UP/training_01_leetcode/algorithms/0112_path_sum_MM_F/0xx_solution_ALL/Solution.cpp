//===========================//
// Solution class definition //
//===========================//

#include "Solution.hpp"

bool Solution::hasPathSum_v1(TreeNode* root, int sum)
{
    // corner case
    if(root == 0) return false;
    // corner case
    if(root->left == 0 && root->right == 0) return (root->val == sum);
    // left zero
    if(root->left == 0) return hasPathSum_v1(root->right, sum-root->val);
    // right zero
    if(root->right == 0) return hasPathSum_v1(root->left, sum-root->val);
    // rest
    return hasPathSum_v1(root->left, sum-root->val) ||
           hasPathSum_v1(root->right, sum-root->val);
}
// end
