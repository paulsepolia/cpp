//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

class Solution {
public:

    // findNthDigit_v1
    int findNthDigit_v1(int);

    // findNthDigit_v2
    int findNthDigit_v2(int);

    // findNthDigit_v3
    int findNthDigit_v3(int);

    // findNthDigit_v4
    int findNthDigit_v4(int);
};

#endif

// end
