//================//
// find nth digit //
//================//

#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// findNthDigit_v1
// findNthDigit_v2
// findNthDigit_v3
// findNthDigit_v4

#define findNthDigit findNthDigit_v4

// the main function

int main()
{
    Solution sol1;
    int n(0);
    int res(0);

    // # 1
    cout << "-------------------------->> 1" << endl;
    n = 1;
    res = sol1.findNthDigit(n);
    cout << " n   = " << n << endl;
    cout << " res = " << res << endl;

    // # 2
    cout << "-------------------------->> 2" << endl;
    n = 10;
    res = sol1.findNthDigit(n);
    cout << " n   = " << n << endl;
    cout << " res = " << res << endl;

    // # 3
    cout << "-------------------------->> 3" << endl;
    n = 11;
    res = sol1.findNthDigit(n);
    cout << " n   = " << n << endl;
    cout << " res = " << res << endl;

    // # 4
    cout << "-------------------------->> 4" << endl;
    n = 12;
    res = sol1.findNthDigit(n);
    cout << " n   = " << n << endl;
    cout << " res = " << res << endl;

    // # 5
    cout << "-------------------------->> 5" << endl;
    n = 100;
    res = sol1.findNthDigit(n);
    cout << " n   = " << n << endl;
    cout << " res = " << res << endl;

    // # 6
    cout << "-------------------------->> 6" << endl;
    n = 101;
    res = sol1.findNthDigit(n);
    cout << " n   = " << n << endl;
    cout << " res = " << res << endl;

    // # 7
    cout << "-------------------------->> 7" << endl;
    n = 201;
    res = sol1.findNthDigit(n);
    cout << " n   = " << n << endl;
    cout << " res = " << res << endl;

    // # 8
    cout << "-------------------------->> 8" << endl;
    n = 220;
    res = sol1.findNthDigit(n);
    cout << " n   = " << n << endl;
    cout << " res = " << res << endl;

    // # 9
    cout << "-------------------------->> 9" << endl;
    n = 100000;
    res = sol1.findNthDigit(n);
    cout << " n   = " << n << endl;
    cout << " res = " << res << endl;

    // # 10
    cout << "-------------------------->> 10" << endl;
    n = 100000000;
    res = sol1.findNthDigit(n);
    cout << " n   = " << n << endl;
    cout << " res = " << res << endl;


    // # 11
    cout << "-------------------------->> 11" << endl;
    n = 200000000;
    res = sol1.findNthDigit(n);
    cout << " n   = " << n << endl;
    cout << " res = " << res << endl;
}

// end
