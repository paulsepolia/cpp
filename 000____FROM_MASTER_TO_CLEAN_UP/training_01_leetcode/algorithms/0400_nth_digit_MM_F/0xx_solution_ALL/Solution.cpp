//===========================//
// Solution class definition //
//===========================//

#include <algorithm>
#include <string>
#include <vector>
#include "Solution.hpp"

#include <iostream>
using std::endl;
using std::cout;

// findNthDigit_v1
// CORRECT BUT SLOW

int Solution::findNthDigit_v1(int n)
{
    int res(0);
    std::string str{""};

    for(int i = 1; i != n+1; i++) {
        str = str + std::to_string(i);
        if(int(str.size()) >= n) {
            res = str[n-1] - '0';
            break;
        }
    }

    return res;
}

// findNthDigit_v2
// CORRECT, MUCH FASTER BUT STILL SLOW

int Solution::findNthDigit_v2(int n)
{
    int res(0);
    std::string str{""};
    std::vector<char> con{};
    bool flg(false);

    for(int i = 1; i != n+1; i++) {
        str = std::to_string(i);
        for(const auto & j : str) {
            con.push_back(j);
            if(int(con.size()) == n) {
                res = j - '0';
                flg = true;
            }
        }
        if(flg) break;
    }

    return res;
}

// findNthDigit_v3
// CORRECT, MUCH FASTER BUT STILL SLOW

int Solution::findNthDigit_v3(int n)
{
    int res(0);
    std::string str{""};
    bool flg(false);
    int counter(0);

    for(int i = 1; i != n+1; i++) {
        str = std::to_string(i);
        for(const auto & j : str) {
            counter++;
            if(counter == n) {
                res = j - '0';
                flg = true;
            }
        }
        if(flg) break;
    }

    return res;
}

// findNthDigit_v4
// CORRECT AND FASTER

int Solution::findNthDigit_v4(int n)
{
    int i = 0;
    unsigned long du = 0;
    unsigned long subsum = du;

    while (static_cast<unsigned long>(n) > du) {
        n -= du;
        ++i;
        subsum += du;
        du = static_cast<unsigned long>(9 * std::pow(10, i - 1) * i);
    }

    unsigned long target = n / i  + std::pow(10, i - 1);
    if (n % i == 0) {
        return (target - 1) % 10;
    }

    return target / static_cast<unsigned long>(std::pow(10, i - (n % i))) % 10;

}

// end
