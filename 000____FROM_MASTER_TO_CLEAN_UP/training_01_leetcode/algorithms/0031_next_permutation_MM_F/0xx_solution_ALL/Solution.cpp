//===========================//
// Solution class definition //
//===========================//

#include <algorithm>
#include "Solution.hpp"

// nextPermutation_v1

void Solution::nextPermutation_v1(std::vector<int>& nums)
{
    std::next_permutation(nums.begin(), nums.end());
}

// end
