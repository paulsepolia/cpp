//===========================//
// Solution class definition //
//===========================//

#include "Solution.hpp"

ListNode* Solution::swapPairs(ListNode* head)
{
    if(head == 0) return 0;
    if(head->next == 0) return head;

    ListNode * left(head);
    ListNode * right(head->next);

    ListNode * tmp(right->next);
    right->next = left;
    left->next = tmp;
    head = right;

    ListNode * tail(0);

    while(true) {
        tail = left;
        if(tmp != 0 && tmp->next != 0) {
            left = tmp;
            right = tmp->next;
            tmp = right->next;
            right->next = left;
            left->next = tmp;
            tail->next = right;
        } else
            break;
    }

    return head;
}
