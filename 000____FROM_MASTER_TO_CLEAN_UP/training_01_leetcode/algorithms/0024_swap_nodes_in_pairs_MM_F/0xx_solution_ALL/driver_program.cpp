//=====================//
// swap nodes in pairs //
//=====================//

#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros

#define swapPairs swapPairs

// the main function

int main()
{
    ListNode * nA(0);
    ListNode * res(0);
    Solution sol1;
    res = sol1.swapPairs(nA);

    cout << " &res = " << &res << endl;

    return 0;
}

// end
