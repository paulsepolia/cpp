//===========================//
// Solution class definition //
//===========================//

#include <set>
#include "Solution.hpp"

#include <iostream>
using std::cout;
using std::endl;

// findRepeatedDnaSequences_v1

std::vector<std::string> Solution::findRepeatedDnaSequences_v1(std::string s)
{
    // get the size of the string
    const unsigned int DIM(s.length());
    const unsigned int TEN(10);

    // corner case
    if(DIM <= TEN) return std::vector<std::string> {};

    // get the division
    std::set<std::string> setA{};
    std::set<std::string> setB{};
    std::string s_tmp{};
    std::pair<std::set<std::string>::iterator, bool> ret{};

    // put the unique strings in the setB via setA
    for(unsigned int i = 0; i != DIM+1-TEN; i++) {
        s_tmp = s.substr(i, TEN);
        ret = setA.insert(s_tmp);
        if(ret.second == false) {
            setB.insert(s_tmp);
        }
    }

    // put the result in res
    std::vector<std::string> res{};
    for(auto it = setB.begin(); it != setB.end(); it++) {
        res.push_back(*it);
    }
    return res;
}

// findRepeatedDnaSequences_v2

std::vector<std::string> Solution::findRepeatedDnaSequences_v2(std::string s)
{
    std::vector<std::string> res{};
    return res;
}

// end
