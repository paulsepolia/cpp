//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <vector>
#include <string>

// Solution declaration

class Solution {
public:

    // findRepeatedDnaSequences_v1
    std::vector<std::string> findRepeatedDnaSequences_v1(std::string);

    // findRepeatedDnaSequences_v2
    std::vector<std::string> findRepeatedDnaSequences_v2(std::string);
};

#endif

// end
