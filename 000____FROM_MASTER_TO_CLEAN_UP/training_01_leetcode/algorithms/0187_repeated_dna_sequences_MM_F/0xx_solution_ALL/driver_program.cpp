//========================//
// repeated DNA sequences //
//========================//

#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// findRepeatedDnaSequences_v1
// findRepeatedDnaSequences_v2

#define findRepeatedDnaSequences findRepeatedDnaSequences_v1

// the main function

int main()
{
    // data

    std::vector<std::string> res{};
    Solution sol1;
    std::string s("");

    // # 1

    s = "AAAAACCCCCAAAAACCCCCCAAAAAGGGTTT";
    res = sol1.findRepeatedDnaSequences(s);

    for(const auto & i: res) {
        cout << i << " ";
    }
    cout << endl;

    return 0;
}

// end
