//===========//
// same tree //
//===========//

#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// isSameTree_v1

#define isSameTree isSameTree_v1

// the main function

int main()
{
    TreeNode * p(0);
    TreeNode * q(0);
    bool res(false);
    Solution sol1;
    res = sol1.isSameTree(p, q);

    cout << " res = " << res << endl;

    return 0;
}

// end
