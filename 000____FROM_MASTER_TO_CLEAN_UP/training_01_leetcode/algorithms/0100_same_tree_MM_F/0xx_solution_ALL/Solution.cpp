//===========================//
// Solution class definition //
//===========================//

#include "Solution.hpp"

bool Solution::isSameTree_v1(TreeNode* p, TreeNode* q)
{

    if ((p == 0) && (q == 0)) {
        return true;
    } else if ((p == 0) || (q == 0)) {
        return false;
    } else if (p->val != q->val) {
        return false;
    }
    return isSameTree_v1(p->left, q->left) && isSameTree_v1(p->right, q->right);
}

// end
