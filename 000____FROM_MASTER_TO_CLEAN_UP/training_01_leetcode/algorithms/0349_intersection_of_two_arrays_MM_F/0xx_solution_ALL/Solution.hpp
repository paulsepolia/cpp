//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <vector>

class Solution {
public:

    //  intersection_v1
    std::vector<int> intersection_v1(std::vector<int>&, std::vector<int>&);

    //  intersection_v2
    std::vector<int> intersection_v2(std::vector<int>&, std::vector<int>&);

    //  intersection_v3
    std::vector<int> intersection_v3(std::vector<int>&, std::vector<int>&);
};

#endif

// end
