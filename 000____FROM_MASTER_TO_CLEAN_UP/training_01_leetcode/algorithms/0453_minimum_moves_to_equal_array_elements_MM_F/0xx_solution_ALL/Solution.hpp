//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <vector>

// Solution declaration

class Solution {
public:

    // minMoves_v1
    int minMoves_v1(std::vector<int>&);

    // minMoves_v2
    int minMoves_v2(std::vector<int>&);

    // minMoves_v3
    int minMoves_v3(std::vector<int>&);
};

#endif

// end
