//==============//
// rotate array //
//==============//

#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// getRow_v1

#define getRow getRow_v1

// the main function

int main()
{
    Solution sol1;
    // # 1
    cout << "---------------------------------->> 1" << endl;
    {
        int k(1);
        std::vector<int> res{};
        res = sol1.getRow(k);
        for(const auto & i: res) {
            cout << i << " ";
        }
        cout << endl;
    }

    // # 2
    cout << "---------------------------------->> 2" << endl;
    {
        int k(5);
        std::vector<int> res{};
        res = sol1.getRow(k);
        for(const auto & i: res) {
            cout << i << " ";
        }
        cout << endl;
    }

    // # 3
    cout << "---------------------------------->> 3" << endl;
    {
        int k(10);
        std::vector<int> res{};
        res = sol1.getRow(k);
        for(const auto & i: res) {
            cout << i << " ";
        }
        cout << endl;
    }
    return 0;
}

// end
