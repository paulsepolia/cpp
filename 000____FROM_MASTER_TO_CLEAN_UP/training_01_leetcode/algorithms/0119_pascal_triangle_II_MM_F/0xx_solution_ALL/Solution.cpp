//===========================//
// Solution class definition //
//===========================//

#include "Solution.hpp"

// getRow_v1

std::vector<int> Solution::getRow_v1(int rowIndex)
{
    const std::vector<int> R1{1};
    const std::vector<int> R2{1,1};
    const std::vector<int> R3{1,2,1};
    // corner cases
    if(rowIndex < 0) rowIndex = -rowIndex;
    if(rowIndex == 0) {
        return R1;
    }
    if(rowIndex == 1) {
        return R2;
    }
    if(rowIndex == 2) {
        return R3;
    }
    // initialize the algo
    std::vector<int> tmp1{};
    std::vector<int> tmp2(R3);
    for(int i(3); i <= rowIndex; i++) {
        tmp1.push_back(1);
        const int SIZE(i+1);
        for(int j = 0; j != SIZE-2; j++) {
            tmp1.push_back(tmp2[j]+tmp2[j+1]);
        }
        tmp1.push_back(1);
        tmp2 = tmp1;
        tmp1.clear();
        tmp1.shrink_to_fit();
    }
    return tmp2;
}
