//===================//
// single number III //
//===================//

#include <algorithm>
#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// singleNumberIII_v1

#define singleNumberIII singleNumberIII_v1

// the main function

int main()
{
    Solution sol1;
    std::vector<int> res{};

    {
        cout << "----------------------------------->> 1" << endl;
        std::vector<int> nums{1,3,4,3,4,2};
        res = sol1.singleNumberIII(nums);
        std::sort(res.begin(), res.end());
        for(const auto & i: res) {
            cout << i << " ";
        }
        cout << endl;
    }

    {
        cout << "----------------------------------->> 2" << endl;
        std::vector<int> nums{1,2};
        res = sol1.singleNumberIII(nums);
        std::sort(res.begin(), res.end());
        for(const auto & i: res) {
            cout << i << " ";
        }
        cout << endl;
    }

    return 0;
}

// end
