//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

class Solution {
public:

    // countPrimes_v1
    int countPrimes_v1(int);

    // countPrimes_v2
    int countPrimes_v2(int);

    // countPrimes_v3
    int countPrimes_v3(int);
};

#endif

//=====//
// end //
//=====//
