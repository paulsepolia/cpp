//===========================//
// Solution class definition //
//===========================//

#include "Solution.hpp"

// countPrimes_v1

int Solution::countPrimes_v1(int num)
{
    int res(0);

    if(num == 0 || num == 1) {
        res = 0;
        return res;
    } else if(num == 2) {
        res = 1;
        return res;
    }

    unsigned int counter_p(0);
    bool is_prime(true);

    for(unsigned int i = 3; i < (unsigned int)num; i++) {
        for(unsigned int j = 2; j < i; j++) {
            if(i%j == 0) {
                is_prime = false;
                break;
            }
        }
        if(is_prime) {
            counter_p++;
        }
        is_prime = true;
    }

    return (static_cast<int>(counter_p)+1);
}

// countPrimes_v2

int Solution::countPrimes_v2(int num)
{
    bool is_prime(true);
    int counter_p(0);

    for(int n = 0; n < num; n++) {
        is_prime = true;

        if(n <= 1) {
            continue;
        } else if(n <= 3) {
            counter_p++;
            continue;
        } else if((n%2 == 0) || (n%3 == 0)) {
            continue;
        }

        int i(5);

        while(i*i <= n) {
            if((n%i) == 0 || (n%(i+2) == 0)) {
                is_prime = false;
                break;
            }
            i = i+6;
        }

        if(is_prime) {
            counter_p++;
        }
    }

    return counter_p;
}

// countPrimes_v3

int Solution::countPrimes_v3(int num)
{
    int counter_p(0);

    for (int i = 2; i < num; i++) {

        bool is_prime = true;

        for (int j = 2; j*j <= i; j++) {
            if(i % j == 0) {
                is_prime = false;
                break;
            }
        }

        if(is_prime) {
            counter_p++;
        }
    }

    return counter_p;
}

// end
