//==============//
// count primes //
//==============//

#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// countPrimes_v1
// countPrimes_v2
// countPrimes_v3

#define countPrimes countPrimes_v3

// the main function

int main()
{
    Solution sol1;
    int num_p(0);
    int input(0);

    // # 1

    input = 0;
    num_p = sol1.countPrimes(input);
    cout << " --> input = " << input << endl;
    cout << " --> num_p = " << num_p << endl;

    // # 2

    input = 1;
    num_p = sol1.countPrimes(input);
    cout << " --> input = " << input << endl;
    cout << " --> num_p = " << num_p << endl;

    // # 3

    input = 2;
    num_p = sol1.countPrimes(input);
    cout << " --> input = " << input << endl;
    cout << " --> num_p = " << num_p << endl;

    // # 4

    input = 3;
    num_p = sol1.countPrimes(input);
    cout << " --> input = " << input << endl;
    cout << " --> num_p = " << num_p << endl;

    // # 5

    input = 4;
    num_p = sol1.countPrimes(input);
    cout << " --> input = " << input << endl;
    cout << " --> num_p = " << num_p << endl;

    // # 6

    input = 1000;
    num_p = sol1.countPrimes(input);
    cout << " --> input = " << input << endl;
    cout << " --> num_p = " << num_p << endl;

    // # 7

    input = 100000;
    num_p = sol1.countPrimes(input);
    cout << " --> input = " << input << endl;
    cout << " --> num_p = " << num_p << endl;

    return 0;
}

//=====//
// end //
//=====//
