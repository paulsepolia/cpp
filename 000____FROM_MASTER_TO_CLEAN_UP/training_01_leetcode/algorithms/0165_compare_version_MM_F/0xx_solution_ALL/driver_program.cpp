//=================//
// version compare //
//=================//

#include <iostream>
#include <iomanip>

using std::cout;
using std::endl;
using std::fixed;
using std::right;
using std::showpos;

#include "Solution.hpp"

// macros
// allowed names are:

#define compareVersion compareVersion_v1

// the main function

int main()
{
    Solution sol1;
    string v1 {};
    string v2 {};
    int res(0);

    cout << fixed;
    cout << showpos;

    // # 1

    v1 = "11.22";
    v2 = "33.44";
    res = sol1.compareVersion(v1, v2);
    cout << " --> 1 --> res = " << right << res << endl;

    // # 2

    v1 = "33.11";
    v2 = "33.00";
    res = sol1.compareVersion(v1, v2);
    cout << " --> 2 --> res = " << right << res << endl;

    // # 3

    v1 = "33.0011";
    v2 = "33.0011";
    res = sol1.compareVersion(v1, v2);
    cout << " --> 3 --> res = " << right << res << endl;

    // # 4

    v1 = "1";
    v2 = "1.1";
    res = sol1.compareVersion(v1, v2);
    cout << " --> 4 --> res = " << right << res << endl;

    // # 5

    v1 = "1.1";
    v2 = "1";
    res = sol1.compareVersion(v1, v2);
    cout << " --> 5 --> res = " << right << res << endl;

    // # 6

    v1 = "1.1";
    v2 = "1.1.0";
    res = sol1.compareVersion(v1, v2);
    cout << " --> 6 --> res = " << right << res << endl;

    // # 7

    v1 = "1";
    v2 = "1.0.1";
    res = sol1.compareVersion(v1, v2);
    cout << " --> 7 --> res = " << right << res << endl;

    // # 8

    v1 = "19.8.3.17.5.01.0.0.4.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0000.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.000000";

    v2 = "19.8.3.17.5.01.0.0.4.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0000.0.0.0.0";

    res = sol1.compareVersion(v1, v2);
    cout << " --> 8 --> res = " << right << res << endl;

    return 0;
}

//=====//
// end //
//=====//
