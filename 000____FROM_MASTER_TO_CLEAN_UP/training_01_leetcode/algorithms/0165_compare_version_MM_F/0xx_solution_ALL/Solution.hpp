//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <string>

using std::string;

class Solution {
public:

    // compareVersion_v1

    int compareVersion_v1(string, string);
};

#endif

//=====//
// end //
//=====//
