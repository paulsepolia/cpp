//===========================//
// Solution class definition //
//===========================//

#include <algorithm>
#include <string>
#include <iostream>
#include <vector>

using std::string;
using std::cout;
using std::endl;
using std::vector;
using std::stoi;
using std::min;

#include "Solution.hpp"

// twoSumSTL_v1

int Solution::compareVersion_v1(string version1, string version2)
{
    int res(0);

    //===========================================//
    // find the positions of dots in each string //
    //===========================================//

    // # 1

    vector<unsigned int> dot_locs1 {};

    for(unsigned int i = 0; i < version1.size(); i++) {
        if(version1[i] == '.') {
            dot_locs1.push_back(i);
        }
    }

    // # 2

    vector<int> dot_locs2 {};

    for(unsigned int i = 0; i < version2.size(); i++) {
        if(version2[i] == '.') {
            dot_locs2.push_back(i);
        }
    }

    //=======================//
    // create the substrings //
    //=======================//

    // # 1

    vector<string> substr_vec1 {};
    string loc1("");

    for(unsigned int i = 0; i != version1.size(); i++) {

        if(version1[i] != '.') {
            loc1 = loc1 + version1[i];
            if (i == version1.size()-1) {
                substr_vec1.push_back(loc1);
                loc1 = "";
            }
        } else {
            substr_vec1.push_back(loc1);
            loc1 = "";
        }
    }

    // # 2

    vector<string> substr_vec2 {};
    string loc2("");

    for(unsigned int i = 0; i != version2.size(); i++) {

        if(version2[i] != '.') {
            loc2 = loc2 + version2[i];
            if (i == version2.size()-1) {
                substr_vec2.push_back(loc2);
                loc2 = "";
            }
        } else {
            substr_vec2.push_back(loc2);
            loc2 = "";
        }
    }

    //========================//
    // compare the substrings //
    //========================//

    // # 1
    // convert substrings to integers

    vector<int> subint_vec1 {};
    vector<int> subint_vec2 {};

    for(unsigned int i = 0; i != substr_vec1.size(); i++) {
        subint_vec1.push_back(stoi(substr_vec1[i]));
    }

    for(unsigned int i = 0; i != substr_vec2.size(); i++) {
        subint_vec2.push_back(stoi(substr_vec2[i]));
    }

    // # 2
    // make them of equal size

    const unsigned int DIA = abs(static_cast<int>(subint_vec1.size()) -
                                 static_cast<int>(subint_vec2.size()));

    if(subint_vec1.size() > subint_vec2.size()) {
        for(unsigned int i = 0; i < DIA; i++) {
            subint_vec2.push_back(0);
        }
    } else if(subint_vec1.size() < subint_vec2.size()) {
        for(unsigned int i = 0; i < DIA; i++) {
            subint_vec1.push_back(0);
        }
    }

    // # 3
    // compare here

    for(unsigned int i = 0; i < subint_vec1.size(); i++) {
        if(subint_vec1[i] > subint_vec2[i]) {
            return 1;
        } else if(subint_vec1[i] < subint_vec2[i]) {
            return -1;
        }
    }

    res = 0;

    return res;
}

//=====//
// end //
//=====//
