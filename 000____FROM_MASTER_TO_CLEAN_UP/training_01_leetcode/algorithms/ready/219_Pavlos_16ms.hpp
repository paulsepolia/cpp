class Solution {
public:
    bool containsNearbyDuplicate(vector<int>& nums, int k)
    {
        // get the size
        const unsigned int DIM1(nums.size());
        // corner case
        if(DIM1 == 0) return false;
        if(k <= 0) return false;
        // keep a back up
        std::vector<int> nums2(nums);
        // sort and unique to check if there are duplicates
        std::sort(nums2.begin(), nums2.end());
        // mark the duplicates
        std::vector<int> vec2{};
        for(unsigned int i = 0; i != DIM1-1; i++) {
            if(nums2[i] == nums2[i+1]) {
                vec2.push_back(nums2[i]);
            }
        }
        // get the size
        const unsigned int DIM2(vec2.size());
        // check if empty and return false
        if(DIM2 == 0) return false;
        // if not empty, go through all the possible duplicates
        std::sort(vec2.begin(), vec2.end());
        auto it = std::unique(vec2.begin(), vec2.end());
        vec2.resize(std::distance(vec2.begin(),it));
        const unsigned int DIM3(vec2.size());
        // clear the nums2
        nums2.clear();
        nums2.shrink_to_fit();
        // check all possible duplicates
        for(unsigned int j = 0; j != DIM3; j++) {
            for(unsigned int i = 0; i != DIM1; i++) {
                if(vec2[j] == nums[i]) {
                    for(unsigned int kk1 = 0; kk1 < k; kk1++) {
                        if(i+kk1+1 < DIM1) {
                            if(nums[i] == nums[i+kk1+1]) return true;
                        }
                    }
                }
            }
        }
        return false;
    }
};
