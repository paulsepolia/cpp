class Solution {

public:

    vector<int> twoSum(vector<int>& numbers, int target)
    {

        vector<int> tmp(numbers);
        auto it(std::unique(tmp.begin(), tmp.end()));
        tmp.resize(std::distance(tmp.begin(), it));
        const unsigned int DIM(tmp.size());
        bool flg(false);
        vector<int> res{};
        res.resize(2);
        for(unsigned int i = 0; i != DIM; i++) {
            int sum(0);
            for(unsigned int j = i+1; j != DIM; j++) {
                sum = tmp[i] + tmp[j];
                if(sum == target) {
                    res[0] = tmp[i];
                    res[1] = tmp[j];
                    flg = true;
                    break;
                }
            }
            if(flg) break;
        }
        if(!flg) {
            for(unsigned int i = 0; i != DIM; i++) {
                int sum = 2*tmp[i];
                if(sum == target) {
                    res[0] = tmp[i];
                    res[1] = tmp[i];
                    break;
                }
            }
        }

        auto it1(std::find(numbers.begin(), numbers.end(), res[0]));
        auto it2(std::find(numbers.begin(), numbers.end(), res[1]));
        res[0] = it1-numbers.begin()+1;
        res[1] = it2-numbers.begin()+1;
        if(res[0] == res[1]) res[1]++;
        return res;
    }
};
