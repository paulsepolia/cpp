class Solution {
public:
    int missingNumber(vector<int>& nums)
    {
        const unsigned int DIM(nums.size());
        int sumA(0);
        for(unsigned int i = 0; i != DIM; i++) {
            sumA = sumA + nums[i];
        }
        int sumB(0);
        for(unsigned int i = 0; i != DIM+1; i++) {
            sumB = sumB + i;
        }
        return sumB-sumA;
    }
};
