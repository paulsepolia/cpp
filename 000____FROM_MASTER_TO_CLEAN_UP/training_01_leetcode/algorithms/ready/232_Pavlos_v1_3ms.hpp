#include <queue>

class Queue {
public:
    // Push element x to the back of queue
    void push(int x)
    {
        _queue.push(x);
    }

    // Removes the element from in front of queue
    void pop(void)
    {
        _queue.pop();
    }

    // Get the front element
    int peek(void)
    {
        return _queue.front();
    }

    // Return whether the queue is empty
    bool empty(void)
    {
        return _queue.empty();
    }

private:
    std::queue<int> _queue{};
};
