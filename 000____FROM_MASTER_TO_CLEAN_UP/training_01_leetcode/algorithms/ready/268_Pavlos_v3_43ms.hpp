class Solution {
public:
    int missingNumber(vector<int>& nums)
    {
        const unsigned int DIM(nums.size());
        double sumA(0);
        for(unsigned int i = 0; i != DIM; i++) {
            sumA = sumA + sin(static_cast<double>(nums[i]));
        }
        double sumB(0);
        for(unsigned int i = 0; i != DIM+1; i++) {
            sumB = sumB + sin(static_cast<double>(i));
        }
        int res(-1);
        const double DIFF(0.0000001);
        for(unsigned int i = 0; i != DIM+1; i++) {
            if(abs(abs(sumB-sumA) - abs(sin(static_cast<double>(i)))) < DIFF) {
                res = i;
                break;
            }
        }
        return res;
    }
};
