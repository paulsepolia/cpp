#include <vector>
#include <algorithm>
#include <string>
#include <iostream>
using std::vector;
using std::string;
using std::endl;
using std::cout;

int maxProduct(vector<string>& words)
{
    int max_loc(0);
    // get the size
    const unsigned int DIM(words.size());
    // sort each word
    for(unsigned int i = 0; i != DIM; i++) {
        std::sort(words[i].begin(), words[i].end());
    }
    // check if contain duplicates
    for(unsigned int i = 0; i != DIM; i++) {
        for(unsigned int j = i+1; j != DIM; j++) {
            vector<char> v1{};
            vector<char> v2{};
            for(unsigned int k1 = 0; k1 != words[i].size(); k1++) {
                v1.push_back(words[i][k1]);
            }
            for(unsigned int k2 = 0; k2 != words[j].size(); k2++) {
                v2.push_back(words[j][k2]);
            }

            const unsigned int TOT(v1.size()+v2.size());
            cout << " TOT = " << TOT << endl;
            std::vector<char> tmp{};
            tmp.resize(TOT);
            std::merge(v1.begin(), v1.end(), v2.begin(), v2.end(), tmp.begin());
            auto it = std::unique(tmp.begin(), tmp.end());
            int max_now(0);
            tmp.resize(std::distance(tmp.begin(), it));
            auto it1 = std::unique(v1.begin(), v1.end());
            auto it2 = std::unique(v2.begin(), v2.end());
            v1.resize(std::distance(v1.begin(), it1));
            v2.resize(std::distance(v2.begin(), it2));

            cout << " tmp.size() = " << tmp.size() << endl;
            if(tmp.size() == v1.size() + v2.size()) {
                max_now = v1.size() * v2.size();
            }
            if(max_loc < max_now) {
                max_loc = max_now;
            }
        }
    }
    return max_loc;
}

int main()
{
    std::vector<std::string> words{};
    std::string s1 = "fcf";
    std::string s2 = "dabae";
    words.push_back(s1);
    words.push_back(s2);

    cout << maxProduct(words) << endl;

}
