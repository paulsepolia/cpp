class Solution {
public:
    string reverseVowels(string s)
    {
        //===============//
        // a, e, i, o, u //
        //===============//

        // get the size
        const unsigned int DIM(s.size());
        // find all vowels put it in a vector
        std::vector<char> vec1{};
        for(unsigned int i = 0; i != DIM; i++) {
            if(tolower(s[i]) == 'a' ||
                    tolower(s[i]) == 'e' ||
                    tolower(s[i]) == 'i' ||
                    tolower(s[i]) == 'o' ||
                    tolower(s[i]) == 'u') {
                vec1.push_back(s[i]);
            }
        }

        // corner cases
        const unsigned int DIM2(vec1.size());
        if (DIM2 == 0) return s;
        if(DIM2 == 1) return s;

        // reverse here
        int counter(0);
        for(unsigned int i = 0; i != DIM; i++) {
            if(tolower(s[i]) == 'a' ||
                    tolower(s[i]) == 'e' ||
                    tolower(s[i]) == 'i' ||
                    tolower(s[i]) == 'o' ||
                    tolower(s[i]) == 'u') {
                s[i] = vec1[DIM2-1-counter];
                counter++;
            }
        }
        return s;
    }
};
