//http://www.purplemath.com/modules/factzero.htm
class Solution {
public:
    int trailingZeroes(int n)
    {
        double pow5(5);
        int sum(0);
        double i(1);
        while(pow5 <= n) {
            pow5 = std::pow(5,i);
            sum = sum + static_cast<int>(n/pow5);
            i++;
        }
        return sum;
    }
};
