class Solution {
public:
    std::vector<int> twoSum(std::vector<int>& numbers, int target)
    {
        const unsigned int DIM(numbers.size());
        int left_pos(0);
        int right_pos(DIM-1);
        while(left_pos < right_pos) {
            if(numbers[left_pos] + numbers[right_pos] == target) {
                vector<int> res{left_pos+1,right_pos+1};
                return res;
            } else if(numbers[left_pos] + numbers[right_pos] > target) {
                right_pos--;
            } else {
                left_pos++;
            }
        }
        return std::vector<int> {};
    }
};
