// Forward declaration of isBadVersion API.
bool isBadVersion(int version);
class Solution {
public:
    int firstBadVersion(int n)
    {
        // corner case
        if(n == 1) return n;
        // corner case
        if(isBadVersion(1)) return 1;
        // rest cases
        int res(0);
        int low(1);
        int high(n);
        while(true) {
            int mid(low+((high-low)/2));
            bool result1(isBadVersion(mid));
            bool result2(isBadVersion(mid+1));
            if(!result1 && result2) {
                res = mid+1;
                break;
            } else if(result1 == false) {
                low = mid+1;
            } else if(result1 == true) {
                high = mid-1;
            }
        }
        return res;
    }
};
