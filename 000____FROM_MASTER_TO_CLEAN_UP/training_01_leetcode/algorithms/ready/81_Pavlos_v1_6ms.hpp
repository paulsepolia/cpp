class Solution {
public:
    bool search(vector<int>& nums, int target)
    {
        const unsigned int DIM(nums.size());
        // corner cases
        if(DIM == 0) return false;
        if(DIM == 1) {
            if(nums[0] == target) return true;
            return false;
        }
        auto it(std::find(nums.begin(), nums.end(), target));
        if(it == nums.end()) return false;
        return true;
    }
};
