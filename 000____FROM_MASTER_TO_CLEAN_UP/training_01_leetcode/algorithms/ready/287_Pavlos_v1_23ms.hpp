class Solution {
public:
    int findDuplicate(vector<int>& nums)
    {
        int res(0);
        const unsigned int DIM(nums.size());
        std::sort(nums.begin(), nums.end());
        for(unsigned int i = 0; i != DIM-1; i++) {
            if(nums[i] == nums[i+1]) {
                res = nums[i];
                break;
            }
        }
        return res;
    }
};
