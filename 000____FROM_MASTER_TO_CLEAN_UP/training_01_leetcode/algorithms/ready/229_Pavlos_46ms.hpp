class Solution {
public:
    vector<int> majorityElement(vector<int>& nums)
    {
        // get the size
        const unsigned int DIM(nums.size());
        // corner cases
        if(DIM == 0) return nums;
        if(DIM == 1) return nums;
        // else cases
        std::map<int,int> map1{};
        for(unsigned int i = 0; i != DIM; i++) {
            auto it(map1.find(nums[i]));
            if(it != map1.end()) {
                map1[nums[i]] = map1[nums[i]] + 1;
            } else {
                map1[nums[i]] = 1;
            }
        }
        std::vector<int> res{};
        for (std::map<int,int>::iterator it(map1.begin()); it != map1.end(); ++it) {
            if(it->second > DIM/3) {
                res.push_back(it->first);
            }
        }
        return res;
    }
};
