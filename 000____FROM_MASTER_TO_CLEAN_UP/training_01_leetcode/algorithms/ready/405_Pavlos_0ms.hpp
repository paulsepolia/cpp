class Solution {
public:
    string toHex(int num)
    {
        std::stringstream stream;
        stream << std::hex << num;
        std::string res(stream.str());
        return res;
    }
};
