class Solution {
public:
    vector<int> findDisappearedNumbers(vector<int>& nums)
    {
        std::vector<int> res{};
        const unsigned int DIM1(nums.size());
        std::sort(nums.begin(), nums.end());
        auto it = std::unique(nums.begin(), nums.end());
        nums.resize(std::distance(nums.begin(), it));
        const unsigned int DIM2(nums.size());
        for(unsigned int i = 1; i <= DIM1; i++) {
            if (!std::binary_search(nums.begin(), nums.end(), i)) {
                res.push_back(i);
            }
        }
        return res;
    }
};
