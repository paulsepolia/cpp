class Solution {
public:
    vector<int> productExceptSelf(vector<int>& nums)
    {
        vector<int> res(1,1);
        for (int i=0; i<nums.size(); i++) {
            res.push_back(res.back()*nums[i]);
        }
        int b = 1;
        for (int i=0; i<nums.size(); i++) {
            res[nums.size()-i-1] = res[nums.size()-i-1] * b;
            b = b* nums[nums.size()-i-1];
        }
        res.pop_back();
        return res;
    }
};
