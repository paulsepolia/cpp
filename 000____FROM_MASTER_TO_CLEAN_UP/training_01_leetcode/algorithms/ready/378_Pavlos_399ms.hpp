class Solution {
public:
    int kthSmallest(vector<vector<int>>& matrix, int k)
    {
        // get the number of rows
        const unsigned int ROWS(matrix.size());
        // get the number of columns
        const unsigned int COLS(matrix[0].size());
        // put the elements of each row in a vector merging them
        // since they are orderend already
        std::vector<int> vec{};
        std::vector<int> v1(matrix[0]);
        std::vector<int> v2{};
        for(unsigned int i = 0; i != ROWS-1; i++) {
            vec.resize((i+2)*COLS);
            v2 = matrix[i+1];
            std::merge(v1.begin(),v1.end(),v2.begin(),v2.end(),vec.begin());
            v1 = vec;
        }
        std::sort(v1.begin(), v1.end());
        return v1[k-1];
    }
};
