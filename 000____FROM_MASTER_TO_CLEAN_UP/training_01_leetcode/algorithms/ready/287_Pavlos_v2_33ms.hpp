class Solution {
public:
    int findDuplicate(vector<int>& nums)
    {
        std::pair<std::set<int>::iterator, bool> rete{};
        int res(0);
        const unsigned int DIM(nums.size());
        std::set<int> set_tmp{};
        for(unsigned int i = 0; i != DIM; i++) {
            rete = set_tmp.insert(nums[i]);
            if(rete.second == false) {
                res = nums[i];
                break;
            }
        }
        return res;
    }
};
