class Solution {
public:
    bool wordPattern(string pattern, string str)
    {
        if((pattern == str) && (str.size() == 1)) return true;
        // get the pattern size
        const unsigned int DIM1(pattern.size());
        // get the str size
        const unsigned int DIM2(str.size());
        // get the number of spaces in str
        unsigned int counter(0);
        for(unsigned int i(0); i != DIM2; i++) {
            if(str[i] == ' ')
                counter++;
        }
        // check the sizes
        if(counter+1 != DIM1) return false;
        // word in vector
        std::string s("");
        std::vector<std::string> vec1{};
        for(unsigned int i(0); i != DIM2; i++) {
            if(str[i] != ' ') {
                s = s + std::to_string(str[i]);
            } else if(str[i] == ' ') {
                vec1.push_back(s);
                s = "";
            }
        }
        vec1.push_back(s);
        if(vec1.size() != DIM1) return false;
        // construct a vector with pairs
        std::vector<std::pair<std::string, std::string>> vec2{};
        for(unsigned i(0); i != DIM1; i++) {
            std::pair<std::string, std::string> pa{};
            pa = std::make_pair(std::to_string(pattern[i]),vec1[i]);
            vec2.push_back(pa);
        }
        for(unsigned int i = 0; i < vec2.size(); i++) {
            std::string s1(vec2[i].first);
            std::string s2(vec2[i].second);
            for(unsigned int j = i+1; j < vec2.size(); j++) {
                if(vec2[j].first == s1) {
                    if(vec2[j].second != s2) return false;
                }
            }
        }
        // final check
        std::sort(pattern.begin(), pattern.end());
        auto it1(std::unique(pattern.begin(), pattern.end()));
        pattern.resize(std::distance(pattern.begin(), it1));
        std::sort(vec1.begin(), vec1.end());

        auto it2(std::unique(vec1.begin(), vec1.end()));
        vec1.resize(std::distance(vec1.begin(), it2));
        if(vec1.size() != pattern.size()) return false;

        // if here return true
        return true;
    }
};


