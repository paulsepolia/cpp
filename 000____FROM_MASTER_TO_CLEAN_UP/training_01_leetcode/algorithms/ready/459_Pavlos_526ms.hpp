class Solution {
public:
    bool repeatedSubstringPattern(string str)
    {
        const unsigned int DIM(str.size());
        // corner case
        if(DIM == 1) return false;
        // find the divisors and try to reproduce it
        for(unsigned int i = 0; i != DIM/2; i++) {
            if(DIM%(i+1) == 0) {
                std::string tmp("");
                while(tmp.size() != DIM) {
                    tmp = tmp + str.substr(0,i+1);
                }
                if (tmp == str) {
                    return true;
                }
            }
        }
        return false;
    }
};
