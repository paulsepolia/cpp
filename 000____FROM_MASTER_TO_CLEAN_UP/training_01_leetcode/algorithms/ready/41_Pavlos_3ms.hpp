class Solution {
public:
    int firstMissingPositive(vector<int>& nums)
    {
        // get the dimension
        const unsigned int DIM1(nums.size());
        // corner case
        if(DIM1 == 0) return 1;
        // sort
        std::sort(nums.begin(), nums.end());
        // remove duplicates
        auto it(std::unique(nums.begin(), nums.end()));
        nums.resize(std::distance(nums.begin(), it));
        const unsigned int DIM2(nums.size());
        // corner cases
        if(nums[DIM2-1] <= 0) return 1;
        if(nums[0] > 1) return 1;
        // remove negatives and mark it
        int index(0);
        bool flg2(false);
        for(unsigned int i = 0; i < DIM2-1; i++) {
            if(nums[i] <= 0 && nums[i+1] > 0) {
                index = i;
                flg2 = true;
                break;
            }
        }
        // replace the negatives and the zeroes
        if(flg2) {
            for(unsigned int i = 0; i != DIM2-1; i++) {
                nums[i] = nums[index+i+1];
                if(index+i+1 > DIM2-2) break;
            }
        }
        // resize
        nums.resize(DIM2-index+1);
        // new dim
        const unsigned int DIM3(nums.size());
        // corner cases
        if(DIM3 == 0) return 1;
        if(DIM3 == 1 && nums[0] == 1) return 2;
        if(DIM3 >= 1 && nums[0] > 1) return 1;
        int res(0);
        bool flg(true);
        for(unsigned int i = 0; i < DIM3; i++) {
            if(nums[i+1] != nums[i]+1) {
                res = nums[i]+1;
                flg = false;
                break;
            }
        }
        if(flg) return nums[DIM3-1]+1;
        return res;
    }
};
