class Solution {
public:
    int missingNumber(vector<int>& nums)
    {
        const unsigned int DIM(nums.size());
        int diff(0);
        nums.push_back(0);
        for(unsigned int i = 0; i < DIM+1; i++) {
            diff = diff + (i-nums[i]);
        }
        return diff;
    }
};
