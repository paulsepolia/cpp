class MinStack {
public:
    /** initialize your data structure here. */
    MinStack()
    {
    }
    void push(int x)
    {
        _stack.push(x);
        _set.insert(x);
    }
    void pop()
    {
        unsigned int S1(_set.size());
        int tmp(_stack.top());
        _set.erase(tmp);
        _stack.pop();
        unsigned int S2(_set.size());
        if(S1-S2 != 1) {
            for(int i = 2; i <= S1-S2; i++)
                _set.insert(tmp);
        }
    }
    int top()
    {
        return _stack.top();
    }
    int getMin()
    {
        return *_set.begin();
    }
private:
    std::stack<int> _stack{};
    std::multiset<int> _set{};
};
/**
 * Your MinStack object will be instantiated and called as such:
 * MinStack obj = new MinStack();
 * obj.push(x);
 * obj.pop();
 * int param_3 = obj.top();
 * int param_4 = obj.getMin();
 */
