class Solution {
public:
    bool isPerfectSquare(int num)
    {
        // corner cases
        if(num < 0) return false;
        if(num == 0) return true;
        if(num == 1) return true;
        // main loop
        for(int i = 2; i*i <= num+1; i++) {
            if(i*i == num) return true;
        }
        return false;
    }
};
