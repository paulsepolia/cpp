class NumArray {
public:
    NumArray(vector<int> &nums)
    {
        _result.push_back(0);
        for(auto& num : nums)
            _result.push_back(num + _result.back());
    }

    int sumRange(int i, int j)
    {
        return _result[j+1] - _result[i];
    }
private:
    vector<int> _result;
};

// Your NumArray object will be instantiated and called as such:
// NumArray numArray(nums);
// numArray.sumRange(0, 1);
// numArray.sumRange(1, 2);


