//===========================//
// Solution class definition //
//===========================//

#include <algorithm>
#include "Solution.hpp"

// reverseString_v1

std::string Solution::reverseString_v1(std::string in)
{
    std::reverse(in.begin(), in.end());
    return in;
}

// end
