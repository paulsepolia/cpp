//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

// ListNode definition

struct ListNode {
    int val;
    ListNode * next;
    ListNode(int x) : val(x), next(0) {}
};

// Solution declaration

class Solution {
public:

    // getIntersectionNode_v1
    ListNode * getIntersectionNode_v1(ListNode *, ListNode *);

    // getIntersectionNode_v2
    ListNode * getIntersectionNode_v2(ListNode *, ListNode *);

    // getIntersectionNode_v3
    ListNode * getIntersectionNode_v3(ListNode *, ListNode *);

    // getIntersectionNode_v4
    ListNode * getIntersectionNode_v4(ListNode *, ListNode *);
};

#endif

// end
