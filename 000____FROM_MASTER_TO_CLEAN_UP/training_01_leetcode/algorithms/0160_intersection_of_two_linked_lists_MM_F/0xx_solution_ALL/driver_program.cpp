//==================================//
// Intersection of two linked lists //
//==================================//

#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// getIntersectionNode_v1
// getIntersectionNode_v2
// getIntersectionNode_v3
// getIntersectionNode_v4

#define getIntersectionNode getIntersectionNode_v4

// the main function

int main()
{
    ListNode * nA(0);
    ListNode * nB(0);
    ListNode * res(0);
    Solution sol1;
    res = sol1.getIntersectionNode(nA, nB);

    cout << " &res = " << &res << endl;

    return 0;
}

// end
