//===========================//
// Solution class definition //
//===========================//

#include <cmath>
#include <vector>
#include <algorithm>
#include "Solution.hpp"

// getIntersectionNode_v1

ListNode * Solution::getIntersectionNode_v1(ListNode * headA, ListNode * headB)
{
    ListNode * rootA(headA);
    ListNode * rootB(headB);

    int lengthA(0);
    int lengthB(0);

    // get the length of the A linked list

    while(headA != 0) {
        ++lengthA;
        headA = headA->next;
    }

    // get the length of the B linked list

    while(headB != 0) {
        ++lengthB;
        headB = headB->next;
    }

    int diff(std::abs(lengthA-lengthB));

    // make a common length start

    if(lengthA > lengthB) {
        for(int index = 0; index < diff; ++index) {
            rootA = rootA->next;
        }
    } else {
        for(int index = 0; index < diff; ++index) {
            rootB = rootB->next;
        }
    }

    // try to find same address

    while(rootA != 0) {
        if(rootA == rootB) {
            return rootA;
        }

        rootA = rootA->next;
        rootB = rootB->next;
    }

    return 0;
}

// getIntersectionNode_v2
// SLOW - NOT ACCEPTED

ListNode * Solution::getIntersectionNode_v2(ListNode * listA, ListNode * listB)
{
    // save the heads of the linked lists
    ListNode * const headA(listA);
    ListNode * res(0);

    unsigned int lengthA(0);

    // get the length of the A linked list
    while(listA != 0) {
        lengthA++;
        listA = listA->next;
    }

    // put into a vector all the nodes of the list A
    std::vector<ListNode*> vecA{};
    vecA.resize(lengthA);
    listA = headA;

    for(unsigned int i = 0; i != lengthA; i++) {
        vecA.push_back(listA);
        listA = listA->next;
    }

    // try to find same address
    while(listB != 0) {
        auto p = std::find(vecA.begin(), vecA.end(), listB);
        if(p != vecA.end()) {
            res = listB;
            break;
        }
        listB = listB->next;
    }

    return res;
}

// getIntersectionNode_v3

ListNode * Solution::getIntersectionNode_v3(ListNode * listA, ListNode * listB)
{
    // save the heads of the linked lists
    ListNode * const headA(listA);
    ListNode * const headB(listB);
    ListNode * res(0);

    int lengthA(0);
    int lengthB(0);

    // get the length of the A linked list
    while(listA != 0) {
        lengthA++;
        listA = listA->next;
    }

    if(lengthA == 0) return 0;

    // put into a vector all the nodes of the list A
    std::vector<ListNode*> vecA{};
    vecA.resize(lengthA);
    listA = headA;

    for(int i = 0; i != lengthA; i++) {
        vecA[i] = listA;
        listA = listA->next;
    }

    // get the length of the B linked list
    while(listB != 0) {
        lengthB++;
        listB = listB->next;
    }

    if(lengthB == 0) return 0;

    // put into a vector all the nodes of the list B
    std::vector<ListNode*> vecB{};
    vecB.resize(lengthB);
    listB = headB;

    for(int i = 0; i != lengthB; i++) {
        vecB[i] = listB;
        listB = listB->next;
    }

    // drop the initial extra elements
    const int DIFF(std::abs(lengthA - lengthB));

    if(lengthA > lengthB) {
        // drop elements from vecA
        vecA.erase(vecA.begin(), vecA.begin()+DIFF);
        vecA.shrink_to_fit();
    } else {
        // drop elements from vecB
        vecB.erase(vecB.begin(), vecB.begin()+DIFF);
        vecB.shrink_to_fit();
    }

    // try to find same address
    for(unsigned int i = 0; i != vecA.size(); i++) {
        if(vecA[i] == vecB[i]) {
            res = vecA[i];
            break;
        }
    }

    return res;
}

// getIntersectionNode_v4

ListNode * Solution::getIntersectionNode_v4(ListNode * listA, ListNode * listB)
{
    // save the heads of the linked lists
    ListNode * const headA(listA);
    ListNode * const headB(listB);
    ListNode * res(0);
    int lengthA(0);
    int lengthB(0);

    // get the length of the A linked list
    while(listA != 0) {
        lengthA++;
        listA = listA->next;
    }

    // corner case
    if(lengthA == 0) return 0;

    // put into a vector all the nodes of the list A
    std::vector<ListNode*> vecA{};
    vecA.resize(lengthA);
    listA = headA;

    for(int i = 0; i != lengthA; i++) {
        vecA[i] = listA;
        listA = listA->next;
    }

    // get the length of the B linked list
    while(listB != 0) {
        lengthB++;
        listB = listB->next;
    }

    // corner case
    if(lengthB == 0) return 0;

    // put into a vector all the nodes of the list B
    std::vector<ListNode*> vecB{};
    vecB.resize(lengthB);
    listB = headB;

    for(int i = 0; i != lengthB; i++) {
        vecB[i] = listB;
        listB = listB->next;
    }

    // reverse the vectors
    std::reverse(vecA.begin(), vecA.end());
    std::reverse(vecB.begin(), vecB.end());

    unsigned int i(0);
    bool inside(false);

    while(vecA[i] == vecB[i]) {
        inside = true;
        ++i;
        if(i > vecA.size() || i > vecB.size()) break;
    }

    if(inside) res = vecA[i-1];

    return res;
}

// end
