//=========//
// two sum //
//=========//

#include <iostream>
#include <iomanip>
#include <cmath>
#include <chrono>

using std::cout;
using std::endl;
using namespace std::chrono;

#include "Solution.hpp"

// macros
// allowed names are:
// twoSumSTL_v1
// twoSumNoSTL_v1
// twoSum_v3

#define twoSum twoSum_v3

// the main function

int main()
{
    // parameters

    const unsigned int SIZE_VEC(static_cast<int>(pow(10.0, 5)));
    const int TRIALS(static_cast<int>(pow(10.0, 1)));

    // input fixed and local variables

    cout << std::setprecision(10);
    Solution sol1;
    auto t1 = system_clock::now();
    auto t2 = system_clock::now();
    duration<double> time_span;
    std::vector<int> res {};

    // input # 1

    std::vector<int> v1 {1,2,3,4,5,6};
    res = sol1.twoSum(v1, 6);

    cout << " --> 1" << endl;
    cout << " --> index1 = " << res[0] << endl;
    cout << " --> index2 = " << res[1] << endl;

    // input # 2

    std::vector<int> v2 {1,2,3,4,5,6};
    res = sol1.twoSum(v2, 11);

    cout << " --> 2" << endl;
    cout << " --> index1 = " << res[0] << endl;
    cout << " --> index2 = " << res[1] << endl;

    // input # 3

    std::vector<int> v3 {2,2,2,2,1,2};
    res = sol1.twoSum(v3, 4);

    cout << " --> 3" << endl;
    cout << " --> index1 = " << res[0] << endl;
    cout << " --> index2 = " << res[1] << endl;

    // input # 4

    std::vector<int> v4 {1,2,2,2,1,10};
    res = sol1.twoSum(v4, 11);

    cout << " --> 4" << endl;
    cout << " --> index1 = " << res[0] << endl;
    cout << " --> index2 = " << res[1] << endl;

    // input # 5

    std::vector<int> v5 {-1,2,2,2,10,10};
    res = sol1.twoSum(v5, 9);

    cout << " --> 5" << endl;
    cout << " --> index1 = " << res[0] << endl;
    cout << " --> index2 = " << res[1] << endl;

    // input # 6

    std::vector<int> v6 {1,-20,2,2,10,30};
    res = sol1.twoSum(v6, 10);

    cout << " --> 6" << endl;
    cout << " --> index1 = " << res[0] << endl;
    cout << " --> index2 = " << res[1] << endl;

    // input # 7
    // the benchmark

    // build a large vector

    std::vector<int> v7 {};
    v7.clear();
    v7.shrink_to_fit();
    v7.resize(SIZE_VEC);

    for(unsigned int i = 0; i != SIZE_VEC; i++) {
        v7[i] = i;
    }

    // start the clock

    t1 = system_clock::now();

    // benchmark here

    for(int k = 0; k != TRIALS; k++) {
        res = sol1.twoSum(v7, 2*SIZE_VEC-3);
        cout << " --------------------------------------> " << k << endl;
    }

    t2 = system_clock::now();
    time_span = duration_cast<duration<double>>(t2-t1);

    cout << " --> 7a" << endl;
    cout << " --> index1 = " << res[0] << endl;
    cout << " --> index2 = " << res[1] << endl;
    cout << " --> 7b  --> total time used for " << TRIALS << " applications of the algorithm is "
         << std::fixed << time_span.count() << endl;

    return 0;
}

// end
