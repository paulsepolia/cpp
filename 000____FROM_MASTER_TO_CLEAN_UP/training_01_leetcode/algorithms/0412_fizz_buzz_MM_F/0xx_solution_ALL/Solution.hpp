//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <vector>
#include <string>

class Solution {
public:

    // fizzBuzz_v1
    std::vector<std::string> fizzBuzz_v1(int);
};

#endif

// end
