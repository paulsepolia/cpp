//===========//
// Fizz Buzz //
//===========//

#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// fizzBuzz_v1

#define fizzBuzz fizzBuzz_v1

// the main function

int main()
{
    Solution sol1;
    int n(0);
    std::vector<std::string> res{};

    // # 1
    cout << "------------------------------------>> 1" << endl;
    n = 0;
    res = sol1.fizzBuzz(n);
    for(const auto & i: res) {
        cout << i << endl;
    }

    // # 2
    cout << "------------------------------------>> 2" << endl;
    n = 2;
    res = sol1.fizzBuzz(n);
    for(const auto & i: res) {
        cout << i << endl;
    }

    // # 3
    cout << "------------------------------------>> 3" << endl;
    n = 15;
    res = sol1.fizzBuzz(n);
    for(const auto & i: res) {
        cout << i << endl;
    }

    return 0;
}

// end
