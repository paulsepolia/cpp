//===========================//
// Solution class definition //
//===========================//

#include "Solution.hpp"

#include <iostream>
using std::cout;
using std::endl;

// maxProfit_v1
// CORRECT BUT SLOW

int Solution::maxProfit_v1(std::vector<int>& prices)
{
    const unsigned int DIM(prices.size());

    // corner case
    if(DIM == 0) return 0;

    // initialize the possible max diff
    int max_diff = prices[1] - prices[0];
    int diff(0);

    // main loop
    for(unsigned int i = 0; i != DIM; i++) {
        for(unsigned int j = i+1; j != DIM; j++) {
            diff = prices[j] - prices[i];
            if(diff > 0 && diff > max_diff) {
                max_diff = diff;
            }
        }
    }

    if(max_diff <= 0) {
        max_diff = 0;
    }

    return max_diff;
}

// maxProfit_v2
// CORRECT AND FAST

int Solution::maxProfit_v2(std::vector<int>& prices)
{
    const unsigned int DIM(prices.size());

    // corner case
    if(DIM == 0) return 0;

    // Initialize diff, current sum and max sum
    int diff = prices[1]-prices[0];
    int curr_sum = diff;
    int max_sum = curr_sum;

    for(unsigned int i = 1; i < DIM-1; i++) {
        // Calculate current diff
        diff = prices[i+1]-prices[i];

        // Calculate current sum
        if (curr_sum > 0) {
            curr_sum += diff;
        } else {
            curr_sum = diff;
        }

        // Update max sum, if needed
        if (curr_sum > max_sum) {
            max_sum = curr_sum;
        }
    }

    if(max_sum < 0) {
        max_sum = 0;
    }

    return max_sum;
}

// end
