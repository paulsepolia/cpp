//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <vector>

class Solution {
public:

    // maxProfit_v1
    int maxProfit_v1(std::vector<int>&);

    // maxProfit_v2
    int maxProfit_v2(std::vector<int>&);
};

#endif

// end
