//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <vector>

class Solution {
public:
    // generate_v1
    std::vector<std::vector<int>> generate_v1(int);
};

#endif

// end
