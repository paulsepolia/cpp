//===========================//
// Solution class definition //
//===========================//

#include <algorithm>
#include <string>
#include <cmath>
#include <vector>
#include "Solution.hpp"

#include <iostream>
using std::endl;
using std::cout;

// happyNumber_v1
// iterative

bool Solution::happyNumber_v1(int num)
{
    bool res(false);
    bool val(true);

    std::vector<int> pool {};
    int sum(num);
    std::string str {};
    str = std::to_string(sum);

    while(val) {

        // reset sum here
        sum = 0;

        // get the new sum here
        for(unsigned int i = 0; i != str.size(); i++) {
            sum = sum + std::pow(std::stoi(std::to_string(str[i]-'0')), 2.0);
        }

        // put the result in the pool
        pool.push_back(sum);

        // test here
        if(sum == 1) {
            res = true;
            val = false;
        } else {
            for(unsigned int i = 0; i != pool.size()-1; i++) {
                if(sum == pool[i]) { // if sum already exists break and exit
                    res = false;
                    val = false;
                    break;
                }
            }
        }

        // set the new string here
        str = std::to_string(sum);
    }

    return res;
}

// happyNumber_v2
// recursive

bool Solution::happyNumber_v2(int n)
{
    if (n == 1 || n == 7) {
        return true;
    }

    if (n < 10) {
        return false;
    }

    int sum = 0;

    while(n) {
        sum = sum + (n%10)*(n%10);
        n = n/10;
    }

    return happyNumber_v2(sum);
}

// happyNumber_v3
// iterative

bool Solution::happyNumber_v3(int n)
{
    int sum = 0;
    bool res(false);

    if (n == 1 || n == 7) {
        res = true;
    }

    if (n < 10) {
        res = false;
    }

    while(true) {
        while(n) {
            sum = sum + (n%10)*(n%10);
            n = n/10;
        }
        if(sum == 1 || sum == 7) {
            res = true;
            break;
        } else if(sum < 10) {
            res = false;
            break;
        } else {
            n = sum;
            sum = 0;
        }
    }

    return res;
}

// end
