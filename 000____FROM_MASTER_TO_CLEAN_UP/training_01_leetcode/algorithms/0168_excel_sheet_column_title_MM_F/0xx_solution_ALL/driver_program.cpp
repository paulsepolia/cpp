//==========================//
// Excel Sheet Column Title //
//==========================//

#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"
#include <cmath>

// macros
// allowed names are:
// convertToTitle_v1

#define convertToTitle convertToTitle_v1

// the main function

int main()
{
    Solution sol1;
    int num(0);
    std::string res("");

    // # 1
    cout << "-------------------------->> 1" << endl;
    num = 100;
    res = sol1.convertToTitle(num);
    cout << " num = " << num << endl;
    cout << " res = " << res << endl;

    // # 2
    cout << "-------------------------->> 2" << endl;
    num = 20000;
    res = sol1.convertToTitle(num);
    cout << " num = " << num << endl;
    cout << " res = " << res << endl;

}

// end
