//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

#include <vector>

class Solution {
public:

    // maxRotateFunction_v1
    int maxRotateFunction_v1(std::vector<int>&);

    // maxRotateFunction_v2
    int maxRotateFunction_v2(std::vector<int>&);
};

#endif

// end
