//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

// ListNode definition

struct ListNode {
    int val;
    ListNode * next;
    ListNode(int x) : val(x), next(0) {}
};

// Solution declaration

class Solution {
public:

    // reverseList_v1
    ListNode * reverseList_v1(ListNode *);

    // reverseList_v2
    ListNode * reverseList_v2(ListNode *);

    // reverseList_v3 - recursive
    ListNode * reverseList_v3(ListNode *);
};

#endif

// end
