//===========================//
// Solution class definition //
//===========================//

#include <vector>
#include <algorithm>
#include "Solution.hpp"

// reverseList_v1

ListNode * Solution::reverseList_v1(ListNode * list)
{
    // save the head of the linked list
    ListNode * const HEAD(list);
    ListNode * res(0);

    // corner case
    if(list == 0) {
        return 0;
    }

    // put the elements of the linked list into a vector
    std::vector<ListNode*> vecR{};
    while(list != 0) {
        vecR.push_back(list);
        list = list->next;
    }

    // reverse the vector
    std::reverse(vecR.begin(), vecR.end());

    // build the reversed list
    res = vecR[0];
    unsigned int i(0);

    while(vecR[i] != HEAD) {
        ++i;
        res->next = vecR[i];
        res = res->next;
    }

    // put the HEAD as the last element
    res->next = HEAD;
    res = res->next;

    // finalize the new linked list
    res->next = 0;

    // initialize and return
    res = vecR[0];

    return res;
}

// reverseList_v2

ListNode * Solution::reverseList_v2(ListNode * head)
{
    if(head == 0) {
        return 0;
    }

    ListNode* previous(0);
    ListNode* current(head);
    ListNode* next2(0);

    while(current->next != 0) {
        next2 = current->next;
        // revert here
        current->next = previous;
        // and here
        previous = current;
        current = next2;
    }

    current->next = previous;

    return current;
}

// reverseList_v3 - recursive

// the reverse function

ListNode* reverse(ListNode* pivot, ListNode* backward = 0)
{

    // corner case
    if (pivot == 0) return backward;
    // flip the head of pivot from forward to backward
    ListNode* rest = pivot->next;
    pivot->next = backward;
    // and continue
    return reverse(rest, pivot);
}

ListNode * Solution::reverseList_v3(ListNode * head)
{
    return reverse(head);
}

// end
