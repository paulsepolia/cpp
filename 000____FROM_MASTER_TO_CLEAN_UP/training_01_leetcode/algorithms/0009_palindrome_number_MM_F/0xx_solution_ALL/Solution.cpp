//===========================//
// Solution class definition //
//===========================//

#include <string>
#include <algorithm>
#include <cmath>

using std::string;
using std::reverse;
using std::to_string;
using std::pow;

#include "Solution.hpp"

// isPalindromeSTL

bool Solution::isPalindromeSTL(int x)
{
    // --> step # 1
    // --> a negative number is not a palindrome number

    if(x < 0) {
        return false;
    }

    // --> step # 2
    // --> convert integer to string

    string str1(to_string(x));

    // --> step # 3
    // --> reverse the string

    string str1_rev(str1);
    reverse(str1_rev.begin(), str1_rev.end());

    // --> step # 4
    // --> compare the two strings

    if (str1.compare(str1_rev) != 0) {
        return false;
    }

    return true;
}

// isPalindromeNoSTL

bool Solution::isPalindromeNoSTL(int x)
{
    // --> step # 1
    // --> a negative number is not a palindrome number

    if(x < 0) {
        return false;
    }

    // --> step # 2

    int n(x);
    int remainder(0);
    int reverseInteger(0);

    while(n != 0) {
        remainder = n%10;
        reverseInteger = reverseInteger*10 + remainder;
        n /= 10;
    }

    if (x != reverseInteger) {
        return false;
    }

    return true;
}

//=====//
// end //
//=====//
