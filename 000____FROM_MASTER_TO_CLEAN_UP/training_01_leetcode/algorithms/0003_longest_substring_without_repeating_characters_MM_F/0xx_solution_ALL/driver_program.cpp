//==========================================================//
// length Of longest substring without repeating characters //
//==========================================================//

#include <iostream>
#include <iomanip>
#include <cmath>
#include <chrono>
#include <set>

using std::cout;
using std::endl;
using std::setw;
using std::fixed;
using std::setprecision;
using std::right;
using std::pow;
using std::set;
using namespace std::chrono;

#include "Solution.hpp"

// macros
// allowed names are:
// lengthOfLongestSubstringSTL _v1
// lengthOfLongestSubstringNoSTL_v1

#define lengthOfLongestSubstring  lengthOfLongestSubstringSTL_v2

// the main function

int main()
{
    // parameters

    const unsigned int SIZE_STR(static_cast<int>(pow(10.0, 5)));
    const int TRIALS(static_cast<int>(pow(10.0, 1)));

    // input fixed and local variables

    cout << setprecision(10);
    Solution sol1;
    auto t1 = system_clock::now();
    auto t2 = system_clock::now();
    duration<double> time_span;
    string s1 {};
    int len(0);

    // test cases

    // # 1

    s1 = {};
    cout << " -->  1 --> string = " << s1 << endl;
    len = sol1.lengthOfLongestSubstring(s1);
    cout << " -->  2 --> len = " << len << endl;

    // # 2

    s1 = "";
    cout << " -->  3 --> string = " << s1 << endl;
    len = sol1.lengthOfLongestSubstring(s1);
    cout << " -->  4 --> len = " << len << endl;

    // # 3

    s1 = "1";
    cout << " -->  5 --> string = " << s1 << endl;
    len = sol1.lengthOfLongestSubstring(s1);
    cout << " -->  6 --> len = " << len << endl;

    // # 4

    s1 = "1234567890";
    cout << " -->  7 --> string = " << s1 << endl;
    len = sol1.lengthOfLongestSubstring(s1);
    cout << " -->  8 --> len = " << len << endl;

    // # 5

    s1 = "12345678901234567890";
    cout << " -->  9 --> string = " << s1 << endl;
    len = sol1.lengthOfLongestSubstring(s1);
    cout << " --> 10 --> len = " << len << endl;

    // # 6

    s1 = "12345678901234567890abcdefghijklmn";
    cout << " --> 11 --> string = " << s1 << endl;
    len = sol1.lengthOfLongestSubstring(s1);
    cout << " --> 12 --> len = " << len << endl;
    // # 7

    s1 = "aaaaaaaaaaaaaaaa";
    cout << " --> 13 --> string = " << s1 << endl;
    len = sol1.lengthOfLongestSubstring(s1);
    cout << " --> 14 --> len = " << len << endl;

    // # 8

    s1 = "2aaaaaaaaaaaaaaaa1";
    cout << " --> 15 --> string = " << s1 << endl;
    len = sol1.lengthOfLongestSubstring(s1);
    cout << " --> 16 --> len = " << len << endl;

    // # 9

    s1 = "dvdf";
    cout << " --> 17 --> string = " << s1 << endl;
    len = sol1.lengthOfLongestSubstring(s1);
    cout << " --> 18 --> len = " << len << endl;

    // # 10
    // the benchmark

    // build the string

    /*
         string s2 {"1234"};
         string s3 {};

         cout << " --> 17 --> building the long string ..." << endl;

         for(unsigned int i = 0; i != SIZE_STR; i++) {
              s3 = s3 + s2;
         }

         // start the clock

         t1 = system_clock::now();

         // benchmark here

         for(int k = 0; k != TRIALS; k++) {
              len = sol1.lengthOfLongestSubstring(s3);
              cout << " --------------------------------------> " << k << endl;
         }

         t2 = system_clock::now();
         time_span = duration_cast<duration<double>>(t2-t1);

         cout << " --> 18 --> lenght must be = " << s2.length() << endl;
         cout << " --> 19 --> length         = " << len << endl;
         cout << " --> 20 --> total time used for " << TRIALS << " applications of the algorithm is "
              << fixed << time_span.count() << " seconds" << endl;

    */
    return 0;
}

//=====//
// end //
//=====//
