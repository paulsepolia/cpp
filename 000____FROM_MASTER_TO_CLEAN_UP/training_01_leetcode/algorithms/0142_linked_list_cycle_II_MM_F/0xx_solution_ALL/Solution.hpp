//============================//
// Solution class declaration //
//============================//

#ifndef SOLUTION_H
#define SOLUTION_H

// ListNode definition

struct ListNode {
    int val;
    ListNode * next;
    ListNode(int x) : val(x), next(0) {}
};

// Solution declaration

class Solution {
public:

    // detectCycle_v1
    ListNode* detectCycle_v1(ListNode *);

    // detectCycle_v2
    ListNode* detectCycle_v2(ListNode *);

    // detectCycle_v3
    ListNode* detectCycle_v3(ListNode *);
};

#endif

// end
