//===========================//
// linked list with cycle II //
//===========================//

#include <iostream>
#include <iomanip>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// detectCycle_v1
// detectCycle_v2
// detectCycle_v3

#define detectCycle detectCycle_v3

// the main function

int main()
{
    ListNode * nA(0);
    ListNode * res(0);
    cout << std::fixed;
    cout << std::boolalpha;
    Solution sol1;
    res = sol1.detectCycle(nA);

    cout << " &res = " << &res << endl;

    return 0;
}

// end
