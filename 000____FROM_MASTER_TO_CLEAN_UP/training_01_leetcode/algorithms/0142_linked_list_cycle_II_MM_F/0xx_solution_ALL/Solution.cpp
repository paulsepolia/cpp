//===========================//
// Solution class definition //
//===========================//

#include <vector>
#include <algorithm>
#include <set>
#include "Solution.hpp"

// detectCycle_v1

ListNode* Solution::detectCycle_v1(ListNode * head)
{
    // corner cases
    if(head == 0) return 0;
    if(head->next == 0) return 0;

    // create a set and put the pointers there
    // check if the size is increazed after each insert
    std::set<ListNode*> aSet{};
    std::pair<std::set<ListNode*>::iterator, bool> ret;
    ListNode * aNode(head);

    while(aNode != 0) {
        ret = aSet.insert(aNode);
        if (ret.second == false) return aNode;
        aNode = aNode->next;
    }

    return 0;
}

// detectCycle_v2
// SLOW - NOT ACCEPTED

ListNode* Solution::detectCycle_v2(ListNode * head)
{
    // corner cases
    if(head == 0) return 0;
    if(head->next == 0) return 0;

    // create a vector and put the pointers there
    // if you get same vaue break;
    std::vector<ListNode*> aVec{};
    ListNode * aNode (head);

    while(aNode != 0) {
        aVec.push_back(aNode);
        auto it = std::find(aVec.begin(), aVec.end(), aNode->next);
        if(it != aVec.end()) return aNode->next;
        aNode = aNode->next;
    }

    return 0;
}

// detectCycle_v3

ListNode* Solution::detectCycle_v3(ListNode * head)
{
    if (head == 0) {
        return 0;
    }

    ListNode * p(head);
    ListNode * q(head);

    while (true) {
        if (p->next != 0) {
            p = p->next;
        } else {
            return 0;
        }

        if (q->next != 0 && q->next->next != 0) {
            q = q->next->next;
        } else {
            return 0;
        }

        if (p == q) {
            //if find the loop, then looking for the loop start
            q = head;
            while (p != q) {
                p = p->next;
                q = q->next;
            }
            return p;
        }
    }
}

// end
