//==========//
// plus one //
//==========//

#include <iostream>
using std::cout;
using std::endl;
#include "Solution.hpp"

// macros
// allowed names are:
// plusOne_v1

#define plusOne plusOne_v1

// the main function

int main()
{
    Solution sol1;

    {
        cout << "-------------------------------->> 1" << endl;
        std::vector<int> vec{1,2,3,4,5};
        vec = sol1.plusOne(vec);
        for(auto & i: vec) {
            cout << i << " ";
        }
        cout << endl;
    }

    {
        cout << "-------------------------------->> 1" << endl;
        std::vector<int> vec{9,9,9,9,9};
        vec = sol1.plusOne(vec);
        for(auto & i: vec) {
            cout << i << " ";
        }
        cout << endl;
    }

    return 0;
}

// end
