//================//
// C++11, Threads //
//================//

#include <iostream>
#include <ctime>
#include <cmath>
#include <iomanip>
#include <thread>

// a function

void funA(int i)
{
    std::cout << " -----------------------------------> " << i << std::endl;
}

// the main function

int main()
{
    // local variables and parameters

    int i;
    const int I_MAX = static_cast<int>(pow(10.0, 7.0));

    // setting the output format

    std::cout << std::fixed;
    std::cout << std::setprecision(5);
    std::cout << std::showpos;
    std::cout << std::showpoint;

    // the main do-loop

    for(i = 0; i < I_MAX; i++) {
        std::thread th1(funA, i);
        std::thread th2(funA,-i);
        th1.join();
        th2.join();
    }

    int sentinel;
    std::cin >> sentinel;
}

//======//
// FINI //
//======//
