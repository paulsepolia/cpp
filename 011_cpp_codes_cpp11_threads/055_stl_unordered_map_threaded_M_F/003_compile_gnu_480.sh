#!/bin/bash

  # 1. compile

  g++-4.8   -O3                \
            -Wall              \
            -std=c++0x         \
	    -pthread           \
	    -fopenmp           \
	    -static            \
            driver_program.cpp \
            -o x_gnu_480
