
//===========//
// functions //
//===========//

#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <algorithm>
#include <iostream>
#include <vector>
#include <iomanip>
#include <thread>

using std::cout;
using std::cin;
using std::endl;
using std::vector;
using std::prev_permutation;
using std::fixed;
using std::setprecision;
using std::showpos;
using std::showpoint;
using std::thread;

// the bench function

void benchFunHeap(const int & I_MAX)
{
    vector<double> vecA;

    // set the output format

    cout << fixed;
    cout << setprecision(2);
    cout << showpos;
    cout << showpoint;

    // build vector

    for(int i = 0; i < I_MAX; i++) {
        vecA.push_back(cos(static_cast<double>(i)));
    }

    // do

    long counter = 0;

    do {
        // counter

        counter = counter + 1;

        // try a new permutation

        prev_permutation(vecA.begin(), vecA.end());

    } while (!is_sorted(vecA.begin(),vecA.end()));

    cout << "-------------------->> the range is sorted!" << endl;
}

#endif // FUNCTIONS_H 

//======//
// FINI //
//======//
