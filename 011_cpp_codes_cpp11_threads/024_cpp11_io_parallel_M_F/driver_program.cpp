//============================//
// Read and Write binary file //
// using buffers              //
//============================//

#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <thread>
#include <ctime>
#include <iomanip>
#include <vector>
#include <sstream>

// using

using std::string;
using std::ofstream;
using std::ifstream;
using std::cout;
using std::endl;
using std::ios;
using std::flush;
using std::thread;
using std::this_thread::get_id;
using std::showpoint;
using std::setprecision;
using std::fixed;
using std::vector;
using std::stringstream;

// functions prototypes

void writeFun(const string &, const long &, double *);
void readFun(const string &, const long &);

// the main function

int main()
{
    // 1. variables and parameters

    const long ARRAY_DIM = static_cast<long>(pow(10.0, 7.0));
    const int NUM_THREADS = 8;
    double* arrayLocal = new double [ARRAY_DIM];
    const long I_MAX = 1000000;
    long i;
    int j;
    string stringFileName;
    vector<thread> vecThreads;
    string * fileName = new string [NUM_THREADS];
    stringstream ss;
    string s_tmp;

    // 2. create the file names

    for (j = 0; j < NUM_THREADS; j++) {
        ss << j;
        ss >> s_tmp;
        stringFileName = "my_file_" + s_tmp + ".bin";
        fileName[j] = stringFileName;

        // reset stream - must step

        ss.str(string());
        ss.clear();
    }

    // 3. set the output format

    cout << fixed;
    cout << setprecision(5);
    cout << showpoint;

    // 4. build the array

    for (i = 0; i < ARRAY_DIM; i++) {
        arrayLocal[i] = cos(static_cast<double>(i));
    }

    // 5. main do-loop

    for (i = 0; i < I_MAX; i++) {
        cout << endl;
        cout << "---------------------------------------------------------------------------------->> "
             << i << " / " << I_MAX << endl;
        cout << endl;

        // span the threads

        for (j = 0; j < NUM_THREADS; j++) {
            vecThreads.push_back(thread(writeFun, fileName[j], ARRAY_DIM, arrayLocal));
        }

        // join the threads

        for (auto& t : vecThreads) {
            t.join();
        }

        // clear the threads vector

        vecThreads.clear();

        cout << endl;

        // spawn the threads

        for (j = 0; j < NUM_THREADS; j++) {
            vecThreads.push_back(thread(readFun, fileName[j], ARRAY_DIM));
        }

        // join the threads

        for (auto& t : vecThreads) {
            t.join();
        }

        // clear the threads container

        vecThreads.clear();
    }

    // exit

    return 0;
}

//==================//
// writing function //
//==================//

void writeFun(const string & fileName, const long & dim, double * arrayLoc)
{
    // local variables and parameters

    ofstream fileOut;
    clock_t t1;
    clock_t t2;

    // start time

    t1 = clock();

    // open the file

    fileOut.open(fileName.c_str(), ios::out | ios::binary | ios::trunc);

    // check if the file opened with success

    if (!fileOut.is_open()) {
        cout << "Error! The file stream is not opened. Abort." << endl;
    }

    // go to the beggining of the file

    fileOut.seekp(0);

    // write to the file

    fileOut.write(reinterpret_cast<char*>(&arrayLoc[0]), dim * sizeof(double));

    // flush the buffer to ensure the data has been written

    fileOut.flush();

    // close the file

    fileOut.close();

    // end time

    t2 = clock();

    // report

    cout << " --> writing done --> thread id = "
         << get_id()
         << " --> time used = "
         << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;
}

//==================//
// reading function //
//==================//

void readFun(const string & fileName, const long & dim)
{
    // local variables and parameters

    ifstream fileIn;
    clock_t t1;
    clock_t t2;

    double * arrayLoc = new double [dim];

    // start time

    t1 = clock();

    // open the file

    fileIn.open(fileName.c_str(), ios::in  | ios::binary);

    // rewind the file

    fileIn.seekg(0, fileIn.beg);

    // read the file

    fileIn.read(reinterpret_cast<char*>(&arrayLoc[0]), dim * sizeof(double));

    // close the file

    fileIn.close();

    // delete the container

    delete [] arrayLoc;

    // end time

    t2 = clock();

    // report

    cout << " --> reading done --> thread id = "
         << get_id()
         << " --> time used = "
         << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;
}

//======//
// FINI //
//======//

