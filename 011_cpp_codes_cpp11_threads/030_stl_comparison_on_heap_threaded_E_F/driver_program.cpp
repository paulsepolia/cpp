//======================//
// STL clear and delete //
//======================//

#include "includes.h"
#include "bench_fun.h"

// the main program

int main()
{
    // variables and parameters

    const long K_MAX = static_cast<long>(pow(10.0, 6.0));
    const long I_MAX = static_cast<long>(pow(10.0, 7.5));
    const int NUM_THREADS = 2;
    long k;
    int i;
    vector<thread> vecThreads;
    clock_t t1;
    clock_t t2;
    double tall;

    // main bench loop

    for (k = 0; k < K_MAX; k++) {
        cout << endl;
        cout << "------------------------------------------------------------>>> " << k << endl;
        cout << endl;

        // spawn threads

        for (i = 0; i < NUM_THREADS; i++) {
            vecThreads.push_back(thread(&bench_fun, I_MAX));
        }

        // join threads

        for (auto & t : vecThreads) {
            t.join();
        }

        // clear vector

        cout << " xx --> clear the threads vector ..." << endl;

        t1 = clock();

        vecThreads.clear();

        t2 = clock();

        tall = (t2-t1) / static_cast<double>(CLOCKS_PER_SEC);

        cout << " yy --> done with threads vector   --> " << tall << endl;

    }

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
