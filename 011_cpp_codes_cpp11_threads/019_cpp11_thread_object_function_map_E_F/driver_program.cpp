//============//
// c++11, map //
//============//

#include <iostream>
#include <iomanip>
#include <cmath>
#include <map>
#include <algorithm>
#include <ctime>
#include <thread>

using std::cout;
using std::cin;
using std::endl;
using std::map;
using std::less;
using std::thread;
using std::pair;

//==========//
// buildMap //
//==========//

void buildMap(map<double, long>* mymap, const long imax)
{
    long i;
    for(i = 0; i < imax; i++) {
        (*mymap).insert(pair<double, long>(cos(static_cast<double>(i)), i));
    }
}

//==========//
// clearMap //
//==========//

void clearMap(map<double, long>* mymap)
{
    (*mymap).clear();
}

//============//
// emplaceMap //
//============//

void emplaceMap(map<double, long>* mymap, const long imax)
{
    long i;
    for(i = 0; i < imax; i++) {
        (*mymap).emplace(pair<double, long>(cos(static_cast<double>(i)), i));
    }
}

//==============//
// findEraseMap //
//==============//

void findEraseMap(map<double, long>* mymap, const long imax)
{
    long i;
    for (i = 0; i < imax; i++) {
        (*mymap).erase((*mymap).find(cos(static_cast<double>(i))));
    }
}

// the main function

int main()
{
    // parameters

    const long int I_MAX = static_cast<long int>(pow(10.0, 7.0));
    const long int K_MAX = static_cast<long int>(pow(10.0, 7.0));
    long int k;

    for (k = 0; k < K_MAX; k++) {
        // counter

        cout << endl;
        cout << "------------------------------------------------------>> " << k << endl;
        cout << endl;

        // declare the maps

        map<double, long>* M1 = new map<double, long> [1];
        map<double, long>* M2 = new map<double, long> [1];
        map<double, long>* M3 = new map<double, long> [1];
        map<double, long>* M4 = new map<double, long> [1];

        // build the maps in parallel

        cout << "--> maps are being built in parallel..." << endl;

        thread th01(buildMap, M1, I_MAX);
        thread th02(buildMap, M2, I_MAX);
        thread th03(buildMap, M3, I_MAX);
        thread th04(buildMap, M4, I_MAX);

        // finalize the building threads

        cout << "--> finalizing the building threads..." << endl;

        th01.join();
        th02.join();
        th03.join();
        th04.join();

        // emplace maps in parallel

        cout << "--> maps are being emplaced in parallel..." << endl;

        thread th05(emplaceMap, M1, I_MAX);
        thread th06(emplaceMap, M2, I_MAX);
        thread th07(emplaceMap, M3, I_MAX);
        thread th08(emplaceMap, M4, I_MAX);

        // finalize the emplacing threads

        cout << "--> finalizing the emplacing threads..." << endl;

        th05.join();
        th06.join();
        th07.join();
        th08.join();

        // erasing maps via find-maps in parallel

        cout << "--> maps are being erased via find-and-erase in parallel..." << endl;

        thread th09(findEraseMap, M1, I_MAX);
        thread th10(findEraseMap, M2, I_MAX);
        thread th11(findEraseMap, M3, I_MAX);
        thread th12(findEraseMap, M4, I_MAX);

        // finalize the find-and-erase threads

        cout << "--> finalizing the find-and-erase threads..." << endl;

        th09.join();
        th10.join();
        th11.join();
        th12.join();

        // clear the maps in parallel

        cout << "--> maps are being cleared in parallel..." << endl;

        thread th13(clearMap, M1);
        thread th14(clearMap, M2);
        thread th15(clearMap, M3);
        thread th16(clearMap, M4);

        // finalize the clearing threads

        cout << "--> finalizing the clearing threads..." << endl;

        th13.join();
        th14.join();
        th15.join();
        th16.join();

        // delete maps

        cout << "--> deleting the threads..." << endl;

        delete [] M1;
        delete [] M2;
        delete [] M3;
        delete [] M4;
    }

    // the sentinel

    int sentinel;
    cin >> sentinel;
}

//======//
// FINI //
//======//

