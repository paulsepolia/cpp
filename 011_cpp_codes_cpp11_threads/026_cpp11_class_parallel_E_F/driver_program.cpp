//=================//
// friend function //
//=================//

#include <iostream>
#include <cmath>
#include <vector>
#include <list>
#include <deque>
#include <iomanip>
#include <string>
#include <thread>
#include <sstream>

// using

using std::cout;
using std::cin;
using std::endl;
using std::vector;
using std::deque;
using std::list;
using std::fixed;
using std::setprecision;
using std::showpoint;
using std::string;
using std::thread;
using std::stringstream;

// a class with a friend function

template <typename T>
class ContainerCL {
public:

    void buildContainer();
    void clearContainer();
    void shrinkToFitContainer();
    void showDim();
    void getSize();

private:

    const long DIM_VEC = static_cast<long>(pow(10.0, 7.6));
    T containerA;
};

// buildContainer --> member function definition

template <typename T>
void ContainerCL<T>::buildContainer()
{
    for (long i = 0; i < DIM_VEC; i++) {
        containerA.push_back(cos(static_cast<double>(i)));
    }
}

// clearContainer --> member function definition

template <typename T>
void ContainerCL<T>::clearContainer()
{
    containerA.clear();
}

// shrinkToFitContainer --> member function definition

template <typename T>
void ContainerCL<T>::shrinkToFitContainer()
{
    containerA.shrink_to_fit();
}

// showDim --> member function

template <typename T>
void ContainerCL<T>::showDim()
{
    cout << " --> DIM_VEC = " << DIM_VEC << endl;
}

// a template function

template <typename T>
void runBench(ContainerCL<T> obj , string s1)
{
    cout << " --> building the " << s1 << endl;

    obj.buildContainer();

    cout << " --> clearing the " << s1 << endl;

    obj.clearContainer();
}

// the main function

int main()
{
    ContainerCL<vector<double>> vecA;
    ContainerCL<deque<double>> deqA;
    ContainerCL<list<double>> listA;
    const int MAX_DO = 100000;
    int i;
    int j;
    const int NUM_THREADS = 4;
    vector<thread> vecThreads;
    string * sA = new string [NUM_THREADS];
    stringstream ss;
    string s_tmp;

    for (i = 0; i < MAX_DO; i++) {
        cout << "----------------------------------------------------------->> " << i << " / " << MAX_DO << endl;

        //=========//
        // vectors //
        //=========//

        for (j = 0; j < NUM_THREADS; j++) {
            // create the name

            ss << j;
            ss >> s_tmp;
            sA[j] = "vector_" + s_tmp;

            // reset stream - must step

            ss.str(string());
            ss.clear();
        }

        // spawn threads

        for (j = 0; j < NUM_THREADS; j++) {
            vecThreads.push_back(thread(&runBench<vector<double>>, vecA, sA[j]));
        }

        // join the threads

        for (auto& t : vecThreads) {
            t.join();
        }

        // clear the thread vector

        vecThreads.clear();

        //========//
        // deques //
        //========//

        for (j = 0; j < NUM_THREADS; j++) {
            ss << j;
            ss >> s_tmp;
            sA[j] = "deque_" + s_tmp;

            // reset stream - must step

            ss.str(string());
            ss.clear();
        }

        // spawn threads

        for (j = 0; j < NUM_THREADS; j++) {
            vecThreads.push_back(thread(&runBench<deque<double>>, deqA, sA[j]));
        }

        // join the threads

        for (auto& t : vecThreads) {
            t.join();
        }

        // clear the thread vector

        vecThreads.clear();

        //=======//
        // lists //
        //=======//

        for (j = 0; j < NUM_THREADS; j++) {
            // create the name

            ss << j;
            ss >> s_tmp;
            sA[j] = "list_" + s_tmp;

            // reset stream - must step

            ss.str(string());
            ss.clear();
        }

        // spawn threads

        for (j = 0; j < NUM_THREADS; j++) {
            vecThreads.push_back(thread(&runBench<list<double>>, listA, sA[j]));
        }

        // join the threads

        for (auto& t : vecThreads) {
            t.join();
        }

        // clear the thread vector

        vecThreads.clear();
    }

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
