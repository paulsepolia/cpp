
#include <iostream>
#include <cmath>
#include <thread>
#include <vector>

using std::endl;
using std::cout;
using std::cin;
using std::pow;
using std::vector;
using std::thread;

// bench function

void benchFun()
{
    const long I_MAX = static_cast<long>(pow(10.0, 9.0));
    const long J_MAX = static_cast<long>(pow(10.0, 9.0));

    for (long j = 0; j < J_MAX; j++) {
        for (long i = 0; i < I_MAX; i++) {
            double *p;
            p = new double;
            delete p;
            p = NULL;
        }
    }
}

// the main function

int main()
{

    const int NUM_THREADS = 80;

    vector<thread> vecTh;

    for (int j = 0; j < NUM_THREADS; j++) {
        vecTh.push_back(thread(benchFun));
    }

    for (auto & t : vecTh) {
        t.join();
    }

    vecTh.clear();

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//