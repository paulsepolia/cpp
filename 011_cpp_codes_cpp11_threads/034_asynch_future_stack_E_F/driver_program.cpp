//==================//
// future and async //
//==================//

#include "includes.h"
#include "buildListFunction.h"

// the main function

int main()
{
    // local variables and parameters

    const long MAX_DIM = static_cast<long>(pow(10.0, 7.0));
    const int  NUM_THREADS = 4;
    const int  I_MAX = 1000000;
    clock_t t1;
    clock_t t2;
    double tall;

    // output style

    cout << setprecision(10);
    cout << fixed;
    cout << showpos;
    cout << showpoint;

    // main loop

    for (int i = 0; i < I_MAX; i++) {
        cout << "-------------------------------------------------->> " << i << endl;

        // timing

        t1 = clock();

        // declare the threaded function

        future<double> * f = new future<double> [NUM_THREADS];

        // spawn the threads

        for (int i = 0; i < NUM_THREADS; i++) {
            f[i] = async(launch::async, [MAX_DIM] () {
                return buildList(MAX_DIM);
            });
        }

        // waiting

        cout << "--->> waiting ..." << endl;

        for (int i = 0; i < NUM_THREADS; i++) {
            f[i].wait();
        }

        // getting the values back

        for (int i = 0; i < NUM_THREADS; i++) {
            cout << " --> f[" << i << "] --> " << f[i].get() << endl;
        }

        // timing

        t2 = clock();

        tall = (t2-t1)/static_cast<double>(CLOCKS_PER_SEC);

        cout << " --> total time in main --> " << tall << endl;

    }

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
