
//========================//
// priority_queue adaptor //
//========================//

#include <iostream>
#include <stack>
#include <deque>
#include <vector>
#include <list>
#include <cmath>
#include <thread>

using std::endl;
using std::cout;
using std::cin;
using std::deque;
using std::vector;
using std::stack;
using std::list;
using std::pow;
using std::thread;

// the bench function

void benchFun(const long int DIMEN_MAX)
{
    const long K_MAX = static_cast<long>(pow(10.0, 5.0));

    for(long k = 0; k < K_MAX; k++) {
        // counter

        cout << "------------------------------------------------>> " << k << endl;

        // declare the adaptors

        stack<double> * qA = new stack<double>;
        stack<double, deque<double>> * qB = new stack<double, deque<double>>;
        stack<double, vector<double>> * qC = new stack<double, vector<double>>;
        stack<double, list<double>> * qD = new stack<double, list<double>>;

        // build the containers

        cout << " --> build --> default stack" << endl;

        for (long i = 0; i < DIMEN_MAX; i++) {
            qA->push(static_cast<double>(i));
        }

        cout << " --> build --> stack via deque" << endl;

        for (long i = 0; i < DIMEN_MAX; i++) {
            qB->push(static_cast<double>(i));
        }

        cout << " --> build --> stack via vector" << endl;

        for (long i = 0; i < DIMEN_MAX; i++) {
            qC->push(static_cast<double>(i));
        }

        cout << " --> build --> stack via list" << endl;

        for (long i = 0; i < DIMEN_MAX; i++) {
            qD->push(static_cast<double>(i));
        }

        // pop the containers

        cout << " --> pop --> default stack" << endl;

        for (long i = 0; i < DIMEN_MAX; i++) {
            qA->pop();
        }

        cout << " --> pop --> stack via deque" << endl;

        for (long i = 0; i < DIMEN_MAX; i++) {
            qB->pop();
        }

        cout << " --> pop --> stack via vector" << endl;

        for (long i = 0; i < DIMEN_MAX; i++) {
            qC->pop();
        }

        cout << " --> pop --> stack via list" << endl;

        for (long i = 0; i < DIMEN_MAX; i++) {
            qD->pop();
        }

        // free RAM

        cout << " --> delete --> default stack" << endl;

        delete qA;

        cout << " --> delete --> stack via deque" << endl;

        delete qB;

        cout << " --> delete --> stack via vector" << endl;

        delete qC;

        cout << " --> delete --> stack via list" << endl;

        delete qD;

    }
}

// the main function

int main()
{
    const long int DIMEN_MAX = static_cast<long int>(pow(10.0, 7.0));
    const int NUM_THREADS = 4;
    vector<thread> vecThreads;

    // span the threads

    for (int j = 0; j < NUM_THREADS; j++) {
        vecThreads.push_back(thread(benchFun, DIMEN_MAX));
    }

    // join the threads

    for (auto& t : vecThreads) {
        t.join();
    }

    // clear the threads vector

    vecThreads.clear();

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
