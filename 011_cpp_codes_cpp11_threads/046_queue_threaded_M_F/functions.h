//=====================//
// benchmark functions //
//=====================//

#include <iostream>
#include <queue>
#include <iomanip>
#include <cmath>
#include <vector>
#include <algorithm>

using std::queue;
using std::endl;
using std::cout;
using std::fixed;
using std::setprecision;
using std::showpos;
using std::showpoint;
using std::pow;
using std::vector;
using std::sort;

//======================================================//
// the benchmark function -> benchFunHeap(const long &) //
//======================================================//

void benchFunHeap(const long & I_MAX)
{

    const long J_MAX = static_cast<long>(pow(10.0, 7.0));

    // set format

    cout << setprecision(10);
    cout << fixed;
    cout << showpoint;
    cout << showpos;


    for (long j = 0; j < J_MAX; j++) {

        cout << "--------------------------------------------->> " << j << endl;

        queue<double> * queA = new queue<double>;
        queue<double> * queB = new queue<double>;
        vector<double> * vecA = new vector<double>;
        vector<double> * vecB = new vector<double>;

        cout << " --> push" << endl;

        for (long i = 0; i < I_MAX; i++) {
            queA->push(sin(static_cast<double>(i)));
            queB->push(cos(static_cast<double>(i)));
        }

        cout << " --> pop" << endl;

        for (long i = 0; i < I_MAX; i++) {
            queA->pop();
            queB->pop();
        }

        cout << " --> emplace" << endl;

        for (long i = 0; i < I_MAX; i++) {
            queA->emplace(sin(static_cast<double>(i)));
            queB->emplace(cos(static_cast<double>(i)));
        }

        cout << " --> swap" << endl;

        queA->swap(*queB);

        cout << " --> sort queue via vector" << endl;

        for (long i = 0; i < I_MAX; i++) {
            vecA->push_back(queA->back());
            vecB->push_back(queB->back());

            queA->pop();
            queB->pop();
        }

        sort(vecA->begin(),vecA->end());

        sort(vecB->begin(),vecB->end());

        cout << " --> is_sorted(vecA->begin(), vecA->end() = " << is_sorted(vecA->begin(), vecA->end()) << endl;
        cout << " --> is_sorted(vecA->begin(), vecA->end() = " << is_sorted(vecB->begin(), vecB->end()) << endl;

        for (long i = 0; i < I_MAX; i++) {
            queA->push((*vecA)[i]);
            queB->push((*vecB)[i]);
        }

        cout << " --> pop" << endl;

        for (long i = 0; i < I_MAX; i++) {
            queA->pop();
            queB->pop();
        }

        cout << " --> delete" << endl;

        delete queA;
        delete queB;
        delete vecA;
        delete vecB;

        queA = NULL;
        queB = NULL;
        vecA = NULL;
        vecB = NULL;
    }
}

//=============================================================//
// the benchmark function --> void benchFunStack(const long &) //
//=============================================================//

void benchFunStack(const long & I_MAX)
{
    const long J_MAX = static_cast<long>(pow(10.0, 7.0));

    // set format

    cout << setprecision(10);
    cout << fixed;
    cout << showpoint;
    cout << showpos;


    for (long j = 0; j < J_MAX; j++) {

        cout << "--------------------------------------------->> " << j << endl;

        queue<double> queA;
        queue<double> queB;
        vector<double> vecA;
        vector<double> vecB;

        cout << " --> push" << endl;

        for (long i = 0; i < I_MAX; i++) {
            queA.push(sin(static_cast<double>(i)));
            queB.push(cos(static_cast<double>(i)));
        }

        cout << " --> pop" << endl;

        for (long i = 0; i < I_MAX; i++) {
            queA.pop();
            queB.pop();
        }

        cout << " --> emplace" << endl;

        for (long i = 0; i < I_MAX; i++) {
            queA.emplace(sin(static_cast<double>(i)));
            queB.emplace(cos(static_cast<double>(i)));
        }

        cout << " --> swap" << endl;

        queA.swap(queB);

        cout << " --> sort" << endl;

        for (long i = 0; i < I_MAX; i++) {
            vecA.push_back(queA.back());
            vecB.push_back(queB.back());

            queA.pop();
            queB.pop();
        }

        sort(vecA.begin(),vecA.end());
        sort(vecB.begin(),vecB.end());

        cout << " --> is_sorted(vecA.begin(), vecA.end()) = " << is_sorted(vecA.begin(), vecA.end()) << endl;
        cout << " --> is_sorted(vecA.begin(), vecA.end()) = " << is_sorted(vecB.begin(), vecB.end()) << endl;

        for (long i = 0; i < I_MAX; i++) {
            queA.push(vecA[i]);
            queB.push(vecB[i]);
        }

        cout << " --> pop" << endl;

        for (long i = 0; i < I_MAX; i++) {
            queA.pop();
            queB.pop();
        }

        cout << " --> delete" << endl;
    }
}

//======//
// FINI //
//======//
