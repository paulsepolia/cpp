//================//
// C++11, Threads //
//================//

#include <iostream>
#include <iomanip>
#include <cmath>
#include <ctime>
#include <thread>
#include <deque>
#include <algorithm>

// a class to create the function object

class X {
public:
    void do_lengthy_work(int i);
};

// the public function

void X::do_lengthy_work(int i)
{
    const long int K_MAX = static_cast<long int>(pow(10.0, 7.4));
    long int k;
    double tmpVal;
    std::deque<double> *dque = new std::deque<double> [1];
    clock_t t1;
    clock_t t2;
    double tall;

    // report

    std::cout << " --> I am --> " << i << std::endl;

    // build

    std::cout << " --> building ..." << std::endl;
    t1 = clock();
    for (k = 0; k < K_MAX; k++) {
        tmpVal = cos(static_cast<double>(k));
        (*dque).push_back(tmpVal);
    }
    t2 = clock();
    tall = (t2-t1)/static_cast<double>(CLOCKS_PER_SEC);
    std::cout << " --> build --> " <<  i << " --> " << tall << std::endl;

    // sort

    std::cout << " --> sorting ..." << std::endl;
    t1 = clock();
    sort((*dque).begin(), (*dque).end());
    t2 = clock();
    tall = (t2-t1)/static_cast<double>(CLOCKS_PER_SEC);
    std::cout << " --> sort --> " << i << " --> " << tall << std::endl;

    // clear

    std::cout << " --> deleting ..." << std::endl;
    t1 = clock();
    delete [] dque;
    t2 = clock();
    tall = (t2-t1)/static_cast<double>(CLOCKS_PER_SEC);
    std::cout << " --> delete --> " << i << " --> "<< tall << std::endl;
};

// the main function

int main()
{
    // local variables and parameters

    X my_x1;
    X my_x2;
    X my_x3;
    X my_x4;
    X my_x5;
    X my_x6;
    X my_x7;
    X my_x8;

    const long int I_MAX = static_cast<long int>(pow(10.0, 8.0));
    long int i;

    // setting the output formats

    std::cout << std::fixed;
    std::cout << std::showpoint;
    std::cout << std::setprecision(5);

    for (i = 0 ; i < I_MAX; i++) {
        std::cout << std::endl;
        std::cout << "----------------------------------------------->>> " << i << std::endl;
        std::cout << std::endl;

        std::thread th1(&X::do_lengthy_work, &my_x1, 1);
        std::thread th2(&X::do_lengthy_work, &my_x2, 2);
        std::thread th3(&X::do_lengthy_work, &my_x3, 3);
        std::thread th4(&X::do_lengthy_work, &my_x4, 4);
        std::thread th5(&X::do_lengthy_work, &my_x5, 5);
        std::thread th6(&X::do_lengthy_work, &my_x6, 6);
        std::thread th7(&X::do_lengthy_work, &my_x7, 7);
        std::thread th8(&X::do_lengthy_work, &my_x8, 8);

        // join the threads

        th1.join();
        th2.join();
        th3.join();
        th4.join();
        th5.join();
        th6.join();
        th7.join();
        th8.join();
    }

    // sentinel to exit

    int sentinel;
    std::cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
