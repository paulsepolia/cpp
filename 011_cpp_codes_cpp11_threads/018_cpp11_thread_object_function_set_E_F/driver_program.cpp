//============//
// c++11, set //
//============//

#include <iostream>
#include <iomanip>
#include <cmath>
#include <set>
#include <algorithm>
#include <ctime>
#include <thread>

using std::cout;
using std::cin;
using std::endl;
using std::set;
using std::less;
using std::thread;

//==========//
// buildSet //
//==========//

void buildSet(set<double, less<double>>* myset, const long int imax)
{
    long int i;
    for(i = 0; i < imax; i++) {
        (*myset).insert(cos(static_cast<double>(i)));
    }
}

//==========//
// clearSet //
//==========//

void clearSet(set<double, less<double>>* myset)
{
    (*myset).clear();
}

//============//
// emplaceSet //
//============//

void emplaceSet(set<double, less<double>>* myset, const long int imax)
{
    long int i;
    for(i = 0; i < imax; i++) {
        (*myset).emplace(cos(static_cast<double>(i)));
    }
}

//==============//
// findEraseSet //
//==============//

void findEraseSet(set<double, less<double>>* myset, const long int imax)
{
    long int i;
    for (i = 0; i < imax; i++) {
        (*myset).erase((*myset).find(cos(static_cast<double>(i))));
    }
}

// the main function

int main()
{
    // parameters

    const long int I_MAX = static_cast<long int>(pow(10.0, 7.0));
    const long int K_MAX = static_cast<long int>(pow(10.0, 7.0));
    long int k;

    for (k = 0; k < K_MAX; k++) {
        // counter

        cout << endl;
        cout << "------------------------------------------------------>> " << k << endl;
        cout << endl;

        // declare the sets

        set<double, less<double>>* S1 = new set<double, less<double>> [1];
        set<double, less<double>>* S2 = new set<double, less<double>> [1];
        set<double, less<double>>* S3 = new set<double, less<double>> [1];
        set<double, less<double>>* S4 = new set<double, less<double>> [1];

        // build sets in parallel

        cout << "--> sets are being built in parallel..." << endl;

        thread th01(buildSet, S1, I_MAX);
        thread th02(buildSet, S2, I_MAX);
        thread th03(buildSet, S3, I_MAX);
        thread th04(buildSet, S4, I_MAX);

        // finalize the building threads

        cout << "--> finalizing the building threads..." << endl;

        th01.join();
        th02.join();
        th03.join();
        th04.join();

        // emplace sets in parallel

        cout << "--> sets are being emplaced in parallel..." << endl;

        thread th05(emplaceSet, S1, I_MAX);
        thread th06(emplaceSet, S2, I_MAX);
        thread th07(emplaceSet, S3, I_MAX);
        thread th08(emplaceSet, S4, I_MAX);

        // finalize the emplacing threads

        cout << "--> finalizing the emplacing threads..." << endl;

        th05.join();
        th06.join();
        th07.join();
        th08.join();

        // erasing sets via find sets in parallel

        cout << "--> sets are being erased via find-and-erase in parallel..." << endl;

        thread th09(findEraseSet, S1, I_MAX);
        thread th10(findEraseSet, S2, I_MAX);
        thread th11(findEraseSet, S3, I_MAX);
        thread th12(findEraseSet, S4, I_MAX);

        // finalize the erasinf threads

        cout << "--> finalizing the find-and-erase threads..." << endl;

        th09.join();
        th10.join();
        th11.join();
        th12.join();

        // clear the sets in parallel

        cout << "--> sets are being cleared in parallel..." << endl;

        thread th13(clearSet, S1);
        thread th14(clearSet, S2);
        thread th15(clearSet, S3);
        thread th16(clearSet, S4);

        // finalize the clearing threads

        cout << "--> finalizing the clearing threads..." << endl;

        th13.join();
        th14.join();
        th15.join();
        th16.join();

        // delete sets

        cout << "--> deleting the threads..." << endl;

        delete [] S1;
        delete [] S2;
        delete [] S3;
        delete [] S4;
    }

    // the sentinel

    int sentinel;
    cin >> sentinel;
}

//======//
// FINI //
//======//
