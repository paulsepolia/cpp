//========//
// struct //
//========//

#include <iostream>
#include <cmath>
#include <thread>

// a struct

struct bigData {
    const long DIM_MAX = static_cast<long>(pow(10.0, 8.0));
    long i;
    double * a1 = new double [DIM_MAX];

    void funBigData()
    {
        for (i = 0; i < DIM_MAX; i++) {
            a1[i] = cos(static_cast<double>(i));
        }

        delete [] a1;
    }
};

// a void function

void fun()
{
    long k;
    const long K_MAX {10000000};

    for (k = 0; k < K_MAX; k++) {
        std::cout << "--------------------------------------->> " << k << std::endl;
        bigData t1;
        t1.funBigData();
    }
}

// the main program

int main()
{
    // start the threads

    std::thread th1(fun);
    std::thread th2(fun);
    std::thread th3(fun);
    std::thread th4(fun);
    std::thread th5(fun);
    std::thread th6(fun);
    std::thread th7(fun);
    std::thread th8(fun);

    // send them to the background

    th1.detach();
    th2.detach();
    th3.detach();
    th4.detach();
    th5.detach();
    th6.detach();
    th7.detach();
    th8.detach();

    // sentineling the exit

    std::cout << "you can enter an interger to exit anytime..." << std::endl;
    int sentinel;
    std::cin >> sentinel;
}

//======//
// FINI //
//======//
