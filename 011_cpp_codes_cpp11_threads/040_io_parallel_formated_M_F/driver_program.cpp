
#include <fstream>
#include <iostream>
#include <thread>
#include <vector>
#include <string>

using std::ifstream;
using std::ofstream;
using std::endl;
using std::cin;
using std::cout;
using std::ios;
using std::thread;
using std::vector;
using std::string;

void bencn_main(int i)
{
    ifstream inputStream;
    ofstream outputStream;
    string s1;
    const long K_MAX = 100000;

    s1 = "output" + std::to_string(i) + ".txt";

    inputStream.open("input.txt");
    outputStream.open(s1, ios::app);

    int x1;
    int x2;
    int x3;

    inputStream >> x1;
    inputStream >> x2;
    inputStream >> x3;

    // x1, x2 ,x3 have rubbish values
    // since the input file is miising

    for (long k = 0; k < K_MAX; k++) {
        outputStream << " x1 = " << x1 << endl;
        outputStream << " x2 = " << x2 << endl;
        outputStream << " x3 = " << x3 << endl;
    }

    inputStream.close();
    outputStream.close();

    //int sentinel;
    //cin >> sentinel;

}

// the main function

int main()
{
    vector<thread> vecThreads;

    const int NUM_THREADS = 8;
    const long J_MAX = 10000000;

    for (long j = 0; j < J_MAX; j++) {
        cout << "--------------------------------------------->> " << j << endl;

        for (int i = 0; i < NUM_THREADS; i++) {
            vecThreads.push_back(thread(bencn_main, i));
        }

        for (auto &t : vecThreads) {
            t.join();
        }

        vecThreads.clear();
    }

    return 0;
}

//======//
// FINI //
//======//
