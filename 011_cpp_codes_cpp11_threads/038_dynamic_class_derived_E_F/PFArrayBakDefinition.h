//========================//
// PFArrayBakDefinition.h //
//========================//

#ifndef PFARRAYDBAK_DEF
#define PFARRAYDBAK_DEF

#include "PFArrayDeclaration.h"
#include "PFArrayBakDeclaration.h"
#include <iostream>

using std::cout;

namespace pgg {
// default constructor

template <typename T, typename P>
PFArrayDBak<T,P>::PFArrayDBak() : PFArrayD<T,P>(), usedB(T(0))
{
    b = new P [PFArrayD<T,P>::capacity];
}

// non default expicit constructor

template <typename T, typename P>
PFArrayDBak<T,P>::PFArrayDBak(T capacityValue) : PFArrayD<T,P>(capacityValue),
    b(NULL), usedB(T(0))
{
    b = new P [capacityValue];
}

// copy constructor

template <typename T, typename P>
PFArrayDBak<T,P>::PFArrayDBak(const PFArrayDBak& oldObject) : PFArrayD<T,P>(oldObject),
    b(NULL), usedB(T(0))
{
    b = new P[PFArrayD<T,P>::capacity];
    usedB = oldObject.usedB;
    for (T i = 0; i < usedB; i++) {
        b[i] = oldObject.b[i];
    }
}

// backup public member function

template <typename T, typename P>
void PFArrayDBak<T,P>::backup()
{
    usedB = PFArrayD<T,P>::used;
    for (T i = 0; i < usedB; i++) {
        b[i] = PFArrayD<T,P>::a[i];
    }
}

// restore public member function

template <typename T, typename P>
void PFArrayDBak<T,P>::restore()
{
    PFArrayD<T,P>::used = usedB;
    for (T i = 0; i < PFArrayD<T,P>::used; i++) {
        PFArrayD<T,P>::a[i] = b[i];
    }
}

// = operator

template <typename T, typename P>
PFArrayDBak<T,P>& PFArrayDBak<T,P>::operator =(const PFArrayDBak& rightSide)
{
    T oldCapacity = PFArrayD<T,P>::capacity;

    PFArrayD<T,P>::operator = (rightSide);

    if (oldCapacity != rightSide.capacity) {
        delete [] b;
        b = new P [rightSide.capacity];
    }

    usedB = rightSide.usedB;

    for (T i = 0; i < usedB; i++) {
        b[i] = rightSide.b[i];
    }

    return *this;
}

template <typename T, typename P>
PFArrayDBak<T,P>::~PFArrayDBak()
{
    delete [] b;
}

} // pgg

#endif // PFARRAYDBAK_DEF

//======//
// FINI //
//======//
