//==============================//
// C++11, spawn several threads //
//==============================//

#include <iostream>
#include <cmath>
#include <ctime>
#include <iomanip>
#include <list>
#include <deque>
#include <thread>
#include <vector>
#include <algorithm>
#include <functional>

// a function

void do_work(int id) {
    std::cout << "------------->> " << id << std::endl;
};

// a function which forks and joins threads

void f() {

    // local variables and parameters

    int i;
    const int I_MAX = 2000000;
    std::vector<std::thread> threads;

    // create threads and do work

    for (i = 0; i < I_MAX; ++i) {
        threads.push_back(std::thread(do_work, i));
    }

    // join each thread

    std::for_each(threads.begin(), threads.end(), std::mem_fn(&std::thread::join));
}

// the main function

int main() {

    // call the function

    f();

    // the sentinel

    int sentinel;
    std::cin >> sentinel;

    // exit

    return 0;
}
