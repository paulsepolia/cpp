#!/bin/bash

  icpc -O3                \
       -xHost             \
       -Wall              \
       -pthread           \
       -openmp            \
       -std=c++11         \
       -static            \
       -wd2012            \
       driver_program.cpp \
       -o x_intel
