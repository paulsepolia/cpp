
//===========//
// functions //
//===========//

#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <iostream>
#include <thread>
#include <vector>

using std::endl;
using std::cout;
using std::vector;
using std::thread;

// type definition

typedef long long int intLL;

// function definition

intLL recFibNum(intLL a, intLL b, intLL n)
{
    if (n == 1) {
        return a;
    } else if (n == 2) {
        return b;
    } else {
        return recFibNum(a, b, n-1) + recFibNum(a, b, n-2);
    }
}

// bench fun

void benchFun(intLL firstFibNum, intLL secondFibNum, intLL nth)
{
    cout << "The fibonacci number at position " << nth
         << " is: " << recFibNum(firstFibNum, secondFibNum, nth) << endl;
}

#endif // FUNCTIONS_H

//======//
// FINI //
//======//
