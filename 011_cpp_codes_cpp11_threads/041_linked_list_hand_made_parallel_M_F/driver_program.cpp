
//================================//
// linked list                    //
// create and delete              //
// CREATE AND DELETE ONLY FORWARD //
//================================//

#include <iostream>
#include <cmath>
#include <iomanip>
#include <vector>
#include <thread>

using std::cout;
using std::cin;
using std::endl;
using std::pow;
using std::setprecision;
using std::showpos;
using std::showpoint;
using std::fixed;
using std::vector;
using std::thread;

// the node type

struct nodeType {
    long info;
    nodeType *link;
};

// the benchmark function

void benchFun ()
{
    const long I_MAX = static_cast<long>(pow(10.0, 7.2));
    const long J_MAX = static_cast<long>(pow(10.0, 8.0));

    cout << setprecision(10);
    cout << fixed;
    cout << showpos;
    cout << showpoint;

    nodeType *first;
    nodeType *last;
    nodeType *newNode;

    long num = 10; // get an integer

    for (long j = 0; j < J_MAX; j++) {
        cout << "---------------------------------------->> " << j << endl;

        first = NULL; // initialize
        last = NULL; // initialize

        // build forward

//                cout << " --> build" << endl;

        for (long i = 0; i < I_MAX; i++) {
            newNode = new nodeType;
            newNode->info = num;
            newNode->link = NULL;

            if (first == NULL) {
                first = newNode;
                last = newNode;
            } else {
                last->link = newNode;
                last = newNode;
            }

            num = i;

        } // end for

        // delete forward
        // only possible deletion

//              cout << " --> delete" << endl;

        nodeType *q;

        for (long i = 1; i < I_MAX; i++) {
            q = first->link;
            first->link = q->link;
            delete q;
        } // end for

    }
}

// the main function

int main()
{
    const int NUM_THREADS = 8;
    vector<thread> threadVec;

    // open threads

    for (int i = 0; i < NUM_THREADS; i++) {
        threadVec.push_back(thread(benchFun));
    }

    // join threads

    for (auto & t : threadVec) {
        t.join();
    }

    // clear the vector

    threadVec.clear();

    // sentineling

    int sentinel;
    cin >> sentinel;
}

//======//
// FINI //
//======//
