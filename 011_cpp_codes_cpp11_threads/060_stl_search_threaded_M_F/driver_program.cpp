
//======================//
// search STL algorithm //
//======================//

#include <iostream>
#include <vector>
#include <thread>
#include <string>
#include <iomanip>
#include <cmath>
#include <algorithm>

using std::endl;
using std::cout;
using std::cin;
using std::vector;
using std::string;
using std::thread;
using std::setprecision;
using std::showpos;
using std::showpoint;
using std::fixed;
using std::pow;
using std::cos;
using std::to_string;
using std::search;
using std::rand;
using std::random_shuffle;

// the bench function

void benchFun(const long & DIMEN_MAX, const string  & STR_PAT)
{
    const long K_MAX = static_cast<long>(pow(10.0, 5.0));
    vector<char>::iterator p;
    vector<char> * vecA = new vector<char>;
    vector<char> * vecB = new vector<char>;
    static long found = 0L;
    static long notFound = 0L;

    // set the output format

    cout << setprecision(20);
    cout << showpos;
    cout << showpoint;

    // build the vector with the pattern

    cout << " --> build the pattern vector" << endl;

    for (unsigned int i = 0; i < STR_PAT.size(); i++) {
        vecB->push_back(STR_PAT[i]);
    }

    // build the vector with elements the characters of the string

    cout << " --> build the vector with the integers" << endl;

    for (long i = 0; i < DIMEN_MAX; i++) {
        string a = to_string(rand()/10);
        vecA->push_back(a[0]);
    }

    for (long k = 0; k < K_MAX; k++) {
        // counter

        cout << "---------------------------------------------------------------->> " << k << endl;

        random_shuffle(vecA->begin(), vecA->end());

        p = search(vecA->begin(), vecA->end(), vecB->begin(), vecB->end());

        if (p != vecA->end()) {
            found = found+1L;
            cout << "-------------------------->> FOUND     --> " << found << endl;
        } else if (p == vecA->end()) {
            notFound = notFound+1L;
            cout << "-------------------------->> NOT FOUND --> " << notFound << endl;
        }
    }

    //cout << " --> delete the vectors" << endl;

    delete vecA;
    delete vecB;
}

// the main function

int main()
{
    // local variables and parameters

    const long DIMEN_MAX = static_cast<long>(pow(10.0, 8.0));
    const string STR_PAT = "1234567";
    const int NUM_THREADS = 4;
    vector<thread> vecThreads;

    // set the output format

    cout << setprecision(20);
    cout << showpos;
    cout << showpoint;

    // span the threads

    for (int j = 0; j < NUM_THREADS; j++) {
        vecThreads.push_back(thread(benchFun, DIMEN_MAX, STR_PAT));
    }

    // join the threads

    for (auto& t : vecThreads) {
        t.join();
    }

    // clear the threads vector

    vecThreads.clear();

    // sentineling

    int sentinel;
    cout << " --> enter an integer to exit: ";
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
