
//======//
// list //
//======//

#include <iostream>
#include <list>
#include <cmath>

using std::list;
using std::cout;
using std::cin;
using std::endl;

// 1 --> benchFunHeap

void benchFunHeap(const long & I_MAX)
{
    const long J_MAX = static_cast<long>(pow(10.0, 8.0));

    const double valA = 100.0;

    for (long j = 0; j < J_MAX; j++) {
        cout << "--------------------------------------------->> " << j << endl;

        list<double> * listA = new list<double>;
        list<double> * listB = new list<double>;

        cout << "--> build" << endl;

        //cout << " --> listA.assign(I_MAX,valA);" << endl;

        listA->assign(I_MAX,valA); // I_MAX doubles with value valA

        //cout << " --> listB.assign(listA.begin(), listB.end());" << endl;

        listB->assign(listA->begin(), listA->end()); // a copy of first

        //cout << " --> listA.size() = " << listA->size() << endl;

        //cout << " --> listA.size() = " << listB->size() << endl;

        cout << "--> delete" << endl;

        delete listA;
        delete listB;

        listA = NULL;
        listB = NULL;
    }
}

// 2 --> benchFunStack

void benchFunStack(const long & I_MAX)
{
    const long J_MAX = static_cast<long>(pow(10.0, 8.0));

    const double valA = 100.0;

    for (long j = 0; j < J_MAX; j++) {
        cout << "--------------------------------------------->> " << j << endl;

        list<double> listA;
        list<double> listB;

        cout << "--> build" << endl;
        //cout << " --> listA.assign(I_MAX,valA);" << endl;

        listA.assign(I_MAX,valA); // I_MAX doubles with value valA

        //cout << " --> listB.assign(listA.begin(), listB.end());" << endl;

        listB.assign(listA.begin(), listA.end()); // a copy of first

        //cout << " --> listA.size() = " << listA.size() << endl;

        //cout << " --> listA.size() = " << listB.size() << endl;

        cout << "--> delete" << endl;
    }
}

//======//
// FINI //
//======//
