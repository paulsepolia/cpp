
//======//
// list //
//======//

#include <iostream>
#include <vector>
#include <thread>
#include "functions.h"

using std::endl;
using std::cin;
using std::vector;
using std::thread;

// the main function

int main()
{
    const int NUM_THREADS = 4;
    const long I_MAX = static_cast<long>(pow(10.0, 7.3));
    vector<thread> thVec;

    // create threads

    for (int i = 0; i < NUM_THREADS; i++) {
        thVec.push_back(thread(benchFunHeap, I_MAX));
        //thVec.push_back(thread(benchFunStack, I_MAX));
    }

    // join threads

    for (int i = 0; i < NUM_THREADS; i++) {
        thVec[i].join();
    }

    // clear vector

    thVec.clear();

    // sentineling

    int sentinel;
    cin >> sentinel;

    return 0;
}

//======//
// FINI //
//======//
